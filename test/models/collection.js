

import sinon from 'sinon';
import { describe, it } from 'mocha';
import { expect, assert } from 'chai';
import fs from 'node:fs';
import path from 'node:path';
import { URL } from 'url';
const __dirname = new URL('.', import.meta.url).pathname;

import Util from '../../lib/util.js';
import { BasicCollection } from '../../lib/models/collection.js';

const data1 = JSON.parse( fs.readFileSync( path.join( __dirname, 'data1.json' ), { encoding: 'utf8' } ) );

describe( 'Simple Collection', () => { 
  const app = {};
  let sandbox, model1, rec1;

  const model1ConfigA = {
    'idField': 'someId',
    'reqFields': [ 'date', 'name', 'color' ],
    'defaultValues': {
      'abc': 123,
      'date': () => { return Date.now() }
    }
  };
  const model1ConfigB = {
    'optFields': [ 'abc', 'str', 'email', 'gender', 'anotherId' ],
  };

  before( () => { 
    sandbox = sinon.createSandbox();
    app.log = sandbox.stub();
    app.util = Util;
  } );

  after( () => {
  } );


  it( 'instantiate', () => {
    model1 = new BasicCollection( app, model1ConfigA, model1ConfigB );
    assert( model1 );
    assert( model1 instanceof BasicCollection );
    expect( model1.config ).to.be.an( 'object' ).to.have.all.keys( 'defaultValues', 'idField', 'optFields', 'reqFields', 'indexFields', 'modelType' );
    assert( model1.config.idField == model1ConfigA.idField );
    expect( model1.config.defaultValues ).to.be.an( 'object' ).to.have.all.keys( 'abc', 'date', 'someId' );
  } );


  it( 'setup', () => {
    expect( () => { model1.setup() } ).to.not.throw();
    expect( model1._data ).to.be.instanceof( Map );
    expect( model1._ids ).to.be.instanceof( Array );
    expect( model1._idxs ).to.be.instanceof( Object );
  } );


  it( 'default record', () => { 
    let defaultrec;
    expect( () => { defaultrec = model1.defaultRecord() } ).to.not.throw();
    expect( defaultrec ).to.be.an( 'object' );
    expect( defaultrec ).to.have.all.keys( 'someId', 'abc', 'date' );
    assert( defaultrec.someId == "" );
    assert( defaultrec.abc === 123 );
    expect( defaultrec.date ).to.be.a( 'number' );
  } );


  it( "add blank record fails", () => {
    let defaultrec, err;
    defaultrec = model1.defaultRecord();
    defaultrec.someId = 123;

    try {
      model1.add( defaultrec );
    }
    catch( e ) {
      err = e;
    }

    expect( err ).to.be.instanceof( Error ).to.match( /missing one or more/ );
  } );


  it( "add ok record", () => {
    let rec, outrec, err;
    rec = model1.defaultRecord();
    rec.color = "Foo";
    rec.name = "Bar";
    rec.someId = 123;

    try {
      model1.add( rec );
      outrec = model1._data.get( rec.someId );
    }
    catch( e ) {
      err = e;
    }

    assert( ! err );
    expect( outrec ).to.include( { 
      someId: 123,
      abc: 123,
      name: 'Bar',
      color: 'Foo',
      gender: undefined
    } );

    rec1 = outrec;
  } );


  it( "duplicate add fails", () => {
    let rec, err;
    rec = model1.defaultRecord();
    rec.color = "Foo";
    rec.name = "Bar";
    rec.someId = 123;

    try {
      model1.add( rec );
    }
    catch( e ) {
      err = e;
    }

    assert( err );
    expect( err ).to.match( /existing ID/ );

  } );


  it( "get one record", () => {
    let safe, unsafe;
    
    safe = model1.getRecord( rec1.someId );
    unsafe = model1._getRecord( rec1.someId );

    assert( safe !== unsafe );
    assert( safe !== model1._data.get( rec1.someId ) );
    assert( unsafe === model1._data.get( rec1.someId ) );
    assert( unsafe === rec1 );
    expect( safe ).to.include( rec1 );
  } );


  it( "fetch records", async () => {
    let out, err, complete = false;

    try {
      out = await model1.fetch();
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( complete );
    assert( ! err );
    expect( out ).to.be.an( 'array' ).to.have.length( 1 );
    expect( out[0] ).to.include( rec1 );
  } );


  it( "update without change succeeds", () => {
    let updated;
    expect( () => { updated = model1.update( rec1 ) } ).to.not.throw();
    expect( updated ).to.be.an( 'array' ).to.have.length( 1 ).to.include( 123 );
  } );


  it( "update record", () => {
    let rec, outrec, updated;
    rec = Object.assign( {}, rec1 );
    rec.color = "Brown";

    expect( () => { updated = model1.update( rec ) } ).to.not.throw();
    outrec = model1._data.get( rec.someId );
    expect( updated ).to.be.an( 'array' ).to.include( 123 );
    expect( outrec ).to.include( rec );

    rec1 = outrec;
  } );

} );


describe( 'Large Collection', () => { 
  const app = {};
  let sandbox, model1;

  const model1Config = {
    'idField': 'someId',
    'reqFields': [ 'created', 'name', 'color', 'lastseen' ],
    'optFields': [ 'email', 'ip_address', 'gender', 'anotherId' ],
    'indexFields': [ 'gender', 'color' ],
    'defaultValues': {
      'lastseen': 0,
      'created': () => { return Date.now() }
    }
  };

  before( () => { 
    sandbox = sinon.createSandbox();
    app.log = sandbox.stub();
    app.util = Util;
  } );

  after( () => {
  } );


  it( 'instantiate', () => {
    model1 = new BasicCollection( app, model1Config );
    assert( model1 );
    assert( model1 instanceof BasicCollection );
    expect( model1.config ).to.be.an( 'object' ).to.have.all.keys( 'defaultValues', 'idField', 'optFields', 'reqFields', 'indexFields', 'modelType' );
    assert( model1.config.idField == model1Config.idField );
    expect( model1.config.defaultValues ).to.be.an( 'object' ).to.have.all.keys( 'created', 'lastseen', 'someId' );

    sandbox.spy( model1 );
  } );


  it( 'setup', () => {
    expect( () => { model1.setup() } ).to.not.throw();
    expect( model1._data ).to.be.instanceof( Map );
    expect( model1._ids ).to.be.instanceof( Array );
    expect( model1._idxs ).to.be.instanceof( Object );
  } );


  it( 'add records', () => {
    let added;
    expect( () => { added = model1.add( data1 ) } ).to.not.throw();
    expect( added ).to.be.an( 'array' ).to.have.length( 1000 );
  } );


  it( 'indexes built', () => {
    let indexes = model1._idxs;
    expect( indexes ).to.be.an( 'object' ).to.have.all.keys( 'gender', 'color' );
  } );


  it( 'fetch without filter', async () => {
    let recs, err, complete = false;

    sandbox.resetHistory();

    try {
      recs = await model1.fetch();
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 1000 );
    assert( model1._filter.notCalled );
  } );


  // Case sensitive index filter
  it( 'filtered fetch 1', async() => {
    let recs, err, complete = false;
    const filters = {
      ':rules': [ { 'gender': { 'eq': 'FEMALE' } } ]
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, undefined, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 0 );
    assert( model1._applyIndexFilter.calledOnce );
    assert( model1._applyFilter.calledOnce );
  } );


  // Case insensitive index filter
  it( 'filtered fetch 2', async() => {
    let recs, err, complete = false;
    const filters = {
      ':rules': [
        { 'gender': { 'eq': 'FEMALE', ':case': false } }
      ]
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, undefined, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 413 );
    assert( model1._applyIndexFilter.calledOnce );
    assert( model1._applyFilter.calledOnce );
  } );


  // Or filter with name part of gender subset
  it( 'filtered fetch 3', async() => {
    let recs, err, complete = false;
    const filters = {
      ':rules': [
        { 'name': { 'eq': 'Christie' } },
        { 'gender': { 'eq': 'FEMALE', ':case': false } }
      ],
      ':mode': "OR"
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, undefined, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 413 );
    assert( model1._applyIndexFilter.calledOnce );
    assert( model1._applyFilter.calledTwice );
  } );


  // Or filter with name a sub filter
  it( 'filtered fetch 4', async() => {
    let recs, err, complete = false;
    const filters = {
      ':rules': [
        { 'name': { 'eq': 'Jeremie' } }, // Matches one record
        { 'gender': { 'eq': 'FEMALE', ':case': false } }
      ],
      ':mode': 'OR'
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, undefined, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 414 );
    assert( model1._applyIndexFilter.calledOnce );
    assert( model1._applyFilter.calledTwice );
  } );


  // AND filter on two indexed fields
  it( 'filtered fetch 4', async() => {
    let recs, err, complete = false;
    const filters = { 
      ':rules': [
        { 'color': { 'eq': 'Green' } },
        { 'gender': { 'eq': 'FEMALE', ':case': false } }
      ]
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, undefined, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 13 );
    assert( model1._applyIndexFilter.calledTwice );
    assert( model1._applyFilter.calledTwice );
  } );


  // AND filter on one indexed, one non indexed field
  it( 'filtered fetch 5', async() => {
    let recs, err, complete = false;
    const filters = { 
      ':rules': [
        { 'color': { 'eq': 'Green' } },
        { 'gender': { 'eq': 'FEMALE', ':case': false } }
      ]
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, undefined, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 13 );
    assert( model1._applyIndexFilter.calledTwice );
    assert( model1._applyFilter.calledTwice );
  } );


  // Filter on non indexed field with null
  it( 'filtered fetch 6', async() => {
    let recs, err, complete = false;
    const filters = { 
      ':rules': [
        { 'email': { 'eq': null } }
      ]
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, undefined, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 121 );
    assert( model1._applyIndexFilter.notCalled );
    assert( model1._applyFilter.calledOnce );
  } );


  // Filter on non indexed field with starts with
  it( 'filtered fetch 7', async() => {
    let recs, err, complete = false;
    const filters = { 
      ':rules': [
        { 'anotherId': { 'sw': '18' } }
      ]
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, undefined, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 9 );
    assert( model1._applyIndexFilter.notCalled );
    assert( model1._applyFilter.calledOnce );
  } );


  // Filter on non indexed and indexed field with bad value
  it( 'filtered fetch 8', async() => {
    let recs, err, complete = false;
    const filters = { 
      ':rules': [
        { 'anotherId': { 'sw': '18' } },
        { 'gender': { 'eq': 'Something' } }
      ]
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, undefined, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 0 );
    assert( model1._applyIndexFilter.calledOnce );
    assert( model1._applyFilter.calledTwice );
  } );


  // Filter on indexed field with in 
  it( 'filtered fetch 9', async() => {
    let recs, err, complete = false;
    const filters = { 
      ':rules': [
        { 'gender': { 'in': [ 'Female', 'Polygender' ] } }
      ]
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, undefined, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 427 );
    assert( model1._applyIndexFilter.calledOnce );
    assert( model1._applyFilter.calledOnce );
  } );


  // Filter on indexed field with in 
  it( 'filtered fetch 10', async() => {
    let recs, err, complete = false;
    const filters = {
      ':rules': [
        { 'gender': { 'ct': 'gender' } }
      ]
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, undefined, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 36 );
    assert( model1._applyIndexFilter.calledOnce );
    assert( model1._applyFilter.calledOnce );
  } );


  // Filter on indexed field with ends with
  it( 'filtered fetch 11', async() => {
    let recs, err, complete = false;
    const filters = { 
      ':rules': [
        { 'gender': { 'ew': 'gender' } }
      ]
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, undefined, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 36 );
    assert( model1._applyIndexFilter.calledOnce );
    assert( model1._applyFilter.calledOnce );
  } );


  // Filter on non indexed field with greater than
  it( 'filtered fetch 12', async() => {
    let recs, err, complete = false;
    const filters = {
      ':rules': [
        { 'someId': { 'gt': 995 } }
      ]
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, undefined, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 5 );
    assert( model1._applyIndexFilter.notCalled );
    assert( model1._applyFilter.calledOnce );
  } );


  // OR Filter on non indexed field with multi less than
  it( 'filtered fetch 13', async() => {
    let recs, err, complete = false;
    const filters = { 
      ':rules': [
        { 'someId': { 
          ':rules': [
            { 'lt': 0.5 }, 
            { 'lt': 5 } 
          ],
          ':mode': "OR"
        } }
      ]
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, undefined, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 4 );
    assert( model1._applyIndexFilter.notCalled );
    assert( model1._applyFilter.calledOnce );
  } );


  // OR Filter on non indexed field with less than or equal and greater or equal
  it( 'filtered fetch 14', async() => {
    let recs, err, complete = false;
    const filters = { 
      ':rules': [
        { 'someId': { 
          ':rules': [ { 'le': 3 }, { 'ge': 990 } ],
          ':mode': "OR"
        } }
      ]
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, undefined, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 14 );
    assert( model1._applyIndexFilter.notCalled );
    assert( model1._applyFilter.calledOnce );
  } );


  // OR Filter on indexed & non indexed field with nested ORs
  it( 'filtered fetch 15', async() => {
    let recs, err, complete = false;
    const filters = { 
      ':rules': [
        { 'someId': { 
          ':rules': [ 
            { 'lt': 0.5 }, 
            { 'lt': 5 }
          ],
          ':mode': "OR"
        } },
        { 'gender': { 
          ':rules': [ 
            { 'eq': 'polygender', ':case': false }, 
            { 'eq': null } 
          ],
          ':mode': "OR"
        } }
      ],
      ':mode': "OR"
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, undefined, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 139 );
    assert( model1._applyIndexFilter.calledOnce );
    assert( model1._applyFilter.calledTwice );
  } );


  // AND Filter on indexed & non indexed field with nested ORs
  it( 'filtered fetch 16', async() => {
    let recs, err, complete = false;
    const filters = { 
      ':rules': [
        { 'someId': { 
          ':rules': [ 
            { 'ge': 900 }, 
            { 'lt': 5 } 
          ],
          ':mode': "OR"
        } },
        { 'gender': { 
          ':rules': [ 
            { 'eq': 'female', ':case': false }, 
            { 'eq': null } 
          ],
          ':mode': "OR"
        } }
      ],
      ':mode': "AND"
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, undefined, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 55 );
    assert( model1._applyIndexFilter.calledOnce );
    assert( model1._applyFilter.calledTwice );
  } );


  // Filter on indexed field with function
  it( 'filtered fetch 17', async() => {
    let recs, err, complete = false;
    const filters = { 
      ':rules': [
        { 'gender': function(v) { return v && v.endsWith( 'gender' ) } }
      ]
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, undefined, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 36 );
    assert( model1._applyIndexFilter.calledOnce );
    assert( model1._applyFilter.calledOnce );
  } );



  // Filter on non indexed field with function
  it( 'filtered fetch 18', async() => {
    let recs, err, complete = false;
    const filters = { 
      ':rules': [
        { 'name': function(v) { return v && v.indexOf( 'Dor' ) !== -1 } }
      ]
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, undefined, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 6 );
    assert( model1._applyIndexFilter.notCalled );
    assert( model1._applyFilter.calledOnce );
  } );


  // Or filter with record-level functions
  it( 'filtered fetch 19', async() => {
    let recs, err, complete = false;
    const filters = {
      ':rules': [
        function(r,m) { return r.filter( id => m.getRecord(id)[ 'name' ] == "Christie" ) },
        function(r,m) { return r.filter( id => m.getRecord(id)[ 'gender' ] == "Female" ) }
      ],
      ':mode': "OR"
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, undefined, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 413 );
    assert( model1._applyIndexFilter.notCalled );
    assert( model1._applyFilter.notCalled );
  } );


  // Deep filter on non indexed field with starts with
  it( 'filtered fetch 20', async() => {
    let recs, err, complete = false;
    const filters = { 
      ':rules': [ {
        ':rules': [
          { 'anotherId': { 'sw': '18' } }
        ]
      } ]
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, undefined, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 9 );
    assert( model1._applyIndexFilter.notCalled );
    assert( model1._applyFilter.calledOnce );
  } );


  // Deep filter on indexed field with in 
  it( 'filtered fetch 21', async() => {
    let recs, err, complete = false;
    const filters = {
      ':rules': [ {
          ':rules': [
            { 'gender': { 'ct': 'gender' } }
          ]
      } ]
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, undefined, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 36 );
    assert( model1._applyIndexFilter.calledOnce );
    assert( model1._applyFilter.calledOnce );
  } );





  // Unique valus for non-existant field
  it( 'Unique values 1', () => {
    let vals;

    expect( () => { vals = model1.uniqueValues( 'notReal' ) } ).to.not.throw();
    expect( vals ).to.be.an( 'array' ).to.have.length( 1 );
    assert( vals[0] === undefined );
  } );


  // Unique valus for non indexed field
  it( 'Unique values 2', () => {
    let vals;

    expect( () => { vals = model1.uniqueValues( 'name' ) } ).to.not.throw();
    expect( vals ).to.be.an( 'array' ).to.have.length( 948 );
  } );


  // Unique valus for indexed field
  it( 'Unique values 3', () => {
    let vals;

    expect( () => { vals = model1.uniqueValues( 'color' ) } ).to.not.throw();
    expect( vals ).to.be.an( 'array' ).to.have.length( 19 );
  } );


  // Sort on non indexed field
  it( 'sorted fetch 1', async() => {
    let recs, err, complete = false;
    const sorts = [ 
      { field: 'name', direction: 'asc', type: 'string' }
    ];

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, sorts  );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 1000 );
    assert( recs[0].name.match( /^A/ ) );
    assert( recs[999].name.match( /^Z/ ) );
    assert( model1._sort.calledOnce );
  } );


  // Sort on indexed field
  it( 'sorted fetch 2', async() => {
    let recs, err, complete = false;
    const sorts = [ 
      { field: 'color', direction: 'asc', type: 'string' }
    ];

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, sorts  );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 1000 );
    assert( recs[0].color.match( /^Aqua/ ) );
    assert( recs[999].color.match( /^Yellow/ ) );
    assert( model1._sort.calledOnce );
  } );


  // Sort on missing field
  it( 'sorted fetch 3', async() => {
    let recs, err, complete = false;
    const sorts = [ 
      { field: 'nothing', direction: 'asc', type: 'string' }
    ];

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, 10, sorts  );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    expect( err ).to.match( /Unknown sort field/ );
    assert( ! complete );
    assert( model1._sort.calledOnce );
  } );


  // Multi-field sort on partial set with non-indexed field
  it( 'sorted fetch 4', async() => {
    let recs, err, complete = false;
    const sorts = [ 
      { field: 'color', direction: 'asc', type: 'string' },
      { field: 'name', direction: 'desc', type: 'string' }
    ];

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, 50, sorts  );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 50 );
    assert( recs[0].name.match( /^Venita/ ) );
    assert( recs[0].color.match( /^Aquamarine/ ) );
    assert( recs[46].name.match( /^Abigael/ ) );
    assert( recs[46].color.match( /^Aquamarine/ ) );
    assert( recs[47].name.match( /^Zahara/ ) );
    assert( recs[47].color.match( /^Blue/ ) );
    assert( model1._sort.calledOnce );
  } );


  // Sort on partial set with generic function
  it( 'sorted fetch 5', async() => {
    let recs, err, complete = false;
    const sorts = [ 
      (a,b) => { return b - a }
    ];

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, 10, sorts  );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 10 );
    assert( recs[0].someId === 1000 );
    assert( recs[9].someId === 991 );
    assert( model1._sort.calledOnce );
  } );


  // Sort on indexed field with field sort function
  it( 'sorted fetch 6', async() => {
    let recs, err, complete = false;
    const sorts = [ 
      { field: 'color', func: (a,b) => { return a.length - b.length } }
    ];

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, sorts  );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 1000 );
    assert( recs[0].color === "Red" );
    assert( recs[999].color === "Aquamarine" );
    assert( model1._sort.calledOnce );
  } );



  // Sort on filtered indexed filter
  it( 'sorted fetch 6', async() => {
    let recs, err, complete = false;
    const sorts = [ 
      { field: 'name', direction: "desc", type: "string" }
    ];

    const filters = {
      ':rules': [
        { 'color': { 'eq': 'Green' } },
      ]
    };

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, undefined, sorts, filters );
      complete = true;
    }
    catch( e ) {
      err = e;
    }

    assert( ! err, 'no error' );
    assert( complete );
    expect( recs ).to.be.an( 'array' ).to.have.length( 43 );
    assert( recs[42].name === "Adams" );
    assert( recs[0].name === "Yule" );
    assert( model1._applyFilter.calledOnce );
    assert( model1._sort.calledOnce );
  } );



} );


