
import http from 'node:http';
import sinon from 'sinon';
import { describe, it } from 'mocha';
import { use as chaiUse, expect, assert } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import fs from 'node:fs';
import path from 'node:path';
import { URL } from 'url';
import { v4 as uuidv4 } from 'uuid';
const __dirname = new URL('.', import.meta.url).pathname;

import Util from '../../lib/util.js';
import { SimplePollingCollection } from '../../lib/models.js';
import exp from 'node:constants';

const data1 = JSON.parse( fs.readFileSync( path.join( __dirname, 'data1.json' ), { encoding: 'utf8' } ) );

chaiUse( chaiAsPromised );

describe( 'Polling Collection', () => {
  const app = {};
  let sandbox, model1, server, port, webconncount = 0, returnStamp = 1234;
  let updFunc, insFunc, delFunc, ids = {};

  const model1Config = {
    'idField': 'someId',
    'reqFields': [ 'name', 'color', 'anotherId' ],
    'versioned': true,
    'versionField': 'anotherId',
    'defaultValues': { 'anotherId': null },
    'optFields': [ 'email', 'gender', 'ip_address' ],
    'url': {},
    'batchSize': 1,
    'pollTime': 30,
    'pollType' : 'auto',
    'pollUnbound': false,
    'pollEmpty': true
  };


  before( () => {
    sandbox = sinon.createSandbox();
    app.log = sandbox.stub();
    app.util = Util;
    global.document = { createDocumentFragment: sandbox.stub() };
    global.document.createDocumentFragment.returns( {
      'addEventListener': () => {},
      'dispatchEvent': () => {},
      'removeEventListener': () => {}
    } );
    global.CustomEvent = sandbox.stub();
    global.window = { location: { origin: "http://127.0.0.1/" } };
  } );

  after( () => {
    delete global.document;
    delete global.CustomEvent;
    delete global.window;
    delete global.WebSocket;

    if ( server ) {
      server.close();
      server = undefined;
    }
  } );

  it( 'instantiate', () => {
    model1 = new SimplePollingCollection( app, model1Config );
    assert( model1 );
    assert( model1.constructor.name == "SimplePollingCollection" );
    expect( model1.config ).to.be.an( 'object' ).to.have.all.keys( 'idField', 'optFields', 'reqFields', 'defaultValues',
      'indexFields', 'url', 'versioned', 'versionField', 'batchSize', 'modelType',
      'pollTime', 'pollType', 'pollUnbound', 'pollEmpty', 'reloadQuery', 'reloadContext', 'loadFirstFetch'
    );
    assert( model1.config.idField == model1Config.idField );
    expect( model1.config.optFields ).to.be.an( 'array' ).to.contain( 'email', 'gender', 'ip_address', 'anotherId' );
    expect( model1.config.reqFields ).to.be.an( 'array' ).to.contain(  'name', 'color' );
    expect( model1.config.defaultValues ).to.be.an( 'object' ).to.have.all.keys( 'someId', 'anotherId' );
    assert( model1.config.modelType.includes( 'polling' ) );
    assert( model1.config.pollEmpty == true );
    assert( model1.config.pollUnbound == false );
  } );


  it( 'test server setup', () => {

    const routeFunc = async ( req, res ) => {
      const url = req.url;
      let body = '';

      const prom = new Promise( (resolve, reject) => {
        webconncount++;
        req.on( 'end', () => { resolve(); } );
        req.on( 'error', () => { reject() } );
        if ( req.method == "POST" ) {
          req.on( "data", (bit) => { body += bit } )
        }
        else
          resolve();
      } );

      await prom;

      if ( url.indexOf( "/notfound" ) !== -1 ) {
        res.statusCode = "404";
        return res.end( `<html>Not found</html>` );
      }

      if ( url.indexOf( '/pollbatch' ) !== -1 ) {
        res.writeHead( 200, { 'content-type': 'application/json' });
        return res.end( JSON.stringify( {
          success: true,
          stamp: returnStamp
        } ) )
      }

      if ( url.indexOf( '/polleach' ) !== -1 ) {
        res.writeHead( 200, { 'content-type': 'application/json' });
        return res.end( JSON.stringify( {
          success: true,
          records: [ Object.assign( {}, data1[0], { 'name': 'Barry', 'anotherId': uuidv4() } ) ]
        } ) )
      }

      if ( url.indexOf( '/fetch1' ) !== -1 ) {
        res.writeHead( 200, { 'content-type': 'application/json' });
        return res.end( JSON.stringify( {
          success: true,
          records: data1.slice( 0, 50 ),
          stamp: 1234
        } ) );
      }
      if ( url.indexOf( '/fetch2' ) !== -1 ) {
        res.writeHead( 200, { 'content-type': 'application/json' });
        return res.end( JSON.stringify( {
          success: true,
          records: data1.slice( 0, 40 ),
          stamp: 2345
        } ) );
      }
      if ( url.indexOf( '/fetch3' ) !== -1 ) {
        res.writeHead( 200, { 'content-type': 'application/json' });
        return res.end( JSON.stringify( {
          success: true,
          records: data1.slice( 0, 60 ),
          stamp: 3456
        } ) );
      }

      res.statusCode = "500";
      res.writeHead(200, { 'content-type': 'application/json' });
      return res.end( JSON.stringify({message:"catch all failure"}) );

    };

    server = http.createServer( routeFunc );
  } );


  it( 'model setup', () => {
    expect( () => { model1.setup() } ).to.not.throw();
    expect( model1._data ).to.be.instanceof( Map );
    expect( model1._ids ).to.be.instanceof( Array );
    assert( model1._loaded == false );
    assert( model1._pollTimer == undefined );
    assert( model1._lastStamp == undefined );
    assert( model1._pollFailed == undefined );
    sandbox.spy( model1 );
  } );


  it( 'failed load', async () => {
    let error, complete = false;
    sandbox.resetHistory();
    model1.config.url.load = 'http://127.0.0.2:1234';
    try {
      await model1.load();
    }
    catch( err ) {
      error = err;
    }
    assert( complete == false );
    expect( error ).to.be.an( 'error' ).to.match( /fetch failed/ );
    assert( model1._load.calledOnce );
    assert( model1._remoteRequest.calledOnce );
    assert( model1._loaded == false );
    assert( model1.stopPolling.calledOnce );
    assert( model1.startPolling.notCalled );
    assert( webconncount == 0 );
  } );



  it( 'test server listen', async () => {
    const listening = new Promise( (resolve) => {
      server.on( 'listening', () => { resolve() } );
    } );
    server.listen( 0, '127.0.0.1' );
    await listening;
    port = server.address().port;
  } );


  // Polling before loading fails
  it( 'poll before load failure', async() => {
    sandbox.resetHistory();
    await model1.poll();
    expect( app.log.firstCall.args[1] ).to.match( /Thwarting polling for non-loaded/ );
    assert( model1._pollTimer !== undefined );
    clearTimeout( model1._pollTimer );
    model1._pollTimer = undefined;
  } );


  // Test adding records before loading
  it( 'add before fetch', () => {
    let added, modified = Object.assign( {}, data1[0] );

    delete modified[ 'someId' ]
    modified.color = 'Red';
    modified.name = 'Greg';
    modified.gender = 'Unknown';
    modified.email = 'mme@herre.com';
    modified.anotherId = uuidv4();

    sandbox.resetHistory();

    model1.clear();

    added = model1.add( data1[0] );

    assert( added[0] == data1[0].someId );
    assert( model1._ids.length == 1 );
    assert( model1.isChanged() == true );
    expect( model1.getRecordInfo( added[0] ) )
      .to.be.an( 'object' )
      .to.have.property( 'operation', 'insert' );
    expect( model1.getChanges( added[0] ) )
      .to.be.an( 'object' )
      .to.contain( data1[ 0 ] );

    added = model1.add( modified );

    assert( added[0] < 0 );
    assert( model1._ids.length == 2 );
    assert( model1.isChanged() == true );
    expect( model1.getRecordInfo( added[0] ) )
      .to.be.an( 'object' )
      .to.have.property( 'operation', 'insert' );
    expect( model1.getChanges( added[0] ) )
      .to.be.an( 'object' )
      .to.contain( modified );

  } );



  it( 'load', async() => {
    let result;
    sandbox.resetHistory();
    model1.config.url.load = 'http://127.0.0.1:' + port + '/:foo:';
    result = await model1.load( { 'foo': 'fetch1' } );
    assert( model1._remoteRequest.calledOnce );
    assert( model1._loaded );
    assert( model1._inconsistent == false );
    assert( model1.parse.calledOnce );
    assert( result.load.length == 50 );
    assert( model1._ids.length == 52 );
    assert( model1._lastStamp == 1234 );

    assert( model1.stopPolling.calledOnce );
    assert( model1.startPolling.calledOnce );
    assert( model1._pollTimer !== undefined );
    clearTimeout( model1._pollTimer );
    model1._pollTimer = undefined;

    assert( webconncount == 1 );
    webconncount = 0;
  } );



  it( 'poll failure not bound', async() => {
    sandbox.resetHistory();
    model1.config.pollUnbound = false;
    await model1.poll();
    expect( app.log.firstCall.args[1] ).to.match( /Thwarting polling for unbound/ );
    assert( model1._pollTimer !== undefined );
    clearTimeout( model1._pollTimer );
    model1._pollTimer = undefined;
  } );



  it( 'poll failure no url', async() => {
    sandbox.resetHistory();
    model1.config.pollUnbound = true;
    await model1.poll();
    assert( model1.pollBatch.calledOnce );
    assert.isRejected(model1.pollBatch.firstCall.returnValue );

    assert( model1._pollTimer !== undefined );
    clearTimeout( model1._pollTimer );
    model1._pollTimer = undefined;
  } );



  it( 'batch poll without update', async () => {
    sandbox.resetHistory();
    model1.config.url.poll = 'http://127.0.0.1:' + port + '/pollbatch';

    await model1.poll();

    assert( model1.pollBatch.calledOnce );
    assert( model1._remoteRequest.calledOnce );
    assert.isFulfilled(model1.pollBatch.firstCall.returnValue );
    assert( model1._loadDelta.notCalled );

    assert( model1._pollTimer !== undefined );
    clearTimeout( model1._pollTimer );
    model1._pollTimer = undefined;
  } );


  it( 'batch poll with update', async () => {
    sandbox.resetHistory();

    model1.config.url.load = 'http://127.0.0.1:' + port + '/fetch2';
    model1.config.url.poll = 'http://127.0.0.1:' + port + '/pollbatch';

    returnStamp = 2345;

    assert( model1._lastStamp < returnStamp );

    await model1.poll();

    assert( model1.pollBatch.calledOnce );
    assert( model1._remoteRequest.calledTwice );
    assert.isFulfilled(model1.pollBatch.firstCall.returnValue );
    assert( model1._load.calledOnce );

    assert( model1.length == 42 );
    assert( model1._lastStamp == 2345 );

    assert( model1._pollTimer !== undefined );
    clearTimeout( model1._pollTimer );
    model1._pollTimer = undefined;
  } );


  it( 'reload updates stamp', async () => {
    sandbox.resetHistory();

    model1.config.url.load = 'http://127.0.0.1:' + port + '/fetch3';

    await model1.reload();

    assert( model1._remoteRequest.calledOnce );
    assert( model1.length == 62 );
    assert( model1._lastStamp == 3456 );

    assert( model1._pollTimer !== undefined );
    clearTimeout( model1._pollTimer );
    model1._pollTimer = undefined;
  } );



  it( 'each poll with update', async () => {
    sandbox.resetHistory();
    model1.config.pollType = 'each';
    model1.config.url.poll = 'http://127.0.0.1:' + port + '/polleach';

    await model1.poll();

    assert( model1.pollEach.calledOnce );
    assert( model1._remoteRequest.calledOnce );
    assert( model1._remoteRequest.firstCall.args[2].records.length == 60 );
    expect( model1._remoteRequest.firstCall.args[2].records[0] )
      .to.be.an( 'object' )
      .to.have.keys( 'someId', 'anotherId' );
    assert( model1.parse.calledOnce );
    assert( model1.parse.firstCall.args[ 0 ].length == 1 );

    assert( model1.getRecord( data1[ 0 ].someId ).anotherId !== data1[0].anotherId );
    assert( model1.getRecord( data1[ 0 ].someId ).name == "Barry" );

    // Polling stopped in next test
  } );



  it( 'stop polling', () => {
    model1.stopPolling();
    assert( model1._pollTimer == undefined );
  } );


} );

