
import http from 'node:http';
import sinon from 'sinon';
import { describe, it } from 'mocha';
import { use as chaiUse, expect, assert } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import fs from 'node:fs';
import path from 'node:path';
import { URL } from 'url';
import { WebSocketServer, WebSocket } from 'ws';
import { v4 as uuidv4 } from 'uuid';
const __dirname = new URL('.', import.meta.url).pathname;

import Util from '../../lib/util.js';
import { ChannelSockClient } from '../../lib/util/channelsockclient.js';
import { SimpleIntegratedCollection } from '../../lib/models.js';

const data1 = JSON.parse( fs.readFileSync( path.join( __dirname, 'data1.json' ), { encoding: 'utf8' } ) );

chaiUse( chaiAsPromised );

describe( 'Integrated Collection', () => {
  const app = {};
  let sandbox, model1, server, wsclient, wss, port, webconncount = 0, wsconcount = 0, wssubcount = 0, wsconerr = 0;
  let updFunc, insFunc, delFunc, ids = {};

  const model1Config = {
    'idField': 'someId',
    'reqFields': [ 'name', 'color' ],
    'defaultValues': {},
    'optFields': [ 'email', 'gender', 'ip_address', 'anotherId' ],
    'wsChannel': '/chan1/:foo:',
    'url': {},
    'batchSize': 1
  };

  const handleMsg = (sock, data) => {
    let json = JSON.parse( data );
    if ( json.type == 'subscribe' ) {
      if ( json.channel == '/chan1/bar' ) {
        wssubcount++;
        sock.send( JSON.stringify( {
          'id': json.id,
          'type': 'subscribe',
          'success': true
        } ) );
      }
    }
    if ( json.type == 'unsubscribe' ) {
      if ( json.channel== '/chan1/bar' ) {
        sock.send( JSON.stringify( {
          'id': json.id,
          'type': 'unsubscribe',
          'success': true
        } ) );
      }
    }
  };


  before( () => {
    sandbox = sinon.createSandbox();
    app.log = sandbox.stub();
    app.fetchWebsock = sandbox.stub();
    app.util = Util;
    global.document = { createDocumentFragment: sandbox.stub() };
    global.document.createDocumentFragment.returns( {
      'addEventListener': () => {},
      'dispatchEvent': () => {},
      'removeEventListener': () => {}
    } );
    global.CustomEvent = sandbox.stub();
    global.window = { location: { origin: "http://127.0.0.1/" } };
    global.WebSocket = WebSocket;
  } );

  after( () => {
    if ( wsclient )
      wsclient.terminate();
    delete global.document;
    delete global.CustomEvent;
    delete global.window;
    delete global.WebSocket;

    server.close();
    server = undefined;
  } );

  it( 'instantiate', () => {
    model1 = new SimpleIntegratedCollection( app, model1Config );
    assert( model1 );
    assert( model1.constructor.name == "SimpleIntegratedCollection" );
    expect( model1.config ).to.be.an( 'object' ).to.have.all.keys( 'idField', 'optFields', 'reqFields', 'defaultValues',
      'indexFields', 'url', 'versioned', 'versionField', 'batchSize', 'modelType',
      'reloadDelete', 'reloadInsert', 'reloadUpdate', 'wsChannel', 'cacheAge',
      'reloadQuery', 'reloadContext', 'loadFirstFetch' );
    assert( model1.config.idField == model1Config.idField );
    expect( model1.config.optFields ).to.be.an( 'array' ).to.contain( 'email', 'gender', 'ip_address', 'anotherId' );
    expect( model1.config.reqFields ).to.be.an( 'array' ).to.contain(  'name', 'color' );
    expect( model1.config.defaultValues ).to.be.an( 'object' ).to.have.all.keys( 'someId' );
    assert( model1.config.indexFields == undefined );
    assert( model1.config.loadFirstFetch == false );
    assert( model1.config.modelType.includes( 'integrated' ) );
  } );


  it( 'test server setup', () => {

    const routeFunc = async ( req, res ) => {
      const url = req.url;
      let data, body = '';

      const prom = new Promise( (resolve, reject) => {

        webconncount++;

        req.on( 'end', () => { resolve(); } );

        req.on( 'error', () => { reject() } );

        if ( req.method == "POST" ) {
          req.on( "data", (bit) => { body += bit } )
        }
        else
          resolve();
      } );

      await prom;

      if ( url.indexOf( "/notfound" ) !== -1 ) {
        res.statusCode = "404";
        return res.end( `<html>Not found</html>` );
      }

      if ( url.indexOf( '/fetch1' ) !== -1 ) {
        body = JSON.parse( body );
        res.writeHead( 200, { 'content-type': 'application/json' });
        data = data1.slice( body.start, ( body.count == 0 ) ? data1.length - body.start : body.start + body.count );
        for ( let want of body.include ) {
          if ( ! data.find( r => r.someId == want ) )
            data.push( data1[ want - 1 ] );
        }
        return res.end( JSON.stringify( {
          success: true,
          records: data,
          meta: {
            filterCount: data1.length,
            totalCount: data1.length,
          }
        } ) );
      }

      if ( url.indexOf( "/update" ) !== -1 ) {
        body = JSON.parse( body );
        return updFunc( req, res, body );
      }

      if ( url.indexOf( "/insert" ) !== -1 ) {
        body = JSON.parse( body );
        return insFunc( req, res, body );
      }

      if ( url.indexOf( "/delete" ) !== -1 ) {
        body = JSON.parse( body );
        return delFunc( req, res, body );
      }

      res.statusCode = "500";
      res.writeHead(200, { 'content-type': 'application/json' });
      return res.end( JSON.stringify({"message":"catch all failure","status":-1}) );

    };

    server = http.createServer( routeFunc );
  } );


  it( 'websock test server', () => {

    wss = new WebSocketServer( { noServer: true } );

    server.on( 'upgrade', ( req, sock, head ) => {
      wss.handleUpgrade( req, sock, head, (ws) => {
        ws.on( 'error', () => { wsconerr++ } );
        ws.on( 'message', (data) => handleMsg( ws, data ) );
        wss.emit( 'connection', ws, req );
        wsconcount++;
      } );
    } );
  } );


  it( 'model setup', () => {
    expect( () => { model1.setup() } ).to.not.throw();
    expect( model1._data ).to.be.instanceof( Map );
    expect( model1._ids ).to.be.instanceof( Array );
    assert( model1._fetched == false );
    assert( model1._lastConnect == undefined );
    assert( model1._reconnectTimer == undefined );
    sandbox.spy( model1 );
  } );


  describe( 'Filter checks', () => {
    it( 'check filters fail 1', () => {
      const filters = { ':rules': {} };
      expect( () => { model1._checkFilters( filters ) } ).to.throw( /Unknown rules type found/ );
    } );


    it( 'check filters fail 2', () => {
      const filters = { ':rules': [], ':mode': "MMM" };
      expect( () => { model1._checkFilters( filters ) } ).to.throw( /Unsupported filter mode/ );
    } );


    it( 'check filters fail 3', () => {
      const filters = { ':rules': [ [] ] };
      expect( () => { model1._checkFilters( filters ) } ).to.throw( /found array/ );
    } );


    it( 'check filters fail 4', () => {
      const filters = { ':rules': [ { a: [] } ] };
      expect( () => { model1._checkFilters( filters ) } ).to.throw( /expecting object/ );
    } );


    it( 'check filters fail 5', () => {
      const filters = { ':rules': [ { a: { 'in': [ function(){} ] } } ] };
      expect( () => { model1._checkFilters( filters ) } ).to.throw( /Unsupported criteria value function/ );
    } );


    it( 'check filters fail 5', () => {
      const filters = { ':rules': [ { a: { 'ct': function(){} } } ] };
      expect( () => { model1._checkFilters( filters ) } ).to.throw( /Unsupported criteria value function/ );
    } );


    it( 'check filters success 1', () => {
      const filters = {
        ':rules': [
          { 'someId': {
              ':rules': [
                { 'ge': 900 },
                { 'lt': 5 }
              ],
              ':mode': "OR"
          } },
          { 'gender': {
            'eq': 'female',
            ':case': false,
          } }
        ],
        ':mode': "AND"
      };
      expect( () => { model1._checkFilters( filters ) } ).to.not.throw();
    } );
  } );

  it( 'no connection', async () => {
    sandbox.resetHistory();
    await model1.load();
    assert( model1._tryConnect.notCalled );
    assert( model1._load.calledOnce );
    assert( webconncount == 0 );
    assert( wsconcount == 0 );
    assert( wsconerr == 0 );
  } );



  it( 'connect on load failure', async () => {
    let complete = false, error;
    let tmpclient = new ChannelSockClient( app, 'ws://127.0.0.2:1235' );
    sandbox.resetHistory();
    app.fetchWebsock.returns( tmpclient );
    sandbox.spy( tmpclient );
    model1.config.url.websock = 'ws://127.0.0.2:1234';
    try {
      await model1.load();
      complete = true;
    }
    catch( err ) {
      error = err;
    }
    assert( complete == false );
    assert( error.error instanceof Error );
    expect( error.message ).to.contain( 'ECONNREFUSED' );
    assert( model1._tryConnect.calledOnce );
    tmpclient.terminate();
  } );

  it( "fetch failure without websock", async () => {
    let error, complete = false;

    sandbox.resetHistory();
    model1.config.url.fetch = 'http://127.0.0.2/blah';
    model1.config.url.websock = undefined;

    try {
      await model1.fetch();
      complete = true;
    }
    catch( err ) {
      error = err;
    }

    assert( ! complete );
    assert( error );
    expect( error.message ).to.contain( 'fetch failed' );
    assert( model1._remoteRequest.calledOnce );
    assert( model1._fetched == false );
    assert( model1._inconsistent == true );
    assert( webconncount == 0 );
  } );



  it( 'test server listen', async () => {
    const listening = new Promise( (resolve) => {
      server.on( 'listening', () => { resolve() } );
    } );
    server.listen( 0, '127.0.0.1' );
    await listening;
    port = server.address().port;
    wsclient = new ChannelSockClient( app, 'ws://127.0.0.1:' + port );
    sandbox.spy( wsclient );
    app.fetchWebsock.returns( wsclient );
  } );


  it( "connect on fetch", async () => {
    let error, complete = false;

    model1.clear();

    sandbox.resetHistory();
    model1.config.url.fetch = 'http://127.0.0.1:' + port + '/fetch1';
    model1.config.url.websock = 'ws://127.0.0.1:' + port + '/';

    try {
      await model1.fetch();
      complete = true;
    }
    catch( err ) {
      error = err;
    }

    assert( complete );
    assert( error == undefined );
    assert( model1._fetchRemote.calledOnce );
    assert( model1._tryConnect.calledOnce );
    assert( model1._subscribe.calledOnce );
    assert( model1._remoteRequest.calledOnce );

    assert( model1._ws.readyState == 1 );

    assert( model1._fetched == true );
    assert( model1._inconsistent == false );
    assert( webconncount == 1 );
  } );



  it( 'connect on load', async() => {
    let error, complete = false;
    model1.clear();
    model1._ws = undefined;
    sandbox.resetHistory();
    model1.config.url.websock = 'ws://127.0.0.1:' + port + '/:foo:/';
    try {
      await model1.load( { 'foo': 'bar' } );
      complete = true;
    }
    catch( err ) {
      error = err;
    }

    assert( complete );
    assert( error == undefined );
    assert( model1._tryConnect.calledOnce );
    assert( app.fetchWebsock.firstCall.firstArg == 'ws://127.0.0.1:' + port + '/bar/' );
    assert( wsclient.connect.calledOnce );
    assert( model1._tryConnect.calledOnce );
    assert( webconncount == 1 );
    assert( wssubcount == 0 );
    assert( wsconcount == 1 );
    assert( wsconerr == 0 );
  } );



  it( 'reload does not reload', async() => {
    sandbox.resetHistory();
    await model1.reload();
    assert( model1._load.notCalled );
    assert( model1._tryConnect.notCalled );
    assert( wsclient.connect.notCalled );
    assert( app.fetchWebsock.notCalled );
  } );



  it( 'can subscribe', async () => {
    await model1._subscribe();
    assert( model1._trySubscribe.calledOnce );
    assert( wsclient.subscribe.calledOnce );
    assert( webconncount == 1 );
    assert( wssubcount == 1 );
    assert( wsconcount == 1 );
    assert( wsconerr == 0 );
  } );


  it( 'resubscribe', async() => {

    sandbox.resetHistory();

    assert( model1._lastQuery == undefined );

    try {
      wsclient._sock.close();
      await new Promise( (res ) => { setTimeout( () => res(), 1000 ) } );

    }
    catch( err ) {
      console.log( "ERROR", err );
    }

    assert( model1.handleEvent.callCount == 3 );
    assert( model1.handleEvent.getCall( 0 ).args[ 0 ] == "disconnect" );
    assert( model1.handleEvent.getCall( 1 ).args[ 0 ] == "connect" );
    assert( model1.handleEvent.getCall( 2 ).args[ 0 ] == "resubscribe" );
    assert( wsclient._resubscribe.calledOnce );

  } );


  it( 'cleared model doesn\'t resubscribe', async() => {

    sandbox.resetHistory();

    assert( model1._lastQuery == undefined );

    try {
      model1.clear();
      await new Promise( (res ) => { setTimeout( () => res(), 500 ) } );
      wsclient._sock.close();
      await new Promise( (res ) => { setTimeout( () => res(), 1000 ) } );

    }
    catch( err ) {
      console.log( "ERROR", err );
    }

    assert( model1.handleEvent.callCount == 0 );
    assert( wsclient._resubscribe.notCalled );
    assert( wsclient.unsubscribe.calledOnce );

  } );


  describe( 'CRUD operations', () => {
    it( 'fetch failure 404', async () => {
      let error, complete = false;

      sandbox.resetHistory();
      wssubcount = 0;
      webconncount = 0;
      wsconcount = 0;
      model1.config.url.websock = 'ws://127.0.0.1:' + port + '/';
      model1.config.url.fetch = 'http://127.0.0.1:' + port + '/notfound';

      try {
        await model1.load( { 'foo': 'bar' } );
        await model1.fetch();
        complete = true;
      }
      catch( err ) {
        error = err;
      }

      assert( ! complete );
      assert( error );
      expect( error.message ).to.contain( 'Not Found' );
      assert( model1._fetched == false );
      assert( model1._inconsistent );
      assert( webconncount == 1 );
      assert( wssubcount == 1 );
      assert( wsconcount == 1 );
      assert( wsconerr == 0 );
    } );



    // Test a fetch
    it( 'fetch success', async () => {
      let recs, complete = false;

      sandbox.resetHistory();
      webconncount = 0;
      model1.config.url.fetch = 'http://127.0.0.1:' + port + '/fetch1';

      try {
        recs = await model1.fetch( 0, 10 );
        complete = true;
      }
      catch( err ) {
        console.log( "Error", err );
      }

      assert( complete );
      assert( model1._fetched == true );
      assert( model1._inconsistent == false );
      assert( model1.parse.calledOnce );
      assert( webconncount == 1 );

      assert( recs.length == 10 );
      assert( recs.fetchCount == 10 );
      assert( recs.totalCount == 1000 );
      assert( recs.filterCount == 1000 );

      assert( model1._remoteRequest.calledOnce );
      expect( model1._remoteRequest.firstCall.args[2] ).to.be.an( 'object' );
      expect( model1._remoteRequest.firstCall.args[2] ).to.have.property( 'operation', 'fetch' );
      expect( model1._remoteRequest.firstCall.args[2] ).to.have.property( 'start', 0 );
      expect( model1._remoteRequest.firstCall.args[2] ).to.have.property( 'count', 10 );
    } );



    // Check model state
    it( 'model state', () => {
      assert( model1._modified.length == 0 );
      assert( model1.failed == false );
      assert( model1.inconsistent == false );
      assert( model1.loaded == true );
      assert( model1.isChanged() == false );
    } );


    // Test adding a record
    it( 'add pending', async() => {
      let added, first = Object.assign( {}, data1[0] );
      delete first[ 'someId' ]
      first.color = 'Red';
      first.name = 'Greg';
      first.gender = 'Unknown';
      first.email = 'mme@herre.com';
      first.anotherId = uuidv4();
      added = model1.add( first );

      assert( added[0] < 0 );
      assert( model1._ids.length == 11 );
      assert( model1.isChanged() == true );
      expect( model1.getRecordInfo( added[0] ) )
        .to.be.an( 'object' )
        .to.have.property( 'operation', 'insert' );
      expect( model1.getChanges( added[0] ) )
        .to.be.an( 'object' )
        .to.contain( first );
    } );


    // Test re-fetching with local changes
    it( 'local fetch with local changes', async () => {
      let recs;

      sandbox.resetHistory();
      recs = await model1.fetch( 0, 10 );

      assert( model1._fetched == true );
      assert( model1._inconsistent == false );
      assert( model1._fetchRemote.notCalled );
      assert( model1.parse.notCalled );

      assert( recs.length == 10 );
      assert( recs.fetchCount == 10 );
      assert( recs.totalCount == 1001 );
      assert( recs.filterCount == 1001 );
      assert( recs.find( r => r.email == 'mme@herre.com' ) == null );

      assert( model1._ids.length == 11 );
      assert( model1.isChanged() == true );
    } );


    // Test remote re-fetching with local changes
    it( 'remote fetch with local changes and include record', async () => {
      let recs;

      sandbox.resetHistory();
      webconncount = 0;

      recs = await model1.fetch( 20, 10, undefined, undefined, [ 1 ] );

      assert( model1._fetched == true );
      assert( model1._inconsistent == false );
      assert( webconncount == 1 );

      assert( recs.length == 11 );
      assert( recs.fetchCount == 11 );
      assert( recs.totalCount == 1001 );
      assert( recs.filterCount == 1001 );
      assert( recs.find( r => r.email == 'mme@herre.com' ) == null );

      assert( model1._fetchRemote.calledOnce );
      assert( model1._remoteRequest.calledOnce );
      assert( model1.parse.calledOnce );

      assert( model1._ids.length == 12 );
      assert( model1.isChanged() == true );
    } );



    // Test remote re-fetching with local changes
    it( 'remote big fetch with local changes', async () => {
      let recs;

      sandbox.resetHistory();
      webconncount = 0;

      recs = await model1.fetch( 0, 0 );

      assert( model1._fetched == true );
      assert( model1._inconsistent == false );
      assert( webconncount == 1 );

      assert( recs.length == 1001 );
      assert( recs.fetchCount == 1001 );
      assert( recs.totalCount == 1001 );
      assert( recs.filterCount == 1001 );
      assert( recs.find( r => r.email == 'mme@herre.com' ) );

      assert( model1._fetchRemote.calledOnce );
      assert( model1._remoteRequest.calledOnce );
      assert( model1.parse.calledOnce );

      assert( model1._ids.length == 1001 );
      assert( model1.isChanged() == true );
    } );


    // Clear model
    it( 'clear model', () => {
      sandbox.resetHistory();
      model1.clear();

      assert( model1._ids.length == 0 );
      assert( model1._data.size == 0 );
      assert( model1._fetched == false );
      assert( model1._inconsistent == false );
      assert( model1.loaded == false ); // websock url has placeholder
      assert( model1._modified.length == 0 );
    } );



    // Test adding a record
    it( 'add pending before fetch', () => {
      let added, first = Object.assign( {}, data1[0] );
      delete first[ 'someId' ]
      first.color = 'Red';
      first.name = 'Greg';
      first.gender = 'Unknown';
      first.email = 'mme@herre.com';
      first.anotherId = uuidv4();
      added = model1.add( first );

      assert( added[0] < 0 );
      assert( model1._ids.length == 1 );
      assert( model1.isChanged() == true );
      assert( model1._modified.length == 1 );
      expect( model1.getRecordInfo( added[0] ) )
        .to.be.an( 'object' )
        .to.have.property( 'operation', 'insert' );
      expect( model1.getChanges( added[0] ) )
        .to.be.an( 'object' )
        .to.contain( first );
    } );


    // Test re-fetching with local changes
    it( 'fetch 3 success', async () => {
      let recs, complete = false;

      sandbox.resetHistory();
      webconncount = 0;

      try {
        recs = await model1.fetch( 10, 5 );
        complete = true;
      }
      catch( err ) {
        console.log( "Error", err );
      }

      assert( complete );
      assert( model1._fetched == true );
      assert( model1._inconsistent == false );
      assert( webconncount == 1 );

      assert( recs.length == 5 );
      assert( recs.fetchCount == 5 );
      assert( recs.totalCount == 1001 );
      assert( recs.filterCount == 1001 );
      assert( model1.length == 6 );

      assert( model1.parse.calledOnce );
      assert( model1._remoteRequest.calledOnce );

      assert( model1.isChanged() == true );
    } );


    // Remove the added records
    it( 'remove added', async() => {
      let removed, added;
      for ( let id of model1._ids )
        if ( id < 0 )
          added = id;
      removed = model1.remove( [ added ] );
      assert( added == removed[ 0 ] );
      assert( model1.length == 5 )
      assert( model1._modified.length == 0 );
      assert( model1.isChanged() == false );
    } );


    // Test adding a record
    it( 'add pending', async() => {
      let added, first = Object.assign( {}, data1[0] );
      delete first[ 'someId' ]
      first.color = 'Red';
      first.name = 'Greg';
      first.gender = 'Unknown';
      first.email = 'mme@herre.com';
      first.anotherId = uuidv4();
      added = model1.add( first );

      assert( added[0] < 0 );
      assert( model1._ids.length == 6 );
      assert( model1.isChanged() == true );
      assert( model1._modified.length == 1 );
      expect( model1.getRecordInfo( added[0] ) )
        .to.be.an( 'object' )
        .to.have.property( 'operation', 'insert' );
      expect( model1.getChanges( added[0] ) )
        .to.be.an( 'object' )
        .to.contain( first );
      assert( model1._data.get( added[0] )._operation == 'insert' );
    } );


    // Test updating a record
    it( 'update pending', () => {
      let updated, updid, rec;
      updid = model1._ids[4];
      rec = model1.getRecord( updid );
      rec[ 'name' ] = 'Barry';
      updated = model1.update( rec );
      assert( updated[0] == updid );
      assert( model1._modified.length == 2 );
      assert( model1._data.get( updid )._operation == 'update' );
    } );


    // Test removing a record
    it( 'remove pending', () => {
      let removed, rmid;
      rmid = model1._ids[2];
      removed = model1.remove( rmid );
      expect( removed[ 0 ] == rmid );
      assert( model1._modified.length == 3 );
      assert( model1._data.get( rmid )._operation == 'delete' );
    } );


    // Check persist for insert fails
    it( 'persist insert URL failure', async() => {
      let info, addid;
      sandbox.resetHistory();
      for ( let id of model1._ids ) {
        if ( model1._data.get( id )._operation == 'insert' ) {
          addid = id;
          break;
        }
      }
      info = await model1.persist( [ addid ] );
      assert( info.insert.length == 0 );
      assert( model1.getUrl.calledOnce );
      assert( model1.getUrl.threw() );
      assert( model1._inconsistent == false );
      assert( model1._data.get( addid )._errored == true );
      assert( model1._data.get( addid )._message !== '' );
      assert( model1.isChanged() == true );
    } );


    // Test all persist attempts & failures
    it( 'all persist failures', async() => {
      let result1, result2, result3, complete = false;

      sandbox.resetHistory();

      model1.config.url.delete = 'http://127.0.0.1:' + port + '/delete';
      model1.config.url.update = 'http://127.0.0.1:' + port + '/update';
      model1.config.url.insert = 'http://127.0.0.1:' + port + '/insert';

      delFunc  = (_0,res) => {
        res.writeHead( 200, { 'content-type': 'application/json' });
        return res.end( JSON.stringify( { success: false, message: 'test delete error' } ) );
      }
      updFunc  = (_0,res) => {
        res.writeHead( 200, { 'content-type': 'application/json' });
        return res.end( JSON.stringify( { success: false, message: 'test update error' } ) );
      }
      insFunc = (_0,res) => {
        res.writeHead( 200, { 'content-type': 'application/json' });
        return res.end( JSON.stringify( { success: false, message: 'test insert error' } ) );
      }
      sandbox.spy( delFunc );
      sandbox.spy( updFunc );
      sandbox.spy( insFunc );

      for ( let id of model1._ids ) {
        if ( model1._data.get( id )._operation )
          ids[ model1._data.get( id )._operation ] = id;
      }

      try {
        result1 = await model1.persist( [ ids[ 'delete' ] ] );
        result2 = await model1.persist( [ ids[ 'update' ] ] );
        result3 = await model1.persist( [ ids[ 'insert' ] ] );
        complete = true;
      }
      catch( err ) {
        console.log( "Error", err );
      }

      assert( complete );
      expect( result1 ).to.be.an( 'object' );
      assert( result1.insert.length == 0 );
      assert( result1.update.length == 0 );
      assert( result1.delete.length == 0 );

      expect( result2 ).to.be.an( 'object' );
      assert( result2.insert.length == 0 );
      assert( result2.update.length == 0 );
      assert( result2.delete.length == 0 );

      expect( result3 ).to.be.an( 'object' );
      assert( result3.insert.length == 0 );
      assert( result3.update.length == 0 );
      assert( result3.delete.length == 0 );

      expect( model1.getRecordInfo( ids[ 'delete' ] ) ).to.be.an( 'object' )
        .to.contain( { 'errored': true, 'message': 'test delete error' } );
      expect( model1.getRecordInfo( ids[ 'update' ] ) ).to.be.an( 'object' )
        .to.contain( { 'errored': true, 'message': 'test update error' } );
      expect( model1.getRecordInfo( ids[ 'insert' ] ) ).to.be.an( 'object' )
        .to.contain( { 'errored': true, 'message': 'test insert error' } );

    } );



    // Test persist success
    it( 'all persist success', async() => {
      let result, complete = false;

      sandbox.resetHistory();

      delFunc  = (_0,res) => {
        res.writeHead( 200, { 'content-type': 'application/json' });
        return res.end( JSON.stringify( { success: true } ) );
      }
      updFunc  = (_0,res,data) => {
        res.writeHead( 200, { 'content-type': 'application/json' });
        return res.end( JSON.stringify( { success: true, records: [ data.records[0] ] } ) );
      }
      insFunc = (_0,res,data) => {
        res.writeHead( 200, { 'content-type': 'application/json' });
        return res.end( JSON.stringify( { success: true, records: [ Object.assign( {}, data.records[0], { 'someId': 1001 } ) ] } ) );
      }
      sandbox.spy( delFunc );
      sandbox.spy( updFunc );
      sandbox.spy( insFunc );

      for ( let id of model1._ids ) {
        if ( model1._data.get( id )._operation )
          ids[ model1._data.get( id )._operation ] = id;
      }

      try {
        result = await model1.persist( );
        complete = true;
      }
      catch( err ) {
        console.log( "Error", err );
      }

      assert( complete );
      expect( result ).to.be.an( 'object' );
      assert( result.insert.length == 1 );
      assert( result.update.length == 1 );
      assert( result.delete.length == 1 );

      assert( model1.getRecord( ids[ 'delete' ] ) == undefined );

      assert( model1.getRecord( ids[ 'insert' ] ) == undefined );
      assert( result.insert[0] == 1001 );
      assert( model1.hasRecord( 1001 ) );

      expect( model1.getRecordInfo( ids[ 'update' ] ) ).to.be.an( 'object' )
        .to.contain( { 'errored': false } );
      assert( model1.getRecord( ids[ 'update' ] ).name == "Barry" );

    } );

  } );

  describe( 'Websock notifications', () => {

    it( 'websock connected', async () => {
      let complete = false, error;

      model1.clear();
      sandbox.resetHistory();

      try {
        await model1.load( { 'foo': 'bar' } );
        complete = true;
      }
      catch( err ) {
        error = err;
      }

      assert( complete );
      assert( error == undefined );
      assert( wsclient.readyState == 1 );
      assert( model1._ids.length == 0 );
    } );



    it( 'empty model notify without fetch', async () => {
      let deleted, complete = false, error;

      sandbox.resetHistory();

      assert( model1.config.reloadInsert == false );
      assert( model1.config.reloadUpdate == false );
      assert( model1.config.reloadDelete == false );

      try {
        await model1.handleNotify( 'insert', [ 1 ] );
        await model1.handleNotify( 'update', [ 1 ] );
        deleted = await model1.handleNotify( 'delete', [ 1 ] );
        complete = true;
      }
      catch( err ) {
        error = err;
      }

      assert( complete );
      assert( error == undefined );
      assert( model1.refetch.notCalled );
      assert( deleted.length == 0 );
    } );



    it( 'empty model notify with fetch', async () => {
      let deleted, complete = false, error;

      sandbox.resetHistory();

      model1.config.reloadInsert = true;
      model1.config.reloadUpdate = true;
      model1.config.reloadDelete = true;

      assert( model1.config.reloadInsert );
      assert( model1.config.reloadUpdate );
      assert( model1.config.reloadDelete );

      model1.refetch.restore();
      sandbox.stub( model1, 'refetch' );

      try {
        await model1.handleEvent( 'notify', { 'change': 'insert', 'ids': [ 1 ] } );
        await model1.handleEvent( 'notify', { 'change': 'update', 'ids': [ 1 ] } );
        deleted = await model1.handleEvent( 'notify', { 'change': 'delete', 'ids': [ 1 ] } );
        complete = true;
      }
      catch( err ) {
        error = err;
      }

      assert( complete );
      assert( error == undefined );
      assert( model1.handleNotify.callCount == 3 );
      assert( model1.refetch.callCount == 3 );
      assert( deleted.length == 0 );

      model1.refetch.restore();
      sandbox.spy( model1, "refetch" );
    } );



    it( 'model fetch', async () => {
      let recs, complete = false, error;

      sandbox.resetHistory();
      model1.config.url.fetch = 'http://127.0.0.1:' + port + '/fetch1';

      try {
        recs = await model1.fetch( 0, 10 );
        complete = true;
      }
      catch( err ) {
        error = err;
      }

      assert( complete );
      assert( error == undefined );
      assert( model1._fetched == true );
      assert( model1._inconsistent == false );
      assert( model1.parse.calledOnce );

      assert( recs.length == 10 );
      assert( recs.fetchCount == 10 );
    } );



    it( 'notify updated record', async () => {
      let copy, orig, ids, complete = false, error;

      orig = Util.clone( model1._data.get( model1._ids[ 0 ] ) );
      copy = Util.clone( model1._data.get( model1._ids[ 0 ] ) );
      copy.name = "Bobbbybob";
      sandbox.resetHistory();

      try {
        ids = await model1.handleEvent( 'records', { 'records': [ copy ] } );
        complete = true;
      }
      catch( err ) {
        error = err;
      }

      assert( complete );
      assert( error == undefined );
      assert( ids.length == 1 );
      assert( ids[ 0 ] == orig.someId );
      assert( model1.handleRecords.calledOnce );
      assert( model1._ids.length == 10 );
      assert( model1.getRecord( ids[ 0 ] ).name == copy.name );
    } );



    it( 'notify insert record', async () => {
      let copy, orig, ids, complete = false, error;

      orig = Util.clone( model1._data.get( model1._ids[ 1 ] ) );
      copy = Util.clone( orig );
      copy.name = "Bobbbybob again";
      copy.someId = 2001
      sandbox.resetHistory();

      try {
        ids = await model1.handleEvent( 'records', { 'records': [ copy ] } );
        complete = true;
      }
      catch( err ) {
        error = err;
      }

      assert( complete );
      assert( error == undefined );
      assert( ids.length == 1 );
      assert( ids[ 0 ] == 2001 );
      assert( model1.handleRecords.calledOnce );
      assert( model1._ids.length == 11 );
      assert( model1.getRecord( ids[ 0 ] ).name == copy.name );
    } );





    it( 'send update', () => {

      for ( let client of wss.clients ) {
        client.send( JSON.stringify( {
          'channel': '/chan1/bar',
          'type': 'update',
          'ids': [ 1001 ]
        } ) );
      }



    } );



  } );


} );

