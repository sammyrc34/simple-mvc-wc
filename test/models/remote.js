
import http from 'node:http';
import sinon from 'sinon';
import { describe, it } from 'mocha';
import { use as chaiUse, expect, assert } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import fs from 'node:fs';
import path from 'node:path';
import { URL } from 'url';
import { v4 as uuidv4 } from 'uuid';
const __dirname = new URL('.', import.meta.url).pathname;

import Util from '../../lib/util.js';
import { SimpleRemoteCollection } from '../../lib/models.js';

const data1 = JSON.parse( fs.readFileSync( path.join( __dirname, 'data1.json' ), { encoding: 'utf8' } ) );

chaiUse( chaiAsPromised );

describe( 'Remote Collection', () => {
  const app = {};
  let sandbox, model1, server, port, webconncount = 0;
  let updFunc, insFunc, delFunc, ids = {};

  const model1Config = {
    'idField': 'someId',
    'reqFields': [ 'name', 'color' ],
    'defaultValues': {},
    'optFields': [ 'email', 'gender', 'ip_address', 'anotherId' ],
    'url': {},
    'batchSize': 1
  };


  before( () => {
    sandbox = sinon.createSandbox();
    app.log = sandbox.stub();
    app.util = Util;
    global.document = { createDocumentFragment: sandbox.stub() };
    global.document.createDocumentFragment.returns( {
      'addEventListener': () => {},
      'dispatchEvent': () => {},
      'removeEventListener': () => {}
    } );
    global.CustomEvent = sandbox.stub();
    global.window = { location: { origin: "http://127.0.0.1/" } };
  } );

  after( () => {
    delete global.document;
    delete global.CustomEvent;
    delete global.window;
    delete global.WebSocket;

    if ( server ) {
      server.close();
      server = undefined;
    }
  } );

  it( 'instantiate', () => {
    model1 = new SimpleRemoteCollection( app, model1Config );
    assert( model1 );
    assert( model1.constructor.name == "SimpleRemoteCollection" );
    expect( model1.config ).to.be.an( 'object' ).to.have.all.keys( 'idField', 'optFields', 'reqFields', 'defaultValues',
      'indexFields', 'url', 'versioned', 'versionField', 'batchSize', 'modelType',
      'reloadQuery', 'reloadContext', 'loadFirstFetch' );
    assert( model1.config.idField == model1Config.idField );
    expect( model1.config.optFields ).to.be.an( 'array' ).to.contain( 'email', 'gender', 'ip_address', 'anotherId' );
    expect( model1.config.reqFields ).to.be.an( 'array' ).to.contain(  'name', 'color' );
    expect( model1.config.defaultValues ).to.be.an( 'object' ).to.have.all.keys( 'someId' );
    assert( model1.config.modelType.includes( 'remote' ) );
  } );


  it( 'test server setup', () => {

    const routeFunc = async ( req, res ) => {
      const url = req.url;
      let body = '';

      const prom = new Promise( (resolve, reject) => {

        webconncount++;

        req.on( 'end', () => { resolve(); } );

        req.on( 'error', () => { reject() } );

        if ( req.method == "POST" ) {
          req.on( "data", (bit) => { body += bit } )
        }
        else
          resolve();
      } );

      await prom;

      if ( url.indexOf( "/notfound" ) !== -1 ) {
        res.statusCode = "404";
        return res.end( `<html>Not found</html>` );
      }

      if ( url.indexOf( '/fetch1' ) !== -1 ) {
        res.writeHead( 200, { 'content-type': 'application/json' });
        return res.end( JSON.stringify( {
          success: true,
          records: data1
        } ) );
      }
      if ( url.indexOf( '/fetch2' ) !== -1 ) {
        res.writeHead( 200, { 'content-type': 'application/json' });
        return res.end( JSON.stringify( {
          success: true,
          records: data1.slice( 0, 100 )
        } ) );
      }
      if ( url.indexOf( '/fetch3' ) !== -1 ) {
        res.writeHead( 200, { 'content-type': 'application/json' });
        return res.end( JSON.stringify( {
          success: true,
          records: data1.slice( 0, 20 )
        } ) );
      }

      if ( url.indexOf( "/update" ) !== -1 ) {
        body = JSON.parse( body );
        return updFunc( req, res, body );
      }

      if ( url.indexOf( "/insert" ) !== -1 ) {
        body = JSON.parse( body );
        return insFunc( req, res, body );
      }

      if ( url.indexOf( "/delete" ) !== -1 ) {
        body = JSON.parse( body );
        return delFunc( req, res, body );
      }

      res.statusCode = "500";
      res.writeHead(200, { 'content-type': 'application/json' });
      return res.end( JSON.stringify({message:"catch all failure"}) );

    };

    server = http.createServer( routeFunc );
  } );


  it( 'model setup', () => {
    expect( () => { model1.setup() } ).to.not.throw();
    expect( model1._data ).to.be.instanceof( Map );
    expect( model1._ids ).to.be.instanceof( Array );
    assert( model1._loaded == false );
    sandbox.spy( model1 );
  } );


  it( 'failed load without url', async () => {
    let error, complete = false;
    sandbox.resetHistory();
    try {
      await model1.load();
    }
    catch( err ) {
      error = err;
    }
    assert( complete == false );
    expect( error ).to.be.an( 'error' ).to.match( /No URL def/ );
    assert( model1._load.calledOnce );
    assert( model1._remoteRequest.notCalled );
    assert( model1._loaded == false );
    assert( webconncount == 0 );
  } );



  it( 'failed load missing bind var', async () => {
    let error, complete = false;
    sandbox.resetHistory();
    model1.config.url.load = 'http://127.0.0.2:1234/l:foo:/bar';
    try {
      await model1.load();
    }
    catch( err ) {
      error = err;
    }
    assert( complete == false );
    expect( error ).to.be.an( 'error' ).to.match( /Cannot load without context/ );
    assert( model1._load.notCalled );
    assert( model1._remoteRequest.notCalled );
    assert( model1._loaded == false );
    assert( webconncount == 0 );
  } );



  it( 'failed load cannot connect', async () => {
    let error, complete = false;
    sandbox.resetHistory();
    model1.config.url.load = 'http://127.0.0.2:1234';
    try {
      await model1.load();
    }
    catch( err ) {
      error = err;
    }
    assert( complete == false );
    expect( error ).to.be.an( 'error' ).to.match( /fetch failed/ );
    assert( model1._load.calledOnce );
    assert( model1._remoteRequest.calledOnce );
    assert( model1._loaded == false );
    assert( webconncount == 0 );
  } );



  it( 'test server listen', async () => {
    const listening = new Promise( (resolve) => {
      server.on( 'listening', () => { resolve() } );
    } );
    server.listen( 0, '127.0.0.1' );
    await listening;
    port = server.address().port;
  } );



  it( 'failed load 404', async() => {
    let error, complete = false;
    sandbox.resetHistory();
    model1.config.url.load = 'http://127.0.0.1:' + port + '/:foo:';
    try {
      await model1.load( { 'foo': 'notfound' } );
    }
    catch (err) {
      error = err;
    }
    assert( ! complete );
    expect( error ).to.be.an( 'error' ).to.match( /Not Found/ );
    assert( model1._remoteRequest.calledOnce );
    assert( model1._loaded == false );
    assert( model1._inconsistent );
    assert( model1.parse.notCalled );
    assert( webconncount == 1 );
  } );



  // Test adding records before loading
  it( 'add before fetch', () => {
    let added, modified = Object.assign( {}, data1[0] );

    delete modified[ 'someId' ]
    modified.color = 'Red';
    modified.name = 'Greg';
    modified.gender = 'Unknown';
    modified.email = 'mme@herre.com';
    modified.anotherId = uuidv4();

    sandbox.resetHistory();

    model1.clear();

    added = model1.add( data1[0] );

    assert( added[0] == data1[0].someId );
    assert( model1._ids.length == 1 );
    assert( model1.isChanged() == true );
    expect( model1.getRecordInfo( added[0] ) )
      .to.be.an( 'object' )
      .to.have.property( 'operation', 'insert' );
    expect( model1.getChanges( added[0] ) )
      .to.be.an( 'object' )
      .to.contain( data1[ 0 ] );

    added = model1.add( modified );

    assert( added[0] < 0 );
    assert( model1._ids.length == 2 );
    assert( model1.isChanged() == true );
    expect( model1.getRecordInfo( added[0] ) )
      .to.be.an( 'object' )
      .to.have.property( 'operation', 'insert' );
    expect( model1.getChanges( added[0] ) )
      .to.be.an( 'object' )
      .to.contain( modified );
  } );



  it( 'load', async() => {
    let result;
    sandbox.resetHistory();
    model1.config.url.load = 'http://127.0.0.1:' + port + '/:foo:';
    result = await model1.load( { 'foo': 'fetch1' } );
    assert( model1._remoteRequest.calledOnce );
    assert( model1._loaded );
    assert( model1._inconsistent == false );
    assert( model1.parse.calledOnce );
    assert( result.load.length == 1000 );
    assert( model1._ids.length == 1002 );
    assert( model1._ids.filter( i=>i<0 ).length == 2 );
    assert( webconncount == 2 );
  } );


  it( 'rollback changes', async () => {
    model1.rollback();

    assert( model1._modified.length == 0 );
    assert( model1._ids.length = 1000 );
  } );



  it( 'reload', async() => {
    sandbox.resetHistory();
    await model1.reload();
    assert( model1._load.calledOnce );
    assert( model1._loaded );
    assert( model1._inconsistent == false );
    assert( model1.parse.calledOnce );
    assert( webconncount == 3 );
  } );



  // Test a fetch
  it( 'fetch success', async () => {
    let recs;
    sandbox.resetHistory();
    recs = await model1.fetch( 0, 10 );

    assert( recs.length == 10 );
    assert( recs.fetchCount == 10 );
    assert( recs.totalCount == 1000 );
    assert( recs.filterCount == 1000 );
  } );



  // Check model state
  it( 'model state', () => {
    assert( model1._modified.length == 0 );
    assert( model1.failed == false );
    assert( model1.inconsistent == false );
    assert( model1.loaded == true );
    assert( model1.isChanged() == false );
  } );



  // Test adding a record
  it( 'add pending', () => {
    let added, first = Object.assign( {}, data1[0] );
    sandbox.resetHistory();
    delete first[ 'someId' ]
    first.color = 'Red';
    first.name = 'Greg';
    first.gender = 'Unknown';
    first.email = 'mme@herre.com';
    first.anotherId = uuidv4();

    added = model1.add( first );

    assert( added[0] < 0 );
    assert( model1._ids.length == 1001 );
    assert( model1.isChanged() == true );
    expect( model1.getRecordInfo( added[0] ) )
      .to.be.an( 'object' )
      .to.have.property( 'operation', 'insert' );
    expect( model1.getChanges( added[0] ) )
      .to.be.an( 'object' )
      .to.contain( first );
  } );


  // Test re-fetching with local changes
  it( 'fetch success', async () => {
    let recs, complete = false;

    sandbox.resetHistory();

    try {
      recs = await model1.fetch( 0, 10, [ { field: 'someId', type: "number" } ] );
      complete = true;
    }
    catch( err ) {
      console.log( "Error", err );
    }

    assert( complete );

    assert( model1.isChanged() == true );
    assert( recs.length == 10 );
    assert( recs.fetchCount == 10 );
    assert( recs.totalCount == 1001 );
    assert( recs.filterCount == 1001 );
    assert( recs[ 0 ].someId < 0 );
  } );


  // Clear model
  it( 'clear model', () => {
    model1.clear();
    assert( model1._ids.length == 0 );
    assert( model1._data.size == 0 );
    assert( model1._inconsistent == false );
    assert( model1._loaded == false );
    assert( Object.keys( model1._context ).length == 0 );
    assert( model1._modified.length == 0 );
  } );



  // Test adding a record before loading
  it( 'add pending before load', () => {
    let added, first = Object.assign( {}, data1[0] );
    delete first[ 'someId' ]
    first.color = 'Red';
    first.name = 'Greg';
    first.gender = 'Unknown';
    first.email = 'mme@herre.com';
    first.anotherId = uuidv4();
    added = model1.add( first );

    assert( added[0] < 0 );
    assert( model1._ids.length == 1 );
    assert( model1.isChanged() == true );
    assert( model1._modified.length == 1 );
    expect( model1.getRecordInfo( added[0] ) )
      .to.be.an( 'object' )
      .to.have.property( 'operation', 'insert' );
    expect( model1.getChanges( added[0] ) )
      .to.be.an( 'object' )
      .to.contain( first );
  } );



  // Load
  it( 'load', async() => {
    sandbox.resetHistory();
    model1.config.url.load = 'http://127.0.0.1:' + port + '/:foo:';
    await model1.load( { 'foo': 'fetch1' } );
    assert( model1._remoteRequest.calledOnce );
    assert( model1._loaded );
    assert( model1._inconsistent == false );
    assert( model1.parse.calledOnce );
    assert( model1._ids.length == 1001 );
    assert( model1.isChanged() == true );
    assert( model1._modified.length == 1 );
    assert( webconncount == 4 );
  } );


  it( 'reload after load', async() => {
    let result;
    sandbox.resetHistory();
    assert( model1._clearonload == false );
    result = await model1.reload();
    assert( model1._clearonload == false );
    assert( model1._load.calledOnce );
    assert( model1._loaded );
    assert( model1._inconsistent == false );
    assert( model1.parse.calledOnce );
    assert( model1._ids.length == 1001 );
    assert( result.load.length == 0 );
    assert( result.update.length == 0 );
    assert( result.delete.length == 0 );
    assert( webconncount == 5 );
  } );



  it( 'reload partial', async() => {
    let result, oldurl;
    sandbox.resetHistory();
    oldurl = model1.config.url.load;
    model1.config.url.load = 'http://127.0.0.1:' + port + '/fetch2';
    assert( model1._clearonload == false );
    result = await model1.reload();
    assert( model1._clearonload == false );
    model1.config.url.load = oldurl;
    assert( model1._load.calledOnce );
    assert( model1._loaded );
    assert( model1._inconsistent == false );
    assert( model1.parse.calledOnce );
    assert( model1._ids.length == 101 );
    assert( result.load.length == 0 );
    assert( result.update.length == 0 );
    assert( result.delete.length == 900 );
    assert( webconncount == 6 );
  } );



  // Remove non-existant record
  it( 'remove failure', () => {
    let error, complete = false;

    try {
      model1.remove( [ 123456 ] );
      complete = true;
    }
    catch( err ) {
      error = err;
    }

    assert( ! complete );
    expect( error ).to.be.an( 'error' ).to.match( /Unable to remove record/ );
  } );


  // Remove the added records
  it( 'remove added', () => {
    let removed, added;
    for ( let id of model1._ids )
      if ( id < 0 )
        added = id;
    removed = model1.remove( [ added ] );
    assert( added == removed[ 0 ] );
    assert( model1.length == 100 )
    assert( model1._modified.length == 0 );
    assert( model1.isChanged() == false );
  } );



  // Test adding a record
  it( 'add pending', () => {
    let added, first = Object.assign( {}, data1[0] );
    delete first[ 'someId' ]
    first.color = 'Red';
    first.name = 'Greg';
    first.gender = 'Unknown';
    first.email = 'mme@herre.com';
    first.anotherId = uuidv4();
    added = model1.add( first );

    assert( added[0] < 0 );
    assert( model1._ids.length == 101 );
    assert( model1.isChanged() == true );
    assert( model1._modified.length == 1 );
    expect( model1.getRecordInfo( added[0] ) )
      .to.be.an( 'object' )
      .to.have.property( 'operation', 'insert' );
    expect( model1.getChanges( added[0] ) )
      .to.be.an( 'object' )
      .to.contain( first );
    assert( model1._data.get( added[0] )._operation == 'insert' );
  } );


  // Test updating a record
  it( 'update pending', () => {
    let updated, updid, rec;
    updid = model1._ids[4];
    rec = model1.getRecord( updid );
    rec[ 'name' ] = 'Barry';
    updated = model1.update( rec );
    assert( updated[0] == updid );
    assert( model1._modified.length == 2 );
    assert( model1._data.get( updid )._operation == 'update' );
  } );


  // Test removing a record
  it( 'remove pending', () => {
    let removed, rmid;
    rmid = model1._ids[5];
    removed = model1.remove( rmid );
    expect( removed[ 0 ] == rmid );
    assert( model1._modified.length == 3 );
    assert( model1._data.get( rmid )._operation == 'delete' );
  } );


  // Check persist for insert fails
  it( 'persist insert URL failure', async() => {
    let info, addid;
    sandbox.resetHistory();
    for ( let id of model1._ids ) {
      if ( model1._data.get( id )._operation == 'insert' ) {
        addid = id;
        break;
      }
    }
    info = await model1.persist( [ addid ] );
    assert( info.insert.length == 0 );
    assert( model1.getUrl.calledOnce );
    assert( model1.getUrl.threw() );
    assert( model1._inconsistent == false );
    assert( model1._data.get( addid )._errored == true );
    assert( model1._data.get( addid )._message !== '' );
    assert( model1.isChanged() == true );
  } );


  // Test all persist attempts & failures
  it( 'all persist failures', async() => {
    let result1, result2, result3, complete = false;

    sandbox.resetHistory();

    model1.config.url.delete = 'http://127.0.0.1:' + port + '/delete';
    model1.config.url.update = 'http://127.0.0.1:' + port + '/update';
    model1.config.url.insert = 'http://127.0.0.1:' + port + '/insert';

    delFunc  = (_0,res) => {
      res.writeHead( 200, { 'content-type': 'application/json' });
      return res.end( JSON.stringify( { success: false, message: 'test delete error' } ) );
    }
    updFunc  = (_0,res) => {
      res.writeHead( 200, { 'content-type': 'application/json' });
      return res.end( JSON.stringify( { success: false, message: 'test update error' } ) );
    }
    insFunc = (_0,res) => {
      res.writeHead( 200, { 'content-type': 'application/json' });
      return res.end( JSON.stringify( { success: false, message: 'test insert error' } ) );
    }
    sandbox.spy( delFunc );
    sandbox.spy( updFunc );
    sandbox.spy( insFunc );

    for ( let id of model1._ids ) {
      if ( model1._data.get( id )._operation )
        ids[ model1._data.get( id )._operation ] = id;
    }

    try {
      result1 = await model1.persist( [ ids[ 'delete' ] ] );
      result2 = await model1.persist( [ ids[ 'update' ] ] );
      result3 = await model1.persist( [ ids[ 'insert' ] ] );
      complete = true;
    }
    catch( err ) {
      console.log( "Error", err );
    }

    assert( complete );
    expect( result1 ).to.be.an( 'object' );
    assert( result1.insert.length == 0 );
    assert( result1.update.length == 0 );
    assert( result1.delete.length == 0 );

    expect( result2 ).to.be.an( 'object' );
    assert( result2.insert.length == 0 );
    assert( result2.update.length == 0 );
    assert( result2.delete.length == 0 );

    expect( result3 ).to.be.an( 'object' );
    assert( result3.insert.length == 0 );
    assert( result3.update.length == 0 );
    assert( result3.delete.length == 0 );

    expect( model1.getRecordInfo( ids[ 'delete' ] ) ).to.be.an( 'object' )
      .to.contain( { 'errored': true, 'message': 'test delete error' } );
    expect( model1.getRecordInfo( ids[ 'update' ] ) ).to.be.an( 'object' )
      .to.contain( { 'errored': true, 'message': 'test update error' } );
    expect( model1.getRecordInfo( ids[ 'insert' ] ) ).to.be.an( 'object' )
      .to.contain( { 'errored': true, 'message': 'test insert error' } );

  } );



  // Test persist success
  it( 'all persist success', async() => {
    let result, complete = false;

    sandbox.resetHistory();

    delFunc  = (_0,res) => {
      res.writeHead( 200, { 'content-type': 'application/json' });
      return res.end( JSON.stringify( { success: true } ) );
    }
    updFunc  = (_0,res,data) => {
      res.writeHead( 200, { 'content-type': 'application/json' });
      return res.end( JSON.stringify( { success: true, records: [ data.records[0] ] } ) );
    }
    insFunc = (_0,res,data) => {
      res.writeHead( 200, { 'content-type': 'application/json' });
      return res.end( JSON.stringify( { success: true, records: [ Object.assign( {}, data.records[0], { 'someId': 1001 } ) ] } ) );
    }
    sandbox.spy( delFunc );
    sandbox.spy( updFunc );
    sandbox.spy( insFunc );

    for ( let id of model1._ids ) {
      if ( model1._data.get( id )._operation )
        ids[ model1._data.get( id )._operation ] = id;
    }

    try {
      result = await model1.persist( );
      complete = true;
    }
    catch( err ) {
      console.log( "Error", err );
    }

    assert( complete );
    expect( result ).to.be.an( 'object' );
    assert( result.insert.length == 1 );
    assert( result.update.length == 1 );
    assert( result.delete.length == 1 );

    assert( model1.getRecord( ids[ 'delete' ] ) == undefined );

    assert( model1.getRecord( ids[ 'insert' ] ) == undefined );
    assert( result.insert[0] == 1001 );
    assert( model1.hasRecord( 1001 ) );

    expect( model1.getRecordInfo( ids[ 'update' ] ) ).to.be.an( 'object' )
      .to.contain( { 'errored': false } );
    assert( model1.getRecord( ids[ 'update' ] ).name == "Barry" );

  } );


  it( 'model clear', () => {
    webconncount = 0;
    model1.clear();
    assert( model1._ids.length == 0 );
    assert( model1._data.size == 0 );
    assert( model1._inconsistent == false );
    assert( model1._loaded == false );
    assert( Object.keys( model1._context ).length == 0 );
    assert( model1._modified.length == 0 );
  } );


  it( 'load', async() => {
    let result;
    sandbox.resetHistory();
    model1.config.url.load = 'http://127.0.0.1:' + port + '/:foo:';
    result = await model1.load( { 'foo': 'fetch3' } );
    assert( model1._remoteRequest.calledOnce );
    assert( model1._loaded );
    assert( model1._inconsistent == false );
    assert( model1.parse.calledOnce );
    assert( result.load.length == 20 );
    assert( model1._ids.length == 20 );
    assert( webconncount == 1 );
  } );


} );

