

import sinon from 'sinon';
import { describe, it } from 'mocha';
import { expect, assert } from 'chai';


import Util from '../../lib/util.js';

import Database from 'better-sqlite3';

import QueryTranslate from '../../lib/util/querytranslate.js';


const rows = [
  { 'name': 'test1', 'a': 'abcdefg', 'b': 'endy', 'c': 111, 'd': 0 },
  { 'name': 'test2', 'a': 'efghijk', 'b': 'startyddddddddddd', 'c': 222, 'd': 1 },
  { 'name': 'test3', 'a': 'popopop', 'b': 'aaastartyddd', 'c': 123, 'd': 0 },
  { 'name': 'test4', 'a': 'uuuuuuu', 'b': 'aaaaaaendy', 'c': 333, 'd': 1 },
  { 'name': 'test5', 'a': 'ppppppp', 'b': 'aaaaaaendydd', 'c': 456, 'd': 0 },
  { 'name': 'test6', 'a': 'abcdxyz', 'b': 'starty3333', 'c': 456, 'd': 1 },
];


const rules1 = {
  ':rules': [
    { ':rules': [
      { 'field_a': { 'ct': 'abc' } },
      { 'field_b': {
        ':rules': [
          { 'sw': 'starty' },
          { 'ew': 'endy' }
        ],
        ':mode': "OR"
      } }
    ],
      ':mode': "AND"
    },
    {
      'field_c': { 'in': [ 123, 456 ] }
    },
  ],
  ':mode': "OR"
};


const rules2 = {
  ':rules': [
    { 'field_d': { 'in': [ 2, 1 ] } }
  ]
};


const rules3 = {
  ':rules': [
    { 'c': { 'gt': 400 } },
    { 'a': { 'eq': 'uuuuuuu' } }
  ],
  ':mode': "OR"
};


const rules4 = {
  ':rules': [
    { '(c)': { 'gt': 400 } },
    { '123aa': { 'eq': 'a OR 1=1' } }
  ],
  ':mode': "OR"
};

const rules5 = {
  ':rules': [
    { 'field_c': { 'eq' : 333, ':case': false } }
  ],
  ':mode': "AND"
}


const sorts1 = [
  { field: 'a' }
];

const sorts2 = [
  { field: 'field_c' }
];



describe( 'Query translation utility', () => {
  let db, sandbox, translator;

  before( () => {
    sandbox = sinon.createSandbox();
  } );

  after( () => {
  } );


  it( 'setup test database', () => {
    let ins;

    db = new Database( ':memory:' );

    db.prepare( `
      CREATE TABLE test1(
        name TEXT,
        field_a TEXT,
        field_b TEXT,
        field_c NUMBER,
        field_d BOOLEAN
    )` ).run();

    ins = db.prepare( `INSERT INTO test1( name, field_a, field_b, field_c, field_d ) VALUES( @name, @a, @b, @c, @d )` );

    for ( let row of rows ) {
      ins.run( row );
    }

  } );


  it( 'failed instantiate', () => {
    const filters = {};
    const sorts = [];
    const language = "";

    expect( () => { translator = new QueryTranslate( language, filters, sorts ); } ).to.throw();
  } );


  it( 'rules 1', () => {
    const filters = rules1;
    const sorts = [];
    const language = "sqlite";
    let rows, binds, stmt, out;

    expect( () => { translator = new QueryTranslate( language, filters, sorts ); } ).to.not.throw();
    expect( () => { out = translator.getFilter() } ).to.not.throw();
    expect( () => { binds = translator.getBinds() } ).to.not.throw();

    stmt = `select * from test1 where ${out}`
    rows = db.prepare( stmt ).all( binds );

    assert( rows.length == 4 );
    expect( rows.map( r => r.name ) ).to.be.an( 'array' ).to.include.members( [ 'test1', 'test3', 'test5', 'test6' ] );
  } );


  it( 'rules 2', () => {
    const filters = Util.clone( rules1 );
    const sorts = [];
    const colmap = {};
    const language = "sqlite";
    let rows, binds, stmt, out;

    filters[ ':mode' ] = "AND";

    expect( () => { translator = new QueryTranslate( language, filters, sorts, colmap ); } ).to.not.throw();
    expect( () => { out = translator.getFilter() } ).to.not.throw();
    expect( () => { binds = translator.getBinds() } ).to.not.throw();

    stmt = `select * from test1 where ${out}`
    rows = db.prepare( stmt ).all( binds );

    assert( rows.length == 1 );
    expect( rows.map( r => r.name ) ).to.be.an( 'array' ).to.include.members( [ 'test6' ] );
  } );


  it( 'rules 3', () => {
    const filters = {};
    const sorts = [];
    const colmap = {};
    const language = "sqlite";
    let rows, binds, stmt, out;

    expect( () => { translator = new QueryTranslate( language, filters, sorts, colmap ); } ).to.not.throw();
    expect( () => { out = translator.getFilter() } ).to.not.throw();
    expect( () => { binds = translator.getBinds() } ).to.not.throw();

    stmt = `select * from test1 where ${out}`
    rows = db.prepare( stmt ).all( binds );

    assert( rows.length == 6 );
  } );


  it( 'rules 4', () => {
    const filters = Util.clone( rules2 );
    const sorts = [];
    const colmap = {};
    const language = "sqlite";
    let rows, binds, stmt, out;

    expect( () => { translator = new QueryTranslate( language, filters, sorts, colmap ); } ).to.not.throw();
    expect( () => { out = translator.getFilter() } ).to.not.throw();
    expect( () => { binds = translator.getBinds() } ).to.not.throw();

    stmt = `select * from test1 where ${out}`
    rows = db.prepare( stmt ).all( binds );

    assert( rows.length == 3 );
    expect( rows.map( r => r.name ) ).to.be.an( 'array' ).to.include.members( [ 'test2', 'test4', 'test6' ] );
  } );


  it( 'rules 5', () => {
    const filters = Util.clone( rules2 );
    const sorts = [];
    const colmap = {};
    const language = "sqlite";
    let rows, binds, stmt, out;

    filters[ ':rules' ][ 0 ][ 'field_d' ][ 'in' ] = [ 0, 1 ];

    expect( () => { translator = new QueryTranslate( language, filters, sorts, colmap ); } ).to.not.throw();
    expect( () => { out = translator.getFilter() } ).to.not.throw();
    expect( () => { binds = translator.getBinds() } ).to.not.throw();

    stmt = `select * from test1 where ${out}`
    rows = db.prepare( stmt ).all( binds );

    assert( rows.length == 6 );
  } );


  it( 'rules 6', () => {
    const filters = Util.clone( rules3 );
    const sorts = [];
    const colmap = {};
    const language = "sqlite";
    let binds, stmt, out;

    expect( () => { translator = new QueryTranslate( language, filters, sorts, colmap ); } ).to.not.throw();
    expect( () => { out = translator.getFilter() } ).to.not.throw();
    expect( () => { binds = translator.getBinds() } ).to.not.throw();

    stmt = `select * from test1 where ${out}`
    expect( () => { rows = db.prepare( stmt ).all( binds ) } ).to.throw();
  } );



  it( 'rules 7', () => {
    const filters = Util.clone( rules3 );
    const sorts = [];
    const colmap = { 'a': 'field_a', 'c': 'field_c' };
    const language = "sqlite";
    let rows, binds, stmt, out;

    expect( () => { translator = new QueryTranslate( language, filters, sorts ); } ).to.not.throw();
    expect( () => { out = translator.getFilter( colmap ) } ).to.not.throw();
    expect( () => { binds = translator.getBinds( colmap ) } ).to.not.throw();

    stmt = `select * from test1 where ${out}`
    rows = db.prepare( stmt ).all( binds );

    assert( rows.length == 3 );
    expect( rows.map( r => r.name ) ).to.be.an( 'array' ).to.include.members( [ 'test4', 'test5', 'test6' ] );
  } );


  it( 'rules 8', () => {
    const filters = Util.clone( rules4 );
    const sorts = [];
    const colmap = { 'a': 'field_a', 'c': 'field_c' };
    const language = "sqlite";

    expect( () => { translator = new QueryTranslate( language, filters, sorts, colmap ); } ).to.not.throw();
    expect( () => { translator.getFilter() } ).to.throw( /Unsupported non-alphanumeric/ );
  } );


  it( 'rules 9', () => {
    const filters = {};
    const sorts = Util.clone( sorts1 );
    const colmap = { 'a': 'field_a', 'c': 'field_c' };
    const language = "sqlite";
    let rows, binds, stmt, out, orderby;

    expect( () => { translator = new QueryTranslate( language, filters, sorts ); } ).to.not.throw();
    expect( () => { out = translator.getFilter( colmap ) } ).to.not.throw();
    expect( () => { binds = translator.getBinds() } ).to.not.throw();
    expect( () => { orderby = translator.getSort( colmap ) } ).to.not.throw();

    stmt = `select * from test1 where ${out} ${orderby}`
    rows = db.prepare( stmt ).all( binds );

    assert( rows.length == 6 );
    assert( rows[ 0 ].name == "test1" );
    assert( rows[ 1 ].name == "test6" );
    assert( rows[ 5 ].name == "test4" );
  } );


  it( 'rules 10', () => {
    const filters = rules1;
    const sorts = Util.clone( sorts2 );
    const colmap = {};
    const language = "sqlite";
    let rows, binds, stmt, out, orderby;

    expect( () => { translator = new QueryTranslate( language, filters, sorts, colmap ); } ).to.not.throw();
    expect( () => { out = translator.getFilter() } ).to.not.throw();
    expect( () => { binds = translator.getBinds() } ).to.not.throw();
    expect( () => { orderby = translator.getSort() } ).to.not.throw();

    stmt = `select * from test1 where ${out} ${orderby}`
    rows = db.prepare( stmt ).all( binds );

    assert( rows.length == 4 );
    expect( rows.map( r => r.name ) ).to.be.an( 'array' ).to.include.members( [ 'test1', 'test3', 'test5', 'test6' ] );
    assert( rows[ 0 ].name == "test1" );
    assert( rows[ 3 ].name == "test5" || rows[ 3 ].name == "test6" );
  } );


  it( 'rules 11', () => {
    const filters = rules1;
    const sorts = Util.clone( sorts2 );
    const colmap = {};
    const language = "sqlite";
    let rows, binds, stmt, out, orderby;

    sorts[ 0 ].direction = "desc";

    expect( () => { translator = new QueryTranslate( language, filters, sorts, colmap ); } ).to.not.throw();
    expect( () => { out = translator.getFilter( undefined, 'test1') } ).to.not.throw();
    expect( () => { binds = translator.getBinds() } ).to.not.throw();
    expect( () => { orderby = translator.getSort( undefined, 'test1' ) } ).to.not.throw();

    stmt = `select * from test1 where ${out} ${orderby}`
    rows = db.prepare( stmt ).all( binds );

    assert( rows.length == 4 );
    assert( orderby.indexOf( 'test1\.' ) !== null );
    assert( out.indexOf( 'test1\.' ) !== null );
    expect( rows.map( r => r.name ) ).to.be.an( 'array' ).to.include.members( [ 'test1', 'test3', 'test5', 'test6' ] );
    assert( rows[ 0 ].name == "test5" || rows[ 0 ].name == "test6" );
    assert( rows[ 3 ].name == "test1" );
  } );


  it( 'rules 12', () => {
    const filters = rules5;
    const sorts = [];
    const colmap = {};
    const language = "sqlite";
    let rows, binds, stmt, out, orderby;

    expect( () => { translator = new QueryTranslate( language, filters, sorts, colmap ); } ).to.not.throw();
    expect( () => { out = translator.getFilter( undefined, 'test1') } ).to.not.throw();
    expect( () => { binds = translator.getBinds() } ).to.not.throw();
    expect( () => { orderby = translator.getSort( undefined, 'test1' ) } ).to.not.throw();

    stmt = `select * from test1 where ${out} ${orderby}`
    rows = db.prepare( stmt ).all( binds );

    assert( rows.length == 1 );
    assert( orderby.indexOf( 'test1\.' ) !== null );
    assert( out.indexOf( 'test1\.' ) !== null );
    expect( rows.map( r => r.name ) ).to.be.an( 'array' ).to.include.members( [ 'test4' ] );
  } );





} );

