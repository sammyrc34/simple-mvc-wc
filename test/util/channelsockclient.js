

import sinon from 'sinon';
import http from 'node:http';
import { describe, it } from 'mocha';
import { expect, assert } from 'chai';
import { v4 as uuidv4 } from 'uuid';

import { WebSocketServer, WebSocket } from 'ws';
import { ChannelSockClient } from '../../lib/util/channelsockclient.js';

class FakeModel {
  constructor() {
    this.uuid = uuidv4();
  }
  handleEvent( name, detail ) {}
}

describe( 'Channel Websocket Client Utility', () => {
  let fakeapp, deets, server, chansock,
    model1, model2, model3,
    client1, client2, client3,
    sandbox, port, conncount = 0;

  const handleMessage = ( sock, data ) => {
    const msg = JSON.parse( data );
    deets = {};

    deets.type = msg.type;
    if ( msg.type == "subscribe" ) {
      deets.channel = msg.channel;
      if ( deets.channel == "/test1" )
        sock.send( JSON.stringify( { type: "subscribe", success: true, id: msg.id } ) );
    }
  };


  before( () => {
    sandbox = sinon.createSandbox();

    fakeapp = {
      log: sandbox.stub()
    };

    global.WebSocket = WebSocket;
  } );


  after( async () => {
    delete global.WebSocket;

    if ( client1 )
      client1.terminate();
    if ( client2 )
      client2.terminate();
    if ( client3 )
      client3.terminate();

    await server.close();
    server = undefined;
  } );


  it( 'test server setup', () => {
    server = http.createServer();
    expect( server ).to.be.instanceof( http.Server );
  } );


  it( 'client 1 create', () => {
    model1 = new FakeModel();
    model3 = new FakeModel();
    client1 = new ChannelSockClient( fakeapp, `ws://127.0.0.1:9999/` );

    assert( model1 instanceof FakeModel );
    assert( model3 instanceof FakeModel );
    assert( client1 instanceof ChannelSockClient );
    expect( client1 ).to.be.an( 'object' )
      .to.include.keys( 'app', '_endpoint', '_sock', '_connProm', '_models', '_subs', '_subProms', '_unsubProms', '_termProm', '_connTimer', '_connDelay', '_failCount' );
    assert( client1._sock == undefined );

    sandbox.spy( client1 );
    sandbox.spy( model1 );
    sandbox.spy( model3 );
  } );


  it( 'client 1 connect failure', async () => {
    let error, connected = false;

    try {
      await client1.connect();
      connected = true;
    }
    catch( err ) {
      error = err;
    }

    assert( client1._connTimer );
    clearTimeout( client1._connTimer )
    assert( ! connected );
    assert( error );
  } );


  it( 'test server listen', async () => {
    const listening = new Promise( (resolve) => {
      server.on( 'listening', () => { resolve() } );
    } );

    chansock = new WebSocketServer( { noServer: true } );

    server.on('upgrade', (req, socket, head) => {
      conncount++;
      chansock.handleUpgrade( req, socket, head, (ws) => {
        ws.on( 'message', (msg) => handleMessage( ws, msg ) );
        chansock.emit( 'connection', ws, req )
      } );
    } );

    server.listen( 0, '127.0.0.1' );

    await listening;

    port = server.address().port;
    client1._endpoint = `ws://127.0.0.1:${port}/`
  } );



  it( 'client 1 connect', async () => {
    let tmpws;
    await client1.connect();

    assert( conncount == 1 );
    assert( client1._sock instanceof global.WebSocket );
    assert( client1.readyState == 1 );

    tmpws = client1._sock;
    await client1.connect();
    assert( client1._sock == tmpws );

  } );



  it( 'client 1 model subscribe', async () => {

    sandbox.resetHistory();

    await client1.subscribe( '/test1', model1 );
    await client1.subscribe( '/test1', model3 );

    assert( client1.readyState == 1 );
    assert( client1._subs.size == 1 );
    assert( client1._subProms.size == 0 );
    assert( client1._handleSubscribe.callCount == 1 );
    assert( client1._models.get( model1 ) == "/test1" );
    assert( client1._models.get( model3 ) == "/test1" );
  } );


  it( 'client 1 auto reconnect and resubscribe', async () => {
    let error, complete = false;

    sandbox.resetHistory();

    try {
      client1._sock.terminate();
      await new Promise( (res) => setTimeout( () => res(), 800 ) );
      complete = true;
    }
    catch( err ) {
      error = err;
    }

    assert( complete );
    assert( ! error );

    assert( client1._sock instanceof global.WebSocket );
    assert( client1.readyState == 1 );
    assert( ! client1._terminating );

    assert( model1.handleEvent.callCount == 3 );
    assert( model1.handleEvent.firstCall.args[0] == 'disconnect' );
    assert( model1.handleEvent.secondCall.args[0] == 'connect' );
    assert( model1.handleEvent.thirdCall.args[0] == 'resubscribe' );

    assert( model3.handleEvent.callCount == 3 );
    assert( model3.handleEvent.firstCall.args[0] == 'disconnect' );
    assert( model3.handleEvent.secondCall.args[0] == 'connect' );
    assert( model3.handleEvent.thirdCall.args[0] == 'resubscribe' );
  } );


  it( 'client 1 terminate socket', async () => {
    let error, complete = false;

    sandbox.resetHistory();

    try {
      await client1.terminate();
      await new Promise( (res) => setTimeout( () => res(), 100 ) );
      complete = true;
    }
    catch( err ) {
      error = err;
    }

    assert( complete );
    assert( ! error );

    assert( client1._sock == undefined );
    assert( client1.readyState == 3 );
    assert( client1._terminating );

    assert( model1.handleEvent.callCount == 1 );
    assert( model1.handleEvent.firstCall.args[0] == 'disconnect' );

    assert( model3.handleEvent.callCount == 1 );
    assert( model3.handleEvent.firstCall.args[0] == 'disconnect' );

  } );


  it( 'client 1 reconnect and resubscribe', async () => {
    let error, complete = false;

    sandbox.resetHistory();

    try {
      await client1.connect();
      await new Promise( (res) => setTimeout( () => res(), 100 ) );
      complete = true;
    }
    catch( err ) {
      error = err;
    }

    assert( complete );
    assert( ! error );

    assert( client1._sock instanceof global.WebSocket );
    assert( client1.readyState == 1 );

    assert( model1.handleEvent.callCount == 2 );
    assert( model1.handleEvent.firstCall.args[0] == 'connect' );
    assert( model1.handleEvent.secondCall.args[0] == 'resubscribe' );

    assert( model3.handleEvent.callCount == 2 );
    assert( model3.handleEvent.firstCall.args[0] == 'connect' );
    assert( model3.handleEvent.secondCall.args[0] == 'resubscribe' );
  } );




  it( 'client 2 create', () => {
    model2 = new FakeModel();
    client2 = new ChannelSockClient( fakeapp, `ws://127.0.0.2:${port}` );

    assert( model2 instanceof FakeModel );
    assert( client2 instanceof ChannelSockClient );
    expect( client2 ).to.be.an( 'object' )
      .to.include.keys( 'app', '_endpoint', '_sock', '_connProm', '_models', '_subs', '_subProms', '_unsubProms', '_termProm', '_connTimer', '_connDelay', '_failCount' );
    assert( client2._sock == undefined );
  } );


  it( 'client 2 connect failure', async () => {
    let err;
    conncount = 0;

    try {
      await client2.connect();
    }
    catch( e ) {
      err = e;
    }

    assert( err.constructor.name == "ErrorEvent" );
    assert( conncount == 0 );
    assert( client2._sock instanceof global.WebSocket );
    assert( client2.readyState == 3 );
  } );


  it( 'client 3 create', () => {
    client3 = new ChannelSockClient( fakeapp, `ws://127.0.0.1:${port}` );

    assert( model2 instanceof FakeModel );
    assert( client3 instanceof ChannelSockClient );
    expect( client3 ).to.be.an( 'object' )
      .to.include.keys( 'app', '_endpoint', '_sock', '_connProm', '_models', '_subs', '_subProms', '_unsubProms', '_termProm', '_connTimer', '_connDelay', '_failCount' );
    assert( client3._sock == undefined );
    sandbox.spy( client3 );
  } );


  it( 'client 3 connect', async () => {

    conncount = 0;

    await client3.connect();

    assert( conncount == 1 );
    assert( client3._sock instanceof global.WebSocket );
    assert( client3.readyState == 1 );
  } );


  it( 'client 3 terminate', async () => {
    let prom;
    sandbox.resetHistory();
    prom = client3.terminate();

    assert( prom instanceof Promise );
    assert( client3._sock );
    assert( client3._termProm instanceof Map );
    assert( client3._termProm.size == 3 );
    assert( client3.readyState == 2 );

    await prom;

    assert( client3.readyState == 3 );
    assert( client3._termProm.size == 0 );
    assert( client3._sock == undefined );
  } );


  it( 'client 3 reconnect', async () => {
    conncount = 0;
    await client3.connect();
    assert( conncount == 1 );
    assert( client3._sock instanceof global.WebSocket );
    assert( client3.readyState == 1 );
  } );


  if ( 'notification message', async () => {
  } );




  it( 'client 1 unsubscribe fail', async () => {
    sandbox.resetHistory();
    await client1.unsubscribe( '/test2', model1 );

    assert( client1.readyState == 1 );
    assert( client1._subs.size == 1 );
    assert( client1._subProms.size == 0 );
    assert( client1._unsubProms.size == 0 );
    assert( client1._models.get( model1 ) == "/test1" );
  } );


  it( 'insert change notification unsubscribed', async () => {
    sandbox.resetHistory();

    for ( let client of chansock.clients ) {
      client.send( JSON.stringify( {
        channel: '/test2',
        change: 'insert',
        type: 'notify',
        ids: [ 1, 2, 3 ]
      } ) );
    }

    // Wait for events etc
    await new Promise( (res) => setTimeout( () => res(), 100 ) );

    assert( client1._handleNotify.calledOnce );
    assert( client3._handleNotify.calledOnce );
    expect( fakeapp.log.lastCall.lastArg ).to.contain( 'unsubscribed channel' );
  } );


  it( 'insert change notification', async () => {
    sandbox.resetHistory();

    for ( let client of chansock.clients ) {
      client.send( JSON.stringify( {
        channel: '/test1',
        change: 'insert',
        type: 'notify',
        ids: [ 1, 2, 3 ]
      } ) );
    }

    // Wait for events etc
    await new Promise( (res) => setTimeout( () => res(), 100 ) );

    assert( client1._handleNotify.calledOnce );
    assert( model1.handleEvent.calledOnce );
    assert( model3.handleEvent.calledOnce );
    assert( model1.handleEvent.firstCall.args[0] == 'notify');
    assert( model3.handleEvent.firstCall.args[0] == 'notify' );
    expect( model1.handleEvent.firstCall.args[1] ).to.deep.equal( { 'change': 'insert', 'ids': [ 1, 2, 3 ] } );
    expect( model3.handleEvent.firstCall.args[1] ).to.deep.equal( { 'change': 'insert', 'ids': [ 1, 2, 3 ] } );
  } );


  it( 'delete change notification', async () => {
    sandbox.resetHistory();

    for ( let client of chansock.clients ) {
      client.send( JSON.stringify( {
        channel: '/test1',
        change: 'delete',
        type: 'notify',
        ids: [ 1, 2, 3 ]
      } ) );
    }

    // Wait for events etc
    await new Promise( (res) => setTimeout( () => res(), 100 ) );

    assert( client1._handleNotify.calledOnce );
    assert( model1.handleEvent.calledOnce );
    assert( model3.handleEvent.calledOnce );
    assert( model1.handleEvent.firstCall.args[0] == 'notify');
    assert( model3.handleEvent.firstCall.args[0] == 'notify' );
    expect( model1.handleEvent.firstCall.args[1] ).to.deep.equal( { 'change': 'delete', 'ids': [ 1, 2, 3 ] } );
    expect( model3.handleEvent.firstCall.args[1] ).to.deep.equal( { 'change': 'delete', 'ids': [ 1, 2, 3 ] } );
  } );


  it( 'update change notification', async () => {
    sandbox.resetHistory();

    for ( let client of chansock.clients ) {
      client.send( JSON.stringify( {
        channel: '/test1',
        change: 'update',
        type: 'notify',
        ids: [ 1, 2, 3 ]
      } ) );
    }

    // Wait for events etc
    await new Promise( (res) => setTimeout( () => res(), 100 ) );

    assert( client1._handleNotify.calledOnce );
    assert( model1.handleEvent.calledOnce );
    assert( model3.handleEvent.calledOnce );
    assert( model1.handleEvent.firstCall.args[0] == 'notify');
    assert( model3.handleEvent.firstCall.args[0] == 'notify' );
    expect( model1.handleEvent.firstCall.args[1] ).to.deep.equal( { 'change': 'update', 'ids': [ 1, 2, 3 ] } );
    expect( model3.handleEvent.firstCall.args[1] ).to.deep.equal( { 'change': 'update', 'ids': [ 1, 2, 3 ] } );
  } );


  it( 'records notification', async () => {
    sandbox.resetHistory();

    const rec1 = { 'foo': 'bar', 'car': 1, 'far': false, 'par': null };
    const rec2 = { 'foo': 'war', 'car': 0, 'far': true, 'par': 123 };

    for ( let client of chansock.clients ) {
      client.send( JSON.stringify( {
        channel: '/test1',
        records: [ rec1, rec2 ],
        type: 'records',
      } ) );
    }

    // Wait for events etc
    await new Promise( (res) => setTimeout( () => res(), 100 ) );

    assert( client1._handleRecords.calledOnce );
    assert( model1.handleEvent.calledOnce );
    assert( model3.handleEvent.calledOnce );
    assert( model1.handleEvent.firstCall.args[0] == 'records');
    assert( model3.handleEvent.firstCall.args[0] == 'records' );
    expect( model1.handleEvent.firstCall.args[1] ).to.deep.equal( { 'records': [ rec1, rec2 ] } );
    expect( model3.handleEvent.firstCall.args[1] ).to.deep.equal( { 'records': [ rec1, rec2 ] } );
  } );



  it( 'client 1 error', async () => {
    let sock, theerr;
    sandbox.resetHistory();
    assert( client1._termProm.size == 0 );
    sock = client1._sock;

    // This doesn't actually error the connection, just tests handler
    theerr = new Error( `A strange message` );
    client1._sock.emit( 'error', theerr );

    await new Promise( res => setTimeout( () => res(), 100 ) );
    assert( client1._sock == undefined );
    assert( client1._subs.size );
    assert( client1._handleClose.calledOnce );
    assert( client1._handleClose.firstCall.args[0].error == theerr );
    assert( client1._handleClose.firstCall.args[1] == true );

    expect( client1._connDelay ).to.be.a( 'number' );
    assert( client1._connTimer );
    clearTimeout( client1._connTimer );
    client1._connTimer = undefined;
    assert( client1._failCount == 1 )

    assert( model1.handleEvent.calledOnce );
    assert( model3.handleEvent.calledOnce );
    assert( model1.handleEvent.firstCall.args[0] == 'disconnect');
    assert( model3.handleEvent.firstCall.args[0] == 'disconnect' );

    // Manually close socket
    assert( sock.readyState == 1 );
    sock.terminate();
  } );


  it( 'client 1 reconnect', async () => {
    let tmpws;

    sandbox.resetHistory();
    conncount = 0;
    assert( client1._subs.size );

    await client1.connect();

    assert( conncount == 1 );
    assert( client1._sock instanceof global.WebSocket );
    assert( client1.readyState == 1 );

    tmpws = client1._sock;
    await client1.connect();
    assert( client1._sock == tmpws );

    assert( model1.handleEvent.calledOnce );
    assert( model3.handleEvent.calledOnce );
    assert( model1.handleEvent.firstCall.args[0] == 'connect');
    assert( model3.handleEvent.firstCall.args[0] == 'connect');
  } );


} );


