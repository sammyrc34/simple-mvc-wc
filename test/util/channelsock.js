

import sinon from 'sinon';
import http from 'node:http';
import { describe, it } from 'mocha';
import { expect, assert } from 'chai';

import { EventEmitter } from 'node:events';

import { ChannelSock } from '../../lib/util/channelsock.js';
import { WebSocketServer, WebSocket } from 'ws';



describe( 'Channel Websocket Utility', () => { 
  let server, chansock, client1, client2, client3, sandbox, port, conncount = 0;

  before( () => { 
    sandbox = sinon.createSandbox();
  } );

  after( async () => {
    if ( client1 )
      client1.terminate();
    if ( client2 )
      client2.terminate();
    await server.close();
    server = undefined;
  } );


  it( 'test server setup', () => {
    server = http.createServer();
    expect( server ).to.be.instanceof( http.Server );
  } );
 


  it( 'instantiate serverless', () => {
    chansock = new ChannelSock( { 'channels': [ '/test2' ], 'subscribeLimit': 2 }, { noServer: true } );

    expect( chansock ).to.be.an( 'object' );
    expect( chansock ).to.be.instanceof( ChannelSock );
    expect( chansock ).to.be.instanceof( EventEmitter );
    expect( chansock._ws ).to.be.instanceof( WebSocketServer );

    expect( chansock._conf ).to.be.an( 'object' );
    expect( chansock._conf ).to.have.property( 'channels' ).to.be.an( 'array' );
    expect( chansock._conf ).to.have.property( 'maxClients', 0 );
    expect( chansock._conf ).to.have.property( 'subscribeLimit', 2 );

  } );



  it( 'test server listen', async () => {
    const listening = new Promise( (resolve) => {
      server.on( 'listening', () => { resolve() } );
    } );

    server.on('upgrade', (req, socket, head) => {
      conncount++;
      chansock.handleUpgrade(req, socket, head );
    } );

    server.listen( 0, '127.0.0.1' );

    await listening;

    port = server.address().port;
  } );



  it( 'client 1', ( done ) => {
    client1 = new WebSocket( `http://127.0.0.1:${port}` );
    assert( client1.readyState == 0 );
    client1.on( 'open', () => { done() } );
    client1.on( 'error', (err) => { done( err ) } );
  } );



  it( 'client 1 ping', ( done ) => {
    assert( client1.readyState == 1 );
    assert( conncount == 1 );
    client1.on( 'pong', () => { done() } );
    client1.ping();
  } );



  it( 'client 1 subscribe fail', ( done ) => { 
    const msg = {
      'type'      : 'subscribe',
      'channel'   : '/test1'
    };

    client1.onerror = ( err ) => {
      done( err );
    };
    client1.onmessage = ( message ) => {
      let resp = JSON.parse( message.data );
      assert( resp.type == "subscribe" );
      assert( resp.success == false );
      done();
    };

    client1.send( JSON.stringify( msg ) );
  } );


  it( 'client 1 subscribe success', ( done ) => { 
    const msg = {
      'type'      : 'subscribe',
      'channel'   : '/test2'
    };

    client1.onerror = ( err ) => done( err );
    client1.onmessage = ( message ) => {
      let resp = JSON.parse( message.data );
      assert( resp.type == "subscribe" );
      assert( resp.success == true );
      done();
    };

    client1.send( JSON.stringify( msg ) );
  } );


  it( 'server channel has clients', () => {
    assert( chansock._channels.get( '/test2' ).socks.size == 1 );
  } );


  it( 'create channel', () => {
    chansock.addChannel( '/test1' );
    assert( chansock._channels.has( '/test1' ) );
  } );



  it( 'client 1 subscribe repeat', ( done ) => { 
    const msg = {
      'type'      : 'subscribe',
      'channel'   : '/test1'
    };

    client1.onerror = ( err ) => {
      done( err );
    };
    client1.onmessage = ( message ) => {
      let resp = JSON.parse( message.data );
      assert( resp.type == "subscribe" );
      assert( resp.success == true );
      done();
    };

    client1.send( JSON.stringify( msg ) );
  } );


  it( 'create channel placeholder', () => {
    chansock.addChannel( '/foo/:bar' );
    assert( chansock._channels.has( '/foo/:bar' ) );
  } );


  it( 'client 1 subscribe with placeholder', ( done ) => { 
    const msg = {
      'type'      : 'subscribe',
      'channel'   : '/foo/1'
    };

    client1.onerror = ( err ) => {
      done( err );
    };
    client1.onmessage = ( message ) => {
      let resp = JSON.parse( message.data );
      assert( resp.type == "subscribe" );
      assert( resp.success == true );
      done();
    };

    client1.send( JSON.stringify( msg ) );
  } );


  it( 'client 1 subscribe failure', ( done ) => { 
    const msg = {
      'type'      : 'subscribe',
      'channel'   : '/foo/3'
    };

    client1.onerror = ( err ) => {
      done( err );
    };
    client1.onmessage = ( message ) => {
      let resp = JSON.parse( message.data );
      assert( resp.type == "subscribe" );
      assert( resp.success == false );
      expect( resp.message ).to.contain( 'limit' );
      done();
    };

    client1.send( JSON.stringify( msg ) );
  } );



  it( 'client 2', ( done ) => {
    client2 = new WebSocket( `http://127.0.0.1:${port}` );
    assert( client2.readyState == 0 );
    client2.on( 'open', () => { done() } );
    client2.on( 'error', (err) => { done( err ) } );
  } );



  it( 'client 2 ping', ( done ) => {
    assert( client2.readyState == 1 );
    assert( conncount == 2 );
    client2.on( 'pong', () => { done() } );
    client2.ping();
  } );


  it( 'server channel has clients', () => {
    assert( chansock._clients.size == 2 );
  } );



  it( 'client 2 subscribe with placeholder', ( done ) => { 
    const msg = {
      'type'      : 'subscribe',
      'channel'   : '/foo/2'
    };

    client2.onerror = ( err ) => {
      done( err );
    };
    client2.onmessage = ( message ) => {
      let resp = JSON.parse( message.data );
      assert( resp.type == "subscribe" );
      assert( resp.success == true );
      done();
    };

    client2.send( JSON.stringify( msg ) );
  } );



  it( 'channels correct', () => {
    assert( typeof chansock._channels.get( '/foo/:bar' ) == "object" );
    assert( chansock._channels.get( '/foo/:bar' ).socks instanceof Map );
    assert( chansock._channels.get( '/foo/:bar' ).socks.size == 0 );
    assert( typeof chansock._channels.get( '/foo/1' ) == "object" );
    assert( chansock._channels.get( '/foo/1' ).socks instanceof Map );
    assert( chansock._channels.get( '/foo/1' ).socks.size == 1 );
    assert( typeof chansock._channels.get( '/foo/2' ) == "object" );
    assert( chansock._channels.get( '/foo/2' ).socks instanceof Map );
    assert( chansock._channels.get( '/foo/2' ).socks.size == 1 );

  } );


  it( 'message broadcast', (done) => {
    let tmp, prom1, prom2;

    prom1 = new Promise( (res,rej) => {
      client1.onmessage = ( message ) => { return res( message ) };
      client1.onerror = (err) => { return rej( err ) };
    } );

    prom2 = new Promise( (res,rej) => {
      client2.onmessage = ( message ) => { return res( message ) };
      client2.onerror = (err) => { return rej( err ) };
    } );

    Promise.all( [ prom1, prom2 ] )
      .then( (resps) => { 
        expect( resps[0] ).to.be.an( 'object' );
        tmp = JSON.parse( resps[0].data );
        expect( tmp ).to.have.property( 'type', 'test' );
        expect( resps[1] ).to.be.an( 'object' );
        tmp = JSON.parse( resps[1].data );
        expect( tmp ).to.have.property( 'type', 'test' );
        done();
      } )
      .catch( err => { done(err) } );

    chansock.broadcastManual( JSON.stringify( { 'type': 'test' } ) );
  } );


  it( 'manual notify', (done) => {
    let tmp, prom1;

    prom1 = new Promise( (res,rej) => {
      client1.onmessage = ( message ) => { return res( message ) };
      client1.onerror = (err) => { return rej( err ) };
    } );

    Promise.all( [ prom1 ] )
      .then( (resps) => { 
        expect( resps[0] ).to.be.an( 'object' );
        tmp = JSON.parse( resps[0].data );
        expect( tmp ).to.have.property( 'type', 'test2' );
        done();
      } )
      .catch( err => { done(err) } );

    chansock.notifyManual( '/foo/1', JSON.stringify( { 'type': 'test2' } ) );
  } );



  it( 'notify update change 1', (done) => {
    let tmp, prom1;

    client2.onmessage = sandbox.stub();

    prom1 = new Promise( (res,rej) => {
      client1.onmessage = ( message ) => { return res( message ) };
      client1.onerror = (err) => { return rej( err ) };
    } );

    Promise.all( [ prom1 ] )
      .then( (resps) => { 
        expect( resps[0] ).to.be.an( 'object' );
        tmp = JSON.parse( resps[0].data );
        expect( tmp ).to.have.property( 'channel', '/foo/1' );
        expect( tmp ).to.have.property( 'change', 'update' );
        expect( tmp ).to.have.property( 'type', 'notify' );
        expect( tmp ).to.have.property( 'ids' ).to.contain( 1, 2, 3 );

        assert( client2.onmessage.notCalled );

        done();
      } )
      .catch( err => { done(err) } );

    chansock.notifyChange( '/foo/1', 'update', [ 1, 2, 3 ] );
  } );



  it( 'notify update change 2', (done) => {
    let tmp, prom1;

    client1.onmessage = sandbox.stub();

    prom1 = new Promise( (res,rej) => {
      client2.onmessage = ( message ) => { return res( message ) };
      client2.onerror = (err) => { return rej( err ) };
    } );

    Promise.all( [ prom1 ] )
      .then( (resps) => { 
        expect( resps[0] ).to.be.an( 'object' );
        tmp = JSON.parse( resps[0].data );
        expect( tmp ).to.have.property( 'channel', '/foo/2' );
        expect( tmp ).to.have.property( 'change', 'update' );
        expect( tmp ).to.have.property( 'type', 'notify' );
        expect( tmp ).to.have.property( 'ids' ).to.contain( 4, 5, 6 );

        assert( client1.onmessage.notCalled );

        done();
      } )
      .catch( err => { done(err) } );

    chansock.notifyChange( '/foo/2', 'update', [ 4, 5, 6 ] );
  } );


  it( 'notify record change', async() => {
    let tmp, prom1;

    const tmprecs = [ {
      id: 123,
      name: "Barry",
      alias: "Bazza Wazza",
      flag: true,
      other: null
    }, { 
      id: 234,
      name: "Shazza",
      alias: "Shazza Wazza",
      flag: false,
      other: undefined
    } ];

    client1.onmessage = sandbox.stub();

    prom1 = new Promise( (res,rej) => {
      client2.onmessage = ( message ) => { return res( message ) };
      client2.onerror = (err) => { return rej( err ) };
    } );

    Promise.all( [ prom1 ] )
      .then( (resps) => { 
        expect( resps[0] ).to.be.an( 'object' );
        tmp = JSON.parse( resps[0].data );
        expect( tmp ).to.have.property( 'channel', '/foo/2' );
        expect( tmp ).to.have.property( 'type', 'records' );
        expect( tmp ).to.have.property( 'records' );
        assert( tmp.records.length == 2 );
        expect( tmp.records ).to.contain( tmprecs[0] );
        expect( tmp.records ).to.contain( tmprecs[1] );

        assert( client1.onmessage.notCalled );

        done();
      } )
      .catch( err => { done(err) } );

    chansock.notifyRecords( '/foo/2', tmprecs );
  } );


  it( 'channel placeholder remove', ( done ) => {
    let tmp, prom1, prom2

    prom1 = new Promise( (res,rej) => {
      client1.onmessage = ( message ) => { return res( message ) };
      client1.onerror = (err) => { return rej( err ) };
    } );

    prom2 = new Promise( (res,rej) => {
      client2.onmessage = ( message ) => { return res( message ) };
      client2.onerror = (err) => { return rej( err ) };
    } );

    Promise.all( [ prom1, prom2 ] )
      .then( (resps) => { 
        expect( resps[0] ).to.be.an( 'object' );
        tmp = JSON.parse( resps[0].data );
        expect( tmp ).to.have.property( 'type', 'unsubscribe' );
        expect( tmp ).to.have.property( 'channel', '/foo/1' );

        expect( resps[1] ).to.be.an( 'object' );
        tmp = JSON.parse( resps[1].data );
        expect( tmp ).to.have.property( 'type', 'unsubscribe' );
        expect( tmp ).to.have.property( 'channel', '/foo/2' );

        assert( ! chansock._channels.has( '/foo/:bar' ) );
        assert( ! chansock._channels.has( '/foo/1' ) );
        assert( ! chansock._channels.has( '/foo/2' ) );

        done();
      } )
      .catch( err => { done(err) } );
    
    chansock.removeChannel( '/foo/:bar' );

  } );


  it( 'connection limit', (done) => {
    chansock._conf.maxClients = 2;

    client3 = new WebSocket( `http://127.0.0.1:${port}` );
    assert( client3.readyState == 0 );
    client3.on( 'close', () => { done(); } );
    client3.on( 'error', (err) => { done( err ) } );
  } );


  it( 'check connections', (done) => {
    let prom1, prom2, prom3;

    prom1 = new Promise( (res,rej) => {
      client1.on( 'ping', () => { return res() } ); 
      client1.on( 'error', (err) => { return rej( err ) } );
    } );

    prom2 = new Promise( (res,rej) => {
      client2.on( 'ping', () => { return res() } ); 
      client2.on( 'error', (err) => { return rej( err ) } );
    } );

    // Wait for ping responses to be processed
    prom3 = new Promise( (res) => {
      setTimeout( () => { res() }, 300 );
    } );

    Promise.all( [ prom1, prom2, prom3 ] )
      .then( () => { 
        for ( let uuid of chansock._clients.keys() ) {
          assert( chansock._clients.get( uuid ).sock.pingresponse == true );
        }
        done();
      } )
      .catch( err => { done(err) } );
    
    chansock._checkClients();
  } );



  it( 'additional connection', (done) => {
    chansock._conf.maxClients = 3;
    let resp = false;

    client3 = new WebSocket( `http://127.0.0.1:${port}` );
    assert( client3.readyState == 0 );
    client3.on( 'close', () => { if ( ! resp ) done( new Error( "Connection should not close" ) ); } );
    client3.on( 'error', (err) => { done( err ) } );
    client3.on( 'open', () => { 
      resp = true;
      assert( chansock._clients.size == 3 );
      done();
    } );
  } );



  it( 'connection removed part 1', (done) => {
    let prom;

    client3.terminate();

    // Wait for ping responses to be processed
    prom = new Promise( (res) => {
      setTimeout( () => { res() }, 300 );
    } );

    Promise.all( [ prom ] )
      .then( () => { 
        assert( chansock._clients.size == 3 );
        assert( Array.from( chansock._clients.values() ).some( c => c.sock.pingresponse == false ) );
        done();
      } )
      .catch( err => { done(err) } );
    
    chansock._checkClients();   
  } );



  it( 'connection removed part 2', (done) => {
    let prom;

    client3.terminate();

    // Wait for ping responses to be processed
    prom = new Promise( (res) => {
      setTimeout( () => { res() }, 300 );
    } );

    Promise.all( [ prom ] )
      .then( () => { 
        assert( chansock._clients.size == 2 );
        assert( Array.from( chansock._clients.values() ).every( c => c.sock.pingresponse ) );
        done();
      } )
      .catch( err => { done(err) } );
    
    chansock._checkClients();   
  } );



  it( 'unsubscribe failure', (done) => {
    const msg = {
      'type'      : 'unsubscribe',
      'channel'   : '/test3'
    };

    client1.onerror = ( err ) => done( err );
    client1.onmessage = ( message ) => {
      let resp = JSON.parse( message.data );
      assert( resp.type == "unsubscribe" );
      assert( resp.success == false );
      done();
    };

    client1.send( JSON.stringify( msg ) );
  } );



  it( 'unsubscribe success', (done) => {
    const msg = {
      'type'      : 'unsubscribe',
      'channel'   : '/test1'
    };

    client1.onerror = ( err ) => done( err );
    client1.onmessage = ( message ) => {
      let resp = JSON.parse( message.data );
      assert( resp.type == "unsubscribe" );
      assert( resp.success == true );
      done();
    };

    client1.send( JSON.stringify( msg ) );
  } );



} );


