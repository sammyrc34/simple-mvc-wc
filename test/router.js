


import sinon from 'sinon';
import { describe, it } from 'mocha';
import { expect, assert } from 'chai';
import fs from 'node:fs';
import path from 'node:path';
import { URL } from 'url';
const __dirname = new URL('.', import.meta.url).pathname;

import Util from '../lib/util.js';
import SimpleRouter from '../lib/router.js';


class View1_2 {
  static slotCount() { return 2 };
  static canContain() { return true };
  static canBeContained() { return true };
};
class View3_4_5_6 {
  static slotCount() { return 1 };
  static canContain(v) { return ( v.context == 'view7' ) ? true : false }
  static canBeContained(v) { return ( v.context == 'view2' ) ? true : false }
};
class View7 {
  static slotCount() { return 0 };
  static canContain() { return false };
  static canBeContained(v) { return ( v.context == 'view2' ) ? true : false };
}


describe( 'Router', () => { 
  const app = {};
  let router, sandbox;

  before( () => { 
    sandbox = sinon.createSandbox();
    app.log = sandbox.stub();
    app.util = Util;
  } );

  after( () => {
  } );


  it( 'instantiate', () => {
    router = new SimpleRouter( app );

    assert( router );
    assert( router instanceof SimpleRouter );
    expect( router._lastViews ).to.be.an( 'array' );
    expect( router._lastQuery ).to.be.an( 'object' );
    expect( router._routeMap ).to.be.an( 'object' );
  } );


  it( 'register1', () => {
    router.registerView( 'view1', View1_2, 'view1' );
    router.registerView( 'view2', View1_2, 'view2' );
    router.registerView( 'view3', View3_4_5_6 );
    router.registerView( 'view4/:id1/:id2', View3_4_5_6, 'view4' );
    router.registerView( 'view5/:id2', View3_4_5_6, 'view5' );
    router.registerView( 'view6/:id1/:id2/else', View3_4_5_6, 'view6' );
    router.registerView( 'view7', View7 );

    assert( Object.keys( router._routeMap ).length == 7 );
  } );


  it( 'clean path', () => {
    assert( router.cleanPath( 'foo/bar' ) == '/foo/bar' );
    assert( router.cleanPath( '/foo/bar' ) == '/foo/bar' );
    assert( router.cleanPath( ' foo/bar ' ) == '/foo/bar' );
    assert( router.cleanPath( '//foo///bar' ) == '/foo/bar' );
  } );


  it( 'path to view failures', () => {
    let tmp;

    tmp = router.pathToViews( 'view1/view3' );
    expect( () => { router.checkHeirarchy( tmp ) } ).to.throw();

    expect( () => { router.pathToViews( 'view1/view4' ) } ).to.throw();

    expect( () => { router.pathToViews( 'view5' ) } ).to.throw();

    tmp = router.pathToViews( 'view1/view4/123/abc' );
    expect( () => { router.checkHeirarchy( tmp ) } ).to.throw();

  } );


  it( 'path to views', () => {
    let tmp;

    tmp = router.pathToViews( 'view1' );
    router.checkHeirarchy( tmp );
    assert( tmp.length == 1 );
    assert( tmp[ 0 ].path == 'view1' );
    assert( tmp[ 0 ].context == 'view1' );
    assert( tmp[ 0 ].class == View1_2 );
    assert( tmp[ 0 ].slots == 2 );

    tmp = router.pathToViews( 'view1/view2' );
    router.checkHeirarchy( tmp );
    assert( tmp.length == 2 );
    assert( tmp[ 0 ].path == 'view1' );
    assert( tmp[ 0 ].context == 'view1' );
    assert( tmp[ 0 ].class == View1_2 );
    assert( tmp[ 0 ].slots == 1 );
    assert( tmp[ 1 ].path == 'view2' );
    assert( tmp[ 1 ].context == 'view2' );
    assert( tmp[ 1 ].class == View1_2 );
    assert( tmp[ 1 ].slots == 2 );

    tmp = router.pathToViews( 'view1/view2/view1' );
    router.checkHeirarchy( tmp );
    assert( tmp.length == 3 );
    assert( tmp[ 0 ].path == 'view1' );
    assert( tmp[ 0 ].context == 'view1' );
    assert( tmp[ 0 ].class == View1_2 );
    assert( tmp[ 0 ].slots == 1 );
    assert( tmp[ 1 ].path == 'view2' );
    assert( tmp[ 1 ].context == 'view2' );
    assert( tmp[ 1 ].class == View1_2 );
    assert( tmp[ 1 ].slots == 1 );
    assert( tmp[ 2 ].path == 'view1' );
    assert( tmp[ 2 ].context == 'view1' );
    assert( tmp[ 2 ].class == View1_2 );
    assert( tmp[ 2 ].slots == 2 );

    tmp = router.pathToViews( 'view5/123abc' );
    router.checkHeirarchy( tmp );
    assert( tmp[ 0 ].path == 'view5/:id2' );
    assert( tmp[ 0 ].context == 'view5' );
    expect( tmp[ 0 ].params ).to.be.an( "object" ).to.have.property( 'id2', '123abc' );
    assert( tmp[ 0 ].class == View3_4_5_6 );
    assert( tmp[ 0 ].slots == 1 );

    tmp = router.pathToViews( 'view2/view4/123/abc/view7' );
    router.checkHeirarchy( tmp );
    assert( tmp[ 0 ].path == 'view2' );
    assert( tmp[ 0 ].context == 'view2' );
    assert( tmp[ 0 ].class == View1_2 );
    assert( tmp[ 0 ].slots == 0 );
    assert( tmp[ 0 ].descendants.length == 2 );
    assert( tmp[ 1 ].path == 'view4/:id1/:id2' );
    assert( tmp[ 1 ].context == 'view4' );
    assert( tmp[ 1 ].class == View3_4_5_6 );
    assert( tmp[ 1 ].slots == 1 );
    expect( tmp[ 1 ].params ).to.be.an( "object" ).to.have.property( 'id1', '123' );
    expect( tmp[ 1 ].params ).to.be.an( "object" ).to.have.property( 'id2', 'abc' );
    assert( tmp[ 2 ].path == 'view7' );
    assert( tmp[ 2 ].class == View7 );
    assert( tmp[ 2 ].slots == 0 );

  } );


} );


