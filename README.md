# simple-mvc-wc

A full featured and easy to use client-side MVC with included web components, built upon Lit. 

## Getting Started

#### Install the library:

```shell
npm i simple-mvc-wc
```

#### Run the demo and documentation server:

```shell
cd node_modules/simple-mvc-wc

npm run start:dev
```

#### In your application

```javascript
import {SimpleApp} from 'simple-mvc-wc';

import {BasicView} from '....';

class BasicApp extends SimpleApp {
  start() {
    this.registerView( 'start', BasicView );
    super.start();
  }
}

const app = new BasicApp( {
  name          : 'Basic App',
  container     : 'container',
  prefix        : 'basicapp',
  defaultpage   : 'start',
  debug         : false
} );

app.start();
```


## Limitations

This library is provided as is, worked on in my spare time only. It is likely full to the brim of bugs. If you find one, please feel free to submit an issue and even a merge request. Otherwise I will get to it when I can.


