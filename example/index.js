
// Styles
import "./styles/style.scss";


// App
import { SimpleApp } from '../lib/index.js';


// Views
import { AppView } from './views/app.js';
import { TopView } from './views/top.js';
import { ControllersView } from './views/controllers.js';
import { ViewsView } from './views/views.js';
import { ButtonView } from './views/buttons.js';
import { InputsView } from './views/inputs.js';
import { SelectsView } from './views/selects.js';
import { TextareasView } from './views/textareas.js';
import { FormsView } from './views/forms.js';
import { TabsView } from './views/tabs.js';
import { ListsView } from './views/lists.js';
import { TablesView } from './views/tables.js';
import { AlertsView } from './views/alerts.js';
import { MenusView } from './views/menus.js';
import { PopupsView } from './views/popups.js';
import { CarouselsView } from './views/carousels.js';
import { AccordionsView } from './views/accordions.js';
import { SlideoutsView } from './views/slideouts.js';
import { SidemenusView } from './views/sidemenus.js';
import { BusyMaskView } from './views/busy.js';
import { ImageView } from "./views/images.js";
import { ThemesView } from './views/themes.js';

import { ModelRecordsView } from './views/model_records.js';
import { ModelCollectionsView } from './views/model_collections.js';
import { ModelRemoteView } from './views/model_remote.js';
import { ModelClassesView } from "./views/model_classes.js";
import { ModelBindableView } from "./views/model_bindable.js";
import { ModelPollingView } from "./views/model_polling.js";
import { ModelIntegratedView } from "./views/model_integrated.js";
import { UtilQueryTranslateView } from "./views/util_querytranslate.js";
import { NotFoundView } from "./views/notfound.js";
import { ErrorView } from "./views/error.js";


// Models
import { ExamplePeople, ExampleColours, ExampleForminfo } from './models/example1.js';


// Example data
import { example1Colors, example1Data, example1Form, exampleSmallColours } from './data/example.js';


class TestApp extends SimpleApp {

  registerTopViews() {

    this.registerView( '404', NotFoundView );
    this.registerView( '500', ErrorView );

    this.registerView( 'example', TopView );
    this.registerView( 'app', AppView );
    this.registerView( 'views', ViewsView );
    this.registerView( 'buttons', ButtonView );
    this.registerView( 'inputs', InputsView );
    this.registerView( 'selects', SelectsView );
    this.registerView( 'textareas', TextareasView );
    this.registerView( 'forms', FormsView );
    this.registerView( 'tabs', TabsView );
    this.registerView( 'lists', ListsView );
    this.registerView( 'tables', TablesView );
    this.registerView( 'alerts', AlertsView );
    this.registerView( 'menus', MenusView );
    this.registerView( 'popups', PopupsView );
    this.registerView( 'carousels', CarouselsView );
    this.registerView( 'accordions', AccordionsView );
    this.registerView( 'slideouts', SlideoutsView );
    this.registerView( 'sidemenus', SidemenusView );
    this.registerView( 'busy', BusyMaskView );
    this.registerView( 'controllers', ControllersView );
    this.registerView( 'themes', ThemesView );
    this.registerView( 'images', ImageView );
    this.registerView( 'model_records', ModelRecordsView );
    this.registerView( 'model_collections', ModelCollectionsView );
    this.registerView( 'model_remote', ModelRemoteView );
    this.registerView( 'model_classes', ModelClassesView );
    this.registerView( 'model_bindable', ModelBindableView );
    this.registerView( 'model_integrated', ModelIntegratedView );
    this.registerView( 'model_polling', ModelPollingView );
    this.registerView( 'util_querytranslate', UtilQueryTranslateView );
  }

  registerModels() {
    this.registerModel( 'form1', ExampleForminfo );
    this.registerModel( 'example', ExamplePeople );
    this.registerModel( 'colours', ExampleColours );
    this.registerModel( 'smallcolours', ExampleColours );
  }

  start() {
    // Load initial data
    const form1 = this.fetchModel( 'form1' );
    const example = this.fetchModel( 'example' );
    const colours = this.fetchModel( 'colours' );
    const smlcolours =  this.fetchModel( 'smallcolours' );


    form1.add( example1Form );
    example.add( example1Data );
    colours.add( example1Colors );
    smlcolours.add( exampleSmallColours );


    super.start();
  }

}


const app = new TestApp( {
  name: 'Test App',
  container: 'app',
  prefix: 'app',
  defaultpage: 'example/app',
  debug: true,
  notFoundPage: '404',
  errorPage: '500'
} );


app.registerTopViews();
app.registerModels();
app.start();

