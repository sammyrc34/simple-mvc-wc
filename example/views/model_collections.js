

import DocParser from '../util/docparser.js';
import { ExampledView } from './example.js';


import rawcode from '!raw-loader!../../lib/models/collection.js';

let __docs = undefined;



class ModelCollectionsView extends ExampledView {

  static methodNames( ) {
    return [ 'constructor', 'setup', 'clear' , 'length', 'hasRecord', 'fetch', 'fetchOne', 'getRecord', 'getRecords', 'searchOne', 'add', 'remove', 'update', 'check', 'parse', 'uniqueValues', 'failed', 'setRecordError', 'clearRecordError', 'getMessage', 'getRecordInfo', 'reset' ];
  }

  static typeNames() {
    return [  'FieldCriteriaType', 'FieldCriteriaCollection', 'FilterType', 'FilterList', 'FiltersCollection', 'SortCriteria', 'SortsType', 'ResultType' ] ;
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }



  static build() {
    return this.builder`
      <div id="model-collections-page">
        <h2>Collections</h2>
        <h3 class="lined">Description</h3>
        <p>The <code>SimpleCollection</code> class is an in-memory multi-record model. It provides a capable interface to interact with the data collection.</p>
        <p>The <code>SimpleCollection</code> class supports the same CRUD-like interface as other data model classes, comprised of <code>add</code>, <code>update</code>, <code>remove</code> and <code>fetch</code> with additional helper methods.</p>
        <p>Filtering occurs at the point of fetching data from the model. Filters can be nested, support <em>AND</em> or <em>OR</em> operators, with many comparison operators suitable for numeric, string and list types. The filter types below describe the filter structures in detail.</p>
        <p>Similarly, sorting occurs at the point of data fetching, occuring after any filtering. Sorting supports multiple fields, custom, string or numeric data types and ascending or descending.</p>
        <p><strong>Note:</strong> As the <code>SimpleCollection</code> is an in-memory collection, it may not be suitable for very large datasets.</p>
        

        <div class="types">
          <h3 class="lined">Types</h3>
          ${this.buildTypes()}
        </div>


        <div class="methods">
          <h3 class="lined">Methods</h3>
          <p></p>
          ${this.buildMethods( 'BasicCollection' )}
        </div>


      </div>
    `
  }


}

export { ModelCollectionsView };

