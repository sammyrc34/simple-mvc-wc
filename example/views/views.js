
import DocParser from '../util/docparser.js';
import { SimpleButton, SimpleTable } from '../../lib/index.js';
import { ExampledView } from './example.js';

import rawcode from '!raw-loader!../../lib/view.js';

let __docs = undefined;

SimpleButton.register();
SimpleTable.register();


class ViewsView extends ExampledView {

  static title() {
    return "Views";
  }

  static methodNames( ) {
    return [ 'title', 'build', 'app', 'render', 'builder', 'rebuild', 'cssBuilder', 'svgBuilder', 'nothing', 'includeSvg', 'includeHtml', 'ifDefined', 'action', 'connect', 'disconnect', 'reconnect', 'slotCount', 'canContain', 'canBeContained', 'getTargets' ];
  }

  static build() {
    const now = new Date();

    return this.builder`
      <div id="view-basic-page">
        <h2>Views</h2>
        <h3 class="lined">Description</h3>
        <p>Simply put, <em>views</em> are the skin for your front-end application.</p>
        <p>Views are powered by Lit Templates, written in JavaScript, providing performant templating to build the needed content to the user. Generally speaking, views should only contain distinct static content for the specific purpose of the view.</p>
        <p>Powered by Lit, views support all Template expressions, conditionals and directives, with many directives directly available on the <code>SimpleView</code> prototype.</p>


        <h3 class="lined">Example</h3>
        <p>A simple example of the most basic view</p>
        <div class="codeblock"><pre>import {SimpleView} from 'simple-mvc-wc';

export class BasicView extends SimpleView {
  static build() {
    return \`
      &lt;h2&gt;This is a basic view&lt;/h2&gt;
      &lt;p&gt;There could be any content here.&lt;/p&gt;
    \`
  }
}
</pre></div>


        <h3 class="lined">Event Handling</h3>
        <p>Events listeners can be attached within views through Lit's '@' event handler expression.</p>
        <p>Combined with <a href="controllers">Controllers</a>, views come to life, responding to user input and executing Actions in an orgnised way.</p>
        <p>For views that use the provided <code>action</code> handler method to invoke Controllers, the application supports any number of Controllers associated with views. When an Action is triggered, the application locates a Controller able to handle the Action, and executes the handler method with all provided arguments. For example:</p>
        <div class="codeblock"><pre>...
static build() {
  return \`
    ...<i>form fields</i>...
    &lt;sc-button 
      label="Button" 
      @click=$\{this.action( 'action1', 'arg1', 'arg2' )\}
    &gt;&lt;/sc-button&gt;
  \`
}</pre></div>
        <p>In this example, when the button is clicked, the Application would look for a Controller able to handle the <em>action1</em> action, calling the found Controller handler method with additional arguments <em>arg1</em> and <em>arg2</em>.</p>


        <h3 class="lined">Composition</h3>
        <p>Views are suggested to be distinct to the required function. Therefore, to present more than a single view to the user, views may be combined or composed in two approaches to deliver a layout such as:</p>
          </p>
        <ul>
          <li>page</li>
          <ul>
            <li>header</li>
            <ul>
              <li>content1</li>
              <li>content2</li>
            </ul>
            <li>footer</li>
          </ul>
        </ul>
        <p>The two approaches to building a layout are either through explicit calls to the <code>build</code> method, or through routing and view nesting which uses the Application router to determine the heirarchy based on the location URL. Both have uses in different situations depending on the needs of the layout.</p>


        <h3 class="lined">Routing</h3>
        <p>Routing is the first method of layout provision. This is provided via a built-in router able to resolve a URL path to a heirarchy of one or more views. This router also supports path-interpolated parameters for views such as <code>/path/:id/something/:id2</code>. This allows for complex paths subviews are placed in the first ancestor view which will accept the subview.</p>
        <p>When resolving a path to a heirarchy of views, the router matches from most specific to least specific. This means that you could have one view for path segment <code>user</code>, a separate view for <code>user/:id</code>, and another separate view for <code>user/admin</code>.</p> 
        <p>For example, the outcomes following would be possible if the application had the following views registered:</p>
        <h4>Views</h4>
        <ul>
          <table>
            <thead><tr><th>Class name</th><th>Path</th><th>Child slots</th><th>Child accepts</th><th>Parent accepts</th></tr></thead>
            <tbody>
              <tr><td>Outer</td><td>path1</td><td>2</td><td><em>all</em></td><td><em>none</em></td></tr>
              <tr><td>Inner1</td><td>inner1</td><td>1</td><td>Other1</td><td>Outer</td></tr>
              <tr><td>Inner2</td><td>inner2/:id/feeds</td><td>0</td><td><em>none</em></td><td>Outer</td></tr>
              <tr><td>Other1</td><td>other1/:otherid</td><td>0</td><td><em>none</em></td><td><em>all</em></td></tr>
              <tr><td>Other2</td><td>other2</td><td>0</td><td><em>none</em></td><td>Outer</td></tr>
            </tbody>
          </table>
        </ul>
        <h4>Route resolution</h4>
        <ul>
          <h4><var>path1/inner1/other1/other2</var></h4>
          <ul><li>Outer</li><ul><li>Inner1</li><ul><li>Other1</li></ul><li>Other2</li></ul>
            <p>As Inner1 can contain Other1, Other1 chooses Inner1 as the parent, and Inner1 will accepts parent Outer. View Other2 only accepts view Outer, and is placed there.</p>
          </ul>

          <h4><var>path1/inner2/1234/feeds/other1/abcd</var></h4>
          <ul><li>Outer</li><ul><li>Inner2</li><li>Other1</li></ul>
            <p>As view Inner2 does not accept child views, Other1 chooses the next accepting parent, Outer. Sub-view Inner2 is accepted by Outer.</p>
          </ul>
        </ul>
        <p>The location to render routed views is controlled via the <code>getTargets</code> method, enabling fine-grain control over the rendering of sub-views.</p>
        <p><strong>Note</strong>, it is not possible with routing to have multiple top-level views, and all views are anticipated to be in a tree with a single root view.</p>


        <h3 class="lined">Explicit Inclusion</h3>
        <p>Explicit inclusion is the second method of layout provision. Compared to routing, explicit inclusion is a more rudimentary way to achieve different layouts. While they no not support dynamic views, and do not support view parameters directly, explicit inclusion has use cases for layout elements such as footers or other static elements where <code>rebuild</code> functionality is not required.</p>
        <p>Explicit inclusion is implemented by calling another view's <code>build</code> method within a <code>build</code> method. For example:</p>
        <div class="codeblock"><pre>class View1 extends SimpleView {
  static build() {
    return this.builder\`
      ...
    \`
  }
}

class View2 extends SimpleView {
  static build() {
    return this.builder\`
      ...
      $\{View1.build()\}
    \`
  }
}</pre></div>


        <div class="methods">
          <h3 class="lined">Methods</h3>
          ${this.buildMethods( 'SimpleView' )}
        </div>

        <p></p>
      </div>
    `
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }

  static slotCount() {
    return 2;
  }

  static canContain( _0, _1 ) {
    return true; // Allow anything
  }

  static getTargets( subs ) {
    const elems = [
      document.getElementById( "sub-main1" ),
      document.getElementById( "sub-main2" )
    ];
    const targets = [];
    for ( let i = 0; i < subs.length; i++ ) {
      targets.push( elems[i] );
    }
    return targets;
  }

}


export { ViewsView }

