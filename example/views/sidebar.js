

import { SimpleView, SimpleSidemenu, SimpleSidemenuGroup, SimpleSidemenuItem, SimpleTable } from '../../lib/index.js';
SimpleSidemenu.register();
SimpleSidemenuGroup.register();
SimpleSidemenuItem.register();
SimpleTable.register();

import { library as faLib, icon as faIcon } from "@fortawesome/fontawesome-svg-core";
import { faQuestionCircle, faPlus, faMinus, faExchangeAlt, faExclamationCircle, faBars, faTable, faList, faCogs, faBeer, faWindowMaximize, faPencilAlt, faCaretSquareDown, faBoxOpen, faCloudUploadAlt, faAsterisk, faSpinner, faBoxes, faLink, faCloudDownloadAlt, faSyncAlt, faPaintRoller, faImage, faWrench } from "@fortawesome/free-solid-svg-icons";
import { faFile, faFileImage, faEdit, faIdCard, faFolder, faListAlt, faShareSquare } from "@fortawesome/free-regular-svg-icons";

faLib.add( faTable, faListAlt, faFolder, faIdCard, faEdit, faFileImage, faFile, faQuestionCircle, faPlus, faMinus, faExchangeAlt, faExclamationCircle, faBars, faList, faCogs, faBeer, faShareSquare, faWindowMaximize, faCaretSquareDown, faPencilAlt, faBoxOpen, faCloudUploadAlt, faAsterisk, faSpinner, faBoxes, faLink, faCloudDownloadAlt, faSyncAlt, faPaintRoller, faImage, faWrench );

class SidebarView extends SimpleView {


  static build() {
    return this.builder`
      <div class="sidemenu" id="sidemenu">
        <sc-sidemenu>
          <sc-sidemenu-item label="Application" navpath="example/app">
            <div class="icon" slot="icon">${this.includeSvg(faIcon( faBoxes ).html[0])}</div>
          </sc-sidemenu-item>

          <sc-sidemenu-item label="Controllers" navpath="example/controllers">
            <div class="icon" slot="icon">${this.includeSvg(faIcon( faExchangeAlt ).html[0])}</div>
          </sc-sidemenu-item>

          <sc-sidemenu-item label="Views" navpath="example/views">
            <div class="icon" slot="icon">${this.includeSvg(faIcon( faFile ).html[0])}</div>
          </sc-sidemenu-item>

          <sc-sidemenu-item label="Themes" navpath="example/themes">
            <div class="icon" slot="icon">${this.includeSvg(faIcon( faPaintRoller ).html[0])}</div>
          </sc-sidemenu-item>

          <sc-sidemenu-group label="Models" collapsed>
            <div class="icon" slot="icon">${this.includeSvg( faIcon( faBoxOpen ).html[0])}</div>
            <sc-sidemenu-item label="Overview" navpath="example/model_classes">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faFile ).html[0])}</div>
            </sc-sidemenu-item>
            <sc-sidemenu-item label="Records" navpath="example/model_records">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faFile ).html[0])}</div>
            </sc-sidemenu-item>
            <sc-sidemenu-item label="Collections" navpath="example/model_collections">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faFolder ).html[0])}</div>
            </sc-sidemenu-item>
            <sc-sidemenu-item label="Bindable" navpath="example/model_bindable">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faLink ).html[0])}</div>
            </sc-sidemenu-item>
            <sc-sidemenu-item label="Remote" navpath="example/model_remote">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faCloudUploadAlt ).html[0])}</div>
            </sc-sidemenu-item>
            <sc-sidemenu-item label="Polling" navpath="example/model_polling">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faSyncAlt ).html[0])}</div>
            </sc-sidemenu-item>
            <sc-sidemenu-item label="Integrated" navpath="example/model_integrated">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faCloudDownloadAlt ).html[0])}</div>
            </sc-sidemenu-item>
          </sc-sidemenu-group>

          <sc-sidemenu-group label="Components" collapsed @toggled=${this.toggleIcon}>
            <div class="icon" slot="icon">${this.includeSvg(faIcon( faCogs ).html[0])}</div>
            <sc-sidemenu-item label="Busy Mask" navpath="example/busy">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faSpinner ).html[0])}</div>
            </sc-sidemenu-item>
            <sc-sidemenu-item label="Buttons" navpath="example/buttons">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faMinus ).html[0])}</div>
            </sc-sidemenu-item>
            <sc-sidemenu-item label="Inputs" navpath="example/inputs">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faEdit ).html[0])}</div>
            </sc-sidemenu-item>
            <sc-sidemenu-item label="Text Areas" navpath="example/textareas">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faPencilAlt  ).html[0])}</div>
            </sc-sidemenu-item>
            <sc-sidemenu-item label="Selects" navpath="example/selects">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faCaretSquareDown ).html[0])}</div>
            </sc-sidemenu-item>
            <sc-sidemenu-item label="Forms" navpath="example/forms">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faIdCard ).html[0])}</div>
            </sc-sidemenu-item>
            <sc-sidemenu-item label="Carousels" navpath="example/carousels">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faExchangeAlt ).html[0])}</div>
            </sc-sidemenu-item>
            <sc-sidemenu-item label="Images" navpath="example/images">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faImage ).html[0])}</div>
            </sc-sidemenu-item>
            <sc-sidemenu-item label="Tabs" navpath="example/tabs">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faFolder ).html[0])}</div>
            </sc-sidemenu-item>
            <sc-sidemenu-item label="Accordions" navpath="example/accordions">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faCaretSquareDown ).html[0])}</div>
            </sc-sidemenu-item>
            <sc-sidemenu-item label="Lists" navpath="example/lists">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faListAlt ).html[0])}</div>
            </sc-sidemenu-item>
            <sc-sidemenu-item label="Tables" navpath="example/tables">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faTable ).html[0])}</div>
            </sc-sidemenu-item>
            <sc-sidemenu-item label="Alerts" navpath="example/alerts">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faExclamationCircle ).html[0])}</div>
            </sc-sidemenu-item>
            <sc-sidemenu-item label="Menus" navpath="example/menus">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faBars ).html[0])}</div>
            </sc-sidemenu-item>
            <sc-sidemenu-item label="Popups" navpath="example/popups">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faWindowMaximize ).html[0])}</div>
            </sc-sidemenu-item>
            <sc-sidemenu-item label="Sidemenus" navpath="example/sidemenus">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faList ).html[0])}</div>
            </sc-sidemenu-item>
            <sc-sidemenu-item label="Slideouts" navpath="example/slideouts">
              <div class="icon" slot="icon">${this.includeSvg(faIcon( faShareSquare ).html[0])}</div>
            </sc-sidemenu-item>
          </sc-sidemenu-group>


          <sc-sidemenu-group label="Utils" collapsed @toggled=${this.toggleIcon}>
            <div class="icon" slot="icon">${this.includeSvg(faIcon( faWrench ).html[0])}</div>
            <sc-sidemenu-item label="QueryTranslate" navpath="example/util_querytranslate">
            </sc-sidemenu-item>
          </sc-sidemenu-group>
        </sc-sidemenu>
      </div>
    `
  }

  static toggleIcon( e ) {
    var icons = e.target.getElementsByClassName( 'expandicon' )[0];
    var collapsed = e.target.collapsed;
    // Protect for edge firing events before all DOM is loaded
    if ( ! icons )
      return;
    // This is the after-toggle state
    if ( collapsed ) {
      icons.getElementsByClassName( 'collapsed' )[0].style.display = "unset";
      icons.getElementsByClassName( 'expanded' )[0].style.display = "none";
    }
    else {
      icons.getElementsByClassName( 'expanded' )[0].style.display = "unset";
      icons.getElementsByClassName( 'collapsed' )[0].style.display = "none";
    }
  }


  static connect() {
    var sub = document.getElementById( "sidemenu" );
    sub.addEventListener( 'toggle-thingo', e => this.toggleIcon( e ) );
  }


  static disconnect() {
  }

}


export default SidebarView;

