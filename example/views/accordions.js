
import DocParser from '../util/docparser.js';
import { SimpleAccordionGroup, SimpleAccordion, SimpleInput, SimpleButton, SimpleTable } from '../../lib/index.js';
import { ExampledView } from './example.js';
SimpleInput.register();
SimpleButton.register();
SimpleAccordionGroup.register();
SimpleAccordion.register();
SimpleTable.register();

import rawcode from '!raw-loader!../../lib/components/accordions.js';

let __docs = undefined;

const eventNames = {
  SimpleAccordion: [ 'before-open', 'before-close', 'after-open', 'after-close' ]
};

const methodNames = {
  SimpleAccordionGroup: [ 'openClose', 'openCloseByIndex' ],
  SimpleAccordion: [ 'toggle' ]
};


class AccordionsView extends ExampledView {

  static title() {
    return "Accordions";
  }

  static propertyElements() {
    return [ 'sc-accordion-group', 'sc-accordion' ];
  }
  static propertyTables() {
    return [ 'group-properties', 'accordion-properties' ]
  }

  static varElements() {
    return [ 'sc-accordion' ];
  }

  static varTables() {
    return [ 'accordion-styles' ]
  }


  static methodNames( className ) {
    return methodNames[ className ];
  }

  static eventNames( className ) {
    return eventNames[ className ];
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }

  static build() {
    return this.builder`
      <div id="accordions-page">

        <h2>Accordions</h2>

        <h3 class="lined">Description</h3>
        <p>The <code>&lt;sc-accordion-group&gt;</code> and <code>&lt;sc-accordion&gt;</code> components are user interactable controls for displaying content in distinct sections that can collapse or expand as needed.</p>
        <p>Note, while a <code>&lt;sc-accordion-group&gt;</code> should have one or more <code>&lt;sc-accordion&gt;</code> components, <code>&lt;sc-accordion&gt;</code> components do not require a <code>&lt;sc-accordion-group&gt;</code> component and can be used independently.</p>

        <h3 class="lined">Try it out</h3>
        <div style="display: flex; flex-direction: row;">

          <div>
            <p>Accordion options:</p>
            <sc-input class="block" name="count" type="number" min=1 max=5 required label="Number of accordions" value=1></sc-input>
            <sc-input class="block" name="fillheight" type="toggle" valuetype="boolean" label="Fill height?"></sc-input>
            <sc-input class="block" name="requireone" type="toggle" valuetype="boolean" label="Require one open?"></sc-input>
            <sc-input class="block" name="multiple"   type="toggle" valuetype="boolean" label="Multiple?"></sc-input>
            <sc-input class="block" name="noanimate"  type="toggle" valuetype="boolean" label="No animation?"></sc-input>

            <span>Bar position:</span>
            <sc-input style="margin-bottom: -15px;" class="block" name="barposition" type="radio" label="Top" checked value="top" grouped></sc-input>
            <sc-input name="barposition" type="radio" label="Bottom" grouped value="bottom"></sc-input>

            <sc-button class="block" label="Generate" @click=${this.generate}></sc-button>
          </div>

          <div style="width: 100px;"></div>

          <div>
            <p>Accordion area:</p>
            <div id="accordion-area" class="bordered" style="width: 600px; height: 500px; position: relative; overflow: hidden;">
            </div>
          </div>
        </div>

        <div class="properties">
          <h3 class="lined">Properties</h3>
          <h4><code>&lt;sc-accordion-group&gt;</code></h4>
          <sc-table
              id="group-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>

        <div class="properties">
          <h4><code>&lt;sc-accordion&gt;</code></h4>
          <sc-table
              id="accordion-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>


        <div class="events">
          <h3 class="lined">Events</h3>
          <p>The <code>sc-accordion</code> component emits the following bubbling events:</p>
          ${this.buildEvents( 'SimpleAccordion' )}
        </div>


        <div class="methods">
          <h3 class="lined">Methods</h3>
          <h4><code>&lt;sc-accordion-group&gt;</code></h4>
          ${this.buildMethods( 'SimpleAccordionGroup' )}

          <h4><code>&lt;sc-accordion&gt;</code></h4>
          ${this.buildMethods( 'SimpleAccordion' )}
        </div>


        <div class="variables">
          <h3 class="lined">CSS Variables</h3>
          <p></p>
          <sc-table
              id="accordion-styles"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.varFields)}
          >
          </sc-table>
        </div>


        <div class="examples">
          <h3 class="lined">Examples</h3>
          <h4>Multi-step form</h4>
          <div class="example" style="height:350px; max-width: 800px;">
            <sc-accordion-group fillheight requireone bardisable>

              <sc-accordion label="Step One" @before-close=${(ev) => {if ( ! ev.target.querySelector( 'sc-input' ).validate()) ev.preventDefault()}}>
                <h3>Step One</h3>
                <p>Input in the following field is required being able to transition to the next step.</p>
                <sc-input name="input1" type="text" placeholder="Enter text" required autovalidate errormessage="Enter a value"></sc-input>
                <sc-button label="next" @click=${(ev) => { ev.target.closest('sc-accordion-group').openCloseByIndex( 1, true )}}></sc-button>
              </sc-accordion>

              <sc-accordion label="Step Two" @before-close=${(ev) => {if ( ! ev.target.querySelector( 'sc-input' ).validate()) ev.preventDefault()}}>
                <h3>Step Two</h3>
                <p>Input in the following field is required being able to transition to the next step.</p>
                <sc-input name="input2" label="Option" type="toggle" placeholder="Turn this on" required autovalidate errormessage="Must select this option"></sc-input>
                <sc-button label="next" @click=${(ev) => { ev.target.closest('sc-accordion-group').openCloseByIndex( 2, true )}}></sc-button>
              </sc-accordion>

              <sc-accordion label="Step Three">
                <h3>Step Three</h3>
                <p>As this example demonstrates, constructing a multi-step form is easy</p>
              </sc-accordion>
            </sc-accordion-group>
          </div>
        </div>


      </div>
    `
  }

  static generate() {
    var grp, kids = [];
    var container   = document.getElementById( "accordion-area" );
    var count       = document.querySelector( 'sc-input[name="count"]' );
    var pos         = document.querySelector( 'sc-input[name=barposition]' );
    var fill        = document.querySelector( 'sc-input[name=fillheight]' );
    var reqone      = document.querySelector( 'sc-input[name=requireone]' );
    var multiple    = document.querySelector( 'sc-input[name=multiple]' );
    var noanim      = document.querySelector( 'sc-input[name=noanimate]' );
    var labels = [ 'First', 'Second', 'Third', 'Fourth', 'Fifth' ];

    container.innerHTML = '';

    for (let i = 0; i < count.value; i++ ) {
      var p = document.createElement( 'p' );
      p.textContent = "This is the " + labels[i].toLowerCase() + " accordion.";
      var acc = document.createElement( 'sc-accordion' );
      acc.label = labels[ i ];
      acc.barposition = pos.value;
      if ( noanim.value )
        acc.style.setProperty( '--accordion-animation-duration', 0 );
      acc.appendChild( p );
      kids.push( acc );
    }

    grp = document.createElement( 'sc-accordion-group' );
    grp.multiple = multiple.value;
    grp.requireone = reqone.value;
    grp.fillheight = fill.value;

    for ( let kid of kids )
      grp.appendChild( kid );
    container.appendChild( grp );
  }

}

export { AccordionsView }

