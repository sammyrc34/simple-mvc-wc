
import DocParser from '../util/docparser.js';
import { SimpleMenuTop, SimpleMenuGroup, SimpleMenuItem, SimpleTable } from '../../lib/index.js';
import { ExampledView } from './example.js';
SimpleMenuTop.register();
SimpleMenuGroup.register();
SimpleMenuItem.register();
SimpleTable.register();

import rawcode from '!raw-loader!../../lib/components/menu.js';

let __docs = undefined;

const methodNames = {
  'SimpleMenuTop': [ 'openAt', 'toggleOpen',  'mode',  'hideAll'  ],
  'SimpleMenuGroup': [ 'toggleOpen' ],
  'SimpleMenuItem': [ 'closeMenu' ]
};

const example = `<sc-menu-top topstyle="button" groupstyle="block">
  <sc-menu-item label="First"></sc-menu-item>
  <sc-menu-item label="Second"></sc-menu-item>
  <sc-menu-group label="More">
    <sc-menu-item label="Third"></sc-menu-item>
    <sc-menu-item label="Fourth"></sc-menu-item>
    <sc-menu-group label="Even More">
      <sc-menu-item label="Fifth"></sc-menu-item>
    </sc-menu-group>
  </sc-menu-group>
</sc-menu-top>
`;

class MenusView extends ExampledView {

  static title() {
    return "Menus";
  }

  static propertyElements() {
    return [ 'sc-menu-top', 'sc-menu-group', 'sc-menu-item' ];
  }

  static propertyTables() {
    return [ 'menu-top-properties', 'menu-group-properties',  'menu-item-properties' ]
  }

  static varElements() {
    return [ 'sc-menu-top' ];
  }

  static varTables() {
    return [ 'menu-styles' ]
  }

  static methodNames( className ) {
    return methodNames[ className ];
  }

  static eventNames() {
    return [ 'click' ];
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }


  static build() {
    return this.builder`
      <div id="menus-page">
        <h2>Menus</h2>
        <div>
          <h3 class="lined">Description</h3>
          <p>The <code>&lt;sc-menu-top&gt;</code>, <code>&lt;sc-menu-group&gt;</code> and <code>&lt;sc-menu-item&gt;</code> elements are user-interactable menu controls that can be used in a variety of contexts and places.</p>
          <p>The <code>&lt;sc-menu-top&gt;</code> element is a flexible menu that can display as a navigation bar, or button, or even just a popup. It is designed to be the outer-most menu component in the case of multi-level menus.</p>
          <p>The <code>&lt;sc-menu-group&gt;</code> element is a menu item that contains one or more child menu items. It adapts to the form of the containing &lt;sc-menu-top&gt; element automatically.</p>
          <p>The <code>&lt;sc-menu-item&gt;</code> element is the individual menu item for the user to indicate some form of action.</p>
          <h3 class="lined">Try it out</h3>
          ${super.build( example )}
        </div>

        <div class="properties">
          <h3 class="lined">Properties</h3>
          <h4><code>&lt;sc-menu-top&gt;</code></h4>
          <sc-table
              id="menu-top-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>

        <div class="properties">
          <h4><code>&lt;sc-menu-group&gt;</code></h4>
          <sc-table
              id="menu-group-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>

        <div class="properties">
          <h4><code>&lt;sc-menu-item&gt;</code></h4>
          <sc-table
              id="menu-item-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>


        <div class="events">
          <h3 class="lined">Events</h3>

          <h4><code>&lt;sc-menu-top&gt;</code></h4>
          ${this.buildEvents( 'SimpleMenuTop' )}

          <h4><code>&lt;sc-menu-group&gt;</code></h4>
          ${this.buildEvents( 'SimpleMenuGroup' )}

          <h4><code>&lt;sc-menu-item&gt;</code></h4>
          ${this.buildEvents( 'SimpleMenuItem' )}
        </div>


        <div class="methods">
          <h3 class="lined">Methods</h3>
          <h4><code>&lt;sc-menu-top&gt;</code></h4>
          ${this.buildMethods( 'SimpleMenuTop' )}

          <h4><code>&lt;sc-menu-group&gt;</code></h4>
          ${this.buildMethods( 'SimpleMenuGroup' )}

          <h4><code>&lt;sc-menu-item&gt;</code></h4>
          ${this.buildMethods( 'SimpleMenuItem' )}
        </div>


        <div class="variables">
          <h3 class="lined">CSS Variables</h3>
          <p></p>
          <sc-table
              id="menu-styles"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.varFields)}
          >
          </sc-table>
        </div>


        <div id="examples" class="examples" @click=${this.chooseExample.bind( this )}>
          <h3 class="lined">Examples</h3>
          <p>Select and try out any example:<sc-button id="choose-button" type="toggle" label="Choose example..." @click=${this.setChooseMode}></sc-button></p>


          <div class="example">
            <h4>Nav menu</h4>
            <sc-menu-top navmenu topstyle="bar">
              <sc-menu-item style="--menu-group-min-width: 110px;" label="Menus page" navpath="example/menus"></sc-menu-item>
              <sc-menu-item style="--menu-group-min-width: 110px;" label="Buttons page" navpath="example/buttons"></sc-menu-item>
              <sc-menu-item style="--menu-group-min-width: 110px;" label="Carousels page" navpath="example/carousels"></sc-menu-item>
            </sc-menu-top>
          </div>


          <div class="example">
            <h4>Top menu</h4>
            <sc-menu-top topstyle="bar" slot="left">
              <sc-menu-group label="File">
                <sc-menu-item label="Open"></sc-menu-item>
                <sc-menu-item label="Save"></sc-menu-item>
                <sc-menu-item label="Close" disabled></sc-menu-item>
                <sc-menu-group label="Recent Files">
                  <sc-menu-item label="One"></sc-menu-item>
                  <sc-menu-item label="Two"></sc-menu-item>
                  <sc-menu-item label="Three"></sc-menu-item>
                </sc-menu-group>
              </sc-menu-group>
              <sc-menu-group label="Edit">
                <sc-menu-item label="Copy"></sc-menu-item>
                <sc-menu-item label="Cut"></sc-menu-item>
                <sc-menu-item label="Paste"></sc-menu-item>
                <sc-menu-group label="More">
                  <sc-menu-item label="One"></sc-menu-item>
                  <sc-menu-item label="Two"></sc-menu-item>
                  <sc-menu-item label="Three"></sc-menu-item>
                  <sc-menu-group label="Even More">
                    <sc-menu-item label="Four"></sc-menu-item>
                    <sc-menu-item label="Five"></sc-menu-item>
                    <sc-menu-item label="Six"></sc-menu-item>
                  </sc-menu-group>
                </sc-menu-group>
              </sc-menu-group>
              <sc-menu-group label="Help">
                <sc-menu-item label="Contents"></sc-menu-item>
                <sc-menu-item label="About"></sc-menu-item>
              </sc-menu-group>
              <sc-menu-item label="Site Map"></sc-menu-item>
            </sc-menu-top>
          </div>


          <div class="example">
            <h4>Auto style menu</h4>
            <sc-menu-top>
              <sc-menu-group label="File">
                <sc-menu-item label="Open"></sc-menu-item>
                <sc-menu-item label="Save"></sc-menu-item>
                <sc-menu-item label="Close"></sc-menu-item>
                <sc-menu-group label="Recent Files">
                  <sc-menu-item label="One"></sc-menu-item>
                  <sc-menu-item label="Two"></sc-menu-item>
                  <sc-menu-item label="Three"></sc-menu-item>
                </sc-menu-group>
              </sc-menu-group>
              <sc-menu-group label="Edit">
                <sc-menu-item label="Copy"></sc-menu-item>
                <sc-menu-item label="Cut"></sc-menu-item>
                <sc-menu-item label="Paste"></sc-menu-item>
                <sc-menu-group label="More">
                  <sc-menu-item label="One"></sc-menu-item>
                  <sc-menu-item label="Two"></sc-menu-item>
                  <sc-menu-item label="Three"></sc-menu-item>
                  <sc-menu-group label="Even More">
                    <sc-menu-item label="Four"></sc-menu-item>
                    <sc-menu-item label="Five"></sc-menu-item>
                    <sc-menu-item label="Six"></sc-menu-item>
                  </sc-menu-group>
                </sc-menu-group>
              </sc-menu-group>
              <sc-menu-group label="Help">
              <sc-menu-item label="Contents"></sc-menu-item>
              <sc-menu-item label="About"></sc-menu-item>
              </sc-menu-group>
            </sc-menu-top>
          </div>


          <div class="example">
            <h4>Button menu</h4>
            <sc-menu-top topstyle="button" slot="left">
              <p slot="barcontent">Anything else could be here</p>
              <sc-menu-item label="First"></sc-menu-item>
              <sc-menu-item label="Second"></sc-menu-item>
              <sc-menu-item label="Third"></sc-menu-item>
              <sc-menu-group label="More">
                <sc-menu-item label="Fourth"></sc-menu-item>
                <sc-menu-item label="Fifth"></sc-menu-item>
              </sc-menu-group>
            </sc-menu-top>
          </div>


          <div class="example">
            <h4>Button menu with image</h4>
            <sc-menu-top topstyle="button" slot="left">
              <style>
                img {
                  padding: 5px;
                }
                img:hover {
                  backdrop-filter: brightness(110%);
                }
              </style>
              <img src="/images/profile.png" width="30" slot="button"></img>
              <sc-menu-item label="First"></sc-menu-item>
              <sc-menu-item label="Second"></sc-menu-item>
              <sc-menu-item label="Third"></sc-menu-item>
              <sc-menu-group label="More">
                <sc-menu-item label="Fourth"></sc-menu-item>
                <sc-menu-item label="Fifth"></sc-menu-item>
              </sc-menu-group>
            </sc-menu-top>
          </div>


          <div class="example">
            <h4>Block button menu</h4>
            <sc-menu-top topstyle="button" groupstyle="block">
              <sc-menu-item label="First"></sc-menu-item>
              <sc-menu-item label="Second"></sc-menu-item>
              <sc-menu-item label="Third"></sc-menu-item>
              <sc-menu-group label="More">
                <sc-menu-item label="Fourth"></sc-menu-item>
                <sc-menu-item label="Fifth"></sc-menu-item>
              </sc-menu-group>
            </sc-menu-top>
          </div>



          <div class="example" style="position: static;">
            <h4>Context menu</h4>
            <div>
              <p oncontextmenu="(function(ev){ev.preventDefault(); ev.target.parentElement.querySelector('sc-menu-top').openAt(ev.pageX, ev.pageY);})(event);">Right click for context menu</p>
              <sc-menu-top topstyle="popup" id="context-menu">
                <sc-menu-item label="First"></sc-menu-item>
                <sc-menu-item label="Second"></sc-menu-item>
                <sc-menu-item label="Third"></sc-menu-item>
                <sc-menu-group label="More">
                  <sc-menu-item label="Fourth"></sc-menu-item>
                  <sc-menu-item label="Fifth"></sc-menu-item>
                </sc-menu-group>
              </sc-menu-top>
            </div>
          </div>

        </div>
      </div>
    `
  }


  static showContext(ev) {
    var menu = document.querySelector( '#context-menu' );
    ev.preventDefault();
    menu.openAt( ev.pageX, ev.pageY );
  }


}


export {
  MenusView
}

