
import DocParser from '../util/docparser.js';
import { SimpleBusyMask, SimpleTable } from '../../lib/index.js';
import { ExampledView } from './example.js';
SimpleBusyMask.register();
SimpleTable.register();


import rawcode from '!raw-loader!../../lib/components/popup.js';

let __docs = undefined;


class BusyMaskView extends ExampledView {

  static title() {
    return "General";
  }

  static propertyElements() {
    return [ 'sc-busy' ];
  }

  static propertyTables() {
    return [ 'busy-properties' ]
  }

  static varElements() {
    return [ 'sc-busy' ];
  }

  static varTables() {
    return [ 'busy-styles' ]
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }

  static build() {
    return this.builder`
      <div id="generals-page">
        <h2>Busy mask</h2>
        <div>
          <h3 class="lined">Description</h3>
          <p>The <code>&lt;sc-busy&gt;</code> component is a base and individual element to contain a variety of content, providing an on-demand visual busy overlay.</p>
        </div>


        <h3 class="lined">Try it out</h3>
        <div class="busymask-tryit">

          <div @after-change=${() => this.updateMask()}>
            <h3>Mask options:</h3>
            <sc-input class="block" id="busy-toggle" name="busy" type="toggle" valuetype="boolean" label="Busy"></sc-input>
            <sc-input class="block" id="fill-toggle" name="fill" type="toggle" valuetype="boolean" label="Fill container"></sc-input>
            <sc-input class="block" id="scroll-toggle" name="scroll" type="toggle" valuetype="boolean" label="Enable scroll"></sc-input>
          </div>

          <div id="busymask-area">
            <h3>Mask area:</h3>
            <div class="bordered">
              <sc-busy id="busymask-example">
                <div class="flex-col-center" style="border: 1px dashed grey">
                  <div style="width: 50px; height: 50px; margin: 5px; background-color: red;"></div>
                  <div style="width: 50px; height: 50px; margin: 5px; background-color: green;"></div>
                  <div style="width: 50px; height: 50px; margin: 5px; background-color: blue;"></div>
                  <p>Some text content with an image below</p>
                  <div>
                    <img style="height: auto; width: 200px;" src="/images/bees.jpg"></img>
                  </div>
                </div>
              </sc-busy>
            </div>
          </div>
        </div>


        <div class="properties">
          <h3 class="lined">Properties</h3>
          <h4><code>&lt;sc-busy&gt;</code></h4>
          <sc-table
              id="busy-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>


        <div class="variables">
          <h3 class="lined">CSS Variables</h3>
          <p></p>
          <sc-table
              id="busy-styles"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.varFields)}
          >
          </sc-table>
        </div>


      </div>
    `
  }

  static updateMask( ) {

    const mask = document.getElementById( 'busymask-example' );
    const busyable = document.getElementById( 'busy-toggle' );
    const fillable = document.getElementById( 'fill-toggle' );
    const scrollable = document.getElementById( 'scroll-toggle' );

    mask.busy = busyable.value;
    mask.fill = fillable.value;
    mask.scroll = scrollable.value;
  }


}


export {
  BusyMaskView
}


