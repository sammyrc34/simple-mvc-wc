

import DocParser from '../util/docparser.js';
import { ExampledView } from './example.js';


import rawcode from '!raw-loader!../../lib/models/record.js';

let __docs = undefined;



class ModelClassesView extends ExampledView {

  static methodNames( ) {
    return [ 'constructor', 'setup', 'clear' , 'length', 'fetch', 'hasRecord', 'fetchOne', 'getRecord', 'getRecords', 'add', 'remove', 'update', 'check', 'parse'  ];
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }



  static build() {
    return this.builder`
      <div id="model-classes-page">
        <h2>Model Classes</h2>
        <h3 class="lined">Description</h3>
        <p>There are several classes providing data model functionality provided. The base classes are <code>BasicRecord</code> for record type models, and <code>BasicCollection</code> for multi-record type models.</p>
        <p>Designed to augment the two base classes, mixin classes are provided to add specific functionality:</p>

        <h4>BindableModel</h4>
        <p>The <a href="model_bindable"><code>BindableModel</code></a> mixin class adds the ability for components and more to bind to data models, providing a functional and event-based interface to respond to data and model state changes.</p>

        <h4>RemoteModel</h4>
        <p>The <a href="model_remote"><code>RemoteModel</code></a> mixin class that adds functionality for a fully-cached remotely authoritative data source. Being fully-cached, changes made are local until specifically perfoming a <code>persist</code></p>

        <h4>PollingModel</h4>
        <p>The <a href="model_polling"><code>PollingModel</code></a> mixin class that adds functionality to the <code>RemoteModel</code> for periodic polling of a remote data source to maintain consistency.</p>

        <h4>IntegratedModel</h4>
        <p>The <a href="model_integrated"><code>IntegratedModel</code></a> mixin class adds functionality to the <code>RemoteModel</code> for a partially-cached, remotely filtered and sorted data source with optional WebSocket based mechanism to maintain consistency.</p>


        <h3 class="lined">Classes</h3>

        <p>The following composed record based classes are available:</p>
        <ul class="typedef">
          <li><div><var>SimpleRecord</var></div> - Composed from BindableModel on BasicRecord</li>
          <li><div><var>SimpleRemoteRecord</var></div> - Composed from BindableModel on RemoteModel on BasicRecord</li>
          <li><div><var>SimplePollingRecord</var></div> - Composed from BindableModel on PollingModel on RemoteModel on BasicRecord</li>
        </ul>


        <p>The following composed collection based classes are available:</p>

        <ul class="typedef">
          <li><div><var>SimpleCollection</var></div> - Composed from BindableModel on BasicCollection</li>
          <li><div><var>SimpleRemoteCollection</var></div> - Composed from BindableModel on RemoteModel on BasicCollection</li>
          <li><div><var>SimpleIntegratedCollection</var></div> - Composed from BindableModel on IntegratedModel on RemoteModel on BasicCollection</li>
          <li><div><var>SimplePollingCollection</var></div> - Composed from BindableModel on PollingModel on RemoteModel on BasicCollection</li>
        <ul>


      </div>
    `
  }


}

export { ModelClassesView };

