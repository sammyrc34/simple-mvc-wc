
import DocParser from '../util/docparser.js';
import { SimpleButton, SimpleInput, SimpleSelect, SimpleSelectOption, SimpleAlerts, SimpleTable } from '../../lib/index.js';
import { ExampledView } from './example.js';
SimpleAlerts.register();
SimpleButton.register();
SimpleInput.register();
SimpleSelect.register();
SimpleSelectOption.register();
SimpleTable.register();

import rawcode from '!raw-loader!../../lib/components/alerts.js';

let __docs = undefined;

const methodNames = {
  'SimpleAlert': [ 'collapse', 'close' ],
  'SimpleAlerts': [ 'addAlert' ]
};

class AlertsView extends ExampledView {

  static title() {
    return "Alerts";
  }

  static propertyElements() {
    return [ 'sc-alerts', 'sc-alert' ];
  }

  static propertyTables() {
    return [ 'alerts-properties', 'alert-properties' ];
  }

  static varElements() {
    return [ 'sc-alert' ];
  }

  static varTables() {
    return [ 'alert-styles' ]
  }

  static methodNames( className ) {
    return methodNames[ className ];
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }

  static build() {
    return this.builder`
      <div id="alerts-page">

        <h2>Alerts</h2>
        <div>
          <h3 class="lined">Description</h3>
          <p>The <code>&lt;sc-alerts&gt;</code> component provides a container and a simple interface for alert creation.</p>
          <p>The <code>&lt;sc-alert&gt;</code> component is an informative element to advise or alert the user to something. It has styles for basic types, including information, warnings, errors, success messages and tasks.</p>
        </div>

        <h3 class="lined">Try it out</h3>
        <div class="alert-tryit">
          <div id="alert-buttons">
            <h3>Add an alert:</h3>
            <sc-select name="type" label="Alert type" class="block">
              <sc-select-option value="success" label="Success"></sc-select-option>
              <sc-select-option value="info" selected label="Info"></sc-select-option>
              <sc-select-option value="task" label="Task"></sc-select-option>
              <sc-select-option value="warning" label="Warning"></sc-select-option>
              <sc-select-option value="error" label="Error"></sc-select-option>
            </sc-select>
            <sc-select name="duration" label="Duration" class="block">
              <sc-select-option value="" label="Default" selected></sc-select-option>
              <sc-select-option value="0" label="Static"></sc-select-option>
              <sc-select-option value="3000" label="3 seconds"></sc-select-option>
              <sc-select-option value="10000" label="10 seconds"></sc-select-option>
            </sc-select>
            <sc-select name="revealstyle" label="Reveal Style" class="block">
              <sc-select-option value="popin" label="Pop In"></sc-select-option>
              <sc-select-option value="slidein" label="Slide"></sc-select-option>
              <sc-select-option value="popflash" label="Pop & Flash"></sc-select-option>
              <sc-select-option value="flash" label="Flash"></sc-select-option>
            </sc-select>
            <sc-select name="hidestyle" label="Hide Style" class="block">
              <sc-select-option value="popout" label="Pop Out"></sc-select-option>
              <sc-select-option value="rollup" label="Roll Up"></sc-select-option>
              <sc-select-option value="slideleft" label="Slide Left"></sc-select-option>
              <sc-select-option value="slideright" label="Slide Right"></sc-select-option>
            </sc-select>
            <sc-select name="iconposition" label="Icon Position" class="block">
              <sc-select-option value="top" label="Top"></sc-select-option>
              <sc-select-option value="middle" label="Middle" selected></sc-select-option>
            </sc-select>
            <sc-input name="showtitle" type="toggle" label="With title?" valuetype="boolean"></sc-input>
            <sc-button id="make-alert" label="Show alert" @click=${this.showAlert}></sc-button>
          </div>
          <div id="alert-area">
            <h3>Alert area:</h3>
            <sc-alerts id="example-alerts"></sc-alerts>
          </div>
        </div>

        <div class="properties">
          <h3 class="lined">Properties</h3>
          <h4><code>&lt;sc-alerts&gt;</code></h4>
          <sc-table
              id="alerts-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>

        <div class="properties">
          <h4><code>&lt;sc-alert&gt;</code></h4>
          <sc-table
              id="alert-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>


        <div class="methods">
          <h3 class="lined">Methods</h3>
          <h4><code>&lt;sc-alerts&gt;</code></h4>
          ${this.buildMethods( 'SimpleAlerts' )}

          <h4><code>&lt;sc-alert&gt;</code></h4>
          ${this.buildMethods( 'SimpleAlert' )}
        </div>


        <div class="variables">
          <h3 class="lined">CSS Variables</h3>
          <p></p>
          <sc-table
              id="alert-styles"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.varFields)}
          >
          </sc-table>
        </div>

      </div>
    `
  }

  static showAlert() {
    const container = document.getElementById( 'example-alerts' );
    const type = document.querySelector( 'sc-select[name=type]' ).value;
    const duration = document.querySelector( 'sc-select[name=duration]' ).value;
    const showstyle = document.querySelector( 'sc-select[name=revealstyle]' ).value;
    const hidestyle = document.querySelector( 'sc-select[name=hidestyle]' ).value;
    const iconpos = document.querySelector( 'sc-select[name=iconposition]' ).value;
    let title = document.querySelector( 'sc-input[name=showtitle]' ).value;

    if ( title )
      title = type[0].toUpperCase() + type.substring( 1 ) + ' Alert';
    else
      title = undefined;

    container.addAlert( type, `A simple test ${type ? type : 'info'} alert`, title, {
      'duration': duration,
      'showstyle': showstyle,
      'hidestyle': hidestyle,
      'iconposition': iconpos
    } );

  }


}


export { AlertsView }

