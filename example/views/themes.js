

import { SimpleView } from '../../lib/index.js';

class ThemesView extends SimpleView {

  static title() {
    return "Themes";
  }


  static currentStyles() {
    const styles = document.head.querySelectorAll( 'style' );
    const htmlRe = new RegExp( /html {(.*?)}/, 'ms' );
    const last = Array.from( styles ).at( -1 );
    let themes, match;

    if ( match = last.innerText.match( htmlRe ) ) {
      themes = match[1];
    }

    return this.builder`${themes}`;
  }


  static build() {
    return this.builder`
      <div id="themes-page">
        <h2>Themes</h2>
        <div>
          <h3 class="lined">Description</h3>
          <p>Each graphical or user interactive component in this library uses CSS variables for sizing and styling where possible. To provide a consistent style between components, each component's CSS rules should be derived or taken from a base set of CSS variables. This base set of CSS variables is therefore effectively a theme. Being CSS variables, these are also easy to set and manipulate per component or per scope in the DOM.</p>

          <p>Changing themes, for example between light and dark styles, can be as simple as applying a class to your document body such as the <em>contrast</em> toggle in the header of this example application.</p>


          <p>For reference, this is the current theme for this example application:</p>
          <div class="codeblock"><pre>${this.currentStyles()}</pre></div>
          <p></p>


        </div>
      </div>
    `
  }

}


export { ThemesView };

