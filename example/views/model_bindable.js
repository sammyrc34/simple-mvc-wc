

import DocParser from '../util/docparser.js';
import { ExampledView } from './example.js';


import rawcode from '!raw-loader!../../lib/models/bindable.js';

let __docs = undefined;



class ModelBindableView extends ExampledView {

  static methodNames( ) {
    return [ 'constructor', 'setup', 'addEventListener', 'dispatchEvent', 'removeEventListener', 'isBound', 'dataChanged', 'lastStatus', 'bindMulti', 'bindSingle', 'unbind', 'fetch', 'reset', 'rollback', 'add', 'update', 'remove', 'persist', 'changeSingleBindId', 'handleDisconnect', 'setRecordError', 'clearRecordError' ];
  }

  static eventNames( ) {
    return [ 'status-cleared', 'status-pending', 'status-online', 'status-enabled', 'status-disabled', 'status-failed', 'status-complete', 'status-changed', 'data-loaded', 'data-added', 'data-updated', 'data-removed', 'data-changed', 'data-rebind' ];
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }



  static build() {
    return this.builder`
      <div id="model-bindable-page">
        <h2>Bindable Mixin</h2>
        <h3 class="lined">Description</h3>
        <p>The <code>BindableModel</code> mixin class adds the ability for components to bind to the data model, with a simple interface to respond to data and model state changes. In addition to the binding functionality, this mixin implements EventTarget functionality for the events below.</p>


        <div class="events">
          <h3 class="lined">Events</h3>
          <p></p>
          ${this.buildEvents( 'BindableModel' )}
        </div>


        <div class="methods">
          <h3 class="lined">Methods</h3>
          <p></p>
          ${this.buildMethods( 'BindableModel' )}
        </div>


      </div>
    `
  }


}

export { ModelBindableView };

