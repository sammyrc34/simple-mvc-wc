

import DocParser from '../util/docparser.js';
import { ExampledView } from './example.js';

import rawcode from '!raw-loader!../../lib/controller.js';

let __docs = undefined;


class ControllersView extends ExampledView {

  static title() {
    return "Controllers";
  }

  static methodNames( ) {
    return [ 'constructor', 'actionMap', 'doAction' ];
  }

  static build() {

    return this.builder`
      <div id="controllers-page">
        <h2>Controllers</h2>
        <h3 class="lined">Description</h3>
        <p>Controllers essentially contain the logic and custom functionality of your application.</p>
        <p>Controllers can be used with one or more Views, associated when registering in the application. Once registered, Controllers provide a mapping of action names to Controller method names. Additionally, since multiple controllers can be associated with a single View, it is recommended that action names be unique.</p>
        <p>Note, the association between Controllers and Views is loose, allowing for plug-in style controllers rather than requiring a firm structure or association.</p>

        <h3 class="lined">Example</h3>
        <p></p>
        <div class="codeblock"><pre>import { SimpleController } from 'simple-mvc-wc';

export class BasicController extends SimpleController {

  static actionMap() {
    return { 
      'action1': 'method1',
      'action2': 'method2'
    }
  }

  async method1( event ) => {
    ...
  }

  method2( event, view, action, otherArg1, otherArg2 ) {
    ...
  }
}</pre></div>


        <div class="methods">
          <h3 class="lined">Methods</h3>
          ${this.buildMethods( 'SimpleController' )}
        </div>

      </div>
    `
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }
}


export { ControllersView };



