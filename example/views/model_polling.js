

import DocParser from '../util/docparser.js';
import { ExampledView } from './example.js';


import rawcode from '!raw-loader!../../lib/models/polling.js';

let __docs = undefined;



class ModelPollingView extends ExampledView {

  static methodNames( ) {
    return [ 'constructor', 'setup', 'load', 'clear', 'poll', 'pollBatch', 'pollEach', 'stopPolling', 'startPolling'  ];
  }

  static typeNames() {
    return [ 'LoadInfoType', 'PersistInfoType' ] ;
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }



  static build() {
    return this.builder`
      <div id="model-polling-page">
        <h2>Remote Models</h2>
        <h3 class="lined">Description</h3>
        <p>The <code>PollingModel</code> class is a mixin, augmenting the <code>RemoteModel</code> model to add functionality for polling the data source for changes. It has two main modes of operation being 'batch' and 'each'. Batch polling uses a timestamp associated with a load request for comparison, re-loadingall records when changed. The <em>each</em> mode uses a version field in each record to periodically ask the data source to check for changes.</p>
        <p>Each mode of operation has use cases with collection and single record based models. By default, <em>batch</em> is used for collection based models and <em>each</em> is used for record based models.</p>
        
        <h3 class="lined">Batch Polling</h3>
        <p>Batch polling is the simpler method of polling with the aim to maintain data consistency with the data source. For this to function, a <var>stamp</var> property is required within each response from the data source per the <a href="model_remote"><code>RemoteModel</code></a> class. This <code>stamp</code> property can contain any comparable value, although is commonly a number. Once loaded, the model periodically polls the data source. When a change is detected in this <code>stamp</code> value, the model identifies that an update is required and attempts to re-load the model from the data source.</p>

        <p><strong>Note 1:</strong> to avoid potential race conditions, the model is re-loaded from the data source whenever the <code>stamp</code> changes as part of any operation. This attracts a performance penalty as each add, update or remove operation will presumably result in an updated <code>stamp</code> which in turn results in the subsequent re-load of the model.</p>

        <p><strong>Note 2:</strong> when a re-load is performed, the model only imports changes into the cache, maintaining any local where possible.</p>


        <h3 class="lined">Each Polling</h3>
        <p>Each polling is more suitable to smaller models. It results in periodic polling of the data source, with a payload of the ID and comparable version field of each data record known locally. From this data, it is expected that the data source can identify records which have changed, providing these changes back in the response.</p>
        

        <div class="methods">
          <h3 class="lined">Methods</h3>
          <p></p>
          ${this.buildMethods( 'PollingModel' )}
        </div>


      </div>
    `
  }


}

export { ModelPollingView };

