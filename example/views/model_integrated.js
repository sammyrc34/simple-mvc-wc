

import DocParser from '../util/docparser.js';
import { ExampledView } from './example.js';


import rawcode from '!raw-loader!../../lib/models/integrated.js';

let __docs = undefined;



class ModelIntegratedView extends ExampledView {

  static methodNames( ) {
    return [ 'constructor', 'setup', 'fetched', 'load', 'reload', 'fetch', 'refetch', 'clear', 'handleNotify', 'handleDisconnect' ];
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }



  static build() {
    return this.builder`
      <div id="model-integrated-page">
        <h2>Integrated Models</h2>
        <h3 class="lined">Description</h3>
        <p>The <code>IntegratedModel</code> class is a mixin that augments the <code>RemoteModel</code> to be a partial cache model, fetching only records required and utilising WebSockets to keep consistency.</p>

        <p>Differeing to <code>RemoteModel</code> models, this model attempts websocket connection (if used) upon <code>load</code>, and fetches records upon a <code>fetch</code> method with a POST request. With tighter integration to ensure that only required records are fetched, pagination, sorting and filtering is attended server side as needed, hence the name.</p>

        <h3 class="lined">Data Consistency</h3>
        <p>Consistency in the data of the <code>IntegratedModel</code> is acheived through a WebSocket connection where the data source can publish changes that may impact the records of this model. The model can be configured to retreive updates, or accept updates provided via WebSocket messages. Supporting this, and to make allowances for multiple data source clients with multiple data models requiring updates for data sources, a publishing / subscribing channel based WebSocket implementation is used. To use this functionality, a <code>wsChannel</code> and <code>url.websock</code> configuration is required</p>

        <p>For implementing the WebSocket server functionality, a flexible "ws" based utility library class <code>ChannelSock</code> is available. This utility provides the channel pub/sub functionality with a simple API to notify of changes to subscribed clients. See inline documentation to implement this in your project.</p>

        <h3 class="lined">Filtering & Sorting</h3>
        <p>For <code>IntegratedModel</code> models, filtering is required to be completed by the server. To acheieve this, filter rules from the client are serialised and sent to the server with the fetch request. To assist with this, a utility class <code>QueryTranslate</code> is available in this library to translate the application filter structure into a SQL constraint clause.</p>

        <p>Similar to filtering, sorting for <code>IntegratedModel</code> is required on the server. The sorting structure is sent with each fetch request. Translating the application sorting structure into a form for the data source is generally not complex.</p>


        <h3 class="lined">Data API</h3>
        <p>The API for data fetching is more integrated with the data source, hence the name. Initial data fetching occurs via a POST request to the <code>fetch</code> URL with the following <var>application/json</var> structure:<p>
        <ul class="typedef">
          <li><div><var>operation</var><var>{String}</var></div> - The operation being performed, being 'fetch'</li>
          <li><div><var>start</var><var>{Number}</var></div> - The zero based index to start the fetch from</li>
          <li><div><var>count</var><var>{Number}</var></div> - The number of records to fetch, or 0 for all records</li>
          <li><div><var>extra</var><var>{Object}</var></div> - Extra query parameters provided to the model</li>
          <li><div><var>filters</var><var>{<a href="model_collections">FiltersCollection}</a></var></div> - The filters to apply to this fetch</li>
          <li><div><var>sorts</var><var>{<a href="model_collections">SortCriteria[]</a>}</var></div> - The sorts to apply to this fetch</li>
          <li><div><var>include</var><var>{Array&lt;String|Number&gt;}</var></div> - A list of additional records to include</li>
        </ul>

        <p><b>Note: </b>IntegratedModel classes do not support javascript sort or filter functions, and do not support the <var>has</var> filter criteria type.</p>

        The result is expected to be of type <var>application/json</var> with the following structure:</p>
        <ul class="typedef">
          <li><div><var>success</var><var>{Boolean}</var></div> - If the request was successful</li>
          <li><div><var>message</var><var>{String}</var></div> - A message about why a request was not successful</li>
          <li><div><var>meta</var><var>{Object}</var></div> - Metadata for result set</li>
          <li><div><var>meta.totalCount</var><var>{Number}</var></div> - Total number of records in dataset</li>
          <li><div><var>meta.filterCount</var><var>{Number}</var></div> - Total number of records in the current filter</li>
          <li><div><var>records</var><var>{Object[]}</var></div> - The resulting data records</li>
          <li><div><var>additional</var><var>{Object[]}</var></div> - Any additional records requested not included in records list</li>
        </ul>

        <p>Implementing the <code>RemoteModel</code> functionality, other create, update and delete operations are available. See <a href="model_remote"><code>RemoteModel</code></a>.</p>






        <div class="methods">
          <h3 class="lined">Methods</h3>
          <p></p>
          ${this.buildMethods( 'SimpleIntegratedCollection' )}
        </div>


      </div>
    `
  }


}

export { ModelIntegratedView };

