
import DocParser from '../util/docparser.js';
import { SimpleSlideout, SimpleButton, SimpleTable } from '../../lib/index.js';
import { ExampledView } from './example.js';
SimpleButton.register();
SimpleSlideout.register();
SimpleTable.register();


import rawcode from '!raw-loader!../../lib/components/slideout.js';

let __docs = undefined;


class SlideoutsView extends ExampledView {

  static title() {
    return "Slideouts";
  }

  static propertyElements() {
    return [ 'sc-slideout' ];
  }

  static propertyTables() {
    return [ 'slideout-properties' ]
  }

  static varElements() {
    return [ 'sc-slideout' ];
  }

  static varTables() {
    return [ 'slideout-styles' ]
  }

  static methodNames() {
    return [ 'getSize', 'show', 'hide', 'toggle' ];
  }

  static eventNames() {
    return [ 'before-hide', 'hide', 'before-show', 'show' ];
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }

  static build() {
    return this.builder`
      <div id="slideouts-page">
        <h2>Slideouts</h2>

        <div>
          <h3 class="lined">Description</h3>
          <p>The <code>&lt;sc-slideout&gt;</code> component is a versatile panel for displaying content outside of the usual document flow. It can function in a specific container, or on the viewport, and supports a range of sizing options and directions.</p>
        </div>

        <h3 class="lined">Try it out</h3>
        <div class="slideouts-tryit">
          <div class="flex-row-start">
            <div>
              <h3>Options:</h3>
              <sc-select id="position" label="Position">
                <sc-select-option value="right" selected label="Right"></sc-select-option>
                <sc-select-option value="left" label="Left"></sc-select-option>
                <sc-select-option value="top" label="Top"></sc-select-option>
                <sc-select-option value="bottom" label="Bottom"></sc-select-option>
              </sc-select>
              <sc-input class="block" id="opensize" name="opensize" type="text" value="auto" label="Initial size" comment="Eg auto, 50% or 250px"></sc-input>
              <sc-input class="block" id="fullpagetoggle" name="fullpagetoggle" type="toggle" label="Viewport mode" valuetype="boolean"></sc-input>
              <sc-input class="block" id="modaltoggle" name="modaltoggle" type="toggle" label="Modal" valuetype="boolean"></sc-input>
              <sc-input class="block" id="hideclosetoggle" name="hideclosetoggle" type="toggle" label="Hide close button" valuetype="boolean"></sc-input>
              <sc-input class="block" id="autoclosetoggle" name="autoclosetoggle" type="toggle" label="Autoclose" valuetype="boolean"></sc-input>
              <sc-input class="block" id="resizabletoggle" name="resizabletoggle" type="toggle" label="Resizable" valuetype="boolean"></sc-input>
            </div>
            <div>
              <h3>&nbsp;</h3>
              <sc-input class="block" id="resizemin" name="resizemin" type="text" label="Minimum resize size"></sc-input>
              <sc-input class="block" id="resizemax" name="resizemax" type="text" label="Maximum resize size" comment="Eg 50% or 250px"></sc-input>

              <sc-input class="block" id="hideheadertoggle" name="hideheadertoggle" type="toggle" label="Hide header?" valuetype="boolean"></sc-input>
              <sc-input class="block" id="startopentoggle" name="startopentoggle" type="toggle" label="Start open?" valuetype="boolean" checked></sc-input>
              <sc-input class="block" id="squeezetoggle" name="squeezetoggle" type="toggle" label="Squeeze size?" valuetype="boolean" comment="Demonstrating the panel detecting content size"></sc-input>
              <div class="flex-row">
                <sc-button label="Open" outline hover @click=${this.showSlideout}></sc-button>
                <sc-button label="Build" hover @click=${this.buildSlideout}></sc-button>
              </div>

            </div>
          </div>

          <div style="width: 50px;"></div>

          <div>
            <h3>Panel area:</h3>
            <div class="bordered" style="width: 600px; height: 500px; position: relative;">
              <sc-slideout id="example-panel" position="right" autoclose>
                <div slot="header" style="font-size: larger;"><span>A heading</span></div>
                <div id="panelcontent">
                  <p>This is some content in a slide panel which could contain anything.</p>
                  <div style="display: flex; flex-direction: column; align-items: center; justify-items: center;">
                    <div style="width: 50px; height: 50px; margin: 5px; background-color: red;"></div>
                    <div style="width: 50px; height: 50px; margin: 5px; background-color: green;"></div>
                    <div style="width: 50px; height: 50px; margin: 5px; background-color: blue;"></div>
                    <sc-button class="block" label="Close panel" hover @click=${this.close}></sc-button>
                    <p><sub>In case autoclose and close button is disabled</sub></p>
                  </div>
                </div>
              </sc-slideout>
            </div>
          </div>
        </div>


        <div class="properties">
          <h3 class="lined">Properties</h3>
          <p></p>
          <sc-table
              id="slideout-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>


        <div class="events">
          <h3 class="lined">Events</h3>
          ${this.buildEvents( 'SimpleSlideout' )}
        </div>


        <div class="methods">
          <h3 class="lined">Methods</h3>
          ${this.buildMethods( 'SimpleSlideout' )}
        </div>


        <div class="variables">
          <h3 class="lined">CSS Variables</h3>
          <p></p>
          <sc-table
              id="slideout-styles"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.varFields)}
          >
          </sc-table>
        </div>


      </div>
    `
  }

  static close() {
    var panel = document.getElementById( 'example-panel' );
    panel.hide();
  }


  static showSlideout() {
    const panel = document.getElementById( 'example-panel' );
    if ( panel )
      panel.show();
  }


  static buildSlideout() {
    const panel = document.getElementById( 'example-panel' );
    const position = document.getElementById( 'position' );
    const content = document.getElementById( 'panelcontent' );
    const opensize = document.getElementById( 'opensize' );
    const toggle = document.getElementById( 'modaltoggle' );
    const squeeze = document.getElementById( 'squeezetoggle' );
    const hideclose = document.getElementById( 'hideclosetoggle' );
    const autoclose = document.getElementById( 'autoclosetoggle' );
    const fullpage = document.getElementById( 'fullpagetoggle' );
    const hideheader = document.getElementById( 'hideheadertoggle' );
    const startopen = document.getElementById( 'startopentoggle' );
    const resizable = document.getElementById( 'resizabletoggle' );
    const resizemin = document.getElementById( 'resizemin' );
    const resizemax = document.getElementById( 'resizemax' );
    let headerelem = document.querySelector( '#example-panel div[slot=header]' );

    if ( panel.open )
      panel.hide( true );

    if ( hideheader.value && headerelem ) {
      panel.removeChild( headerelem );
    }
    else if ( ! hideheader.value && ! headerelem ) {
      headerelem = document.createElement( 'div' );
      headerelem.innerHTML = '<div slot="header" style="font-size: larger;"><span>A heading</span></div>';
      panel.appendChild( headerelem.firstElementChild );
    }

    if ( squeeze.value ) {
      if ( position == "left" || position == "right" ) {
        content.style[ "width" ] = "150px";
        content.style.removeProperty( 'height' );
      }
      else {
        content.style.removeProperty( 'width' );
        content.style[ "height" ] = "75px";
      }
    }
    else {
      content.style.removeProperty( 'height' );
      content.style.removeProperty( 'width' );
    }

    panel._opensize = undefined;
    panel.opensize = opensize.value;
    panel.position = position.value;
    panel.fullpage = fullpage.value;
    panel.hideclose = hideclose.value;
    panel.autoclose = autoclose.value;
    panel.modal = toggle.value;
    panel.resizable = resizable.value;
    panel.resizemin = resizemin.value;
    panel.resizemax = resizemax.value;

    if ( startopen.value )
      panel.show();
  }


}


export {
  SlideoutsView
}

