

import DocParser from '../util/docparser.js';
import { ExampledView } from './example.js';


import rawcode from '!raw-loader!../../lib/models/record.js';

let __docs = undefined;



class ModelRecordsView extends ExampledView {

  static methodNames( ) {
    return [ 'constructor', 'setup', 'clear' , 'length', 'fetch', 'hasRecord', 'fetchOne', 'getRecord', 'getRecords', 'add', 'remove', 'update', 'check', 'parse'  ];
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }



  static build() {
    return this.builder`
      <div id="model-records-page">
        <h2>Records</h2>
        <h3 class="lined">Description</h3>
        <p>The <code>SimpleRecord</code> class is the most basic in-memory and single-record model type in the application. It provides only a single record object with an interface to interact with the data record. While use cases are few, this class serves as the basis for other record type classes.</p>
        <p>The <code>SimpleRecord</code> class supports the same CRUD-like interface as other data model classes, comprised of <code>add</code>, <code>update</code>, <code>remove</code> and <code>fetch</code>. In addition, several helper methods are available for use as appropriate.</p>


        <div class="methods">
          <h3 class="lined">Methods</h3>
          <p></p>
          ${this.buildMethods( 'BasicRecord' )}
        </div>


      </div>
    `
  }


}

export { ModelRecordsView };

