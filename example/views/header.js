
import { library as faLib, icon as faIcon } from "@fortawesome/fontawesome-svg-core";
import { faAdjust } from "@fortawesome/free-solid-svg-icons";

import { SimpleBusyMask, SimpleConfirm, SimpleView, SimpleButton, SimpleImage, SimplePopup } from '../../lib/index.js';
SimpleBusyMask.register();
SimpleButton.register();
SimpleImage.register();
SimplePopup.register();
SimpleConfirm.register();

faLib.add( faAdjust );


class HeaderView extends SimpleView {


  static build() {
    return this.builder`
      <div id="header">
        <div class="flex-row">
          <div class="flex-row flex-start flex-center">
            <div><h2 class="inline">simple-mvc-wc&nbsp;</h2><span> - The simple web component and MVC web library</span></div>
          </div>
          <div class="flex-row flex-end flex-grow">
            <div title="Toggle dark mode" class="contrast-icon" slot="icon" @click=${this.toggleDarkMode}>${this.includeSvg(faIcon( faAdjust ).html[0])}</div>
          </div>
        </div>
      </div>
    `
  }


  static toggleDarkMode() {
    const body = document.body;
    body.classList.toggle( 'dark' );
  }

}

export default HeaderView;

