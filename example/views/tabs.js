

import DocParser from '../util/docparser.js';
import { SimpleTabsContainer, SimpleTab, SimpleTable } from '../../lib/index.js';
import {ExampledView} from './example.js';
SimpleTabsContainer.register();
SimpleTab.register();
SimpleTable.register();


import rawcode from '!raw-loader!../../lib/components/tabs.js';

let __docs = undefined;

const methodNames = {
  'SimpleTabsContainer': [ 'selectTab', 'selectTabIndex' ],
  'SimpleTab': [ 'selectTab' ]
};

const eventNames = {
  'SimpleTabsContainer': [ 'before-select', 'after-select' ]
};

class TabsView extends ExampledView {

  static title() {
    return "Tabs";
  }

  static propertyElements() {
    return [ 'sc-tabs', 'sc-tab' ];
  }
  static propertyTables() {
    return [ 'tabs-properties', 'tab-properties' ]
  }

  static varElements() {
    return [ 'sc-tabs' ];
  }

  static varTables() {
    return [ 'tab-styles' ]
  }

  static methodNames( className ) {
    return methodNames[ className ];
  }

  static eventNames( className ) {
    return eventNames[ className ];
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }




  static build() {
    return this.builder`
      <div id="tabs-page">
        <h2>Tabs</h2>
        <div>
          <h3 class="lined">Description</h3>
          <p>The <code>&lt;sc-tab&gt;</code> and containing <code>&lt;sc-tabs&gt;</code> components are a user interactable control with one or more tabs. The <code>&lt;sc-tab&gt;</code> component is for a single tab, and should be contained in a <code>&lt;sc-tabs&gt;</code> container component.</p>
        </div>

        <div class="properties">
          <h3 class="lined">Properties</h3>
          <h4><code>&lt;sc-tabs&gt;</code></h4>
          <sc-table
              id="tabs-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>

          <h4><code>&lt;sc-tab&gt;</code></h4>
          <sc-table
              id="tab-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>


        <div class="events">
          <h3 class="lined">Events</h3>
          <h4><code>&lt;sc-tabs&gt;</code></h4>
          ${this.buildEvents( 'SimpleTabsContainer' )}
        </div>


        <div class="methods">
          <h3 class="lined">Methods</h3>

          <h4><code>&lt;sc-tabs&gt;</code></h4>
          ${this.buildMethods( 'SimpleTabsContainer' )}

          <h4><code>&lt;sc-tab&gt;</code></h4>
          ${this.buildMethods( 'SimpleTab' )}
        </div>


        <div class="variables">
          <h3 class="lined">CSS Variables</h3>
          <p></p>
          <sc-table
              id="tab-styles"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.varFields)}
          >
          </sc-table>
        </div>


        <div id="examples" class="examples">
          <h3 class="lined">Examples</h3>

          <h4>Autosized tabs</h4>
          <div class="example-row">
            <div class="example">
              <sc-tabs id="tabbar">
                <sc-tab label="First">
                  <p slot="content">The first tab's content</p>
                  <p slot="content">There could be just about any HTML elements in here</p>
                  <img slot="content" style="height: 250px; width: auto;" src="/images/bees.jpg"></img>
                </sc-tab>
                <sc-tab label="Second">
                  <p slot="content">The second tab's amazing content</p>
                </sc-tab>
                <sc-tab label="Disabled" disabled>
                  <p slot="content">This tab is not reachable</p>
                </sc-tab>
              </sc-tabs>
            </div>
          </div>


          <h4>Sized to parent container with raised effect</h4>
          <div class="example-row">
            <div style="height: 200px; width: 400px;">
              <sc-tabs id="tabbar2" fill raised scroll>
                <sc-tab label="One">
                  <div slot="content">
                    <h4>First title</h4>
                    <p>There could be anything in here.<br>
                      The tab will scroll to the height of the parent.
                      <ul><li>One</li><li>Two</li><li>Three</li><li>Four</li></ul>
                      <img slot="content" style="height: 200px; width: auto;" src="/images/bees.jpg"></img>
                    </p>
                  </div>
                </sc-tab>
                <sc-tab label="Two">
                  <p slot="content">The second tab's amazing content</p>
                </sc-tab>
              </sc-tabs>
            </div>
          </div>


          <h4>Long label tabs</h4>
          <div class="example-row" style="width: 400px; height: 200px;">
            <sc-tabs id="tabbar" fill>
              <sc-tab label="This is the first tab">
                <p slot="content">The first tab's content</p>
              </sc-tab>
              <sc-tab label="This is the second tab">
                <p slot="content">The second tab's amazing content</p>
              </sc-tab>
            </sc-tabs>
          </div>

        </div>
      </div>
    `
  }

}


export { TabsView };

