
import DocParser from '../util/docparser.js';
import { SimpleButton, SimpleInput, SimpleForm, SimpleTable } from '../../lib/index.js';
import { ExampledView } from './example.js';
SimpleButton.register();
SimpleInput.register();
SimpleForm.register();
SimpleTable.register();

import rawcode from '!raw-loader!../../lib/components/inputs.js';

let __docs = undefined;

const example = `<sc-input label="Text input"></sc-input>`;

class InputsView extends ExampledView {

  static title() {
    return "Inputs";
  }

  static propertyElements() {
    return [ 'sc-input' ];
  }
  static propertyTables() {
    return [ 'input-properties' ]
  }

  static varElements() {
    return [ 'sc-input' ];
  }

  static varTables() {
    return [ 'input-styles' ]
  }

  static filterExample( clone ) {
    const inputs = clone.querySelectorAll( 'input' );
    for ( let input of inputs )
      input.remove();
  }

  static methodNames() {
    return [ 'value', 'reset', 'changed', 'setInitial', 'focus', 'validate' ];
  }

  static eventNames() {
    return [ 'search', 'after-input', 'after-change' ];
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }

  static build() {
    return this.builder`
      <div id="inputs-page">
        <h2>Inputs</h2>
        <div>
          <h3 class="lined">Description</h3>
          <p>The <code>&lt;sc-input/&gt;</code> control is a user interactable input control for data entry and user interaction. The control has several styles, and expanded capabilities for data entry.</p>

          <h3 class="lined">Try it out</h3>
          ${ExampledView.build( example )}
        </div>

        <div class="properties">
          <h3 class="lined">Properties</h3>
          <p></p>
          <sc-table
              id="input-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>


        <div class="events">
          <h3 class="lined">Events</h3>
          ${this.buildEvents( 'SimpleInput' )}
        </div>


        <div>
          <h3 class="lined">Methods</h3>
          ${this.buildMethods( 'SimpleInput' )}
        </div>


        <div>
          <h3 class="lined">Format masks</h3>
          <p>Text type inputs support format masks, using the properties <code>format</code> and <code>formatchars</code> to control what the user is presented with.</p>
          <p>The <code>format</code> property specifies the blank format mask to display to the user, eg <code>__:__:__:__:__:__</code> or <code>YYYY-MM-DD</code>.</p>
          <p>The <code>formatchars</code> property specifies the characters of the format mask which will accept user input. For text type inputs, this defaults to <code>_</code>. For datetime type inputs, this defaults to <code>YMDShmdsZAa</code>.</p>
          <p>If greater control is needed around user input, then the optional <code>pattern</code> property can be set with a regular expression and autovalidation to limit user input into the field.</p>
          <p>For example, these properties provide a MAC address input field:</p>
          <div class="codeblock">
<pre>&lt;sc-input
  type="text"
  label="MAC Address"
  autovalidate
  format="__:__:__:__:__:__"
  formatchars="_"
  pattern="^(?:[A-Fa-f0-9]{2}:){5}:[A-Fa-f0-9]{2}$"
&gt;&lt;/sc-input&gt;</pre>
          </div>
          <p><b>NOTE</b>: When autovalidation is used with a pattern, each individual key is validated. To acheieve that, this control converts
          the pattern to one that supports partial string matches. However, this pattern only matches left-to-right, and therefore does not
          support input such as <code>__:__:__:AB:__:__</code>. To enable such input, disable <code>autovalidation</code> and check validation
          after user input.</p>
        </div>


        <div class="variables">
          <h3 class="lined">CSS Variables</h3>
          <p></p>
          <sc-table
              id="input-styles"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.varFields)}
          >
          </sc-table>
        </div>


        <div id="examples" class="examples" @click=${this.chooseExample.bind( this )}>
          <h3 class="lined">Examples</h3>
          <p>Select and try out any example:<sc-button id="choose-button" type="toggle" label="Choose example..." @click=${this.setChooseMode}></sc-button></p>

          <h4>Simple inputs</h4>

          <div class="example-row">
            <div class="example">
              <sc-input name="one" type="text" label="Animated label"></sc-input>
            </div>

            <div class="example">
              <sc-input name="ten" type="text" label="Non-animated" noanimate emptyasnull></sc-input>
            </div>

            <div class="example">
              <sc-input name="two" type="text" label="Small label only" smalllabel></sc-input>
            </div>

            <div class="example">
              <sc-input name="three" type="text" label="Initial value" value="Foo Bar"></sc-input>
            </div>
          </div>

          <div class="example-row">
            <div class="example">
              <sc-input name="five" type="text" label="Required field" required></sc-input>
            </div>

            <div class="example">
              <sc-input name="six" type="text" label="Another field" placeholder="Simple placeholder"></sc-input>
            </div>

            <div class="example">
              <sc-input name="seven" type="text" label="Long field" style="--input-width: 400px;"></sc-input>
            </div>
          </div>

          <div class="example-row">
            <div class="example">
              <sc-input name="eight" type="text" label="Disabled" disabled></sc-input>
            </div>

            <div class="example">
              <sc-input name="eight" type="text" label="Disabled and small" disabled smalllabel value="some value"></sc-input>
            </div>

            <div class="example">
              <sc-input name="nine" type="text" label="Read only" readonly value="Read only value"></sc-input>
            </div>

            <div class="example">
              <sc-input name="four" type="text" label="Small field" style="--input-width: 120px;"></sc-input>
            </div>
          </div>

          <div class="example-row">
            <div class="example">
              <sc-input name="ten" type="text" label="Autovalidated field" required autovalidate errormessage="Enter a valid value"></sc-input>
            </div>

            <div class="example">
              <sc-input name="eleven" type="text" label="A very long label which might be wrapped"></sc-input>
            </div>

            <div class="example">
              <div class="inline"><p class="inline">No label:</p><sc-input name="twelve" type="text" value="some value"></sc-input></div>
            </div>
          </div>

          <div class="example-row">
            <div class="example">
              <sc-input name="thirteen" type="text" label="Field with subtext" comment="Comment about the field"></sc-input>
            </div>

            <div class="example">
              <sc-input name="fourteen" type="text" label="Field with subtext" comment="There may be a longer comment about a field"></sc-input>
            </div>

            <div class="example">
              <sc-input name="searchinput" type="search" label="Search" placeholder="Enter search terms..."></sc-input>
            </div>
          </div>

          <div class="example-row">
            <div class="example">
              <sc-input name="sixteen" type="text" label="Custom validator" autovalidate errormessage="Enter value 'abc'" comment="Custom validation, value 'abc' will pass"></sc-input>
            </div>

            <div class="example">
              <sc-input name="fifteen" type="text" label="Manual validation" required errormessage="Enter a valid value"></sc-input>
            </div>

            <sc-button style="padding: 5px;" label="Validate" @click=${() => {document.querySelector( 'sc-input[name=fifteen]' ).validate()}}></sc-button>
          </div>

          <h4>Text-like inputs</h4>
          <div class="example-row">
            <div class="example">
              <sc-input name="email" type="email" label="Email address"></sc-input>
            </div>
            <div class="example">
              <sc-input name="samplepassword" type="password" autocomplete="off" label="Password"></sc-input>
            </div>
            <div class="example">
              <sc-input name="number" type="number" label="Number field"></sc-input>
            </div>
            <div class="example">
              <sc-input name="tel" type="tel" autovalidate minlength=8 maxlength=16 label="Telephone number" pattern="^\\+?[0-9 ]+$" placeholder="+xx x xxxx xxxx"></sc-input>
            </div>
          </div>

          <div class="example-row">
            <div class="example">
              <sc-input name="simpleurl1" type="url" label="URL" placeholder="https://xxx"></sc-input>
            </div>
            <div class="example">
              <sc-input name="samplepassword2" type="password" label="Checked Password" pattern="^\\w{4,}$" errormessage="Minimum four characters" autovalidate></sc-input>
            </div>
            <div class="example">
              <sc-input name="samplepassword3" type="text" noanimate label="Non-animated validation" pattern="^\\w{4,}$" errormessage="Minimum four characters" autovalidate></sc-input>
            </div>
            <div class="example">
              <sc-input name="simpledatetime2" type="datetime" label="Datetime (no mask)" valuetype=number step=60></sc-input>
            </div>
          </div>

          <h4>Formatted inputs</h4>
          <div class="example-row">
            <div class="example">
              <sc-input name="simplemask1" type="text" label="Simple mask" format="____-____" placeholder="abcd-1234" comment="2+ letters, then 2+ numbers" autovalidate emptyasnull pattern="^[a-zA-Z]{2,4}-[0-9]{2,4}$"></sc-input>
            </div>
            <div class="example">
              <sc-input name="simpledate1" type="datetime" label="Date with mask" format="YYYY-MM-DD" valuetype=date emptyasnull autovalidate></sc-input>
            </div>
            <div class="example">
              <sc-input name="betterphone" type="text" label="Phone number" format="(+__ _) ____ ____" comment="Eg (+00 1) 5555 5555" autovalidate pattern="^\\(\\+[0-9]{2} \\d\\) \\d{4} \\d{4}$"></sc-input>
            </div>
            <div class="example">
              <sc-input name="simpledate2" type="datetime" label="Disabled Date" format="YYYY-MM-DD" valuetype=date emptyasnull autovalidate disabled></sc-input>
            </div>
          </div>

          <div class="example-row">
            <div class="example">
              <sc-input name="simpledate3" type="datetime" label="Readonly Date" format="YYYY-MM-DD" valuetype=string value="2001-04-23" autovalidate readonly></sc-input>
            </div>
            <div class="example">
              <sc-input name="simpledate4" type="datetime" label="Readonly Invalid Date" format="YYYY-MM-DD" invalid readonly></sc-input>
            </div>
            <div class="example">
              <sc-input name="simpledate5" type="datetime" label="Date (epoch value)" format="YYYY-MM-DD" valuetype=number></sc-input>
            </div>
            <div class="example">
              <sc-input name="simplemac1" type="text" label="MAC Address" format="__:__:__:__:__:__" autovalidate emptyasnull pattern="^(?:[A-Fa-f0-9]{2}:){5}[A-Fa-f0-9]{2}$"></sc-input>
            </div>
          </div>

          <div class="example-row">
            <div class="example">
              <sc-input name="simplenummask" type="text" label="Numbered mask" format="___ years" placeholder="23 years" comment="enter whole years" autovalidate emptyasnull pattern="^[0-9]{1,3} years$" valuetype=number></sc-input>
            </div>
          </div>

          <h4>Toggles</h4>
          <div class="example-row">
            <div class="example">
              <sc-input name="toggle1" type="toggle" label="Simple toggle"></sc-input>
            </div>
            <div class="example">
              <sc-input name="toggle2" type="toggle" label="Switched toggle" checked></sc-input>
            </div>
            <div class="example">
              <sc-input name="toggle3" type="toggle" label="Disabled toggle" disabled></sc-input>
            </div>
            <div class="example">
              <sc-input name="toggle4" type="toggle" label="Disabled switched" disabled checked></sc-input>
            </div>
          </div>

          <div class="example-row">
            <div class="example">
              <sc-input name="toggle5" type="toggle" label="Left control" position="left"></sc-input>
            </div>
            <div class="example">
              <sc-input name="toggle6" type="toggle" label="Non-animated" noanimate></sc-input>
            </div>
            <div class="example">
              <sc-input name="toggle7" type="toggle" label="Invalid" required invalid autovalidate errormessage="Check this toggle!"></sc-input>
            </div>
            <div class="example">
              <sc-input name="toggle8" type="toggle" label="A very long label which might be wrapped"></sc-input>
            </div>
          </div>

          <div class="example-row">
            <div class="example">
              <sc-input name="toggle10" type="toggle" label="Tri-state toggle" tristate=true></sc-input>
            </div>
            <div class="example">
              <sc-input name="toggle9" type="toggle" label="Simple toggle" comment="A comment for this toggle"></sc-input>
            </div>
          </div>

          <h4>Checkboxes</h4>
          <div class="example-row">
            <div class="example">
              <sc-input name="checkbox1" type="checkbox" label="Simple checkbox"></sc-input>
            </div>
            <div class="example">
              <sc-input name="checkbox2" type="checkbox" label="Selected checked" checked></sc-input>
            </div>
            <div class="example">
              <sc-input name="checkbox3" type="checkbox" label="Checked disabled" disabled checked></sc-input>
            </div>
            <div class="example">
              <sc-input name="checkbox4" type="checkbox" label="Unchecked disabled" disabled></sc-input>
            </div>
          </div>

          <div class="example-row">
            <div class="example">
              <sc-input name="checkbox5" type="checkbox" label="Left checkbox" position="left" checked></sc-input>
            </div>
            <div class="example">
              <sc-input name="checkbox6" type="checkbox" label="Non-animated" noanimate></sc-input>
            </div>
            <div class="example">
              <sc-input name="checkbox7" type="checkbox" label="Invalid" required autovalidate invalid errormessage="Check this box!"></sc-input>
            </div>
            <div class="example">
              <sc-input name="checkbox8" type="checkbox" label="A very long label which might be wrapped"></sc-input>
            </div>
          </div>

          <div class="example-row">
            <div class="example">
              <sc-input name="checkbox11" type="checkbox" tristate label="Tri-state checkbox" unset></sc-input>
            </div>
            <div class="example">
              <sc-input name="checkbox9" type="checkbox" label="Simple checkbox" comment="This checkbox does xyz" falsevalue=3 defaulttrue="" unsetvalue=2 valuetype="number" tristate></sc-input>
            </div>
            <div class="example">
              <sc-input name="checkbox10" type="checkbox" label="Extra long left sided checkbox" comment="This checkbox does xyz" position="left" style="--input-width: calc( 22 * var( --theme-control-size ) );"></sc-input>
            </div>
          </div>

          <h4>Radios</h4>
          <div class="example-row">
            <div class="example">
              <sc-input name="radio1" type="radio" label="Simple radio"></sc-input>
            </div>
            <div class="example">
              <sc-input name="radio2" type="radio" label="Selected radio" checked></sc-input>
            </div>
            <div class="example">
              <sc-input name="radio3" type="radio" label="Disabled selected" disabled checked></sc-input>
            </div>
            <div class="example">
              <sc-input name="radio4" type="radio" label="Disabled unselected" disabled></sc-input>
            </div>
          </div>

          <div class="example-row">
            <div class="example">
              <sc-input name="radio5" type="radio" label="Left radio" position="left" checked></sc-input>
            </div>
            <div class="example">
              <sc-input name="radio6" type="radio" label="Non-animated" noanimate value="yes"></sc-input>
            </div>
            <div class="example">
              <sc-input name="radio7" type="radio" label="Invalid" required invalid autovalidate errormessage="Select this radio!"></sc-input>
            </div>
            <div class="example">
              <sc-input name="radio8" type="radio" label="A very long label which might be wrapped"></sc-input>
            </div>
          </div>

          <div class="example-row">
            <div class="example">
              <sc-input name="radio9" type="radio" label="Tri-state radio" tristate unset></sc-input>
            </div>
          </div>

          <h4>Grouped controls</h4>
          <div class="example-row">
            <div class="example">
              <div class="flex-row">
                <span style="padding-right: 50px;">Choose:</span>
                <sc-input name="groupr1" type="radio" nomessage value="a" label="Option A" grouped position="left" checked style="--input-radio-label-right-padding: 10px; --input-width: 120px;"></sc-input>
                <sc-input name="groupr1" type="radio" nomessage value="b" label="Option B" grouped position="left" style="--input-radio-label-right-padding: 10px; --input-width: 120px;"></sc-input>
              </div>
            </div>
          </div>

          <h4>Group controls mixed</h4>
          <div class="example-row">
            <div class="example">
              <div class="flex-row">
                <span style="padding-right: 50px;">Choose:</span>
                <sc-input name="groupr2" type="radio" label="Option A" nomessage value="a" grouped position="left" checked style="--input-radio-label-right-padding: 10px; --input-width: 150px;"></sc-input>
                <sc-input name="groupr2" type="checkbox" label="Option B" nomessage value="b" grouped position="left" style="--input-checkbox-label-right-padding: 10px; --input-width: 150px;" checked></sc-input>
                <sc-input name="groupr2" type="toggle" label="Option C" nomessage value="c" grouped position="left" style="--input-toggle-label-right-padding: 10px; --input-width: 150px;"></sc-input>
              </div>
            </div>
          </div>
        </div>
      </div>
    `
  }

  static connect() {
    super.connect();
    document.querySelector( 'sc-input[name=sixteen]' ).validator = (val) => { return val == "abc" };
  }

}


export { InputsView }

