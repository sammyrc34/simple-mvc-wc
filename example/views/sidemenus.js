

import DocParser from '../util/docparser.js';
import { SimpleSidemenu, SimpleSidemenuGroup, SimpleSidemenuItem, SimpleTable } from '../../lib/index.js';
import {ExampledView} from './example.js';
SimpleSidemenu.register();
SimpleSidemenuGroup.register();
SimpleSidemenuItem.register();
SimpleTable.register();

import { library as faLib, icon as faIcon } from "@fortawesome/fontawesome-svg-core";

import { faQuestionCircle, faTable, faCogs, faBeer, faSave } from "@fortawesome/free-solid-svg-icons";

import rawcode from '!raw-loader!../../lib/components/sidemenu.js';



faLib.add( faTable, faCogs, faBeer, faQuestionCircle, faSave );

let __docs = undefined;

const methodNames = {
  'SimpleSidemenu': [ 'toggleMenu' ],
  'SimpleSidemenuGroup': [ 'toggle' ],
  'SimpleSidemenuItem': []
};

const eventNames = {
  'SimpleSidemenu': [ 'after-resize' ],
  'SimpleSidemenuGroup': [ 'toggled' ],
  'SimpleSidemenuItem': []
};

class SidemenusView extends ExampledView {

  static title() {
    return "Tabs";
  }

  static propertyElements() {
    return [ 'sc-sidemenu', 'sc-sidemenu-group', 'sc-sidemenu-item' ];
  }
  static propertyTables() {
    return [ 'sidemenu-properties', 'sidemenu-group-properties', 'sidemenu-item-properties' ]
  }

  static methodNames( className ) {
    return methodNames[ className ];
  }

  static eventNames( className ) {
    return eventNames[ className ];
  }

  static varElements() {
    return [ 'sc-sidemenu' ];
  }

  static varTables() {
    return [ 'sidemenu-styles' ]
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }

  static build() {
    return this.builder`
      <div id="sidemenus-page">
        <h2>Sidemenus</h2>
        <div>
          <h3 class="lined">Description</h3>
          <p>The <code>&lt;sc-sidemenu&gt;</code>, <code>&lt;sc-sidemenu-group&gt;</code> and <code>&lt;sc-sidemenu-item&gt;</code> components are a user interactable controls for side-menus. It's intended to be used for side-sitting navigation menus.</p>
        </div>

        <div class="properties">
          <h3 class="lined">Properties</h3>
          <h4><code>&lt;sc-sidemenu&gt;</code></h4>
          <sc-table
              id="sidemenu-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>

        <div class="properties">
          <h4><code>&lt;sc-sidemenu-group&gt;</code></h4>
          <sc-table
              id="sidemenu-group-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>

        <div class="properties">
          <h4><code>&lt;sc-sidemenu-item&gt;</code></h4>
          <sc-table
              id="sidemenu-item-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>

        </div>


        <div class="events">
          <h3 class="lined">Events</h3>
          <h4><code>&lt;sc-sidemenu&gt;</code></h4>
          ${this.buildEvents( 'SimpleSidemenu' )}

          <h4><code>&lt;sc-sidemenu-group&gt;</code></h4>
          ${this.buildEvents( 'SimpleSidemenuGroup' )}
        </div>


        <div class="methods">
          <h3 class="lined">Methods</h3>

          <h4><code>&lt;sc-sidemenu&gt;</code></h4>
          ${this.buildMethods( 'SimpleSidemenu' )}

          <h4><code>&lt;sc-sidemenu-group&gt;</code></h4>
          ${this.buildMethods( 'SimpleSidemenuGroup' )}
        </div>


        <div class="variables">
          <h3 class="lined">CSS Variables</h3>
          <p></p>
          <sc-table
              id="sidemenu-styles"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.varFields)}
          >
          </sc-table>
        </div>


        <div id="examples" class="examples">
          <h3 class="lined">Examples</h3>
          <h4>Flexbox layout</h4>
          <p>In a flexbox, the sidemenu takes the minimum space required</p>
          <div class="example" style="display:flex;">
            <sc-sidemenu>
              <sc-sidemenu-item label="Item 1">
                <div class="icon" slot="icon">${this.includeSvg(faIcon( faCogs ).html[0])}</div>
              </sc-sidemenu-item>
              <sc-sidemenu-group label="Group 1">
                <div class="icon" slot="icon">${this.includeSvg(faIcon( faQuestionCircle ).html[0])}</div>
                <sc-sidemenu-item label="Item 2">
                  <div class="icon" slot="icon">${this.includeSvg(faIcon( faTable ).html[0])}</div>
                </sc-sidemenu-item>
                <sc-sidemenu-item label="Item 3">
                  <div class="icon" slot="icon">${this.includeSvg(faIcon( faSave ).html[0])}</div>
                </sc-sidemenu-item>
                <sc-sidemenu-group label="Sub-Group 1">
                  <div class="icon" slot="icon">${this.includeSvg(faIcon( faBeer ).html[0])}</div>
                  <sc-sidemenu-item label="Item 4"></sc-sidemenu-item>
                </sc-sidemenu-group>
              </sc-sidemenu-group>
            </sc-sidemenu>
          </div>

          <div class="example">
            <h4>Block layout</h4>
            <p>In a traditional block layout, the sidemenu fills the provided space upto the maximum</p>
            <sc-sidemenu>
              <sc-sidemenu-item label="Item 1">
                <div class="icon" slot="icon">${this.includeSvg(faIcon( faCogs ).html[0])}</div>
              </sc-sidemenu-item>
              <sc-sidemenu-group label="Group 1">
                <div class="icon" slot="icon">${this.includeSvg(faIcon( faQuestionCircle ).html[0])}</div>
                <sc-sidemenu-item label="Item 2">
                  <div class="icon" slot="icon">${this.includeSvg(faIcon( faTable ).html[0])}</div>
                </sc-sidemenu-item>
                <sc-sidemenu-item label="Item 3">
                  <div class="icon" slot="icon">${this.includeSvg(faIcon( faSave ).html[0])}</div>
                </sc-sidemenu-item>
                <sc-sidemenu-group label="Sub-Group 1">
                  <div class="icon" slot="icon">${this.includeSvg(faIcon( faBeer ).html[0])}</div>
                  <sc-sidemenu-item label="Item 4"></sc-sidemenu-item>
                </sc-sidemenu-group>
              </sc-sidemenu-group>
            </sc-sidemenu>
          </div>
        </div>
      </div>
    `
  }

}


export { SidemenusView };

