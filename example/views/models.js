
import { SimpleView, SimpleButton, SimpleImage } from '../../lib/index.js';
SimpleButton.register();
SimpleImage.register();


class ModelsView extends SimpleView {


  static build() {
    return this.builder`
      <div id="models">
        <h2>Models</h2>
        <h4>Overview</h4>
        <p>Models are the data-containing components of the framework. Models are flexible, available with a range of behaviours.
        </p>
        <h4>Remote models</h4>
        <p>
          
        </p>
      </div>
    `
  }


}

export { ModelsView };

