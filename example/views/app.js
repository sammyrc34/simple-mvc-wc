



import DocParser from '../util/docparser.js';
import { SimpleButton } from '../../lib/index.js';
import { ExampledView } from './example.js';

import rawcode from '!raw-loader!../../lib/app.js';

let __docs = undefined;

SimpleButton.register();


class AppView extends ExampledView {

  static title() {
    return "Application";
  }

  static methodNames( ) {
    return [ 'constructor', 'registerView', 'registerController', 'registerModel', 'registerStyle', 'fetchController', 'fetchModel', 'fetchStyles', 'fetchWebsock', 'clearModels', 'scrollTo', 'start' ];
  }

  static build() {

    return this.builder`
      <div id="app-page">
        <h2>Application</h2>
        <h3 class="lined">Description</h3>
        <p>The <code>SimpleApp</code> class is the core of your web application.</p>
        <p>Powered by <a href="https://lit.dev" target="_blank">Lit</a>, this library uses the well known MVC design pattern to deliver <em>Views</em> to application users, using <em>Controllers</em> to contain logic and business rules, and <em>Models</em> to work with underlying data and interface with data sources.</p>

        <h3 class="lined">Example</h3>
        <p>This basic example should be executed upon the browser's <code>DOMContentLoaded</code> event. Achieving this is implementation and stack dependent.</p>
        <div class="codeblock">
          <pre>import {SimpleApp} from 'simple-mvc-wc';

import {BasicView} from '....';

class BasicApp extends SimpleApp {
  start() {
    this.registerView( 'start', BasicView );
    super.start();
  }
}

const app = new BasicApp( {
  name          : 'Basic App',
  container     : 'container',
  prefix        : 'basicapp',
  defaultpage   : 'start',
  debug         : false
} );

app.start();</pre>
        </div>
        <p>This example would start an application that will render into the HTML element ID <code>container</code>. The application will have URL prefix of <code>basicapp</code>, containing a single view named <code>start</code>, and redirect the browser to the <var>defaultpage</var> page <code>/basicapp/start</code> if there is no other URL specified by the browser.</p>


        <div class="methods">
          <h3 class="lined">Methods</h3>
          ${this.buildMethods( 'SimpleApp' )}
        </div>

      </div>
    `
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }
}


export { AppView };

