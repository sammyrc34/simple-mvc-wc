
import DocParser from '../util/docparser.js';
import { SimplePopup, SimpleButton, SimpleInput, SimpleSelect, SimpleSelectOption, SimpleTable } from '../../lib/index.js';
import { ExampledView } from './example.js';
SimpleButton.register();
SimplePopup.register();
SimpleInput.register();
SimpleSelect.register();
SimpleSelectOption.register();
SimpleTable.register();


import rawcode from '!raw-loader!../../lib/components/popup.js';

let __docs = undefined;

const methodNames = {
  'SimplePopup': [ 'show', 'hide' ],
  'SimpleConfirm': [ 'promptWait' ],
};

const eventNames = {
  'SimplePopup': [ 'open', 'close' ],
  'SimpleConfirm': [ 'open', 'close', 'positive', 'negative' ]
};



class PopupsView extends ExampledView {

  static title() {
    return "Popups";
  }

  static propertyElements() {
    return [ 'sc-popup', 'sc-confirm' ];
  }

  static propertyTables() {
    return [ 'popup-properties', 'confirm-properties' ]
  }

  static varElements() {
    return [ 'sc-popup' ];
  }

  static varTables() {
    return [ 'popup-styles' ]
  }

  static methodNames( className ) {
    return methodNames[ className ];
  }

  static eventNames( className ) {
    return eventNames[ className ];
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }

  static build() {
    return this.builder`
      <div id="popups-page">
        <h2>Popups</h2>
        <div>
          <h3 class="lined">Description</h3>
          <p>The <code>&lt;sc-popup&gt;</code> and derivative <code>&lt;sc-confirm&gt;</code> components are floating display elements able to provide information and request information from a user.</p>
          <p>The <code>&lt;sc-popup&gt;</code> component is more flexible, able to contain a range of content, and offers options to control positioning, modality, and more.</p>
          <p>The <code>&lt;sc-confirm&gt;</code> component is relatively simpler, used for providing a confirmation prompt to the user with an easy to use interface.</p>
        </div>


        <h3 class="lined">Try it out</h3>
        <div class="popups-tryit">

          <div>
            <sc-input class="block" id="modal-toggle" name="modal" type="toggle" valuetype="number" label="Modal"></sc-input>
            <sc-input class="block" id="autoclose-toggle" name="autoclose" type="toggle" valuetype="number" label="Auto-close"></sc-input>
            <sc-input class="block" id="draggable-toggle" name="draggable" type="toggle" valuetype="number" label="Draggable"></sc-input>
            <sc-input class="block" id="noanimate-toggle" name="noanimate" type="toggle" valuetype="number" label="No animate?"></sc-input>
            <sc-select class="block" id="position-select" name="position" label="Position" required smalllabel>
              <sc-select-option label="center"       value="center" selected></sc-select-option>
              <sc-select-option label="topleft"      value="topleft"></sc-select-option>
              <sc-select-option label="topmiddle"    value="topmiddle"></sc-select-option>
              <sc-select-option label="topright"     value="topright"></sc-select-option>
              <sc-select-option label="middleleft"   value="middleleft"></sc-select-option>
              <sc-select-option label="middleright"  value="middleright"></sc-select-option>
              <sc-select-option label="bottomleft"   value="bottomleft"></sc-select-option>
              <sc-select-option label="bottommiddle" value="bottommiddle"></sc-select-option>
              <sc-select-option label="bottomright"  value="bottomright"></sc-select-option>
            </sc-select>
            <sc-button class="block" label="Open Popup" hover @click=${() => this.showPopup()}></sc-button>
            <sc-popup id="example-popup">
              <div slot="header">
                <span>Example popup header</span>
              </div>
              <div style="min-width: 400px;" slot="body">
                <p>This is some content in a popup l which could contain anything.</p>
                <div style="display: flex; flex-direction: column; align-items: center; justify-items: center;">
                  <div style="width: 50px; height: 50px; margin: 5px; background-color: red;"></div>
                  <div style="width: 50px; height: 50px; margin: 5px; background-color: green;"></div>
                  <div style="width: 50px; height: 50px; margin: 5px; background-color: blue;"></div>
                  <sc-button class="block" label="Close popup" hover @click=${this.close}></sc-button>
                  <p><sub>In case autoclose and close button is disabled</sub></p>
                </div>

              </div>
            </sc-popup>
          </div>

        </div>


        <div class="properties">
          <h3 class="lined">Properties</h3>
          <h4><code>&lt;sc-popup&gt;</code></h4>
          <sc-table
              id="popup-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>

        <div class="properties">
          <h4><code>&lt;sc-confirm&gt;</code></h4>
          <sc-table
              id="confirm-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>


        <div class="events">
          <h3 class="lined">Events</h3>
          <h4><code>&lt;sc-popup&gt;</code></h4>
          ${this.buildEvents( 'SimplePopup' )}

          <h4><code>&lt;sc-confirm&gt;</code></h4>
          ${this.buildEvents( 'SimpleConfirm' )}
        </div>


        <div class="methods">
          <h3 class="lined">Methods</h3>
          <h4><code>&lt;sc-popup&gt;</code></h4>
          ${this.buildMethods( 'SimplePopup' )}

          <h4><code>&lt;sc-confirm&gt;</code></h4>
          ${this.buildMethods( 'SimpleConfirm' )}
        </div>


        <div class="variables">
          <h3 class="lined">CSS Variables</h3>
          <p></p>
          <sc-table
              id="popup-styles"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.varFields)}
          >
          </sc-table>
        </div>

      </div>
    `
  }

  static close() {
    var popup = document.getElementById( 'example-popup' );
    popup.hide();
  }

  static showPopup( ) {

    const popup = document.getElementById( 'example-popup' );
    const position = document.getElementById( 'position-select' );
    const modal = document.getElementById( 'modal-toggle' );
    const autoclose = document.getElementById( 'autoclose-toggle' );
    const draggable = document.getElementById( 'draggable-toggle' );
    const noanimate = document.getElementById( 'noanimate-toggle' );

    popup.modal = ( modal.value == 1 ) ? true : false;
    popup.autoclose = ( autoclose.value == 1 ) ? true : false;
    popup.draggable = ( draggable.value == 1 ) ? true : false;
    popup.noanimate = ( noanimate.value == 1 ) ? true : false;
    popup.position = position.value;
    popup.show();
  }


}


export {
  PopupsView
}

