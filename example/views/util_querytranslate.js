

import DocParser from '../util/docparser.js';
import { ExampledView } from './example.js';


import rawcode from '!raw-loader!../../lib/util/querytranslate.js';
import filtercode from '!raw-loader!../../lib/models/collection.js';



let __docs = undefined;



class UtilQueryTranslateView extends ExampledView {

  static methodNames( ) {
    return [ 'constructor', 'getFilter', 'getBinds', 'getSort' ];
  }

  static typeNames() {
    return [ 'FieldCriteriaType', 'FieldCriteriaCollection', 'FilterType', 'FilterList', 'FiltersCollection', 'SortCriteria', 'SortsType' ] ;
  }

  static getDocs() {
    let otherdocs;
    if ( __docs == undefined ) {
      otherdocs = filtercode.substr( 0, filtercode.indexOf( '\nclass BasicCollection' ) ); 
      __docs = DocParser.parseJs( otherdocs.concat( rawcode ) );
    }
    return __docs
  }


  static build() {
    return this.builder`
      <div id="util-querytranslate-page">
        <h2>QueryTranslate Utility</h2>
        <h3 class="lined">Description</h3>
        <p>The <code>QueryTranslate</code> class is a utility to translate filters and sorts from <code>IntegratedModel</code> based models into database aware SQL.</p><br/>

        <p><b>Note: </b>The <code>QueryTranslate</code> class does not support javascript sort or filter functions, and does not support the <var>has</var> filter criteria type.</p>

        <div class="types">
          <h3 class="lined">Types</h3>
          <p>Types per <a href="model_collections"><code>SimpleCollection</code></a> class.</p>
          ${this.buildTypes()}
        </div>


        <div class="methods">
          <h3 class="lined">Methods</h3>
          <p></p>
          ${this.buildMethods( 'QueryTranslate' )}
        </div>


      </div>
    `
  }


}

export { UtilQueryTranslateView };

