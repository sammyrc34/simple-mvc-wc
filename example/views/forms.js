

import DocParser from '../util/docparser.js';
import { SimpleForm, SimpleInput, SimpleSelect, SimpleSelectList, SimpleSelectOption, SimpleTextarea, SimpleTable } from '../../lib/index.js';
import { ExampledView } from './example.js';
SimpleForm.register();
SimpleInput.register();
SimpleSelect.register();
SimpleSelectList.register();
SimpleSelectOption.register();
SimpleTextarea.register();
SimpleTable.register();

import rawcode from '!raw-loader!../../lib/components/form.js';

let __docs = undefined;

class FormsView extends ExampledView {

  static title() {
    return "Forms";
  }

  static propertyElements() {
    return [ 'sc-form' ];
  }
  static propertyTables() {
    return [ 'form-properties' ]
  }

  static methodNames( ) {
    return [ 'setDisabledElement', 'reset', 'clear', 'submit', 'validate', 'collectValues', 'changed', 'applyChanges' ];
  }

  static eventNames( ) {
     return [ 'before-submit', 'submit', 'after-submit', 'failed-submit', 'reset', 'loaded' ];
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }

  static build() {
    return this.builder`
      <div id="forms-page" style="display: flex; flex-direction: column;">

        <div>
          <h3 class="lined">Description</h3>
          <p>The <code>&lt;sc-form&gt;</code> component is a semi-interactive element to contain and work with other provided input-type controls including <code>&lt;sc-input&gt;</code>, <code>&lt;sc-textarea&gt;</code>, <code>&lt;sc-select&gt;</code> and <code>&lt;sc-select-list&gt;</code>. Submission and reset can also be instigated by <code>&lt;sc-button&gt;</code> components with a type of 'submit' or 'reset'.</p>

          <p>As an optionally data-bound component, the <code>&lt;sc-form&gt;</code> componenti can provide an integrated interface between data sources and controls, allowing for the easy data display, modification and creation</p>
        </div>

        <div class="properties">
          <h3 class="lined">Properties</h3>
          <p></p>
          <sc-table
              id="form-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>


        <div class="events">
          <h3 class="lined">Events</h3>
          ${this.buildEvents( 'SimpleForm' )}
        </div>


        <div class="methods">
          <h3 class="lined">Methods</h3>
          ${this.buildMethods( 'SimpleForm' )}
        </div>


        <div id="examples" class="examples">
          <h3 class="lined">Examples</sc-button></h3>

          <h4>Simple unbound form</h4>
          <p>A basic example without data model binding. Input is automatically validated upon submission. The collected values are displayed if validation is successful.</p>
          <div class="flex-row-start">
            <div class="flex-col" style="min-width: 400px;">
              <sc-form name="form1" autovalidate @submit=${this.form1AfterSubmit}>
                <div>
                  <sc-input name="email" type="email" label="Email address" required class="block"></sc-input>
                  <sc-input name="password" type="password" label="Password" required class="block"></sc-input>
                  <sc-select name="region" label="Region" required>
                    <sc-select-option value="EU" label="Europe" selected></sc-select-option>
                    <sc-select-option value="USA" label="USA"></sc-select-option>
                    <sc-select-option value="ASIA" label="Oceania"></sc-select-option>
                  </sc-select>
                  <sc-input name="private" type="checkbox" label="Private computer?" class="block"></sc-input>
                </div>
                <div>
                  <sc-button name="reset" label="Reset" type="reset" ></sc-button>
                  <sc-button name="submit" label="Submit" type="submit"></sc-button>
                </div>
              </sc-form>
            </div>
            <div class="flex-col">
              <sc-textarea name="form1result" label="Submit results" smalllabel readonly></sc-textarea>
            </div>
          </div>

          <div style="height: 30px"></div>

          <h4>Simple bound form</h4>
          <p>An example of a data model bound form with default submission handling to update the data model. Select people on the right to modify records in the form.</p>
          <div class="flex-row-start">
            <div class="flex-col" style="min-width: 400px;">
              <sc-form id="boundform1" name="boundForm" modelName="form1" autovalidate>
                <div>
                  <sc-input name="name" type="text" label="Name"  required class="block"></sc-input>
                  <sc-input name="number" type="number" valuetype="number" label="Number" required class="block"></sc-input>
                  <sc-select-list name="color" label="Colour" required .data=${this.colourData}  valueField="value" textField="text" field="color" nofill comment="Select one" class="block" style="--select-list-height: 170px;"></sc-select-list>
                  <sc-select name="fruit" .data=${this.fruitData} textField="text" label="Fruit" valueField="value" textField="text" field="fruit" maxselect=0 smalllabel class="block"></sc-select>
                  <sc-input name="other" type="checkbox" label="Option?" emptyasnull defaulttrue="else" class="block"></sc-input>
                  <div>
                    <sc-button name="reset" label="Reset" type="reset" ></sc-button>
                    <sc-button name="submit" label="Submit" type="submit"></sc-button>
                  </div>
                </div>
              </sc-form>
            </div>

            <div class="flex-col">
              <div style="width: 180px;">
                <sc-select-list
                  fillheight
                  label="People"
                  modelName="form1"
                  textField="name"
                  valueField="id"
                  valuetype="number"
                  maxselect=1
                  emptyasnull
                  @after-select=${this.updateForm}
                >
                </sc-list>
              </div>
            </div>
          </div>
        </div>
      </div>
    `
  }


  static form1AfterSubmit( ev ) {
    const out = document.querySelector( 'sc-textarea[name=form1result]' );
    const vals = ev.target.collectValues();
    out.value = JSON.stringify( vals );
  }


  static updateForm(ev) {
    const selected = ev.target.value;
    if ( selected == null )
      return;
    document.getElementById( 'boundform1' ).bindId = selected;
  }


  static get fruitData() {
    return [
      { value: "apple", text: "Apple" },
      { value: "banana", text: "Banana" },
      { value: "pear", text: "Pear" },
      { value: "peach", text: "Peach" }
    ];
  }


  static get colourData() {
    return [
      { value: 'red', text: "Red" },
      { value: 'green', text: "Green" },
      { value: 'blue', text: "Blue" },
      { value: 'purple', text: "Purple" }
    ];
  }


  static get colourTemplate() {
    return (item) => {
      return this.builder`
        <div><span>${item.text}</span></div>
      `
    }
  }


}


export {
  FormsView
}

