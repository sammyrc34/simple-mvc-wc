
import DocParser from '../util/docparser.js';
import { SimpleButton, SimpleInput, SimpleSelect, SimpleSelectList, SimpleSelectOption, SimpleTab, SimpleTabsContainer, SimpleTable } from '../../lib/index.js';
import { ExampledView } from './example.js';
SimpleButton.register();
SimpleSelect.register();
SimpleSelectList.register();
SimpleSelectOption.register();
SimpleInput.register();
SimpleTab.register();
SimpleTabsContainer.register();
SimpleTable.register();

import rawcode from '!raw-loader!../../lib/components/select.js';

let __docs = undefined;

const methodNames = {
  'SimpleSelect': [ 'reset', 'value', 'changed', 'setInitial', 'focus', 'validate' ],
  'SimpleSelectList': [ 'reset', 'value', 'changed', 'setInitial', 'focus', 'validate' ],
  'SimpleSelectOption': [ 'toggleOption' ]
};

const eventNames = {
  'SimpleSelect': [ 'before-select', 'after-select', 'after-change' ],
  'SimpleSelectList': [ 'before-select', 'after-select', 'after-change' ]
};

const example = `<sc-select label="Basic select">
  <sc-select-option value=1 label="One"></sc-select-option>
  <sc-select-option value=2 label="Two"></sc-select-option>
  <sc-select-option value=3 label="Three"></sc-select-option>
</sc-select>`;

class SelectsView extends ExampledView {

  static title() {
    return "Selects";
  }

  static propertyElements() {
    return [ 'sc-select', 'sc-select-list', 'sc-select-option' ];
  }
  static propertyTables() {
    return [ 'select-properties', 'select-list-properties',  'option-properties' ]
  }

  static varElements() {
    return [ 'sc-select' ];
  }

  static varTables() {
    return [ 'select-styles' ]
  }

  static methodNames( className ) {
    return methodNames[ className ];
  }

  static eventNames( className ) {
    return eventNames[ className ];
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }

  static build() {
    return this.builder`
      <div id="selects-page">
        <h2>Selects</h2>
        <div>
          <h3 class="lined">Description</h3>
          <p>The <code>&lt;sc-select&gt;</code>, <code>&lt;sc-select-list&gt;</code> and associated <code>&lt;sc-select-option&gt;</code> are user interactable controls for selecting one or more entries from a list of entries.</p>
          <p>As an optionally data-bound component, the <code>&lt;sc-select&gt;</code> and <code>&lt;sc-select-list&gt;</code> components can accept a data model to provide the options data source.</p>
          <h3 class="lined">Try it out</h3>
          ${super.build( example )}
        </div>

        <div class="properties">
          <h3 class="lined">Properties</h3>
          <h4><code>&lt;sc-select&gt;</code></h4>
          <sc-table
              id="select-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>

        <div class="properties">
          <h4><code>&lt;sc-select-list&gt;</code></h4>
          <sc-table
              id="select-list-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>

        <div class="properties">
          <h4><code>&lt;sc-select-option&gt;</code></h4>
          <sc-table
              id="option-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>


        <div class="events">
          <h3 class="lined">Events</h3>

          <h4><code>&lt;sc-select&gt;</code></h4>
          ${this.buildEvents( 'SimpleSelect' )}

          <h4><code>&lt;sc-select-list&gt;</code></h4>
          ${this.buildEvents( 'SimpleSelectList' )}
        </div>


        <div class="methods">
          <h3 class="lined">Methods</h3>
          <h4><code>&lt;sc-select&gt;</code></h4>
          ${this.buildMethods( 'SimpleSelect' )}

          <h4><code>&lt;sc-select-list&gt;</code></h4>
          ${this.buildMethods( 'SimpleSelectList' )}

          <h4><code>&lt;sc-select-option&gt;</code></h4>
          ${this.buildMethods( 'SimpleSelectOption' )}
        </div>


        <div class="variables">
          <h3 class="lined">CSS Variables</h3>
          <p></p>
          <sc-table
              id="select-styles"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.varFields)}
          >
          </sc-table>
        </div>


        <div id="examples" class="examples" @click=${this.chooseExample.bind( this )}>
          <h3 class="lined">Examples</h3>
          <p>Select and try out any example:<sc-button id="choose-button" type="toggle" label="Choose example..." @click=${this.setChooseMode}></sc-button></p>
          <h4>Selects</h4>

          <div class="example-row">

            <div class="example">
              <sc-select name="select1" label="Simple select" emptyasnull required value=2>
                <sc-select-option value="1" label="One"></sc-select-option>
                <sc-select-option value="2" label="Two"></sc-select-option>
                <sc-select-option value="3" label="Three"></sc-select-option>
              </sc-select>
            </div>

            <div class="example">
              <sc-select name="select2" label="Color" placeholder="Select two colors..." maxselect=2 modelName="smallcolours" valueField="value" textField="text" .optionTemplate=${this.getColorTemplate}>
                <sc-select-option value="multi" label="Multi">
                  <div style="display: flex; align-items: center; width: 100%;">
                    <div style="width: 20px; height: 0.75em; display: inline-block; margin: 0px 1em; background-image: linear-gradient(to right,red,orange,yellow,green,blue,indigo,violet); border: 1px solid grey;">
                    </div>
                    <span>Multi-colour</span>
                  </div>
                </sc-select-option>
              </sc-select>
            </div>


            <div class="example">
              <sc-select name="select3" label="Text colours" required search modelName="colours" textField="text" valueField="value" maxselect=3>
                <sc-select-option value="multi" label="Multi"></sc-select-option>
              </sc-select>
            </div>


            <div class="example">
              <sc-select name="select4" label="Non-animated Colours" noanimate search modelName="colours" .optionTemplate=${this.getColorTemplate} textField="text" valueField="value"></sc-select>
            </div>
          </div>


          <div class="example-row">

            <div class="example">
              <sc-select name="select10" label="Tag values" placeholder="Select one or more" valuestyle="tags" maxselect=3>
                <sc-select-option value="1" label="One"></sc-select-option>
                <sc-select-option value="2" label="Two" selected></sc-select-option>
                <sc-select-option value="3" label="Three"></sc-select-option>
              </sc-select>
            </div>

            <div class="example">
              <sc-select name="select11" label="Autovalidated" required autovalidate placeholder="Select one or more" errormessage="Please select an option" >
                <sc-select-option value="1" label="One"></sc-select-option>
                <sc-select-option value="2" label="Two"></sc-select-option>
                <sc-select-option value="3" label="Three"></sc-select-option>
              </sc-select>
            </div>

            <div class="example">
              <sc-select name="select5" label="With Validation" required errormessage="An option must be selected" comment="Please select an option" >
                <sc-select-option value="1" label="One"></sc-select-option>
                <sc-select-option value="2" label="Two"></sc-select-option>
                <sc-select-option value="3" label="Three"></sc-select-option>
              </sc-select>
            </div>

            <sc-button style="padding: 5px;" label="Validate" @click=${() => {document.querySelector( 'sc-select[name=select5]' ).validate()}}></sc-button>
            <sc-button style="padding: 5px;" label="Clear" @click=${() => {document.querySelector( 'sc-select[name=select5]' ).reset()}}></sc-button>


          </div>

          <div class="example-row" style="position: relative;">

            <div class="example">
              <sc-select name="select6" label="Relative positioned">
                <sc-select-option value="1" label="One" selected></sc-select-option>
                <sc-select-option value="2" label="Two"></sc-select-option>
                <sc-select-option value="3" label="Three"></sc-select-option>
              </sc-select>
            </div>

            <div class="example">
              <sc-select name="select7" label="Long labels are truncated" required>
                <sc-select-option value="1" label="One label which is very very long"></sc-select-option>
                <sc-select-option value="2" label="Two label which is very very long"></sc-select-option>
                <sc-select-option value="3" label="Three label which is very very long"></sc-select-option>
              </sc-select>
            </div>

            <div class="example">
              <sc-select name="select8" label="Missing model" required search modelName="missing" textField="text"></sc-select>
            </div>

            <div class="example">
              <sc-select name="select9" disabled label="Disabled select" required>
                <sc-select-option value="1" label="One"></sc-select-option>
                <sc-select-option value="2" label="Two"></sc-select-option>
                <sc-select-option value="3" label="Three"></sc-select-option>
              </sc-select>
            </div>
          </div>

          <h4>Select Lists</h4>
          <div class="example-row">

            <div class="example">
              <sc-select-list name="select-list1" label="Simple Select List" required>
                <sc-select-option value="1" label="One"></sc-select-option>
                <sc-select-option value="2" label="Two" selected></sc-select-option>
                <sc-select-option value="3" label="Three"></sc-select-option>
              </sc-select-list>
            </div>

            <div class="example">
              <sc-select-list name="select-list2" label="Select 2 Colors" valuestyle="tags" maxselect=2 modelName="smallcolours" valueField="value" textField="text" .optionTemplate=${this.getColorTemplate}>
                <sc-select-option value="multi" label="Multi">
                  <div style="display: flex; align-items: center; width: 100%;">
                    <div style="width: 20px; height: 0.75em; display: inline-block; margin: 0px 1em; background-image: linear-gradient(to right,red,orange,yellow,green,blue,indigo,violet); border: 1px solid grey;">
                    </div>
                    <span>Multi-colour</span>
                  </div>
                </sc-select-option>
              </sc-select-list>
            </div>

            <div class="example">
              <sc-select-list name="select-list3" label="Text colours" required search modelName="colours" textField="text" maxselect=1 valueField="value" ></sc-select-list>
            </div>

            <div class="example">
              <sc-select-list name="select-list4" label="Autovalidated" required errormessage="An option must be selected" autovalidate comment="Please select an option" >
                <sc-select-option value="1" label="One"></sc-select-option>
                <sc-select-option value="2" label="Two"></sc-select-option>
                <sc-select-option value="3" label="Three"></sc-select-option>
              </sc-select-list>
            </div>

          </div>

          <div class="example-row">

            <div class="example">
              <sc-select-list name="select-list5" disabled label="Disabled Select List" required>
                <sc-select-option value="1" label="One" selected></sc-select-option>
                <sc-select-option value="2" label="Two"></sc-select-option>
                <sc-select-option value="3" label="Three"></sc-select-option>
              </sc-select-list>
            </div>

          </div>
        </div>

      </div>
    </div>
    `
  }


  static get getColorTemplate() {
    return (opt) => {
      return this.builder`<div style="display: flex; align-items: center; width: 100%;"><div style="width: 20px; height: 0.75em; display: inline-block; margin: 0px 1em; background-color: ${opt.value}; border: 1px solid grey;"></div><span>${opt.text}</span></div>`
    }
  }

}


export { SelectsView }

