

import DocParser from '../util/docparser.js';
import { SimpleTable, SimpleButton, SimpleConfirm } from '../../lib/index.js';
import { ExampledView } from './example.js';

SimpleConfirm.register();
SimpleTable.register();
SimpleButton.register();


import rawcode from '!raw-loader!../../lib/components/table.js';

let __docs = undefined;


class TablesView extends ExampledView {

  static get styles() {
    return this.cssBuilder`
        .sc-table table {
          table-layout: fixed;
        }
        .inline {
          display: inline-block;
        }
        thead .column-name {
          width: 20%;
        }
        thead .column-email {
          width: 30%;
        }
        thead .column-colour {
          width: 30%;
        }
        thead .column-id {
          min-width: unset;
          width: 50px;
        }
        thead .column-actions {
          text-align: center;
          width: 75px;
        }
        thead .column-id {
          min-width: unset;
          width: 60px;
        }
        thead .column-title {
          min-width: unset;
          width: 20%;
        }
        thead .column-colour select {
          width: 100%;
        }
        tbody td .icon.fa {
          margin-left: 5px;
        }
        div.icon.edit {
          height: var( --theme-text-size );
          width: var( --theme-text-size );
          background-image: url( "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCIgZmlsbD0iYmxhY2siIHdpZHRoPSIxOHB4IiBoZWlnaHQ9IjE4cHgiPjxwYXRoIGQ9Ik0wIDBoMjR2MjRIMHoiIGZpbGw9Im5vbmUiLz48cGF0aCBkPSJNMyAxNy4yNVYyMWgzLjc1TDE3LjgxIDkuOTRsLTMuNzUtMy43NUwzIDE3LjI1ek0yMC43MSA3LjA0Yy4zOS0uMzkuMzktMS4wMiAwLTEuNDFsLTIuMzQtMi4zNGMtLjM5LS4zOS0xLjAyLS4zOS0xLjQxIDBsLTEuODMgMS44MyAzLjc1IDMuNzUgMS44My0xLjgzeiIvPjwvc3ZnPg==" );
        }
        div.icon.delete {
          height: var( --theme-text-size );
          width: var( --theme-text-size );
          background-image: url( "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCIgZmlsbD0iYmxhY2siIHdpZHRoPSIxOHB4IiBoZWlnaHQ9IjE4cHgiPjxwYXRoIGQ9Ik0wIDBoMjR2MjRIMHoiIGZpbGw9Im5vbmUiLz48cGF0aCBkPSJNMCAwaDI0djI0SDBWMHoiIGZpbGw9Im5vbmUiLz48cGF0aCBkPSJNNiAxOWMwIDEuMS45IDIgMiAyaDhjMS4xIDAgMi0uOSAyLTJWN0g2djEyem0yLjQ2LTcuMTJsMS40MS0xLjQxTDEyIDEyLjU5bDIuMTItMi4xMiAxLjQxIDEuNDFMMTMuNDEgMTRsMi4xMiAyLjEyLTEuNDEgMS40MUwxMiAxNS40MWwtMi4xMiAyLjEyLTEuNDEtMS40MUwxMC41OSAxNGwtMi4xMy0yLjEyek0xNS41IDRsLTEtMWgtNWwtMSAxSDV2MmgxNFY0eiIvPjwvc3ZnPg==");
        }
        td[data-field=actions] {
          padding: 0px;
          padding-left: 5px;
        }
    `
  }

  static get fields() {
    return SimpleTable.toFieldDefintions( [
      { label: "ID",          field: 'id',          sortable: true,   sorttype: "number" },
      { label: "Title",       field: 'title',       sortable: true,   sorttype: "string",   showfilter: true,   filtertype: "select", multiselect: true,    filtercase: true },
      { label: "Name",        field: 'name',        sortable: true,   sorttype: "string",   searchable: true,   filtertype: "string", quicksearch: true, filtercase: false },
      { label: "Email",       field: "email",       sortable: true,   sorttype: "string",   searchable: true,   filtertype: "string", filtermode: "AND", quicksearch: true },
      { label: "Colours",     field: "colour",      template: (arr) => arr.join(", "),      sortable: false,    filtertype: "custom", customfilter: this.colorFilter.bind( this ), customfiltertype: 'has' },
    ] );
  }

  static colorFilter( table, curVal ) {
    const colorsModel = this.app.fetchModel( 'colours' );
    const colors = colorsModel.getRecords();
    return this.builder`
      <select @change=${(ev) => this.applyColor( ev, table )}>
        <option value="" ?selected=${curVal == "" || curVal === undefined}>Any</option>
        ${colors.map( (color) => this.builder`<option value=${color.value} ?selected=${color.value == curVal}>${color.text}</option>` )}
      </select>
    `;
  }


  static applyColor( ev, table ) {

    // Any standard input/select element or element with a value attribute
    // can call default method with the change event, otherwise set the filter
    // value direct on the table
    return table._doFieldFilter( ev );
  }


  static title() {
    return "Tables";
  }

  static propertyElements() {
    return [ 'sc-table' ];
  }

  static propertyTables() {
    return [ 'table-properties' ];
  }

  static varElements() {
    return [ 'sc-table' ];
  }

  static varTables() {
    return [ 'table-styles' ]
  }

  static methodNames( ) {
    return [ 'toFieldDefintions', 'total', 'length', 'clear', 'reset', 'applySort', 'applyFieldFilter' ];
  }

  static eventNames( ) {
    return [ 'updated' ];
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }

  static get columnFields() {
    return [
      { label: "Property",    field: 'prop' },
      { label: "Type",        field: 'type' },
      { label: "Default",     field: 'def' },
      { label: "Description", field: "desc", enablehtml: true },
    ]
  }

  static build() {
    return this.builder`
      <div id="tables-page">
        <h2>Tables</h2>
        <div>
          <h3 class="lined">Description</h3>
          <p>The <code>&lt;sc-table&gt;</code> component is an interactive data tbale type control for display and interaction of large data sets that could include pagination, sorting, filtering and more.</p>
        </div>

        <div class="properties">
          <h3 class="lined">Properties</h3>
          <h4><code>&lt;sc-table&gt;</code></h4>
          <sc-table
              id="table-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>

         <div class="properties">
          <h4>Column definition properties</h4>
          <sc-table
              id="column-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.columnFields)}
          >
          </sc-table>
        </div>


        <div class="events">
          <h3 class="lined">Events</h3>
          ${this.buildEvents( 'SimpleTable' )}
        </div>


        <div class="methods">
          <h3 class="lined">Methods</h3>
          <h4><code>&lt;sc-table&gt;</code></h4>
          ${this.buildMethods( 'SimpleTable' )}
        </div>


        <div class="variables">
          <h3 class="lined">CSS Variables</h3>
          <p></p>
          <sc-table
              id="table-styles"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.varFields)}
          >
          </sc-table>
        </div>


        <div id="examples" class="examples">
          <h3 class="lined">Examples</h3>

          <div style="display: flex; flex-direction: column; margin-right: var( --theme-space-size );">
            <h4>Paginated table with quick search, sorting and filtering</h4>
            <sc-table
              showpages
              showempty
              showrange
              searchmode="OR"
              editable
              range=10
              showsearch
              multisort
              scrollable
              select="multi"
              modelName="example"
              sorttypes='["number","string"]'
              sortfields='["id", "name"]'
              sortdirections='["asc", "asc"]'
              ranges='[10,20,50]'
              .fieldDefinitions=${this.fields}
              .tableStyles=${this.styles}>
            </sc-table>
          </div>

      </div>
    `
  }


  static updateProps() {
    const columnTable = document.getElementById( 'column-properties' );
    let columnProps = SimpleTable.columnProperties;
    let propname, data = [];
    super.updateProps();

    for ( let property in columnProps ) {
      propname = property;
      if ( ! columnProps[ property ].desc )
        continue;
      data.push( {
        prop: propname,
        type: columnProps[ property ].type.name,
        def:  this.textifyValue( columnProps[ property ].def ),
        desc: columnProps[ property ].desc
      } );
    }

    columnTable._data = data;
  }


}


export { TablesView };

