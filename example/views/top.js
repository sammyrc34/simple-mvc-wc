

import { SimpleView } from '../../lib/index.js';

import HeaderView from './header.js';
import SidebarView from './sidebar.js';
import FooterView from './footer.js';

class TopView extends SimpleView {

  static build( ) {

    return this.builder`
      ${HeaderView.build()}
      ${SidebarView.build()}

      <div id="main-wrap">
        <div id="main-pane">
        </div>
      </div>

      ${FooterView.build()}
    `
  }

  static connect() {
    HeaderView.connect();
    SidebarView.connect();
    FooterView.connect();
  }

  static disconnect() {
    HeaderView.disconnect();
    SidebarView.disconnect();
    FooterView.disconnect();
  }

  static slotCount() {
    return 1; // One subview only
  }

  static canContain( ) {
    return true;
  }

  static getTargets( ) {
    const main = document.getElementById( 'main-pane' );
    return [ main ];
  }

}

export { TopView };

