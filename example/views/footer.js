
import { SimpleView } from '../../lib/index.js';


class FooterView extends SimpleView {


  static build() {
    return this.builder`
      <div id="footer" style="background-color: var( --theme-third-light );">
        <div class="flex-row flex-center">
          <div style="text-align: center;">
            <span style="font-size: small; line-height: 50px;">A footer containing some links, or a copyright notice.</span>
          </div>
        </div>
      </div>
    `
  }

  static connect() {
  }


  static disconnect() {
  }

}

export default FooterView;

