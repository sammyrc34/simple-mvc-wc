

import DocParser from '../util/docparser.js';
import { SimpleButton, SimpleTextarea, SimpleTable } from '../../lib/index.js';
import { ExampledView } from './example.js';

import { library as faLib, icon as faIcon } from "@fortawesome/fontawesome-svg-core";
import { faBeer, faTimes } from "@fortawesome/free-solid-svg-icons";
import { faFile, faSave } from "@fortawesome/free-regular-svg-icons";

import rawcode from '!raw-loader!../../lib/components/button.js';

let __docs = undefined;

faLib.add( faBeer, faTimes, faFile, faSave );

SimpleTextarea.register();
SimpleButton.register();
SimpleTable.register();


const example = `<sc-button label="Plain Button"></sc-button>`


class ButtonView extends ExampledView {

  static title() {
    return "Buttons";
  }

  static propertyElements() {
    return [ 'sc-button' ];
  }
  static propertyTables() {
    return [ 'button-properties' ]
  }

  static varElements() {
    return [ 'sc-button' ];
  }

  static varTables() {
    return [ 'button-styles' ]
  }

  static methodNames( ) {
    return [ 'focus' ];
  }

  static eventNames( ) {
    return [ 'click' ];
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }

  static build() {
    return this.builder`
      <div id="buttons-page">
        <h2>Buttons</h2>
        <div>
          <h3 class="lined">Description</h3>
          <p>The <code>sc-button</code> control is a user interactable button control which can be pressed via keyboard or mouse. Functionally similar yet simpler than vanilla html buttons.</p>
          <h3 class="lined">Try it out</h3>
          ${ExampledView.build( example )}
        </div>

        <div class="properties">
          <h3 class="lined">Properties</h3>
          <p></p>
          <sc-table
              id="button-properties"
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>

        <div class="events">
          <h3 class="lined">Events</h3>
          ${this.buildEvents( 'SimpleButton' )}
        </div>

        <div class="methods">
          <h3 class="lined">Methods</h3>
          ${this.buildMethods( 'SimpleButton' )}
        </div>


        <div class="variables">
          <h3 class="lined">CSS Variables</h3>
          <p></p>
          <sc-table
              id="button-styles"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.varFields)}
          >
          </sc-table>
        </div>


        <div id="examples" class="examples" @click=${this.chooseExample.bind( this )}>
          <h3 class="lined">Examples</h3>
          <p>Select and try out any example:<sc-button id="choose-button" type="toggle" label="Choose example..." @click=${this.setChooseMode}></sc-button></p>

          <h4>Basic buttons</h4>
          <div class="example-row">
            <div class="example">
              <sc-button name="one" label="Normal" ></sc-button>
            </div>
            <div class="example">
              <sc-button name="two" label="Disabled" disabled></sc-button>
            </div>
            <div class="example">
              <sc-button name="toggled" label="Toggled" type="toggle"></sc-button>
            </div>
          </div>


          <h4>Buttons with icons</h4>
          <div class="example-row">
            <div class="example">
              <sc-button label="Beer" icon="left">
                <div class="icon" slot="icon">${this.includeSvg(faIcon( faBeer ).html[0])}</div>
              </sc-button>
            </div>

            <div class="example">
              <sc-button label="Beer" icon="right">
                <div class="icon" slot="icon">${this.includeSvg(faIcon( faBeer ).html[0])}</div>
              </sc-button>
            </div>

            <div class="example">
              <sc-button label="Beer" icon="top">
                <div class="icon" slot="icon">${this.includeSvg(faIcon( faBeer ).html[0])}</div>
              </sc-button>
            </div>
          </div>


          <h4>Buttons without label</h4>
          <div class="example-row">
            <div class="example">
              <sc-button icon="only">
                <div class="icon" slot="icon">${this.includeSvg(faIcon( faSave ).html[0])}</div>
              </sc-button>
            </div>

            <div class="example">
              <sc-button icon="only">
                <div class="icon" slot="icon">${this.includeSvg(faIcon( faFile ).html[0])}</div>
              </sc-button>
            </div>

            <div class="example">
              <sc-button disabled icon="only">
                <div class="icon" slot="icon">${this.includeSvg(faIcon( faTimes ).html[0])}</div>
              </sc-button>
            </div>
          </div>


          <h4>Buttons with outlines or hover</h4>
          <div class="example-row">
            <div class="example">
              <sc-button label="Outlined" outline></sc-button>
            </div>
            <div class="example">
              <sc-button label="Hover" hover></sc-button>
            </div>
            <div class="example">
              <sc-button label="Both" hover outline></sc-button>
            </div>
          </div>


          <h4>Disabled buttons with outlines or hover</h4>
          <div class="example-row">
            <div class="example">
              <sc-button label="Outlined" outline disabled></sc-button>
            </div>
            <div class="example">
              <sc-button label="Hover" hover disabled></sc-button>
            </div>
            <div class="example">
              <sc-button label="Both" hover outline disabled></sc-button>
            </div>
          </div>

        </div>
      </div>
    `
  }


}



export {
  ButtonView
}

