

import DocParser from '../util/docparser.js';
import { ExampledView } from './example.js';


import rawcode from '!raw-loader!../../lib/models/remote.js';

let __docs = undefined;



class ModelRemoteView extends ExampledView {

  static methodNames( ) {
    return [ 'constructor', 'setup', 'failed', 'inconsistent', 'loaded', 'isChanged', 'getQuery', 'getContext', 'setContextQuery', 'getRecordInfo', 'getChanges', 'getRecord', 'load', 'reload', 'fetch', 'fetchOne', 'reset', 'clear', 'rollback', 'add', 'remove', 'update', 'persist', 'parse', 'newId', 'requiredFields' ];
  }

  static typeNames() {
    return [ 'LoadInfoType', 'PersistInfoType' ] ;
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }



  static build() {
    return this.builder`
      <div id="model-remote-page">
        <h2>Remote Models</h2>
        <h3 class="lined">Description</h3>
        <p>The <code>RemoteModel</code> class is a mixin, providing expanded functionality for a full-cache and remotely authoritative data model.</p>

        <p>While the same CRUD-like interface is maintained, changes made to the model are local until an explicit call to <code>persist</code>. Also, in order to retrieve records, the model must first attempt to <code>load</code>.</p>

        <p><strong>Note:</strong> as this is a full-cache model, it is not suitable for very large data sets.</p>


        <h3 class="lined">URLs</h3>
        <p>URLs provide the location of the data source to the model. While URL configurations are separate by action, the same URL can be specified as the operation is included in each request. Additionally, URLs can contain interpolated variable segments in the format of <var>:name:</var>. When making a request, the model substitutes each variable with the value from the model's <var>context</var>. The <var>context</var> and or <var>query</var> can be set either through an explicit call to <code>setContextQuery</code> or as an argument to <code>load</code>.</p>


        <h3 class="lined">Data API</h3>
        <p>The API for data fetching is aimed to be simple and functional. Initial data fetching occurs via a GET request to the <code>load</code> URL with a querystring containing <code>operation=load</code>. The result is expected to be of type <var>application/json</var> with the following structure:</p>
        <ul class="typedef">
          <li><div><var>success</var><var>{Boolean}</var></div> - If the request was successful</li>
          <li><div><var>message</var><var>{String}</var></div> - A message about why a request was not successful</li>
          <li><div><var>records</var><var>{Object[]}</var></div> - The resulting data records</li>
        </ul>

        <p>Subsequent persisting actions occur via POST requests with a JSON payload as follows:</p>
        <ul class="typedef">
          <li><div><var>operation</var><var>{String}</var></div> - The operation type, one of insert, delete or update</li>
          <li><div><var>records</var><var>{Object[]}</var></div> - The affected records (if collection model)</li>
          <li><div><var>record</var><var>{Object}</var></div> - The affected record (if record model)</li>
        </ul>

        <p>The expected response JSON structure is as follow:</p>
        <ul class="typedef">
          <li><div><var>success</var><var>{Boolean}</var></div> - If the request was successful</li>
          <li><div><var>message</var><var>{String}</var></div> - A message about why a request was not successful</li>
          <li><div><var>messages</var><var>{Object}</var></div> - Optional per-record error messages, keyed by record ID, value being the message</li>
          <li><div><var>record</var><var>{Object}</var></div> - One resulting data record</li>
          <li><div><var>records</var><var>{Object[]}</var></div> - The resulting data record updates</li>
        </ul>
        <p>Should the request be incomplete, records in the model have an error message set, either individually via the <code>messages</code> property, or a single record specified by the <code>record</code> property with the <code>message</code> specified.</p>
        <p><strong>Note:</strong> If using the <code>messages</code> property, the request is interpretted as partially successful and request records not included in the <code>messages</code> property will have changes applied.</p>



        <div class="types">
          <h3 class="lined">Types</h3>
          ${this.buildTypes()}
        </div>


        <div class="methods">
          <h3 class="lined">Methods</h3>
          <p></p>
          ${this.buildMethods( 'RemoteModel' )}
        </div>


      </div>
    `
  }


}

export { ModelRemoteView };

