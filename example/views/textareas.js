
import DocParser from '../util/docparser.js';
import { SimpleTextarea, SimpleTable } from '../../lib/index.js';
import { ExampledView } from './example.js';
SimpleTextarea.register();
SimpleTable.register();

import rawcode from '!raw-loader!../../lib/components/textarea.js';

let __docs = undefined;

const example = `<sc-textarea label="Textarea input" fill>Default value</sc-textarea>`;

class TextareasView extends ExampledView {

  static title() {
    return "Inputs";
  }

  static propertyElements() {
    return [ 'sc-textarea' ];
  }
  static propertyTables() {
    return [ 'textarea-properties' ]
  }

  static varElements() {
    return [ 'sc-textarea' ];
  }

  static varTables() {
    return [ 'textarea-styles' ]
  }

  static methodNames() {
    return [ 'value', 'reset', 'changed', 'setInitial', 'focus', 'validate' ];
  }

  static eventNames() {
    return [ 'after-input', 'after-change' ];
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }


  static build() {
    return this.builder`
      <div id="textareas-page">
        <h2>Text Areas</h2>
        <div>
          <h3 class="lined">Description</h3>
          <p>The <code>&lt;sc-textarea/&gt;</code> control is a user interactable input control for multi-line data entry and user interaction.</p>
          <h3 class="lined">Try it out</h3>
          ${ExampledView.build( example )}
        </div>

        <div class="properties">
          <h3 class="lined">Properties</h3>

          <p>Note: Unlike other input controls, this control has no value attribute. Set the text content of the control to provide a default.</p>

          <sc-table
              id="textarea-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>


        <div class="events">
          <h3 class="lined">Events</h3>
          ${this.buildEvents( 'SimpleTextarea' )}
        </div>


        <div>
          <h3 class="lined">Methods</h3>
          ${this.buildMethods( 'SimpleTextarea' )}
        </div>


        <div class="variables">
          <h3 class="lined">CSS Variables</h3>
          <p></p>
          <sc-table
              id="textarea-styles"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.varFields)}
          >
          </sc-table>
        </div>


        <div id="examples" class="examples" @click=${this.chooseExample.bind( this )}>
          <h3 class="lined">Examples</h3>
          <p>Select and try out any example:<sc-button id="choose-button" type="toggle" label="Choose example..." @click=${this.setChooseMode}></sc-button></p>

          <h4>Textareas</h4>
          <div class="example-row">
            <div class="example">
              <sc-textarea name="textarea1" placeholder="Enter some text" required label="Comment" resizable></sc-textarea>
            </div>

            <div class="example">
              <sc-textarea name="textarea2" label="Comment"></sc-textarea>
            </div>
          </div>

          <div class="example-row">
            <div class="example">
              <sc-textarea name="textarea3" label="Comment" autovalidate pattern="^.{100,}$" comment="This is required" required errormessage="Minimum 100 characters"></sc-textarea>
            </div>
            <div class="example">
              <sc-textarea name="textarea4" placeholder="Enter some text" required disabled label="Disabled textarea"></sc-textarea>
            </div>
          </div>
        </div>
      </div>
    `
  }


}


export { TextareasView }

