
import DocParser from '../util/docparser.js';
import { SimpleBusyMask, SimpleTable } from '../../lib/index.js';
import { ExampledView } from './example.js';
SimpleBusyMask.register();
SimpleTable.register();


import rawcode from '!raw-loader!../../lib/components/image.js';

let __docs = undefined;

const example = `<sc-image effect=pop mousezoom fit=cover width=250 src="/images/dragonflies.jpg" real="/images/bees.jpg"></sc-imge>`;

class ImageView extends ExampledView {

  static title() {
    return "Images";
  }

  static propertyElements() {
    return [ 'sc-image' ];
  }

  static propertyTables() {
    return [ 'image-properties' ]
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }

  static build() {
    return this.builder`
      <div id="images-page">
        <h2>Images</h2>
        <div>
          <h3 class="lined">Description</h3>
          <p>The <code>&lt;sc-image&gt;</code> component is an enhanced image element which supports lazy-loading with placeholder low resolution images, and zoom functionality via touch or the mouse.</p>

          <h3 class="lined">Try it out</h3>
          ${ExampledView.build( example, undefined, false )}
        </div>


        <div class="properties">
          <h3 class="lined">Properties</h3>
          <p></p>
          <sc-table
              id="image-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>

      </div>
    `
  }

}


export { ImageView };


