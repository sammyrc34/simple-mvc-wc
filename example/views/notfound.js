

import { SimpleView } from '../../lib/index.js';

import HeaderView from './header.js';
import SidebarView from './sidebar.js';
import FooterView from './footer.js';

class NotFoundView extends SimpleView {

  static build( ) {

    return this.builder`
      ${HeaderView.build()}
      ${SidebarView.build()}

      <div id="main-wrap">
        <div id="main-pane">
          <h1>Sorry about this!</h1>
          <h3>This is embarassing, that page could not be found</h3>
          <p>Please submit an issue to have the link or page checked. For now, please go back or choose another destination from the menu.</p>
        </div>
      </div>

      ${FooterView.build()}
    `
  }

  static connect() {
    HeaderView.connect();
    SidebarView.connect();
    FooterView.connect();
  }

  static disconnect() {
    HeaderView.disconnect();
    SidebarView.disconnect();
    FooterView.disconnect();
  }

}

export { NotFoundView };

