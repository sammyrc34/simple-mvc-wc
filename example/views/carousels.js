
import DocParser from '../util/docparser.js';
import { SimpleCarousel, SimpleTable } from '../../lib/index.js';
import { ExampledView } from './example.js';
SimpleCarousel.register();
SimpleTable.register();

const example = `<sc-carousel controls="circles" fill>
  <img src="/images/bees.jpg"></img>
  <img src="/images/dragonflies.jpg"></img>
  <img src="/images/ladybugs.jpg"></img>
</sc-carousel>`;


import rawcode from '!raw-loader!../../lib/components/carousel.js';

let __docs = undefined;


class CarouselsView extends ExampledView {

  static title() {
    return "Carousels";
  }

  static propertyElements() {
    return [ 'sc-carousel' ];
  }
  static propertyTables() {
    return [ 'carousel-properties' ]
  }

  static varElements() {
    return [ 'sc-carousel' ];
  }

  static varTables() {
    return [ 'carousel-styles' ]
  }

  static methodNames( ) {
    return [ 'position', 'show', 'previous', 'next' ];
  }

  static eventNames( ) {
    return [ 'position-change', 'position-changed' ];
  }

  static getDocs() {
    if ( __docs == undefined )
      __docs = DocParser.parseJs( rawcode );
    return __docs
  }


  static filterExample( clone ) {
    for ( let ele of clone.querySelectorAll( 'sc-carousel > *' ) ) {
      ele.style.removeProperty( 'opacity' );
      ele.style.removeProperty( 'visibility' );
      ele.style.removeProperty( 'position' );
      ele.style.removeProperty( 'transform' );
      delete ele.dataset.observed;
    }
  }

  static build() {
    return this.builder`
      <div id="carousels-page">

        <div>
          <h3 class="lined">Description</h3>
          <p>The <code>&lt;sc-carousel&gt;</code> component is a user interactable control for displaying a carousel of content elements.</p>
          <h3 class="lined">Try it out</h3>
          ${ExampledView.build( example )}
        </div>


        <div class="properties">
          <h3 class="lined">Properties</h3>
          <sc-table
              id="carousel-properties"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.propFields)}
          >
          </sc-table>
        </div>


        <div class="events">
          <h3 class="lined">Events</h3>
          ${this.buildEvents( 'SimpleCarousel' )}
        </div>


        <div class="methods">
          <h3 class="lined">Methods</h3>
          ${this.buildMethods( 'SimpleCarousel' )}
        </div>


        <div class="variables">
          <h3 class="lined">CSS Variables</h3>
          <p></p>
          <sc-table
              id="carousel-styles"
              scrollable
              .fieldDefinitions=${SimpleTable.toFieldDefintions(this.varFields)}
          >
          </sc-table>
        </div>


        <div id="examples" class="examples" @click=${this.chooseExample.bind( this )}>
          <h3 class="lined">Examples</sc-button></h3>
          <p>Select and try out any example:<sc-button id="choose-button" type="toggle" label="Choose example..." @click=${this.setChooseMode}></sc-button></p>

          <div class="example-row">
            <div class="example">
              <h3>Image carousel on fixed-size carousel</h3>
              <div style="width: 400px;">
                <sc-carousel id="carousel1" controls="circles" >
                  <img src="/images/bees.jpg"></img>
                  <img src="/images/dragonflies.jpg"></img>
                  <img src="/images/ladybugs.jpg"></img>
                </sc-carousel>
              </div>
            </div>
          </div>

          <div class="example-row">
            <div class="example">
              <h3>Image carousel with captions below (panels increasing in size)</h3>
              <div class="inline" style="border: 1px solid lightgrey; border-radius:6px;">
                <sc-carousel id="carousel2">
                  <div>
                    <img style="height: auto; width: 200px;" src="/images/bees.jpg"></img>
                    <div class="text-center"><p>Some nice looking bees</p></div>
                  </div>
                  <div>
                    <img style="height: auto; width: 300px;" src="/images/dragonflies.jpg"></img>
                    <div class="text-center"><p>Dragonflies, arial masters</p></div>
                  </div>
                  <div>
                    <img style="height: auto; width: 400px;" src="/images/ladybugs.jpg"></img>
                    <div class="text-center"><p>Colorful ladybugs</p></div>
                  </div>
                </sc-carousel>
              </div>
            </div>
          </div>

          <div class="example-row">
            <div class="example">
              <h3>Wrapped image carousel with captions to the side</h3>
              <div class="inline" style="border: 1px solid lightgrey; border-radius:6px;">
                <sc-carousel id="carousel3" animate wrap>
                  <div class="flex-row">
                    <img style="height: auto; width: 400px;" src="/images/bees.jpg"></img>
                    <div class="text-center" style="width: 200px;"><p>Some nice looking bees</p></div>
                  </div>
                  <div class="flex-row">
                    <img style="height: auto; width: 400px;" src="/images/dragonflies.jpg"></img>
                    <div class="text-center" style="width: 200px;"><p>Dragonflies, arial masters</p></div>
                  </div>
                  <div class="flex-row">
                    <img style="height: auto; width: 400px;" src="/images/ladybugs.jpg"></img>
                    <div class="text-center" style="width: 200px;"><p>Colorful ladybugs</p></div>
                  </div>
                </sc-carousel>
              </div>
            </div>
          </div>

          <div class="example-row">
            <div class="example">
              <h3>Content Carousel larger than content</h3>
              <div class="inline" style="border: 1px solid lightgrey; border-radius:6px; width: 600px; height: 250px;">
                <sc-carousel id="carousel4" fill>
                  <div style="margin: 10px; max-width: 70%;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                  <div style="margin: 10px; max-width: 70%;">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
                  <div style="margin: 10px; max-width: 70%;">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</div>
                </sc-carousel>
              </div>
            </div>
          </div>

          <div class="example-row">
            <div class="example">
              <h3>Content Carousel smaller than content</h3>
              <div class="inline" style="border: 1px solid lightgrey; border-radius:6px; width: 200px; height: 150px;">
                <sc-carousel id="carousel5" fill willscroll>
                  <div style="margin: 10px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                  <div style="margin: 10px;">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
                  <div style="margin: 10px;">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</div>
                </sc-carousel>
              </div>
            </div>
          </div>

          <div class="example-row">
            <div class="example">
              <h3>Dynamic size to content</h3>
              <div class="inline" style="border: 1px solid lightgrey; border-radius:6px;">
                <sc-carousel id="carousel6" animate wrap>
                  <div class="flex-row">
                    <img id="resizable1" style="height: auto; width: 200px;" src="/images/bees.jpg"></img>
                    <div class="text-center" style="width: 150px">
                      <button onclick="(function(ev){let img=ev.target.parentElement.previousElementSibling;img.style.width=(img.style.width=='200px')?'400px':'200px'})(event);">Resize</button>
                    </div>
                  </div>
                  <div class="flex-row">
                    <img id="resizable2" style="height: auto; width: 200px;" src="/images/dragonflies.jpg"></img>
                    <div class="text-center" style="width: 150px">
                      <button onclick="(function(ev){let img=ev.target.parentElement.previousElementSibling;img.style.width=(img.style.width=='200px')?'400px':'200px'})(event);">Resize</button>
                    </div>
                  </div>
                </sc-carousel>
              </div>
            </div>
          </div>

          <div class="example-row">
            <div class="example">
              <h3>Carousel that fills up space</h3>
              <div class="block" style="border: 1px solid lightgrey; border-radius:6px; height: 200px;">
                <sc-carousel id="carousel7" fill>
                  <div style="margin: 10px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                  <div style="margin: 10px;">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
                  <div style="margin: 10px;">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</div>
                </sc-carousel>
              </div>
            </div>
          </div>

        </div>

      </div>
    `
  }


}


export {
  CarouselsView
}

