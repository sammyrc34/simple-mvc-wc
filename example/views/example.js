


import DocParser from '../util/docparser.js';
import { SimpleTable, SimpleTabsContainer, SimpleTab, SimpleView } from "../../lib/index.js";
SimpleTable.register();
SimpleTabsContainer.register();
SimpleTab.register();

import rawstyle from '!raw-loader!../styles/style.scss';


let _styles = undefined;

class ExampledView extends SimpleView {


  static build( example, code, hasExamples=true ) {
    return this.builder`
      <p>Enter your own example${hasExamples ? this.builder`, or choose from from <span class="link" @click=${this.scrollExamples}>examples</span> below.` : ' below'}</p>
      <div id="example-area" class="example-area">
        <div class="example-code">
          <div>
            <sc-tabs class="example-code-inputs" fill>
              <sc-tab label="Template">
                <sc-textarea
                    slot="content"
                    class="example-code-html"
                    nomessage
                    fill
                    @after-change=${this.updateExample}
                    @keypress=${this.updateOnEnter.bind( this )}
                >
${example}
                </sc-textarea>
              </sc-tab>
              <sc-tab label="JavaScript">
                <sc-textarea
                    slot="content"
                    class="example-code-js"
                    nomessage
                    fill
                    @after-change=${this.updateExample}
                    @keypress=${this.updateOnEnter.bind( this )}
                >
${code}
                </sc-textarea>
              </sc-tab>
              <sc-tab label="Info">
                <p slot="content">The Template tab is where the HTML template including all required attributes is crafted. While templates can ordinarily contain properties and event handlers, the dynamic environment limits this to plain HTML.</p>
                <p slot="content">The JavaScript tab can be used for any required properties or event handlers. The 'this' context is the outermost component constructed.</p>
              </sc-tab>
            </sc-tabs>
          </div>
          <div style="flex: 0.6;">
            <sc-tabs class="example-code-outputs" fill scroll>
              <sc-tab label="Result">
                <div slot="content" class="example-code-output">
                </div>
              </sc-tab>
            </sc-tabs>
          </div>
        </div>
      </div>
      <sub>Enter to update, Shift-Enter for carriage return</sub>
    `;
  }


  static get commonMethods() {
    return this.builder`
      <div class="method">
        <h4>adoptCustomStyles( styles )</h4>
        <ul>
          <li>styles {CSSResult[]} - List of Lit CSSResults</li>
        </ul>
        <p>Adopt one or more styles into this element instance</p>
      </div>
    `
  }


  static get propFields() {
    return [
      { label: "Property",    field: 'prop' },
      { label: "Type",        field: 'type' },
      { label: "Attribute",   field: 'attr' },
      { label: "Default",     field: 'def' },
      { label: "Description", field: "desc", enablehtml: true },
    ]
  }


  static get varFields() {
    return [
      { label: "Group",       field: 'group' },
      { label: "Variable",    field: 'var' },
      { label: "Default",     field: 'def' },
      { label: "Note",        field: "note" },
    ]
  }




  static exampleId() {
    return undefined;
  }


  static setChooseMode( ev ) {
    const toggled = ev.target.toggled;
    const examples = ev.target.closest( '.examples' );
    ev.preventDefault();
    if ( toggled )
      examples.classList.add( 'choose' );
    else
      examples.classList.remove( 'choose' );
  }


  static filterExample( _0 ) {
    return;
  }



  static async chooseExample( ev ) {
    const target = ev.target;
    const examples = ev.target.closest( '.examples' );
    const chooseBtn = examples.querySelector( '#choose-button' );
    const tryItArea = document.querySelector( '#example-area' );
    const htmlInput = document.querySelector( '#example-area .example-code-html' );
    const jsInput = document.querySelector( '#example-area .example-code-js' );
    let match, padding, elem, cloned = [], cloneHtml = [], clone, cloneJs, tmpElem, props, funcs = {};

    if ( ! target || ! jsInput || ! htmlInput || ! examples || ! examples.classList.contains( 'choose' ) )
      return;
    if ( ! target.classList.contains( 'example' ) )
      return;

    tmpElem = document.createElement( 'div' );

    clone = target.querySelector( '*:not( h3,h2,h4 )' ).cloneNode( true );
    tmpElem.appendChild( clone );
    for ( let ele of tmpElem.querySelectorAll( '*' ) )
      ele.removeAttribute( 'id' );

    this.filterExample( clone );

    cloneHtml = tmpElem.innerHTML.split( /[\r\n]/ );
    match = new RegExp( /^(\s*)\S/ ).exec( cloneHtml[ cloneHtml.length - 1 ] );
    if ( match )
      padding = match[1];

    elem = target.querySelector( '*:not( h3,h2,h4 )' );

    if ( ! elem )
      return;

    for ( let line of cloneHtml ) {
      if ( line.startsWith( padding ) )
        cloned.push( line.replace( padding, '' ) );
      else
        cloned.push( line );
    }
    htmlInput.value = cloned.join( String.fromCharCode(13, 10) );

    // Copy function properties
    props = elem.constructor.properties;
    for ( let prop in props ) {
      if ( prop == 'listStyles' || prop == 'tableStyles' ) {
        if ( elem[ prop ] !== undefined ) {
          funcs[ prop ] = `this.cssBuilder\`${elem[prop].toString()}\``
        }
      }
      if ( props[ prop ].type.name !== "Function" )
        continue;

      if ( elem[ prop ] !== undefined ) {
        funcs[ prop ] = elem[ prop ].toString();
      }
    }

    if ( Object.keys( funcs ).length ) {
      cloneJs = Object.keys( funcs ).reduce( (str, prop) => {
        str += `this.${prop} = ${funcs[prop]}

`;
        return str;
      }, "" );
    }

    jsInput.value = cloneJs;

    await htmlInput.updateComplete;
    await jsInput.updateComplete;

    tryItArea.scrollIntoView( { behavior: "smooth" } );
    examples.classList.remove( 'choose' );
    chooseBtn.toggled = false;

    this.updateExample( { target: htmlInput } );
  }


  static scrollExamples() {
    const examples = document.getElementById( 'examples' );
    if ( examples )
      examples.scrollIntoView( { behavior: "smooth" } );
  }


  static updateExample( ev ) {
    const src = ev.target;
    let tab, html, code, output, elem;
    if ( ! src ) return;
    code = src.closest( '.example-code' ).querySelector( '.example-code-js' );
    html = src.closest( '.example-code' ).querySelector( '.example-code-html' );
    output = src.closest( '.example-code' ).querySelector( '.example-code-output' );
    tab = code.closest( 'sc-tab' );

    if ( ! output || ! html || ! code ) return;

    // If Lit doesn't re-render, functions will re-run and maybe result in dupe handlers. So render nothing first
    this.render( this.nothing, output );
    this.render( this.includeHtml( html.value ), output );

    elem = output.firstElementChild;

    if ( code.value ) {
      try {
        Function.constructor( code.value ).call( elem );
        code.invalid = false;
        tab.classList.remove( 'invalid' );
      }
      catch( err ) {
        console.log( "Code evaluation error: " + err.message );
        tab.classList.add( 'invalid' );
      }
    }
  }


  static updateOnEnter( ev ) {
    if ( ev.key == "Enter" && ! ev.shiftKey ) {
      ev.preventDefault();
      ev.stopPropagation();
      this.updateExample( { target: ev.target } );
    }
  }


  static updateProps() {
    if ( ! this.propertyElements )
      return;
    const elements = this.propertyElements();
    const tables = this.propertyTables();
    let component, attr, propname, target, properties, data = [];

    if ( ! elements.length || ! tables.length )
      return;

    for ( let idx = 0; idx < elements.length; idx++ ) {
      data = [];
      attr = true;

      component = document.createElement( elements[ idx ] );
      if ( ! component )
        continue;

      target = document.getElementById( tables[ idx ] );
      if ( ! target )
        continue;

      properties = component.constructor.properties;
      for ( let property in properties ) {
        attr = true;
        propname = property;
        if ( ! properties[ property ].desc )
          continue;
        if ( properties[ property ].attribute && properties[ property ].attribute !== true )
          propname = properties[ property ].attribute;
        if ( properties[ property ].type.name == "Function" )
          attr = false;
        else if ( properties[ property ].attribute === false )
          attr = false;
        data.push( {
          prop: propname,
          type: properties[ property ].type.name,
          attr: ( attr ) ? 'Yes' : 'No',
          def:  this.textifyValue( properties[ property ].def ),
          desc: properties[ property ].desc
        } );
      }

      target._data = data;
    }
  }


  static updateVars() {
    if ( ! this.varElements )
      return;
    const elements = this.varElements();
    const tables = this.varTables();
    let styles, elemStyles, target, data = [];

    if ( ! elements.length || ! tables.length )
      return;

    styles = this.getStyles();

    for ( let idx = 0; idx < elements.length; idx++ ) {

      elemStyles = styles.elements[ elements[ idx ] ];
      if ( ! elemStyles )
        continue;

      target = document.getElementById( tables[ idx ] );
      if ( ! target )
        continue;

      data = [];

      for ( let group of styles.elements[ elements[ idx ] ].variables ) {
        for ( let v in group.variables ) {
          data.push( {
            'group': group.description,
            'var': v,
            'def': group.variables[ v ].default,
            'note': group.variables[ v ].note
          } );
        }
      }

      target._data = data;
    }
  }


  static buildTypes() {
    if ( ! this.typeNames )
      return;
    const wantTypes = this.typeNames();
    const docs = this.getDocs();
    const out = [];
    let found;

    for ( let want of wantTypes ) {
      found = docs.typedefs.find( t => t.name == want );
      if ( found )
        out.push( found );
    }


    return this.builder`
      ${out.map( (t) => this.builder`
        <div class="typedef">
          <h4><var>${t.name} = {${t.type}}</var></h4>
          <ul>
            ${t.docs.filter( (p) => p.tag == 'property' )
              .map( (p) => this.builder`<li><div><var>${p.name}</var> <var>{${p.type}}</var></div> ${p.description}</li>`)}
          </ul>
          <p>${t.desc}</p>
        </div>
      `)}
    `;


  }



  static buildMethods( className ) {
    const methodNames = this.methodNames( className );
    const docs = this.getDocs();
    const out = [];
    let methodInfo, tmp, paramgroup, params;

    for ( let wantMethod of methodNames ) {

      methodInfo = docs.methods.find( m => ( m.class == className || m.class == 'anon' ) && m.name == wantMethod );
      if ( ! methodInfo )
        continue;

      tmp = {};
      params = [];
      paramgroup = undefined;

      tmp[ 'desc' ] = methodInfo.docs.description;
      tmp[ 'flags' ] = [];
      tmp[ 'params' ] = [];
      tmp[ 'see' ] = [];

      for ( let flag of [ 'getset', 'async', 'static' ] ) {
        if ( methodInfo.hasOwnProperty( flag ) && methodInfo[ flag ] )
          tmp[ 'flags' ].push( ( flag == 'getset' ) ? methodInfo[ flag ] : flag );
      }

      for ( let param of methodInfo.docs.tags ) {
        if ( param.tag == 'param' ) {
          if ( ! paramgroup || ! param.name.startsWith( paramgroup ) )
            params.push( param.name );
          if ( ( ! paramgroup || ! param.name.startsWith( paramgroup ) ) && ( param.type.startsWith( 'Object' ) || param.type.startsWith( '...Object' ) ) )
            paramgroup = param.name;
          tmp.params.push( {
            'name': param.optional
            ? ( param.default )
            ? `[${param.name}=${param.default}]`
            : `[${param.name}]`
            : param.name,
            'type': param.type,
            'desc': ( param.description.startsWith( '-' ) )
            ? param.description.substring( 1 ).trim()
            : param.description
          } );
        }
        else if ( param.tag == 'returns' ) {
          tmp[ 'return' ] = ( param.name && param.name !== "-" ) ? `${param.name} =` : 'return = ';
          tmp[ 'params' ].push( {
            'name': ( param.name && param.name !== "-" ) ? param.name : 'return',
            'type': param.type,
            'desc': ( param.description.startsWith( '-' ) )
              ? param.description.substring( 1 ).trim()
              : param.description
          } );
        }
        else if ( [ 'abstract', 'override', 'private', 'protected' ].includes( param.tag ) )
          tmp.flags.push( param.tag );
        else if ( param.tag == "throws" ) {
          tmp.flags.push( 'throws' );
          tmp.params.push( {
            'name': 'throws',
            'type': undefined,
            'desc': param.description
          } );
        }
        else if ( param.tag == "see" ) {
          tmp[ 'see' ].push( param.type );
        }
      }

      if ( methodInfo.getset ) {
        tmp[ 'method' ] = methodInfo.name;
        if ( methodInfo.getset == 'set' ) {
          tmp[ 'method' ] += ' = <em>' + params[ 0 ] + '</em>';
        }
      }
      else {
        tmp[ 'method' ] = ( methodInfo.name == "constructor" )
          ? "new " + className + "( "
          : methodInfo.name + '( ';
        if ( tmp.params.length )
          tmp[ 'method' ] += params.join( ', ' ) + ' ';
        tmp[ 'method' ] += ')';
      }

      tmp[ 'plainmethod' ] = methodInfo.name;

      out.push( tmp );
    }

    return this.builder`
      ${out.map( (m) => this.builder`
        <div class="method" id=${"method_" + m.plainmethod} >
          <h4><var>${m.return} ${this.includeHtml(m.method)} ${m.flags.map( (f) => this.builder`<code>${f}</code>` )}</var></h4>
          <ul>
            ${m.params.map( (p) => this.builder`<li><div>
              <var>${p.name}</var>
              <var>${p.type ? '{' + p.type + '}' : '' }</var>
              </div>${p.desc ? this.builder` - ${p.desc}` : '' }</li>` )}
          </ul>
          <p>${m.desc}</p>
          <p>${m.see.length ? this.builder`See <var>${m.see.join( ', ' )}</var>` : '' }</p>
        </div>
      ` ) }
    `;
  }


  static buildEvents( className ) {
    const eventNames = this.eventNames( className );
    const docs = this.getDocs();
    const out = [];

    for ( let ev of docs.events ) {
      if ( ! eventNames.includes( ev.name ) )
        continue
      if ( ev.class != className )
        continue;
      out.push( ev );
    }

    return this.builder`
      ${out.map( (e) => this.builder`
        <div class="event">
          <h4><var>${e.name}</var></h4>
          ${e.docs.length ? this.builder`<h5>Event <code>detail</code> properties:</h5>` : '' }
          <ul>
            ${e.docs.filter( (p) => p.tag == 'property' )
              .map( (p) => this.builder`<li><div><var>${p.name}</var> <var>{${p.type}}</var></div> ${p.description}</li>`)}
          </ul>
          <p>${e.desc}</p>
        </div>
      `)}
    `;
  }


  static getStyles() {
    if ( ! _styles )
      _styles = DocParser.parseSCSS( rawstyle );
    return _styles;
  }


  static textifyValue( val ) {
    if ( val === null )
      return "null";
    else if ( val === undefined )
      return "undefined"
    else
      return val;
  }


  static connect() {
    let inputs;
    super.connect();
    this.updateProps();
    this.updateVars();
    inputs = document.querySelectorAll( `#example-area .example-code-html` );
    if ( ! inputs || ! inputs.length )
      return;
    inputs.forEach( input => {
      this.updateExample( { target: input } );
    } );
  }


  static reconnect() {
    let inputs;
    super.reconnect();
    this.updateProps();
    this.updateVars();
    inputs = document.querySelectorAll( `#example-area .example-code-html` );
    if ( ! inputs || ! inputs.length )
      return;
    inputs.forEach( input => {
      this.updateExample( { target: input } );
    } );
  }


}


export { ExampledView };
