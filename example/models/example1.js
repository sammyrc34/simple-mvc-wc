
import { SimpleCollection } from '../../lib/index.js';


const colorConf = {
  reqFields:    [ 'value', 'text' ]
};

const peopleConf = {
  reqFields:    [ 'name', 'title', 'email' ],
  optFields:    [ 'dob', 'colour', 'tall', 'status', 'comment' ]
};


const formConf = {
  reqFields:    [ 'name', 'number', 'color', 'toggle', 'fruit', 'other' ]
};


export class ExampleForminfo extends SimpleCollection {
  constructor( app ) {
    super( app, formConf );
  }
}

export class ExampleColours extends SimpleCollection {
  constructor( app ) {
    super( app, colorConf );
  }
}

export class ExamplePeople extends SimpleCollection {
  constructor( app ) {
    super( app, peopleConf );
  }
}

