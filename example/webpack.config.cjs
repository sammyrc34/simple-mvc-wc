
// Development bundle for example
module.exports = {
  devServer: {
    allowedHosts: "all",
    compress: false,
    hot: 'only',
    historyApiFallback: {
      index: 'index.html'
    },
    client: {
      overlay: false
    },
    static: [ 
      './example',
      './node_modules'
    ]
  },
  devtool: 'source-map',
  output: {
    filename: 'bundle.js'
  },
  optimization: {
    minimize: false
  },
  stats: {
    orphanModules: true,
  },
  mode: "development",
  entry: './example/index.js',
  module:{
    rules:[
      {
        test: /\.scss$/i,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader', options: { sourceMap: true, import: false } },
          { loader: 'sass-loader', options: { sourceMap: true } }
        ],
        sideEffects: true
      },
      {
        test: /lib\/.*\.s?(c|a)ss$/,
        use: [{
          loader: 'lit-scss-loader',
          options: {
            minify: false,
          },
        }, 'extract-loader', 'css-loader', 'sass-loader'],
      },
    ]
  }

};
