


import { parse } from 'comment-parser';




class DocParser {

  /**
   * Extract comment blocks from javascript source
   */
  static parseJs( source ) {
    const parsed = { events: [], methods: [], typedefs: [] };
    const preClassRegex = new RegExp( /^(.*?)(?:^\s+|^)(?:export )?class /, 'sm' );
    const classRegex = new RegExp( /(?:export )?class (\w*)\s?(?:extends .+?)?\{(.+?)^\}/, 'gsm' );
    const blockRegex = new RegExp( /^\s*(\/\*\*(?:.|\n)+?\*\/)[ \t]*\n(.*)/, 'gm' );
    const nameRegex = new RegExp( /^\s*(static)?\s?(async)?\s?(?:(get|set)\s)?\s?(\w.*?)\(/, 'i' );
    let preClass, classMatch, matches, className;

    // Look for events and type definitions outside of classes
    preClass = preClassRegex.exec( source );
    if ( preClass !== null ) {

      while ( ( matches = blockRegex.exec( preClass[ 1 ] ) ) !== null ) {

        let docs = parse( matches[ 1 ] );
        let firstType, evs = [], types = [], defs = [];

        // No match?
        if ( ! docs.length || ! docs[ 0 ].tags.length )
          continue;

        firstType = docs[ 0 ].tags[ 0 ].tag;
        if ( firstType !== "event" && firstType !== 'typedef' )
          continue;

        for ( let tag of docs[ 0 ].tags ) {
          if ( tag.tag == "event" ) {
            evs.push( tag.name );
            continue;
          }
          if ( tag.tag == "typedef" ) {
            types.push( tag.name );
            continue;
          }
          defs.push( tag );
        }

        for ( let evname of evs ) {
          parsed[ 'events' ].push( {
            docs: defs,
            desc: docs[0].description,
            name: evname.split( '#' )[ 1 ],
            class: evname.split( '#' )[ 0 ]
          } );
        }
        for ( let typename of types ) {
          parsed[ 'typedefs' ].push( {
            docs: defs,
            desc: docs[0].description,
            name: typename,
            type: docs[0].tags[0].type
          } );
        }
      }
    }


    // Add methods
    while ( ( classMatch = classRegex.exec( source ) ) !== null ) {

      blockRegex.lastIndex = 0;
      className = classMatch[ 1 ] ? classMatch[ 1 ] : 'anon';

      while ( ( matches = blockRegex.exec( classMatch[ 2 ] ) ) !== null ) {

        let comment = parse( matches[ 1 ] );
        let info = nameRegex.exec( matches[ 2 ] );

        // Comment didn't parse?
        if ( ! comment.length || ! info )
          continue;

        parsed[ 'methods' ].push( {
          name:     info[ 4 ],
          class:    className,
          docs:     comment[ 0 ],
          static:   info[ 1 ] ? true : false,
          async:    info[ 2 ] ? true : false,
          getset:   info[ 3 ] ? info[ 3 ] : ''
        } );
      }
    }

    return parsed;
  }


  /**
   * Extract comment blocks from SCSS
   */
  static parseSCSS( source ) {
    const parsed = { global: [], elements: {} };
    const globalRegex = new RegExp( /^html \{((?:.|\n)+?)^\}/, 'gm' );
    const blockRegex = new RegExp( /^(\/\*\*(?:.|\n)+?\*\/)[ \t]*\n^(.+)\{((?:.|\n)+?\})/, 'gm' );
    const varsRegex = new RegExp( /^(?:\n|.)+?\s*(\/\*\*(?:.|\n)+?\*\/)\n((?:\n|.)+?)\n(?:\n|\})/, 'gm');
    let globalMatch, blockMatch, varsMatch, vars, varInfo;

    // Add methods
    while ( ( blockMatch = blockRegex.exec( source ) ) !== null ) {

      let comment = parse( blockMatch[ 1 ] );
      let elems = blockMatch[2].split( ',' ).map( e => e.trim() );

      for ( let elemName of elems ) {
        if ( ! parsed.elements.hasOwnProperty( elemName ) )
          parsed.elements[ elemName ] = {
            'description': comment[0].description,
            'variables': []
          };
      }

      while( ( varsMatch = varsRegex.exec( blockMatch[3] ) ) !== null ) {

        varInfo = {};
        let inner = parse( varsMatch[1] );

        vars = varsMatch[2].split( /\n/ ).map( v => v.trim() );
        for ( let v of vars ) {
          let [ name, defValue, varComment ] = v.split( /:|\/\*/ ).map( bit => bit.trim() );
          if ( varComment && varComment.includes( '*/' ) )
            varComment = varComment.substring( 0, varComment.indexOf( '*/' ) )
          varInfo[ name ] = {
            'note': varComment,
            'default': defValue.replace( /\;$/, "" )
          };
        }

        for ( let elemName of elems ) {
          parsed.elements[ elemName ].variables.push( {
            'description': inner[0].description,
            'variables': varInfo
          } );
        }
      }
    }

    return parsed;
  }


}

export default DocParser;

