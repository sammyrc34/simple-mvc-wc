/**
 * A component containig a list of selectable items or rows
 *
 * Supports single or multiple selection models, handling events
 * for selections, double-clicks and context menus
 * @class
 */
export class SelectableListComponent extends DataboundComponent {
    select: string;
    modelName: any;
    _data: any[];
    bindToNew: boolean;
    bindMode: string;
    _selected: any[];
    _lastSel: EventTarget | HTMLElement;
    /**
     * Is a row ID selected?
     * @param {String|Number} id          - The ID to check
     * @return {Boolean}                  - If the item (row) is selected
     */
    isSelected(id: string | number): boolean;
    /**
     * Handle a click and try to select things appropriately
     * @param {MouseEvent} ev               - This click event
     * @return {Boolean}                    - If selection was successful
     */
    handleSelectClick(ev: MouseEvent): boolean;
    /**
     * Select a range of items from the last selected item to the current one
     * @param {MouseEvent} ev       - MouseEvent
     * @param {HTMLElement} elem    - The clicked item element
     * @return {Boolean}            - If the selection was successful
     */
    selectByRange(ev: MouseEvent, elem: HTMLElement): boolean;
    /**
     * Toggle selection of one item
     * @param {MouseEvent} ev       - MouseEvent
     * @param {HTMLElement} elem    - The clicked item element
     * @return {Boolean}            - If the selection changed
     */
    selectByToggle(ev: MouseEvent, elem: HTMLElement): boolean;
    /**
     * Handle a double-click on a selected item.
     * Requires selection to be enabled, or the raw double click will bubble.
     */
    handleDblClick(ev: any): void;
    /**
     * Handle a context-menu click on a selected item.
     * Requires selection to be enabled, or the context menu click will bubble.
     */
    handleContextMenu(ev: any): void;
    /**
     * Get the ID associated with a list item
     * @param {HTMLElement} elem        - The sc-list-item element
     * @return {String|Number}          - The record's ID or undefined if none
     */
    getItemID(elem: HTMLElement): string | number;
    /**
     * Get all selected values
     * @return {String[]|Number[]}      - The IDs for all selected items
     */
    getSelected(): string[] | number[];
    /**
     * Set selected values
     * @param {String[]|Number[]} ids   - The row IDs to select
     */
    setSelected(ids: string[] | number[]): void;
    /**
     * Clear selected items
     */
    clearSelection(): void;
    /**
     * Handle a data model status chaange
     * @protected
     * @see DataboundComponent#_statusChanged
     */
    protected _statusChanged(type: any): void;
    /**
     * Handle a data model data change
     * @protected
     * @see DataboundComponent#_dataChanged
     */
    protected _dataChanged(detail: any): void;
    getDisplayedItems(): void;
}
import { DataboundComponent } from './databound.js';
