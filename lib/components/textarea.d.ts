/**
 * Simple Textarea component
 */
export class SimpleTextarea extends SimpleComponent {
    static get _elementName(): string;
    _initValue: string;
    _defaultValue: string;
    _smalllabel: boolean;
    _focused: boolean;
    _message: string;
    _hascomment: boolean;
    _pristine: boolean;
    render(): import("lit-html").TemplateResult<1>;
    /*******************
     * Lit methods
     ******************/
    willUpdate(props: any): void;
    firstUpdated(): void;
    _value: any;
    /**
     * Set the elements value
     * @public
     * @param {*} value - The value for the element
    */
    public set value(val: any);
    /***********************
     * Element methods
     **********************/
    /**
     * Get the element value
     * @public
     * @returns {*} - A typed value per valuetype and emptyasnull attributes
     */
    public get value(): any;
    /**
     * Reset the elements value to last initial value, or default value
     * @public
     * @param {Boolean} toDefault - If the element should be reset to defaults instead of initial value
     */
    public reset(toDefault?: boolean): void;
    invalid: boolean;
    /**
     * Set this elements value to an initial value
     * @public
     * @param {*} value - The initial value to set
     */
    public setInitial(val: any): void;
    /**
     * Has this elements value changed?
     * @public
     * @returns {Boolean} - If this element has changed
     */
    public changed(): boolean;
    /**
     * Focus this element
     */
    focus(): void;
    /**
     * Validate the input as needed
     * @public
     * @returns {Boolean} - If element value is valid
     */
    public validate(): boolean;
    _updateMessage(): void;
    _afterInput(ev: any): void;
    _afterChange(ev: any): void;
    _afterBlur(): void;
    _updateLabel(ev: any): void;
}
import { SimpleComponent } from '../index.js';
