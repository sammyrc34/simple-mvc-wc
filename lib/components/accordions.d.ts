export class SimpleAccordionGroup extends SimpleComponent {
    static get _elementName(): string;
    _skipanimate: boolean;
    _kids: any[];
    _sizes: any[];
    _resizing: boolean;
    _resizehandle: () => void;
    _resizeobserver: Polyfill;
    render(): import("lit-html").TemplateResult<1>;
    /**
     * Handle changed accordion items
     */
    _slotChanged(): void;
    _needresize: boolean;
    /***********************
     * Lit methods
     ***********************/
    /**
     * Handle property updates
     */
    updated(changes: any): void;
    /**
     * Handle first render
     */
    firstUpdated(): Promise<void>;
    /***********************
     * Our methods
     ***********************/
    /**
     * Check if a child accordion can attempt to close
     */
    canClose(accordion: any): boolean;
    /**
     * Indicate a desire to open or close an accordion by index
     * @param {Number} index      - Accordion index for opening or closing
     * @param {Boolean} open      - Open state sought for accordion
     */
    openCloseByIndex(index: number, open: boolean): void;
    /**
     * Indicate a desire to open or close an accordion
     * @param {HTMLElement} accordion   - Accordion requesting a change
     * @param {Boolean} open            - Open state sought for accordion
     */
    openClose(accordion: HTMLElement, open: boolean): void;
    /**
     * Calculate the sizes of all accordions
     * @private
     */
    private _calculateSizes;
    /**
     * Get the height of the accordion
     * @protected
     */
    protected _getFillHeight(kid: any): any;
    /**
     * Request a resize of the accordion group
     * @protected
     */
    protected _requestResize(): void;
    /**
     * Resize the group of accordions
     * @private
     * @async
     */
    private _resize;
}
export class SimpleAccordion extends SimpleComponent {
    static get _elementName(): string;
    _minheight: number;
    _lastheight: any;
    _grouped: boolean;
    _animate: boolean;
    _resizing: boolean;
    /********************
     * Rendering methods
     ********************/
    render(): import("lit-html").TemplateResult<1>;
    barTop(): import("lit-html").TemplateResult<1>;
    barBottom(): import("lit-html").TemplateResult<1>;
    /************************
     * LIT methods
     ************************/
    /**
     * Check for needed side effects
     */
    willUpdate(changes: any): void;
    skipanimate: boolean;
    open: any;
    groupopen: any;
    firstUpdated(): void;
    updated(changes: any): void;
    /***********************
     * Our methods
     ***********************/
    /**
     * Helper method to get the gap between the bar and the top
     * @protected
     */
    protected _getBarMargin(): number;
    /**
     * Inherit animation time from CSS as needed
     * @private
     */
    private _inheritAnimateTime;
    animatetime: number;
    /**
     * Handle a mouse over for cursor display
     * @private
     */
    private _mouseOver;
    _noclose: boolean;
    _noopen: any;
    /**
     * Handle changes to slotted content
     * @private
     */
    private _slotChanged;
    _heightfix: boolean;
    /**
     * Toggle from open to close or vice versa
     * @public
     */
    public toggle(): void;
    /**
     * Get the height of the accordion bar in pixels
     * @protected
     * @returns {Number}
     */
    protected _getBarHeight(): number;
    /**
   * Resize this accordion with a group timeline
   * @protected
   * @param {Array[]} sequence        - A sequence of animations
   * @returns {Function}              - Callback upon animation completion
   */
    protected _groupResize(sequence: any[][]): Function;
    show: boolean;
    /**
     * Resize this accordion as necessary
     * @protected
     */
    protected _resize(): Promise<void>;
    minheight: number;
}
import { SimpleComponent } from '../index.js';
import { ResizeObserver as Polyfill } from '@juggle/resize-observer';
