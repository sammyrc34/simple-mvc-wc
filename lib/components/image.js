
import { SimpleComponent } from '../index.js';
import { animate } from 'motion';

import imageStyles from './image.css';

// A container with other containers inside
export class SimpleImage extends SimpleComponent {

  constructor() {
    super();

    // Private
    this._realed    = false;
    this._zoomed    = false;
    this._zooming   = false;
    this._panning   = false;
    this._refSpace  = undefined;
    this._refScale  = undefined;
    this._refX      = undefined; // Last touched position (pan)
    this._refY      = undefined;
    this._posX      = 0;         // Last image position (pan)
    this._posY      = 0;
    this._scale     = 1;         // Current image scale
    this._offsetX   = undefined; // Offset applied to subsequent pans
    this._offsetY   = undefined;
    this._refRangeX = undefined; // Allowable range of image movement
    this._refRangeY = undefined;
    this._observer  = undefined;

    // Support touch events
    this.addEventListener( 'touchstart', this._touchStart );
    this.addEventListener( 'touchend', this._touchEnd );
    this.addEventListener( 'touchmove', this._touchMove );
    this.addEventListener( 'touchcancel', this._touchEnd );

    // Support mouse zooming
    this.addEventListener( 'wheel', this._mouseZoom );
  }
  static get _elementName() { return 'sc-image' }
  static get properties() {
    return Object.assign( {}, super.properties, {

      height:       { type: Number, def: undefined, desc: "An optional constained height for this image" },
      width:        { type: Number, def: undefined, desc: "An optional constained width for this image" },
      src:          { type: String, def: undefined, desc: "The image or initial image to display" },
      real:         { type: String, def: undefined, desc: "The real image to display if lazy loading is enabled" },
      altText:      { type: String, def: undefined, desc: "Text to display in place of an image if unable to load the image" },
      lazy:         { type: Boolean, def: false, desc: "If lazy mode is enabled, loading the image on intersection with the viewport" },
      effect:       { type: String, def: "none", desc: "An optional hover effect, currently none or pop" },
      fit:          { type: String, def: "contain", desc: "How to fit this image in the container, either contain or cover" },

      touchzoom:    { type: Boolean, def: false, desc: "If this image support zooming via touch controls" },  
      mousezoom:    { type: Boolean, def: false, desc: "If this image support zooming via mouse controls" },  
      zoomdelta:    { type: Number, def: 0.2, desc: "The amount of zoom each mouse zoom input result in, larger numbers zooming quickly" },
      maxzoom:      { type: Number, def: undefined, desc: "An optional maximum zoom factor" }
    } );
  }
  static get styles() {
    return super.styles.concat( [ imageStyles ] );
  }
  render() {
    return this.builder`
      <div 
          class="sc-image-container"
          fit=${this.fit}
      >
        <img 
            @mouseenter=${this._mousein}
            @mouseleave=${this._mouseout}
            src="${this.src}"
            alt="${this.ifDefined(this.altText)}" 
            height="${this.ifDefined(this.height)}"
            width="${this.ifDefined(this.width)}"
            ?touchzoom=${this.touchzoom}
        >
        </img>
      </div>
    `;
  }
  connectedCallback() {
    super.connectedCallback();
    if ( this.lazy && this.real != undefined && this._observer == undefined ) {
      this._setupObserver();
      if ( this._observer )
        this._observer.observe( this );
    }
  }
  disconnectedCallback() {
    super.disconnectedCallback();
    if ( this.lazy && this.real != undefined && this._observer != undefined ) {
      this._observer.unobserve( this );
      this._realed = false;
    }
  }

  willUpdate( changes ) {
    if ( changes.has( 'real' ) && this._realed && this._observer ) {
      this._observer.observe( this );
    }
  }


  _mouseZoom(ev) {
    const img = this.shadowRoot.querySelector( 'img' );
    let delta, newscale;
    if ( ! this.mousezoom )
      return;

    delta = ( ev.deltaY / 100 ) * this.zoomdelta * -1;
    newscale = parseFloat( img.style.scale ) || 1;
    newscale += delta;
    if ( newscale < 1 )
      newscale = 1;

    if ( newscale > this.maxzoom )
      newscale = this.maxzoom;
    
    img.style.scale = newscale;

    if ( newscale > 1 )
      this._zoomed = true;
    else {
      this._zoomed = false;
      img.style.transform = 'translate( 0px, 0px )';
    }

    ev.preventDefault();
    ev.stopPropagation();
  }


  _touchStart(ev) {
    if ( ! this.touchzoom )
      return;

    // Zooming or panning
    if ( ev.touches.length == 2 )
      return this._startZoom( ev );
    else if ( this._zoomed && ev.touches.length == 1 )
      return this._startPan( ev );
  }

  _startZoom(ev) {
    let betweenX, betweenY;
    betweenX = Math.floor( ev.touches[0].screenX - ev.touches[1].screenX );
    betweenY = Math.floor( ev.touches[0].screenY - ev.touches[1].screenY );
    if ( betweenX < 0 )
      betweenX *= -1;
    if ( betweenY < 0 )
      betweenY *= -1;
    this._refSpace = ( betweenX > betweenY ) ? betweenX : betweenY;
    this._refScale = this._scale || 1;
    this._zooming = true;
    this._panning = false;

    ev.preventDefault();
    ev.stopPropagation();
  }

  _startPan(ev) {
    const cont = this.shadowRoot.querySelector( '.sc-image-container' );
    const img = this.shadowRoot.querySelector( 'img' );
    let rangeX, rangeY, scale, contSize, contRatio, imgRatio;
    this._panning = true;
    this._zooming = false;
    this._refX = Math.floor( ev.touches[0].screenX );
    this._refY = Math.floor( ev.touches[0].screenY );

    // Get size ratios
    contSize = cont.getBoundingClientRect();
    imgRatio = img.naturalWidth / img.naturalHeight;
    contRatio = contSize.width / contSize.height;

    // Calculate range
    scale = this._scale;
    if ( imgRatio >= contRatio ) { /* width based scale and range */
      rangeX = ( ( contSize.width * scale ) - contSize.width ) / 2 / scale;
      rangeY = ( ( img.naturalHeight * ( contSize.width / img.naturalWidth ) * scale ) - contSize.height ) / 2 / scale;
    }
    else { /* height based scale and range */
      rangeX = ( ( img.naturalHeight * ( contSize.height / img.naturalHeight ) * scale ) - contSize.width ) / 2 / scale;
      rangeY = ( ( contSize.height * scale ) - contSize.height ) / 2 / scale;
    }

    rangeX = ( rangeX < 0 ) ? 0 : rangeX;
    rangeY = ( rangeY < 0 ) ? 0 : rangeY;

    this._refRangeX = Math.floor( rangeX );
    this._refRangeY = Math.floor( rangeY );

    this._offsetX = this._posX;
    this._offsetY = this._posY;

    ev.preventDefault();
    ev.stopPropagation();
  }


  /**
   * Check a pan is within the range limits
   */
  _checkPan( ev ) {
    const cont = this.shadowRoot.querySelector( '.sc-image-container' );
    const img = this.shadowRoot.querySelector( 'img' );
    let rangeX, rangeY, scale, contSize, contRatio, imgRatio;
    this._panning = false;
    this._zooming = false;

    // Get size ratios
    contSize = cont.getBoundingClientRect();
    imgRatio = img.naturalWidth / img.naturalHeight;
    contRatio = contSize.width / contSize.height;

    // Calculate range
    scale = this._scale;
    if ( imgRatio >= contRatio ) { /* width based scale and range */
      rangeX = ( ( contSize.width * scale ) - contSize.width ) / 2 / scale;
      rangeY = ( ( img.naturalHeight * ( contSize.width / img.naturalWidth ) * scale ) - contSize.height ) / 2 / scale;
    }
    else { /* height based scale and range */
      rangeX = ( ( img.naturalHeight * ( contSize.height / img.naturalHeight ) * scale ) - contSize.width ) / 2 / scale;
      rangeY = ( ( contSize.height * scale ) - contSize.height ) / 2 / scale;
    }
    rangeX = ( rangeX < 0 ) ? 0 : rangeX;
    rangeY = ( rangeY < 0 ) ? 0 : rangeY;

    // Calculate new position if current is out of range
    if ( this._posX > 0 && this._posX > rangeX )
      this._posX = rangeX;
    else if ( this._posX < 0 && this._posX < ( rangeX * -1 ) )
      this._posX = ( rangeX * -1 );

    if ( this._posY > 0 && this._posY > rangeY )
      this._posY = rangeY;
    else if ( this._posY < 0 && this._posY < ( rangeY * -1 ) )
      this._posY = ( rangeY * -1 );

    // Set transforms
    img.style.transform = `scale(${this._scale}) translate(${this._posX}px, ${this._posY}px)`;

    ev.preventDefault();
    ev.stopPropagation();

    this._touchCancel();
  }

  _touchMove(ev) {
    if ( this._zooming )
      return this._zoom( ev );
    else if ( this._panning )
      return this._pan( ev );
  }

  _zoom( ev ) {
    const img = this.shadowRoot.querySelector( 'img' );
    let newscale, between, betweenX, betweenY;

    betweenX = Math.floor( ev.touches[0].screenX - ev.touches[1].screenX );
    betweenY = Math.floor( ev.touches[0].screenY - ev.touches[1].screenY );
    if ( betweenX < 0 ) betweenX *= -1;
    if ( betweenY < 0 ) betweenY *= -1;
    between = ( betweenX > betweenY ) ? betweenX : betweenY;

    newscale = between / this._refSpace;
    newscale = Math.round( this._refScale * newscale * 1000 ) / 1000;
    newscale = ( newscale < 1 ) ? 1 : newscale;

    if ( this.maxzoom && newscale > this.maxzoom )
      newscale = this.maxzoom;
    this._scale = newscale

    if ( newscale > 1 )
      this._zoomed = true;
    else {
      this._zoomed = false;
      this._posX = 0;
      this._posY = 0;
    }

    // Set transforms
    img.style.transform = `scale(${this._scale}) translate(${this._posX}px, ${this._posY}px)`;

    ev.preventDefault();
    ev.stopPropagation();
  }

  _pan( ev ) {
    const img = this.shadowRoot.querySelector( 'img' );
    let curX, curY, deltaX, deltaY, moveX, moveY, overX, overY;
    curX = ev.touches[0].screenX;
    curY = ev.touches[0].screenY;
    deltaX = Math.floor( ( this._refX - curX ) * 0.6 );
    deltaY = Math.floor( ( this._refY - curY ) * 0.6 );

    deltaX -= this._offsetX;
    deltaY -= this._offsetY;

    // Calculate position
    if ( deltaX < 0 && deltaX < ( this._refRangeX * -1 ) ) {
      moveX = this._refRangeX;
      overX = deltaX - ( this._refRangeX * -1 );
    }
    else if ( deltaX > 0 && deltaX > this._refRangeX ) {
      moveX = this._refRangeX * -1;
      overX = deltaX - this._refRangeX;
    }
    else {
      moveX = deltaX * -1;
      overX = 0;
    }

    if ( deltaY < 0 && deltaY < ( this._refRangeY * -1 ) ) {
      moveY = this._refRangeY;
      overY = deltaY - ( this._refRangeY * -1 );
    }
    else if ( deltaY > 0 && deltaY > this._refRangeY ) {
      moveY = this._refRangeY * -1;
      overY = deltaY - this._refRangeY;
    }
    else {
      moveY = deltaY * -1;
      overY = 0;
    }

    // Store last translation
    this._posX = moveX;
    this._posY = moveY;

    // Move the refrence point if at a boundary
    if ( moveX == this._refRangeX || ( moveX * -1 ) == this._refRangeX )
      this._refX = this._refX - overX;
    if ( moveY == this._refRangeY || ( moveY * -1 ) == this._refRangeY )
      this._refY = this._refY - overY;

    // Set transforms
    img.style.transform = `scale(${this._scale}) translate(${this._posX}px, ${this._posY}px)`;

    ev.preventDefault();
    ev.stopPropagation();
  }

  _touchEnd(ev) {
    if ( ! ev.touches.length && this._panning ) /* From panning to nothing */
      return this._checkPan(ev);
    else if ( ev.touches.length == 1 && this._zooming ) /* From zooming to panning */
      return this._startPan(ev);
    else if ( ! ev.touches.length && this._zooming ) /* From zooming to nothing */
      return this._checkPan(ev);
  }

  _touchCancel() {
    this._zooming = false;
    this._panning = false;
    this._refX = undefined;
    this._refY = undefined;
    this._refScale = undefined;
  }

  _mousein(ev) {
    if ( this.effect == "pop" ) {
      animate( ev.target, {
        scale: 1.1
      }, {
        duration: 0.25,
        easing: 'ease-in-out'
      });
    }
  }

  _mouseout(ev) {
    if ( this.effect == "pop" ) {
      animate( ev.target, {
        scale: 1
      }, {
        duration: 0.25,
        easing: 'ease-in-out'
      });
    }
  }

  _setupObserver() {
    if ( this._observer != undefined )
      return;
    if ( ! window.IntersectionObserver ) {
      this.app.log( "warning", "IntersectionObserver not supported, cannot lazy load" );
      this.lazy = false;
    }

    this._observer = new IntersectionObserver( (entities, observer) => {
      entities.forEach( (entity) => {
        if ( entity.isIntersecting ) {
          var instance = entity.target;

          if ( instance.src !== instance.real ) {
            instance.src = instance.real;
            instance._realed = true;
            observer.unobserve( instance );
          }
        }
      } );
    } );
  }

}


