/**
 * Simple Input component
 * Includes checkboxes, toggles, radio buttons and all forms of text inputs
 */
export class SimpleInput extends SimpleComponent {
    static get _elementName(): string;
    _initValue: string;
    _defaultValue: string;
    _smalllabel: boolean;
    _focused: boolean;
    _hascomment: boolean;
    _pristine: boolean;
    _type: any;
    _formatpattern: RegExp;
    _formatgroups: Map<any, any>;
    _lastvalue: string;
    _pickerType: string;
    /***********************
     * Lit methods
     **********************/
    willUpdate(changes: any): void;
    tristate: boolean;
    formatchars: string;
    _value: any;
    updated(changes: any): void;
    firstUpdated(): void;
    render(): import("lit-html").TemplateResult<1>;
    /**
     * Set the elements value
     * @public
     * @param {*} newval - The value for the element
     */
    public set value(newval: any);
    /***********************
     * Element methods
     **********************/
    /**
     * Get the element value
     * @public
     * @returns {*} - A typed value per valuetype and emptyasnull attributes
     */
    public get value(): any;
    /**
     * Reset the elements value to last initial value, or default value
     * @public
     * @param {Boolean} toDefault - If the element should be reset to defaults instead of initial value
     */
    public reset(toDefault?: boolean): void;
    invalid: boolean;
    /**
     * Has this elements value changed?
     * @public
     * @returns {Boolean} - If this element has changed
     */
    public changed(): boolean;
    /**
     * Set this elements value to an initial value
     * @public
     * @param {*} value - The initial value to set
     */
    public setInitial(value: any): void;
    /**
     * Focus this element
     */
    focus(): void;
    /**
     * Validate the input as needed
     * @public
     * @returns {Boolean} - If element value is valid
     */
    public validate(): boolean;
    /**
     * Validate a datetime input
     */
    _validateDatetime(): boolean;
    /**
     * Get a type coerced value. Input could be many types.
     * @private
     * @param {*} valueAny        - Value to convert into required type
     * @param {String} [type]     - Specific type to return, otherwise element value type
     * @param {Boolean} [nullish] - If falsy values ("", undefined or NaN) yield null. Default is this.emptyasnull
     */
    private _typedValue;
    /**
     * Get the value of a grouped element
     * @private
     */
    private _groupedValue;
    /**
     * Get the elements truthy value
     * @private
     */
    private get _truevalue();
    /**
     * Configure the element for any format mask
     */
    _setupFormatMask(): void;
    placeholder: string;
    /**
     * Update the visible or proxy input elements value
     * @private
     * @param {Boolean} withMask  - Return a mask if empty
     */
    private _updateInput;
    /**
     * Update our checked state from a value change
     */
    _updateChecked(): void;
    checked: any;
    unset: any;
    /**
     * Update the proxy checkbox input element
     */
    _updateToggle(): void;
    /**
     * Update our internal value from user input
     * @private
     */
    private _updateValue;
    /**
     * Validate a toggle type element
     * @private
     */
    private _validateToggle;
    /**
     * Validate a grouped toggle type element
     * @private
     */
    private _validateGrouped;
    /**
     * Update the message content
     * @private
     */
    private _updateMessage;
    _message: any;
    /**
     * Toggle this toggle type element as appropriate
     * @private
     */
    private _toggleInput;
    /**
     * Handle the element after input
     * @private
     */
    private _afterInput;
    /**
     * Handle the element when changing
     * @private
     */
    private _afterChange;
    /**
     * Handle the element when receiving focus
     * @private
     */
    private _afterFocus;
    /**
     * Handle the element when losing focus
     * @private
     */
    private _afterBlur;
    /**
     * Emit a search when attempted by search type
     */
    _emitSearch(ev: any): void;
    /**
     * Handle an input on the picker element
     */
    _pickerInput(ev: any): void;
    /**
     * Handle a change event on the picker element
     */
    _pickerChange(ev: any): void;
    /**
   * Handle format masks on keydown
   */
    _keyDown(ev: any): void;
    /**
     * Handle a paste into a formatted field
     */
    _beforePaste(ev: any): void;
    /**
     * Move within the elements groups
     * @private
     * @param {Number} direction    - The direction to move, > 0 for foward, < 0 for backward, 0 for current
     * @param {Event} ev            - The associated keydown event
     */
    private _moveGroup;
    /**
     * Format a string per the mask
     * @param {*} value           - The value to format
     * @param {String} fromformat - The provided format, or any if not known
     * @param {String} toformat   - The desired format
     */
    _format(value: any, fromformat: string, toformat: string): any;
    /**
     * Expand a short-formatted string to a full format mask
     * @private
     * @param {String} value    - Value to expand
     * @returns {String[]}      - Expanded string as array
     */
    private _expandToMask;
    /**
     * Convert a plain string to a full format string
     * @private
     * @param {String} value      - String to convert
     * @returns {String[]}        - Formatted string as an array
     */
    private _plainToMask;
    /**
     * Apply a format mask to a date
     * @private
     * @param {Date} date       - The date to format
     * @returns {String}        - The string representation
     */
    private _formatDate;
    /**
     * Construct a partial value for validation
     * @private
     * @param {String} inval  - Input value
     * @returns {String}      - Partial value
     */
    private _partialValue;
    /**
     * Update the elements label display
     * @protected
     */
    protected _updateLabel(autofill?: boolean): boolean;
    /**
     * Show the appropriate picker if there is one
     * @private
     */
    private _showPicker;
    /**
     * Work out the datetime picker type, and format min/maxes
     */
    _datetimeType(): void;
    _hasTime(): boolean;
    _hasDate(): boolean;
    /**
     * Get this elements template
     * @private
     */
    private _inputTemplate;
    /**
     * Build a picker input element"
     */
    _pickerTemplate(): import("lit-html").TemplateResult<1>;
    _pickerIconTemplate(): import("lit-html").TemplateResult<1>;
}
import { SimpleComponent } from '../index.js';
