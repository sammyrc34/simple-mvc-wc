
import { SimpleComponent } from '../index.js';

import buttonstyles from './button.css';


/**
 * An event fired when the button is clicked
 * @event SimpleButton#click
 */


// A container with other containers inside
class SimpleButton extends SimpleComponent {

  /**
   * Modified events:
   * @emits {click} Button click if not a reset or submit type
   */


  constructor() {
    super();
  }
  static get _elementName() { return 'sc-button' }
  static get properties() {
    return Object.assign( {}, super.properties, {
      disabled:     { type: Boolean, def: false,  desc: 'If this button should be disabled' },
      name:         { type: String,  def: "",     desc: 'Name for the button, used if submitted in a form' },
      value:        { type: String,  def: "",     desc: 'Value for the button, used if submitted in a form' },
      label:        { type: String,  def: "",     desc: 'Display label within the button' },
      outline:      { type: Boolean, def: false,  desc: 'If this button should use an outline style of display' },
      hover:        { type: Boolean, def: false,  desc: 'If this button should use an on-hover style of display' },
      icon:       { type: String,  def: "left", desc: 'Button icon position, either left, right, top or only' },
      type:       { type: String,  def: "",     desc: 'Action type if used within forms, submit or reset, or toggle for toggle buttons' },
      toggled:    { type: Boolean, def: false,  desc: 'Toggle state if a toggle button' },
      noanimate:  { type: Boolean, def: false,  desc: 'If this button should not use animation' }
    } );
  }

  
  static get styles() {
    return super.styles.concat( [ buttonstyles ] )
  }


  render() {
    // The slots and span must be on the same line to avoid space between the inline-block elements
    return this.builder`
      <div 
          id="button-wrap" 
          class="sc-button-wrap" 
          tabindex=-1
          @click=${this._inhibitClick}> 
        <button
            name="${this.name}"
            ?disabled=${this.disabled}
            ?outline=${this.outline}
            ?hover=${this.hover} 
            ?noanimate=${this.noanimate}
            ?toggled=${this.toggled}
            tabindex=0
            value="${this.value}" 
            @click=${this.tryClick}
          >
          ${this.iconAndText()}
        </button>
      </div>
    `;
  }

  iconAndText() {
    if ( this.icon == "only" ) {
      return this.builder`
        <div class="sc-button-block" icon="${this.icon}" tabindex="-1">
          <div class="sc-button-icon">
            <slot name="icon"></slot>
          </div>
        </div>
      `
    }
    else if ( this.icon == "top" ) {
      return this.builder`
        <div class="sc-button-block" icon="${this.icon}" tabindex="-1">
          <div class="sc-button-icon">
            <slot name="icon"></slot>
          </div>
          <span>${this.label}</span>
        <div>
      `
    }
    else if ( this.icon == "left" ) {
      return this.builder`
        <div class="sc-button-block" tabindex="-1">
          <slot name="icon"></slot>
          <span>${this.label}</span>
        </div>
      `
    }
    else if ( this.icon == "right" ) {
      return this.builder`
        <div class="sc-button-block" tabindex="-1">
          <span>${this.label}</span>
          <slot name="icon"></slot>
        </div>
      `
    }
  }


  /** 
   * Deliberately set focus to this button
   */
  focus() {
    const button = this.shadowRoot.querySelector( 'button' );
    button.focus();
  }


  /**
   * Inhibit a bubbled or other click outside the button
   */
  _inhibitClick( ev ) {
    const wrapper = this.shadowRoot.getElementById( 'button-wrap' );
    if ( ev.target == wrapper ) {
      ev.stopPropagation();
      return ev.preventDefault();
    }
  }

  /**
   * Attempt a form action if a type is set within a form.
   * Supports vanilla forms and SimpleForm elements
   */
  tryClick( ev ) {
    let form;
    ev.preventDefault();
    switch (this.type) {
      case "toggle":
        this.toggled = this.toggled ? false : true;
        break;
      case "reset":
        form = this.closest( 'sc-form' ) || this.closest( 'form' );
        return form.reset();
      case "submit":
        form = this.closest( 'sc-form' ) || this.closest( 'form' );
        return form.submit();
    }
  }
}


export { SimpleButton };


