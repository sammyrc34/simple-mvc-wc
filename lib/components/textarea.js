
import { SimpleComponent } from '../index.js';

import textAreaStyles from './textarea.css';


/**
 * Event emitted after user input in the control
 * @event SimpleTextarea#after-input
 */
const onAfterInput = 'after-input';


/**
 * Event emitted after a change is made to the value
 * @event SimpleTextarea#after-change
 */
const onAfterChange = 'after-change';



/**
 * Simple Textarea component
 */
class SimpleTextarea extends SimpleComponent {

  /**
   * Modified or custom events:
   * @emits {after-input} After keyboard input
   * @emits {after-change} After a change is made
   */

  constructor() {
    super();

    // Private
    this._initValue     = "";
    this._defaultValue  = ""
    this._smalllabel    = false;
    this._focused       = false;
    this._message       = "";
    this._hascomment    = false;
    this._pristine      = true;
  }

  static get _elementName() { return 'sc-textarea' }
  static get properties() {
    return Object.assign( {}, super.properties, {

      name:          { type: String, reflect: true, def: "",
        desc: "Name for the textarea when submitting within a form, overridden by the field property" },
      label:         { type: String, reflect: true, def: "",
        desc: "Text label for the textarea" },
      field:         { type: String, reflect: true, def: undefined, desc: "The field name this textarea is for if part of a form" },

      disabled:      { type: Boolean, reflect: true, def: false, desc: "If this textarea is disabled" },
      readonly:      { type: Boolean, reflect: true, def: false, desc: "If this textarea is read only to the user" },
      required:      { type: Boolean, reflect: true, def: false, desc: "If this textarea is required" },

      invalid:       { type: Boolean, def: false, desc: "If this textarea is currently invalid" },
      emptyasnull:   { type: Boolean, def: false, desc: "If an empty value returns null" },
      placeholder:   { type: String, def: "", desc: "A placeholder value" },
      smalllabel:    { type: Boolean, def: false, desc: "If the textarea should only display a small label" },
      comment:       { type: String, def: "", desc: "A helpful comment displayed under the textarea" },
      noanimate:     { type: Boolean, def: false, desc: "If this textarea should not animate" },
      nomessage:     { type: Boolean, def: false, desc: "Hide the comment and error message field" },
      errormessage:  { type: String, def: "", desc: "An error message displayed if the value is invalid" },

      rows:          { type: Number, def: 3, desc: "The number of text rows the textarea should display" },
      fill:          { type: Boolean, reflect: true, def: false,
        desc: "If this textarea should fill the container, ignoring rows property" },
      resizable:     { type: Boolean, def: false, desc: "If this textarea is resizable" },
      minlength:     { type: Number, def: undefined, desc: "Minimum value length for this textarea" },
      maxlength:     { type: Number, def: undefined, desc: "Maximum value length for this textarea" },
      pattern:       { type: String, def: undefined, desc: "Regular expression for value validation" },
      autovalidate:  { type: Boolean, def: false, desc: "If this textarea should autovalidate after input" },
      checkdisabled: { type: Boolean, def: false, desc: "Validate readonly and disabled textareas" },

      // Used only to monitor for changes
      _value:        { type: String, attribute: false, def: "" },

      // Private
      _smalllabel:  { type: Boolean, attribute: false },
      _focused:     { type: Boolean, attribute: false },
      _message:     { type: String, attribute: false },
      _hascomment:  { type: Boolean, attribute: false }
    } );
  }

  static get styles() {
    return super.styles.concat( [ textAreaStyles ] );
  }

  render() {
    return this.builder`
      <div class="sc-textarea"
        ?focused=${this._focused}
        ?invalid=${this.invalid}
        ?fill=${this.fill}
        ?resizable=${this.resizable}
      >
        <div class="sc-textarea-default-value">
          <slot></slot>
        </div>
        <div class="sc-textarea-control">
          <textarea
            name="${this.name}"
            ?required=${this.required}
            ?readonly=${this.readonly}
            ?disabled=${this.disabled}
            placeholder="${this.placeholder}"
            rows=${this.rows}
            minlength=${this.ifDefined(this.minlength)}
            maxlength=${this.ifDefined(this.maxlength)}
            @input=${this._afterInput}
            @change=${this._afterChange}
            @focus=${e=>{this._focused = true}}
            @blur=${this._afterBlur}
          ></textarea>
          ${ this.label == '' ? '': this.builder`
          <label
            for="${this.name}"
            class="sc-textarea-label"
            ?required=${this.required}
            ?disabled=${this.disabled}
            ?smalllabel=${this._smalllabel}
            ?noanimate=${this.noanimate}
          >${this.label}</label>`}
        </div>
        <div class="sc-textarea-message" ?hidden=${this.nomessage} ?invalid=${this.invalid} ?comment=${this._hascomment}>
          <slot name="message">
            <span>
              ${this._message}
            </span>
          </slot>
        </div>
      </div>

    `;
  }

  /*******************
   * Lit methods
   ******************/

  willUpdate( props ) {
    var control = this.shadowRoot.querySelector( 'textarea' );
    super.willUpdate( props );
    if ( control && props.has( "_value" ) )
      control.value = this._value;
    if ( props.has( "comment" ) && this.comment != "" )
      this._hascomment = true
    this._smalllabel = this.smalllabel;
    if ( this.placeholder != "" || this._value != "" )
      this._smalllabel = true;
    if ( ! this._pristine && this.autovalidate )
      this.validate();
    this._updateMessage();
  }


  firstUpdated() {
    super.firstUpdated();
    let defValue = this.textContent.replace(/^[\n\r]/g, '').trimEnd();
    if ( this._value == "" && defValue !== "" ) {
      this._initValue = defValue;
      this._defaultValue = defValue;
      this._value = defValue;
    }
    else {
      this._initValue     = this._value;
      this._defaultValue  = this._value;
    }
  }


  /***********************
   * Element methods
   **********************/

  /**
   * Get the element value
   * @public
   * @returns {*} - A typed value per valuetype and emptyasnull attributes
   */
  get value() {
    let raw = this.shadowRoot.querySelector( 'textarea' ).value;
    if ( this.emptyasnull && ( raw === "" || raw === undefined ) )
      return null;
    else
      return raw;
  }


  /**
   * Set the elements value
   * @public
   * @param {*} value - The value for the element
  */
  set value( val ) {
    if ( val === null || val === undefined )
      this._value = "";
    else
      this._value = val;
  }


  /**
   * Reset the elements value to last initial value, or default value
   * @public
   * @param {Boolean} toDefault - If the element should be reset to defaults instead of initial value
   */
  reset( toDefault=false ) {
    if ( toDefault ) {
      this.value      = this._defaultValue;
      this._initValue = this._defaultValue;
    }
    else
      this.value = this._initValue;

    this.invalid    = false;
    this._pristine  = true;
  }


  /**
   * Set this elements value to an initial value
   * @public
   * @param {*} value - The initial value to set
   */
  setInitial( val ) {
    if ( val === null || val === undefined )
      this.value = "";
    else
      this.value = val;
    this._initValue = val;
  }


  /**
   * Has this elements value changed?
   * @public
   * @returns {Boolean} - If this element has changed
   */
  changed() {
    return this.value != this._initValue;
  }


  /**
   * Focus this element
   */
  focus() {
    const textarea = this.shadowRoot.querySelector( 'textarea' );
    if ( textarea )
      textarea.focus();
  }


  /**
   * Validate the input as needed
   * @public
   * @returns {Boolean} - If element value is valid
   */
  validate() {
    var valid = true;

    if ( this.checkdisabled || ( ! this.disabled && ! this.readonly ) ) {
      if ( this.required && this.value == "" )
        valid = false;
      if ( valid && this.pattern != "" && ! new RegExp( this.pattern ).test( this.value ) )
        valid = false;
      if ( valid && ! this.shadowRoot.querySelector( 'textarea' ).checkValidity() )
        valid = false;
      if ( valid && this.validator != undefined && typeof this.validator == "function" )
        valid = this.validator( this.value );
    }

    this.invalid = ! valid;
    this._updateMessage();
    return valid;
  }



  _updateMessage() {
    if ( this.invalid )
      this._message = this.errormessage;
    else
      this._message = this.comment;
  }


  _afterInput( ev ) {
    this.dispatchEvent( new CustomEvent( onAfterInput, { bubbles: true, cancelable: false } ) );
    this._updateLabel( ev );
  }


  _afterChange( ev ) {
    this.dispatchEvent( new CustomEvent( onAfterChange, { bubbles: true, cancelable: false } ) );
    this._pristine = false;
    if ( this.autovalidate )
      this.validate();
    this._updateLabel( ev );
  }


  _afterBlur( ) {
    // This get's the control's value and sets the component's copy
    this.value = this.value;
    this._focused = false;
  }


  _updateLabel(ev) {
    if ( ev.target.value != "" || this.placeholder != "" )
      this._smalllabel = true;
    else if ( this.smalllabel == false )
      this._smalllabel = false;
  }


}


export { SimpleTextarea };


