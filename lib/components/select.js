
// TODO:
// 1) Search term highlight with optionTemplate

import { SimpleComponent } from '../index.js';
import { DataboundComponent } from './databound.js';

import { library as faLib, icon as faIcon } from "@fortawesome/fontawesome-svg-core";
import { faAngleDown, faAngleUp } from "@fortawesome/free-solid-svg-icons";

import selectOptionStyles from './select-1.css';
import selectStyles from './select-2.css';

faLib.add( faAngleDown, faAngleUp );


/**
 * A cancelable event fired before (de)selection of an option element
 * @event SimpleSelect#before-select
 * @event SimpleSelectList#before-select
 * @type {Object}
 * @property {HTMLElement} option     - The option element generating the selection
 * @property {String|Number} value    - The value of the option that was selected
 * @property {Boolean} selecting      - If the option is being selected
 */
const onBeforeSelect = 'before-select';


/**
 * An event after the (de)selection of an option element
 * @event SimpleSelect#after-select
 * @event SimpleSelectList#after-select
 * @type {Object}
 * @property {HTMLElement} option     - The option element generating the selection
 * @property {String|Number} value    - The value of the option that was selected
 * @property {Boolean} selecting      - If the option is being selected
 */
const onAfterSelect = 'after-select';


/**
 * An event fired after change in value of this control
 * @event SimpleSelect#after-change
 * @event SimpleSelectList#after-change
 * @type {Object}
 */
const onAfterChange = 'after-change';


class SimpleSelect extends DataboundComponent {

  /**
   * @emits {before-select} Before a selection is made via option
   * @emits {after-select} After a selection is made via option
   * @emits {after-change} After a change in value
   */

  constructor() {
    super();

    // Private and stateful
    this.bindMode         = "multi";
    this.bindToNew        = false;
    this._values          = [];
    this._optionsmap      = new Map;
    this._focused         = false;
    this._animatelabel    = true;
    this._smalllabel      = false;
    this._showopts        = false;
    this._open            = false;
    this._customvalue     = false;
    this._valueelement    = undefined;
    this._displayvalue    = "";
    this._message         = "";
    this._isplaceholder   = false;
    this._hascomment      = false;
    this._current         = undefined;
    this._initSelected    = [];
    this._defaultSelected = [];
    this._filter          = "";
    this._height          = 0;
    this._maxheight       = 0;

  }
  static get _elementName() { return 'sc-select' }
  static get properties() {
    return Object.assign( {}, super.properties, {

      // Re-define to hide from docs
      bindId:            { type: String, def: undefined, attribute: false },

      name:              { type: String, reflect: true, def: "",
        desc: "Name for the select when submitting within a form, overridden by field property" },
      field:             { type: String, reflect: true, def: undefined,
        desc: "The field name this select is for if part of a form" },
      label:             { type: String, reflect: true, def: "",
        desc: "The text label for this select" },
      value:             { type: Array, def: undefined, converter: (v) => { return v },
        desc: "The initial value for the select (via attribute)" },

      disabled:          { type: Boolean, reflect: true, def: false,
        desc: "If this control is disabled" },
      readonly:          { type: Boolean, reflect: true, def: false,
        desc: "If this control is read only" },
      required:          { type: Boolean, reflect: true, def: false,
        desc: "If input on this control is required" },

      data:              { type: Array, def: undefined, desc: "List of Objects representing select options if using generated options" },
      invalid:           { type: Boolean, def: false, desc: "If this control is invalid" },
      maxselect:         { type: Number, def: 1, desc: "The maximum number of options that can be selected, 0 for no limit" },
      emptyasnull:       { type: Boolean, def: false, desc: "If an empty selection results in the value being null" },
      placeholder:       { type: String, def: "", desc: "Any placeholder string when no selections" },
      position:          { type: String, def: "auto", desc: "Options position, auto, top or bottom" },
      maxheight:         { type: Number, def: undefined, desc: "Maximum height of the options panel" },
      search:            { type: Boolean, def: false, desc: "If a search control is visible in the option list" },
      nomessage:         { type: Boolean, def: false, desc: "Hide the comment and error message field" },
      noanimate:         { type: Boolean, def: false, desc: "Disable animation for this control" },
      smalllabel:        { type: Boolean, def: false, desc: "Display only a small label variant" },
      valuestyle:        { type: String, def: "simple", desc: "Value display style, either simple, tags or custom via a set valueTemplate function" },
      valuetype:         { type: String, def: "string", desc: "Type of value exptected and returned, one of boolean, string, number and date"  },
      valueField:        { type: String, def: "", desc: "Data field containing the value if using generated options" },
      textField:         { type: String, def: "", desc: "Text field containing the label if using generated options" },
      autovalidate:      { type: Boolean, def: false, desc: "If this control should run validation after each change" },
      comment:           { type: String, def: "", desc: "Any comment to display to the user" },
      errormessage:      { type: String, def: "", desc: "Any error message to display to the user if control is not valid" },
      checkdisabled:     { type: Boolean, def: false, desc: "If validation should be performed on a disabled or read only control" },

      optionTemplate:    { type: Function, attribute: false, def: undefined, desc: "A display option template builder function for data-sourced options, called with each data record" },
      valueTemplate:     { type: Function, attribute: false, def: undefined, desc: "A display value template builder function, called with selected option elements" },
      validator:         { type: Function, attribute: false, def: undefined, desc: "A custom funtion for testing validation, called with the typed value" },



      /* private or stateful */
      _values:           { type: Array, attribute: false },
      _displayvalue:     { type: String, attribute: false },
      _message:          { type: String, attribute: false },
      _isplaceholder:    { type: Boolean, attribute: false },
      _showopts:         { type: Boolean, attribute: false },
      _open:             { type: Boolean, attribute: false },
      _focused:          { type: Boolean, attribute: false },
      _smalllabel:       { type: Boolean, attribute: false },
      _hascomment:       { type: Boolean, attribute: false }
    } );
  }


  static get styles() {
    return super.styles.concat( [ selectStyles ] );
  }

  connectedCallback() {
    super.connectedCallback();
    window.addEventListener( 'resize', () => this._positionOpts() )
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    window.addEventListener( 'resize', () => this._positionOpts() )
  }

  render() {
    // The slots and span must be on the same line to avoid space between the inline-block elements
    return this.builder`
      <div class="sc-select"
          ?required=${this.required}
          ?invalid=${this.invalid}
          tabindex="${(this.search && this._focused) ? -1 : 0}"
          @_try-select-option=${this._trySelectOption}
          @_try-unselect-option=${this._tryUnselectOption}
          @_active-change=${this._activeChange}
          @focusin=${this._focusIn}
          @focusout=${this._closeOpts}
          @keydown=${this._keyDown}
      >
        <div class="sc-select-control"
            ?disabled=${this.disabled}
            ?focused=${this._focused}
            @click=${this._toggleOptions}
        >
          ${this.renderValue()}
          <label
            for=${this.name}
            class="sc-select-label"
            ?required=${this.required}
            ?disabled=${this.disabled}
            ?smalllabel=${this._smalllabel}
            ?noanimate=${this.noanimate}
          >
            <slot name="label">
              <span>${this.label}</span>
            </slot>
          </label>
          <div class="sc-select-separator"></div>
          <div class="sc-select-icon"
              @click=${this._toggleOptions}
              @mousedown=${e=>e.preventDefault()}
          >
            ${this._open?
              this.includeSvg( faIcon( faAngleUp ).html[0] ) :
              this.includeSvg( faIcon( faAngleDown ).html[0] )
            }
          </div>
          <input type="hidden" name=${this.name}></input>
        </div>
        <div class="sc-select-options-wrap-outer" >
          <div class="sc-select-options-wrap" ?noanimate=${this.noanimate} ?sized=${this._showopts}>
            <div class="sc-select-options-border">
              ${this.search? this.builder`
                <div class="sc-select-option-search">
                  <input
                    type="search"
                    tabindex="-1"
                    placeholder="Search..."
                    @input=${this._filterOptions}
                    @focusin=${this._filterOptions}
                    @keydown=${this._searchKeyDown}
                  >
                </div>
              `: ''}
              ${this.optionsTemplate()}
            </div>
          </div>
        </div>
        <div class="sc-select-message" ?hidden=${this.nomessage} ?noanimate=${this.noanimate} ?invalid=${this.invalid} ?comment=${this._hascomment}>
          <slot name="message">
            ${this._message}
          </slot>
        </div>
      </div>
    `;
  }


  renderValue() {
    const opts = Array.from( this._optionsmap.values() );
    if ( this.valuestyle == "simple" ) {
      return this.builder`
        <div class="sc-select-value-wrap" ?placeholder=${this._isplaceholder}>
          <div class="sc-select-value">
            ${this._displayvalue}
          </div>
        </div>
      `
    }
    else if ( this.valuestyle == "tags" ) {
      return this.builder`
        <div class="sc-select-value-wrap" ?placeholder=${this._isplaceholder}>
          ${opts.length == 0
              ? this._displayvalue
              : opts.map( (s) => {
                return this.builder`
                  <div class="sc-select-value-tag" data-value=${s.value} @click=${this._deselectFromDisplay}>
                    ${s.label}
                  </div>
                ` } )
          }
        </div>
      `
    }
    else if ( this.valuestyle == "custom" ) {
      return this.builder`
        <div class="sc-select-value-wrap" ?placeholder=${this._isplaceholder}>
          ${opts.length == 0
              ? this._displayvalue
              : this.builder`
                  <div class="sc-select-value-custom">
                    ${this.valueTemplate( selected )}
                  </div>`
          }
        </div>
      `
    }
  }


  optionsTemplate() {
    let values, data, valueField, textField;
    if ( this.modelName == undefined && this.data == undefined ) {
      return this.builder`
          <div class="sc-select-options">
            <slot></slot>
          </div>
      `
    }
    else {
      data = this.data || [];
      valueField = this.valueField;
      textField = this.textField || "";
      values = this._values;
      if ( valueField === undefined && this.dataModel !== undefined )
        valueField = this.idField;
      return this.builder`
        <div class="sc-select-options">
          <slot></slot>
          ${this.optionTemplate !== undefined
          ? data.map( row => this.builder`
              <sc-select-option
                  value=${row[valueField]}
                  label=${row[textField]}
                  ?selected=${values.includes( row[valueField] )}
                  generated
              >${this.optionTemplate(row)}</sc-select-option>
            ` )
          : data.map( row => this.builder`
              <sc-select-option
                  value=${row[valueField]}
                  label=${row[textField]}
                  ?selected=${values.includes( row[valueField] )}
                  generated
              ></sc-select-option>
            ` )
          }
        </div>
      `
    }
  }

  /******************
   * Lit methods
   *****************/

  /* An update is happening, setup the component */
  willUpdate( changes ) {
    super.willUpdate( changes );

    // Resize options on data change
    if ( changes.has( 'data' ) )
      this._maxheight = 0;

    // Update display flags
    this._smalllabel = this.smalllabel;
    if ( this.placeholder != "" || this._displayvalue != "" )
      this._smalllabel = true;
    if ( this._open )
      this._smalllabel = true;
    if ( changes.has( 'comment' ) && this.comment != "" )
      this._hascomment = true

    // Validate
    if ( changes.has( '_values' ) && this.autovalidate && this.hasUpdated )
      this.validate();

    this._updateMessage();
  }


  async updated( changes ) {
    super.updated( changes );

    // Position options
    if ( changes.has( '_open' ) ) {
      if ( this._open ) {
        this._positionOpts();
        if ( this.search )
          this.shadowRoot.querySelector( ".sc-select-option-search input" ).focus();
      }
      else
        this._shrinkOpts();
    }

    // Find selected option elements. Has to occur after rendering for bound controls
    if ( changes.has( 'data' ) || changes.has( 'value' ) || changes.has( '_values' ) )
      this._getSelected();

    // Display value. Depends on options map from _getSelected
    if ( changes.has( '_values' ) )
      this._updateDisplayValue();

    // Notify listeners
    if ( changes.has( '_values' ) )
      this._selectionChange();

    // Update option selections
    if ( changes.has( 'data' ) || changes.has( 'value' ) || changes.has( '_values' ) )
       this._updateSelections();
  }


  async firstUpdated() {
    super.firstUpdated();

    this._shrinkOpts();

    // Setup default and initial values
    this._initialSelected();
  }


  /******************
   * Own methods
   *****************/

  /**
   * Get the current value(s)
   * @returns {String|Number|String[]|Number[]} - Selected value(s)
   */
  get value() {
    if ( this._values.length == 0 && this.emptyasnull )
      return null;
    if ( this.maxselect == 1 )
      return this.app.util.clone( this._values[ 0 ] );
    else
      return this.app.util.clone( this._values );
  }


  /**
   * Set the currently selected value(s)
   * @param {String|Number|String[]|Number[]} vals - Values to be selected
   */
  set value( vals ) {
    // Coerce to array
    if ( ! Array.isArray( vals ) )
      vals = [ vals ];

    // Convert to our type
    vals = this._typeValues( vals );

    if ( this.maxselect == 1 && vals.length > 1 )
      throw new Error( "Unable to set multiple values to single value control" );

    this._values = vals;
  }


  /**
   * Set this controls initial value
   * @param {String|Number|String[]|Number[]} values - The selected values
   */
  setInitial( values ) {
    // Coerce to array
    if ( ! Array.isArray( values ) )
      values = [ values ];

    this._initSelected = values
    this.value = values;
  }


  /**
   * Reset the value to the initial value or a default value
   * @public
   * @param {Boolean} [toDefault=false] - Reset to the default value instead
   */
  reset( toDefault=false ) {
    if ( toDefault ) {
      this._values        = this._defaultSelected;
      this._initSelected  = this._defaultSelected;
    }
    else
      this._values        = this._initSelected;

    this.invalid    = false;
  }


  /**
   * Is the value of this component changed?
   * @public
   * @returns {Boolean}
   */
  changed() {
    if ( this._initSelected.length !== this._values.length )
      return true;

    for ( let val of this._values )
      if ( ! this._initSelected.includes( val ) )
        return true;

    return false;
  }


  /**
   * Validate this input as needed
   * @returns {Boolean} - If validation was successful
   * @public
   */
  validate() {
    let valid = true;

    if ( this.checkdisabled || ( ! this.disabled && ! this.readonly ) ) {
      if ( this.required && this._values.length === 0 )
        valid = false;
      if ( valid && this.validator !== undefined && typeof this.validator === "function" )
        valid = this.validator( this.value );
    }

    this.invalid = ! valid;
    return valid;
  }


  /**
   * Get the currently selected options
   */
  _getSelected() {
    const options = this._options;
    const selections = this._values;
    let val;

    this._optionsmap = new Map();

    for ( let opt of options ) {
      val = this._typeValue( opt.value );
      if ( selections.includes( val ) )
        this._optionsmap.set( val, opt );
    }
  }


  /**
   * Get any initially selected elements, set our default value and current if not otherwise set
   */
  _initialSelected() {
    const options = this._options;
    const current = this._values;
    let selected = [];

    for ( let opt of options ) {
      if ( opt.selected )
        selected.push( opt );
    }

    if ( selected.length ) {
      this._defaultSelected = selected.map( s => this._typeValue( s.value ) );
      if ( ! current.length ) {
        this._initSelected = this._defaultSelected;
        this._values = this.app.util.clone( this._defaultSelected );
      }
      else
        this._initSelected = this.app.util.clone( current );
    }
  }


  /**
   * Set the selection state on options, and build the value
   */
  _updateSelections() {
    const options = this._options;
    const selected = Array.from( this._optionsmap.values() );

    for ( let opt of options ) {
      if ( selected.includes( opt ) )
        opt.selected = true;
      else
        opt.selected = false;
    }
  }


  /**
   * Get all options
   * @protected
   */
  get _options() {
    const options = [];

    // Not yet rendered?
    if ( ! this.shadowRoot )
      return [];
    if ( this.shadowRoot.querySelector( ".sc-select-options" ) == null )
      return [];

    options.push(
      ...Array.from(
        this.shadowRoot.querySelector( ".sc-select-options slot" ).assignedNodes()
      ).filter( n => n.nodeType === Node.ELEMENT_NODE )
    );

    options.push(
      ...Array.from(
        this.shadowRoot.querySelector( ".sc-select-options" ).childNodes
      ).filter( n => n.nodeType === Node.ELEMENT_NODE && n.tagName !== "SLOT" )
    );

    return options;
  }


  /**
   * Coerce the value types as needed
   * @param {*} values    - Values to type-coerce
   * @returns             - Coerced values
   * @private
   */
  _typeValues( values ) {
    const converted = [];
    for ( let val of values ) {
      converted.push( this._typeValue( val ) );
    }

    // Set null if needed
    if ( converted.length == 1 && converted[0] === "" && this.emptyasnull )
      converted[ 0 ] = null;
    else if ( converted.length == 0 && this.emptyasnull )
      converted[ 0 ] = null;

    return converted;
  }


  /**
   * Type coerce a value
   * @param {*} value     - The value to be type coerced, usually a string
   * @returns {*}         - The type coerced value
   */
  _typeValue( value ) {
    const type = this.valuetype;

    // Nulls and empty strings are never type coerced
    if ( value === null || value === "" ) {
      return value;
    }

    // Undefined values are interpretted as empty strings
    if ( value === undefined ) {
      return "";
    }


    switch( type ) {
      case "boolean":
        return ( value ) ? true : false;
      case "string":
        if ( typeof value !== "string" && value.toString )
          return value.toString();
        else if ( typeof value !== "string" )
          return value + "";
        else
          return value;
      case "number":
        return ( typeof value == "number" ) ? value : parseFloat( value );
      case "date":
        return ( value instanceof Date ) ? value : new Date( value );
    }
  }



  /**
   * Detect the options panel size
   * @private
   */
  _detectSize() {
    let opts, bounds;
    opts = this.shadowRoot.querySelector( '.sc-select-options-wrap' );
    opts.style.setProperty( 'opacity', 0 );
    opts.style.removeProperty( 'max-height' );
    bounds = opts.getBoundingClientRect();
    this._maxheight = bounds.height;
    opts.style.setProperty( 'max-height', "0px" );
    opts.style.removeProperty( 'opacity' );
    this._showopts = true;
  }


  /**
   * Handle a model status change
   * @protected
   * @see DataboundComponent#_statusChanged
   */
  _statusChanged( type ) {
    if ( type == "bind" )
      this._dataChanged();
    else if ( type == "cleared" || type == "unbind" )
      this.data = [];
  }


  /**
   * Handle a model data change
   * @protected
   * @async
   * @see DataboundComponent#_dataChanged
   */
  async _dataChanged(){
    if ( ! this.dataModel )
      return;
    this.data = await this.dataModel.fetch();
  }


  /**
   * Update the displayed message or error
   * @protected
   */
  _updateMessage() {
    if ( this.invalid )
      this._message = this.errormessage;
    else
      this._message = this.comment;
  }


  /**
   * Handle a key down event in the search input
   * @protected
   */
  _searchKeyDown(ev) {
    const search = this.shadowRoot.querySelector( ".sc-select-option-search input" );
    if ( ev.key == "ArrowDown" || ev.key == "ArrowUp" )
      ev.preventDefault();
    if ( ( ev.key == " " && ! search.value ) || ( ev.key == " " && ev.shiftKey ) ) {
      this._selectDeselectOption( this._current, false, false );
      ev.preventDefault();
    }
  }


  /**
   * Handle a key down event
   * @protected
   * @param {Event} ev          - The keydown event
   */
  _keyDown(ev) {
    if ( this.readonly )
      return;
    if ( ! this._open && ev.key == "ArrowDown" ) {
      ev.preventDefault();
      this._open = true;
      return;
    }
    if ( ! this._open )
      return;
    else if ( this.search && this._open && ev.key == "ArrowRight" ) {
      this._selectDeselectOption( this._current, false, false );
      ev.preventDefault();
    }
    else if ( ! this.search && ev.key == " " ) {
      ev.preventDefault();
      this._selectDeselectOption( this._current, false, false );
    }
    else if ( this.search && ev.key == " " && ev.ctrlKey ) {
      ev.preventDefault();
      this._selectDeselectOption( this._current, false, false );
    }
    else if ( ev.key == "Enter" ) {
      this._selectDeselectOption( this._current, true, true );
      ev.preventDefault();
    }
    else if ( ev.key == "ArrowDown" ) {
      ev.preventDefault();
      this._moveDown();
    }
    else if ( ev.key == "ArrowUp" ) {
      ev.preventDefault();
      this._moveUp();
    }
    else if ( ev.key == "Escape" )
      this._closeOpts();
  }


  /**
   * Move the active element to the previous one
   * @protected
   */
  _moveUp() {
    const visible = this._options.filter( o => o.visible );
    let next = visible.indexOf( this._current );
    if ( visible.length == 0 )
      return;
    if ( next == -1 || next == 0 || next == undefined )
      next = visible.length - 1;
    else
      next = next - 1;
    this._setActiveOption( next );
  }


  /**
   * Move the active element to the next one
   * @protected
   */
  _moveDown() {
    const visible = this._options.filter( o => o.visible );
    let next = visible.indexOf( this._current );
    if ( visible.length == 0 )
      return;
    if ( next == undefined )
      next = 0;
    else
      next = next + 1;
    if ( next > ( visible.length - 1 ) )
      next = 0;
    this._setActiveOption( next );
  }


  /**
   * Toggle an active element
   * @protected
   * @param {HTMLElement} option    - Option to select or de-select
   * @param {Boolean} close         - If the selection box should close after selection
   * @param {Boolean} onlyselect    - If this should select the item only, not de-select
   */
  _selectDeselectOption( option, close, onlyselect ) {
    let valueTyped, idx;

    valueTyped = this._typeValue( option.value );
    idx = this._values.indexOf( valueTyped );

    // Deselect or select
    if ( idx >= 0 ) {
      if ( ! onlyselect )
        this._values = this._values.filter( (_,i) => i !== idx ); // Don't mutate as lit won't notice
    }
    else {
      if ( this.maxselect == 1 )
        this._values = [ valueTyped ];
      if ( this.maxselect == 0 || this._values.length < this.maxselect )
        this._values = this._values.concat( [ valueTyped ] ); // Don't mutate as lit won't notice
    }

    // Close as needed
    if ( close )
      this._closeOpts();
  }



  /**
   * The active option has changed, store current
   * @protected
   */
  _activeChange(ev) {
    if ( this._current && this._current !== ev.target )
      this._current.active = false;
    this._current = ev.target;
  }


  /**
   * Set an option as active by index
   * @protected
   */
  _setActiveOption( number ) {
    const visible = this._options.filter( o => o.visible );
    this._options.forEach( o => { if ( o.active ) o.active = false; } );

    // Out of bounds?
    if ( number < 0 || number >= visible.length ) {
      this._current = undefined;
      return;
    }

    visible[number].active = true;
    visible[number].scrollIntoView({ block: "nearest", inline: "nearest" });
    this._current = visible[number];
  }


  /**
   * Notify listeners that our selections have changed
   * @protected
   */
  _selectionChange() {
    this.dispatchEvent( new CustomEvent( onAfterChange, { bubbles: true, cancelable: false } ) );
  }


  /**
   * Work out the displayed value, show placeholder as needed
   * @protected
   */
  _updateDisplayValue( ) {
    const displayVals = [];

    for ( let opt of this._optionsmap.values() )
      displayVals.push( opt.label );

    if ( ! displayVals.length ) {
      if ( this.placeholder !== "" )
        this._displayvalue = this.placeholder;
      else
        this._displayvalue = "";
    }
    else
      this._displayvalue = displayVals.join( ', ' );
  }


  /**
   * Focus the options panel
   * @protected
   */
  _focusIn() {
    if ( this.readonly || this.disabled )
      return;
    this._focused = true;
  }


  /**
   * Close the options panel
   * @protected
   */
  _closeOpts( ev ) {
    let searchInput;

    // Not yet rendered?
    if ( ! this.shadowRoot )
      return;

    searchInput = this.shadowRoot.querySelector( ".sc-select-option-search input" );

    /* ignore focusout from outselves */
    if ( ev && this.shadowRoot.contains( ev.relatedTarget ) )
      return;

    /* ignore focusout from own focus change */
    if ( ev && this.search && ev.relatedTarget == searchInput )
      return;

    /* focusout lost to outside */
    if ( ev )
      this._focused = false;

    if ( ! this._open )
      return;

    /* shift focus from search for manual close */
    if ( this.search && ! ev )
      this.shadowRoot.firstElementChild.focus();

    if ( this.autovalidate )
      this.validate();

    this._open = false;
  }


  /**
   * Select an option if it can be selected
   * @protected
   */
  _trySelectOption(ev) {
    const target = ev.target;
    const value = this._typeValue( target.value );
    let ok;

    if ( this.readonly || this.disabled )
      return;

    // Fire pre selection event
    ok = this.dispatchEvent( new CustomEvent( onBeforeSelect, {
      detail: { option: target, value: value, selecting: true },
      bubbles: true, cancelable: true
    } ) );
    if ( ok === false )
      return;

    // Unable to select more?
    if ( this.maxselect !== 1 && this.maxselect !== 0 && this._values.length >= this.maxselect )
      return;

    // De-select other options
    if ( this.maxselect == 1 )
      this._values = [ value ];
    else
      this._values = this._values.concat( [ value ] );

    // After selecting a single option
    if ( this.maxselect == 1 )
      this._closeOpts();

    // After selection event
    this.dispatchEvent( new CustomEvent( onAfterSelect, {
      detail: { option: target, value: value, selecting: true },
      bubbles: true, cancelable: false
    } ) );
  }


  /**
   * Check if an option can be un-selected
   * @protected
   */
  _tryUnselectOption(ev) {
    const target = ev.target;
    const value = this._typeValue( target.value );
    let ok;

    if ( this.readonly || this.disabled )
      return;

    // Need one selected?
    if ( this.required && this._values.length <= 1 )
      return;

    ok = this.dispatchEvent( new CustomEvent( onBeforeSelect, {
      detail: { option: target, value: value, selecting: false },
      bubbles: true, cancelable: true
    } ) );
    if ( ok === false )
      return;

    this._values = this._values.filter( v => v !== value );

    // After selection event
    this.dispatchEvent( new CustomEvent( onAfterSelect, {
      detail: { option: target, value: value, selecting: false },
      bubbles: true, cancelable: false
    } ) );
  }


  /**
   * Filter the option pane
   * @protected
   */
  _filterOptions(ev) {
    const opts = this._options;
    const filter = ev.target.value;
    let visible = opts.filter( o => o.visible );
    let reg, activeIndex;

    /* clear filter */
    if ( filter == "" ) {
      opts.forEach( o => { o.visible = true; o.filter = "" } );
      this._filter = "";
      return;
    }

    reg = new RegExp( filter, "i" );

    /* non-consecutive filters */
    if ( ! filter.startsWith( this._filter ) )
      visible = opts;
    this._filter = filter;

    // Update visible options list
    visible.forEach( option => {
      if ( ! option.label.match( reg ) ) {
        option.visible = false;
        option.filter = "";
      }
      else
        option.visible = true;
        option.filter = this._filter;
    } );
    visible = opts.filter( o => o.visible );

    // Nothing active yet?
    if ( ! this._current ) {
      if ( visible.length )
        this._current = visible[ 0 ];
      else
        return;
    }

    /* make nearest option active */
    activeIndex = visible.indexOf( this._current );
    if ( activeIndex == -1 ) {
      activeIndex = opts.indexOf( this._current );
      if ( activeIndex > visible.length )
        this._setActiveOption( visible.length - 1 );
      else
        this._setActiveOption( activeIndex );
    }
    else
      this._setActiveOption( activeIndex );
  }


  /**
   * Toggle the option list visiblity
   * @protected
   */
  _toggleOptions(ev) {
    const target = ev.target;
    if ( ev ) {
      ev.preventDefault();
      ev.stopPropagation();
    }
    if ( this._open )
      return this._closeOpts();
    if ( ! this._open && this._focused ) {
      if ( ! this.readonly && ! this.disabled )
        this._open = true;
    }
    else if ( ! this._open && target.classList.contains( 'sc-select-icon' ) ) {
      if ( ! this.readonly && ! this.disabled )
        this._open = true;
    }

    this.shadowRoot.firstElementChild.focus();
  }


  /**
   * Shrink the options panel for animation purposes
   * @protected
   */
  _shrinkOpts() {
    const opts = this.shadowRoot.querySelector( '.sc-select-options-wrap' );
    opts.style.setProperty( 'max-height', '0px' );
  }


  /**
   * Re-position the option panel
   * @protected
   */
  _positionOpts() {
    if ( ! this._open )
      return;
    const inner = this.shadowRoot.querySelector( '.sc-select-options-wrap' );
    const controlBounds = this.getBoundingClientRect();
    const control = this.shadowRoot.querySelector( '.sc-select-control' );
    const margin = parseFloat( window.getComputedStyle( control ).marginTop );
    let roomBelow, roomAbove;

    // Detect maximum size if needed
    if ( this._maxheight == 0 )
      this._detectSize();

    roomBelow = window.innerHeight - ( controlBounds.top + controlBounds.height );
    roomAbove = controlBounds.top;

    if ( this.position == "top" ) {
      inner.style.setProperty( 'bottom', ( controlBounds.height - margin ) + "px" );
      this._height = Math.min( roomBelow - margin * 2, this._maxheight );
    }
    else if ( this.position == "bottom" ) {
      this._height = Math.min( roomBelow - 2 * margin, this._maxheight );
      inner.style.removeProperty( 'bottom' );
    }
    else if ( roomBelow < this._maxheight && roomAbove > this._maxheight ) {
      inner.style.setProperty( 'bottom', ( controlBounds.height - margin ) + "px" );
      this._height = this._maxheight;
    }
    else if ( ( roomAbove * 0.5 ) > roomBelow ) {
      inner.style.setProperty( 'bottom', ( controlBounds.height - margin ) + "px" );
      this._height = Math.min( roomAbove - 2 * margin, this._maxheight );
    }
    else if ( roomBelow < this._maxheight ) {
      inner.style.removeProperty( 'bottom' );
      this._height = Math.min( roomBelow - margin * 2, this._maxheight );
    }
    else {
      inner.style.removeProperty( 'bottom' );
      this._height = this._maxheight;
    }

    inner.style.setProperty( 'max-height', Math.min( this.maxheight || this._maxheight, this._height ) + 'px' );
  }


    /**
   * Attempt to deselect an option by display value control
   * @protected
   */
  _deselectFromDisplay(ev) {
    const optionValue = this._typeValue( ev.target.dataset.value );
    const selected = this._values;

    // Unable to remove?
    if ( selected.length <= 1 && this.required )
      return;

    // Note, don't mutate
    this._values = this._values.filter( v => v !== optionValue );
  }
}


class SimpleSelectList extends DataboundComponent {

  /**
   * @emits {before-select} Before a selection is made via option
   * @emits {after-select} After a selection is made via option
   * @emits {after-change} After a change in value
   */

  constructor() {
    super();

    // Private and stateful
    this.bindMode         = "multi";
    this.bindToNew        = false;
    this._values          = [];
    this._optionsmap      = new Map;
    this._focused         = false;
    this._customvalue     = false;
    this._valueelement    = undefined;
    this._message         = "";
    this._hascomment      = false;
    this._current         = undefined;
    this._initSelected    = [];
    this._defaultSelected = [];
    this._filter          = "";
  }

  static get _elementName() { return 'sc-select-list' }
  static get properties() {
    return Object.assign( {}, super.properties, {

      // Re-define to hide from docs
      bindId:            { type: String, def: undefined, attribute: false },

      name:              { type: String, reflect: true, def: "",
        desc: "Name for the select when submitting within a form, overridden by field property" },
      field:             { type: String, reflect: true, def: undefined,
        desc: "The field name this select is for if part of a form" },
      label:             { type: String, reflect: true, def: "",
        desc: "The text label for this select" },
      value:             { type: Array, def: undefined,
        desc: "The initial value for the select (via attribute)" },

      disabled:          { type: Boolean, reflect: true, def: false,
        desc: "If this control is disabled" },
      readonly:          { type: Boolean, reflect: true, def: false,
        desc: "If this control is read only" },
      required:          { type: Boolean, reflect: true, def: false,
        desc: "If input on this control is required" },

      data:              { type: Array, def: undefined, desc: "List of Objects representing select options if using generated options" },
      invalid:           { type: Boolean, def: false, desc: "If this control is invalid" },
      maxselect:         { type: Number, def: 1, desc: "The maximum number of options that can be selected, 0 for no limit" },
      emptyasnull:       { type: Boolean, def: false, desc: "If an empty selection results in the value being null" },
      search:            { type: Boolean, def: false, desc: "If a search control is visible in the option list" },
      valuetype:         { type: String, def: "string", desc: "Type of value exptected and returned, one of boolean, string, number and date"  },
      valueField:        { type: String, def: "", desc: "Data field containing the value if using generated options" },
      textField:         { type: String, def: "", desc: "Text field containing the label if using generated options" },
      autovalidate:      { type: Boolean, def: false, desc: "If this control should run validation after each change" },
      comment:           { type: String, def: "", desc: "Any comment to display to the user" },
      errormessage:      { type: String, def: "", desc: "Any error message to display to the user if control is not valid" },
      checkdisabled:     { type: Boolean, def: false, desc: "If validation should be performed on a disabled or read only control" },
      nomessage:         { type: Boolean, def: false, desc: "Hide the comment and error message field" },
      noanimate:         { type: Boolean, def: false, desc: "Disable animation for this control" },
      fillheight:        { type: Boolean, reflect: true, def: false, desc: "Fill the parent container's height" },

      optionTemplate:    { type: Function, attribute: false, def: undefined, desc: "A generated option display label template builder function, called with the data record" },
      validator:         { type: Function, attribute: false, def: undefined, desc: "A custom funtion for testing validation, called with the typed value" },

      /* private or stateful */
      _values:           { type: Array, attribute: false },
      _message:          { type: String, attribute: false },
      _focused:          { type: Boolean, attribute: false },
      _hascomment:       { type: Boolean, attribute: false }

    } );
  }

  static get styles() {
    return super.styles.concat( [ selectStyles ] );
  }

  connectedCallback() {
    super.connectedCallback();
  }

  disconnectedCallback() {
    super.disconnectedCallback();
  }


  /******************
   * Render methods
   *****************/


  render() {
    // The slots and span must be on the same line to avoid space between the inline-block elements
    return this.builder`
      <div class="sc-select-list"
          ?required=${this.required}
          ?invalid=${this.invalid}
          ?fillheight=${this.fillheight}
          tabindex="${(this.search && this._focused) ? -1 : 0}"
          @_try-select-option=${this._trySelectOption}
          @_try-unselect-option=${this._tryUnselectOption}
          @_active-change=${this._activeChange}
          @focusin=${this._focusIn}
          @focusout=${this._focusOut}
          @keydown=${this._keyDown}
      >
        <div class="sc-select-list-control"
            ?disabled=${this.disabled}
            ?focused=${this._focused}
        >
          <label
            for=${this.name}
            class="sc-select-label"
            ?required=${this.required}
            ?disabled=${this.disabled}
            smalllabel
          >
            <slot name="label">
              <span>${this.label}</span>
            </slot>
          </label>
          <input type="hidden" name=${this.name}></input>
          <div class="sc-select-list-options-wrap">
            ${this.search? this.builder`
              <div class="sc-select-option-search">
                <input
                  type="search"
                  tabindex="-1"
                  placeholder="Search..."
                  @input=${this._filterOptions}
                  @keydown=${this._searchKeyDown}
                >
              </div>
            `: ''}
            ${this.optionsTemplate()}
          </div>
        </div>
        <div class="sc-select-message"
            ?hidden=${this.nomessage}
            ?noanimate=${this.noanimate}
            ?invalid=${this.invalid}
            ?comment=${this._hascomment}
        >
          <slot name="message">
            ${this._message}
          </slot>
        </div>
      </div>
    `;
  }


  optionsTemplate() {
    let data, valueField, textField;
    if ( this.modelName == undefined && this.data == undefined ) {
      return this.builder`
          <div class="sc-select-list-options">
            <slot></slot>
          </div>
      `
    }
    else {
      data = this.data || [];
      valueField = this.valueField;
      textField = this.textField || "";
      if ( valueField == undefined && this.dataModel != undefined )
        valueField = this.idField;
      return this.builder`
        <div class="sc-select-list-options">
          <slot></slot>
          ${this.optionTemplate != undefined
          ? data.map( row => this.builder`
              <sc-select-option
                  value=${row[valueField]}
                  label=${row[textField]}
              >${this.optionTemplate(row)}</sc-select-option>
            ` )
          : data.map( row => this.builder`
              <sc-select-option
                  value=${row[valueField]}
                  label=${row[textField]}
              ></sc-select-option>
            ` )
          }
        </div>
      `
    }
  }


  /*****************
   * Lit methods
   ****************/


  /* An update is happening, setup the component */
  willUpdate( changes ) {
    super.willUpdate( changes );

    if ( changes.has( 'comment' ) && this.comment != "" )
      this._hascomment = true

    // Validate
    if ( changes.has( '_values' ) && this.autovalidate && this.hasUpdated )
      this.validate();

    this._updateMessage();
  }


  async updated( changes ) {
    super.updated( changes );

    // Find selected option elements. Has to occur after rendering for bound controls
    if ( changes.has( 'data' ) || changes.has( 'value' ) || changes.has( '_values' ) )
      this._getSelected();

    // Notify listeners
    if ( changes.has( '_values' ) )
      this._selectionChange();

    // Update option selections
    if ( changes.has( 'data' ) || changes.has( 'value' ) || changes.has( '_values' ) )
       this._updateSelections();
  }


  async firstUpdated() {
    super.firstUpdated();

    // Setup default and initial values
    this._initialSelected();
  }


  /******************
   * Own methods
   *****************/

  /**
   * Get the current value(s)
   * @returns {String|Number|String[]|Number[]} - Selected value(s)
   */
  get value() {
    if ( this._values.length == 0 && this.emptyasnull )
      return null;
    if ( this.maxselect == 1 )
      return this.app.util.clone( this._values[ 0 ] );
    else
      return this.app.util.clone( this._values );
  }


  /**
   * Set the currently selected value or values
   * @param {String|Number|String[]|Number[]} vals - Selected value(s)
   */
  set value( vals ) {
    // Coerce to array
    if ( ! Array.isArray( vals ) )
      vals = [ vals ];

    // Convert to our type
    vals = this._typeValues( vals );

    if ( this.maxselect == 1 && vals.length > 1 )
      throw new Error( "Unable to set multiple values to single value control" );

    this._values = vals;
  }


  /**
   * Reset the value to a provided value or set from a default
   * @param {String|Number|String[]|Number[]} values - The selected values
   */
  setInitial( values ) {
    // Coerce to array
    if ( ! Array.isArray( values ) )
      values = [ values ];

    this._initSelected = values
    this.value = values;
  }


    /**
   * Reset the value to the initial value
   * @public
   * @param {Boolean} [toDefault=false] - Reset to the default value instead
   */
  reset( toDefault=false ) {
    if ( toDefault ) {
      this._values        = this._defaultSelected;
      this._initSelected  = this._defaultSelected;
    }
    else
      this._values        = this._initSelected;

    this.invalid    = false;
  }


  /**
   * Is the value of this component changed?
   * @public
   * @returns {Boolean}
   */
  changed() {
    if ( this._initSelected.length !== this._values.length )
      return true;

    for ( let val of this._values )
      if ( ! this._initSelected.includes( val ) )
        return true;

    return false;
  }


  /**
   * Validate this input as needed
   * @returns {Boolean} - If validation was successful
   * @public
   */
  validate() {
    let valid = true;

    if ( this.checkdisabled || ( ! this.disabled && ! this.readonly ) ) {
      if ( this.required && this._values.length == 0 )
        valid = false;
      if ( valid && this.validator != undefined && typeof this.validator == "function" )
        valid = this.validator( this.value );
    }

    this.invalid = ! valid;
    return valid;
  }


  /**
   * Coerce the value types as needed
   * @param {*} values    - Values to type-coerce
   * @returns             - Coerced values
   * @private
   */
  _typeValues( values ) {
    const converted = [];
    for ( let val of values ) {
      converted.push( this._typeValue( val ) );
    }

    // Set null if needed
    if ( converted.length == 1 && converted[0] === "" && this.emptyasnull )
      converted[ 0 ] = null;
    else if ( converted.length == 0 && this.emptyasnull )
      converted[ 0 ] = null;

    return converted;
  }


  /**
   * Type coerce a value
   * @param {*} value     - The value to be type coerced, usually a string
   * @returns {*}         - The type coerced value
   */
  _typeValue( value ) {
    const type = this.valuetype;

    // Nulls and empty strings are never type coerced
    if ( value === null || value === "" ) {
      return value;
    }

    // Undefined values are interpretted as empty strings
    if ( value === undefined ) {
      return "";
    }


    switch( type ) {
      case "boolean":
        return ( value ) ? true : false;
      case "string":
        if ( typeof value !== "string" && value.toString )
          return value.toString();
        else if ( typeof value !== "string" )
          return value + "";
        else
          return value;
      case "number":
        return ( typeof value == "number" ) ? value : parseFloat( value );
      case "date":
        return ( value instanceof Date ) ? value : new Date( value );
    }
  }


  /**
   * Get the currently selected options
   */
  _getSelected() {
    const options = this._options;
    const selections = this._values;
    let val;

    this._optionsmap = new Map();

    for ( let opt of options ) {
      val = this._typeValue( opt.value );
      if ( selections.includes( val ) )
        this._optionsmap.set( val, opt );
    }
  }


  /**
   * Get any initially selected elements, set our default value and current if not otherwise set
   */
  _initialSelected() {
    const options = this._options;
    const current = this._values;
    let selected = [];

    for ( let opt of options ) {
      if ( opt.selected )
        selected.push( opt );
    }

    if ( selected.length ) {
      this._defaultSelected = selected.map( s => this._typeValue( s.value ) );
      if ( ! current.length ) {
        this._initSelected = this._defaultSelected;
        this._values = this.app.util.clone( this._defaultSelected );
      }
      else
        this._initSelected = this.app.util.clone( current );
    }
  }


  /**
   * Set the selection state on options, and build the value
   */
  _updateSelections() {
    const options = this._options;
    const selected = Array.from( this._optionsmap.values() );

    for ( let opt of options ) {
      if ( selected.includes( opt ) )
        opt.selected = true;
      else
        opt.selected = false;
    }
  }


  /**
   * Get all list options
   * @protected
   */
  get _options() {
    const options = [];

    if ( this.shadowRoot.querySelector( ".sc-select-list-options" ) == null )
      return [];

    options.push(
      ...Array.from(
        this.shadowRoot.querySelector( ".sc-select-list-options slot" ).assignedNodes()
      ).filter( n => n.nodeType === Node.ELEMENT_NODE )
    );

    options.push(
      ...Array.from(
        this.shadowRoot.querySelector( ".sc-select-list-options" ).childNodes
      ).filter( n => n.nodeType === Node.ELEMENT_NODE && n.tagName !== "SLOT" )
    );

    return options;
  }


  /**
   * Handle a model status change
   * @protected
   */
  _statusChanged( type ) {
    if ( type == "bind" )
      this._dataChanged();
    else if ( type == "cleared" || type == "unbind" )
      this.data = [];
  }


  /**
   * Handle a model data change
   * @private
   * @async
   */
  async _dataChanged(){
    if ( ! this.dataModel )
      return;
    this.data = await this.dataModel.fetch();
  }


  /**
   * Handle a focus in event
   * @protected
   */
  _focusIn() {
    this._focused = true;
    if ( this.search )
      this.shadowRoot.querySelector( '.sc-select-option-search input' ).focus()
  }


  /**
   * Handle a focus out event
   * @protected
   */
  _focusOut() {
    this._focused = false;
    if ( this.autovalidate )
      this.validate();
  }


  /**
   * Update the displayed hint message
   * @protected
   */
  _updateMessage() {
    if ( this.invalid )
      this._message = this.errormessage;
    else
      this._message = this.comment;
  }


  /**
   * Handle key down in search input
   * @protected
   */
  _searchKeyDown(ev) {
    if ( ev.key == "ArrowDown" || ev.key == "ArrowUp" )
      ev.preventDefault();
  }


  /**
   * Handle a key down event in the list
   * @protected
   */
  _keyDown(ev) {
    if ( this.readonly || this.disabled )
      return;
    if ( this.search && this._open && ev.key == "ArrowRight" )
      this._selectDeselectOption( this._current, false );
    else if ( ! this.search && ev.key == " " ) {
      ev.preventDefault();
      this._selectDeselectOption( this._current, false );
    }
    else if ( this.search && ev.key == " " && ev.ctrlKey ) {
      ev.preventDefault();
      this._selectDeselectOption( this._current, false, false );
    }
    else if ( ev.key == "Enter" ) {
      ev.preventDefault();
      this._selectDeselectOption( this._current, true );
    }
    else if ( ev.key == "ArrowDown" ) {
      ev.preventDefault();
      this._moveDown();
    }
    else if ( ev.key == "ArrowUp" ) {
      ev.preventDefault();
      this._moveUp();
    }
  }


  /**
   * Handle a move up key action
   * @protected
   */
  _moveUp() {
    const visible = this._options.filter( o => o.visible );
    let next = visible.indexOf( this._current );
    if ( visible.length == 0 )
      return;
    if ( next == -1 || next == 0 || next == undefined )
      next = visible.length - 1;
    else
      next = next - 1;
    this._setActiveOption( next );
  }


  /**
   * Handle a move down key action
   * @protected
   */
  _moveDown() {
    const visible = this._options.filter( o => o.visible );
    let next = visible.indexOf( this._current );
    if ( visible.length == 0 )
      return;
    if ( next == undefined )
      next = 0;
    else
      next = next + 1;
    if ( next > ( visible.length - 1 ) )
      next = 0;
    this._setActiveOption( next );
  }


  /**
   * Toggle an option selection
   * @protected
   * @param {HTMLElement} option    - The option to select or de-select
   * @param {Boolean} onlyselect    - If this should select the item only, not de-select
   */
  _selectDeselectOption( option, onlyselect ) {
    let valueTyped, idx;
    valueTyped = this._typeValue( option.value );
    idx = this._values.indexOf( valueTyped );

    // Deselect or select
    if ( idx >= 0 ) {
      if ( ! onlyselect )
        this._values = this._values.filter( (_,i) => i !== idx ); // Don't mutate as lit won't notice
    }
    else {
      if ( this.maxselect == 1 )
        this._values = [ valueTyped ];
      if ( this.maxselect == 0 || this._values.length < this.maxselect )
        this._values = this._values.concat( [ valueTyped ] ); // Don't mutate as lit won't notice
    }
  }


  /**
   * The active option has changed, store current
   * @protected
   */
  _activeChange(ev) {
    if ( this._current && this._current !== ev.target )
      this._current.active = false;
    this._current = ev.target;
  }


  /**
   * Set active option by index
   * @protected
   */
  _setActiveOption( number ) {
    const visible = this._options.filter( o => o.visible );
    this._options.forEach( o => { if ( o.active ) o.active = false; } );

    // Out of bounds?
    if ( number < 0 || number >= visible.length ) {
      this._current = undefined;
      return;
    }

    visible[number].active = true;
    visible[number].scrollIntoView({ block: "nearest", inline: "nearest" });
    this._current = visible[number];
  }


  /**
   * Notify listeners that our selections have changed
   * @protected
   */
  _selectionChange() {
    this.dispatchEvent( new CustomEvent( onAfterChange, { bubbles: true, cancelable: false } ) );
  }


  /**
   * Select an option if it can be selected
   * @protected
   */
  _trySelectOption(ev) {
    const target = ev.target;
    const value = this._typeValue( target.value );
    let ok;

    if ( this.readonly || this.disabled )
      return;

    // Fire pre selection event
    ok = this.dispatchEvent( new CustomEvent( onBeforeSelect, {
      detail: { option: target, value: value, selecting: true },
      bubbles: true, cancelable: true
    } ) );
    if ( ok === false )
      return;

    // De-select other options
    if ( this.maxselect == 1 )
      this._values = [ value ];
    else {
      if ( this.maxselect == 0 || ( this.maxselect > 1 && this._values.length < this.maxselect ) )
        this._values = this._values.concat( [ value ] );
    }

    // After selection event
    this.dispatchEvent( new CustomEvent( onAfterSelect, {
      detail: { option: target, value: value, selecting: true },
      bubbles: true, cancelable: false
    } ) );
  }


  /**
   * Check if an option can be un-selected
   * @protected
   */
  _tryUnselectOption(ev) {
    const target = ev.target;
    const value = this._typeValue( target.value );
    let ok;

    if ( this.readonly || this.disabled )
      return;

    // Need one selected?
    if ( this.required && this._values.length <= 1 )
      return;

    ok = this.dispatchEvent( new CustomEvent( onBeforeSelect, {
      detail: { option: target, value: value, selecting: false },
      bubbles: true, cancelable: true
    } ) );
    if ( ok === false )
      return;

    this._values = this._values.filter( v => v !== value );

    // After selection event
    this.dispatchEvent( new CustomEvent( onAfterSelect, {
      detail: { option: target, value: value, selecting: false },
      bubbles: true, cancelable: false
    } ) );
  }


  /**
   * Filter the option pane
   * @protected
   */
  _filterOptions(ev) {
    const opts = this._options;
    const filter = ev.target.value;
    let visible = opts.filter( o => o.visible );
    let reg, activeIndex;

    /* clear filter */
    if ( filter == "" ) {
      opts.forEach( o => { o.visible = true; o.filter = "" } );
      activeIndex = this._options.findIndex( o => o.active == true );
      if ( activeIndex != -1 )
        this._setActiveOption( activeIndex );
      else
        this._current = undefined;
      this._filter = "";
      return;
    }

    reg = new RegExp( filter, "i" );

    /* non-consecutive filters */
    if ( ! filter.startsWith( this._filter ) )
      visible = opts;
    this._filter = filter;

    visible.forEach( option => {
      if ( ! option.label.match( reg ) ) {
        option.visible = false;
        option.filter = "";
      }
      else
        option.visible = true;
        option.filter = this._filter;
    } );
    visible = opts.filter( o => o.visible );

    // Nothing active yet?
    if ( ! this._current ) {
      if ( visible.length )
        this._current = visible[ 0 ];
      else
        return;
    }

    /* make nearest option active */
    activeIndex = visible.indexOf( this._current );
    if ( activeIndex == -1 ) {
      activeIndex = opts.indexOf( this._current );
      if ( activeIndex > visible.length )
        this._setActiveOption( visible.length - 1 );
      else
        this._setActiveOption( activeIndex );
    }
    else
      this._setActiveOption( activeIndex );
  }


  /**
   * Clear the current selection
   * @protected
   */
  _clearSelection() {
    const opts = this._options;
    for( let opt of opts ) {
      if ( opt.selected )
        opt.selected = false;
    }
  }

}


class SimpleSelectOption extends SimpleComponent {
  constructor() {
    super();
  }
  static get _elementName() { return 'sc-select-option' }
  static get properties() {
    return Object.assign( {}, super.properties, {
      value:          { type: String, def: undefined, desc: "The value for this option" },
      label:          { type: String, def: "", desc: "The text label for this option" },
      filter:         { type: String, def: "", desc: "The current text filter applied, used for augmenting display label" },
      selected:       { type: Boolean, reflect: true, def: false, desc: "If this option is selected" },
      active:         { type: Boolean, reflect: true, def: false, desc: "If this option is active or hovered" },
      visible:        { type: Boolean, reflect: true, def: true, desc: "If this option is currently visible" },
      generated:      { type: Boolean, def: false, desc: "If this option is a generated option"  }
    } );
  }

  static get styles() {
    return super.styles.concat( [ selectOptionStyles ] );
  }

  render() {
    return this.builder`
      <div class="sc-select-option"
          ?selected=${this.selected}
          ?active=${this.active}
          ?visible=${this.visible}
          @click=${this.toggleOption}
          @mouseover=${this._mouseActive}
          @mouseout=${this._mouseActive}
          @mousedown=${e=>{e.preventDefault()}}
        >
        <slot>
          <div class="sc-select-option-text">
            <span>
              ${this.filter == "" ? this.includeHtml(this.label) : this.includeHtml(this._displayValue)}
            </span>
          </div>
        </slot>
      </div>
    `;
  }


  get _displayValue() {
    const regex = new RegExp( '(' + this.filter + ')', "ig" );
    const val = this.label.replace(regex, '<b>$1</b>');
    return val;
  }


  _mouseActive( ev ) {
    if ( ev.type == "mouseover" ) {
      this.active = true;
      this.dispatchEvent( new CustomEvent( '_active-change', { bubbles: true, cancelable: false, composed: true } ) );
    }
    else if ( ev.type == "mouseout" )
      this.active = false;
  }


  /**
   * Request to select or deselect this option
   */
  toggleOption(ev) {
    let ourev, eventName;

    ev.preventDefault();

    if ( this.selected )
      eventName = "_try-unselect-option";
    else
      eventName = "_try-select-option";

    // Request (de)selection permission
    ourev = new CustomEvent( eventName, { bubbles: true, cancelable: true, composed: true } );
    this.dispatchEvent( ourev );
  }

}



export { SimpleSelect, SimpleSelectList, SimpleSelectOption };


