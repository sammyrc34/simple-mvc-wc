export class SimpleBusyMask extends SimpleComponent {
    static get _elementName(): string;
    busy: boolean;
    fill: boolean;
    scroll: boolean;
    noflex: boolean;
    render(): import("lit-html").TemplateResult<1>;
}
import { SimpleComponent } from '../index.js';
