
import { SimpleComponent, SimpleButton } from '../index.js';
SimpleButton.register();

import { library as faLib, icon as faIcon } from "@fortawesome/fontawesome-svg-core";
import { faWindowClose } from "@fortawesome/free-solid-svg-icons";

import popupStyles from './popup.css';



faLib.add( faWindowClose );


/**
 * A cancelable event emitted before popup closure
 * Cancel to prevent closing the popup
 * @event SimplePopup#close
 * @event SimpleConfirm#close
 */
const onClose = "close";


/**
 * A cancelable event emitted before the popup opens
 * Cancel to prevent opening the popup
 * @event SimplePopup#open
 * @event SimpleConfirm#open
 */
const onOpen = "open";


/**
 * An event emitted when the positive action is triggered
 * @event SimpleConfirm#positive
 */
const onPositive = "positive";


/**
 * An event emitted when the negative action is triggered
 * @event SimpleConfirm#positive
 */
const onNegative = "negative";



/**
 * A popup window for user interaction
 * @class
 */
export class SimplePopup extends SimpleComponent {


  /**
   * @emits {open} As the popup is being opened, cancelable
   * @emits {close} As the popup is being hidden, cancelable
   */


  constructor() {
    super();

    // Private and or stateful
    this._lastx = undefined;
    this._lasty = undefined;
    this._offsetx = 0;
    this._offsety = 0;
    this._currentx = 0;
    this._currenty = 0;
    this._zindex = 0;
  }

  static get _elementName() { return 'sc-popup' }

  static get properties() {
    return Object.assign( {}, super.properties, {

      width:          { type: Number, def: 0, desc: "A desired width for this popup, 0 meaning automatic" },
      open:           { type: Boolean, def: false, desc: "If this popup is currently open" },
      modal:          { type: Boolean, def: false, desc: "If this popup prevents interaction with elements behind the popup" },
      noheader:       { type: Boolean, def: false, desc: "Hide the popup header including close button" },
      noclose:        { type: Boolean, def: false, desc: "Hide the popup close button" },
      draggable:      { type: Boolean, def: false, desc: "If this popup can be moved by the user" },
      position:       { type: String, def: "center", desc: "Initial position for this popup, from center,topleft, topmiddle, topright, middleleft, middleright, bottomleft, bottommiddle and bottomright" },
      autoclose:      { type: Boolean, def: false, desc: "If this popup should automatically close upon a click outside" },
      noanimate:      { type: Boolean, def: false, desc: "If animation should be disabled on this popup" },

      _dragging:       { type: Boolean, def: false },
    } );
  }
  static get styles() {
    return super.styles.concat( [ popupStyles ] );
  }

  renderPopup( header, body, footer, close ) {
    return this.builder`
      <div
        class="sc-popup-mask"
        ?modal=${this.modal}
        ?autoclose=${this.autoclose}
        ?noanimate=${this.noanimate}
        ?open=${this.open}
        @mouseup=${this._moveEnd}
        @mousemove=${this._move}
        @click=${this._autohide}
      ></div>
      <div class="sc-popup-area">
        <div class="sc-popup"
          ?open=${this.open}
          ?dragging=${this._dragging}
          ?noanimate=${this.noanimate}
          ?noheader=${this.noheader}
          ?noclose=${this.noclose}
          @mousemove=${this._move}
          @mouseup=${this._moveEnd}
          @animationend=${this._updateDisplay}
        >
          <div class="sc-popup-content" >
            <div class="sc-popup-header" @mousedown=${this._moveStart}>
              <slot name="close">
                ${close}
              </slot>
              <slot name="header">
                ${header}
              </slot>
            </div>
            <div class="sc-popup-body">
              <slot name="body">
                ${body}
              </slot>
            </div>
            <div class="sc-popup-footer">
              <slot name="footer">
                ${footer}
              </slot>
            </div>
          </div>
        </div>
      </div>
    `;

  }

  render() {
    const defheader = this.builder`
      <div>Popup</div>
    `;

    const defbody = this.builder`
    `;

    const deffooter = this.builder`
    `;

    const defclose = this.builder`
      <div class="sc-popup-close" @click=${this.hide}>
        ${this.includeSvg( faIcon( faWindowClose ).html[0] )}
      </div>
    `

    return this.renderPopup( defheader, defbody, deffooter, defclose );
  }

  connectedCallback() {
    super.connectedCallback();
    window.addEventListener( 'resize', (_0) => this._updatePosition() )
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    window.addEventListener( 'resize', (_0) => this._updatePosition() )
  }

  firstUpdated( bits ) {
    super.firstUpdated( bits );
    const body = this.shadowRoot.querySelector( '.sc-popup-content' );
    if ( this.width > 0 )
      body.style.width = this.width + 'px';
    this._updateDisplay();
  }

  updated( changes ) {
    super.updated( changes );
    if ( changes.has( 'open' ) && this.open == true ) {
      if ( this.modal )
        this._disableBodyScroll();
      this._updateDisplay();
      this._updatePosition();
    }
    if ( changes.has( 'open' ) && this.open == false ) {
      if ( this.modal )
        this._enableBodyScroll();
    }
  }


  /***************
   * Our methods
   **************/


  /**
   * Reveal this popup
   * @public
   */
  show() {
    var cev, zindex;
    if ( ! this.open ) {

      cev = new CustomEvent( onOpen, { bubbles: true, cancelable: true, composed: true } );
      if ( ! this.dispatchEvent( cev ) ) {
        return;
      }

      zindex = this.app.getTopZ( 0 );
      if ( zindex !== this._zindex )
        this._zindex = this.app.getTopZ( 3 );

      this.shadowRoot.querySelector( '.sc-popup-mask' ).style.setProperty( 'z-index', ( this._zindex - 2 ) );
      this.shadowRoot.querySelector( '.sc-popup-area' ).style.setProperty( 'z-index', ( this._zindex - 1 ) );
      this.shadowRoot.querySelector( '.sc-popup' ).style.setProperty( 'z-index', this._zindex );
      this.open = true;
      return;
    }
  }


  /**
   * Hide this popup
   * @public
   */
  hide() {
    var cev;
    if ( ! this.open )
      return;

    cev = new CustomEvent( onClose, { bubbles: true, cancelable: true, composed: true } );
    if ( ! this.dispatchEvent( cev ) ) {
      return;
    }

    this.open = false;
  }


  /**
   * Disable the body from scrolling
   * @private
   */
  _disableBodyScroll() {
    var body = document.body;
    body.style[ 'height' ] = "100vh";
    body.style[ 'width' ] = "100vw";
    body.style[ 'overflow-y' ] = "hidden";
    body.style[ 'margin-right' ] = '15px';
  }


  /**
   * Enable body scrolling again
   * @private
   */
  _enableBodyScroll() {
    var body = document.body;
    body.style.removeProperty( 'height' );
    body.style.removeProperty( 'width' );
    body.style.removeProperty( 'overflow-y' );
    body.style.removeProperty( 'margin-right' );
  }


  /**
   * Update the position of this popup
   * @private
   */
  _updatePosition() {
    const content = this.shadowRoot.querySelector( '.sc-popup-content' );
    const contentBounds = content.getBoundingClientRect();
    const vpwidth = window.innerWidth + window.scrollY;
    const vpheight = window.innerHeight + window.scrollX;
    const sizex = parseInt( contentBounds.width );
    const sizey = parseInt( contentBounds.height );
    const factor = 7;
    let wantx, wanty = 0;

    switch( this.position ) {
      case "center":
        wantx = ( vpwidth / 2 ) - ( sizex / 2 );
        wanty = ( vpheight / 2 ) - ( sizey / 2 );
        break;
      case "topleft":
        wantx = ( vpwidth / 100 ) * factor;
        wanty = ( vpheight / 100 ) * factor;
        break;
      case "topmiddle":
        wantx = ( vpwidth / 2 ) - ( sizex / 2 );
        wanty = ( vpheight / 100 ) * factor;
        break;
      case "topright":
        wantx = vpwidth - sizex - ( ( vpwidth / 100 ) * factor );
        wanty = ( vpheight / 100 ) * factor;
        break;
      case "middleleft":
        wantx = ( vpwidth / 100 ) * factor;
        wanty = ( vpheight / 2 ) - ( sizey / 2 );
        break;
      case "middleright":
        wantx = vpwidth - sizex - ( ( vpwidth / 100 ) * factor );
        wanty = ( vpheight / 2 ) - ( sizey / 2 );
        break;
      case "bottomleft":
        wantx = ( vpwidth / 100 ) * factor;
        wanty = vpheight - sizey - ( ( vpheight / 100 ) * factor );
        break;
      case "bottommiddle":
        wantx = ( vpwidth / 2 ) - ( sizex / 2 );
        wanty = vpheight - sizey - ( ( vpheight / 100 ) * factor );
        break;
      case "bottomright":
        wantx = vpwidth - sizex - ( ( vpwidth / 100 ) * factor );
        wanty = vpheight - sizey - ( ( vpheight / 100 ) * factor );
        break;
    }
    if ( wantx < 0 )
      wantx = 0;
    if ( wanty < 0 )
      wanty = 0;

    this._currentx = Math.floor( wantx );
    this._currenty = Math.floor( wanty );

    // Translate from our current position
    content.style.transform = "translate(" + this._currentx + "px," + this._currenty + "px )";
  }


  /**
   * Handle a drag start
   * @private
   */
  _moveStart( ev ) {
    if ( ! this.draggable )
      return;
    if ( ! Array.from( this.children ).find( e => e.slot == "header" ).contains( ev.target ) )
      return;
    this._lastx = ev.clientX;
    this._lasty = ev.clientY;
    this._dragging = true;
  }


  /**
   * Handle a drag end
   * @private
   */
  _moveEnd() {
    this._dragging = false;
  }



  /**
   * Move this popup if being dragged
   * @private
   */
  _move( ev ) {
    let content;
    if ( this._dragging ) {
      ev.preventDefault();
      this._offsetx = ev.clientX - this._lastx;
      this._offsety = ev.clientY - this._lasty;
      this._lastx = ev.clientX;
      this._lasty = ev.clientY;
      this._currentx = this._currentx + this._offsetx;
      this._currenty = this._currenty + this._offsety;
      content = this.shadowRoot.querySelector( '.sc-popup-content' );
      content.style.transform = "translate(" + this._currentx + "px," + this._currenty + "px )";
    }
  }


  /**
   * Update display after or before animation
   */
  _updateDisplay() {
    const popup = this.shadowRoot.querySelector( '.sc-popup' );
    if ( ! this.open )
      popup.style.display = 'none';
    else
      popup.style.display = 'block';
  }


  /**
   * Hide this popup if we can
   * @private
   */
  _autohide() {
    if ( this.autoclose )
      this.hide();
  }
}



/**
 * Confirmation type popup with a consistent style
 * @class
 */
export class SimpleConfirm extends SimplePopup {


  /**
   * @emits SimpleConfirm#positive
   * @emits SimpleConfirm#negative
   */


  constructor() {
    super();

    // Stateful and private
    this._promRes         = undefined;
    this._promRej         = undefined;
    this._prom            = undefined;
  }

  static get _elementName() { return 'sc-confirm' }

  static get properties() {
    return Object.assign( {}, super.properties, {

      message:        { type: String, def: "", desc: "The message to provide to the user" },
      modal:          { type: Boolean, def: true, desc: "If this prompt prevents interaction with the page behind" },
      width:          { type: Number, def: 300, desc: "A desired width for this popup, 0 meaning automatic" },
      buttons:        { type: String, def: "ok_cancel", desc: "Which buttons should be available for interaction, from ok_cancel, yes_no and continue_cancel" },

      _noLabel:       { type: String, def: "" },
      _yesLabel:      { type: String, def: "" }
    } );
  }

  static get styles() {
    return super.styles.concat( SimpleButton.styles );
  }

  render() {

    const header = this.builder`
      <h3 style="margin-bottom: 5px; margin-top: 5px;">Confirm</h3>
    `;

    const body = this.builder`
      <span>${this.message}</span>
    `;

    const footer = this.builder`
      <sc-button label="${this._noLabel}" hover outline @click=${this._fireNo}></sc-button>
      <sc-button label="${this._yesLabel}" hover @click=${this._fireYes}></sc-button>
    `;

    const close = this.builder``;

    return this.renderPopup( header, body, footer, close );
  }

  willUpdate( changes ) {
    super.willUpdate( changes );
    if ( changes.has( 'buttons' ) ) {
      switch ( this.buttons ) {
        case "ok_cancel":
          this._yesLabel = "OK";
          this._noLabel = "Cancel";
          break;
        case "yes_no":
          this._yesLabel = "Yes";
          this._noLabel = "No";
          break;
        case "continue_cancel":
          this._yesLabel = "Continue";
          this._noLabel = "Cancel";
          break;
      }
    }
  }


  /**
   * Adopt styles into the buttons elements too
   */
  adoptCustomStyles( styles ) {
    const buttons = this.shadowRoot.querySelectorAll( 'sc-button' );
    buttons.forEach( b => b.adoptCustomStyles( styles ) );
    super.adoptCustomStyles( styles );
  }


  /***************
   * Our methods
   **************/

  /**
   * Show and wait for a response
   * @async
   * @public
   * @param {Boolean} [shouldThrow=false]   - If the negative response should reject instead of resolve
   * @returns {Promise<Boolean>} promise    - Promise resolving to true or false indicating yes/no
   */
  async promptWait( shouldThrow=false ) {
    if ( this._prom )
      return this._prom;
    let promise = new Promise( ( resolve, reject ) => {
      this._promRes = resolve;
      this._promRej = shouldThrow ? reject : resolve;
      this.show();
    } );
    this._prom = promise;
    return this._prom;
  }


  /**
   * Handle a negative user response
   * @private
   */
  _fireNo() {
    let negev = new CustomEvent( onNegative, { bubbles: false, cancelable: false, composed: true } );
    this.dispatchEvent( negev );
    if ( this._promRej )
      this._promRej( false );
    this._promRej = undefined;
    this._promRes = undefined;
    this._prom = undefined;
    this.open = false;
  }


  /**
   * Handle a positive user response
   * @private
   */
  _fireYes() {
    let posev = new CustomEvent( onPositive, { bubbles: false, cancelable: false, composed: true } );
    this.dispatchEvent( posev );
    if ( this._promRes )
      this._promRes( true );
    this._promRej = undefined;
    this._promRes = undefined;
    this._prom = undefined;
    this.open = false;
  }

}



