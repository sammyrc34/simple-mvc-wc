
import { SimpleComponent } from '../index.js';
import Util from '../util.js';

import DayJS from 'dayjs';
import CustomParseFormat from 'dayjs/plugin/customParseFormat.js';


import { library as faLib, icon as faIcon } from "@fortawesome/fontawesome-svg-core";
import { faCalendarAlt } from "@fortawesome/free-regular-svg-icons";


import inputStyles from './inputs.css';


const typeMap = {
  'datetime': 'text',
};


faLib.add( faCalendarAlt );

DayJS.extend( CustomParseFormat );


/**
 * Event when a search is attempted
 * @event SimpleInput#search
 */
const onSearch = 'search';


/**
 * Event after user input
 * @event SimpleInput#after-input
 */
const onAfterInput = 'after-input';


/**
 * Event after a change is made to the value
 * @event SimpleInput#after-change
 */
const onAfterChange = 'after-change';


/**
 * Simple Input component
 * Includes checkboxes, toggles, radio buttons and all forms of text inputs
 */
class SimpleInput extends SimpleComponent {

  /**
   * @emits {after-input} After keyboard input
   * @emits {after-change} After a change is made
   * @emits {search} When a search is attempted
   */

  constructor() {
    super();

    // The expected value type to get/set, string, number, date, boolean, format-short, format-long, format-full
    // For format-* properties, '____-____':
    // aa-12      (short)
    // aa  -  12  (long)
    // aa__-__12  (full)
    // Private
    //
    this._initValue       = "";
    this._defaultValue    = "";
    this._smalllabel      = false;
    this._focused         = false;
    this._hascomment      = false;
    this._pristine        = true;
    this._type            = undefined;
    this._formatpattern   = undefined;
    this._formatgroups    = undefined;
    this._lastvalue       = "";
    this._pickerType      = undefined;
    
  }

  static get _elementName() { return 'sc-input' }

  static get properties() {
    return Object.assign( {}, super.properties, {

      // Essential properties
      name:          { type: String, reflect: true, def: "",      
        desc: "Name for the input when submitting within a form, overridden by the field property" },
      label:         { type: String, reflect: true, def: "",      
        desc: "Text label for the input" },
      type:          { type: String, reflect: true, def: "text",  
        desc: "Type of the control, one of text, hidden, number, email, tel, password, url, search and datetime for text style controls, or one of toggle, checkbox and radio for toggled type controls" },

      _value:        { type: String, attribute: "value", def: "", desc: "Initial untyped value for this input" }, 

      // Common optional properties
      disabled:      { type: Boolean, reflect: true, def: false, desc: "If this input is disabled" },
      readonly:      { type: Boolean, reflect: true, def: false, desc: "If this input is read only to the user" },
      required:      { type: Boolean, reflect: true, def: false, desc: "If this input is required" },
      field:         { type: String, reflect: true, def: undefined, desc: "The field name this input is for if part of a form" },
      valuetype:     { type: String, def: "string", desc: "Type of value returned, one of string, boolean, number, date, format-short, format-long or format-full" },
      

      // Text type properties
      noicon:        { type: Boolean, def: false, desc: "If any default picker icon should be hidden" },
      smalllabel:    { type: Boolean, def: false, desc: "If the text type input should only display a small label" },
      placeholder:   { type: String, def: "", desc: "A placeholder value for text type inputs" },
      minlength:     { type: Number, def: undefined, desc: "Minimum value length for text type inputs" },
      maxlength:     { type: Number, def: undefined, desc: "Maximum value length for text type inputs" },
      pattern:       { type: String, def: undefined, desc: "Regular expression for input validation of text type inputs" },
      format:        { type: String, def: "", desc: "A format mask to present to the user, see below" },
      formatchars:   { type: String, def: "_", desc: "Characters of the format mask for user input, see below" },

      // Number type properties
      min:           { type: Number, def: undefined, desc: "A minimum value for number type inputs" },
      max:           { type: Number, def: undefined, desc: "A maximum value for number type inputs" },
      step:          { type: Number, def: undefined, desc: "The granularity that the value is increased or decreased"  },

      // Toggle/checkbox type properties
      defaulttrue:   { type: String, def: "1", desc: "The default true value for toggle type inputs" },
      falsevalue:    { type: String, def: "0", desc: "The default false value for toggle type inputs" },
      unsetvalue:    { type: String, def: null, desc: "The default unset value for tri-state toggle inputs" },
      checked:       { type: Boolean, def: false, desc: "The checked state of toggle type inputs" },
      unset:         { type: Boolean, def: false, desc: "The unset state of tri-state toggle type inputs" },
      position:      { type: String, def: "right", desc: "Label position for toggle type inputs, either left or right" },
      grouped:       { type: Boolean, def: false, desc: "If this toggle type input is in a group (with a shared name)" },
      tristate:      { type: Boolean, def: false, desc: "If this toggle type input is a tri-state control" },

      invalid:       { type: Boolean, def: false, desc: "If this input is currently invalid" },
      emptyasnull:   { type: Boolean, def: false, desc: "If an empty value of a text type input returns null" },
      comment:       { type: String, def: "", desc: "A helpful comment displayed under the input" },
      autocomplete:  { type: String, def: undefined, desc: "The type of autocomplete this input uses, per vanilla input autocomplete attribute" },
      noanimate:     { type: Boolean, def: false, desc: "If this input should not animate" },
      errormessage:  { type: String, def: "", desc: "An error message displayed if the input is invalid" },
      nomessage:     { type: Boolean, def: false, desc: "Hide the comment and error message field" },
      autovalidate:  { type: Boolean, def: false, desc: "If this input should autovalidate after input" },
      validator:     { type: Function, attribute: false, def: undefined, desc: "Optional function to validate user input" },
      checkdisabled: { type: Boolean, def: false, desc: "Validate readonly and disabled input" },
      entersubmit:   { type: Boolean, def: false, desc: "If a carriage-return will try to submit a parent form" },

      // Private
      _smalllabel:   { type: Boolean, attribute: false },
      _focused:      { type: Boolean, attribute: false },
      _message:      { type: String, attribute: false },
      _hascomment:   { type: Boolean, attribute: false },
      _type:         { type: String, attribute: false }
    } );
  }
  
  static get styles() {
    return super.styles.concat( [ inputStyles ] );
  }


  /***********************
   * Lit methods
   **********************/

  willUpdate( changes ) {
    super.willUpdate( changes );
    if ( ! this.hasUpdated || changes.has( 'smalllabel' ) )
      this._smalllabel = this.smalllabel;
    if ( this.placeholder != "" || this._value != "" )
      this._smalllabel = true;
    if ( ! this._pristine && this.autovalidate && changes.has( '_value' ) )
      this.validate();
    if ( changes.has( 'comment' ) && this.comment != "" )
      this._hascomment = true
    if ( changes.has( 'type' ) ) {
      if ( typeMap.hasOwnProperty( this.type ) )
        this._type = typeMap[ this.type ];
      else
        this._type = this.type;
    }
    if ( changes.has( 'tristate' ) && this.tristate && this.grouped ) {
      this.app.log( "error", "Grouped toggle type input may not be tri-state" );
      this.tristate = false;
    }
    if ( this.type == "datetime" && this.formatchars == "_" )
      this.formatchars = "YMDShmdsZAa"; 
    if ( this.type == "datetime" && changes.has( '_value' ) && this._value && this._value instanceof Date == false )
      this._value = this._typedValue( this._value, 'date' );
    if ( changes.has( 'format' ) ) {
      this._setupFormatMask();
      this._datetimeType();
    }
    if ( changes.has( '_value' ) )
      this._updateInput();
    this._updateLabel();
    this._updateMessage();
  }



  updated( changes ) {
    if ( this.type == "checkbox" || this.type == "toggle" || this.type == "radio" ) {
      if ( changes.has( 'checked' ) || changes.has( 'unset' ) )
        this._updateInput();
    }
  }


  firstUpdated() {
    super.firstUpdated();

    /**
     * FIXME: Chrome bug
     * Chrome has several long standing bugs when it comes to autofilling 
     * inputs which sit inside shadow DOMs. Rather than lose the benefits
     * of shadowDOM for the temporary bug, hoist the input into lightdom 
     * for now. 
     */
    const input = this.renderRoot.querySelector( 'input:not(.hidden)' );
    if ( input && input.hasAttribute( 'slot' ) ) {
      this.append( input );
    }

    /**
     * FIXME: Chrome bug
     * Then detect when chrome autofills the field on initialisation with a timer
     */
    if ( this.autocomplete !== "" ) { 
      setTimeout( () => { 
        if ( this.querySelector( 'input:-webkit-autofill' ) !== null )
          this._smalllabel = true;
      }, 500 );
    }

    // Set initial and default values as needed
    if ( this.type == "checkbox" || this.type == "toggle" || this.type == "radio" ) {
      this._initValue       = this._value;
      this._defaultValue    = this._value;
      this._updateChecked();
    }
    else if ( this._initValue === "" ) {
      this._initValue       = this._value;
      this._defaultValue    = this._value;
    }

    this._updateInput();
    this._updateLabel();
  }


  render() {
    return this.builder`
      <div class="sc-input" 
          type="${this.type}" 
          ?disabled=${this.disabled} 
          ?readonly=${this.readonly}
          ?invalid=${this.invalid}
      >
        ${this._inputTemplate()}
      </div>
    `;
  }


  /***********************
   * Element methods
   **********************/

  /**
   * Get the element value
   * @public
   * @returns {*} - A typed value per valuetype and emptyasnull attributes
   */
  get value() {
    switch (this.type) {
      case "search":
      case "datetime":
      case "number":
      case "text":
      case "email":
      case "tel":
      case "password":
      case "url":
      case "hidden":
        return this._typedValue( this._value );
      case "toggle":
      case "checkbox":
      case "radio":
        if ( this.grouped )
          return this._typedValue( this._groupedValue() );
        else {
          if ( this.unset && this.tristate )
            return this._typedValue( this.unsetvalue );
          else if ( this.checked )
            return this._typedValue( this._truevalue );
          else
            return this._typedValue( this.falsevalue );
        }
    }
  }


  /**
   * Set the elements value
   * @public
   * @param {*} newval - The value for the element
   */
  set value( newval ) {
    switch( this.type ) {
      case "datetime":
        // Date types always store dates
        this._value = this._typedValue( newval, 'date' );
        break;
      case "search":
      case "number":
      case "text":
      case "email":
      case "tel":
      case "password":
      case "hidden":
      case "url":
        if ( this.format )
          this._value = this._typedValue( newval, 'format-full' );
        else
          this._value = newval;
        break;
      case "toggle":
      case "checkbox":
      case "radio":
        this._value = newval
        this._updateChecked();
        break;
    }
  }


  /**
   * Reset the elements value to last initial value, or default value
   * @public
   * @param {Boolean} toDefault - If the element should be reset to defaults instead of initial value
   */
  reset( toDefault=false ) {
    if ( toDefault ) {
      this.value        = this._defaultValue;
      this._initValue   = this._defaultValue;
    }
    else
      this.value        = this._initValue;
    
    this.invalid    = false;
    this._pristine  = true;
  }


  /**
   * Has this elements value changed?
   * @public
   * @returns {Boolean} - If this element has changed
   */
  changed() {
    const input = this.renderRoot.querySelector( 'input:not(.hidden)' ) || this.querySelector( 'input' );
    let tmp;

    switch( this.type ) {
      case "datetime":
        tmp = this._typedValue( input.value, 'date' );
        if ( tmp && this._initValue && tmp.getTime() !== this._initValue.getTime() )
          return true;
        else if ( ! this.emptyasnull && this._initValue && tmp && this._initValue.getTime() !== tmp.getTime() )
          return true;
        else
          return false;
      case "search":
      case "text":
      case "email":
      case "tel":
      case "password":
      case "url":
      case "number":
      case "hidden":
        if ( this._value !== this._initValue )
          return true;
        if ( ! this.emptyasnull && this._initValue !== "" && this._initValue !== this._typedValue( input.value ) )
          return true;
        else
          return false;
      case "toggle":
      case "checkbox":
      case "radio":
        return this.value !== this._initValue;
    }
  }


  /**
   * Set this elements value to an initial value
   * @public
   * @param {*} value - The initial value to set
   */
  setInitial( value ) {
    this.value = value;
    this._initValue = this._value;
  }


  /**
   * Focus this element
   */
  focus() {
    const input = this.renderRoot.querySelector( 'input:not(.hidden)' ) || this.querySelector( 'input' );
    if ( input )
      input.focus();
  }


  /** 
   * Validate the input as needed
   * @public
   * @returns {Boolean} - If element value is valid
   */
  validate() {
    const input = this.renderRoot.querySelector( 'input:not(.hidden)' ) || this.querySelector( 'input' );
    let val, valid = true;

    // Only validate after render
    if ( ! this.hasUpdated )
      return;

    if ( this.checkdisabled || ( ! this.disabled && ! this.readonly ) ) {

      // Type specific validation
      if ( this.type == "toggle" || this.type == "checkbox" || this.type == "radio" )
        return this._validateToggle();
      if ( this.type == "datetime" )
        return this._validateDatetime();

      // Empty required things
      if ( valid && this.required && input.value === "" )
          valid = false;

      // Things with a pattern
      if ( valid && ! this.format && ! input.checkValidity() )
        valid = false;
      else if ( valid && this.pattern && this.format ) {
        val = this._format( input.value, 'full', 'short' );
        if ( input.value !== this.format && input.value !== "" && ! new RegExp( this.pattern ).test( val ) )
          valid = false;
      }

      // Custom validator function
      if ( valid && this.validator )
        valid = this.validator( this.value );
    }

    this.invalid = ! valid;
    this._updateMessage();

    return valid;
  }


  /**
   * Validate a datetime input
   */
  _validateDatetime() {
    const input = this.renderRoot.querySelector( 'input:not(.hidden)' ) || this.querySelector( 'input' );
    let val, valid = true;

    // Empty required things
    if ( valid && this.required && input.value === "" )
      valid = false;
    if ( valid && this.required && input.value === this.format )
      valid = false;

    // Check via coerced date
    val = this.value;
    if ( valid && this.required && val && ! isNaN( val.getTime() ) )
      valid = DayJS( val ).isValid();

    // Custom validator function
    if ( valid && this.validator )
      valid = this.validator( this.value );

    return valid;
  }



  /**
   * Get a type coerced value. Input could be many types.
   * @private
   * @param {*} valueAny        - Value to convert into required type
   * @param {String} [type]     - Specific type to return, otherwise element value type
   * @param {Boolean} [nullish] - If falsy values ("", undefined or NaN) yield null. Default is this.emptyasnull
   */
  _typedValue( valueAny, type, nullish ) {
    let valueString;

    if ( ! type )
      type = this.valuetype
    if ( nullish === undefined )
      nullish = this.emptyasnull;

    // Make a string coerced value
    if ( typeof valueAny !== "string" )
      valueString = valueAny + "";
    else
      valueString = valueAny;

    // Return null if needed
    if ( nullish && ( valueString === "" || Number.isNaN( valueAny ) ) || valueAny === null || valueAny === undefined )
      return null;

    switch( type ) {
      case "boolean": 
        return valueAny === "1" || valueAny === true || valueAny === 1 || valueAny === "true";

      case "string": // String from a ...
        if ( valueString !== undefined ) // string
          return valueString + "";
        if ( valueAny instanceof Date ) { // dates
          if ( this.format )
            return DayJS( valueAny ).format( this.format ); // formatted
          else
            return valueAny.toISOString(); // date
        }
        if ( typeof valueAny == "number" ) // number
          return valueAny + "";

      case "number": // Number from a ...
        if ( valueAny instanceof Date ) // date
          return valueAny.getTime();
        if ( typeof valueAny == "number" ) // number
          return valueAny + 0;
        else // string (inc mask)
          return parseFloat( valueString );

      case "date": // Date from a....
        if ( valueAny instanceof Date ) // date
          return new Date( valueAny.getTime() );
        if ( typeof valueAny == "number" ) // Number
          return new Date( valueAny );
        if ( this.format && typeof valueAny == "string" ) // Mask
          return DayJS( valueAny, this.format ).toDate();
        if ( this.valuetype == "number" && ! isNaN( valueString ) )
          return new Date( parseInt( valueString ) );
        else // String
          return new Date( valueString );

      case "format-short":
      case "format-long":
      case "format-full":
        if ( ! this.format )
          return undefined;

        if ( type == "format-long" )
          return this._format( valueAny, 'any', 'long' );
        if ( type == "format-short" )
          return this._format( valueAny, 'any', 'short' );
        if ( type == "format-full" )
          return this._format( valueAny, 'any', 'full' );
    }
  }


  /**
   * Get the value of a grouped element
   * @private
   */
  _groupedValue() {
    let elements, val = "";
    let form = this.closest( 'form' );
    if ( form == null )
      form = document.body;
    elements = Array.from( form.querySelectorAll( 'sc-input[name=' + this.name +  ']' ) );
    elements.forEach( elem => {
      if ( elem.checked )
        val = elem._value;
    } );
    return val;
  }


  /**
   * Get the elements truthy value
   * @private
   */
  get _truevalue() {
    if ( this._value !== "" )
      return this._value;
    else
      return this.defaulttrue;
  }



  /**
   * Configure the element for any format mask
   */
  _setupFormatMask() {
    const mask = this.format;
    const maskchars = Array.from( this.formatchars );
    let groupstart;
    if ( ! mask )
      return;

    // Build progressive input mask for any autovalidation
    if ( this.pattern )
      this._formatpattern = Util.partialRegex( this.pattern );
    else
      this._formatpattern = undefined;

    if ( ! this.placeholder )
      this.placeholder = mask + "";

    // Build format groups
    this._formatgroups = new Map();
    for ( let idx in mask ) {
      if ( groupstart === undefined && maskchars.includes( mask[ idx ] ) ) {
        groupstart = parseInt( idx );
      }
      else if ( groupstart !== undefined && ! maskchars.includes( mask[ idx ] ) ) {
        this._formatgroups.set( groupstart, parseInt( idx ) - 1 );
        groupstart = undefined;
      }
      else if ( groupstart !== undefined && parseInt( idx ) == ( mask.length - 1 ) ) {
        this._formatgroups.set( groupstart, parseInt( idx ) );
        groupstart = undefined;
      }

    }
  }


  /**
   * Update the visible or proxy input elements value
   * @private
   * @param {Boolean} withMask  - Return a mask if empty
   */
  _updateInput( withMask=false ) {
    let input, hiddeninput, value;

    // Skip if not rendered
    if ( ! this.hasUpdated )
      return;

    // Toggle type controls are different
    if ( this.type == "checkbox" || this.type == "toggle" || this.type == "radio" )
      return this._updateToggle();

    input = this.renderRoot.querySelector( 'input:not(.hidden)' ) || this.querySelector( 'input' );
    value = this._value;

    // Handle nullish object values
    if ( value === null || value === undefined || value === "" )
      value = "";

    // Handle invalid date... set empty
    if ( this._value instanceof Date && isNaN( this._value.getTime() ) ) {
      if ( withMask )
        return input.value = this.format;
      else
        return input.value = "";
    }

    switch( this.type ) {
      case "datetime":
        hiddeninput = this.shadowRoot.querySelector( 'input.hidden' );
        if ( this.format ) {
          value = this._formatDate( value );
          if ( value == this.format && ! withMask )
            value = "";
        }
        else if ( value instanceof Date )
          value = value.toISOString();
        input.value = value;
        if ( this._value instanceof Date )
          hiddeninput.valueAsNumber = this._value.getTime();
        else
          hiddeninput.value = null;
        break;
      case "search":
      case "number":
      case "text":
      case "email":
      case "tel":
      case "password":
      case "url":
      case "hidden":
        // Apply a format mask as needed
        if ( this.format )
          value = this._format( value, 'full', 'full' );
        if ( value === this.format && ! withMask )
          value = "";
        input.value = value;
        break;
    }
  }


  /**
   * Update our checked state from a value change
   */
  _updateChecked() {
    let value = this._value;
    let negval, posval, unval;

    if ( this.type !== "toggle" && this.type !== "checkbox" && this.type !== "radio" )
      return;

    negval = this._typedValue( this.falsevalue );
    posval = this._typedValue( this._truevalue );
    unval = this._typedValue( this.unsetvalue );

    if ( this.grouped ) {
      value = this.value;
      if ( value == posval )
        this.checked = true;
      else
        this.checked = false;
    }
    else if ( value === unval && this.tristate ) {
      this.unset = true;
      this.checked = false;
    }
    else if ( value === negval ) {
      this.unset = false;
      this.checked = false;
    }
    else if ( value === posval || value ) {
      this.unset = false;
      this.checked = true;
    }
  }



  /**
   * Update the proxy checkbox input element
   */
  _updateToggle() {
    const input = this.renderRoot.querySelector( 'input:not(.hidden)' ) || this.querySelector( 'input' );
     
    input.checked = this.checked;
    input.indeterminate = this.unset;
  }


  /**
   * Update our internal value from user input
   * @private
   */
  _updateValue() {
    const input = this.renderRoot.querySelector( 'input:not(.hidden)' ) || this.querySelector( 'input' );

    switch ( this.type ) {
      case "datetime":
        // Always store datetime types as date
        this._value = this._typedValue( input.value, 'date' );
        break;
      case "search":
      case "number":
      case "text":
      case "email":
      case "tel":
      case "password":
      case "url":
      case "hidden":
        if ( this.format )
          this._value = this._typedValue( input.value, 'format-full' );
        else
          this._value = this._typedValue( input.value );
        break;
      case "toggle":
      case "checkbox":
      case "radio":
        this.checked = input.checked;
        this.unset = input.indeterminate;
        break;
    }
  }


  /**
   * Validate a toggle type element
   * @private
   */
  _validateToggle() {
    let valid = true;
    if ( ! this.grouped && this.required && ! this.checked )
      valid = false;
    else if ( this.grouped && this.required && ! this.checked )
      valid = this._validateGrouped();
    if ( this.hasUpdated && valid && this.validator != undefined && typeof this.validator == "function" )
      valid = this.validator( this.value );
    this.invalid = ! valid;
    this._updateMessage();
    return valid;
  }


  
  /**
   * Validate a grouped toggle type element
   * @private
   */
  _validateGrouped() {
    let elements, form, checked = false;
    form = this.closest( 'form' );
    if ( form == null )
      form = document.body;
    elements = Array.from( form.querySelectorAll( 'sc-input[name=' + this.name +  ']' ) );
    elements.forEach( elem => {
      if ( elem.checked )
        checked = true;
    } );
    return checked;
  }


  /**
   * Update the message content
   * @private
   */
  _updateMessage() {
    if ( this.invalid ) {
      if ( this.required )
        this._message = this.errormessage || "This input is required";
      else
        this._message = this.errormessage || this.comment;
    }
    else
      this._message = this.comment;
  }

  
  /** 
   * Toggle this toggle type element as appropriate
   * @private
   */
  _toggleInput(ev) {
    const item = this.shadowRoot.querySelector( '.sc-input-control-item' );
    let form, elements;
    if ( ev && ev.type == "keydown" && ev.key !== " " )
      return;
    if ( ev )
      ev.preventDefault();
    if ( ! this.disabled && ! this.readonly )
      item.focus();
    if ( this.disabled || this.readonly )
      return;
    if ( this.grouped && this.checked )
      return;
    this._pristine = false;
    if ( this.unset ) { // Unset to unchecked
      this.checked = false;
      this.unset = false;
    }
    else if ( this.checked && this.tristate ) { // Checked to unset
      this.checked = false;
      this.unset = true;
    }
    else if ( this.checked ) { // Checked to not checked
      this.unset = false;
      this.checked = false;
    }
    else { // Unchecked to checked
      this.unset = false;
      this.checked = true;
    }
    if ( this.grouped ) {
      form = this.closest( 'form' );
      if ( form == null )
        form = document.body;
      elements = Array.from( form.querySelectorAll( 'sc-input[name=' + this.name +  ']' ) );
      elements.forEach( elem => {
        if ( elem != this )
          elem.checked = false;
      } );
    }
    this._updateInput();
    this._afterChange();
  }


  /**
   * Handle the element after input
   * @private
   */
  _afterInput( _0 ) {
    this.dispatchEvent( new CustomEvent( onAfterInput, { bubbles: true, cancelable: false } ) );
    this._pristine = false;
  }


  /**
   * Handle the element when changing
   * @private
   */
  _afterChange( _0 ) {
    this.dispatchEvent( new CustomEvent( onAfterChange, { bubbles: true, cancelable: false } ) );
    this._pristine = false;
    this._updateValue();
    if ( this.autovalidate )
      this.validate();
    this._updateLabel();
  }


  /**
   * Handle the element when receiving focus
   * @private
   */
  _afterFocus( ev ) {
    const form = this.closest( 'sc-form' );
    const prevval = ev.target.value;
    if ( this.disabled || this.readonly )
      return;
    this._focused = true;
    if ( form )
      form.elementFocused();
    if ( this.format ) {
      this._updateInput( true );
      if ( prevval == "" ) {
        ev.target.setSelectionRange( 0, 0 );
        this._moveGroup( 0, ev)
      }
      else {
        this._moveGroup( 0, ev );
      }
    }
    this._showPicker();
  }


  /**
   * Handle the element when losing focus
   * @private
   */
  _afterBlur( ev ) {
    this._focused = false;

    if ( this.format ) {
      // Formatted inputs don't fire change events natively
      this._afterChange( ev );
      this._updateInput();
    }
  }


  /**
   * Emit a search when attempted by search type
   */
  _emitSearch( ev ) {
    ev.preventDefault();
    ev.stopPropagation();
    this._updateValue();
    this.dispatchEvent( new CustomEvent( onSearch, { bubbles: true, cancelable: false } ) );
  }


  /** 
   * Handle an input on the picker element
   */
  _pickerInput( ev ) {
    const input = this.renderRoot.querySelector( 'input:not(.hidden)' ) || this.querySelector( 'input' );
    let value, newev;

    ev.preventDefault();

    if ( this.type == "datetime" ) {
      value = ev.target.valueAsNumber;
      if ( isNaN( value ) )
        input.value = null;
      else if ( this.format ) {
        value = this._formatDate( new Date( value ) );
        input.value = value;
      }
      else
        input.value = new Date( value ).toISOString();
      newev = new Event( 'input' );
      input.dispatchEvent( newev );
    }
  }


  /**
   * Handle a change event on the picker element
   */
  _pickerChange( ev ) {
    const input = this.renderRoot.querySelector( 'input:not(.hidden)' ) || this.querySelector( 'input' );
    let value, newev;

    ev.preventDefault();

    if ( this.type == "datetime" ) {
      value = ev.target.valueAsNumber;
      if ( isNaN( value ) )
        input.value = null;
      else if ( this.format ) {
        value = this._formatDate( new Date( value ) );
        input.value = value;
      }
      else
        input.value = new Date( value ).toISOString();
      newev = new Event( 'change' );
      input.dispatchEvent( newev );
    }

  }


    /**
   * Handle format masks on keydown
   */
  _keyDown( ev ) {
    const mask = this.format;
    let form, start, end, vals, newpos, maskchars, partial;

    // Skip control or combo keys
    if ( ev.ctrlKey || ev.altKey || ev.key == "Shift" )
      return;

    // Attempt to submit a form
    if ( this.entersubmit && ev.key == "Enter" ) {
      form = this.closest( 'sc-form' ) || this.closest( 'form' );
      if ( form ) {
        this._updateValue();
        form.submit();
      }
    }

    // Everything else is for masked inputs
    if ( ! this.format )
      return;

    // Tab between groups as able
    if ( ev.key == "Tab" && ! ev.shiftKey )
      return this._moveGroup( 1, ev );
    else if ( ev.key == "Tab" && ev.shiftKey )
      return this._moveGroup( -1, ev );


    // Only handle delete, backspace and inputs
    if ( ev.key !== "Delete" && ev.key !== "Backspace" && ev.key.length !== 1 )
      return;

    ev.preventDefault();

    maskchars = Array.from( this.formatchars );

    // Replacing or removing a range?
    start = ev.target.selectionStart;
    end = ev.target.selectionEnd;
    vals = Array.from( ev.target.value );

    if ( end - start > 0 ) { // Got a range, clear first
      for ( let idx = start; idx < end; idx++ ) {
        if ( vals[ idx ] !== mask[ idx ] )
          vals[ idx ] = mask[ idx ];
      }
    }
    else if ( ev.key == "Delete" ) { // Just delete
      if ( start < mask.length ) {
        if ( vals[ start ] !== mask[ start ] )
          vals[ start ] = mask[ start ];
        newpos = start;
      }     
    }
    else if ( ev.key == "Backspace" ) { // Just backspace
      if ( start > 0 ) {
        if ( vals[ start - 1 ] !== mask[ start - 1 ] )
          vals[ start - 1 ] = mask[ start - 1 ];
        newpos = start - 1;
      }
    }

    // Insert the key into pos
    if ( start > vals.length )
      return;
    else if ( ev.key.length == 1 ) {
      if ( maskchars.includes( vals[ start ] ) ) // Got a blank?
        vals[ start ] = ev.key;
      else if ( maskchars.includes( mask[ start ] ) ) // Replace?
        vals[ start ] = ev.key;
      else {
        // Find next slot to put key
        for ( let idx = start; idx < mask.length; idx++ ) {
          if ( maskchars.includes( vals[ idx ] ) ) {
            vals[ idx ] = ev.key;
            break;
          }
        }
      }

      // Pattern conforming?
      if ( this.autovalidate ) {
        partial = this._partialValue( vals.join( '' ) );
        if ( this._formatpattern && ! this._formatpattern.exec( partial ) )
          return;
        else if ( this.validator && ! this.validator( partial ) )
          return;
      }

      // Find next input char for the cursor or just go next
      newpos = start + 1;
      for ( let idx = start + 1; idx < mask.length; idx++ ) {
        if ( maskchars.includes( mask[ idx ] ) ) {
          newpos = idx;
          break;
        }
      }
    }

    // Update value and set new cursor position
    ev.target.value = vals.join( '' );
    ev.target.setSelectionRange( newpos, newpos );

    // Perform post-modify actions
    this._afterInput();
  }


  /**
   * Handle a paste into a formatted field
   */
  _beforePaste( ev ) {
    let mask, maskchars, start, end, vals, topaste, valIdx = 0, inIdx = 0, partial;

    if ( ! this.format )
      return;

    ev.preventDefault();

    mask = this.format;
    maskchars = Array.from( this.formatchars );
    start = ev.target.selectionStart;
    end = ev.target.selectionEnd;
    vals = Array.from( ev.target.value );
    topaste = ( ev.clipboardData || window.clipboardData ).getData('text/plain');

    // Clear enough space
    if ( end - start == 0 || topaste.length > ( end - start ) ) {
      end = start + topaste.length;
    }

    //  Clear the range
    for ( let idx = start; idx < end; idx++ ) {
      if ( vals[ idx ] !== mask[ idx ] )
        vals[ idx ] = mask[ idx ];
    }

    // Insert the content into pos
    if ( start > vals.length )
      return;
    else if ( topaste.length > 0 ) {
      valIdx = start;

      while( inIdx < topaste.length && valIdx < vals.length ) {

        // Into a blank spot?
        if ( maskchars.includes( vals[ valIdx ] ) )
          vals[ valIdx++ ] = topaste[ inIdx++ ];
        else if ( vals[ valIdx ] === topaste[ inIdx ] ) {
          // Pasted format char?
          valIdx++;
          inIdx++;
          continue;
        }
        else {
          // Find next slot to put input
          for ( let idx = valIdx; idx < vals.length; idx++ ) {
            if ( maskchars.includes( mask[ idx ] ) ) {
              vals[ idx ] = topaste[ inIdx++ ];
              valIdx++;
              break;
            }
            else
              valIdx++;
          }
        }
      }

      // Pattern conforming?
      if ( this.autovalidate ) {
        partial = this._partialValue( vals.join( '' ) );
        if ( this._formatpattern && ! this._formatpattern.exec( partial ) )
          return;
        else if ( this.validator && ! this.validator( partial ) )
          return;
      }
    }

    // Update value and set new cursor position
    ev.target.value = vals.join( '' );
    ev.target.setSelectionRange( end, end );

    // Perform post-modify actions
    this._afterInput();
  }


  /**
   * Move within the elements groups
   * @private
   * @param {Number} direction    - The direction to move, > 0 for foward, < 0 for backward, 0 for current
   * @param {Event} ev            - The associated keydown event
   */
  _moveGroup( direction, ev ) {
    const groups = this._formatgroups;
    let curpos, next;

    curpos = ev.target.selectionStart;
    for ( let [ start, end ] of groups )  {
      if ( direction > 0 && end > curpos && start > curpos ) {
        if ( next === undefined )
          next = start;
        else if ( start < next )
          next = start;
      }
      else if ( direction < 0 && end < curpos ) {
        if ( next === undefined )
          next = start;
        else if ( start > next )
          next = start;
      }
      else if ( direction == 0 && end >= curpos ) {
        if ( start <= curpos ) {
          next = start;
          break;
        }
      }
    }

    // Choose the first group if not moving
    if ( direction == 0 ) {
      for ( let start of groups.keys() ) {
        if ( next == undefined || start < next )
          next = start;
      }
    }

    if ( next !== undefined ) {
      ev.preventDefault();
      ev.target.setSelectionRange( next, groups.get( next ) + 1 );
    }

  }
  

  /**
   * Format a string per the mask
   * @param {*} value           - The value to format
   * @param {String} fromformat - The provided format, or any if not known
   * @param {String} toformat   - The desired format
   */
  _format( value, fromformat, toformat ) {
    const mask = Array.from( this.format );
    const maskchars = Array.from( this.formatchars );
    let strvalue, valcount, formatchars, inval, plain = false;

    // Coerce value to a string
    if ( typeof value === "string" )
      strvalue = value;
    else if ( typeof value === "number" )
      strvalue = value + "";
    else {
      this.app.log( "error","Cannot format non string" );
      return undefined;
    }

    // If nothing to convert
    if ( strvalue === "" || strvalue === null )
      return this.format;

    // If too big for the mask
    if ( strvalue.length > this.format.length ) {
      this.app.log( "error", "Cannot convert string longer than format" );
      return undefined;
    }

    // Arrayify input value for checking and conversion
    inval = Array.from( strvalue );

    // Work out from format as needed
    if ( fromformat == 'any' ) {
      if ( value.length == this.format.length ) {
        valcount = inval 
          .filter( m => maskchars.includes( m ) )
          .length;
        if ( valcount > 0 )
          fromformat = 'full';
        else
          fromformat = 'long';
      }
      else { // shorter value than mask
        formatchars = mask
          .filter( m => ! maskchars.includes( m ) );
        valcount = inval
          .filter( v => formatchars.includes( v ) )
          .length;
        if ( formatchars.length == valcount )
          fromformat = "short";
        else {
          fromformat = "full";
          plain = true;
        }
      }
    }

    // Format plain strings first
    if ( plain )
      inval = this._plainToMask( strvalue );

    // Expand short values into full
    if ( fromformat == "short" ) {
      inval = this._expandToMask( strvalue );
    }

    // Convert long to full format
    if ( fromformat == "long" ) {
      for ( let idx in inval ) {
        if ( maskchars.includes( mask[ idx ] ) ) {
          if ( inval[ idx ] == " " )
            inval[ idx ] = mask[ idx ];
        }
      }
    }


    // Modify as needed
    switch( toformat ) {
      case "full":
        return inval.join( '' );
      case "long":
        for ( let idx in inval ) {
          if ( maskchars.includes( mask[ idx ] ) && inval[ idx ] === mask[ idx ] )
            inval[ idx ] = " ";
        }
        return inval.join( '' );
      case "short":
        for ( let idx in inval ) {
          if ( maskchars.includes( mask[ idx ] ) && inval[ idx ] === mask[ idx ] )
            inval[ idx ] = null;
        }
        return inval.filter( v => v !== null ).join( '' );
      case "plain":
        for ( let idx in inval ) {
          if ( inval[ idx ] === mask[ idx ] )
            inval[ idx ] = null;
        }
        return inval.filter( v => v !== null ).join( '' );
    }
  }



  /**
   * Expand a short-formatted string to a full format mask
   * @private
   * @param {String} value    - Value to expand
   * @returns {String[]}      - Expanded string as array
   */
  _expandToMask( value ) {
    const outval = Array.from( value );
    const mask = Array.from( this.format );
    const maskchars = Array.from( this.formatchars );
    let nextchar, valIdx = 0;

    // Build from the value
    for ( let idx in mask ) {

      // If a formatting char
      if ( ! maskchars.includes( mask[ idx ] ) ) {
        nextchar = mask[ idx ];

        // Find and check formatting char position in inval
        valIdx = outval.findIndex( v => v == nextchar );
        if ( valIdx == -1 || valIdx > idx ) {
          this.app.log( "error", "Cannot format mask with mismatch format" );
          return undefined;
        }

        // Create space the value as needed
        outval.splice( valIdx, 0, ...new Array( idx - valIdx ) );

        // Hide the formatting character for now
        outval[ idx ] = null;
      }
    }

    // Add full formatting chars
    for ( let idx in mask ) {
      if ( ! maskchars.includes( mask[ idx ] ) )
        outval[ idx ] = mask[ idx ];
      else if ( outval[ idx ] === undefined )
        outval[ idx ] = mask[ idx ];
    }

    return outval;
  }


  /**
   * Convert a plain string to a full format string
   * @private
   * @param {String} value      - String to convert
   * @returns {String[]}        - Formatted string as an array
   */
  _plainToMask( value ) {
    const mask = Array.from( this.format );
    const maskchars = Array.from( this.formatchars );
    let valIdx = 0, outval = [];

    for ( let maskIdx in mask ) {
      if ( maskchars.includes( mask[ maskIdx ] ) ) {
        if ( value[ valIdx ] !== mask[ maskIdx ] )
          outval.push( value[ valIdx++ ] );
        else if ( value[ valIdx ] === mask[ maskIdx ] ) {
          outval.push( mask[ maskIdx ] );
          valIdx++;
        }
        else
          outval.push( mask[ maskIdx ] );
      }
      else {
        outval.push( mask[ maskIdx ] );
        if ( mask[ maskIdx ] == value[ valIdx ] )
          valIdx++;
      }
    }
    return outval;
  }



  /**
   * Apply a format mask to a date
   * @private
   * @param {Date} date       - The date to format
   * @returns {String}        - The string representation
   */
  _formatDate( date ) {
    let retdate;
    if ( date === "" )
      return this.format;
    if ( date && date instanceof Date && isNaN( date.getTime() ) )
      retdate = this.format;
    else if ( date && date instanceof Date )
      retdate = DayJS( date ).format( this.format );
    else if ( date && ! isNaN( date ) )
      retdate = DayJS( date, 'x' ).format( this.format );
    return retdate;
  }


  /**
   * Construct a partial value for validation
   * @private
   * @param {String} inval  - Input value
   * @returns {String}      - Partial value
   */
  _partialValue( inval ) {
    const mask = Array.from( this.format );
    const maskChars = Array.from( this.formatchars );
    let ret = false, outval = "";

    for ( let idx = mask.length - 1; idx >= 0; idx-- ) {
      if ( ! ret && ! maskChars.includes( mask[ idx ] ) )
        continue;
      else if ( ret && ! maskChars.includes( mask[ idx ] ) ) {
        outval = mask[ idx ] + outval;
        continue;
      }
      
      if ( ! ret && mask[ idx ] !== inval[ idx ] )
        ret = true;
      if ( ret && maskChars.includes( inval[ idx ] ) )
        continue;

      if ( ret )
        outval = inval[ idx ] + outval;
    }
    return outval;
  }



  /**
   * Update the elements label display
   * @protected
   */
  _updateLabel( autofill=false ) {
    let val = this._value;
    if ( this.type == "toggle" || this.type == "checkbox" || this.type == "radio" )
      return;
    if ( this.type == "datetime" )
      return this._smalllabel = true;
    if ( this.format )
      return this._smalllabel = true;
    if ( autofill || ( val != undefined && val !== "" ) || this.placeholder != "" )
      this._smalllabel = true;
    else if ( this.smalllabel == false )
      this._smalllabel = false;
  }


  /**
   * Show the appropriate picker if there is one
   * @private
   */
  _showPicker() {
    let datetimeInput;

    if ( this.disabled || this.readonly )
      return;

    switch ( this.type ) {
      case "datetime": 
        datetimeInput = this.shadowRoot.querySelector( 'input.hidden' );
        if ( datetimeInput && datetimeInput.showPicker ) {
          try { 
            // This can fail depending on event origin, so quietly fail.
            datetimeInput.showPicker();
          }
          catch ( e ) {}
        }
        break;
      default:
        break;
    }

  }


  /**
   * Work out the datetime picker type, and format min/maxes
   */
  _datetimeType() {
    let hasTime, hasDate;

    if ( this.type !== "datetime" )
      return;

    hasTime = this._hasTime();
    hasDate = this._hasDate();

    if ( hasDate && hasTime )
      this._pickerType = "datetime-local";
    else if ( hasDate )
      this._pickerType = "date";
    else if ( hasTime )
      this._pickerType = "time";
    else
      this._pickerType = "datetime-local"
  }


  _hasTime() {
    const timeParts = Array.from( 'HhSsmAaxX' );
    for ( let bit of timeParts )
      if ( this.format && this.format.indexOf( bit ) !== -1 )
        return true;
    return false;
  }


  _hasDate() {
    const dateParts = Array.from( 'YMDZ' );
    for ( let bit of dateParts )
      if ( this.format && this.format.indexOf( bit ) !== -1 )
        return true;
    return false;
  }




  /**
   * Get this elements template
   * @private
   */
  _inputTemplate() {
    switch (this.type) {
      case "hidden":
        return this.builder`
          <input type="${this.type}" name="${this.name}" value=${this._value}></input>
        `
      case "search":
      case "datetime":
      case "text":
      case "email":
      case "number":
      case "tel":
      case "password":
      case "url":
        return this.builder`
          <div 
              class="sc-input-text" 
              ?focused=${this._focused}
          >
            <div class="sc-input-text-before-icon">
              <slot name="iconbefore">
                <div></div>
              </slot>
            </div>
            <div class="sc-input-text-input">
              <slot name="input">
                <input 
                  slot="input"
                  type="${this._type}" 
                  name="${this.name}" 
                  ?required=${this.required}
                  ?readonly=${this.readonly}
                  ?disabled=${this.disabled}
                  placeholder="${this.placeholder}" 
                  minlength=${this.ifDefined(this.minlength)}
                  maxlength=${this.ifDefined(this.maxlength)}
                  min=${this.ifDefined(this.min)}
                  max=${this.ifDefined(this.max)}
                  step=${this.ifDefined(this.step)}
                  autocomplete=${this.ifDefined(this.autocomplete)}
                  pattern=${this.ifDefined(this.pattern)}
                  tabindex=${! this.disabled && ! this.readonly ? "0" : "-1"}
                  @keydown=${this._keyDown}
                  @input=${this._afterInput} 
                  @paste=${this._beforePaste}
                  @change=${this._afterChange}
                  @search=${this._emitSearch}
                  @focus=${this._afterFocus}
                  @blur=${this._afterBlur}
                ></input>
              </slot>

              ${ this.label == '' ? '': this.builder`
                <label 
                  for="${this.name}" 
                  class="sc-input-text-label" 
                  ?required=${this.required} 
                  ?disabled=${this.disabled}
                  ?smalllabel=${this._smalllabel}
                  ?noanimate=${this.noanimate}
                  this._focused = false;
                >
                  <slot name="label">
                    <span>${this.label}</span>
                  </slot>
                </label>`
              }
            </div>
            ${this._pickerTemplate()}
            <div class="sc-input-text-after-icon">
              <slot name="iconafter">
                <div>
                  ${this._pickerIconTemplate()}
                </div>
              </slot>
            </div>
          </div>
          <div class="sc-input-message" ?hidden=${this.nomessage} ?noanimate=${this.noanimate} ?invalid=${this.invalid} ?comment=${this._hascomment}>
            <slot name="message">
              <span>${this._message}</span>
            </slot>
          </div>
          `
      case "toggle":
      case "checkbox":
      case "radio":
        return this.builder`
          <div 
              class="sc-input-toggled" 
              type=${this._type} 
              position=${this.position}
              ?checked=${this.checked}
              ?unset=${this.unset}
              ?disabled=${this.disabled}
              ?readonly=${this.readonly}
              @mousedown=${e=>{e.preventDefault()}}
            >
            ${ this.label == '' ? '': this.builder`
            <label class="sc-input-label" ?required=${this.required}>
              <slot name="label">
                <span>${this.label}</span>
              </slot>
            </label>`}
            <label class="sc-input-control" @click=${this._toggleInput}>
              <input 
                  slot="input"
                  type="checkbox" 
                  name=${this.name} 
                  ?disabled=${this.disabled} 
                  value=${this._value}>
              </input>
              <span 
                  tabindex=${this.disabled || this.readonly ? "-1" : "0"}
                  class="sc-input-control-item" 
                  @keydown=${this._toggleInput}
                  ?noanimate=${this.noanimate} 
                  ?disabled=${this.disabled}
                  ?readonly=${this.readonly}
              >
              </span>
            </label>
          </div>
          <div class="sc-input-message" ?hidden=${this.nomessage} ?noanimate=${this.noanimate} ?invalid=${this.invalid} ?comment=${this._hascomment}>
            <slot name="message">
              ${this._message}
            </slot>
          </div>`
    }
  }


  /**
   * Build a picker input element"
   */
  _pickerTemplate() {
    let inputType;
    if ( this.type == "datetime" ) {
      inputType = this._pickerType;
      return this.builder`
        <input 
          tabindex=-1
          class="hidden" 
          type=${inputType} 
          @input=${this._pickerInput}
          @change=${this._pickerChange}
          min=${this.ifDefined(this.min)}
          max=${this.ifDefined(this.max)}
          step=${this.ifDefined(this.step)}
        ></input>
      `;
    }
  }


  _pickerIconTemplate() {
    if ( this.type !== "datetime" || this.noicon )
      return this.builder``;
    return this.builder`
      <div class="sc-input-picker-icon" @click=${this._showPicker}>
        ${this.includeSvg( faIcon( faCalendarAlt ).html[0] )}    
      </div>
    `;
  }

}



export { SimpleInput };


