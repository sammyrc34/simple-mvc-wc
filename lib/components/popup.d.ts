/**
 * A popup window for user interaction
 * @class
 */
export class SimplePopup extends SimpleComponent {
    static get _elementName(): string;
    _lastx: any;
    _lasty: any;
    _offsetx: number;
    _offsety: number;
    _currentx: number;
    _currenty: number;
    _zindex: number;
    renderPopup(header: any, body: any, footer: any, close: any): import("lit-html").TemplateResult<1>;
    render(): import("lit-html").TemplateResult<1>;
    firstUpdated(bits: any): void;
    updated(changes: any): void;
    /***************
     * Our methods
     **************/
    /**
     * Reveal this popup
     * @public
     */
    public show(): void;
    open: boolean;
    /**
     * Hide this popup
     * @public
     */
    public hide(): void;
    /**
     * Disable the body from scrolling
     * @private
     */
    private _disableBodyScroll;
    /**
     * Enable body scrolling again
     * @private
     */
    private _enableBodyScroll;
    /**
     * Update the position of this popup
     * @private
     */
    private _updatePosition;
    /**
     * Handle a drag start
     * @private
     */
    private _moveStart;
    _dragging: boolean;
    /**
     * Handle a drag end
     * @private
     */
    private _moveEnd;
    /**
     * Move this popup if being dragged
     * @private
     */
    private _move;
    /**
     * Update display after or before animation
     */
    _updateDisplay(): void;
    /**
     * Hide this popup if we can
     * @private
     */
    private _autohide;
}
/**
 * Confirmation type popup with a consistent style
 * @class
 */
export class SimpleConfirm extends SimplePopup {
    _promRes: (value: any) => void;
    _promRej: (reason?: any) => void;
    _prom: Promise<any>;
    willUpdate(changes: any): void;
    _yesLabel: string;
    _noLabel: string;
    /***************
     * Our methods
     **************/
    /**
     * Show and wait for a response
     * @async
     * @public
     * @param {Boolean} [shouldThrow=false]   - If the negative response should reject instead of resolve
     * @returns {Promise<Boolean>} promise    - Promise resolving to true or false indicating yes/no
     */
    public promptWait(shouldThrow?: boolean): Promise<boolean>;
    /**
     * Handle a negative user response
     * @private
     */
    private _fireNo;
    /**
     * Handle a positive user response
     * @private
     */
    private _fireYes;
}
import { SimpleComponent } from '../index.js';
