/**
 * An event fired when the button is clicked
 * @event SimpleButton#click
 */
export class SimpleButton extends SimpleComponent {
    static get _elementName(): string;
    render(): import("lit-html").TemplateResult<1>;
    iconAndText(): import("lit-html").TemplateResult<1>;
    /**
     * Deliberately set focus to this button
     */
    focus(): void;
    /**
     * Inhibit a bubbled or other click outside the button
     */
    _inhibitClick(ev: any): any;
    /**
     * Attempt a form action if a type is set within a form.
     * Supports vanilla forms and SimpleForm elements
     */
    tryClick(ev: any): any;
    toggled: any;
}
import { SimpleComponent } from '../index.js';
