/**
 * A carousel for any contained content, such as images or forms
 * @class
 */
export class SimpleCarousel extends SimpleComponent {
    static get _elementName(): string;
    _position: number;
    _count: number;
    _reveal: boolean;
    _skipanim: boolean;
    _touching: boolean;
    _direction: any;
    _maxH: number;
    _maxW: number;
    _kids: any;
    _moving: boolean;
    _nudging: boolean;
    _moveTo: number;
    _next: any;
    _curX: number;
    _curY: number;
    _lastWidth: number;
    _lastHeight: number;
    resizeobserver: Polyfill;
    /***************
     * Templates
     **************/
    render(): import("lit-html").TemplateResult<1>;
    controlsTemplate(): import("lit-html").TemplateResult<1>;
    circleControlsTemplate(): import("lit-html").TemplateResult<1>;
    shadowControlsTemplate(): import("lit-html").TemplateResult<1>;
    /***************
     * lit methods
     **************/
    update(props: any): void;
    willUpdate(changes: any): void;
    /***************
     * own methods
     **************/
    /**
     * Get the current zero-indexed position
     * @property {Number} position
     */
    get position(): number;
    /**
     * Handle the touchstart event
     * @private
     */
    private _touchStart;
    /**
     * Handle the touchcancel event
     * @private
     */
    private _touchCancel;
    /**
     * Handle the touchmove event
     * @private
     */
    private _touchMove;
    /**
     * Handle the touchend event
     * @private
     */
    private _touchEnd;
    /**
     * Nudge a panel one direction or the other
     * @private
     */
    private _nudge;
    /**
     * Place the next panel ready for animation
     * @private
     */
    private _prepareNudgePos;
    /**
     * Check if we can move to a previous panel
     * @returns {Boolean}
     */
    canPrevious(): boolean;
    /**
     * Check if we can move to a next panel
     * @returns {Boolean}
     */
    canNext(): boolean;
    _revealControls(_0: any): void;
    _hideControls(_0: any): void;
    _slotChanged(_0: any): void;
    /**
     * Setup panels for display
     * @private
     */
    private _setupKids;
    /**
     * Inherit animation time from CSS
     * @private
     */
    private _inheritAnimateTime;
    animatetime: number;
    /**
     * Show a specific panel
     * @param {Number} panelIndex         - The zero-based index of which paanel to show
     * @param {Boolean} [instant=false]   - If the carousel should move instantly
     */
    show(panelIndex: number, instant?: boolean): Promise<boolean>;
    /**
     * Move to a specific via panel selection element
     */
    gotoPanel(ev: any): void;
    /**
     * Prepare next panel
     */
    _preparePanels(current: any, next: any): void;
    /**
     * Adjust panel display as needed after a movement
     * @private
     */
    private _adjustPanels;
    /**
     * Move to the previous panel
     * @async
     * @param {Number} [panelIndex] - Optional panel index to move backward to
     */
    previous(panelIndex?: number): Promise<boolean>;
    /**
     * Move to the next panel
     * @async
     * @param {Number} [panelIndex] - Optional panel index to move forward to
     */
    next(panelIndex?: number): Promise<boolean>;
}
import { SimpleComponent } from '../index.js';
import { ResizeObserver as Polyfill } from '@juggle/resize-observer';
