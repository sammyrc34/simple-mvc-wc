export class SimpleTabsContainer extends SimpleComponent {
    static get _elementName(): string;
    resizeobserver: Polyfill;
    /******************
     * Lit's methods
     *****************/
    render(): import("lit-html").TemplateResult<1>;
    willUpdate(changes: any): Promise<void>;
    firstUpdated(): Promise<void>;
    /******************
     * Own methods
     *****************/
    /**
     * Select a specific tab
     * @param {SimpleTab} toSelect    - Tab element to select
     */
    selectTab(toSelect: SimpleTab): void;
    /**
     * Select a specific tab by index
     * @param {Number} index    - The zero-indexed tab number to select
     */
    selectTabIndex(index: number): void;
    /**
     * Try to select a tab
     */
    _selectTab(tab: any): void;
    /**
     * Handle a change to slotted tabs, propagate properties
     */
    _slotChanged(): Promise<void>;
    /**
     * Detect the size of the largest tab and size ourself to that
     */
    _detectSize(): void;
    /**
     * Check that a tab is selected
     */
    _checkSelected(): void;
}
export class SimpleTab extends SimpleComponent {
    static get _elementName(): string;
    raised: boolean;
    scroll: boolean;
    label: any;
    /******************
     * Lit's methods
     *****************/
    render(): import("lit-html").TemplateResult<1>;
    /******************
     * Own methods
     *****************/
    /**
     * Attempt to select this tab
     */
    selectTab(): void;
    /**
     * Get this tab's height?
     */
    _getTabHeight(): number;
    /**
     * Get this tabs' content height
     */
    _getContentSize(): number[];
}
import { SimpleComponent } from '../index.js';
import { ResizeObserver as Polyfill } from '@juggle/resize-observer';
