
import { SimpleComponent, SimpleRecord, SimpleCollection, SimpleRemoteRecord, SimpleRemoteCollection, SimplePollingRecord, SimplePollingCollection } from '../index.js';


import equal from 'fast-deep-equal';

/**
 * Data-bound component super class for all data model bound components
 * This class cannot be instantiated directly
 * @class
 */
class DataboundComponent extends SimpleComponent {

  constructor() {
    super();

    // Decendants must provide:
    //  - modelName:  Name of model to bind to
    this.bindMode = undefined;  // single or multi
    this.bindToNew = undefined; // If the component supports binding to new records

    // Protected properties
    this.dataModel = undefined; // model object
    this.idField = undefined;
    this.contextchange = false;

    // For multi-bind mode
    this.begin = undefined;
    this.range = undefined;

    // State properties
    this._bound = false;
    this._boundid = undefined; /* The actually bound record ID */
    this._silent = false;
  }


  static get properties() {
    return Object.assign( {}, super.properties, {

      // User-available reactive properties
      modelName:      { type: String, def: undefined, desc: "The name of the model this component should bind to" },
      bindId:         { type: String, def: undefined, attribute: false, desc: "The ID of the record to bind to" },
      modelContext:   { type: Object, def: undefined, desc: "Any context for a bound model" },

    } );
  }


  static get styles() {
    return super.styles;
  }


  /**
   * Get bound state
   * @type {Boolean}
   */
  get bound() {
    return this._bound;
  }


  /** Get the currently bound ID
   * @type {String|Number}
   */
  get boundid() {
    return this._boundid;
  }


  /* Catch this component being removed, unbind first */
  disconnectedCallback() {
    super.disconnectedCallback();
    this.unbind();
    this.dataModel = undefined;
  }


  /* Check that an update should occur */
  shouldUpdate() {
    if ( this._silent ) {
      this._silent = false;
      return false;
    }
    return true;
  }


  /**
   * Handle updates being made
   */
  willUpdate( changes ) {
    super.willUpdate( changes );

    // Catch model context changing, flag that context has changed
    if ( changes.has( 'modelContext' ) && this.bound ) {
      if ( ! equal( this.modelContext, changes.get( 'modelContext' ) ) ) {
        this.contextchange = true;
        this.dataModel.setContextQuery( this.modelContext || {} );
      }
    }
  }


  /**
   * Catch property changes before render
   * Note: while performing update-causing action post an update is not efficient, re-binding mid update could result in odd states
   * @async
   */
  async updated( props ) {
    let doBind = false;
    let doUnbind = false;
    super.updated( props );

    // Context change flag always reset post property update lifecycle
    this.contextchange = false;

    // Catch bind ID changing
    if ( props.has( 'bindId' ) && this.bindId !== props[ 'bindId' ] && this.bindId !== undefined )
      doBind = true;
    else if ( props.has( 'bindId' ) && this.bound && this.bindId === undefined )
      doUnbind = true;

    // Catch model related changes
    if ( props.has( 'modelName' ) && this.bound )
      doUnbind = true;
    else if ( props.has( 'modelName' ) && this.bindMode == "multi" )
      doBind = true;

    /* Binding or unbinding happens asyncronously rather than holding up rendering. */
    try {
      if ( doUnbind )
        await this.unbind();
      if ( doBind )
        await this.bind();
    }
    catch( err ) {
      this.app.log( "error", "Binding or unbinding failed: ", err );
    }
  }


  /**
   * Get the model associated with this component
   * @returns {SimpleCollection|SimpleRecord|SimpleRemoteCollection|SimpleRemoteRecord|SimplePollingCollection|SimplePollingRecord} - Data model class
   */
  getModel() {
    if ( ! this.dataModel )
      this.dataModel = this.app.fetchModel( this.modelName );
    return this.dataModel;
  }


  /**
   * Bind if able
   * @throws        - In event of any failure during binding
   */
  async bind() {
    // Support optionally bound components
    if ( ! this.bindMode || ! this.modelName )
      return;

    this.app.log( "debug", "Binding component", this.constructor.name, "to model", this.modelName );

    /* Get the model if needed */
    if ( this.dataModel == undefined ) {
      this.dataModel = this.app.fetchModel( this.modelName );
      if ( this.dataModel == undefined ) {
        this.app.log( 'error', 'Unknown data model', this.modelName );
        throw new Error( "Could not find model to bind to" );
      }
    }

    // Sync ID field
    this.idField = this.dataModel.idField;

    try {
      /* Bind multi-row models */
      if ( this.bindMode == "multi" ) {
        this._boundid = null;
        this._bound = true;
        await this.dataModel.bindMulti( this.uuid, this, this.modelContext )
      }

      /* Bind single-row models */
      if ( this.bindMode == "single" && this.bindId !== undefined ) {
        this._boundid = this.bindId;
        this._bound = true;
        await this.dataModel.bindSingle( this.uuid, this, this.bindId, this.bindToNew, this.modelContext );
      }
    }
    catch( err ) {
      this.app.log( "error", "Failed to bind to data model", err.message );
      this._bound = false;
      this._boundid = undefined;
      throw err;
    }
  }


  /**
   * Unbind and forget the data model
   * @async
   */
  async unbind() {
    this.app.log( "debug", "Unbinding record from component", this.constructor.name );
    if ( this.dataModel )
      await this.dataModel.unbind( this.uuid, this );
    this.dataModel = undefined;
    this.bindId = undefined;
    this._bound = false;
    this._boundid = undefined;
  }


  /**
   * Check if the bind model is missing anything before a fetch
   * @returns {Boolean}   - If the model is able to perform a fetch
   */
  canFetch() {
    if ( ! this.bound || ! this.dataModel )
      return false;
    if ( this.dataModel.checkBinds )
      return this.dataModel.checkBinds();
    return true;
  }



  /**
   * Called by a bound model when a change to model or data has occurred.
   * Must be overridden in subclasses. Promise-aware to ensure structured handling
   * @async
   * @abstract
   * @param {Object} detail
   * @param {String} detail.change              - The type of change, load, add, update or remove
   * @param {Boolean} detail.pending            - If the change is non-authoritative (eg pending)
   * @param {Map} detail.mapping                - Mapping of IDs for an add if known
   * @param {String[]|Number[]} details.ids     - The IDs associated with the change
   */
  async _dataChanged( detail ) {
    this.app.log( 'warn', 'Unhandled data model change' );
  }


  /**
   * Called by a bound model when a change to the model has occurred.
   * Must be overridden in subclasses. Promise-aware to ensure structured handling
   * @async
   * @abstract
   * @param {String} type       - Type of status change, either 'pending', 'bind', 'unbind', 'disable' or 'enable'
   */
  async _statusChanged( type ) {
    this.app.log( 'warn', "Unhandled model status change" );
  }


  /**
   * Process one complete record into the bound model
   * @param {Object} data                 - The changes to make in the model
   * @param {String} [id]                 - The id for the record change, required in multi-bind mode
   * @throws                              - Errors if unable to add or update the record
   */
  ingressRecord( data, id ) {
    var current,newid, record;
    if ( ! this.bound )
      throw new Error( "Component not bound to model" );
    if ( this.bindMode == "multi" && id == undefined )
      throw new Error( "Model record ID required for data update in multi bind mode" );
    if ( id == undefined )
      id = this.boundid;
    this.app.log( "debug", "Processing data record into model", this.modelName );

    // Adding?
    if ( ! this.dataModel.hasRecord( id ) && ! this.bindToNew )
      throw new Error( "Unable to process record, existing bound record not found in model" );

    // Get current or blank record
    if ( ! this.dataModel.hasRecord( id ) ) {
      current = this.dataModel.defaultRecord();
      current[ this.idField ] = id;
    }
    else
      current = this.dataModel.getRecord( id );

    // Add or update
    if ( ! this.dataModel.hasRecord( id ) ) {
      // Add the new record, and if ID changes, follow it
      record = Object.assign( current, data );
      newid = this.dataModel.add( [ record ] );
      if ( newid[ 0 ] !== this.bindId ) {
        // About to set properties silently, ensure all updates are up to date
        if ( this.isUpdatePending )
          this.performUpdate();

        this._silent = true;
        this.bindId = newid[ 0 ];
        this._boundid = newid[ 0 ];
        this._bound = true;

        // Ensure the component processes silent updates now
        this.performUpdate();
        this.dataModel.changeSingleBindId( this.uuid, newid[ 0 ] );
      }
    }
    else {
      record = Object.assign( current, data );
      this.dataModel.update( [ record ] );
    }

    return id;
  }


}


export { DataboundComponent };

