/**
 * The simple form component, supporting binds and submit actions
 * @class
 * @extends DataboundComponent
 */
export class SimpleForm extends DataboundComponent {
    static get _elementName(): string;
    bindMode: string;
    modelName: string;
    bindToNew: boolean;
    _predisabled: any[];
    _observer: any;
    _timer: NodeJS.Timeout;
    firstUpdated(): void;
    render(): import("lit-html").TemplateResult<1>;
    /**
     * Short-hand method to load form data from a model record
     * @public
     */
    public load(id: any): void;
    /**
     * Set a form element as disabled or not (won't automatically be disabled / enabled)
     * @public
     * @param {String} name             - The form element to disabled
     * @param {Boolean} disabled        - Disabled or not
     */
    public setDisabledElement(name: string, disabled: boolean): void;
    /**
     * Reset the form to intial record values
     * @public
     */
    public reset(): void;
    /**
     * Clear the form to defaults, clearing form elements to defaults and unbind
     * @public
     */
    public clear(): void;
    /**
     * Submit this form as appropriate if possible.
     * @public
     * @fires SimpleForm#beforesubmit       - A cancelable event fired at the start of submission, before validation
     * @fires SimpleForm#submit             - A cancelable event fired before updating any bound models
     * @returns {String}                    - A message if submission is not possible or was cancelled, undefined upon success
     * @throws                              - In the case of any hard error during submission
     */
    public submit(): string;
    /**
     * Validate the form values
     * @public
     * @return {Boolean}          - Validation result
     */
    public validate(): boolean;
    /**
     * Get the form's data, providing this as an Object
     * @public
     */
    public collectValues(): {};
    /**
     * Determine if the form has changed
     * @public
     * @returns {Boolean}         - If form has been altered
     */
    public changed(): boolean;
    /**
     * Apply changes to the form elements without submission
     * @param {Object} changes      - A key,value set to apply
     * @throws                      - If unable to apply changes to field elements
     */
    applyChanges(changes: any): void;
    /**
     * Update elements to be disabled or not without overriding manually disabled elements
     * @private
     * @param {Boolean} disabled        - Form disabled state
     */
    private _setDisabled;
    /**
     * Set required fields from the model
     * @private
     * */
    private _setRequired;
    /**
     * Handle a data model status change
     * @protected
     */
    protected _statusChanged(action: any): void;
    disable: boolean;
    /**
     * Handle a model data change
     * @protected
     * @async
     * @see DataboundComponent#_dataChanged
     * @param {Object} detail
     * @param {String} detail.change          - The type of change, load, add, update or remove
     * @param {Boolean} detail.local          - If the change is non-authoritative (eg local)
     * @param {String[]|Number[]} details.ids - The IDs associated with the change
     */
    protected _dataChanged(detail: {
        change: string;
        local: boolean;
    }): Promise<void>;
    /**
     * Read the bound record and apply values to the form
     * @private
     * */
    private _loadFromModel;
    /**
     * A form element is focused, watch for autofills
     * @protected
     */
    protected elementFocused(): void;
    /**
     * Check inputs for autofill, set values
     * @private
     */
    private _checkAutofill;
}
import { DataboundComponent } from './databound.js';
