
import { ResizeObserver as Polyfill } from '@juggle/resize-observer';
import { timeline } from 'motion';


import { SimpleComponent } from '../index.js';

import carouselStyles from './carousel.css';

const ResizeObserver = window.ResizeObserver || Polyfill;


/**
 * A cancelable event before a position change
 * @event SimpleCarousel#position-change
 * @type {Object} detail
 * @property {Number} from  - The indexed position transitioning from
 * @property {Number} to    - The indexed position transitioning to
 */
const onPosChange = "position-change";

/**
 * An event after a position change
 * @event SimpleCarousel#position-changed
 * @type {Object} detail
 * @property {Number} from  - The indexed position transitioned from
 * @property {Number} to    - The indexed position transitioned to
 */
const onPosChanged = "position-changed";



/**
 * A carousel for any contained content, such as images or forms
 * @class
 */
export class SimpleCarousel extends SimpleComponent {


  /**
   * @emits {position-change} Before a position change occurs, cancelable
   * @emits {position-changed} After position has changed with position provided
   */


  constructor() {
    super();

    // State properties
    this._position      = 0;
    this._count         = 0;
    this._reveal        = false;
    this._skipanim      = false;
    this._touching      = false;
    this._direction     = undefined;
    this._maxH          = 0;        /* Max height of child or container if filling */
    this._maxW          = 0;
    this._kids          = undefined;
    this._moving        = false;
    this._nudging       = false;
    this._moveTo        = 0;
    this._next          = undefined;
    this._curX          = 0;
    this._curY          = 0;

    this._lastWidth     = undefined;
    this._lastHeight    = undefined;

    // Private objects
    this.resizeobserver = new ResizeObserver( ( entries ) => {
      if ( ! Array.isArray(entries) || ! entries.length )
        return;

      if ( ! this._moving && ! this._touching ) {

        // Prevent needless resizes
        if ( this._lastWidth == entries[0].contentBoxSize[0].inlineSize &&
          this._lastHeight == entries[0].contentBoxSize[0].blockSize )
          return;

        window.requestAnimationFrame(() => {
          this._lastWidth = entries[0].contentBoxSize[0].inlineSize;
          this._lastHeight = entries[0].contentBoxSize[0].blockSize;
          this._setupKids()
        });
      }
    } );

    // Support touch events
    this.addEventListener( 'touchstart', this._touchStart, { passive: true } );
    this.addEventListener( 'touchend', this._touchEnd, { passive: true } );
    this.addEventListener( 'touchmove', this._touchMove, { passive: true } );
    this.addEventListener( 'touchcancel', this._touchCancel, { passive: true } );
  }
  static get _elementName() { return 'sc-carousel' }
  static get properties() {
    return Object.assign( {}, super.properties, {

      wrap:         { type: Boolean, def: false, desc: "If the carousel wraps from last-to-first and vice versa" },
      animatetime:  { type: Number, def: undefined, desc: "Transition animation duration, inherited from CSS variable --carousel-animation-duration if not specified" },
      fill:         { type: Boolean, def: false, desc: "If the control should be sized to the control's parent, otherwise sizes to content" },
      willscroll:   { type: Boolean, def: false, desc: "Assume content will scroll, try to avoid reflow of content" },
      panelalign:   { type: String, def: "center", desc: "Align panels smaller than content, either center, start or end" },
      controls:     { type: String, def: "shadow", desc: "The style of user controls, one of shadow, circles, none" },
      notouch:      { type: Boolean, def: false, desc: "Do not respond to touch events" },

      _count:       { type: Number, attribute: false },
      _position:    { type: Number, attribute: false },
      _reveal:      { type: Boolean, attribute: false },
      _maxW:        { type: Number, attribute: false },
      _maxH:        { type: Number, attribute: false },
    } );
  }
  static get styles() {
    return super.styles.concat( [ carouselStyles ] );
  }


  /***************
   * Templates
   **************/

  render() {
    return this.builder`
      <div
          id="sc-carousel-container"
          class="sc-carousel-container"
          @mouseenter=${this._revealControls}
          @mouseleave=${this._hideControls}
          ?fill=${this.fill}
        >
        <div
            id="sc-carousel-panels"
            class="sc-carousel-panels"
            panelalign=${this.panelalign}
            ?willscroll=${this.willscroll}
            ?fill=${this.fill}
          >
          <slot @slotchange=${this._slotChanged}></slot>
        </div>
        <div class="sc-carousel-controls">
          <slot name="controls">
            ${this.controlsTemplate()}
          </slot>
        </div>
      </div>
    `;
  }

  controlsTemplate() {
    switch ( this.controls ) {
      case "shadow":
        return this.shadowControlsTemplate();
      case "circles":
        return this.circleControlsTemplate();
      case "none":
        return this.builder``;
    }
  }

  circleControlsTemplate() {
    return this.builder`
      <div class="sc-carousel-circles" ?reveal=${this._reveal}>
        <div class="sc-carousel-circles-previous" ?show=${this.canPrevious()} @click=${()=>{this.previous()}}>
          <div class="arrow-previous"></div>
        </div>
        <div class="sc-carousel-circles-next" ?show=${this.canNext()} @click=${()=>{this.next()}}>
          <div class="arrow-next"></div>
        </div>
        <div class="sc-carousel-circles-place">
          ${Array(this._count).fill().map( (_0,i) => {
            return this.builder`
              <div
                  class="sc-carousel-circles-place-dot"
                  @click=${this.gotoPanel}
                  data-panel=${i}
                  ?selected=${i == this._position}
              ></div>
            `
          } ) }
        </div>
      </div>
    `;
  }

  shadowControlsTemplate() {
    return this.builder`
      <div class="sc-carousel-shadow" ?reveal=${this._reveal}>
        <div class="sc-carousel-shadow-button-previous" ?show=${this.canPrevious()} @click=${()=>{this.previous()}}>
          <div class="arrow-previous"></div>
        </div>
        <div class="sc-carousel-shadow-button-next" ?show=${this.canNext()} @click=${()=>{this.next()}}>
          <div class="arrow-next"></div>
        </div>
      </div>
    `
  }


  /***************
   * lit methods
   **************/

  update( props ) {
    super.update( props );
    props.forEach( (oldv, name) => {
      if ( name == "_position" && this._position !== oldv ) {
        let ev = new CustomEvent( onPosChanged, { bubbles: false, cancelable: false, detail: { from: props.get( '_position' ), to: this._position } } );
        this.dispatchEvent( ev );
      }
      if ( name == "fill" && ! this.fill && oldv ) {
        this._setupKids();
      }
    } )
  }


  willUpdate( changes ) {
    if ( ! this.hasUpdated || changes.has( 'animatetime' ) )
      this._inheritAnimateTime();
  }



  /***************
   * own methods
   **************/

  /**
   * Get the current zero-indexed position
   * @property {Number} position
   */
  get position() {
    return this._position;
  }

  /**
   * Handle the touchstart event
   * @private
   */
  _touchStart( ev ) {
    if ( this._moving )
      return;
    if ( this.notouch )
      return;
    this._touching = true;
    this._curX = ev.touches[0].clientX;
    this._curY = ev.touches[0].clientY;
  }

  /**
   * Handle the touchcancel event
   * @private
   */
  _touchCancel( _0 ) {
    if ( this._touching ) {
      this._touching = false;
      this._nudging = false;
      this._moveTo = 0;
      this._next = undefined;
    }
  }

  /**
   * Handle the touchmove event
   * @private
   */
  _touchMove( ev ) {
    let nowX, nowY, diffX, diffY, direction;
    if ( ! this._touching )
      return;
    nowX = ev.touches[0].clientX;
    nowY = ev.touches[0].clientY;
    diffX = Math.abs( this._curX - nowX );
    diffY = Math.abs( this._curY - nowY );
    // Prevent small movements doing anything
    if ( diffX <= 10 && diffY <= 10 )
      return;
    // End touching, seems to be scrolling
    if ( diffY > diffX )
      return this._touchEnd();
    // Attempt moving
    direction = ( ( this._curX - nowX ) < 0 ) ? 1 : -1;

    // If unable to move
    if ( direction > 0 && ! this.canPrevious() )
      return this._touchEnd();
    else if ( direction < 0 && ! this.canNext() )
      return this._touchEnd();

    this._nudge( direction, diffX );
  }


  /**
   * Handle the touchend event
   * @private
   */
  _touchEnd( _0 ) {
    const currentIdx = this._position;
    const current = this._kids[ currentIdx ];
    const next = this._next;
    if ( ! this._touching || this._position == undefined )
      return;

    // Change position as needed
    if ( this._moveTo == 0 ) {
      // Snapping back, not moving.
      if ( next ) {
        next.style.setProperty( 'position', 'absolute' );
        next.style.setProperty( 'transform', `translate( ${this._maxW * -1}, 0px )` );
      }
      current.style.setProperty( 'position', 'static' );
      current.style.setProperty( 'transform', 'transform: translate( 0px, 0px )' );
    }
    else if ( this._moveTo < 0 ) /* next */
      this.next();
    else
      this.previous();

    this._moveTo = 0;
    this._touching = false;
    this._nudging = false;
    this._next = undefined;
  }


  /**
   * Nudge a panel one direction or the other
   * @private
   */
  _nudge( direction, amount ) {
    const factor = 0.75;
    const threshold = 0.25;
    const pos = direction * ( amount * factor );
    let curPanel, nextIdx, nextPos, nextPanel;
    if ( this._position == undefined )
      return;

    curPanel = this._kids[ this._position ];
    nextIdx = this._position + ( direction * -1 );
    if ( nextIdx < 0 )
      nextIdx = this._kids.length - 1;
    else if ( nextIdx >= this._kids.length )
      nextIdx = 0;
    nextPanel = this._kids[ nextIdx ];

    if ( ! nextPanel )
      throw new Error( "Unable to find panel for nudging" );

    /* Place the next pane for nudging */
    if ( ! this._nudging ) {
      this._nudging = true
      this._next = nextPanel;
      this._prepareNudgePos( direction );
      this._preparePanels( curPanel, nextPanel);
      this._direction = direction;
    }
    else if ( this._direction !== direction ) {
      // Change of direction
      this._next.style.setProperty( 'position', 'absolute' );
      this._next = nextPanel;
      this._prepareNudgePos( direction );
      this._preparePanels( curPanel, nextPanel);
      this._direction = direction;
    }

    if ( Math.abs( amount ) > ( this._maxW * threshold ) )
      this._moveTo = direction;
    else
      this._moveTo = 0;

    curPanel.style.setProperty( 'transform', `translate( ${pos}px, 0px )` );
    nextPos = (this._maxW * direction * -1) + pos;
    nextPanel.style.setProperty( 'transform', `transform( ${nextPos}px, 0px )` );
  }

  /**
   * Place the next panel ready for animation
   * @private
   */
  _prepareNudgePos( direction ) {
    let start = 0;
    if ( direction < 0 )
      start = this._maxW;
    else
      start = this._maxW * -1;
    this._next.style.setProperty( 'transform', `translate( ${start}px, 0px )` );
  }

  /**
   * Check if we can move to a previous panel
   * @returns {Boolean}
   */
  canPrevious() {
    if ( this._position == 0 && ! this.wrap )
      return false;
    else
      return true;
  }

  /**
   * Check if we can move to a next panel
   * @returns {Boolean}
   */
  canNext() {
    if ( this._position == ( this._count - 1 ) && ! this.wrap )
      return false;
    else
      return true;
  }

  _revealControls( _0 ) {
    this._reveal = true;
  }

  _hideControls( _0 ) {
    this._reveal = false;
  }

  _slotChanged( _0 ) {
    this._setupKids();
  }


  /**
   * Setup panels for display
   * @private
   */
  _setupKids() {
    const container = this.shadowRoot.getElementById('sc-carousel-container' );
    const panelwrap = this.shadowRoot.getElementById( 'sc-carousel-panels' );
    const slot = panelwrap.firstElementChild;
    const kids = slot.assignedNodes().filter(n => n.nodeType === Node.ELEMENT_NODE);
    let kidWidth, kidHeight, kidBounds, maxH, maxW, panelsbounds, panelsheight, panelswidth;

    // Not while in transition
    if ( this._moving || this._touching ) {
      window.requestAnimationFrame(() => {
        this._setupKids();
      });
      return;
    }

    this._kids = kids;
    this._count = kids.length;

    panelsbounds = panelwrap.getBoundingClientRect();
    panelsheight = panelsbounds.height;
    panelswidth = panelsbounds.width;

    // Get the fill size or setup for detection
    if ( this.fill ) {
      this._maxW = panelswidth;
      this._maxH = panelsheight;
    }
    else {
      // Automatic size
      container.style.opacity = 0;
      container.style.visibility = "hidden";
      container.style.width = "initial";
      container.style.height = "initial";
      maxH = 0;
      maxW = 0;
    }

    // Find the largest child
    for ( const kid of kids ) {
      kid.style.setProperty( 'opacity', 1 );
      kid.style.setProperty( 'visibility', 'visible' );
      kid.style.setProperty( 'position', 'initial' );
      kid.style.removeProperty( 'display' );

      kidBounds = kid.getBoundingClientRect();
      kidWidth = Math.floor( kidBounds.width );
      kidHeight = Math.floor( kidBounds.height );

      // Get maximum kid size if size unrestricted
      if ( kidHeight > maxH ) maxH = kidHeight;
      if ( kidWidth > maxW ) maxW = kidWidth;

      // Hide all but the needed panel
      if ( this._position != undefined && kid != this._kids[ this._position ] ) {
        kid.style.setProperty( 'position', 'absolute' );
        kid.style.setProperty( 'opacity', 0 );
        kid.style.setProperty( 'visibility', 'hidden' );
        kid.style.setProperty( 'transform', `translate( ${kidWidth * -5}px, 0px )` );
      }

      if ( ! kid.dataset.observed ) {
        this.resizeobserver.observe( kid );
        kid.dataset.observed = 1;
      }
    }

    // Set height from kids as needed
    if ( ! this.fill ) {
      this._maxH = maxH;
      this._maxW = maxW;
    }
    else if ( this._maxH == 0 )
      this._maxH = maxH;

    // With size constrained, ensure center alignment starts at baseline for larger panels
    if ( this.fill ) {
      for ( const kid of kids ) {
        kidBounds = kid.getBoundingClientRect();
        kidHeight = kidBounds.height;
        // Fix display for large kids
        if ( kidHeight > panelsheight )
          kid.style.setProperty( 'align-self', 'baseline' );
      }
    }
    else if ( maxW > 0 && maxH > 0 ) {
      container.style.width = maxW + "px";
      container.style.height = maxH + "px";
    }

    container.style.setProperty( 'visibility', 'visible' );
    container.style.setProperty( 'opacity', 1 );
  }


  /**
   * Inherit animation time from CSS
   * @private
   */
  _inheritAnimateTime() {
    let styles, animTime;
    if ( this.animatetime === undefined ) {
      styles = window.getComputedStyle( this );
      animTime = styles.getPropertyValue( '--carousel-animation-duration' );
      if ( animTime != "" && animTime != undefined )
        this.animatetime = parseFloat( animTime );
    }
  }


  /**
   * Show a specific panel
   * @param {Number} panelIndex         - The zero-based index of which paanel to show
   * @param {Boolean} [instant=false]   - If the carousel should move instantly
   */
  show( panelIndex, instant=false ) {
    if ( instant && panelIndex !== this._position )
      this._skipanim = true;
    if ( panelIndex < this._position )
      return this.previous( panelIndex );
    else if ( panelIndex > this._position )
      return this.next( panelIndex );
  }


  /**
   * Move to a specific via panel selection element
   */
  gotoPanel( ev ) {
    const wantIndex = parseFloat( ev.currentTarget.dataset.panel );
    if ( isNaN( wantIndex ) )
      return;
    if ( wantIndex == this._position )
      return;
    this.show( wantIndex );
  }


  /**
   * Prepare next panel
   */
  _preparePanels( current, next ) {
    const panelwrap = this.shadowRoot.querySelector( '#sc-carousel-panels' );
    let curWidth;

    // Constrain current panel width to prevent reflow
    curWidth = current.style.getPropertyValue( 'width' );
    if ( ! isNaN( parseInt( curWidth ) ) )
      current.dataset.origWidth = curWidth
    else
      current.dataset.noWidth = 1;
    current.style.setProperty( 'width', current.clientWidth + "px" );
    current.style.setProperty( 'position', 'absolute' );

    // Prepare positioning and visibility
    next.style.setProperty( 'position', 'static' );
    next.style.setProperty( 'visibility', 'visible' );
    next.style.setProperty( 'opacity', 1 );

    // Hide scrollbars
    panelwrap.classList.add( 'hidescroll' );
  }

  /**
   * Adjust panel display as needed after a movement
   * @private
   */
  _adjustPanels( oldpanel, newpanel ) {
    const panelwrap = this.shadowRoot.querySelector( '#sc-carousel-panels' );

    // Enable scrolling if panel is too large
    if ( this.fill ) {
      newpanel.style.removeProperty( 'width' );
      if ( newpanel.scrollHeight > panelwrap.scrollHeight || newpanel.scrollWidth > panelwrap.clientWidth ) {
        // Ensure panel wrapper overflows
        if ( ! this.willscroll ) panelwrap.style.setProperty( 'overflow', 'auto' );
      }
    }
    else if ( newpanel.scrollHeight > this._maxH || newpanel.scrollWidth > this._maxW ) {
      // Enlarge carousel
      this._maxH = newpanel.scrollHeight;
      this._maxW = newpanel.scrollWidth;
    }

    // Restore any width
    if ( oldpanel.dataset.origWidth ) {
      oldpanel.style.setProperty( 'width', oldpanel.dataset.origWidth );
      delete oldpanel.dataset.origWidth;
    }
    else if ( oldpanel.dataset.noWidth )
      oldpanel.style.removeProperty( 'width' );

    oldpanel.style.setProperty( 'transform', `translate( ${window.outerWidth * -2}px, 0px )` );

    // Show scrollbars
    panelwrap.classList.remove( 'hidescroll' );
  }


  /**
   * Move to the previous panel
   * @async
   * @param {Number} [panelIndex] - Optional panel index to move backward to
   */
  async previous( panelIndex=undefined ) {
    const currentIdx = this._position;
    const current = this._kids[ currentIdx ];
    const anims = [];
    let next, nextIdx, nextx, moveev;

    if ( ! this.canPrevious() || this._moving )
      return false;

    // Get next panel
    if ( panelIndex !== undefined )
      nextIdx = panelIndex;
    else
      nextIdx = currentIdx - 1;
    if ( nextIdx < 0 )
      nextIdx = this._count + nextIdx;

    moveev = new CustomEvent( onPosChange, {
      bubbles: false, cancelable: true, detail: { from: this._position, to: nextIdx }
    } );
    if ( ! this.dispatchEvent( moveev ) )
      return;

    next = this._kids[ nextIdx ];

    // Start moving
    this._moving = true;
    if ( ! this._skipanim && this.animatetime > 0 ) {
      // Take current position of next panel, or start off screen?
      nextx = ( this._touching ) ? undefined : this._maxW * -1;

       // Setup movements
      if ( current )
        anims.push( [ current, { 'x': [ null, this._maxW ] }, { at: 0 } ] );
      anims.push( [ next, { 'x': [ nextx, 0 ] }, { at: 0 } ] );

      // Setup panel states
      this._preparePanels( current, next );

      // Create timeline
      await timeline( anims, {
        duration: this.animatetime,
        defaultOptions: {
          'ease': 'ease-in-out'
        }
      } ).finished;

      // Finish positioning and state
      if ( current )
        current.style.setProperty( 'transform', `translate( ${window.outerWidth * -2}px, 0px )` );
      this._adjustPanels( current, next );
      this._moving = false;
    }
    else { /* no animation */
      if ( current ) {
        current.style.setProperty( 'position', 'absolute' );
        current.style.setProperty( 'visibility', 'hidden' );
        current.style.setProperty( 'opacity', 0 );
        current.style.setProperty( 'transform', `translate( ${window.outerWidth * -2}px, 0px )` );
      }

      next.style.setProperty( 'display', 'none' );
      next.style.setProperty( 'position', 'static' );
      next.style.setProperty( 'visibility', 'visible' );
      next.style.setProperty( 'opacity', 1 );
      next.style.setProperty( 'transform', 'translate( 0px, 0px )' );

      // Given observers a chance to see intersection and visibility
      setTimeout( () => {
        next.style.removeProperty( 'display' );
      } );

      this._moving = false;
      this._skipanim = false;
    }

    this._position = nextIdx;
  }


  /**
   * Move to the next panel
   * @async
   * @param {Number} [panelIndex] - Optional panel index to move forward to
   */
  async next( panelIndex=undefined ) {
    const currentIdx = this._position;
    const current = this._kids[ currentIdx ];
    const anims = [];
    let next, nextIdx, nextx, moveev;

    if ( ! this.canNext() || this._moving )
      return false;

    if ( panelIndex != undefined )
      nextIdx = panelIndex;
    else
      nextIdx = currentIdx + 1;
    if ( nextIdx > ( this._count - 1 ) )
      nextIdx = 0;

    moveev = new CustomEvent( onPosChange, {
      bubbles: false, cancelable: true, detail: { from: this._position, to: nextIdx }
    } );
    if ( ! this.dispatchEvent( moveev ) )
      return;

    next = this._kids[ nextIdx ];

    // Start moving
    this._moving = true;
    if ( ! this._skipanim && this.animatetime > 0 ) {
      // Take current position of next panel, or start off screen?
      nextx = ( this._touching ) ? undefined : this._maxW;

       // Setup movements
      if ( current )
        anims.push( [ current, { 'x': [ null, this._maxW * -1 ] }, { at: 0 } ] );
      anims.push( [ next, { 'x': [ nextx, 0 ] }, { at: 0 } ] );

      // Setup panel states
      this._preparePanels( current, next );

      // Create timeline
      await timeline( anims, {
        duration: this.animatetime,
        defaultOptions: {
          'ease': 'ease-in-out'
        }
      } ).finished;

      // Finish positioning and state
      if ( current )
        current.style.setProperty( 'transform', `translate( ${window.outerWidth * -2}px, 0px )` );
      this._adjustPanels( current, next );
      this._moving = false;
    }
    else { /* no animation */
      if ( current ) {
        current.style.setProperty( 'position', 'absolute' );
        current.style.setProperty( 'visibility', 'hidden' );
        current.style.setProperty( 'opacity', 0 );
        current.style.setProperty( 'transform', `translate( ${window.outerWidth * -2}px, 0px )` );
      }

      next.style.setProperty( 'display', 'none' );
      next.style.setProperty( 'position', 'static' );
      next.style.setProperty( 'visibility', 'visible' );
      next.style.setProperty( 'opacity', 1 );
      next.style.setProperty( 'transform', 'translate( 0px, 0px )' );

      // Given observers a chance to see intersection and visibility
      setTimeout( () => {
        next.style.removeProperty( 'display' );
      } );

      this._moving = false;
      this._skipanim = false;
    }

    this._position = nextIdx;

  }
}


