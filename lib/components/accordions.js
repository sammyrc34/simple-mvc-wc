

import { ResizeObserver as Polyfill } from '@juggle/resize-observer';

import { animate, timeline } from 'motion';

import { SimpleComponent } from '../index.js';

import accordionStyles from './accordions.css';
import accordionGroupStyles from './accordions2.css';


const ResizeObserver = window.ResizeObserver || Polyfill;


/**
 * A bubbling cancelable event emitted before closing an accordion.
 * @event SimpleAccordion#before-close
 */
const onBeforeClose = 'before-close';

/**
 * A bubbling cancelable event emitted before opening an accordion.
 * @event SimpleAccordion#before-close
 */
const onBeforeOpen = 'before-open';

/**
 * A bubbling event emitted after closing an accordion.
 * @event SimpleAccordion#before-close
 */
const onAfterClose = 'after-close';

/**
 * A bubbling event emitted after opening an accordion.
 * @event SimpleAccordion#before-close
 */
const onAfterOpen = 'after-open';


export class SimpleAccordionGroup extends SimpleComponent {

  constructor() {
    super();

    // Stateful properties
    this._skipanimate = false;
    this._kids = [];
    this._sizes = undefined;
    this._resizing = false;
    this._resizehandle = undefined;

    // Resize handler
    this._resizeobserver = new ResizeObserver( () => {
      this._slotChanged();
    } );

  }
  static get _elementName() { return 'sc-accordion-group' }
  static get properties() {
    return Object.assign( {}, super.properties, {

      'multiple':         { type: Boolean, def: false, desc: "Allow multiple accordions to be open" },
      'requireone':       { type: Boolean, def: false, desc: "Require one accordion to be open" },
      'bardisable':       { type: Boolean, def: false, desc: "Disable accordion bar user interaction" },
      'fillheight':       { type: Boolean, def: false,
        desc: "If this accordion group should fill the container's <u>constrained</u> height" },
      'sizetoid':         { type: String, def: undefined,
        desc: "Optional ID for a different element to size to, defaulting to parent element" },

      '_needresize':      { type: Boolean, def: false },
    } );
  }
  static get styles() {
    return super.styles.concat( [ accordionGroupStyles ] );
  }
  render() {
    return this.builder`
      <div class="sc-accordion-group" id="sc-accordion-group" ?fillheight=${this.fillheight} >
        <slot @slotchange=${this._slotChanged}></slot>
      </div>
    `;
  }


  /**
   * Handle changed accordion items
   */
  _slotChanged() {
    const container = this.shadowRoot.getElementById('sc-accordion-group' );
    const slot = container.firstElementChild;
    const kids = slot.assignedNodes().filter(n => n.nodeType === Node.ELEMENT_NODE);
    this._kids = kids;

    if ( this._resizing )
      return;

    // Cascade options
    for ( const kid of kids ) {
      if ( this.fillheight )
        kid.fillheight = true;
      if ( this.bardisable )
        kid.bardisable = true;
      kid.skipanimate = true;
    }

    // Request a resize
    this._skipanimate = true;
    this._needresize = true;
  }


  /**
   * Respond to browser resizing
   */
  connectedCallback() {
    super.connectedCallback();
    this._resizehandle = () => this._slotChanged();
    window.addEventListener( 'resize', this._resizehandle )
    if ( this._resizeobserver )
      this._resizeobserver.observe( this.parentElement );
  }


  /**
   * Remove browser resizing listener
   */
  disconnectedCallback() {
    super.disconnectedCallback();
    if ( this._resizehandle )
      window.removeEventListener( 'resize', this._resizehandle );
    this._resizehandle = undefined;
    if ( this._resizeobserver && this.parentElement )
      this._resizeobserver.unobserve( this.parentElement );
  }



  /***********************
   * Lit methods
   ***********************/

  /**
   * Handle property updates
   */
  updated( changes ) {
    if ( changes.has( '_needresize' ) && this._needresize ) {
      this._resize();
      this._needresize = false;
    }
  }


  /**
   * Handle first render
   */
  async firstUpdated() {
    await this.updateComplete;

    // Ensure kids are fetched
    if ( ! this._kids.length )
      this._slotChanged();
  }

  /***********************
   * Our methods
   ***********************/


  /**
   * Check if a child accordion can attempt to close
   */
  canClose( accordion ) {
    let opencount = 0;
    if ( ! this.requireone )
      return true;
    for ( const kid of this._kids ) {
      if ( kid == accordion )
        continue;
      if ( kid.open )
        opencount++;
    }
    if ( opencount < 1 )
      return false;
    return true;
  }


  /**
   * Indicate a desire to open or close an accordion by index
   * @param {Number} index      - Accordion index for opening or closing
   * @param {Boolean} open      - Open state sought for accordion
   */
  openCloseByIndex( index, open ) {
    const accordion = this._kids[ index ];

    // Ensure accordion known
    if ( ! accordion )
      return;

    return this.openClose( accordion, open );
  }



  /**
   * Indicate a desire to open or close an accordion
   * @param {HTMLElement} accordion   - Accordion requesting a change
   * @param {Boolean} open            - Open state sought for accordion
   */
  openClose( accordion, open ) {
    let ok, closed = [], opencount = 0;

    // Count open kids not including the source
    for ( const kid of this._kids ) {
      if ( kid.open && kid !== accordion )
        opencount++;
    }

    // Prevent closure
    if ( ! open && ! opencount && this.requireone )
      return;

    // Prevent a toggle while one is happening
    if ( this._resizing )
      return;

    // Opening one and need to close other accordions
    if ( ( ! this.multiple && open ) || ( this.requireone && open && opencount ) ) {

      // Check open accordions can close
      for ( const kid of this._kids ) {
        if ( kid.open && kid !== accordion ) {
          ok = kid.dispatchEvent( new CustomEvent( onBeforeClose, { bubbles: true, cancelable: true } ) );
          if ( ! ok )
            return;
        }
      }

      // Check closed accordion can open
      ok = accordion.dispatchEvent( new CustomEvent( onBeforeOpen, { bubbles: true, cancelable: true } ) );
      if ( ! ok )
        return;

      // Close
      for ( const kid of this._kids ) {
        if ( kid.open && kid !== accordion ) {
          closed.push( kid );
          kid.groupopen = false;
        }
      }

      // Open
      accordion.groupopen = open;
      this._needresize = true;
      accordion.dispatchEvent( new CustomEvent( onAfterOpen, { bubbles: true } ) );

      for ( let kid of closed )
        kid.dispatchEvent( new CustomEvent( onAfterClose, { bubbles: true } ) );

      return;
    }
    else {
      // Just this accordion closing or opening
      ok = accordion.dispatchEvent( new CustomEvent( open ? onBeforeOpen : onBeforeClose,
        { bubbles: true, cancelable: true } ) );
      if ( ! ok )
        return;

      accordion.groupopen = open;
      accordion.dispatchEvent( new CustomEvent( open ? onAfterOpen : onAfterClose, { bubbles: true } ) );
      this._needresize = true;
    }
  }


  /**
   * Calculate the sizes of all accordions
   * @private
   */
  _calculateSizes( ) {
    const firstacc = this._kids[0];
    let autocount = 0, margin, barheight, minheight = 0, maxheight = 0, availheight = 0, equalheight = 0, sizes = [], sizeelem;

    // Nothing to calculate?
    if ( ! firstacc )
      return;

    // What is accordion bar height?
    barheight = firstacc._getBarHeight();
    margin = firstacc._getBarMargin();

    // What are we sizing to?
    if ( this.sizetoid )
      sizeelem = document.getElementById( this.sizetoid );
    else
      sizeelem = this.parentElement;

    // What's available?
    maxheight = sizeelem.clientHeight;
    maxheight -= this._kids.length * ( barheight + margin ) + margin;

    availheight = maxheight;

    // Get minimum height
    for ( let kid of this._kids ) {
      if ( kid.open )
        minheight += kid._minheight;
      if ( kid.fillheight && kid.open )
        autocount++;
    }
    availheight -= minheight;

    // Don't have enough space for open minimums? Everything gets a minimum only and can scroll.
    if ( availheight < 0 ) {
      availheight = 0;
      for ( let kid of this._kids ) {
        if ( kid.open ) {
          if ( kid._minheight > maxheight )
            sizes.push( maxheight )
          sizes.push( kid._minheight );
        }
        else
          sizes.push( null );
      }
      this._sizes = sizes;
      return;
    }

    // Got enough for minimums
    equalheight = availheight / autocount;

    // Set everything to minimums first, and filling accordions get extra
    for ( let idx = 0; idx < this._kids.length; idx++ ) {
      let kid = this._kids[ idx ];
      sizes[ idx ] = kid._minheight;
      if ( kid.fillheight )
        sizes[ idx ] += equalheight;
    }

    this._sizes = sizes;
  }


  /**
   * Get the height of the accordion
   * @protected
   */
  _getFillHeight( kid ) {
    for( let i = 0; i < this._kids.length; i++ ) {
      if ( this._kids[ i ] == kid ) {
        return this._sizes[ i ];
      }
    }
  }


  /**
   * Request a resize of the accordion group
   * @protected
   */
  _requestResize() {
    this._needresize = true;
  }


  /**
   * Resize the group of accordions
   * @private
   * @async
   */
  async _resize() {
    let seqs = [], callbacks = [], callback, opencount = 0;

    // Ensure at least one is open if required
    if ( this.requireone ) {
      for ( const kid of this._kids ) {
        if ( kid.open )
          opencount++;
      }
      if ( ! opencount && this._kids.length ) {
        this._kids[ 0 ].groupopen = true;
      }
    }

    // Animation about to begin
    this._resizing = true;

    // Calculate sizes for each accordion
    this._calculateSizes();
    if ( ! this._sizes ) {
      this._resizing = false;
      return;
    }

    // Collect animations to run
    for ( const kid of this._kids ) {
      kid.skipanimate = this._skipanimate;
      callback = kid._groupResize( seqs );
      if ( callback )
        callbacks.push( callback );
    }

    await timeline( seqs, {
      defaultOptions: {
        'ease': 'ease-in-out'
      }
    } ).finished;

    // Run callbacks
    for ( let cb of callbacks )
      cb();

    this._resizing = false;
    this._skipanimate = false;
  }

}


export class SimpleAccordion extends SimpleComponent {

  /**
   * @emits {before-close} Before an accordion is closed
   * @emits {before-open} Before an accordion is opened
   * @emits {after-close} After an accordion is closed
   * @emits {after-open} After an accordion in opened
   */

  constructor() {
    super();

    // Private, stateful
    this._minheight   = undefined;
    this._lastheight  = undefined;
    this._grouped     = false;
    this._animate     = false;
    this._resizing    = false;
  }
  static get _elementName() { return 'sc-accordion' }
  static get properties() {
    return Object.assign( {}, super.properties, {

      label:        { type: String, def: "", desc: "Label for the accordion" },
      barposition:  { type: String, def: "top", desc: "Accordion bar position, top or bottom" },
      bardisable:   { type: Boolean, def: false, desc: "Disable accordion bar user interaction" },
      animatetime:  { type: Number, def: undefined, desc: "Optional animation duration in seconds, defaulting to CSS defined value" },
      showoverflow: { type: Boolean, def: false, desc: "Enable workaround to enable overflow being visible" },
      noscroll:     { type: Boolean, def: false, desc: "Disable scrolling within the accordion" },

      open:         { type: Boolean, reflect: true, def: false, desc: "If this accordion is currently open" },
      show:         { type: Boolean, reflect: true, def: false, desc: "If the accordion content is currently visible" },
      fillheight:   { type: Boolean, reflect: true, def: false, desc: "If this accordion should fill available height" },
      minheight:    { type: Number, def: 0, desc: "The minimum height of this accordion when open, 0 meaning detected minimum" },

      skipanimate:  { type: Boolean, def: false, desc: "Do not animate opening or closing of this accordsion" },

      groupopen:    { type: Boolean, def: false },


      _noclose:     { type: Boolean, def: false },
      _noopen:      { type: Boolean, def: false },
      _heightfix:   { type: Boolean, def: false }
    } );
  }
  static get styles() {
    return [
      super.styles, // abstracted styles
      accordionStyles
    ];
  }


  /********************
   * Rendering methods
   ********************/

  render() {
    return this.builder`
      <div
          class="sc-accordion"
          position=${this.barposition}
          ?fillheight=${this.fillheight}
          @mouseenter=${this._mouseOver}
        >
        ${this.barTop()}
        <div class="sc-accordion-content"
            id="sc-accordion-content"
            ?show=${this.show}
            ?overflow=${this.showoverflow}
            ?noscroll=${this.showscroll}
            ?fillheight=${this.fillheight}
            ?animate=${this._animate}
            ?relpos=${this._heightfix}
        >
          <div class="sc-accordion-content-wrap">
            <slot @slotchange=${this._slotChanged}></slot>
          </div>
        </div>
        ${this.barBottom()}
      </div>
    `
  }

  barTop() {
    if ( this.barposition == "top" ) {
      return this.builder`
        <div class="sc-accordion-bar"
            id="sc-accordion-bar"
            ?open=${this.open}
            ?show=${this.show}
            @click=${this.toggle}
            ?noclose=${this._noclose}
            ?noopen=${this._noopen}
        >
          <slot name="bar">
            <div class="sc-accordion-bar-content">
              <span>${this.label}</span>
              <i class="arrow ${this.open ? 'up' : 'down' }"></i>
            </div>
          </slot>
        </div>
      `
    }
  }

  barBottom() {
    if ( this.barposition == "bottom" ) {
      return this.builder`
        <div class="sc-accordion-bar"
            id="sc-accordion-bar"
            ?open=${this.open}
            ?show=${this.show}
            @click=${this.toggle}
            ?noclose=${this._noclose}
            ?noopen=${this._noopen}
        >
          <slot name="bar">
            <div class="sc-accordion-bar-content">
              <span>${this.label}</span>
              <i class="arrow ${this.open ? 'down' : 'up' }"></i>
            </div>
          </slot>
        </div>
      `
    }
  }


  /************************
   * LIT methods
   ************************/

  /**
   * Check for needed side effects
   */
  willUpdate( changes ) {
    let group;
    super.willUpdate( changes );

    // Ensure we check the slotted content
    if ( ! this.hasUpdated )
      this._slotChanged();

    // Check if this accordion is in a group
    if ( ! this.hasUpdated && this.parentElement.tagName.toUpperCase() == "SC-ACCORDION-GROUP" )
      this._grouped = true;

    // Set to skip animation on first render
    if ( changes.has( 'open' ) && this.open !== changes.get( 'open' ) && ! this._grouped ) {
      // Newly displayed, skip animation
      if ( changes.get( 'open' ) == undefined ) {
        this.skipanimate = true;
      }
    }

    if ( changes.has( 'minheight' ) && this.minheight !== changes.get( 'minheight' ) && changes.get( 'minheight' ) !== undefined )
      this._slotChanged();

    // Get animation duration
    if ( ! this.hasUpdated || changes.has( 'animatetime' ) )
      this._inheritAnimateTime();

    // Need to ask the group to open us instead?
    if ( changes.has( 'open' ) && this.open !== changes.get( 'open' ) && changes.get( 'open' ) !== undefined && this._grouped ) {

      // Revert before asking the group to open or close us
      this.open = changes.get( 'open' );
      this.groupopen = this.open;

      group = this.parentElement;
      group.openClose( this, !this.open );
    }

    if ( changes.has( 'groupopen' ) && changes.get( 'groupopen' ) !== undefined ) {
      // Group has changed our state
      this.open = this.groupopen;
    }
  }


  firstUpdated() {
    // Ensure initial open state is reflected to group open state
    if ( this.open )
      this.groupopen = this.open;
  }


  updated( changes ) {
    super.updated( changes );

    // Resize if open has changed while not in a group
    if ( changes.has( 'open' ) && this.open != changes.get( 'open' ) && ! this._grouped )
      this._resize();
  }

  /***********************
   * Our methods
   ***********************/

  /**
   * Helper method to get the gap between the bar and the top
   * @protected
   */
  _getBarMargin() {
    const barelem = this.shadowRoot.firstElementChild.firstElementChild;
    return barelem.getBoundingClientRect().y - this.getBoundingClientRect().y;
  }


  /**
   * Inherit animation time from CSS as needed
   * @private
   */
  async _inheritAnimateTime() {
    let animTime, styles;
    // Wait for loading to finish
    await this.updateComplete;
    // Take animation time from CSS unless specified
    if ( this.animatetime === undefined ) {
      styles = window.getComputedStyle( this );
      animTime = styles.getPropertyValue( '--accordion-animation-duration' );
      if ( animTime != "" && animTime != undefined ) {
        this.animatetime = parseFloat( animTime );
        if ( this.animatetime == 0 )
          this._animate = false;
        else
          this._animate = true;
      }
    }
  }


  /**
   * Handle a mouse over for cursor display
   * @private
   */
  _mouseOver(ev) {
    const group = this.parentElement;
    let close = true;

    // Ask the group if this accordion may close
    if ( this.bardisable )
      close = false;
    else if ( this._grouped )
      close = group.canClose( this );

    this._noclose = !close;
    this._noopen = this.bardisable;
    return ev.preventDefault();
  }


  /**
   * Handle changes to slotted content
   * @private
   */
  async _slotChanged() {
    let container, wrapper;

    // Ensure we've built ourself
    await this.updateComplete;

    container = this.shadowRoot.getElementById( 'sc-accordion-content' );
    wrapper = container.firstElementChild;

    // In all cases get a minimum height
    container.style.setProperty( 'height', 'auto' );
    if ( this.minheight )
      this._minheight = this.minheight;
    else
      this._minheight = wrapper.getBoundingClientRect().height;

    // If the minimum is greater than content size, flag for position adaption
    if ( this.minheight && this.minheight > wrapper.getBoundingClientRect().height )
      this._heightfix = true;

    // Set to initial height
    if ( this.open && ! this._grouped && ! this._resizing )
      this._resize();
    else if ( this._grouped )
      this.parentElement._requestResize();
    else if ( ! this.open && ! this._resizing )
      container.style.setProperty( 'height', '0px' );

  }


  /**
   * Toggle from open to close or vice versa
   * @public
   */
  toggle( ) {
    let ok, wantopen = ! this.open;
    if ( this._grouped ) {
      // Group will intercept and manage this
      this.open = wantopen;
    }
    else {
      ok = this.dispatchEvent( new CustomEvent( wantopen ? onBeforeOpen : onBeforeClose ,
        { bubbles: true, cancelable: true } ) );

      if ( ! ok )
        return;

      this.open = wantopen;

      this.dispatchEvent( new CustomEvent( wantopen ? onAfterOpen : onAfterClose,
        { bubbles: true } ) );
    }
  }


  /**
   * Get the height of the accordion bar in pixels
   * @protected
   * @returns {Number}
   */
  _getBarHeight() {
    const bar = this.shadowRoot.getElementById( 'sc-accordion-bar' );
    return bar.getBoundingClientRect().height;
  }

    /**
   * Resize this accordion with a group timeline
   * @protected
   * @param {Array[]} sequence        - A sequence of animations
   * @returns {Function}              - Callback upon animation completion
   */
  _groupResize( sequence ) {
    const container = this.shadowRoot.getElementById( 'sc-accordion-content' );
    const group = this.parentElement;
    const duration = this.animatetime;
    let wantHeight;

    // Ask for target height if filling
    if ( this.open && this.fillheight )
      wantHeight = group._getFillHeight( this );
    else if ( this.open )
      wantHeight = this._minheight;
    else
      wantHeight = 0;

    // Don't resize if same size
    if ( wantHeight == this._lastheight )
      return;

    // Animating?
    if ( this._animate && ! this.skipanimate ) {
      if ( this.open ) {
        this.show = true;
        if ( ! container.style.getPropertyValue( 'height' ) )
          container.style.setProperty( 'height', '0px' );
      }
      container.style.setProperty( 'overflow', 'hidden' );
      container.style.setProperty( 'pointer-events', 'none');

      sequence.push( [
        container,
        { height: wantHeight + 'px' },
        { duration: duration, at: 0 }
      ] );

      return () => {
        this._lastheight = wantHeight;
        if ( this.open ) {
          container.style.setProperty( 'overflow', this.showoverflow ? "visible" : "auto" );
          container.style.setProperty( 'pointer-events', "auto" );
        }
        else
          this.show = false;
      };
    }
    else {
      // Not animating
      container.style.setProperty( 'height', wantHeight + "px" );

      if ( ! this.open ) {
        container.style.setProperty( 'overflow', 'hidden' );
        this.show = false;
      }
      else {
        container.style.setProperty( 'overflow', this.showoverflow ? "visible" : "auto" );
        this.show = true;
      }

      if ( this.skipanimate )
        this.skipanimate = false;
    }
  }


  /**
   * Resize this accordion as necessary
   * @protected
   */
  async _resize() {
    const container = this.shadowRoot.getElementById( 'sc-accordion-content' );
    let showHeight = 0;
    let group, wrapper;

    // Get any group
    if ( this._grouped )
      group = this.parentElement;

    // Groups handle the resizing
    if ( group )
      return;

    this._resizing = true;

    // If we still don't have a min size, detect it now
    if ( ! this.minheight ) {
      wrapper = container.firstElementChild;
      if ( this.fillheight )
        container.style.setProperty( 'height', 'auto' );
      this.minheight = wrapper.getBoundingClientRect().height;
    }

    if ( this.open )
      showHeight = this.minheight;

    // From closed to open
    if ( this.open ) {
      if ( this._animate && ! this.skipanimate ) {
        this.show = true;

        await animate( container,
          { 'height': showHeight + "px" },
          { 'duration': this.animatetime, easing: 'ease-in-out' } ).finished;

        container.style.setProperty( 'overflow', this.showoverflow ? "visible" : "auto" );
      }
      else {
        this.show = true;
        container.style.setProperty( 'height', showHeight + "px" );
        container.style.setProperty( 'overflow', this.showoverflow ? "visible" : "auto" );
        if ( this.skipanimate )
          this.skipanimate = false;
      }
    }
    else { // From opened to clsoed
      if ( this.show && this._animate && ! this.skipanimate ) {
        container.style.setProperty( 'overflow', 'hidden' );

        await animate( container,
          { height: 0 + "px" },
          { duration: this.animatetime, easing: 'ease-in-out' } ).finished;

        this.show = false;
      }
      else if ( this.show ) {
        this.show = false;
        container.style.setProperty( 'height', '0px' );
        container.style.setProperty( 'overflow', 'hidden' );
        if ( this.skipanimate )
          this.skipanimate = false;
      }
      else if ( ! this.show )
        if ( this.skipanimate )
          this.skipanimate = false;
    }

    this._resizing = false;
  }

}


