/**
 * @element sc-table
 * @attribute {Number} range              - Default range (displayed rows), 0 to disable pagination
 * @property {Number[]} ranges            - List of available range sizes
 * @attribute {Number} pagerange          - Number of pages either side of current page, eg 3 gives 1,2,3 for page 4
 * @attribute {Boolean} showpages         - Show the page selection control
 * @attribute {Boolean} showfooter        - Show the table footer ?? FIXME: containing
 * @attribute {Boolean} showrange         - Show the range selection control
 * @attribute {Boolean} showempty         - Show empty rows to fill up to page range or min rows
 * @attribute {Boolean} showsearch        - Show the table quicksearch control in a toolbar
 * @attribute {String} searchmode=ALL     - Quicksearch mode, AND/OR/ALL, ALL being the search string as is, AND/OR splits terms.
 * @attribute {String} searchtext=Search  - Quicksearch field placeholder text
 * @attribute {Boolean} showfilters       - Always show column filter inputs
 * @attribute {Boolean} scrollable        - Enable scrolling in the table
 * @attribute {Number} minrows=0          - Minimum number of rows to display (with showempty)
 * @attribute {String[]} sortfields       - Optional indexed field(s) to sort initially
 * @attribute {String[]} sorttypes        - Optional indexed list of sort types for fields
 * @attribute {String[]} sortdirections   - Optional indexed list of sort directions for fields
 *
 * @attribute {String} sortdir            - Direction for initial sort, asc or desc
 * @attribute {Boolean} multisort         - Enable sorts of multiple columns
 * @attribute {Boolean} usersortfirst     - Apply user-initiated sorts first
 * @attribute {Boolean} disabled          - Disable the table while set
 * @attribute {Boolean} busy              - Show busy mask while set
 * @attribute {Function} rowClasses       - Function to get a CSS class for a row, called with row record
 *
 * @property {ColumnType[]} fieldDefinitions - Table column definitions
 */
export class SimpleTable extends SelectableListComponent {
    static get _elementName(): string;
    /**
     * Return column property definitions
     */
    static get columnProperties(): {
        label: {
            type: StringConstructor;
            def: any;
            desc: string;
        };
        field: {
            type: StringConstructor;
            def: any;
            desc: string;
        };
        enablehtml: {
            type: BooleanConstructor;
            def: boolean;
            desc: string;
        };
        template: {
            type: FunctionConstructor;
            def: any;
            desc: string;
        };
        sortable: {
            type: BooleanConstructor;
            def: boolean;
            desc: string;
        };
        sorttype: {
            type: StringConstructor;
            def: any;
            desc: string;
        };
        quicksearch: {
            type: BooleanConstructor;
            def: boolean;
            desc: string;
        };
        showfilter: {
            type: BooleanConstructor;
            def: boolean;
            desc: string;
        };
        hidetoggle: {
            type: BooleanConstructor;
            def: boolean;
            desc: string;
        };
        filtertype: {
            type: StringConstructor;
            def: any;
            desc: string;
        };
        filtermode: {
            type: StringConstructor;
            def: string;
            desc: string;
        };
        filtercase: {
            type: BooleanConstructor;
            def: boolean;
            desc: string;
        };
        filtertext: {
            type: StringConstructor;
            def: string;
            desc: string;
        };
        multiselect: {
            type: BooleanConstructor;
            def: boolean;
            desc: string;
        };
        customfilter: {
            type: FunctionConstructor;
            def: any;
            desc: string;
        };
        customfiltertype: {
            type: StringConstructor;
            def: string;
            desc: string;
        };
        lookupmodel: {
            type: StringConstructor;
            def: any;
            desc: string;
        };
        lookupwith: {
            type: StringConstructor;
            def: any;
            desc: string;
        };
        lookupfield: {
            type: StringConstructor;
            def: any;
            desc: string;
        };
        lookupremote: {
            type: StringConstructor;
            def: any;
            desc: string;
        };
    };
    /**
     * Build table column definitions from provided definitions
     * @param {Array<Object>} defs  - The unprocessed column definitions
     * @returns {Array<Object>}     - Processed column definitions with defaults applied
     */
    static toFieldDefintions(defs: Array<any>): Array<any>;
    resizeobserver: Polyfill;
    _bodyheight: number;
    editable: boolean;
    _editing: any;
    _position: number;
    _models: {};
    _quickfilters: any[];
    _filterchange: boolean;
    _fieldfilters: {};
    _fixedfilters: any[];
    _quicksearch: string;
    render(): import("lit-html").TemplateResult<1>;
    toolbarTemplate(): import("lit-html").TemplateResult<1>;
    searchTemplate(): import("lit-html").TemplateResult<1>;
    headerTemplate(): import("lit-html").TemplateResult<1>;
    headerFieldTemplate(column: any): import("lit-html").TemplateResult<1>;
    bodyTemplate(): import("lit-html").TemplateResult<1>;
    rowTemplate(row: any): import("lit-html").TemplateResult<1>;
    filterTemplate(column: any): import("lit-html").TemplateResult<1>;
    cellTemplate(def: any, row: any): import("lit-html").TemplateResult<1>;
    _editableRowTemplate(field: any, val: any): import("lit-html").TemplateResult<1>;
    editableInputTemplate(): import("lit-html").TemplateResult<1>;
    footerTemplate(): import("lit-html").TemplateResult<1>;
    rangeTemplate(): import("lit-html").TemplateResult<1>;
    paginationTemplate(): import("lit-html").TemplateResult<1>;
    pageButtonsTemplate(): import("lit-html").TemplateResult<1>;
    /**
     * Lookup a display value from another model
     * @param {Object} fieldDef     - Column definition
     * @param {Object} record       - Local model's record
     */
    _lookupValue(fieldDef: any, record: any): any;
    hinderTextSelect(ev: any): void;
    shouldUpdate(changes: any): boolean;
    willUpdate(changes: any): void;
    _sorts: any;
    firstUpdated(): void;
    updated(props: any): void;
    /**
     * Remove our data
     * @public
     */
    public clear(): void;
    /**
     * Reset this table, removing data, filters and sorts
     * @public
     */
    public reset(): void;
    _filters: {
        ':rules'?: undefined;
    } | {
        ':rules': any[];
    };
    /**
     * Get total number of records in the model
     * @public
     * @type {Number}           - Total number of records in the model
     */
    public get total(): number;
    /**
     * Get number of records last fetched
     * @public
     * @type {Number}           - Total number of fetched records
     */
    public get length(): number;
    /**
     * Replace sorting of this table with provided sort criteria
     * @public
     * @param {Object[]}  sorts                     - A list of sorting criteria
     * @param {String}    sorts[].field             - The name of the field to sort on
     * @param {Function} [sorts[].sortFunc]         - A custom sorting function used
     * @param {String}   [sorts[].direction]        - The direction to sort, asc or desc
     * @param {String}   [sorts[].type]             - Sort type, number or string if not a defined column
     * @throws                                      - If unable to sort for any reason
     */
    public applySort(sorts: {
        field: string;
        sortFunc?: Function;
        direction?: string;
        type?: string;
    }): void;
    /**
     * Apply a field filter to this table
     * @public
     * @param {String} field            - The field to filter
     * @param {String|Number} filterval - The value to filter with
     */
    public applyFieldFilter(field: string, filterval: string | number): void;
    _needUpdate: boolean;
    /**
     * Get the field filter for a field
     * @public
     * @param {String} field        - The field
     * @returns {String|Number}     - The string or number applied, or undefined if no filter
     */
    public getFieldFilter(field: string): string | number;
    /**
     * Apply a set of non-interactive supplementary filters to this table
     * @public
     * @param {FiltersCollection} filters   - The filters to apply
     * @throws                              - If filters cannot be applied
     */
    public applyFixedFilters(filters: FiltersCollection): void;
    /**
     * Get the non-interactive supplementary filters applied to this table
     * @public
     * @returns {FiltersCollection}   - The filters applied
     */
    public getFixedFilters(): FiltersCollection;
    /**
     * Get a copy of all current filters applied to this table
     * @public
     * @returns {Array<Function|FilterType>}  - The filters applied to this table
     */
    public getFilters(): Array<Function | FilterType>;
    /**
     * Get all displayed table rows
     * @protected
     */
    protected getDisplayedItems(): Element[];
    disabled: boolean;
    busy: boolean;
    /**
     * Apply a quick search
     * @param {Event} ev        - Change event
     */
    _setSearch(ev: Event): void;
    /**
     * Check for slotted toolbar items
     */
    _toolbarSlotted(): void;
    _hasToolbar: boolean;
    /**
     * Prepare filter controls
     */
    _prepareFieldFilters(): Promise<void>;
    /**
     * Toggle the filter control
     * @param {MouseEvent} ev     - Click event
     */
    _toggleFieldFilter(ev: MouseEvent): void;
    /**
     * Take a field and enable or disable a filter
     * @param {Event} ev            - The change event
     * @see BasicCollection#CriteriaType
     */
    _doFieldFilter(ev: Event): void;
    /**
     * Fetch any models that will be required for value lookups and listen for changes
     * @private
     */
    private _fetchLookupModels;
    /**
     * Update our data from the model, causing a display update
     * @protected
     * @async
     * @fires SimpleTable#updated      - When the data is updated
     * @see DataboundComponent#_dataChanged
     */
    protected _dataChanged(detail: any): Promise<void>;
    /**
     * Populate our data from the model
     * @private
     * @async
     */
    private _loadFromModel;
    /**
     * Build or rebuild filters as needed
     * @return {FiltersCollection}  - Built filters
     */
    _buildFilters(): FiltersCollection;
    /**
     * Attempt to edit a cell
     * @private
     * FIXME: Unimplemented
     */
    private _tryEditing;
    /**
     * Apply a sort if possible when attempted on a column
     * @private
     * @param {MouseEvent} ev       - The originating mouse event
     */
    private _sortIfSortable;
    /**
     * Paginate in some way
     * @private
     */
    private _selectPage;
    /**
     * Change the range of results
     * @private
     */
    private _selectRange;
    /**
     * Flash the table to let the eye know of a change
     * @private
     */
    private _flashTable;
}
import { SelectableListComponent } from './selectablelist.js';
import { ResizeObserver as Polyfill } from '@juggle/resize-observer';
