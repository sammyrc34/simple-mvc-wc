/**
 * Data-bound component super class for all data model bound components
 * This class cannot be instantiated directly
 * @class
 */
export class DataboundComponent extends SimpleComponent {
    bindMode: any;
    bindToNew: any;
    dataModel: any;
    idField: any;
    begin: any;
    range: any;
    _bound: boolean;
    _boundid: any;
    _silent: boolean;
    /**
     * Get bound state
     * @type {Boolean}
     */
    get bound(): boolean;
    /** Get the currently bound ID
     * @type {String|Number}
     */
    get boundid(): string | number;
    shouldUpdate(): boolean;
    updated(props: any): Promise<void>;
    /**
     * Get the model associated with this component
     * @returns {SimpleCollection|SimpleRecord|SimpleRemoteCollection|SimpleRemoteRecord|SimplePollingCollection|SimplePollingRecord} - Data model class
     */
    getModel(): SimpleCollection | SimpleRecord | SimpleRemoteCollection | SimpleRemoteRecord | SimplePollingCollection | SimplePollingRecord;
    /**
     * Bind if able
     * @throws        - In event of any failure during binding
     */
    bind(): Promise<void>;
    /**
     * Unbind and forget the data model
     * @async
     */
    unbind(): Promise<void>;
    bindId: any;
    /**
     * Called by a bound model when a change to model or data has occurred.
     * Must be overridden in subclasses. Promise-aware to ensure structured handling
     * @async
     * @abstract
     * @param {Object} detail
     * @param {String} detail.change              - The type of change, load, add, update or remove
     * @param {Boolean} detail.pending            - If the change is non-authoritative (eg pending)
     * @param {Map} detail.mapping                - Mapping of IDs for an add if known
     * @param {String[]|Number[]} details.ids     - The IDs associated with the change
     */
    _dataChanged(detail: {
        change: string;
        pending: boolean;
        mapping: Map<any, any>;
    }): Promise<void>;
    /**
     * Called by a bound model when a change to the model has occurred.
     * Must be overridden in subclasses. Promise-aware to ensure structured handling
     * @async
     * @abstract
     * @param {String} type       - Type of status change, either 'pending', 'bind', 'unbind', 'disable' or 'enable'
     */
    _statusChanged(type: string): Promise<void>;
    /**
     * Process one complete record into the bound model
     * @param {Object} data                 - The changes to make in the model
     * @param {String} [id]                 - The id for the record change, required in multi-bind mode
     * @throws                              - Errors if unable to add or update the record
     */
    ingressRecord(data: any, id?: string): string;
}
import { SimpleComponent } from '../index.js';
import { SimpleCollection } from '../index.js';
import { SimpleRecord } from '../index.js';
import { SimpleRemoteCollection } from '../index.js';
import { SimpleRemoteRecord } from '../index.js';
import { SimplePollingCollection } from '../index.js';
import { SimplePollingRecord } from '../index.js';
