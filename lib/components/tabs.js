
import { ResizeObserver as Polyfill } from '@juggle/resize-observer';
import { SimpleComponent } from '../index.js';

import tabStyles from './tabs.css';

const ResizeObserver = window.ResizeObserver || Polyfill;

/**
 * A cancelable event emitted before selection of a tab
 * @event SimpleTabsContainer#before-select
 * @type {Object} detail
 * @property {HTMLElement} tab - Tab to be selected
 */
const onBeforeSelect = 'before-select';


/**
 * An event emitted after selection of a tab
 * @event SimpleTabsContainer#after-select
 * @type {Object} detail
 * @property {HTMLElement} tab - Tab that has been selected
 */
const onAfterSelect = 'after-select';



export class SimpleTabsContainer extends SimpleComponent {

  /**
   * @emits {before-select} Before selection of a tab
   * @emits {after-select} After selection of a tag
   */


  constructor() {
    super();

    // Private objects
    this.resizeobserver = new ResizeObserver( ( entries ) => {
      if ( ! Array.isArray(entries) || ! entries.length )
        return;

      // Resize
      window.requestAnimationFrame(() => {
        this._detectSize();
      });

    } );


  }


  static get _elementName() { return 'sc-tabs' }
  static get properties() {
    return Object.assign( {}, super.properties, {
      fill:       { type: Boolean, def: false, desc: "Fill the container's size, otherwise sizes to content" },
      raised:     { type: Boolean, def: false, desc: "Use a raised style effect" },
      scroll:     { type: Boolean, def: false, desc: "Allow tabs to scroll content" },
    } );
  }


  static get styles() {
    return super.styles.concat( [ tabStyles ] );
  }


  connectedCallback() {
    super.connectedCallback();
    this.selectTab( ); //Note async
  }


  /******************
   * Lit's methods
   *****************/

  render() {
    return this.builder`
      <div class="sc-tabs-container" ?fill=${this.fill} ?scroll=${this.scroll}>
        <slot @slotchange=${this._slotChanged}></slot>
      </div>
    `;
  }


  async willUpdate( changes ) {
    super.willUpdate( changes );
    if ( ! this.hasUpdated )
      this._slotChanged();
  }


  async firstUpdated() {
    await this.updateComplete;
    this._detectSize();
    this._checkSelected();
  }


  /******************
   * Own methods
   *****************/


  /**
   * Select a specific tab
   * @param {SimpleTab} toSelect    - Tab element to select
   */
  selectTab( toSelect ) {
    const tabs = Array.from( this.getElementsByTagName( 'sc-tab' ) );

    if ( tabs.includes( toSelect ) )
      return this._selectTab( toSelect );
  }


  /**
   * Select a specific tab by index
   * @param {Number} index    - The zero-indexed tab number to select
   */
  selectTabIndex( index ) {
    const tabs = Array.from( this.getElementsByTagName( 'sc-tab' ) );

    this._selectTab( tabs[ index ] );
  }


  /**
   * Try to select a tab
   */
  _selectTab( tab ) {
    const tabs = Array.from( this.getElementsByTagName( 'sc-tab' ) );
    let ok = true;

    // Before selection event
    ok = this.dispatchEvent( new CustomEvent( onBeforeSelect, {
      detail: { tab: tab },
      bubbles: true, cancelable: true
    } ) );
    if ( ok === false )
      return;

    // De-select before selecting
    for ( let t of tabs ) {
      if ( t !== tab )
        t.selected = false;
    }
    tab.selected = true;

    // After selection event
    this.dispatchEvent( new CustomEvent( onAfterSelect, {
      detail: { tab: tab },
      bubbles: true, cancelable: false
    } ) );
  }


  /**
   * Handle a change to slotted tabs, propagate properties
   */
  async _slotChanged() {
    const kids = Array.from( this.getElementsByTagName( 'sc-tab' ) );
    for ( let kid of kids ) {
      if ( ! kid.dataset.observed )
        this.resizeobserver.observe( kid );
      kid.raised = this.raised;
      kid.scroll = this.scroll;
    }
    await this.updateComplete;
    this._detectSize();
  }


  /**
   * Detect the size of the largest tab and size ourself to that
   */
  _detectSize() {
    const container = this.shadowRoot.querySelector( '.sc-tabs-container' );
    let tabsWidth, tabHeight = 0, contentWidth, contentHeight, tmpW, tmpH, tabs;

    if ( this.fill )
      return;

    tabs = Array.from( this.getElementsByTagName( 'sc-tab' ) );
    tabHeight = tabs[ 0 ]._getTabHeight();
    tabsWidth = this.getBoundingClientRect().width;

    for ( let tab of tabs ) {
      [ tmpH, tmpW ] = tab._getContentSize();

      if ( ! contentHeight || tmpH > contentHeight )
        contentHeight = tmpH;
      if ( ! contentWidth || tmpW > contentWidth )
        contentWidth = tmpW;
    }

    container.style.height = ( Math.ceil( contentHeight + tabHeight + 1 ) + 'px' );
    container.style.width = ( tabsWidth > contentWidth ) ? tabsWidth + 'px' : contentWidth + 'px';
  }


  /**
   * Check that a tab is selected
   */
  _checkSelected() {
    const tabs = Array.from( this.getElementsByTagName( 'sc-tab' ) );
    const first = this.querySelector( 'sc-tab' );
    if ( ! tabs.some( t => t.selected ) && first )
      this.selectTab( first );
  }

}


export class SimpleTab extends SimpleComponent {
  constructor() {
    super();

    this.raised       = false;
    this.scroll       = false;
    this.label        = undefined;
  }


  static get _elementName() { return 'sc-tab' }


  static get properties() {
    return Object.assign( {}, super.properties, {
      disabled:     { type: Boolean, def: false, desc: "If this tab is disabled" },
      selected:     { type: Boolean, def: false, reflect: true, desc: "If this tab is selected" },
      raised:       { type: Boolean, def: false, desc: "Use a raised style effect" },
      label:        { type: String, def: undefined, desc: "The tab's label" },
      scroll:       { type: Boolean, def: false, desc: "Allow tabs to scroll content" },
    } );
  }


  static get styles() {
    return super.styles.concat( [ tabStyles ] );
  }

  /******************
   * Lit's methods
   *****************/

  render() {
    return this.builder`
      <div
          class="sc-tab"
          ?selected=${this.selected}
          ?raised=${this.raised}
          ?disabled=${this.disabled}
          ?scroll=${this.scroll}
      >
        <div class="sc-tab-label-wrap" @click=${this.selectTab} >
          <slot name="label">
            <div class="sc-tab-label">
              <p>${this.label}</p>
            </div>
          </slot>
        </div>
        <div class="sc-tab-content">
          <slot name="content"></slot>
        </div>
        <div class="sc-tab-size-helper"></div>
      </div>
    `
  }


  /******************
   * Own methods
   *****************/


  /**
   * Attempt to select this tab
   */
  selectTab() {
    if ( this.disabled )
      return;
    if ( this.parentElement && this.parentElement.tagName.toUpperCase() == "SC-TABS" )
      this.parentElement.selectTab( this );
  }


  /**
   * Get this tab's height?
   */
  _getTabHeight() {
    const tab = this.shadowRoot.querySelector( '.sc-tab-label-wrap' );
    const helper = this.shadowRoot.querySelector( '.sc-tab-size-helper' );
    let height = 0;
    height = helper.getBoundingClientRect().height;
    height += tab.getBoundingClientRect().height;
    return height;
  }


  /**
   * Get this tabs' content height
   */
  _getContentSize() {
    const content = this.shadowRoot.querySelector( '.sc-tab-content' );

    let box;

    content.classList.add( 'sizing' );

    box = content.getBoundingClientRect();

    content.classList.remove( 'sizing' );

    return [ box.height, box.width ];
  }

}


