export class SimpleSelect extends DataboundComponent {
    static get _elementName(): string;
    bindMode: string;
    bindToNew: boolean;
    _values: any[];
    _optionsmap: Map<any, any>;
    _focused: boolean;
    _animatelabel: boolean;
    _smalllabel: boolean;
    _showopts: boolean;
    _open: boolean;
    _customvalue: boolean;
    _valueelement: any;
    _displayvalue: string;
    _message: string;
    _isplaceholder: boolean;
    _hascomment: boolean;
    _current: any;
    _initSelected: any[];
    _defaultSelected: any[];
    _filter: string;
    _height: number;
    _maxheight: number;
    render(): import("lit-html").TemplateResult<1>;
    renderValue(): import("lit-html").TemplateResult<1>;
    optionsTemplate(): import("lit-html").TemplateResult<1>;
    /******************
     * Lit methods
     *****************/
    willUpdate(changes: any): void;
    firstUpdated(): Promise<void>;
    /**
     * Set the currently selected value(s)
     * @param {String|Number|String[]|Number[]} vals - Values to be selected
     */
    set value(vals: string | number | string[] | number[]);
    /******************
     * Own methods
     *****************/
    /**
     * Get the current value(s)
     * @returns {String|Number|String[]|Number[]} - Selected value(s)
     */
    get value(): string | number | string[] | number[];
    /**
     * Set this controls initial value
     * @param {String|Number|String[]|Number[]} values - The selected values
     */
    setInitial(values: string | number | string[] | number[]): void;
    /**
     * Reset the value to the initial value or a default value
     * @public
     * @param {Boolean} [toDefault=false] - Reset to the default value instead
     */
    public reset(toDefault?: boolean): void;
    invalid: boolean;
    /**
     * Is the value of this component changed?
     * @public
     * @returns {Boolean}
     */
    public changed(): boolean;
    /**
     * Validate this input as needed
     * @returns {Boolean} - If validation was successful
     * @public
     */
    public validate(): boolean;
    /**
     * Get the currently selected options
     */
    _getSelected(): void;
    /**
     * Get any initially selected elements, set our default value and current if not otherwise set
     */
    _initialSelected(): void;
    /**
     * Set the selection state on options, and build the value
     */
    _updateSelections(): void;
    /**
     * Get all options
     * @protected
     */
    protected get _options(): any[];
    /**
     * Coerce the value types as needed
     * @param {*} values    - Values to type-coerce
     * @returns             - Coerced values
     * @private
     */
    private _typeValues;
    /**
     * Type coerce a value
     * @param {*} value     - The value to be type coerced, usually a string
     * @returns {*}         - The type coerced value
     */
    _typeValue(value: any): any;
    /**
     * Detect the options panel size
     * @private
     */
    private _detectSize;
    /**
     * Handle a model status change
     * @protected
     * @see DataboundComponent#_statusChanged
     */
    protected _statusChanged(type: any): void;
    data: any;
    /**
     * Handle a model data change
     * @protected
     * @async
     * @see DataboundComponent#_dataChanged
     */
    protected _dataChanged(): Promise<void>;
    /**
     * Update the displayed message or error
     * @protected
     */
    protected _updateMessage(): void;
    /**
     * Handle a key down event in the search input
     * @protected
     */
    protected _searchKeyDown(ev: any): void;
    /**
     * Handle a key down event
     * @protected
     * @param {Event} ev          - The keydown event
     */
    protected _keyDown(ev: Event): void;
    /**
     * Move the active element to the previous one
     * @protected
     */
    protected _moveUp(): void;
    /**
     * Move the active element to the next one
     * @protected
     */
    protected _moveDown(): void;
    /**
     * Toggle an active element
     * @protected
     * @param {HTMLElement} option    - Option to select or de-select
     * @param {Boolean} close         - If the selection box should close after selection
     * @param {Boolean} onlyselect    - If this should select the item only, not de-select
     */
    protected _selectDeselectOption(option: HTMLElement, close: boolean, onlyselect: boolean): void;
    /**
     * The active option has changed, store current
     * @protected
     */
    protected _activeChange(ev: any): void;
    /**
     * Set an option as active by index
     * @protected
     */
    protected _setActiveOption(number: any): void;
    /**
     * Notify listeners that our selections have changed
     * @protected
     */
    protected _selectionChange(): void;
    /**
     * Work out the displayed value, show placeholder as needed
     * @protected
     */
    protected _updateDisplayValue(): void;
    /**
     * Focus the options panel
     * @protected
     */
    protected _focusIn(): void;
    /**
     * Close the options panel
     * @protected
     */
    protected _closeOpts(ev: any): void;
    /**
     * Select an option if it can be selected
     * @protected
     */
    protected _trySelectOption(ev: any): void;
    /**
     * Check if an option can be un-selected
     * @protected
     */
    protected _tryUnselectOption(ev: any): void;
    /**
     * Filter the option pane
     * @protected
     */
    protected _filterOptions(ev: any): void;
    /**
     * Toggle the option list visiblity
     * @protected
     */
    protected _toggleOptions(ev: any): void;
    /**
     * Shrink the options panel for animation purposes
     * @protected
     */
    protected _shrinkOpts(): void;
    /**
     * Re-position the option panel
     * @protected
     */
    protected _positionOpts(): void;
    /**
   * Attempt to deselect an option by display value control
   * @protected
   */
    protected _deselectFromDisplay(ev: any): void;
}
export class SimpleSelectList extends DataboundComponent {
    static get _elementName(): string;
    bindMode: string;
    bindToNew: boolean;
    _values: any[];
    _optionsmap: Map<any, any>;
    _focused: boolean;
    _customvalue: boolean;
    _valueelement: any;
    _message: string;
    _hascomment: boolean;
    _current: any;
    _initSelected: any[];
    _defaultSelected: any[];
    _filter: string;
    /******************
     * Render methods
     *****************/
    render(): import("lit-html").TemplateResult<1>;
    optionsTemplate(): import("lit-html").TemplateResult<1>;
    /*****************
     * Lit methods
     ****************/
    willUpdate(changes: any): void;
    firstUpdated(): Promise<void>;
    /**
     * Set the currently selected value or values
     * @param {String|Number|String[]|Number[]} vals - Selected value(s)
     */
    set value(vals: string | number | string[] | number[]);
    /******************
     * Own methods
     *****************/
    /**
     * Get the current value(s)
     * @returns {String|Number|String[]|Number[]} - Selected value(s)
     */
    get value(): string | number | string[] | number[];
    /**
     * Reset the value to a provided value or set from a default
     * @param {String|Number|String[]|Number[]} values - The selected values
     */
    setInitial(values: string | number | string[] | number[]): void;
    /**
   * Reset the value to the initial value
   * @public
   * @param {Boolean} [toDefault=false] - Reset to the default value instead
   */
    public reset(toDefault?: boolean): void;
    invalid: boolean;
    /**
     * Is the value of this component changed?
     * @public
     * @returns {Boolean}
     */
    public changed(): boolean;
    /**
     * Validate this input as needed
     * @returns {Boolean} - If validation was successful
     * @public
     */
    public validate(): boolean;
    /**
     * Coerce the value types as needed
     * @param {*} values    - Values to type-coerce
     * @returns             - Coerced values
     * @private
     */
    private _typeValues;
    /**
     * Type coerce a value
     * @param {*} value     - The value to be type coerced, usually a string
     * @returns {*}         - The type coerced value
     */
    _typeValue(value: any): any;
    /**
     * Get the currently selected options
     */
    _getSelected(): void;
    /**
     * Get any initially selected elements, set our default value and current if not otherwise set
     */
    _initialSelected(): void;
    /**
     * Set the selection state on options, and build the value
     */
    _updateSelections(): void;
    /**
     * Get all list options
     * @protected
     */
    protected get _options(): any[];
    /**
     * Handle a model status change
     * @protected
     */
    protected _statusChanged(type: any): void;
    data: any;
    /**
     * Handle a model data change
     * @private
     * @async
     */
    private _dataChanged;
    /**
     * Handle a focus in event
     * @protected
     */
    protected _focusIn(): void;
    /**
     * Handle a focus out event
     * @protected
     */
    protected _focusOut(): void;
    /**
     * Update the displayed hint message
     * @protected
     */
    protected _updateMessage(): void;
    /**
     * Handle key down in search input
     * @protected
     */
    protected _searchKeyDown(ev: any): void;
    /**
     * Handle a key down event in the list
     * @protected
     */
    protected _keyDown(ev: any): void;
    /**
     * Handle a move up key action
     * @protected
     */
    protected _moveUp(): void;
    /**
     * Handle a move down key action
     * @protected
     */
    protected _moveDown(): void;
    /**
     * Toggle an option selection
     * @protected
     * @param {HTMLElement} option    - The option to select or de-select
     * @param {Boolean} onlyselect    - If this should select the item only, not de-select
     */
    protected _selectDeselectOption(option: HTMLElement, onlyselect: boolean): void;
    /**
     * The active option has changed, store current
     * @protected
     */
    protected _activeChange(ev: any): void;
    /**
     * Set active option by index
     * @protected
     */
    protected _setActiveOption(number: any): void;
    /**
     * Notify listeners that our selections have changed
     * @protected
     */
    protected _selectionChange(): void;
    /**
     * Select an option if it can be selected
     * @protected
     */
    protected _trySelectOption(ev: any): void;
    /**
     * Check if an option can be un-selected
     * @protected
     */
    protected _tryUnselectOption(ev: any): void;
    /**
     * Filter the option pane
     * @protected
     */
    protected _filterOptions(ev: any): void;
    /**
     * Clear the current selection
     * @protected
     */
    protected _clearSelection(): void;
}
export class SimpleSelectOption extends SimpleComponent {
    static get _elementName(): string;
    render(): import("lit-html").TemplateResult<1>;
    get _displayValue(): any;
    _mouseActive(ev: any): void;
    active: boolean;
    /**
     * Request to select or deselect this option
     */
    toggleOption(ev: any): void;
}
import { DataboundComponent } from './databound.js';
import { SimpleComponent } from '../index.js';
