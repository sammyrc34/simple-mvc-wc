

import { DataboundComponent } from './databound.js';


/**
 * Event when double clicking on an list item
 * @event SimpleList#item-dbl-click
 * @type {Object}
 * @property {String[]|Number[]} selected - The selected item's ID
 * @property {Number} clientX             - The mouse X coordinates relative to the viewport
 * @property {Number} clientY             - The mouse Y coordinates relative to the viewport
 * @property {Number} pageX               - The mouse X coordinates relative to the document
 * @property {Number} pageY               - The mouse Y coordinates relative to the document
 */
const onItemDoubleClick = 'item-dbl-click';

/**
 * Event when context-menu or right-clicking on an item
 * @event SimpleList#item-menu-click
 * @type {Object}
 * @property {String[]|Number[]} selected - The selected item's ID
 * @property {Number} clientX             - The mouse X coordinates relative to the viewport
 * @property {Number} clientY             - The mouse Y coordinates relative to the viewport
 * @property {Number} pageX               - The mouse X coordinates relative to the document
 * @property {Number} pageY               - The mouse Y coordinates relative to the document
 */
const onItemContextMenu = 'item-menu-click';

/**
 * A cancelable event before selection of an item
 * @event SimpleList#beforeselected
 * @type {Object}
 * @property {String[]|Number[]} old  - The previous selected item's ID
 * @property {String[]|Number[]} new  - The new selected item's ID
 */
const onBeforeSelected = 'before-select';

/**
 * Event after selection of an item.
 * @event SimpleList#select
 * @type {Object}
 * @property {String[]|Number[]} old  - The previous selected item's ID
 * @property {String[]|Number[]} new  - The new selected item's ID
 */
const onSelected = 'select';


/**
 * A component containig a list of selectable items or rows
 *
 * Supports single or multiple selection models, handling events
 * for selections, double-clicks and context menus
 * @class
 */
class SelectableListComponent extends DataboundComponent {


  /**
   * @emits {before-select}     - Cancel-able event before selection is made
   * @emits {select}            - After selection is made
   * @emits {item-dbl-click}    - When an item is double-clicked
   * @emits {item-menu-click}   - When an item is context-menu clicked
   */

  constructor() {
    super();

    // Options
    this.select           = "none"; // Either "none", "single" or "multi"
    this.modelName        = undefined;

    // For storing the data displayed
    this._data            = [];

    // Required for superclass
    this.bindToNew        = false;
    this.bindMode         = "multi";

    // Private, stateful
    this._selected        = [];
    this._lastSel         = undefined;
  }

  static get properties() {
    return Object.assign( {}, super.properties, {
      _data:         { type: Array },
      select:        { type: String },

      // Private
      _selected:     { type: Array },
    } );
  }

  /**
   * Is a row ID selected?
   * @param {String|Number} id          - The ID to check
   * @return {Boolean}                  - If the item (row) is selected
   */
  isSelected( id ) {
    if ( this._selected.indexOf( id ) !== -1 )
      return true;
    return false;
  }

  /**
   * Handle a click and try to select things appropriately
   * @param {MouseEvent} ev               - This click event
   * @return {Boolean}                    - If selection was successful
   */
  handleSelectClick(ev) {
    const elem = ev.currentTarget;
    const multi = ( this.select == "multi" );
    const ctrl = ev.ctrlKey;
    const shft = ev.shiftKey;
    let rowid, current, next, cont = false;

    if ( this.select == "none" )
      return false;

    // Outside of table row display?
    if ( ! elem )
      return false;

    ev.preventDefault();

    // Support shift or ctrl clicks
    if ( multi && ctrl )
      return this.selectByToggle( ev, elem );
    else if ( multi && shft && this._lastSel !== undefined )
      return this.selectByRange( ev, elem );

    // Get any row
    rowid = this.getItemID( elem );

    // Plain old select-one-item
    current = this.getSelected();
    if ( rowid !== undefined )
      next = [ rowid ];
    else
      next = []

    // No changes?
    if ( next.length == current.length && next.every( e => current.includes( e ) ) )
      return;

    cont = this.dispatchEvent( new CustomEvent( onBeforeSelected, { 
      detail: { 'old': current, 'new': next }, 
      bubbles: true, cancelable: true }
    ) );
    if ( cont === false )
      return false;

    this._selected = next;
    this._lastSel = elem;

    this.dispatchEvent( new CustomEvent( onSelected, {
      detail: { 'old': current, 'new': next },
      bubbles: true
    } ) );

    return true;
  }


  /**
   * Select a range of items from the last selected item to the current one
   * @param {MouseEvent} ev       - MouseEvent
   * @param {HTMLElement} elem    - The clicked item element
   * @return {Boolean}            - If the selection was successful
   */
  selectByRange( ev, elem ) {
    var elems;
    var fromidx, toidx, tmpid, dir = -1;
    var current, next, cont;

    elems = this.getDisplayedItems();

    fromidx = elems.indexOf( this._lastSel );
    if ( fromidx === -1 )
      return false;

    toidx = elems.indexOf( elem );
    if ( toidx === -1 )
      return false;

    if ( toidx > fromidx )
      dir = 1;

    // Skip the last selected item
    fromidx += dir;

    current = this.getSelected();
    next = [ ...current ];
    for( let i = fromidx;; i += dir ) {
      tmpid = this.getItemID( elems[ i ] );
      if ( next.indexOf( tmpid ) === -1 ) 
        next.push( tmpid );
      if ( i == toidx || i < 0 || i >= elems.length )
        break;
    }

    cont = this.dispatchEvent( new CustomEvent( onBeforeSelected, { 
      detail: { 'old': current, 'new': next }, 
      bubbles: true, cancelable: true }
    ) );
    if ( cont === false ) {
      return false;
    }

    this._selected = next;
    this._lastSel = elem;

    this.dispatchEvent( new CustomEvent( onSelected, {
      detail: { 'old': current, 'new': next },
      bubbles: true
    } ) );

    return true;
  }


  /**
   * Toggle selection of one item 
   * @param {MouseEvent} ev       - MouseEvent
   * @param {HTMLElement} elem    - The clicked item element
   * @return {Boolean}            - If the selection changed
   */
  selectByToggle( ev, elem ) {
    var rowid;
    var select = false;
    var cont = false;
    var current, next;

    rowid = this.getItemID( elem );

    if ( ! this.isSelected( rowid ) )
      select = true;

    current = this.getSelected();
    next = [ ...current ];
  
    if ( select )
      next.push( rowid );
    else
      next.splice( next.indexOf( rowid ), 1 );

    cont = this.dispatchEvent( new CustomEvent( onBeforeSelected, { 
      detail: { 'old': current, 'new': next }, 
      bubbles: true, cancelable: true }
    ) );
    if ( cont === false ) {
      return false;
    }

    this._selected = next;
    this._lastSel = elem;

    this.dispatchEvent( new CustomEvent( onSelected, {
      detail: { 'old': current, 'new': next },
      bubbles: true
    } ) );

    return true;
  }


  /**
   * Handle a double-click on a selected item. 
   * Requires selection to be enabled, or the raw double click will bubble.
   */
  handleDblClick( ev ) {
    var rowid, dblclickev, dets = {};
    var elem = ev.currentTarget;

    if ( this.select == "none" )
      return;

    rowid = this.getItemID( elem );
    if ( rowid == undefined || rowid == "" )
      return;

    ev.preventDefault();

    [ 'clientX', 'clientY', 'pageX', 'pageY' ].forEach( k => { dets[ k ] = ev[ k ] } );

    this._selected = [ rowid ]; 

    dets[ 'selected' ] = this._selected;
    dblclickev = new CustomEvent( onItemDoubleClick, { detail: dets, bubbles: true, cancelable: false } );
    this.dispatchEvent( dblclickev );
  }


  /**
   * Handle a context-menu click on a selected item. 
   * Requires selection to be enabled, or the context menu click will bubble.
   */
  handleContextMenu( ev ) {
    var cont, rowid, menuclickev, dets = {};
    var elem = ev.currentTarget;

    rowid = this.getItemID( elem );
    if ( rowid == undefined )
      return;

    if ( this.select == "none" )
      return;

    if ( ! this.isSelected( rowid ) ) {
      cont = this.handleSelectClick( ev );
      if ( ! cont ) 
        return;
    }

    ev.preventDefault();

    [ 'clientX', 'clientY', 'pageX', 'pageY', 'screenX', 'screenY' ].forEach( k => { dets[ k ] = ev[ k ] } );
    dets[ 'selected' ] = this.getSelected();

    menuclickev = new CustomEvent( onItemContextMenu, { detail: dets, bubbles: true, cancelable: false } );
    this.dispatchEvent( menuclickev ); 
  }


  /**
   * Get the ID associated with a list item
   * @param {HTMLElement} elem        - The sc-list-item element
   * @return {String|Number}          - The record's ID or undefined if none
   */
  getItemID( elem ) {
    let type, id;

    id = elem.dataset.id;
    type = elem.dataset.type;

    if ( type == "number" )
      id = parseFloat( id );

    return id;
  }

  /**
   * Get all selected values
   * @return {String[]|Number[]}      - The IDs for all selected items
   */
  getSelected() {
    return [...this._selected]
  }


  /**
   * Set selected values
   * @param {String[]|Number[]} ids   - The row IDs to select
   */
  setSelected( ids ) {
    this._selected = ids;
  }


  /** 
   * Clear selected items
   */
  clearSelection() {
    this._selected = [];
  }


  /**
   * Handle a data model status chaange
   * @protected
   * @see DataboundComponent#_statusChanged
   */
  _statusChanged( type ) {
    if ( type == "cleared" || type == "bind" || type =="unbind" )
      this.clearSelection();
  }


  /**
   * Handle a data model data change
   * @protected
   * @see DataboundComponent#_dataChanged
   */
  _dataChanged( detail ) {
    const change = detail[ 'change' ];
  
    // Remove removed records from our selection list
    if ( change == "load" || change == "remove" ) {
      this._selected.forEach( (s,i) => {
        if ( ! this.dataModel.hasRecord( s ) )
          this._selected.splice( i, 1 );
      } );
    }
  }


  getDisplayedItems() {
    throw new Error( "SelectableList components must define getDisplayedItems" );
  }

}


export { SelectableListComponent };

