

import { ResizeObserver as Polyfill } from '@juggle/resize-observer';
import { timeline } from 'motion';

import { library as faLib, icon as faIcon } from "@fortawesome/fontawesome-svg-core";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

import { SimpleComponent } from '../index.js';

import slideoutStyles from './slideout.css';

const ResizeObserver = window.ResizeObserver || Polyfill;

faLib.add( faTimes );


/**
 * Cancelable event before hiding
 * @event SimpleSlideout#before-hide
 */
const onBeforeHide = 'before-hide';


/**
 * Event fired after starting to hide a slideout
 * @event SimpleSlideout#hide
 */
const onHide = 'hide';


/**
 * Cancelable event before showing
 * @event SimpleSlideout#before-show
 */
const onBeforeShow = 'before-show';


/**
 * Event fired after starting to show a slideout
 * @event SimpleSlideout#show
 */
const onShow = 'show';


// A container with other containers inside
export class SimpleSlideout extends SimpleComponent {
  constructor() {
    super();

    // Private properties
    this._zindex         = 0;
    this._showing        = false;
    this._skipanim       = false;
    this._sizing         = false;
    this._bodytouch      = false;
    this._resizefromx    = undefined;
    this._resizefromy    = undefined;
    this._touchhandler   = undefined;
    this._movehandler    = undefined;
    this._grabbedelem    = undefined;
    this._resizeobserver = new ResizeObserver( () => {
      if ( this._showing )
        this._slotChanged();
    } );
  }

  static get _elementName() { return 'sc-slideout' }

  static get properties() {
    return Object.assign( {}, super.properties, {
      
      open:           { type: Boolean, def: false, desc: "If this slideout is currently opened" },
      modal:          { type: Boolean, def: false, desc: "If this slideout prevents interaction with elements underneath" },
      fullpage:       { type: Boolean, def: false, desc: "Should we position to the viewport" },
      position:       { type: String, def: "right", desc: "Side of the screen to be bound to, either left, right, top or bottom" },
      autoclose:      { type: Boolean, def: false, desc: "Should this slideout automatically close when clicking outside or hitting Esc" },
      hideclose:      { type: Boolean, def: false, desc: "Should the default close button be hidden" },

      opensize:       { type: String, def: "auto", desc: "Initial open size, auto or a size in percent or pixels with the appropriate unit suffix" },
      resizable:      { type: Boolean, def: false, desc: "Is this slideout resizable by the user" },
      resizemin:      { type: String, def: undefined, desc: "The minimum size in pixels or percent with the appropriate unit suffix" },
      resizemax:      { type: String, def: undefined, desc: "The maximum size in pixels or percent with the appropriate unit suffix" },

      allowzoom:      { type: Boolean, def: false, desc: "Enable pinch-zoom support on mobile devices" },

      borders:        { type: String, def: "auto", desc: "Content border display, none, single or auto where auto is on the leading and adjacent sides, and single is on the leading side only" },
      animatetime:    { type: Number, def: undefined, desc: "Animation duration, inherited from CSS if not set" },

      _opensize:      { type: Number, def: 0 },
      _hasheader:     { type: Boolean, def: false },
    } );
  }
  static get styles() {
    return super.styles.concat( [ slideoutStyles ] );
  }
  render() {
    return this.builder`
      <div
        class="sc-slideout"
        id="slideout-outer"
        position=${this.position}
        borders=${this.borders}
        ?full=${this.fullpage}
        ?modal=${this.modal}
        ?resizable=${this.resizable}
        ?open=${this.open}>

        <div 
          id="slideout-mask" 
          class="sc-slideout-mask" 
          @click=${this._tryClose}>
        </div>

        <div 
          id="slideout-wrap"
          class="sc-slideout-wrap" 
        >
          <div 
            id="slideout-content-wrap" 
            class="sc-slideout-content-wrap">

            ${ this.hideclose ? '' : this.builder`
              <div id="slideout-close" class="sc-slideout-close" @click=${() => this.hide()}>
                ${this.includeSvg( faIcon( faTimes ).html[0] ) }
              </div>`
            }

            <div 
                id="slideout-resize-bar-before" 
                class="sc-slideout-resize-bar-before"
                @mousedown=${this._startResize}
                @touchstart=${this._startResize}
            >
            </div>

            <div 
                id="slideout-content" 
                class="sc-slideout-content"
                @touchstart=${() => { this._bodytouch = true }}
                @touchend=${() => { this._bodytouch = false }}
            >
              <div 
                  id="slideout-header" 
                  class="sc-slideout-header" 
                  ?padded=${!this.hideclose} 
                  ?showheader=${this._hasheader}
                >
                <slot name="header" @slotchange=${this._headingChanged}></slot>
              </div>

              <div 
                  id="slideout-body" 
                  class="sc-slideout-body"
                  ?allowzoom=${this.allowzoom}
              >
                <slot @slotchange=${this._slotChanged}></slot>
              </div>
            </div>

            <div 
                id="slideout-resize-bar-after" 
                class="sc-slideout-resize-bar-after"
                @mousedown=${this._startResize}
                @touchstart=${this._startResize}
            >
            </div>

          </div>
        </div>
      </div>
    `;
  }

  connectedCallback() {
    super.connectedCallback();
    window.addEventListener( 'resize', () => this._slotChanged() )
    if ( this._resizeobserver )
      this._resizeobserver.observe( this.parentElement );
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    window.addEventListener( 'resize', () => this._slotChanged() )
    if ( this._resizeobserver && this.parentElement )
      this._resizeobserver.unobserve( this.parentElement );
  }


  /**********************
   * Lit methods
   *********************/

  /**
   * Determine if properties should change
   * @protected
   */
  shouldUpdate( changes ) {
    let ev;
    if ( ! super.shouldUpdate( changes ) )
      return false;
    if ( changes.has( 'open' ) && changes[ 'open' ] == false && this.open ) {
      ev = new CustomEvent( onBeforeShow, { bubbles: true, cancelable: true, composed: true } );
      if ( ! this.dispatchEvent( ev ) ) {
        this.open = false;
        return false;
      }
    }
    else if ( changes.has( 'open' ) && changes[ 'open' ] == true && this.open == false ) {
      ev = new CustomEvent( onBeforeHide, { bubbles: true, cancelable: true, composed: true } );
      if ( ! this.dispatchEvent( ev ) ) {
        this.open = true;
        return false;
      }
    }
    return true;
  }


  /**
   * Handle pending property updates
   * @protected
   */
  willUpdate( changes ) {
    super.willUpdate( changes );
    if ( ! this.hasUpdated || changes.has( 'animatetime' ) )
      this._inheritAnimateTime();
    if ( ! this.hasUpdated ) {
      this._headingChanged();
      this._slotChanged();
    }
  }

  /**
   * Handle updated properties
   * @protected
   */
  updated( changes ) {
    super.updated( changes );
    if ( ( changes.has( 'opensize' ) || changes.has( 'position' ) ) && this.open ) 
      this._slotChanged();
    if ( changes.has( 'open' ) && this.open )
      this._show();
    if ( changes.has( 'open' ) && ! this.open )
      this._hide();
  }


  /**********************
   * Own methods
   *********************/

  /** 
   * Get the current slideout size
   * @public
   * @returns {Number} - Height or width of the slideout's size in pixels depending on orientation
   */
  getSize() {
    return this._opensize;
  }


  /**
   * Try to show the slideout
   * @public
   * @param {Boolean} instant=false   - Display instantly without any animation
   */
  show( instant=false ) {
    if ( this.open )
      return;
    this.open = true;
    this._skipanim = instant;
  }


  /**
   * Try to hide the slideout
   * @public
   * @param {Boolean} instant=false   - Hide instantly without any animation
   */
  hide( instant=false ) {
    if ( ! this.open )
      return;
    this.open = false;
    this._skipanim = instant;
  }


  /**
   * Toggle to show or hide the slideout
   * @public
   */
  toggle() {
    if ( this.open )
      this.open = false;
    else
      this.open = true;
  }


  /**
   * Detect the presence of a header element
   * @private
   */
  _headingChanged() {
    const haskid = this.querySelector( '*[slot=header]' )
    if ( haskid )
      this._hasheader = true;
    else
      this._hasheader = false;
  }


  /**
   * Setup the component following content change
   * @async
   * @private
   */
  async _slotChanged() {
    if ( this._sizing ) {
      clearTimeout( this._sizing );
    }

    // Slow down when triggered by animations and such
    this._sizing = setTimeout( () => { this._updateSize() }, 25 );
  }


  /**
   * Update our content size
   * @private
   */
  _updateSize() {
    // Size up and position content
    this._opensize = 0;
    this._detectSize();
    this._sizeContent();
    this._positionAtStart();
    this._sizing = undefined;
  }


  /**
   * Inherit animation time from CSS if required
   * @private
   */
  _inheritAnimateTime() {
    let styles, animTime;
    if ( this.animatetime === undefined ) {
      styles = window.getComputedStyle( this );
      animTime = styles.getPropertyValue( '--slideout-animation-duration' );
      if ( animTime != "" && animTime != undefined )
        this.animatetime = parseFloat( animTime );
    }
  }


  /**
   * Show the slideout
   * @private
   */
  async _show() {
    let zindex, showev;

    zindex = this.app.getTopZ( 0 );
    if ( zindex !== this._zindex )
      this._zindex = this.app.getTopZ( 3 );

    this._detectSize();
    this._sizeContent();
    this._positionAtStart();
    await this._slideOut();

    showev = new CustomEvent( onShow, { bubbles: true, cancelable: false, composed: true } );
    this.dispatchEvent( showev );
  }


  /**
   * Hide the slideout
   * @private
   */
  async _hide() {
    const position = this.position;
    const container = this.shadowRoot.getElementById( 'slideout-wrap' );
    const mask = this.shadowRoot.getElementById( 'slideout-mask' );
    const anim = {}, seqs = [];
    let animTime = this.animatetime, hideev;
 
    // Can't hide unless sized
    if ( this._opensize == 0 ) {
      if ( this._skipanim )
        this._skipanim = false;
      return;
    }

    if ( this._skipanim ) {
      animTime = 0;
      this._skipanim = false;
    }

    this._showing = false;

    if ( this.modal && this.fullpage )
      this._enableBodyScroll();

    if ( position == "left" )
      anim[ 'left' ]    = ( 0 - this._opensize ) + "px";
    else if ( position == "right" )
      anim[ 'right' ]   = ( 0 - this._opensize ) + "px";
    else if ( position == "top" )
      anim[ 'top' ]     = ( 0 - this._opensize ) + "px";
    else if ( position == "bottom" )
      anim[ 'bottom' ]  = ( 0 - this._opensize ) + "px";

    seqs.push( [ mask, { opacity: 0 }, { at: 0 } ] );
    seqs.push( [ container, anim, { at: 0 } ] );

    await timeline( seqs, { 
      duration: animTime,
      defaultOptions: { 
        'ease': 'ease-in-out'
      } 
    } ).finished;

    mask.style.setProperty( 'display', 'none' );
    container.style.setProperty( 'opacity', 0 );

    hideev = new CustomEvent( onHide, { bubbles: true, cancelable: false, composed: true } );
    this.dispatchEvent( hideev );
  }


  /**
   * Detect the size of the content to display
   * @private
   */
  _detectSize() {
    const container = this.shadowRoot.getElementById( 'slideout-wrap' );
    const content = this.shadowRoot.getElementById( 'slideout-content-wrap' );
    const parentBounds = this.parentElement.getBoundingClientRect();
    let prop, rect, maxH, maxW;

    // Stop sizing
    if ( this._sizing ) {
      clearTimeout( this._sizing );
      this._sizing = undefined;
    }

    // Keep resizable-set sizes
    if ( this.resizable && this._opensize )
      return;

    // Hide and remove sizing to measure
    container.style.opacity = 0;
    content.style.removeProperty( 'width' );
    content.style.removeProperty( 'height' );
    content.style.removeProperty( 'max-height' );
    content.style.removeProperty( 'max-width' );
    container.style.removeProperty( 'width' );
    container.style.removeProperty( 'height' );
    container.style.removeProperty( 'display' );

    maxW = ( this.fullpage ) ? window.innerWidth + window.scrollX : parentBounds.width;
    maxH = ( this.fullpage ) ? window.innerHeight + window.scrollY : parentBounds.height;
    rect = content.getBoundingClientRect();

    if ( this.position == "left" || this.position == "right" )
      prop = "width";
    else
      prop = "height";

    if ( this.opensize == "auto" )
        this._opensize = rect[ prop ];
    else if ( this.opensize.endsWith( '%' ) ) {
      if ( prop == "width" )
        this._opensize = maxW * ( parseInt( this.opensize ) / 100 );
      else
        this._opensize = maxH * ( parseInt( this.opensize ) / 100 );
    }
    else if ( this.opensize )
      this._opensize = parseInt( this.opensize );
    else // what?
      this._opensize = rect[ prop ];
  }

  
  /**
   * Size the content ready for positioning
   * @private
   */
  _sizeContent() {
    const container = this.shadowRoot.getElementById( 'slideout-content-wrap' );
    let want;
    if ( this.position == "left" || this.position == "right" ) {
      want = ( this.fullpage ) ? window.innerHeight + window.scrollY : this.parentElement.clientHeight;
      container.style.maxWidth = this._opensize + "px";
      container.style.width = this._opensize + "px";
      container.style.height = want + "px";
      container.style.removeProperty( 'max-height' );
    }
    else {
      want = ( this.fullpage ) ? window.innerWidth + window.scrollX : this.parentElement.clientWidth;
      container.style.maxHeight = this._opensize + "px";
      container.style.height = this._opensize + "px";
      container.style.width = want + "px";
      container.style.removeProperty( 'max-width' );
    }
  }

  
  /**
   * Position content as needed
   * @private
   */
  _positionAtStart() {
    const container = this.shadowRoot.getElementById( 'slideout-wrap' );
    const props = {
      "opacity": 1,
      "max-width": "100%"
    };
    if ( this.position == "right" ) {
      props[ "left" ] = "";
      props[ "right" ] = ( 0 - ( this._showing ? 0 : this._opensize ) ) + "px";
      props[ "top" ] = 0;
      props[ "bottom" ] = "";
    }
    else if ( this.position =="left" ) {
      props[ "left" ] = ( 0 - ( this._showing ? 0 : this._opensize ) ) + "px";
      props[ "right" ] = "";
      props[ "top" ] = 0;
      props[ "bottom" ] = "";
    }
    else if ( this.position == "top" ) {
      props[ "left" ] = 0;
      props[ "right" ] = "";
      props[ "top" ] = ( 0 - ( this._showing ? 0 : this._opensize ) ) + "px";
      props[ "bottom" ] = "";
    }
    else if ( this.position == "bottom" ) {
      props[ "left" ] = 0;
      props[ "right" ] = "";
      props[ "top" ] = "";
      props[ "bottom" ] = ( 0 - ( this._showing ? 0 : this._opensize ) ) + "px";
    }
    Object.assign( container.style, props );
  }


  /**
   * Slide the slideout out
   * @private
   */
  async _slideOut() {
    const position = this.position;
    const container = this.shadowRoot.getElementById( 'slideout-wrap' );
    const outer = this.shadowRoot.getElementById( 'slideout-outer' );
    const mask = this.shadowRoot.getElementById( 'slideout-mask' );
    const anim = {}, seqs = [];
    let animTime = this.animatetime;

    outer.style.setProperty( 'z-index', this._zindex - 2 );
    mask.style.setProperty( 'z-index', this._zindex - 1 );
    container.style.setProperty( 'z-index', this._zindex );

    if ( this._skipanim ) {
      animTime = 0;
      this._skipanim = false;
    }

    if ( this.modal && this.fullpage )
      this._disableBodyScroll();

    if ( position == "left" )
      anim[ 'left' ] = 0;
    else if ( position == "right" )
      anim[ 'right' ] = 0;
    else if ( position == "top" )
      anim[ 'top' ] = 0;
    else if ( position == "bottom" )
      anim[ 'bottom' ] = 0;

    if ( this.autoclose || this.modal ) {
      mask.style.setProperty( 'display', 'block' );
      mask.style.setProperty( 'opacity', 0 );
      seqs.push( [ mask, { opacity: 1 }, { at: 0 } ] );
    }

    container.style.setProperty( 'opacity', 1 );
    seqs.push( [ container, anim, { at: 0 } ] );

    await timeline( seqs, { 
      duration: animTime,
      defaultOptions: { 
        'ease': 'ease-in-out'
      } 
    } ).finished;
  }


  /**
   * Disable document body scrolling
   * @private
   */
  _disableBodyScroll() {
    var body = document.body;
    var scrollY = window.scrollY;
    body.style[ 'top' ] = '-' + scrollY + 'px';
    body.style[ 'overflow-y' ] = "hidden";
  }

  /**
   * Enable document body scrolling
   * @private
   */
  _enableBodyScroll() {
    var body = document.body;
    var scrollY = body.style[ 'top' ];
    body.style[ 'top' ] = '';
    body.style[ 'overflow-y' ] = "auto";
    window.scrollTo(0, parseInt(scrollY || '0') * -1);
  }


  /**
   * Attempt to close if autoclose
   */
  _tryClose( ) {
    if ( this.autoclose )
      this.open = false;
  }


  /**
   * Start a resize on the panel
   */
  _startResize( ev ) {
    let relx, rely, part;
    if ( ! this.resizable )
      return;

    if ( ev.cancelable )
      ev.preventDefault();
    ev.stopPropagation();

    // Contain iOS body touches
    if ( this._bodytouch )
      return;

    // Prevent multi-touch starts
    if ( this._grabbedelem )
      return;

    part = ev.currentTarget;
    /* iOS workaround */
    if ( part == window )
      part = ev.target.closest( '*[slot]' );

    // Only touches originating on the header or resize bars
    if ( part !== this.shadowRoot.getElementById( 'slideout-resize-bar-before' ) &&
        part !== this.shadowRoot.getElementById( 'slideout-resize-bar-after' ) &&
        part.slot !== "header" )
      return;

    this._grabbedelem = ev.currentTarget;

    if ( 'changedTouches' in ev ) {
      relx = ev.changedTouches[0].pageX;
      rely = ev.changedTouches[0].pageY;
    }
    else {
      relx = ev.pageX;
      rely = ev.pageY;
    }

    this._resizefromx = relx;
    this._resizefromy = rely;

    this._touchhandler = (e) => { return this._resizePanel( e ) };
    this._movehandler = (e) => { return this._resizePanel( e ) };

    // Attach move and end events
    window.addEventListener( 'mousemove', this._movehandler );
    window.addEventListener( 'touchmove', this._touchhandler, { passive: false } );
    window.addEventListener( 'mouseup', (e) => { this._stopResize( e ) }, { once: true } );
    window.addEventListener( 'touchend', (e) => { this._stopResize( e ) }, { once: true } );

    this._grabbedelem.classList.add( 'grabbed' );
  }


  /**
   * End the panel resizing
   */
  _stopResize( ) {
    const container = this.shadowRoot.getElementById( 'slideout-content-wrap' );
    const bounds = container.getBoundingClientRect();

    if ( ! this._grabbedelem )
      return;

    // Remove listeners
    window.removeEventListener( 'mousemove', this._movehandler );
    window.removeEventListener( 'touchmove', this._touchhandler );
    this._movehandler = undefined;
    this._movehandler = undefined;

    // Keep current size
    if ( this.position == "left" || this.position == "right" ) {
      this._opensize = bounds.width;
    }
    else {
      this._opensize = bounds.height;
    }

    if ( this._grabbedelem )
      this._grabbedelem.classList.remove( 'grabbed' );
    this._grabbedelem = undefined;
  }


  /**
   * Resize the slideout in or out
   */
  _resizePanel( ev ) {
    const container = this.shadowRoot.getElementById( 'slideout-content-wrap' );
    const parentBounds = this.parentElement.getBoundingClientRect();
    let prop, tmpsize, delta, rel, pos, minsize, maxsize, max;

    if ( ev.cancelable )
      ev.preventDefault();
    ev.stopPropagation();

    // Figure out min and maximum sizes
    if ( this.position == "left" || this.position == "right" )
      max = ( this.fullpage ) ? window.innerWidth + window.scrollX : parentBounds.width;
    else
      max = ( this.fullpage ) ? window.innerHeight + window.scrollY : parentBounds.height;

    if ( this.resizemin && this.resizemin.endsWith( '%' ) )
      minsize = max * ( parseInt( this.resizemin ) / 100 );
    else if ( this.resizemin )
      minsize = parseInt( this.resizemin );
    else minsize = 0;

    if ( this.resizemax && this.resizemax.endsWith( '%' ) )
      maxsize = max * ( parseInt( this.resizemax ) / 100 );
    else if ( this.resizemax )
      maxsize = parseInt( this.resizemax );
    else
      maxsize = max;

    // Get current and relative positions
    if ( this.position == "right" || this.position == "left" ) {
      rel = this._resizefromx;
      prop = "pageX";
    }
    else {
      rel = this._resizefromy;
      prop = "pageY";
    }
    if ( 'changedTouches' in ev )
      pos = ev.changedTouches[ 0 ][ prop ];
    else
      pos = ev[ prop ];

    // Normalise the delta
    delta = rel - pos ;
    if ( this.position == "left" || this.position == "top" )
      delta *= -1;

    // Limit max size and resize
    tmpsize = this._opensize + delta;
    if ( this.position == "right" || this.position == "left" ) {
      if ( maxsize && tmpsize > maxsize )
        tmpsize = maxsize;
      else if ( minsize && tmpsize < minsize )
        tmpsize = minsize;
      container.style.width = tmpsize;
      container.style.maxWidth = tmpsize;
    }
    else {
      if ( maxsize && tmpsize > maxsize )
        tmpsize = maxsize;
      else if ( minsize && tmpsize < minsize )
        tmpsize = minsize;
      container.style.height = tmpsize + "px";
      container.style.maxHeight = tmpsize + "px";
    }

  }


}




