
import { SimpleComponent } from '../index.js';

import { library as faLib, icon as faIcon } from "@fortawesome/fontawesome-svg-core";
import { faBars } from "@fortawesome/free-solid-svg-icons";

import { animate } from 'motion';

import topMenuStyles from './menu-top.css';
import groupMenuStyles from './menu-group.css';
import itemMenuStyles from './menu-item.css';

faLib.add( faBars )


/**
 * An event fired when the menu is activated or opened
 * @event SimpleMenuTop#click
 * @event SimpleMenuGroup#click
 * @event SimpleMenuItem#click
 */


/**
 * Top menu component either in bar or button form
 *
 * A top menu is one that has a button, bar or popup to show menu groups and items.
 */
export class SimpleMenuTop extends SimpleComponent {
  constructor() {
    super();

    // Internal properties
    this._hiderFunc   = undefined;
    this._x           = undefined;
    this._y           = undefined;
  }
  static get _elementName() { return 'sc-menu-top' }
  static get properties() {
    return Object.assign( {}, super.properties, {

      topstyle:       { type: String, def: "auto", desc: "Menu presentation, either popup, button, bar or auto, where auto means bar on desktop, button on mobile" },
      groupstyle:     { type: String, def: "auto", desc: "Menu item presentation, block, popup or auto where auto is block on mobile, popup on desktop" },
      navmenu:        { type: Boolean, def: false, desc: "Should this menu respond to application navigation events to indicate a current page for example" },

      animatetime:    { type: Number, def: undefined, desc: "Animation duration, inherited from CSS if not set" }, 

      open:           { type: Boolean, def: false, reflect: true, desc: "Is this menu currently open?" },

      _grpblock:      { type: Boolean, def: false },
    } );
  }

  static get styles() {
    return super.styles.concat( [ groupMenuStyles, topMenuStyles ] );
  }

  connectedCallback() {
    super.connectedCallback();
    this.app.addEventListener( 'after-navigate', () => this._updateNavMenu() ); // Note async
    document.addEventListener( 'click', e => this._closeIfOutside(e) );
  }

  disconnectedCallback() {
    this.app.removeEventListener( 'after-navigate', () => this._updateNavMenu() ); // Note async
    document.removeEventListener( 'click', e => this._closeIfOutside(e) );
  }

  render() {
    switch( this.mode ) {
      case "bar":
        return this.renderBar();
      case "button":
        return this.renderButton();
      case "popup":
        return this.renderPopup();
    }
  }

  renderPopup() {
    return this.builder`
      <div 
          id="sc-menu-popup"
          class="sc-menu-popup" 
          @mouseleave=${this._mouseLeft} 
          @mouseenter=${this._mouseEnter}>
        <div 
            id="sc-menu-items" 
            class="sc-menu-items" 
            popup
            ?open=${this.open} 
            @click=${this.itemClickStop}
          >
            <slot></slot>
        </div>
      </div>`
  }

  renderBar() {
    return this.builder`
      <div 
          id="sc-menu-top"
          class="sc-menu-top" 
          @mouseleave=${this._mouseLeft} 
          @mouseenter=${this._mouseEnter} 
          @click=${this.toggleOpen}
      >
        <div 
          id="sc-menu-items" 
          class="sc-menu-items" 
          @click=${this.itemClickStop}
        >
          <slot></slot>
        </div>
      </div>`
  }


  renderButton() {
    return this.builder`
      <div 
          id="sc-menu-button"
          class="sc-menu-button" 
          @mouseenter=${this._mouseEnter} 
          @mouseleave=${this._mouseLeft}
        >
        <div id="sc-menu-button-bar" class="sc-menu-button-bar">
          <div id="sc-menu-button-wrap" class="sc-menu-button-wrap" @click=${this.toggleOpen}>
            <slot name="button">
              <div class="sc-menu-default-button">
                ${this.includeSvg( faIcon( faBars, { classes: [ 'sc-menu-default-button-svg' ] } ).html[0] )}
              </div>
            </slot>
          </div>
          <div id="sc-menu-button-bar-content" class="sc-menu-button-bar-content">
            <slot name="barcontent"></slot>
          </div>
        </div>
        <div class="sc-menu-items-wrap">
          <div 
              id="sc-menu-items" 
              class="sc-menu-items" 
              ?popup=${! this._grpblock}
              ?blocky=${this._grpblock}
              ?open=${this.open} 
              @click=${this.itemClickStop}
            >
            <slot></slot>
          </div>
        </div>
      </div>
    `;
  }



  willUpdate( changes ) {
    let topStyleChanged = false;
    super.willUpdate( changes );
    if ( ! this.hasUpdated || changes.has( 'animatetime' ) )
      this._inheritAnimateTime();
    if ( changes.has( 'topstyle' ) ) {
      if ( this.topstyle == "popup" ) {
        this._grpblock = false;
        this.groupstyle = "popup";
        topStyleChanged = true;
      }
    }
    if ( changes.has( 'groupstyle' ) || topStyleChanged ) {
      if ( this.groupstyle == "popup" )
        this._grpblock = false;
      else if ( this.groupstyle == "block" )
        this._grpblock = true;
      else if ( this.groupstyle == "auto" ) 
        this._grpblock = this.app.isMobile();
    }
  }


  async firstUpdated( props ) {
    super.firstUpdated( props );
    await this.updateComplete;
    const kids = Array.from( this.querySelectorAll( ":scope > sc-menu-group, :scope > sc-menu-item" ) );
    for ( let kid of kids ) {
      kid.setAttribute( 'depth', 1 );
    }
  }

  updated( props ) {
    if ( props.has( 'open' ) ) {
      if ( this.open && ! this._grpblock )
        this._repositionPopup( );
      else if ( this.open && this._grpblock )
        this._showBlock();
      else if ( props.get( 'open' ) && this._grpblock )
        this._hideBlock();
    };
  }


  /*******************
   * Our methods
   ******************/


  /**
   * Get the display mode of this top level menu, bar, button or popup
   * @type {String} - Top level display mode
   */
  get mode() {
    if ( this.topstyle == "auto" ) {
      if ( this.app.isMobile() )
        return "button";
      else
        return "bar";
    }
    else
      return this.topstyle;
  }


  /**
   * Is the group menu a block style
   * @protected
   * @type {Boolean} - If the group is a block layout
   */
  get groupBlock() {
    return ( this._grpblock ) ? true : false;
  }


  /**
   * Open this popup menu at given coordinates relative to the viewport
   * @param {Number} x - X position
   * @param {Number} y - Y position
   */
  openAt( x, y ) {
    this._x = x;
    this._y = y;
    this.open = true;
  }



  /**
   * Toggle this menu popup open or closed
   */
  toggleOpen() {
    if ( this.open ) {
      this.open = false;
      this.hideAll();
    }
    else
      this.open = true;
  }


  /**
   * Recursively close all menus
   */
  hideAll() {
    const kids = this.children;
    for ( const kid of kids )
      kid.open = false;
    this.open = false;
  }


  /**
   * Reveal our block menu items and groups with animation
   * @private
   */
  _showBlock() {
    const items = this.shadowRoot.getElementById( 'sc-menu-items' );
    const height = items.scrollHeight;
    animate( items, {
      height: [ null, height + 'px' ]
    }, {
      duration: this.animatetime,
      easing: 'ease-in-out'
    } );
  }


  /**
   * Hide our block menu items and groups with animation
   * @private
   */
  _hideBlock() {
    const items = this.shadowRoot.getElementById( 'sc-menu-items' );
    animate( items, {
      height: '0px'
    }, {
      duration: this.animatetime,
      easing: 'ease-in-out'
    } );
  }


  /**
   * Set the height of the items block to auto for resize
   */
  setAutoHeight() {
    const items = this.shadowRoot.getElementById( 'sc-menu-items' );
    items.style.setProperty( 'height', 'auto' );
  }


  /**
   * Inherit animation duration from CSS
   * @private
   */
  _inheritAnimateTime() {
    let styles, animTime;
    if ( this.animatetime === undefined ) {
      styles = window.getComputedStyle( this );
      animTime = styles.getPropertyValue( '--menu-animation-duration' );
      if ( animTime !== "" && animTime !== undefined )
        this.animatetime = parseFloat( animTime );
    }
  }


  /**
   * Reposition the group popup element
   * @private
   */
  _repositionPopup() {
    const popupbit = this.shadowRoot.getElementById( 'sc-menu-items' );
    const popupw = popupbit.clientWidth;
    const popuph = popupbit.clientHeight;
    let spacebelow, spaceright, ypos, xpos, nudge, relativeelem, relativecoords;

    if ( this.mode == "popup" ) {
      nudge = ( ( ( window.innerHeight + window.scrollY ) - this._y ) <= popuph );
      if ( nudge )
        ypos = ( window.innerHeight + window.scrollY ) - ( popuph * 1.1 );
      else 
        ypos = this._y;

      nudge = ( ( ( window.innerWidth + window.scrollX ) - this._x ) <= popupw );
      if ( nudge )
        xpos = ( window.innerWidth + window.scrollX ) - ( popupw * 1.1 );
      else
        xpos = this._x;
    }
    else if ( this.mode == "button" ) {
      relativeelem = this.shadowRoot.getElementById( 'sc-menu-button-wrap' );
      relativecoords = relativeelem.getBoundingClientRect();

      // Check there's space below and right
      spacebelow = window.innerHeight - relativecoords.bottom;
      spaceright = window.innerWidth - relativecoords.right;

      if ( spaceright < popupw )
        xpos = window.innerWidth - popupw;
      else
        xpos = 0;
      if ( spacebelow < popuph )
        ypos = 0 - relativecoords.height - popuph;
      else
        ypos = 0;
    }

    popupbit.style.top = ypos + 'px';
    popupbit.style.left = xpos + 'px';
  }


  /**
   * Start timer to hide the menu on mouse leave
   * @private
   */
  _mouseLeft() {
    if ( this._grpblock )
      return;
    this._hiderFunc = setTimeout( this.hideAll.bind( this ), 600 );
  }


  /**
   * Cancel any timeer to hide the menu popup
   * @private
   */
  _mouseEnter() {
    if ( this._hiderFunc ) {
      clearTimeout( this._hiderFunc )
      this._hiderFunc = undefined;
    }
  }


  /**
   * Close this menu when something else is clicked
   * @private
   */
  _closeIfOutside(ev) {
    if ( ! this.open )
      return;
    if ( ev.target.closest( 'sc-menu-top' ) !== this )
      this.open = false;
  }


  /**
   * Update the navigation menu to highlight the active navigation option
   * @private
   */
  async _updateNavMenu( ) {
    let url, path, tag, query, item, group, items, matching = [], scored = [];

    if ( ! this.navmenu )
      return;

    // Wait for children and paints
    await this.updateComplete;

    url = this.app.getStateProp( 'url' );
    path = url.path;
    tag = url.hash;
    query = url.query;

    // Navigation may not have completed
    if ( ! path || path == "" )
      return;

    // Find the best matching menu item
    items = Array.from( this.getElementsByTagName( 'sc-menu-item' ) );
    matching = items.filter( i => {
      if ( i.hasAttribute( 'navpath' ) ) return ( path.indexOf( i.navpath ) != -1 );
    } );
    if ( matching.length == 1 )
      item = matching[0];
    else if ( matching.length > 1 ) {
      scored = [];
      for ( let match of matching ) {
        let rest, score = 0;
        if ( Object.keys( match.navquery ).length > 0 && equal( query, match.navquery ) )
          score = 2; 
        else if ( Object.keys( match.navquery ).length == 0 && equal( query, match.navquery ) )
          score = 1;
        if ( tag != "" && tag == match.navtag )
          score += 2;
        else if ( tag == match.navtag ) 
          score += 1;
        if ( match.navpath != path ) {
          rest = path.replace( match.navquery, '' );
          score -= rest.match(/\//g).length;
        }
        scored.push( { 'item': match, 'score': score } );
      }
      scored.sort( (a,b) => { 
        return b.score - a.score;
      } );
      item = scored[0]['item'];
    }

    // Update properties
    for ( let i of items ) 
      i.removeAttribute( 'selected' );
    if ( item ) {
      group = item.parentElement;
      item.setAttribute( 'selected', '' );
      if ( group )
        group.setAttribute( 'selected', '' );
      if ( item.parentElement && item.parentElement.hasAttribute( 'collapsed' ) )
        item.parentElement.removeAttribute( 'collapsed' );
    }
  }
}

/**
 * The menu group component
 *
 * The menu group is essential a menu item with sub-items
 */
export class SimpleMenuGroup extends SimpleComponent {
  constructor() {
    super();

    // State properties
    this._x            = undefined;
    this._y            = undefined;

    // Internal properties
    this._hiderFunc = undefined;
  }
  static get _elementName() { return 'sc-menu-group' }
  static get properties() {
    return Object.assign( {}, super.properties, {

      label:          { type: String, def: "", desc: "The display label for this menu group" },

      open:           { type: Boolean, def: false, reflect: true, desc: "Is this menu group expanded" },
      direction:      { type: String, def: "auto", desc: "Where to place the menu items relative to the menu group item, either up, down, left, right or auto" },

      animatetime:    { type: Number, def: undefined, desc: "Animation duration, inherited from CSS if not set" }, 

      _block:         { type: Boolean, attribute: "isblock", def: false, reflect: true,  },
      _noicon:        { type: Boolean, def: false },
      _depth:         { type: Number, attribute: "depth" }
    } );
  }

  static get styles() {
    return super.styles.concat( [ groupMenuStyles ] );
  }

  render() {
    return this.builder`
    <div class="sc-menu-group-wrapper" @mouseenter=${this._mouseEnter} @mouseleave=${this._mouseLeft}>
      <div 
          id="sc-menu-group-name" 
          class="sc-menu-group-name" 
          @click=${this.toggleOpen} 
          ?blocky=${this._block}
          ?noicon=${this._noicon}
          depth=${this._depth}
          ?open=${this.open}
      >
        <slot name="label">
          <span>${this.label}</span>
        </span>
      </div>
      <div class="sc-menu-items-wrap">
        <div 
            id="sc-menu-items" 
            class="sc-menu-items" 
            ?popup=${! this._block}
            ?blocky=${this._block}
            ?open=${this.open} 
            @click=${this._itemClickStop}
          ><slot></slot>
        </div>
      </div>
    </div>
    `;
  }


  willUpdate( changes ) {
    super.willUpdate( changes );
    let kids;
    if ( ! this.hasUpdated || changes.has( 'animatetime' ) )
      this._inheritAnimateTime();

    if ( changes.has( '_depth' ) ) {
      kids = Array.from( this.querySelectorAll( ":scope > sc-menu-group, :scope > sc-menu-item" ) );
      for ( let kid of kids ) {
        kid.setAttribute( 'depth', this._depth + 1 );
      }
    }
  }

  updated( props ) {
    if ( props.has( 'open' ) ) {
      if ( this.open )
        this._reposition( );
      if ( this.open && this._block )
        this._showBlock();
      else if ( props.get( 'open' ) && this._block )
        this._hideBlock();
    };
  }


  async firstUpdated( props ) {
    await super.firstUpdated( props );
    await this.updateComplete;
    this._inheritGroupStyle();
  }


  /******************
   * Own methods
   *****************/


  /**
   * Toggle this menu's open state
   * @public
   */
  toggleOpen( ) {
    if ( this.open )
      this.open = false;
    else
      this.open = true;
  }


  /**
   * Inherit animation duration from CSS
   * @private
   */
  _inheritAnimateTime() {
    var styles, animTime;
    if ( this.animatetime === undefined ) {
      styles = window.getComputedStyle( this );
      animTime = styles.getPropertyValue( '--menu-animation-duration' );
      if ( animTime != "" && animTime != undefined )
        this.animatetime = parseFloat( animTime );
    }
  }


  /**
   * Reveal our block items with animation
   * @private
   */
  _showBlock() {
    const topMenu = this.closest( 'sc-menu-top' );
    const items = this.shadowRoot.getElementById( 'sc-menu-items' );
    const height = items.scrollHeight;

    // Resize the root block element with our kids
    topMenu.setAutoHeight();
    animate( items, {
      height: [ null, height + 'px' ]
    }, {
      duration: this.animatetime,
      easing: 'ease-in-out'
    } );
  }


  /**
   * Hide our block items with animation
   * @private
   */
  _hideBlock() {
    const items = this.shadowRoot.getElementById( 'sc-menu-items' );
    animate( items, {
      height: '0px'
    }, {
      duration: this.animatetime,
      easing: 'ease-in-out'
    } );
  }


  /**
   * Take the group style from our parent if possible
   * @private
   */
  _inheritGroupStyle() {
    const parnt = this.parentElement;
    const topMenu = this.closest( 'sc-menu-top' );
    this._block = topMenu.groupBlock;
    if ( parnt.tagName == 'SC-MENU-TOP' && parnt.mode == "bar" )
      this._noicon = true;
  }


  /**
   * Is the provided element a menu group or menu top element?
   * @private
   */
  _isMenu( elem ) {
    if ( elem.tagName == 'SC-MENU-TOP' || elem.tagName == "SC-MENU-GROUP" )
      return true;
    return false;
  }


  /**
   * Position our group relative to the parent group / top menu
   * @private
   */
  _reposition() {
    const label = this.shadowRoot.querySelector( '.sc-menu-group-name' ); 
    const popup = this.shadowRoot.querySelector( '.sc-menu-items' );

    let dir = this.direction;
    let popupw, popuph, blocky, bounds, ypos, xpos;

    // If block mode, remove positioning and stop
    blocky = this.closest( 'sc-menu-top' ).groupBlock;
    if ( blocky ) {
      popup.style.removeProperty( 'left' );
      popup.style.removeProperty( 'top' );
      return;
    }

    popupw = popup.clientWidth;
    popuph = popup.clientHeight;
    bounds = label.getBoundingClientRect();

    if ( dir == "auto" ) {
      // Determine position based on parent type, bars being up/down, popups being left/right

      if ( this.parentElement.tagName == 'SC-MENU-TOP' && this.parentElement.mode == "bar" ) {
        // Up or down
        dir = ( ( window.innerHeight - bounds.bottom ) < popuph ) ? "up" : "down";
      }
      else {
        // Left or right
        dir = ( ( window.innerWidth - bounds.right ) < popupw ) ? "left" : "right"
      }
    }

    // Calculate axis positions
    if ( dir == "left" || dir == "right" ) {
      xpos = ( dir == "left" ) ? ( popupw + bounds.width ) * -1 : bounds.width; 
      ypos = ( ( window.innerHeight - bounds.top ) < popuph ) ? ( popuph * -1 ) : bounds.height * -1;
    }
    else {
      xpos = 0;
      ypos = ( dir == "up" ) ? ( popuph + bounds.height ) * -1 : 0;
    }

    popup.style.setProperty( 'top', ypos + 'px' );
    popup.style.setProperty( 'left', xpos + 'px' );
  }


  /**
   * Stop propagation of clicks
   * @private
   */
  _itemClickStop(ev) {
    // Stop clicks from items acting on the parent
    ev.stopPropagation();
    return false;
  }


  /**
   * Start timer to hide ourself if a popup
   * @private
   */
  _mouseLeft( _0 ) {
    const par = this.parentElement;
    if ( this._block )
      return;
    if ( par.tagName == "SC-MENU-GROUP" )
      this._hiderFunc = setTimeout( () => { this.open = false }, 200 );
  }



  /**
   * Reveal on hover and cancel any hider function
   * @private
   */
  _mouseEnter( _0 ) {
    const par = this.parentElement;
    let siblings;
    if ( this._hiderFunc ) {
      clearTimeout( this._hiderFunc );
      this._hiderFunc = undefined;
    }
    if ( this._block )
      return;
    if ( this._isMenu( par ) && par.open ) {
      this.open = true;
      siblings = par.children;
      for ( const sibling of siblings ) {
        if ( sibling == this )
          continue;
        if ( sibling.open != undefined && sibling.open == true )
          sibling.open = false;
      }
    }
  }

}


/** 
 * The menu item component
 */
export class SimpleMenuItem extends SimpleComponent {

  constructor() {
    super();
  }

  static get _elementName() { return 'sc-menu-item' }

  static get properties() {
    return Object.assign( {}, super.properties, {

      label:        { type: String, def: "", desc: "The text label for this menu item" },
      navpath:      { type: String, def: "", desc: "Optional navigation path when activated" },
      navtag:       { type: String, def: "", desc: "Optional navigation hashtag when activated" },
      navquery:     { type: Object, def: {}, desc: "Optional navigation querystring when activated" },

      selected:     { type: Boolean, def: false, desc: "Is this menu item currently selected?" },
      disabled:     { type: Boolean, def: false, desc: "Is this menu item currently disabled?" },

      _block:       { type: Boolean, def: false },
      _depth:       { type: Number, attribute: "depth", def: undefined }
    } );
  }

  static get styles() {
    return super.styles.concat( [ itemMenuStyles ] );
  }

  render() {
    return this.builder`
      <div 
          class="sc-menu-item-name" 
          ?selected=${this.selected} 
          ?disabled=${this.disabled}
          ?blocky=${this._block}
          depth=${this._depth}
          @click=${this._navTo} 
        >
        ${this.navpath ? this.builder`<a href=${this.app.prefix + this.navpath} @click=${this._inhibitClick}>
          <span>${this.label}</span>
        </a>` : this.builder`<span>${this.label}</span>` }
      </div>
    `;
  }

  async firstUpdated( props ) {
    await super.firstUpdated( props );
    await this.updateComplete;
    this._inheritBlockStyle();
  }


  /*******************
   * Our methods
   ******************/

  /**
   * Check to see if we're a block item
   * @private
   */
  _inheritBlockStyle() {
    const par = this.closest( 'sc-menu-top' );
    this._block = par.groupBlock;
  }


  /**
   * Prevent the default link click action
   * @private
   */
  _inhibitClick( ev ) {
    ev.preventDefault();
  }



  /**
   * Completely close this menu
   * @public
   */
  closeMenu() {
    const menu = this.closest( 'sc-menu-top' );
    if ( menu )
      menu.hideAll();
  }


  /**
   * Handle a menu click, navigating as required, and closes the menu after
   * @private
   */
  _navTo() {
    if ( this.disabled )
      return;
    if ( this.navpath )
      this.app.navigate( this.navpath, this.navtag, this.navquery );
    this.closeMenu();
  }
}


