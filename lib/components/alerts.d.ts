export class SimpleAlerts extends SimpleComponent {
    static get _elementName(): string;
    zindex: number;
    render(): import("lit-html").TemplateResult<1>;
    /**
     * Add a simple text message alert
     * @public
     * @param {String} type                   - Type of alert, info, warning, error, success or task
     * @param {String} message                - Message for the alert
     * @param {String} title                  - Title for the alert, or undefined for none
     * @param {Object} [options]              - Alert option overrides
     * @param {Number} [options.duration]     - Duration for the alert, defaults to 0 (meaning no duration, static)
     * @param {String} [options.showstyle]    - Style to reveal the alert, popin, slidein, flash. Defaults to CSS defined style
     * @param {String} [options.hidestyle]    - Style to hide the alert, popout, rollup. Defaults to CSS defined style
     * @param {String} [options.iconposition] - Vertical icon position, middle or top. Defaults to middle.
     * @param {Boolean} [options.nouserclose] - If this alert cannot be closed by the user
     * @param {Boolean} [options.changeflash] - If the alert should graphically flash when altered, defaults to false
     * @returns {HTMLElement} alert           - The created alert
     */
    public addAlert(type: string, message: string, title: string, options?: {
        duration?: number;
        showstyle?: string;
        hidestyle?: string;
        iconposition?: string;
        nouserclose?: boolean;
        changeflash?: boolean;
    }): HTMLElement;
}
import { SimpleComponent } from '../index.js';
