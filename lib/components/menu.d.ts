/**
 * An event fired when the menu is activated or opened
 * @event SimpleMenuTop#click
 * @event SimpleMenuGroup#click
 * @event SimpleMenuItem#click
 */
/**
 * Top menu component either in bar or button form
 *
 * A top menu is one that has a button, bar or popup to show menu groups and items.
 */
export class SimpleMenuTop extends SimpleComponent {
    static get _elementName(): string;
    _hiderFunc: NodeJS.Timeout;
    _x: number;
    _y: number;
    render(): import("lit-html").TemplateResult<1>;
    renderPopup(): import("lit-html").TemplateResult<1>;
    renderBar(): import("lit-html").TemplateResult<1>;
    renderButton(): import("lit-html").TemplateResult<1>;
    willUpdate(changes: any): void;
    _grpblock: boolean;
    groupstyle: string;
    firstUpdated(props: any): Promise<void>;
    updated(props: any): void;
    /*******************
     * Our methods
     ******************/
    /**
     * Get the display mode of this top level menu, bar, button or popup
     * @type {String} - Top level display mode
     */
    get mode(): string;
    /**
     * Is the group menu a block style
     * @protected
     * @type {Boolean} - If the group is a block layout
     */
    protected get groupBlock(): boolean;
    /**
     * Open this popup menu at given coordinates relative to the viewport
     * @param {Number} x - X position
     * @param {Number} y - Y position
     */
    openAt(x: number, y: number): void;
    open: boolean;
    /**
     * Toggle this menu popup open or closed
     */
    toggleOpen(): void;
    /**
     * Recursively close all menus
     */
    hideAll(): void;
    /**
     * Reveal our block menu items and groups with animation
     * @private
     */
    private _showBlock;
    /**
     * Hide our block menu items and groups with animation
     * @private
     */
    private _hideBlock;
    /**
     * Set the height of the items block to auto for resize
     */
    setAutoHeight(): void;
    /**
     * Inherit animation duration from CSS
     * @private
     */
    private _inheritAnimateTime;
    animatetime: number;
    /**
     * Reposition the group popup element
     * @private
     */
    private _repositionPopup;
    /**
     * Start timer to hide the menu on mouse leave
     * @private
     */
    private _mouseLeft;
    /**
     * Cancel any timeer to hide the menu popup
     * @private
     */
    private _mouseEnter;
    /**
     * Close this menu when something else is clicked
     * @private
     */
    private _closeIfOutside;
    /**
     * Update the navigation menu to highlight the active navigation option
     * @private
     */
    private _updateNavMenu;
}
/**
 * The menu group component
 *
 * The menu group is essential a menu item with sub-items
 */
export class SimpleMenuGroup extends SimpleComponent {
    static get _elementName(): string;
    _x: any;
    _y: any;
    _hiderFunc: NodeJS.Timeout;
    render(): import("lit-html").TemplateResult<1>;
    willUpdate(changes: any): void;
    updated(props: any): void;
    firstUpdated(props: any): Promise<void>;
    /******************
     * Own methods
     *****************/
    /**
     * Toggle this menu's open state
     * @public
     */
    public toggleOpen(): void;
    open: boolean;
    /**
     * Inherit animation duration from CSS
     * @private
     */
    private _inheritAnimateTime;
    animatetime: number;
    /**
     * Reveal our block items with animation
     * @private
     */
    private _showBlock;
    /**
     * Hide our block items with animation
     * @private
     */
    private _hideBlock;
    /**
     * Take the group style from our parent if possible
     * @private
     */
    private _inheritGroupStyle;
    _block: any;
    _noicon: boolean;
    /**
     * Is the provided element a menu group or menu top element?
     * @private
     */
    private _isMenu;
    /**
     * Position our group relative to the parent group / top menu
     * @private
     */
    private _reposition;
    /**
     * Stop propagation of clicks
     * @private
     */
    private _itemClickStop;
    /**
     * Start timer to hide ourself if a popup
     * @private
     */
    private _mouseLeft;
    /**
     * Reveal on hover and cancel any hider function
     * @private
     */
    private _mouseEnter;
}
/**
 * The menu item component
 */
export class SimpleMenuItem extends SimpleComponent {
    static get _elementName(): string;
    render(): import("lit-html").TemplateResult<1>;
    firstUpdated(props: any): Promise<void>;
    /*******************
     * Our methods
     ******************/
    /**
     * Check to see if we're a block item
     * @private
     */
    private _inheritBlockStyle;
    _block: any;
    /**
     * Prevent the default link click action
     * @private
     */
    private _inhibitClick;
    /**
     * Completely close this menu
     * @public
     */
    public closeMenu(): void;
    /**
     * Handle a menu click, navigating as required, and closes the menu after
     * @private
     */
    private _navTo;
}
import { SimpleComponent } from '../index.js';
