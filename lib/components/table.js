
import { ResizeObserver as Polyfill } from '@juggle/resize-observer';

import { SimpleSelect, SimpleSelectOption, SimpleInput, SimpleBusyMask } from '../index.js';
import { SelectableListComponent} from './selectablelist.js';

import { library as faLib, icon as faIcon } from "@fortawesome/fontawesome-svg-core";
import { faSearch, faFilter, faSort, faSortAlphaDown, faSortAlphaDownAlt, faSortNumericDown, faSortNumericDownAlt } from "@fortawesome/free-solid-svg-icons";

import tableStyles from './table.css';

const ResizeObserver = window.ResizeObserver || Polyfill;

faLib.add( faSearch, faFilter, faSort, faSortAlphaDown, faSortAlphaDownAlt, faSortNumericDown, faSortNumericDownAlt );

// Prevent tree shaking
SimpleBusyMask.register();
SimpleInput.register();
SimpleSelectOption.register();
SimpleSelect.register();


/**
 * An event fired upon datatable data update
 * @event SimpleTable#updated
 */
const onDataUpdated = "updated";


const fieldEditableTypes = {
  'string': { element: "input", type: "text" },
  'number': { element: "input", type: "text" },
  // "enum":   { element: "select",
}


/**
 * @element sc-table
 * @attribute {Number} range              - Default range (displayed rows), 0 to disable pagination
 * @property {Number[]} ranges            - List of available range sizes
 * @attribute {Number} pagerange          - Number of pages either side of current page, eg 3 gives 1,2,3 for page 4
 * @attribute {Boolean} showpages         - Show the page selection control
 * @attribute {Boolean} showfooter        - Show the table footer ?? FIXME: containing
 * @attribute {Boolean} showrange         - Show the range selection control
 * @attribute {Boolean} showempty         - Show empty rows to fill up to page range or min rows
 * @attribute {Boolean} showsearch        - Show the table quicksearch control in a toolbar
 * @attribute {String} searchmode=ALL     - Quicksearch mode, AND/OR/ALL, ALL being the search string as is, AND/OR splits terms.
 * @attribute {String} searchtext=Search  - Quicksearch field placeholder text
 * @attribute {Boolean} showfilters       - Always show column filter inputs
 * @attribute {Boolean} scrollable        - Enable scrolling in the table
 * @attribute {Number} minrows=0          - Minimum number of rows to display (with showempty)
 * @attribute {String[]} sortfields       - Optional indexed field(s) to sort initially
 * @attribute {String[]} sorttypes        - Optional indexed list of sort types for fields
 * @attribute {String[]} sortdirections   - Optional indexed list of sort directions for fields
 *
 * @attribute {String} sortdir            - Direction for initial sort, asc or desc
 * @attribute {Boolean} multisort         - Enable sorts of multiple columns
 * @attribute {Boolean} usersortfirst     - Apply user-initiated sorts first
 * @attribute {Boolean} disabled          - Disable the table while set
 * @attribute {Boolean} busy              - Show busy mask while set
 * @attribute {Function} rowClasses       - Function to get a CSS class for a row, called with row record
 *
 * @property {ColumnType[]} fieldDefinitions - Table column definitions
 */
export class SimpleTable extends SelectableListComponent {


  /**
   * @emits {updated} After the table has updated displayed data
   */


  constructor() {
    super();

    // Private objects
    this.resizeobserver = new ResizeObserver( ( entries ) => {
      if ( ! Array.isArray(entries) || ! entries.length )
        return;
      this._bodyheight = entries[ 0 ].target.getBoundingClientRect().height;
    } );

    // Options (and defaults)
    this.editable         = false;

    // State properties
    this._doLoad          = false;
    this._editing         = undefined;
    this._position        = 0; /* Absolute start position in the sorted model */
    this._models          = {};
    this._quickfilters    = [];
    this._filterchange    = false;
    this._fieldfilters    = {};
    this._fixedfilters    = [];
    this._quicksearch     = "";
  }

  static get _elementName() { return 'sc-table' }

  static get properties() {
    return Object.assign( {}, super.properties, {

      range:              { type: Number, def: 10, desc: "Number of rows currently displayed, with 0 to show all rows" },
      ranges:             { type: Array, def: [ 5, 10, 25 ], desc: "A list of possible ranges to present to the user" },
      pagerange:          { type: Number, def: 3, desc: "Number of adjacent pages forward or backward to be visible in pagination controls" },
      showpages:          { type: Boolean, def: false, desc: "Show pagination controls" },
      showrange:          { type: Boolean, def: false, desc: "Show the range selection control" },

      showfooter:         { type: Boolean, def: false, desc: "Show the table footer" },
      showsearch:         { type: Boolean, def: false, desc: "Show the quick search control" },
      searchmode:         { type: String, def: "ALL", desc: "Quick search multi-term mode, ALL, AND or OR" },
      searchtext:         { type: String, def: 'Search', desc: "Quick search field placeholder text" },

      showempty:          { type: Boolean, def: false, desc: "Show empty rows if there are fewer rows than the range" },
      minrows:            { type: Number, def: 0, desc: "A minimum number of rows to display (with showempty and range of 0)" },

      showfilters:        { type: Boolean, def: false, desc: "Show column filters by default" },
      scrollable:         { type: Boolean, def: false, desc: "Enable scrolling of the table body" },

      multisort:          { type: Boolean, def: false, desc: "Enable multi-column sorting" },

      disabled:           { type: Boolean, def: false, desc: "Disable user interaction for this table" },
      busy:               { type: Boolean, def: false, reflect: true, desc: "Show this table's busy mask" },

      sortfields:         { type: Array, def: [], desc: "Indexed list of fields for initial sorting" },
      sortdirections:     { type: Array, def: [], desc: "Indexed list of sort directions for initial sorting" },
      sorttypes:          { type: Array, def: [], desc: "Indexed list of sort types for initial sorting" },
      usersortfirst:      { type: Boolean, def: false, desc: "Apply user-caused sorts first" },

      tableStyles:        { type: Array, attribute: false, def: undefined, desc: "An optional list of additional style CSSResults for this table instance" },
      fieldDefinitions:   { type: Array, attribute: false, def: undefined, desc: "Column defintions" },

      rowClasses:         { type: Function, attribute: false, def: undefined, desc: "Optional function to get a class name for a row, called with the row record" },

      editable            : { type: Boolean },

      _bodyheight         : { type: Number, def: undefined },
      _sorts              : { type: Array, def: [] },
      _filters            : { type: Object, def: {} },
      _needUpdate         : { type: Boolean, def: false },
      _editing            : { type: Object },
      _hasToolbar         : { type: Boolean, def: false }
    } );
  }


  /**
   * Return column property definitions
   */
  static get columnProperties() {
    return {
      label:            { type: String, def: undefined, desc: "Table header label for this column" },
      field:            { type: String, def: undefined, desc: "Data model field name for this column" },
      enablehtml:       { type: Boolean, def: false, desc: "Enable disply of HTML in the field value" },
      template:         { type: Function, def: undefined, desc: "Optional template builder function, called with the field value and row" },
      sortable:         { type: Boolean, def: false, desc: "If this column can be sorted" },
      sorttype:         { type: String, def: undefined, desc: "Sort type, number or string" },
      quicksearch:      { type: Boolean, def: false, desc: "If this column is included in any quick-search" },
      showfilter:       { type: Boolean, def: false, desc: "Show filter controls by default" },
      hidetoggle:       { type: Boolean, def: false, desc: "Hide the filter toggle control" },
      filtertype:       { type: String, def: undefined, desc: "Filter type, string, number, select or custom defaulting to no filtering. Note, select type does not function properly with IntegratedModel" },
      filtermode:       { type: String, def: "ALL", desc: "How string filters apply multiple terms, AND, OR or ALL, with ALL being string as is" },
      filtercase:       { type: Boolean, def: false, desc: "If string filters are case sensitive" },
      filtertext:       { type: String, def: 'Contains...', desc: "String filter control placeholder string" },
      multiselect:      { type: Boolean, def: false, desc: "If a select filter allows multiple selections" },
      customfilter:     { type: Function, def: undefined, desc: "Function to return a custom filter control, called with the table, and any current filter value" },
      customfiltertype: { type: String, def: 'eq', desc: "How the custom filter applies, supports <a href='models#criteriatype'>CriteriaType</a> types" },
      lookupmodel:      { type: String, def: undefined, desc: "Other model name to lookup data from" },
      lookupwith:       { type: String, def: undefined, desc: "This models field for data lookup" },
      lookupfield:      { type: String, def: undefined, desc: "Other models' field for data lookup" },
      lookupremote:     { type: String, def: undefined, desc: "Other models' field for display value" },
    }
  }


  /**
   * Build table column definitions from provided definitions
   * @param {Array<Object>} defs  - The unprocessed column definitions
   * @returns {Array<Object>}     - Processed column definitions with defaults applied
   */
  static toFieldDefintions( defs ) {
    const defaults = Object.keys( this.columnProperties ).reduce( (a, p) => { if ( this.columnProperties[ p ].def !== undefined ) a[ p ] = this.columnProperties[ p ].def; return a }, {} );
    let processed = [];
    for ( let def of defs )
      processed.push( Object.assign( {}, defaults, def ) );
    return processed;
  }


  static get styles() {
    return super.styles.concat( [ tableStyles ] );
  }


  //==========================
  // HTML templates
  //==========================

  render() {
    // Header intentionally after body
    return this.builder`
      <div class="sc-table-wrap">
        <sc-busy id="table-busy" fill ?busy=${this.busy}>
          <div class="sc-table">
            ${this.toolbarTemplate()}
            <div class="sc-table-scroll-wrap" ?scrollable=${this.scrollable} ?hastoolbar=${this.showsearch || this._hasToolbar}>
              <table ?disabled=${this.disabled}>
                ${this.bodyTemplate()}
                ${this.headerTemplate()}
                ${this.footerTemplate()}
              </table>
            </div>
            <div class="sc-table-controls" ?hidden=${! this.showpages && ! this.showrange} >
              ${this.rangeTemplate()}
              ${this.paginationTemplate()}
            </div>
          </div>
        </sc-busy>
      </div>
    `;
  }


  // Generate toolbar
  toolbarTemplate() {
    return this.builder`
      <div class="sc-table-toolbar" ?visible=${this.showsearch || this._hasToolbar}>
        ${this.searchTemplate()}
        <div class="sc-table-toolbar-left">
          <slot name="toolbarleft" @slotchange=${this._toolbarSlotted}>
          </slot>
        </div>
        <div class="sc-table-toolbar-right">
          <slot name="toolbarright" @slotchange=${this._toolbarSlotted}>
          </slot>
        </div>
      </div>`;
  }


  searchTemplate() {
    if ( ! this.showsearch )
      return this.builder``;
    return  this.builder`
      <slot name="search">
        <div class="sc-table-search-wrap">
          <div class="sc-table-search">
            <sc-input name="search" type="search" autocomplete=off placeholder="${this.searchtext}" @search=${this._setSearch} value=${this._quicksearch}>
              <div class="sc-table-search-icon" slot="iconbefore">
                ${this.includeSvg( faIcon( faSearch ).html[0] )}
              </div>
            </sc-input>
          </div>
        </div>
      </slot>
    `;
  }


  headerTemplate() {
    let defs = this.fieldDefinitions;
    return this.builder`
      <thead ?sticky=${this.scrollable}>
        <tr>
          ${defs.map( (d) => this.headerFieldTemplate( d ))}
        </tr>
      </thead>
    `
  }

  headerFieldTemplate( column ) {
    const sorttype = column.sorttype;
    const sortnum = this._sorts.filter( s => s.hidden == false ).findIndex( s => s.field == column.field );
    const sorty = sortnum !== -1 ? this._sorts[ sortnum ] : undefined;
    const sortDir = sorty !== undefined ? sorty.direction : undefined;
    let sortIcon, hasfilter = false, showfilter = false;

    if ( sorty && sortDir ) {
      if ( sortDir == "asc" )
        sortIcon = ( sorttype == "number" ) ? faIcon( faSortNumericDown ).html[0] : faIcon( faSortAlphaDown ).html[0];
      else if ( sortDir == "desc" )
        sortIcon = ( sorttype == "number" ) ? faIcon( faSortNumericDownAlt ).html[0] : faIcon( faSortAlphaDownAlt ).html[0];
    }
    else if ( column.sortable )
      sortIcon = faIcon( faSort ).html[0];
    if ( column.filtertype !== undefined )
      hasfilter = true;
    if ( this.showfilters || column.showfilter )
      showfilter = true;
    return this.builder`
      <th
          data-column=${column.field}
          class="column-${column.field} ${ column.sortable ? 'sortable' :'' } ${sorty ? sorty.direction : ''}"
          ?sorting=${sorty && sorty.field == column.field}
          @dblclick=${this._sortIfSortable}
      >
        <div class="sc-table-column-title">
          <span class="sc-table-column-title-text ${showfilter ? `showfilter` : ``}">
            ${column.label}
            ${ ( ! column.hidetoggle && hasfilter ) ? this.builder`
              <div class="sc-table-column-title-search-icon" @click=${this._toggleFieldFilter} >
                ${this.includeSvg(faIcon(faFilter).html[0])}
              </div>` : ``
            }
            <div class="sc-table-column-title-search-input ${showfilter ? 'showfilter' : ''}">
              ${this.filterTemplate(column)}
            </div>
          </span>
          <span class="sc-table-column-title-sort" @click=${this._sortIfSortable}>
            ${sorty && this.multisort ? sortnum + 1 : ''}
            ${this.includeSvg(sortIcon)}
          </span>
        </div>
      </th>`
  }


  bodyTemplate() {
    const showdata = Array.from( this._data );
    let wantlength = 0;

    if ( this.showempty ) {

      if ( this.range == 0 )
        wantlength = this.minrows;
      else
        wantlength = this.range;

      while ( showdata.length < wantlength ) {
        showdata.push( {} );
      }
    }

    return this.builder`
      <tbody>
        ${ showdata.map( row => this.rowTemplate(row) ) }
      </tbody>`;
  }


  rowTemplate( row ) {
    const defs = this.fieldDefinitions;
    const classFunc = this.rowClasses;
    const blank = row[ this.idField ] == undefined;
    let classes, meta = {};

    if ( ! blank && this.dataModel && this.dataModel.getRecordInfo )
      meta = this.dataModel.getRecordInfo( row[ this.idField ] ) || {};

    if ( classFunc )
      classes = classFunc( row );

    return this.builder`
      <tr
          data-id=${row[this.idField]}
          data-type=${typeof row[this.idField]}
          data-selected=${this.isSelected(row[this.idField]) ? "true":"false"}
          data-status=${this.ifDefined( meta.operation )}
          data-message=${this.ifDefined(meta.message)}
          data-errored=${this.ifDefined(meta.errored)}
          @click=${this.handleSelectClick}
          @dblclick=${this.handleDblClick}
          @contextmenu=${this.handleContextMenu}
          @mousedown=${this.hinderTextSelect}
          class=${this.ifDefined( classes )}
      >
        ${defs.map( def => this.cellTemplate(def, row) ) }
      </tr>
    `
  }


  filterTemplate( column ) {
    const model = this.getModel();
    const filtertype = column.filtertype;
    let vals = [], currentval = '', customfunc, maxselect;

    if ( ! filtertype )
      return this.builder``;

    currentval = this._fieldfilters[ column.field ];
    if ( currentval === undefined )
      currentval = '';

    switch( filtertype ) {
      case "string":
        return this.builder`
          <sc-input type="search" placeholder="${column.filtertext}" @search=${this._doFieldFilter} value=${currentval}></sc-input>
        `
      case "number":
        return this.builder`
          <sc-input type="search" placeholder="${column.filtertext}" pattern="\d*" @after-change=${this._doFieldFilter} value=${currentval}></sc-input>
        `
      case "select":
        vals = model.uniqueValues( column.field );
        maxselect = column.multiselect ? 0 : 1;
        return this.builder`
          <sc-select @after-change=${this._doFieldFilter} maxheight=${this._bodyheight} position="bottom" maxselect=${maxselect} placeholder="All values">
            ${vals.map( v => this.builder`<sc-select-option data-type=${typeof v} ?selected=${currentval===v} value=${v} label=${v}></sc-select-option>` ) }
          </sc-select>
        `
      case "custom":
        customfunc = column.customfilter;
        return this.builder`${customfunc( this, currentval )}`;
    }
  }


  cellTemplate( def, row ) {
    let value = row[ def.field ];
    if ( Object.keys( row ).length == 0 )
      return this.builder`<td data-field=${def.field}><span>&nbsp;</span></td>`
    if ( value === undefined && def.hasOwnProperty( def.field ) )
      return this.builder`<td data-field=${def.field}><span>&nbsp;</span></td>`
    if ( def.editable == true )
      return this._editableRowTemplate( def, value );
    if ( def.lookupmodel != undefined )
      value = this._lookupValue( def, row );
    if ( def.template != undefined )
      return this.builder`<td data-field=${def.field}>${def.template(value, row)}</td>`;
    else if ( def.enablehtml )
      return this.builder`<td data-field=${def.field}><span>${this.includeHtml(value)}</span></td>`
    else
      return this.builder`<td data-field=${def.field}><span>${value}</span></td>`
  }


  _editableRowTemplate( field, val ) {
    // FIXME: support templated editable fields
    return this.builder`
      <td @dblclick=${this._tryEditing} data-field=${f.field}>
        ${this.editableInputTemplate( field,val )}
        <span>${val}</span>
      </td>`;
  }


  editableInputTemplate( ) {
    // FIXME: Implement
    return this.builder`
      <input/>
    `;
  }


  // FIXME: what display?
  footerTemplate() {
    if ( ! this.showfooter )
      return this.builder``
    var count = this.fieldDefinitions.length;
    return this.builder`
      <tfoot>
        <tr><td colspan=${count}></td></tr>
      </tfoot>
    `
  }

  rangeTemplate() {
    const count = this.total;
    if ( ! this.showrange || this.range == 0 )
      return this.builder``;
    return this.builder`
      <div class="ranges">
        <label>Display:</label>
        <select id="range-select" ?disabled=${this.disabled} @change=${this._selectRange}>
          ${this.ranges.map( (r) => this.builder`<option ?selected=${r == this.range} value=${r}>${r}</option>` ) }
        <select>
        <span>of ${count} records ${ ( this._filters[ ':rules' ] && this._filters[ ':rules' ].length ) ? '(filtered)' : ''}</span>
      </div>
    `
  }


  paginationTemplate() {
    let curpage, maxpages;
    if ( ! this.showpages || this.range == 0 )
      return this.builder``;
    curpage = Math.ceil( ( this._position + 1 ) / this.range );
    maxpages = Math.ceil( this.total / this.range );
    if ( maxpages == 0 )
      maxpages = 1;
    return this.builder`
      <div class="pagination">
        <button id="first-page" ?disabled=${curpage == 1 || this.disabled} @click=${this._selectPage} data-page="first" type="button">First</button>
        <button id="prev-page" ?disabled=${curpage == 1 || this.disabled} @click=${this._selectPage} data-page="prev" type="button">Previous</button>
        ${this.pageButtonsTemplate()}
        <button id="next-page" ?disabled=${curpage == maxpages || this.disabled} @click=${this._selectPage} data-page="next" type="button">Next</button>
        <button id="last-page" ?disabled=${curpage == maxpages || this.disabled} @click=${this._selectPage} data-page="last" type="button">Last</button>
      </div>
    `
  }


  pageButtonsTemplate() {
    const pages = [];
    const curpage = Math.ceil( ( this._position + 1 ) / this.range );
    let maxpages = Math.ceil( this.total / this.range );
    let begin = curpage - this.pagerange;
    let end = curpage + this.pagerange;
    if ( maxpages == 0 ) maxpages = 1;
    if ( begin < 1 ) begin = 1;
    if ( end > maxpages ) end = maxpages;
    for( var a=begin; a <= end; a++ ) { pages.push( a ); }
    return this.builder`${pages.map( (p) => this.builder`
      <button type="button" ?disabled=${p == curpage || this.disabled} @click=${this._selectPage} data-page=${p}>${p}</button>
    `)}`
  }


  /**
   * Lookup a display value from another model
   * @param {Object} fieldDef     - Column definition
   * @param {Object} record       - Local model's record
   */
  _lookupValue( fieldDef, record ) {
    let model, remoteRecord;

    const modelName = fieldDef[ 'lookupmodel' ]; /* Name of the model */
    const lookupBy = fieldDef[ 'lookupwith' ]; /* Local data field for lookup */
    const lookupField = fieldDef[ 'lookupfield' ]; /* Field used to locate other model's record */
    const displayField = fieldDef[ 'lookupremote' ]; /* Field of lookup record to display */

    model = this._models[ modelName ];
    remoteRecord = model.searchOne( lookupField, record[ lookupBy ] );
    if ( remoteRecord )
      return remoteRecord[ displayField ];
    else
      return "";
  }


  hinderTextSelect(ev) {
    if ( ev.ctrlKey || ev.shiftKey ) {
      ev.preventDefault();
    }
  }


  //=======================
  // Lit methods
  //=======================


  shouldUpdate( changes ) {
    let filterChange = false;

    // Finished a data reload only? Don't propagate.
    if ( changes.has( '_needUpdate' ) && ! this._needUpdate && changes.size == 1 )
      return false;

    // Check if a filter update is needed
    for ( let key of changes.keys() ) {
      if ( [ 'searchmode', 'fieldDefinitions' ].includes( key ) )
        filterChange = true;
    }
    if ( filterChange )
      this._filterchange = true;

    if ( filterChange || ( changes.has( '_needUpdate' ) && this._needUpdate ) )
      this._doLoad = true;

    return true;
  }


  willUpdate( changes ) {
    const defsorts = [];
    super.willUpdate( changes );

    // Trim multiple sorts if disabling
    if ( changes.has( 'multisort' ) && ! this.multisort && this._sorts > 1 )
      this._sorts = this._sorts.slice( 0, 1 );

    // First render?
    if ( ! this.hasUpdated ) {
      this._fetchLookupModels();

      // Use any default sorting
      if ( this._sorts.length == 0
          && this.sortfields.length
          && this.sortdirections.length
          && this.sorttypes.length
      ) {

        for ( let idx in this.sortfields ) {
          defsorts.push( {
            'field'       : this.sortfields[ idx ],
            'direction'   : this.sortdirections[ idx ],
            'type'        : this.sorttypes[ idx ]
          } );
        }

        this.applySort( defsorts );
      }
    }

    // Load from model as needed
    if ( this._doLoad || this.contextchange ) {
      this._loadFromModel(); // Note, async
      this._doLoad = false;
    }

  }


  firstUpdated() {
    if ( this.tableStyles )
      this.adoptCustomStyles();

    this.resizeobserver.observe( this.shadowRoot.querySelector( 'tbody' ) );

    this._prepareFieldFilters();
  }


  async updated( props ) {
    await super.updated( props );

    // If data is updated, flash the table to let the eye know
    if ( props.has( '_data' ) && props.get( '_data' ) !== undefined )
      this._flashTable();
  }



  //======================
  // Own methods
  //======================

  /**
   * Adopt styles into the busy element too
   */
  adoptCustomStyles( styles ) {
    const toAdopt = [];
    const busy = this.shadowRoot.querySelector( 'sc-busy' );
    let additional;
    if ( this.tableStyles ) {
      additional = Array.isArray( this.tableStyles ) ? this.tableStyles : [ this.tableStyles ];
      for ( let addit of additional )
        toAdopt.push( addit );
    }
    if ( styles )
      toAdopt.push( styles );
    busy.adoptCustomStyles( toAdopt );
    super.adoptCustomStyles( toAdopt );
  }


  /**
   * Remove our data
   * @public
   */
  clear() {
    this._data = [];
  }


  /**
   * Reset this table, removing data, filters and sorts
   * @public
   */
  reset() {
    this._data = [];
    this._sorts = [];
    this._filters = {};
    this.shadowRoot.querySelectorAll( 'input' ).forEach( input => input.value = '' );
  }


  /**
   * Get total number of records in the model
   * @public
   * @type {Number}           - Total number of records in the model
   */
  get total() {
    return ( this._data.filterCount !== undefined ) ? this._data.filterCount : this._data.totalCount || 0;
  }


  /**
   * Get number of records last fetched
   * @public
   * @type {Number}           - Total number of fetched records
   */
  get length(){
    return this._data.fetchCount;
  }



  /**
   * Replace sorting of this table with provided sort criteria
   * @public
   * @param {Object[]}  sorts                     - A list of sorting criteria
   * @param {String}    sorts[].field             - The name of the field to sort on
   * @param {Function} [sorts[].sortFunc]         - A custom sorting function used
   * @param {String}   [sorts[].direction]        - The direction to sort, asc or desc
   * @param {String}   [sorts[].type]             - Sort type, number or string if not a defined column
   * @throws                                      - If unable to sort for any reason
   */
  applySort( sorts ) {
    const model = this.getModel();
    const checked = [];
    const alldefs = this.fieldDefinitions;
    let coldef, hidden;

    // No model yet?
    if ( ! model )
      return;

    if ( ! this.multisort && sorts.length > 1 )
      throw new Error( "This table does not support multiple sort criteria" );

    // Check each field is known to the model
    for ( let {
        field       : sortfield,
        direction   : sortdirection,
        type        : sorttype,
        func        : sortfunction
    } of sorts ) {

      // Ensure sort field is known
      if ( ! model.hasField( sortfield ) )
        throw new Error( `Sort field ${sortfield} is not known to model` );

      // Get the column definition
      coldef = alldefs.find( (d) => d.field == sortfield );
      hidden = coldef ? false : true;

      // Check we have a sort type
      if ( coldef )
        sorttype ||= coldef.sorttype;
      if ( ! sorttype )
        throw new Error( `Sort type not specified for sort field ${sortfield}` );
      if ( sorttype !== "string" && sorttype !== "number" )
        throw new Error( `Unknown sort type ${sorttype}` );

      // Check direction
      if ( ! sortfunction && sortdirection !== "desc" && sortdirection !== "asc" )
        throw new Error( "Unknown sort direction " + sortdirection );

      // All checked
      checked.push( {
        field       : sortfield,
        type        : sorttype,
        direction   : sortdirection,
        func        : sortfunction,
        hidden      : hidden
      } );
    }

    this._sorts = checked;
  }


  /**
   * Apply a field filter to this table
   * @public
   * @param {String} field            - The field to filter
   * @param {String|Number} filterval - The value to filter with
   */
  applyFieldFilter( field, filterval ) {
    const defs = this.fieldDefinitions;
    const fieldfilters = this._fieldfilters;
    let coldef;

    coldef = defs.find( (d) => d.field == field );
    if ( ! coldef )
      throw new Error( `Unknown field ${field}` );
    if ( ! coldef.filtertype )
      throw new Error( `Filters not enabled on field ${field}` );

    if ( filterval === undefined || filterval === "" )
      delete fieldfilters[ field ];
    else
      fieldfilters[ field ] = filterval;
    this._needUpdate = true;
    this._filterchange = true;
  }


  /**
   * Get the field filter for a field
   * @public
   * @param {String} field        - The field
   * @returns {String|Number}     - The string or number applied, or undefined if no filter
   */
  getFieldFilter( field ) {
    const defs = this.fieldDefinitions;

    if ( ! defs.find( (d) => d.field == field ) )
      return undefined;

    return this._fieldfilters[ field ];
  }


  /**
   * Apply a set of non-interactive supplementary filters to this table
   * @public
   * @param {FiltersCollection} filters   - The filters to apply
   * @throws                              - If filters cannot be applied
   */
  applyFixedFilters( filters ) {
    this._fixedfilters = this.app.util.clone( filters );
    this._needUpdate = true;
    this._filterchange = true;
  }


  /**
   * Get the non-interactive supplementary filters applied to this table
   * @public
   * @returns {FiltersCollection}   - The filters applied
   */
  getFixedFilters() {
    return this.app.util.clone( this._fixedfilters );
  }


  /**
   * Get a copy of all current filters applied to this table
   * @public
   * @returns {Array<Function|FilterType>}  - The filters applied to this table
   */
  getFilters() {
    return this.app.util.clone( this._filters );
  }


  /**
   * Get all displayed table rows
   * @protected
   */
  getDisplayedItems() {
    var rows = Array.from( this.shadowRoot.querySelectorAll( 'tbody tr' ) );
    return rows;
  }


  /**
   * Handle a model status change
   * @protected
   */
  _statusChanged( type ) {
    super._statusChanged( type );
    switch( type ) {
      case 'disable':
        this.disabled = true;
        break;
      case 'enable':
        this.disabled = false;
        break;
      case 'bind':
      case 'online':
        this._needUpdate = true;
        break;
      case 'unbind':
      case 'cleared':
        this.clear();
        break;
      case 'pending':
        this.busy = true;
        break;
      case 'failure':
      case 'complete':
        this.busy = false;
        break;

    }
  }


  /**
   * Apply a quick search
   * @param {Event} ev        - Change event
   */
  _setSearch( ev ) {
    this._quicksearch = ev.target.value;
    this.clearSelection();
    this._needUpdate = true;
    this._filterchange = true;
  }


  /**
   * Check for slotted toolbar items
   */
  _toolbarSlotted() {
    const toolbarleft = this.shadowRoot.querySelector( '.sc-table-toolbar-left' );
    const toolbarright = this.shadowRoot.querySelector( '.sc-table-toolbar-right' );
    const slotleft = toolbarleft.firstElementChild;
    const slotright = toolbarright.firstElementChild;
    const kidsl = slotleft.assignedNodes().filter(n => n.nodeType === Node.ELEMENT_NODE);
    const kidsr = slotright.assignedNodes().filter(n => n.nodeType === Node.ELEMENT_NODE);
    this._hasToolbar = ( kidsl.length + kidsr.length ) ? true : false;
  }


  /**
   * Prepare filter controls
   */
  async _prepareFieldFilters() {
    const header = this.shadowRoot.querySelector( 'thead' );
    const controls = header.querySelectorAll( 'th .sc-table-column-title-search-input' );
    let minheight;
    if ( this.showfilters )
      return;
    await this.updateComplete;
    for ( let control of controls ) {
      minheight = control.getBoundingClientRect().height;
      control.parentElement.style.setProperty( 'min-height', minheight );
      if ( ! control.classList.contains( 'showfilter' ) )
        control.classList.add( 'hidefilter' );
    }
  }


  /**
   * Toggle the filter control
   * @param {MouseEvent} ev     - Click event
   */
  _toggleFieldFilter( ev ) {
    const header = ev.target.closest( 'th' );
    const field = header.dataset.column;
    const control = header.querySelector( '.sc-table-column-title-search-input' );
    let minheight, needUpd = false;

    if ( ! control.firstElementChild == null )
      return;

    if ( control.classList.contains( 'showfilter' ) ) {
      if ( this._fieldfilters.hasOwnProperty( field ) ) {
        delete this._fieldfilters[ field ];
        needUpd = true;
      }
      minheight = control.getBoundingClientRect().height;
      control.parentElement.style.setProperty( 'min-height', minheight );
      control.classList.remove( 'showfilter' );
      control.classList.add( 'hidefilter' );
    }
    else {
      control.classList.remove( 'hidefilter', 'hiddenfilter' );
      control.classList.add( 'showfilter' );
    }

    if ( needUpd ) {
      this._needUpdate = true;
      this._filterchange = true;
    }
  }


  /**
   * Take a field and enable or disable a filter
   * @param {Event} ev            - The change event
   * @see BasicCollection#CriteriaType
   */
  _doFieldFilter( ev ) {
    const input = ev.target;
    const header = input.closest( 'th' );
    const defs = this.fieldDefinitions;
    let coldef, field, value, remove = false;

    if ( ! header || ! header.dataset )
      return;
    field = header.dataset.column;

    coldef = defs.find( (c) => { return c.field == field } );
    if ( ! coldef )
      return;

    value = input.value;
    if ( value === "" || value === undefined || value === null || value.length == 0 )
      remove = true;

    if ( remove ) {
      if ( this._fieldfilters.hasOwnProperty( field ) ) {
        delete this._fieldfilters[ field ];
        this._filterchange = true;
        this._needUpdate = true;
      }
      return;
    }

    this._fieldfilters[ field ] = value;
    this._filterchange = true;
    this._needUpdate = true;
  }



  /**
   * Fetch any models that will be required for value lookups and listen for changes
   * @private
   */
  _fetchLookupModels() {
    var model;
    var defs = this.fieldDefinitions;
    for ( let def of defs ) {
      if ( def.hasOwnProperty( 'lookupmodel' ) ) {
        if ( ! this._models.hasOwnProperty( def[ 'lookupmodel' ] ) ) {
          model = this.app.fetchModel( def[ 'lookupmodel' ] );
          if ( ! this._models.hasOwnProperty( def[ 'lookupmodel' ] ) ) {
            this._models[ def[ 'lookupmodel' ] ] = model;
            model.addEventListener( 'data-changed', () => {
              this.requestUpdate();
            } );
          }
        }
      }
    }
  }


  /**
   * Update our data from the model, causing a display update
   * @protected
   * @async
   * @fires SimpleTable#updated      - When the data is updated
   * @see DataboundComponent#_dataChanged
   */
  async _dataChanged( detail ) {
    this.app.log( "debug", this.constructor.name, "data model changed, queuing update" );

    super._dataChanged( detail );

    // Always re-load from model
    this._needUpdate = true;
  }


  /**
   * Populate our data from the model
   * @private
   * @async
   */
  async _loadFromModel() {
    let filters, ev;

    this._needUpdate = false;

    // No model or not bound?
    if ( ! this.getModel() )
      return;
    if ( ! this.bound )
      return;

    // Skip loading if the model missing context
    if ( ! this.canFetch() ) {
      this.app.log( "debug", "Model not ready for fetch" );
      return;
    }

    this.app.log( "debug", "Updating table data from model" );

    try {
      filters = this._buildFilters();

      // Fetch sorted and filtered data
      this._data = await this.getModel().fetch( this._position, this.range, this._sorts, filters );

      // Let listeners know
      ev = new CustomEvent( onDataUpdated, { bubbles: false, cancelable: false, composed: true } );
      this.dispatchEvent( ev );

      // Reset pagination if filters push current position out of range
      if ( this._data.filterCount < this._position ) {
        this._position = 0;
        this._needUpdate = true;
      }
    }
    catch( err ) {
      // Just conain errors as model status updates will propagate states
      this.app.log( "warn", `Unable to load records: ${err.message}` );
    }
  }


  /**
   * Build or rebuild filters as needed
   * @return {FiltersCollection}  - Built filters
   */
  _buildFilters() {
    const defs = this.fieldDefinitions;
    const qfmode = this.searchmode;
    const fieldfilters = this._fieldfilters;
    const fixedfilters = this._fixedfilters;
    const quicksearch = this._quicksearch;
    const filters = [];
    let filter, stringmode, mode, subfilter, subfilters, coldef, val, type, qsfilter = {};

    // Only rebuild if changed
    if ( ! this._filterchange )
      return this._filters;

    this._filterchange = false;

    // Fixed filters are first
    if ( fixedfilters.length ) {
      for ( let filter of fixedfilters )
        filters.push( filter );
    }

    // Build field filters
    for ( let field in fieldfilters ) {
      filter = {};

      coldef = defs.find( (c) => { return c.field == field } );
      if ( ! coldef || ! coldef.filtertype )
        continue;

      val = fieldfilters[ field ];

      // Work out mode and string mode
      stringmode = coldef[ 'filtermode' ] || 'ALL';
      mode = stringmode;
      if ( mode == 'ALL' )
        mode = 'AND';

      switch( coldef.filtertype ) {
        case "string":
          type = "ct";
          break;
        case "number":
          type = "ct";
          break;
        case "select":
          type = "in";
          break;
        case "custom":
          type = coldef.customfiltertype || 'eq';
      }

      // ?? no filter type set
      if ( ! type )
        continue;

      // Generate subfilters for string like types
      subfilters = [];
      if ( ( stringmode == "OR" || stringmode == "AND" ) && type !== "in" ) {
        for ( let term of val.split( ' ' ) ) {
          subfilter = {};
          subfilter[ ':case' ] = ( coldef[ 'filtercase' ] ) ? true : false;
          subfilter[ type ] = term;
          subfilters.push( subfilter );
        }
      }

      // IN type
      if ( type == 'in' ) {
        filter[ field ] = {
          ':rules': [],
          ':mode': 'OR'
        };
        if ( Array.isArray( val ) )
          filter[ field ][ ':rules' ].push( { 'in': val, ':case': ( coldef[ 'filtercase' ] ) ? true : false } );
        else if ( stringmode == "OR" || stringmode == "AND" )
          filter[ field ][ ':rules' ].push( { 'in': val.split( ' ' ), ':case': ( coldef[ 'filtercase' ] ) ? true : false } );
        else
          filter[ field ][ ':rules' ].push( { 'in': [ val ], ':case': ( coldef[ 'filtercase' ] ) ? true : false } );
      }
      else {
        // All other types, with subfilters and without
        if ( subfilters.length ) {
          filter[ field ] = {
            ':rules': subfilters,
            ':mode': mode
          }
        }
        else {
          // Simple equality or without subfilters
          subfilter = {};
          subfilter[ type ] = val;
          subfilter[ ':case' ] = ( coldef[ 'filtercase' ] ) ? true : false;
          filter[ field ] = {
            ':rules': [ subfilter ],
            ':mode': mode
          };
        }
      }

      filters.push( filter );
    }

    // Build quicksearch filters
    if ( quicksearch && quicksearch !== "" ) {
      qsfilter[ ':rules' ] = [];
      qsfilter[ ':mode' ] = "OR";

      if ( quicksearch.includes( ' ' ) && ( qfmode === "OR" || qfmode === "AND" ) ) {
        for ( let coldef of defs ) {
          if ( coldef.hasOwnProperty( 'quicksearch' ) && coldef.quicksearch ) {
            filter = {};
            filter[ coldef.field ] = {
              ':rules': [],
              ':mode': qfmode
            };
            for ( let term of quicksearch.split( ' ' ) ) {
              subfilter = {};
              subfilter[ 'ct' ] = term;
              subfilter[ ':case' ] = ( coldef[ 'filtercase' ] ) ? true : false;
              filter[ coldef.field ][ ':rules' ].push( subfilter );
            }
            qsfilter[ ':rules' ].push( filter );
          }
        }
      }
      else { // Complete string
        for ( let coldef of defs ) {
          if ( coldef.hasOwnProperty( 'quicksearch' ) && coldef.quicksearch ) {
            filter = {};
            filter[ coldef.field ] = {};
            filter[ coldef.field ][ ':rules' ] = [ { 'ct': quicksearch, ':case': ( coldef[ 'filtercase' ] ) ? true : false } ];
            filter[ coldef.field ][ ':mode' ] = 'AND';
            qsfilter[ ':rules' ].push( filter );
          }
        }
      }

      filters.push( qsfilter );
    }

    this._filters = { ':rules': filters };
    return this._filters;
  }


  /**
   * Attempt to edit a cell
   * @private
   * FIXME: Unimplemented
   */
  _tryEditing( ) {
    return;
  }


  /**
   * Get all available selected IDs
   * @return {String[]|Number[]}      - The IDs for all selected items
   */
  getSelected() {
    let ourids;
    let selected = [];

    // If any selections aren't in our dataset, return just those that do
    if ( this._selected.length ) {
      ourids = this._data.map( rec => rec[ this.idField ] );
      for ( let id of this._selected ) {
        if ( ourids.includes( id ) )
          selected.push( id );
      }
      return selected;
    }

    return super.getSelected();
  }



  /**
   * Apply a sort if possible when attempted on a column
   * @private
   * @param {MouseEvent} ev       - The originating mouse event
   */
  _sortIfSortable( ev ) {
    const defs = this.fieldDefinitions;
    const column = ev.target.closest( 'th' ).dataset.column;
    const coldef = defs.find( (c) => { return c.field == column } );
    const sorty = this._sorts.find( s => s.field == column );
    const source = ev.target;
    let firstidx;

    // Only events from the heading or sort icon
    if ( ! source.classList.contains( 'sc-table-column-title-text' ) &&
        ! source.classList.contains( 'sc-table-column-title-sort' ) &&
        source.closest( '.sc-table-column-title-sort' ) === null
    )
      return;

    if ( ! coldef || ! coldef.sortable )
      return;

    ev.preventDefault();
    ev.stopPropagation();

    // Clear selection if sorting
    this._selected = [];

    // Remove the column sort
    if ( sorty && sorty.direction == "desc" ) {
      this._sorts = this._sorts.filter( s => s.field !== column );
    }
    else if ( sorty && sorty.direction == "asc" ) { // Change direction
      sorty.direction = "desc";
    }
    else { // New sort
      // Clear if only one sort possible
      if ( ! this.multisort )
        this._sorts = [];
      if ( this.usersortfirst ) {
        firstidx = this._sorts.findIndex( f => f.hidden );
        if ( ! firstidx )
          firstidx = 0;
        this._sorts.splice( firstidx, 0, {
          field       : column,
          direction   : "asc",
          type        : coldef.sorttype,
          hidden      : false
        } );
      }
      else {
        this._sorts.push( {
          field       : column,
          direction   : "asc",
          type        : coldef.sorttype,
          hidden      : false
        } );
      }
    }

    this._needUpdate = true;
  }


  /**
   * Paginate in some way
   * @private
   */
  _selectPage( ev ) {
    const maxpages = Math.ceil( this.total / this.range );
    let wantpage;

    switch( ev.target.dataset.page ) {
      case "first":
        this._position = 0;
        break;
      case "next":
        this._position = this._position + this.range;
        break;
      case "prev":
        this._position = this._position - this.range;
        break;
      case "last":
        this._position = (maxpages * this.range ) - this.range;
        break;
    }

    // Numbered button
    wantpage  = parseInt( ev.target.dataset.page, 10 );
    if ( ! isNaN( wantpage ) )
      this._position = (wantpage * this.range ) - this.range;

    this._needUpdate = true;
  }


  /**
   * Change the range of results
   * @private
   */
  _selectRange( ev ) {
    var newrange = parseInt( ev.target.value, 10 );
    if ( newrange != this.range ) {
      this.range = newrange;
      this._needUpdate = true;
    }
  }


  /**
   * Flash the table to let the eye know of a change
   * @private
   */
  _flashTable() {
    const table = this.shadowRoot.querySelector( 'tbody' );
    table.classList.remove( 'flash-table' );
    setTimeout( () => { table.classList.add( 'flash-table' ) }, 0 );
  }


}


