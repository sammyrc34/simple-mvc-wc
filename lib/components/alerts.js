
import { SimpleComponent } from '../index.js';

import { library as faLib, icon as faIcon } from "@fortawesome/fontawesome-svg-core";
import { faSpinner, faExclamationTriangle, faCheckCircle, faInfoCircle, faTimesCircle } from "@fortawesome/free-solid-svg-icons";

import alertContainerStyles from './alerts-1.css';
import alertStyles from './alerts-2.css';


faLib.add( faSpinner, faExclamationTriangle, faCheckCircle, faInfoCircle, faTimesCircle );



class SimpleAlert extends SimpleComponent {
  constructor() {
    super();

    this.hidestyles   = [ 'rollUp', 'popOut', 'fadeOut', 'slideRight', 'slideLeft' ]; 
    this._timer        = undefined;
  }
  static get _elementName() { return 'sc-alert' }
  static get properties() {
    return Object.assign( {}, super.properties, {

      type:         { type: String, def: "info", desc: "Type of alert, either info, success, warning, error or task" },
      duration:     { type: Number, def: undefined, desc: "Duration of the alert in milliseconds, 0 meaning static, undefined meaning CSS define default" },
      title:        { type: String, def: "", desc: "Title to display in the alert" },
      message:      { type: String, def: "", desc: "The message to display in the alert" },

      iconposition: { type: String, def: "middle", desc: "Vertical position of any alert icon, either top or middle" },
      nouserclose:  { type: Boolean, def: false, desc: "Disable closure of this alert by the user" },
      showstyle:    { type: String, def: "defshow", desc: "Reveal style, either popin, slidein, popflash, flash or defshow for the CSS defined default" },
      hidestyle:    { type: String, def: "defhide", desc: "Hide style, either rollup, popout, sideslide or defhide for CSS defined default" },
      changeflash:  { type: Boolean, def: false, desc: "Flash this alert to draw attention when changing" },

      collapsed:    { type: Boolean, reflect: true, def: false },
    } );
  }
  static get styles() {
    return super.styles.concat( [ alertStyles ] );
  }
  render() {
    return this.builder`
      <div 
          class="sc-alert ${this.type} ${this.collapsed ? this.hidestyle : this.showstyle}" 
          ?collapsed=${this.collapsed} 
          @click=${this._tryclose}
          @animationend=${this._animEnd}
      >
        <div class="sc-alert-sizing">
          <div class="sc-alert-bordered">

            <div class="sc-alert-icon" position=${this.iconposition}>
              <slot name="icon">
                <div class="sc-alert-icon-svg">
                  ${this.iconTemplate()}
                </div>
              </slot>
            </div>
         
            <div class="sc-alert-content">

              <div class="sc-alert-title">
                <slot name="title">
                  <span>${this.title}</span>
                </slot>
              </div>

              <div class="sc-alert-body">
                <slot name="message">
                  <span>${this.message}</span>
                </slot>
              </div>

            </div>
          </div>
        </div>
      </div>
    `
  }
  iconTemplate() {
    var icon;
    switch( this.type ) {
      case "info":
        icon = faIcon( faInfoCircle ).html[0];
        break;
      case "task":
        icon = faIcon( faSpinner ).html[0];
        break;
      case "warning":
      case "warn":
        icon = faIcon( faExclamationTriangle ).html[0];
        break;
      case "error":
        icon = faIcon( faTimesCircle ).html[0];
        break;
      case "success":
        icon = faIcon( faCheckCircle ).html[0];
        break;
      default:
        icon = "";
    }
    return this.includeSvg( icon );
  }

  /***************
   * lit methods
   **************/

  willUpdate( changes ) {
    if ( 
        ( ! changes.has( 'duration' ) && ! this.hasUpdated )
        && ( this.duration === undefined || this.duration === "" )
    )
      this._inheritDuration();
  }


  update( props ) {
    super.update( props );
    props.forEach( (oldv, name) => {
      if ( name == "duration" ) {
        if ( this.duration && ! this._timer )
          this._timer = setTimeout( () => { this.collapse() }, this.duration );
        else if ( this.duration === 0 && this._timer && ! this.collapsed ) {
          // collapsing to static
          clearTimeout( this._timer );
          this._timer = undefined;
        }
        else if ( oldv && this.duration && ! this.collapsed ) {
          // collapsing to collapsing
          clearTimeout( this._timer );
          this._timer = setTimeout( () => { this.collapse() }, this.duration );
        }
        else if ( this.duration === "" || this.duration === undefined || this.duration === null ) {
          // any to detected collapsing
          if ( this._timer )
            clearTimeout( this._timer );
          this._inheritDuration()
            .then( () => { 
              this._timer = setTimeout( () => { this.collapse() }, this.duration );
            } );
        }
      }
    } );
  }

  connectedCallback() {
    super.connectedCallback();
    if ( this.duration > 0 )
      this._timer = setTimeout( () => { this.collapse() }, this.duration );
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    if ( this._timer )
      clearTimeout( this._timer );
  }


  /***************
   * Own methods
   **************/


  /**
   * Collapse this alert out of view and close
   * @public
   */
  collapse() {
    this.showstyle = undefined;
    this.collapsed = true;
  }


  /**
   * Close and remove this alert immediately
   * @public
   */
  close() {
    this.remove();
  }


  /**
   * Close this alert if able
   * @private
   */
  _tryclose() {
    if ( 
        ! this.nouserclose 
        && ( this.parentElement !== window.getSelection().focusNode
          || window.getSelection().type == "Caret"  )
    )
      this.collapse();
  }


  /**
   * Handle state changes on animation completion
   * @private
   * @param {AnimationEvent} ev   - Animation end event
   */
  _animEnd( ev ) {
    let elem, animName = ev.animationName;
    if ( animName.indexOf( '_' ) !== -1 )
      animName = animName.split( '_' )[0];
    if ( this.hidestyles.includes( animName ) )
      this.close();
    if ( animName == "flash" ) {
      elem = this.shadowRoot.querySelector( '.sc-alert' );
      elem.classList.remove( 'flash' );
    }
  }


  /** 
   * Inherit duration from CSS as needed
   * @private
   */
  async _inheritDuration() {
    let dur, styles;
    styles = window.getComputedStyle( this );
    dur = styles.getPropertyValue( `--alert-${this.type}-duration` );
    if ( dur !== "" && dur !== undefined )
      this.duration = parseFloat( dur );
  }


}


export class SimpleAlerts extends SimpleComponent {
  constructor() {
    super();

    this.zindex = 0;

    SimpleAlert.register();
  }
  static get _elementName() { return 'sc-alerts' }
  static get properties() {
    return Object.assign( {}, super.properties, {

      duration:     { type: Number, def: undefined, desc: "Default duration of alerts in milliseconds, 0 meaning static" },
      iconposition: { type: String, def: undefined, desc: "Default vertical position of any alert icon, either top or middle" },
      changeflash:  { type: Boolean, def: undefined, desc: "Default option to flash this alert to draw attention when changing" },
      showstyle:    { type: String, def: undefined, desc: "Default reveal style, either popin, slidein, popflash, flash or defshow for the CSS defined default" },
      hidestyle:    { type: String, def: undefined, desc: "Default hide style, either rollup, popout, sideslide or defhide for CSS defined default" },
      nouserclose:  { type: Boolean, def: undefined, desc: "Default option to disable closure of this alert by the user" },

    } );
  }
  static get styles() {
    return [
      super.styles, // abstracted styles
      alertContainerStyles,
    ];
  }
  render() {
    return this.builder`
      <div class="sc-alerts">
        <slot></slot>
      </div>
    `;
  }

  /**
   * Add a simple text message alert
   * @public
   * @param {String} type                   - Type of alert, info, warning, error, success or task
   * @param {String} message                - Message for the alert
   * @param {String} title                  - Title for the alert, or undefined for none
   * @param {Object} [options]              - Alert option overrides
   * @param {Number} [options.duration]     - Duration for the alert, defaults to 0 (meaning no duration, static)
   * @param {String} [options.showstyle]    - Style to reveal the alert, popin, slidein, flash. Defaults to CSS defined style
   * @param {String} [options.hidestyle]    - Style to hide the alert, popout, rollup. Defaults to CSS defined style
   * @param {String} [options.iconposition] - Vertical icon position, middle or top. Defaults to middle.
   * @param {Boolean} [options.nouserclose] - If this alert cannot be closed by the user
   * @param {Boolean} [options.changeflash] - If the alert should graphically flash when altered, defaults to false
   * @returns {HTMLElement} alert           - The created alert
   */
  addAlert( type, message, title, options={} ) {
    const zindex = this.app.getTopZ( 0 );
    const newalert = document.createElement( 'sc-alert' );
    const setprops = [ 'duration', 'showstyle', 'hidestyle', 'iconposition', 'nouserclose', 'changeflash'];

    // Place alerts on top as needed
    if ( zindex !== this.zindex )
      this.zindex = this.app.getTopZ( 1 );
    newalert.style.setProperty( 'z-index', this.zindex );

    // Set properties
    newalert.setAttribute( 'type', type );
    newalert.setAttribute( 'message', message );
    if ( title )
      newalert.setAttribute( 'title', title );
    for ( let prop of setprops ) {
      if ( options.hasOwnProperty( prop ) ) {
        if ( options[ prop ] !== undefined && options[ prop ] !== "" )
          newalert.setAttribute( prop, options[ prop ] );
      }
    }

    this.append( newalert );
    return newalert;
  }

}




