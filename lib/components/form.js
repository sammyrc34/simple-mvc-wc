

/* A sc-form fulfills three main purposes:
 * 1) For databound forms, form the glue between the model and form elements.
 * 2) Perform validation and reset functionality if needed.
 * 3) For non-databound forms, bundle form data for controllers.
 */

import { SimpleBusyMask } from '../index.js';
import { DataboundComponent } from './databound.js';

SimpleBusyMask.register();

import formStyles from './form.css';


/* Our custom components to look for */
const ourElements = "sc-input, sc-select, sc-textarea, sc-select-list";

/**
 * A cancelable event fired before form submission, before validation.
 * Cancel to prevent validation and checking. 
 * @event SimpleForm#before-submit
 */
const onBeforeSubmit = "before-submit";

/**
 * A cancelable event fired upon form submission, after validation.
 * Cancel to stop default behaviour and bound model updates.
 * @event SimpleForm#submit
 */
const onSubmit = "submit";

/**
 * An event fired upon after successful form submission.
 * @event SimpleForm#after-submit
 */
const onAfterSubmit = "after-submit";

/**
 * An event fired in case of unsuccessful form submission.
 * @event SimpleForm#failed-submit
 * @type {Object}
 * @property {String} message     - A message explaining the failure
 */
const onFailedSubmit = "failed-submit";

/**
 * A cancelable event fired upon form reset.
 * Cancel to stop default behaviour
 * @event SimpleForm#reset
 */
const onReset = "reset";

/**
 * An event fired when a bound record is loaded or reloaded
 * @event SimpleForm#loaded
 */
const onLoaded = "loaded";

/**
 * The simple form component, supporting binds and submit actions
 * @class
 * @extends DataboundComponent
 */
export class SimpleForm extends DataboundComponent {

  /**
   * @emits {beforesubmit} Before the form is submitted, cancelable
   * @emits {submit} Before the form is submitted, after validation, cancelable
   * @emits {aftersubmit} After the form is submitted successfully
   * @emits {failedsubmit} If a bound form fails to submit successfully 
   * @emits {reset} Before the form is reset, cancelable
   * @emits {loaded} When a bound record is loaded or re-loaded
   */

  constructor() {
    super();

    // Inherited
    this.bindMode       = "single";
    this.modelName      = "";
    this.bindToNew      = true;

    // Private, stateful
    this._predisabled   = [];
    this._observer      = undefined;
    this._timer         = undefined;
  }

  static get _elementName() { return 'sc-form' }
  static get properties() {
    return Object.assign( {}, super.properties, {

      name:           { type: String, reflect: true, def: "", desc: "Name for the form, useful for form identification" },
      disabled:       { type: Boolean, reflect: true, def: false, desc: "If this form is disabled" },

      setrequired:    { type: Boolean, def: false, desc: "Set field required flags based on data model fields" },
      disableunbound: { type: Boolean, def: false, desc: "If this form should disable field controls when not bound" },
      autovalidate:   { type: Boolean, def: false, desc: "If this form should automatically validate on submission" },

      nomask:         { type: Boolean, def: false, desc: "Set to disable a busy mask display upon form actions" },
      busy:           { type: Boolean, def: false, desc: "If this form is currently busy and should display the busy mask" },

      fill:           { type: Boolean, reflect: true, def: false, desc: "If this form should fill the container" },
      scroll:         { type: Boolean, def: false, desc: "If this form should scroll the contained content" },
    } )
  }

  static get styles() {
    return super.styles.concat( [ formStyles ] );
  }

  /*************************
   * Lit methods
   ************************/

  async updated( changes ) {
    super.updated( changes );
    await this.updateComplete;

    if ( changes.has( 'setrequired' ) && this.setrequired )
      this._setRequired();

    /* Propagate disabled changes */
    if ( changes.has( 'disabled' ) && this.disabled !== undefined && changes.get( 'disabled' ) !== undefined )
      this._setDisabled( this.disabled );
    else if ( this.disableunbound && ! this.bound )
      this._setDisabled( true );
    else if ( this.disableunbound && this.bound )
      this._setDisabled( false );
  }


  firstUpdated() {
    const elements = Array.from( this.querySelectorAll( ourElements ) );

    /* Catch intentionally disabled elements */
    this._predisabled = elements.filter( e => e.disabled );
  }


  render() {
    return this.builder`
      <div class="sc-form-wrap" ?fill=${this.fill}>
        <sc-busy ?busy=${this.nomask == false && this.busy} ?fill=${this.fill} ?scroll=${this.scroll}>
          <slot></slot>
        </sc-busy>
      </div>
    `
  }


  /******************
   * Our methods
   *****************/

  /**
   * Adopt styles into the busy element too
   */
  adoptCustomStyles( styles ) {
    const busy = this.shadowRoot.querySelector( 'sc-busy' );
    busy.adoptCustomStyles( styles );
    super.adoptCustomStyles( styles );
  }


  /**
   * Short-hand method to load form data from a model record
   * @public
   */
  load( id ) {
    this.bindId = id;
  }

  /** 
   * Set a form element as disabled or not (won't automatically be disabled / enabled)
   * @public
   * @param {String} name             - The form element to disabled
   * @param {Boolean} disabled        - Disabled or not
   */
  setDisabledElement( name, disabled ) {
    const elements = Array.from( this.querySelectorAll( ourElements ) );
    const elem = elements.find( e => { return e.name == name } );
    if ( ! elem ) {
      this.app.log( "warning", "Cannot find element to set disabled state" );
      return;
    }
    if ( disabled && this._predisabled.indexOf( elem ) === -1 )
      this._predisabled.push( elem );
    elem.disabled = disabled;
  }


  /** 
   * Reset the form to intial record values
   * @public
   */
  reset() {
    // Find elements, restore initial values or load from model
    let resetev, elements;

    // Allow custom reset event
    resetev = new CustomEvent( onReset, { bubbles: true, cancelable: true, composed: true } );
    if ( ! this.dispatchEvent( resetev ) ) {
      return;
    }

    elements = Array.from( this.querySelectorAll( ourElements ) );
    elements.forEach( elem => {
      elem.reset();
    } );
  }

  /**
   * Clear the form to defaults, clearing form elements to defaults and unbind
   * @public
   */
  clear() {
    const elements = Array.from( this.querySelectorAll( ourElements ) );
    for ( const elem of elements ) {
      elem.reset( true );
    }
    this.bindId = undefined;
  }

  /** 
   * Submit this form as appropriate if possible. 
   * @public
   * @fires SimpleForm#beforesubmit       - A cancelable event fired at the start of submission, before validation
   * @fires SimpleForm#submit             - A cancelable event fired before updating any bound models
   * @returns {String}                    - A message if submission is not possible or was cancelled, undefined upon success
   * @throws                              - In the case of any hard error during submission
   */
  submit() {
    let changes, ev;
    this.app.log( "debug", "Submitting form", this.name );

    // Before submission event
    ev = new CustomEvent( onBeforeSubmit, { bubbles: true, cancelable: true, composed: true } );
    if ( ! this.dispatchEvent( ev ) ) {
      this.app.log( "debug", "Default form pre-submission cancelled" );
      return;
    }

    if ( this.autovalidate && ! this.validate() ) {
      this.app.log( "debug", "Unable to submit form, validation failure" );
      ev = new CustomEvent( onFailedSubmit, { detail: { "message": "Validation failure" }, bubbles: true, cancelable: false, composed: true } );
      this.dispatchEvent( ev );
      throw new Error( "Validation failing, cannot submit" );
    }

    if ( ! this.changed() ) {
      this.app.log( "debug", "Unable to submit form, no changes to submit" )
      ev = new CustomEvent( onFailedSubmit, { detail: { "message": "No changes to submit" }, bubbles: true, cancelable: false, composed: true } );
      this.dispatchEvent( ev );
      return "Unable to submit form without changes";
    }

    ev = new CustomEvent( onSubmit, { bubbles: true, cancelable: true, composed: true } );
    if ( ! this.dispatchEvent( ev ) ) {
      this.app.log( "debug", "Default form submission cancelled" );
      return;
    }

    // Submit bound record changes
    if ( this.bound ) {
      try { 
        changes = this.collectValues();
        this.ingressRecord( changes );

        ev = new CustomEvent( onAfterSubmit, { bubbles: true, cancelable: false, composed: true } );
        this.dispatchEvent( ev );
      }
      catch(e) {
        this.app.log( "error", "Failed to submit bound form", e );
        ev = new CustomEvent( onFailedSubmit, { detail: { "message": "Submission failed: " + e.message }, bubbles: true, cancelable: false, composed: true } );
        this.dispatchEvent( ev );
        throw e;
      }
    }
    else {
      this.app.log( "debug", "Form not bound, no submission occured" );
      ev = new CustomEvent( onFailedSubmit, { detail: { "message": "Unbound form" }, bubbles: true, cancelable: false, composed: true } );
      this.dispatchEvent( ev );
    }
  }


  /**
   * Validate the form values
   * @public
   * @return {Boolean}          - Validation result
   */
  validate() {
    const elements = Array.from( this.querySelectorAll( ourElements ) );
    let ok = true;
    elements.forEach( opt => {
      if ( ! opt.validate() )
        ok = false;
    } );
    return ok;
  }


  /**
   * Get the form's data, providing this as an Object 
   * @public
   */
  collectValues() {
    const elements = Array.from( this.querySelectorAll( ourElements ) );
    const data = {};

    for ( const elem of elements ) {
      const field = elem.field || elem.name;
      data[ field ] = elem.value;
    }
   
    return data;
  }


  /**
   * Determine if the form has changed
   * @public
   * @returns {Boolean}         - If form has been altered
   */
  changed() {
    const elements = Array.from( this.querySelectorAll( ourElements ) );
    const changed = elements.some( e => e.changed() );
    return changed
  }


  /**
   * Apply changes to the form elements without submission
   * @param {Object} changes      - A key,value set to apply
   * @throws                      - If unable to apply changes to field elements
   */
  applyChanges( changes ) {
    const elements = Array.from( this.querySelectorAll( ourElements ) );
    let field;
    for ( let element of elements ) {
      field = element.field || element.name;
      if ( changes.hasOwnProperty( field ) )
        element.value = changes[ field ];
    }
  }


  /**
   * Update elements to be disabled or not without overriding manually disabled elements
   * @private
   * @param {Boolean} disabled        - Form disabled state
   */
  _setDisabled( disabled ) {
    const elements = Array.from( this.querySelectorAll( ourElements ) );
    for ( const elem of elements ) {
      if ( this._predisabled.indexOf( elem ) === -1 )
        elem.disabled = disabled;
    }
  }


  /**
   * Set required fields from the model
   * @private
   * */
  _setRequired() {
    const elements = Array.from( this.querySelectorAll( ourElements ) );
    const model = this.getModel();
    let needFields;

    if ( ! model )
      return;

    needFields = model.requiredFields;
    for ( const elem of elements ) {
      // Skip checkbox, radios, toggles where setting requires means needing them selected
      const field = elem.field || elem.name;
      if ( elem.type == "checkbox" || elem.type == "radio" || elem.type == "toggle" )
        return;
      if ( needFields.includes( field ) )
        elem.required = true;
    }
  }


  /**
   * Handle a data model status change
   * @protected
   */
  _statusChanged( action ) {
    switch( action ) {
      case 'disable':
        this.disable = true;
        break;
      case 'enable':
        this.disable = false;
        break;
      case 'bind':
        this._loadFromModel();
        break;
      case 'unbind':
        this.bindId = undefined;
        this.clear();
        break;
    }
  }


  /**
   * Handle a model data change
   * @protected
   * @async
   * @see DataboundComponent#_dataChanged
   * @param {Object} detail
   * @param {String} detail.change          - The type of change, load, add, update or remove
   * @param {Boolean} detail.local          - If the change is non-authoritative (eg local)
   * @param {String[]|Number[]} details.ids - The IDs associated with the change
   */
  async _dataChanged( detail ) {
    // Handle actions
    switch ( detail[ 'change' ] ) {
      case 'update':
      case 'load':
        this._loadFromModel( );
        break;
      case 'remove': 
        this.clear();
        break;
      case 'rebind':
        if ( this.boundid == detail.ids[ 0 ] ) {
          this.app.log( "debug", "Rebinding form to new ID", detail.ids[ 1 ] );
          this._silent = true;
          this.bindId = detail.ids[ 1 ];
          this._boundid = detail.ids[ 1 ];
          this._bound = true;
          this.dataModel.changeSingleBindId( this.uuid, detail.ids[ 1 ] );
        }
        break;
    }
  }


  /**
   * Read the bound record and apply values to the form
   * @private
   * */
  _loadFromModel( ) {
    const elements = Array.from( this.querySelectorAll( ourElements ) );
    let record, ev;

    // Get the record, or a default one
    if ( this.dataModel.hasRecord( this.boundid ) )
      record = this.dataModel.getRecord( this.boundid );
    else
      record = this.dataModel.defaultRecord();

    /* Set value the field is in record */
    for ( const elem of elements ) {
      const field = elem.field || elem.name;
      if ( record.hasOwnProperty( field ) )
        elem.setInitial( record[ field ] );
      else
        elem.reset( true );
    }

    ev = new CustomEvent( onLoaded, { composed: true, bubbles: true, cancelable: false } );
    this.dispatchEvent( ev );
  }



  /**
   * A form element is focused, watch for autofills
   * @protected
   */
  elementFocused() {
    this._timer = setTimeout( () => { this._checkAutofill() }, 100 );
  }


  /**
   * Check inputs for autofill, set values
   * @private
   */
  _checkAutofill() {
    const ourinputs = this.querySelectorAll( 'sc-input' );
    let stop = true;

    for ( const ourinput of ourinputs ) {
      const input = ourinput.querySelector( ':scope > input' );
      if ( input && input.matches( 'input:-webkit-autofill' ) )
        ourinput._updateLabel( true );
      else if ( input )
        ourinput._updateLabel( false );
      if ( input && input.matches( 'input:focus' ) )
        stop = false;
    }
    if ( ! stop )
      this._timer = setTimeout( () => { this._checkAutofill() }, 100 );
  }


}


