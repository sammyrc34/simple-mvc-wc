export class SimpleSlideout extends SimpleComponent {
    static get _elementName(): string;
    _zindex: number;
    _showing: boolean;
    _skipanim: boolean;
    _sizing: boolean;
    _bodytouch: boolean;
    _resizefromx: any;
    _resizefromy: any;
    _touchhandler: (e: any) => void;
    _movehandler: (e: any) => void;
    _grabbedelem: any;
    _resizeobserver: Polyfill;
    render(): import("lit-html").TemplateResult<1>;
    /**********************
     * Lit methods
     *********************/
    /**
     * Determine if properties should change
     * @protected
     */
    protected shouldUpdate(changes: any): boolean;
    open: boolean;
    /**
     * Handle pending property updates
     * @protected
     */
    protected willUpdate(changes: any): void;
    /**
     * Handle updated properties
     * @protected
     */
    protected updated(changes: any): void;
    /**********************
     * Own methods
     *********************/
    /**
     * Get the current slideout size
     * @public
     * @returns {Number} - Height or width of the slideout's size in pixels depending on orientation
     */
    public getSize(): number;
    /**
     * Try to show the slideout
     * @public
     * @param {Boolean} instant=false   - Display instantly without any animation
     */
    public show(instant?: boolean): void;
    /**
     * Try to hide the slideout
     * @public
     * @param {Boolean} instant=false   - Hide instantly without any animation
     */
    public hide(instant?: boolean): void;
    /**
     * Toggle to show or hide the slideout
     * @public
     */
    public toggle(): void;
    /**
     * Detect the presence of a header element
     * @private
     */
    private _headingChanged;
    _hasheader: boolean;
    /**
     * Setup the component following content change
     * @async
     * @private
     */
    private _slotChanged;
    /**
     * Update our content size
     * @private
     */
    private _updateSize;
    _opensize: any;
    /**
     * Inherit animation time from CSS if required
     * @private
     */
    private _inheritAnimateTime;
    animatetime: number;
    /**
     * Show the slideout
     * @private
     */
    private _show;
    /**
     * Hide the slideout
     * @private
     */
    private _hide;
    /**
     * Detect the size of the content to display
     * @private
     */
    private _detectSize;
    /**
     * Size the content ready for positioning
     * @private
     */
    private _sizeContent;
    /**
     * Position content as needed
     * @private
     */
    private _positionAtStart;
    /**
     * Slide the slideout out
     * @private
     */
    private _slideOut;
    /**
     * Disable document body scrolling
     * @private
     */
    private _disableBodyScroll;
    /**
     * Enable document body scrolling
     * @private
     */
    private _enableBodyScroll;
    /**
     * Attempt to close if autoclose
     */
    _tryClose(): void;
    /**
     * Start a resize on the panel
     */
    _startResize(ev: any): void;
    /**
     * End the panel resizing
     */
    _stopResize(): void;
    /**
     * Resize the slideout in or out
     */
    _resizePanel(ev: any): void;
}
import { SimpleComponent } from '../index.js';
import { ResizeObserver as Polyfill } from '@juggle/resize-observer';
