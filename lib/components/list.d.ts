/**
 * A simple list component.
 *
 * Intended to display model data in some way as provided.
 * @class SimpleList
 * @extnds DataboundComponent
 */
export class SimpleList extends SelectableListComponent {
    static get _elementName(): string;
    render(): import("lit-html").TemplateResult<1>;
    renderItemTemplate(row: any): import("lit-html").TemplateResult<1>;
    willUpdate(changes: any): void;
    _needUpdate: boolean;
    updated(props: any): void;
    firstUpdated(): void;
    /**
     * Get total number of records in the model
     * @type {Number}           - Total number of records in the model
     */
    get total(): number;
    /**
     * Get number of records last fetched
     * @type {Number}           - Total number of fetched records
     */
    get length(): number;
    /**
     * Replace sorting of this list with provided sort criteria
     * @public
     * @param {Object[]} sorts                      - A list of sorting criteria
     * @param {String} sorts[].field                - The name of the field to sort on
     * @param {Function} [sorts[].sortFunc]         - A custom sorting function used
     * @param {String} [sorts[].direction]          - The direction to sort, asc or desc unless function provided
     * @param {String} [sorts[].type]               - The type of sort, number or string if not function sort
     * @throws                                      - If unable to sort for any reason
     */
    public applySort(sorts: {
        field: string;
        sortFunc?: Function;
        direction?: string;
        type?: string;
    }): void;
    _sorts: {
        field: string;
        type: string;
        direction: string;
        func: Function;
    }[];
    /**
     * Apply a set of filters to this list
     * @param {FiltersCollection} filters     - The filters to apply
     * @see {BasicCollection#FiltersCollection}
     * @throws                                - If filters cannot be applied
     */
    applyFilters(filters: FiltersCollection): void;
    _filters: any[];
    /**
     * Get all displayed list item elements
     * @public
     * @returns {HTMLElement[]}     - List elements
     */
    public getDisplayedItems(): HTMLElement[];
    /**
     * Get all displayed item record IDs
     * @public
     * @returns {String[]|Number[]} - Record IDs
     */
    public getDisplayedIDs(): string[] | number[];
    /**
     * Remove our data, retaining sorts and filters
     * @public
     */
    public clear(): void;
    /**
     * Stop the text selection when multi-selecting items
     */
    _hinderTextSelect(ev: any): void;
    disabled: boolean;
    busy: boolean;
    /**
     * Handle a model data change
     * @protected
     * @async
     * @fires SimpleList#updated      - When the data is updated
     * @see DataboundComponent#_dataChanged
     */
    protected _dataChanged(detail: any): Promise<void>;
    /**
     * Populate our data from the model
     * @private
     * @async
     */
    private _loadFromModel;
}
import { SelectableListComponent } from './selectablelist.js';
