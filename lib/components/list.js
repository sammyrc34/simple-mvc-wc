
import { SelectableListComponent } from './selectablelist.js';
import { SimpleBusyMask } from '../index.js';

import listStyles from './list.css';


// Prevent tree shaking trimming the component
SimpleBusyMask.register();


/**
 * An event fired upon data update
 * @event SimpleList#updated
 */
const onDataUpdated = "updated";


/**
 * A simple list component.
 *
 * Intended to display model data in some way as provided.
 * @class SimpleList
 * @extnds DataboundComponent
 */
class SimpleList extends SelectableListComponent {


  /**
   * @emits {updated} After the list has updated displayed data
   */

  constructor() {
    super();
  }

  static get _elementName() { return 'sc-list' }

  static get properties() {
    return Object.assign( {}, super.properties, {

      fillwrap:       { type: Boolean, def: false, desc: "Should this list fill the parent container and wrap items. Ideal for tile type items." },
      fillheight:     { type: Boolean, def: false, desc: "Should this list fill the parent's height" },
      fillwidth:      { type: Boolean, def: false, desc: "Should this lsit fill the parent's width" },

      busy:           { type: Boolean, def: false, desc: "Display the busy overlay mask" },
      disabled:       { type: Boolean, def: false, desc: "Display the list in a disabled mode" },

      range:          { type: Number, def: undefined, desc: "The number of records from the data source to display" },
      begin:          { type: Number, def: 0, desc: "The zero-indexed position to begin data display" },

      sortfields:     { type: Array, def: [], desc: "Data model fields on which to sort on, indexed with sorttypes and sortdirections" },
      sorttypes:      { type: Array, def: [], desc: "Data model field sort types, indexed with sortfields. Either number or string" },
      sortdirections: { type: Array, def: [], desc: "Data model field sort directions, indexed with sortfields. Either asc or desc" },

      itemTemplate:   { type: Function, attribute: false, def: undefined, desc: "The list item template builder function, called with each data row object" },
      listStyles:     { type: Array, attribute: false, def: undefined, desc: "An optional list of additional style CSSResults for this list instance" },

      // Private
      _sorts              : { type: Array, def: [] },
      _filters            : { type: Array, def: [] },
      _needUpdate         : { type: Boolean, def: false },
    } );
  }

  static get styles() {
    return super.styles.concat( [ listStyles ] );
  }


  render() {
    var showdata = Array.from( this._data );

    return this.builder`
      <div class="sc-list"
        ?fillwrap=${this.fillwrap}
        ?fillheight=${this.fillheight}
        ?fillwidth=${this.fillwidth}
        ?selectable=${this.select !== "none"}
      >
        <div class="sc-list-content-wrap">
          <sc-busy id="list-busy" ?busy=${this.busy} fill>
            <div class="sc-list-content" ?disabled=${this.disabled}>
              ${showdata.map( row => this.renderItemTemplate( row ) ) }
            </div>
          </sc-busy>
        </div>
      </div>
    `;
  }


  renderItemTemplate( row ) {
    var idfield = this.idField;
    return this.builder`
      <div
          class="sc-list-item"
          data-id="${row[idfield]}"
          data-type=${typeof row[idfield]}
          data-selected=${this.isSelected(row[idfield]) ? "true":"false"}
          @click=${this.handleSelectClick}
          @mousedown=${this._hinderTextSelect}
          @dblclick=${this.handleDblClick}
          @contextmenu=${this.itemContextMenu}
      >
        ${this.itemTemplate(row)}
      </div>
    `
  }


  /*
   * ================
   * Lit methods
   * ================
  */

  willUpdate( changes ) {
    const sorts = [];
    let needUpd = false;

    super.willUpdate( changes );

    // Use any default sorting
    if ( ! this.hasUpdated ) {
      if ( this._sorts.length == 0
          && this.sortfields.length
          && this.sorttypes.length
          && this.sortdirections.length
      ) {

        for ( let idx in this.sortfields ) {
          sorts.push( {
            'field'       : this.sortfields[ idx ],
            'direction'   : this.sortdirections[ idx ],
            'type'        : this.sorttypes[ idx ]
          } );
        }

        this.applySort( sorts );

        this._needUpdate = true;
      }
    }

    // Catch properties that require a re-load from the model
    if ( changes.has( '_needUpdate' ) && this._needUpdate )
      needUpd = true;
    else
      needUpd = [ 'range', 'begin' ].some( prop => changes.has( prop) );

    // Refresh data
    if ( needUpd || this.contextchange  )
      this._loadFromModel();

  }


  firstUpdated() {
    if ( this.listStyles )
      this.adoptCustomStyles();
  }



  //======================
  // Own methods
  //======================


  /**
   * Adopt styles into the busy element too
   */
  adoptCustomStyles( styles ) {
    const toAdopt = [];
    const busy = this.shadowRoot.querySelector( 'sc-busy' );
    let additional;
    if ( this.listStyles ) {
      additional = Array.isArray( this.listStyles ) ? this.listStyles : [ this.listStyles ];
      for ( let addit of additional )
        toAdopt.push( addit );
    }
    if ( styles )
      toAdopt.push( styles );
    busy.adoptCustomStyles( toAdopt );
    super.adoptCustomStyles( toAdopt );
  }


  /**
   * Get total number of records in the model
   * @type {Number}           - Total number of records in the model
   */
  get total() {
    return this._data.totalCount || 0;
  }


  /**
   * Get number of records last fetched
   * @type {Number}           - Total number of fetched records
   */
  get length(){
    return this._data.fetchCount;
  }



  /**
   * Replace sorting of this list with provided sort criteria
   * @public
   * @param {Object[]} sorts                      - A list of sorting criteria
   * @param {String} sorts[].field                - The name of the field to sort on
   * @param {Function} [sorts[].sortFunc]         - A custom sorting function used
   * @param {String} [sorts[].direction]          - The direction to sort, asc or desc unless function provided
   * @param {String} [sorts[].type]               - The type of sort, number or string if not function sort
   * @throws                                      - If unable to sort for any reason
   */
  applySort( sorts ) {
    const model = this.getModel();
    const checked = [];

    this.app.log( 'debug', "Setting list sort", sorts );

    // Check each field is known to the model
    for ( const { field: sortfield, type: sorttype, direction: sortdirection, sortFunc: sortfunction } of sorts ) {

      // Ensure sort field is known
      if ( ! model.allFields.includes( sortfield ) ) {
        this.app.log( "error", "Sort field", sortfield, "not specified in model" );
        throw new Error( "Sort field " + sortfield + " not known to model" );
      }

      // Check sort type
      if ( ! sortfunction && ! sorttype ) {
        this.app.log( "error", "Sort type not provided for field", sortfield );
        throw new Error( "Sort type must be specified for field " + sortfield );
      }

      // Check direction
      if ( ! sortfunction && sortdirection !== "desc" && sortdirection !== "asc" )
        throw new Error( "Unknown sort direction " + sortdirection );

      checked.push( {
        field       : sortfield,
        type        : sorttype,
        direction   : sortdirection,
        func        : sortfunction
      } );
    }

    this._sorts = checked;
    this._needUpdate = true;
  }


  /**
   * Apply a set of filters to this list
   * @param {FiltersCollection} filters     - The filters to apply
   * @see {BasicCollection#FiltersCollection}
   * @throws                                - If filters cannot be applied
   */
  applyFilters( filters ) {
    const model  = this.getModel();
    const checked = [];

    // Do any form of sanity checking?
    for ( let filter of filters ) {
      if ( typeof filter == "function" )
        checked.push( filter );
      else {
        Object.keys( filter ).every( field => {
          if ( ! model.allFields.includes( field ) ) {
            this.app.log( "error", "Sort field", field, "not specified in model" );
            throw new Error( "Sort field " + field + " not known to model" );
          }
        } );
        checked.push( filter );
      }
    }

    this.clearSelection();
    this._filters = checked;
    this._needUpdate = true;
  }



  /**
   * Get all displayed list item elements
   * @public
   * @returns {HTMLElement[]}     - List elements
   */
  getDisplayedItems() {
    let elems = Array.from( this.shadowRoot.querySelectorAll( '.sc-list-item' ) );
    return elems;
  }


  /**
   * Get all displayed item record IDs
   * @public
   * @returns {String[]|Number[]} - Record IDs
   */
  getDisplayedIDs() {
    return this._data.map( d => d[ this.idField ] );
  }


  /**
   * Remove our data, retaining sorts and filters
   * @public
   */
  clear() {
    this._data = [];
  }


  /**
   * Stop the text selection when multi-selecting items
   */
  _hinderTextSelect(ev) {
    if ( ev.ctrlKey || ev.shiftKey ) {
      ev.preventDefault();
    }
  }


  /**
   * Handle a model status change
   * @protected
   */
  _statusChanged( type ) {
    super._statusChanged( type );
    switch( type ) {
      case 'disable':
        this.disabled = true;
        break;
      case 'enable':
        this.disabled = false;
        break;
      case 'unbind':
      case 'cleared':
        this.clear();
        break;
      case 'pending':
        this.busy = true;
        break;
      case 'failure':
      case 'complete':
        this.busy = false;
        break;

    }
  }


  /**
   * Handle a model data change
   * @protected
   * @async
   * @fires SimpleList#updated      - When the data is updated
   * @see DataboundComponent#_dataChanged
   */
  async _dataChanged( detail ) {

    this.app.log( "debug", this.constructor.name, "data model changed, queuing update" );

    super._dataChanged( detail );

    this._needUpdate = true;
  }


  /**
   * Populate our data from the model
   * @private
   * @async
   */
  async _loadFromModel() {
    this.app.log( "debug", "Updating list data from model" );

    this._needUpdate = false;

    // No model or not bound?
    if ( ! this.getModel() )
      return;
    if ( ! this.bound )
      return;

    // Skip loading if the model missing context
    if ( ! this.canFetch() ) {
      this.app.log( "debug", "Model not ready for fetch" );
      return;
    }

    try {
      // Fetch sorted and filtered data
      this._data = await this.getModel().fetch( this.begin, this.range, this._sorts, this._filters );

      // Let listeners know
      this.dispatchEvent( new CustomEvent( onDataUpdated, { bubbles: false, cancelable: false, composed: true } ) );
    }
    catch( err ) {
      // Just conain errors as model status updates will propagate states
      this.app.log( "warn", `Unable to load records: ${err.message}` );
    }
  }

}


export { SimpleList };

