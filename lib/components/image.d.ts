export class SimpleImage extends SimpleComponent {
    static get _elementName(): string;
    _realed: boolean;
    _zoomed: boolean;
    _zooming: boolean;
    _panning: boolean;
    _refSpace: number;
    _refScale: number;
    _refX: any;
    _refY: any;
    _posX: number;
    _posY: number;
    _scale: number;
    _offsetX: number;
    _offsetY: number;
    _refRangeX: number;
    _refRangeY: number;
    _observer: IntersectionObserver;
    render(): import("lit-html").TemplateResult<1>;
    willUpdate(changes: any): void;
    _mouseZoom(ev: any): void;
    _touchStart(ev: any): void;
    _startZoom(ev: any): void;
    _startPan(ev: any): void;
    /**
     * Check a pan is within the range limits
     */
    _checkPan(ev: any): void;
    _touchMove(ev: any): void;
    _zoom(ev: any): void;
    _pan(ev: any): void;
    _touchEnd(ev: any): void;
    _touchCancel(): void;
    _mousein(ev: any): void;
    _mouseout(ev: any): void;
    _setupObserver(): void;
    lazy: boolean;
}
import { SimpleComponent } from '../index.js';
