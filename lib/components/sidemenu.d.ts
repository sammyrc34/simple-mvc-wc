export class SimpleSidemenuItem extends SimpleComponent {
    static get _elementName(): string;
    static get properties(): {
        label: {
            type: StringConstructor;
            def: string;
            desc: string;
        };
        navpath: {
            type: StringConstructor;
            def: string;
            desc: string;
        };
        navtag: {
            type: StringConstructor;
            def: string;
            desc: string;
        };
        navquery: {
            type: ObjectConstructor;
            def: {};
            desc: string;
        };
        selected: {
            type: BooleanConstructor;
            def: boolean;
            desc: string;
        };
    };
    render(): import("lit-html").TemplateResult<1>;
    /**
     * Prevent the helper link being directly navigated to
     * @private
     */
    private _inhibitClick;
    /**
     * Attempt to navigate somewhere
     * @private
     */
    private _navTo;
}
export class SimpleSidemenuGroup extends SimpleComponent {
    static get _elementName(): string;
    static get properties(): {
        label: {
            type: StringConstructor;
            def: string;
            desc: string;
        };
        collapsed: {
            type: BooleanConstructor;
            reflect: boolean;
            def: boolean;
            desc: string;
        };
        selected: {
            type: BooleanConstructor;
            reflect: boolean;
            def: boolean;
            desc: string;
        };
    };
    _maxHeight: number;
    render(): import("lit-html").TemplateResult<1>;
    updated(changes: any): void;
    firstUpdated(): Promise<void>;
    /**
     * Toggle our collapsed state
     * @public
     */
    public toggle(): void;
    collapsed: boolean;
}
/**
 * A side-menu style component with groups and menu items within
 * @class SimpleSidemenu
 */
export class SimpleSidemenu extends SimpleComponent {
    static get _elementName(): string;
    static get properties(): {
        minimised: {
            type: BooleanConstructor;
            reflect: boolean;
            def: boolean;
            desc: string;
        };
        skipanimation: {
            type: BooleanConstructor;
            def: boolean;
            desc: string;
        };
        label: {
            type: StringConstructor;
            def: string;
            desc: string;
        };
    };
    render(): import("lit-html").TemplateResult<1>;
    firstUpdated(): Promise<void>;
    skipanimation: boolean;
    /**
     * When the menu has finished resizing
     * @private
     */
    private _resizeEnd;
    /**
     * Toggle the menu minimised state
     * @public
     */
    public toggleMenu(): void;
    minimised: any;
    /**
     * Update the menu to reflect selected navigation item
     * @private
     */
    private _updateSelected;
}
import { SimpleComponent } from '../index.js';
