
import { SimpleComponent } from '../index.js';


import commonStyles from './busy.css';


export class SimpleBusyMask extends SimpleComponent {
  constructor() {
    super();
    this.busy = false;
    this.fill = false;
    this.scroll = false;
    this.noflex = false;
  }
  static get _elementName() { return 'sc-busy' }
  static get properties() {
    return Object.assign( {}, super.properties, {

      busy:     { type: Boolean, reflect: true, def: false, desc: "If this busy mask currently displays" },
      fill:     { type: Boolean, reflect: true, def: false, desc: "If this busy mask should fill the parent container" },
      scroll:   { type: Boolean, reflect: true, def: false, desc: "If scrolling of the nested elements is enabled" },
      noflex:   { type: Boolean, reflect: true, def: false, desc: "Disable flexbox display of nested elements" },

    } );
  }
  static get styles() {
    return super.styles.concat( [ commonStyles ] );
  }
  render() {
    return this.builder`
      <div class="sc-busy" ?busy=${this.busy} ?fill=${this.fill} ?scroll=${this.scroll} ?flex=${this.noflex == false}>
        <div class="sc-busy-mask">
          <div class="sc-busy-spinner-container">
            <div class="sc-busy-spinner"></div>
          </div>
        </div>
        <div class="sc-busy-content">
          <slot></slot>
        </div>
      </div>
    `;
  }

}
  

