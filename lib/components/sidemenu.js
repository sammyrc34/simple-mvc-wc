
import { SimpleComponent } from '../index.js';

import { library as faLib, icon as faIcon } from "@fortawesome/fontawesome-svg-core";
import { faPlus, faMinus, faAngleLeft, faAngleRight } from "@fortawesome/free-solid-svg-icons";

import sidemenuStyles from './sidemenu.css';

faLib.add( faPlus, faMinus, faAngleLeft, faAngleRight );

import equal from 'fast-deep-equal';


/**
 * An event fired upon after the menu has been resized.
 * @event SimpleSidemenu#after-resize
 */
const onAfterResize = "after-resize";

/**
 * Event when the sidemenu group collapsed state is toggled
 * @event SimpleSidemenuGroup#toggled
 */
const onGroupToggled = 'toggled';



export class SimpleSidemenuItem extends SimpleComponent {
  constructor() {
    super();
  }
  static get _elementName() { return 'sc-sidemenu-item' }
  static get properties() {
    return {
      'label':    { type: String, def: "", desc: "Display label for this menu item" },
      'navpath':  { type: String, def: "", desc: "A URL or application URI path to navigate to" },
      'navtag':   { type: String, def: "", desc: "An optional application anchor to scroll into view" },
      'navquery': { type: Object, def: {}, desc: "An optional key-value object of query parameters" },
      'selected': { type: Boolean, def: false, desc: "Is this menu item selected" }
    }
  }
  static get styles() {
    return super.styles.concat( [ sidemenuStyles ] );
  }
  render() {
    return this.builder`
      <div class="sc-sidemenu-item" @click=${this._navTo} ?selected=${this.selected}>
        <div class="sc-sidemenu-icon-wrapper" title=${this.label}>
          <slot name="icon"></slot>
        </div>
        <a href=${this.navpath.match( /^http/i ) ? this.navpath : this.app.prefix + this.navpath} @click=${this._inhibitClick}>
          <span>${this.label}</span>
        </a>
      </div>
    `
  }


  /**
   * Prevent the helper link being directly navigated to
   * @private
   */
  _inhibitClick( ev ) {
    ev.preventDefault();
  }


  /**
   * Attempt to navigate somewhere
   * @private
   */
  _navTo() {
    if ( this.navpath != '' )
      this.app.navigate( this.navpath, this.navtag, this.navquery );
  }
}



export class SimpleSidemenuGroup extends SimpleComponent {

  /**
   * @emits {toggled}   - After menu group is toggled
   */

  constructor() {
    super();
    this._maxHeight = undefined;
  }
  static get _elementName() { return 'sc-sidemenu-group' }
  static get properties() {
    return {
      'label':        { type: String, def: "", desc: "Display label for this menu group" },
      'collapsed':    { type: Boolean, reflect: true, def: false, desc: "If this group is currently collapsed" },
      'selected':     { type: Boolean, reflect: true, def: false, desc: "If this group is currently selected"  },
    }
  }
  static get styles() {
    return super.styles.concat( [ sidemenuStyles ] );
  }
  render() {

    return this.builder`
      <div class="sc-sidemenu-group" ?collapsed=${this.collapsed} ?selected=${this.selected}>
        <div class="sc-sidemenu-item" @click=${this.toggle}>
          <div class="sc-sidemenu-icon-wrapper" title=${this.collapsed ? this.label : ''}>
            <slot name="icon"></slot>
          </div>
          <p>${this.label}</p>
          <div class="sc-sidemenu-expandicon-wrapper"> 
            <slot name="expandicon">
              <div class="sc-item-expander">
                ${this.collapsed? this.includeSvg( faIcon( faPlus ).html[0] ) : this.includeSvg( faIcon( faMinus ).html[0] ) }
              </div>
            </slot>
          </div>
        </div>
        <div class="sc-sidemenu-group-items">
          <slot></slot>
        </div>
      </div>
    `
  }

  updated( changes ) {
    let ev;
    super.updated( changes );
    if ( changes.has( 'collapsed' ) && changes !== undefined ) {
      ev = new CustomEvent( onGroupToggled, { bubbles: true, cancelable: false } );
      this.dispatchEvent( ev );
    }
  }


  async firstUpdated() {
    let kids, maxheight = 0;
    await this.updateComplete;
    if ( this.collapsed ) {
      kids = this.shadowRoot.querySelector( '.sc-sidemenu-group-items' )
        .firstElementChild.assignedNodes()
        .filter(n => n.nodeType === Node.ELEMENT_NODE);
      for ( let kid of kids ) {
        maxheight += kid.getBoundingClientRect().height;
      }
      this._maxHeight = maxheight;
    }
  }


  /**
   * Toggle our collapsed state
   * @public
   */
  toggle() {
    const menu = this.closest( 'sc-sidemenu' );
    const items = this.shadowRoot.querySelector( '.sc-sidemenu-group-items' );
    const isMin = menu.hasAttribute( 'minimised' )
    let bounds;

    // Prevent toggle when minimised
    if ( isMin )
      return;

    bounds = items.getBoundingClientRect();
    if ( bounds.height > 0 && ( ! this._maxHeight || bounds.height > this._maxHeight ) )
      this._maxHeight = bounds.height;
    items.style.setProperty( 'max-height', this._maxHeight + 'px' );

    if ( this.collapsed )
      this.collapsed = false;
    else {
      // Wait for max-height to take effect
      window.requestAnimationFrame( () => { this.collapsed = true; } );
    }
  }

}



/**
 * A side-menu style component with groups and menu items within
 * @class SimpleSidemenu
 */
export class SimpleSidemenu extends SimpleComponent {

  /**
   * @emits {afterresize} After the menu has been resized
   */

  constructor() {
    super();
  }
  static get _elementName() { return 'sc-sidemenu' }
  static get properties() {
    return {
      minimised:      { type: Boolean, reflect: true, def: false, desc: "If this menu in a minimised state" },
      skipanimation:  { type: Boolean, def: true, desc: "Skip animation for the next transition only" },
      label:          { type: String, def: "Menu", desc: "Display label for the menu" }
    }
  }
  static get styles() {
    return super.styles.concat( [ sidemenuStyles ] );
  }
  render() {
    return this.builder`
      <div 
          class="sc-sidemenu" 
          ?minimised=${this.minimised}
          ?skipanimation=${this.skipanimation}
          @transitionend=${this._resizeEnd}
        >
        <slot name="menuexpander">
          <div 
              class="sc-sidemenu-expander" 
              ?minimised=${this.minimised}
            >
            <p><strong>${this.label}</strong></p>
            <div 
                class="sc-sidemenu-expander-icon" 
                @click=${this.toggleMenu}
              >
              ${this.minimised? 
                this.includeSvg( faIcon( faAngleRight ).html[0] ) : 
                this.includeSvg( faIcon( faAngleLeft ).html[0] )
              }
            </div>
          </div>
        </slot>
        <slot></slot>
      </div>
    `;
  }
  connectedCallback() {
    super.connectedCallback();
    this.app.addEventListener( 'after-navigate', () => this._updateSelected() ); // Note async
  }
  disconnectedCallback() {
    this.app.removeEventListener( 'after-navigate', () => this._updateSelected() ); // Note async
  }
  async firstUpdated() {
    // Wait for children and paints
    await this.updateComplete;
    if ( this.minimised ) {
      var groups = Array.from( this.getElementsByTagName( 'sc-sidemenu-group' ) );
      groups.forEach( (group) => { group.setAttribute( 'collapsed', true ) } );
    }
    // Enabled transitions after initial draw
    setTimeout( () => { this.skipanimation = false; }, 500 );
  }

  /**
   * When the menu has finished resizing
   * @private
   */
  _resizeEnd( ev ) {
    const path = ev.composedPath();
    if ( ev.propertyName == "max-width" && path[0] == this.shadowRoot.firstElementChild )
      this.dispatchEvent( new CustomEvent( onAfterResize, { bubbles: true, cancelable: false, composed: true } ) );
  }

  /**
   * Toggle the menu minimised state
   * @public
   */
  toggleMenu( ) {
    const groups = Array.from( this.getElementsByTagName( 'sc-sidemenu-group' ) );
    let minimised = this.minimised ? false : true;
    groups.forEach( (group) => { 
      if ( ! group.collapsed )
        group.toggle();
    } );
    this.minimised = minimised;
    if ( ! minimised )
      this._updateSelected(); // Note: async
  }


  /**
   * Update the menu to reflect selected navigation item
   * @private
   */
  async _updateSelected() {
    const items = Array.from( this.getElementsByTagName( 'sc-sidemenu-item' ) );
    const groups = Array.from( this.getElementsByTagName( 'sc-sidemenu-group' ) );
    let url, path, tag, query, item, matching;

    // Wait for children and paints
    await this.updateComplete;

    url = this.app.getStateProp( 'url' );
    path = url.path;
    tag = url.hash;
    query = url.query;

    // Navigation may not have completed
    if ( ! path || path == "" )
      return;

    // Find the best matching menu item
    matching = items.filter( i => {
      if ( i.hasAttribute( 'navpath' ) ) return ( path.indexOf( i.navpath ) != -1 );
    } );
    if ( matching.length == 1 )
      item = matching[0];
    else if ( matching.length > 1 ) {
      var scored = [];
      matching.forEach( i => {
        var score = 0;
        if ( Object.keys( i.navquery ).length > 0 && equal( query, i.navquery ) )
          score = 2; 
        else if ( Object.keys( i.navquery ).length == 0 && equal( query, i.navquery ) )
          score = 1;
        if ( tag != "" && tag == i.navtag )
          score += 2;
        else if ( tag == i.navtag ) 
          score += 1;
        if ( i.navpath != path ) {
          var rest = path.replace( i.navquery, '' );
          score -= rest.match(/\//g).length;
        }
        scored.push( { 'item': i, 'score': score } );
      } );
      scored.sort( (a,b) => { 
        return b.score - a.score;
      } );
      item = scored[0]['item'];
    }

    // Update properties
    items.forEach( i => { i.removeAttribute( 'selected' ) } );
    groups.forEach( i => { i.removeAttribute( 'selected' ) } );
    if ( item ) {
      var group = item.closest( 'sc-sidemenu-group' );
      item.setAttribute( 'selected', '' );
      if ( group != null ) {
        while ( group != null ) {
          group.setAttribute( 'selected', '' );
          if ( !this.minimised && group.hasAttribute( 'collapsed' ) )
            group.removeAttribute( 'collapsed' );
          group = group.parentElement.closest( 'sc-sidemenu-group' );
        }
      }
    }
  }

  

}


