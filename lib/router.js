


class SimpleRouter {

  constructor( app ) {
    this.app = app;

    this._lastViews = [];
    this._lastQuery = {};
    this._routeMap = {};
  }


  /**
   * Register a routeable view
   * @param {String} pathpiece          - The view's URL segment
   * @param {Class}  viewclass          - The view's class
   * @param {String} [contextName]      - Context name for URL
   */
  registerView( pathpiece, viewclass, contextName ) {
    if ( this._routeMap.hasOwnProperty( pathpiece ) )
      throw new Error( "Path segment already mapped" );
    this._routeMap[ pathpiece ] = { 'class': viewclass, 'context': contextName };
  }


  /*
   * Navigate to the next state
   * @async
   * @param {String} [overrideUrl]  - Any override URL to render
   * @throws - On any error while navigating
   */
  async navigate( overrideUrl ) {
    const title = [];
    let views, newPath, newHash, newQuery, newUrl, newViews, sameViews, oldViews;

    // Get the URL
    if ( overrideUrl ) {
      newUrl = { 
        'path': overrideUrl,
        'hash': "",
        'query': {}
      };
    }
    else
      newUrl = this.app.getStateProp( 'url' );
    newPath = newUrl.path;
    newHash = newUrl.hash;
    newQuery = newUrl.query;
    this.app.log( 'debug', 'Navigating to', newPath );

    // Clean URL
    newPath = this.cleanPath( newPath );

    try {
      // Identify all views to build the view
      views = this.pathToViews( newPath );

      // Check the structure and that all views can be rendered
      this.checkHeirarchy( views );
    }
    catch( err ) {
      err.navCode = 404;
      throw err;
    }

    // Render
    try { 
      await this.app.builder.render( views, newQuery );
    }
    catch( err ) {
      this.app.log( 'error', 'Unable to render views', err );
      err.navCode = 500;
      throw err;
    }

    // Get lists of new, old and same views
    sameViews = views.filter( v => { return this._lastViews.find( l => l[ 'class' ] == v['class'] ) } );
    oldViews = this._lastViews.filter( v => { return ! views.find( p => p['class'] == v['class'] ) } );
    newViews = views.filter( v => { return ! sameViews.includes( v ) } );

    // Remove and disconnect views not currently rendered.
    for ( let prev of oldViews ) {
      this.app.builder.clear( prev, views );
      setTimeout( () => { prev['class'].disconnect( prev['params'], this._lastQuery, prev['context'] ) }, 10 );
    }

    // Tell same views to reconnect
    for ( let same of sameViews )
      setTimeout( () => { same['class'].reconnect( same['params'], newQuery, same['context'] ) }, 10 );

    // Tell new views to connect
    for( let view of newViews )
      setTimeout( () => { view['class'].connect( view['params'], newQuery, view['context'] ) }, 10 );

    // Store what was rendered
    this._lastViews = views;
    this._lastQuery = newQuery;

    // Update title
    title.push( this.app.name );
    for ( let view of views ) {
      if ( view['class'].title() !== undefined )
        title.push( view['class'].title() );
    }
    document.title = title.join( ' - ' );

    // Scroll as required
    if ( newHash != "" && newHash != undefined ) {
      let elem = document.getElementById( newHash );
      if ( ! elem )
        this.app.log( 'warn', "Navigation hash does not resolve to a page element" );
      else
        elem.scrollIntoView(true);
    }
  }


  /** 
   * Attempt to rebuild a provided view
   * @async
   * @throws              - In event of any error
   */
  async rebuild( viewClass ) {
    try { 
      this.app.log( "debug", "Rebuilding view", viewClass.name );
      for ( let view of this._lastViews ) {
        if ( view[ 'class' ] == viewClass )
          await this.app.builder.reRender( view, this._lastQuery );
      }
    }
    catch( err ) {
      this.app.log( "error", "Failed to rebuild view", err );
    }
  }


  /*
   * Clean a url path of anything unwanted
   * @param {String} path - The path to clean
   * @returns {String} - The clean path, eg /foo/bar
   */
  cleanPath( path ) {
    // Ensure a leading slash
    if ( path.charAt(0) != "/" )
      path = "/" + path;

    // Remove trailing slash
    if ( path.charAt( path.length - 1 ) == "/" )
      path = path.slice( 0, -1 );

    // Clean any double slashes, spaces
    path = path.replace(/\/\/+/g, '/' );
    path = path.replace( /\s/g, '' );

    return path;
  }


  /*
   * Check the heirarchy of views is consistent, identifying descendant view parents
   * @param {Object[]} views - The ordered list of views
   * @throws An exception if any descendant view has no target
   */
  checkHeirarchy( views ) {
    let targetView;

    // Work backwards from innermost to outermost
    views.slice().reverse().forEach( ( view, idx, arr ) => {
      // The top level view doesn't need a target
      if ( idx == ( views.length - 1 ) )
        return;

      // Check for a target
      targetView = arr.slice( idx + 1 ).find( maybeparent => {
        return maybeparent[ 'slots' ] > 0 && 
          maybeparent[ 'class' ].canContain( view ) && 
          view[ 'class' ].canBeContained( maybeparent );
      } )

      // No target view?
      if ( ! targetView ) {
        this.app.log( 'error', 'Unable to identify target for descendant view', view[ 'path' ] );
        throw new Error( "Unable to identify target for descendant view " + view[ 'path' ] );
      }

      // Note the unshift to keep order consistent
      targetView[ 'descendants' ].unshift( view );
      targetView[ 'slots' ]--;
    } );
  }


  /*
   * Take a path and identify all views, returning matching views
   * @param {String} path                       - The path to process
   * @returns {Object[]} views                  - Ordered structures with the view path and class
   * @returns {String} views[].path             - The path of the view segment
   * @returns {Object} views[].class            - The view class for the view
   * @returns {Object} views[].params={}        - Map of any URL bind parameters keyed by name
   * @returns {Object} views[].target={}        - The render target for this view
   * @returns {Object[]} views[].descendants=[] - List for descendant views
   * @returns {Object[]} views[].targets=[]     - List for descendant view render targets
   * @returns {Number} views[].slots            - Number of descendants this view will accept
   */
  pathToViews( path ) {
    const pathBits = path.split( '/' );
    let sortedViews, viewClass, viewContext, startOnly, viewPathBits, failedBits, foundIndex, found, params;

    // Remove leading blank pieces
    if ( pathBits[0] == "" )
      pathBits.splice( 0, 1 );

    // Sort views from most specific to least specific and filter to those that might match.
    sortedViews = Object.keys( this._routeMap ).sort( (a,b) => {
      let bc = 0;
      let ac = 0;
      bc = (b.match(/\//g)||[]).length * 2;
      ac = (a.match(/\//g)||[]).length * 2;
      if ( b.includes(':') ) bc -= 1; 
      if ( a.includes(':') ) ac -= 1;
      return bc - ac;
    } );
    sortedViews = sortedViews.filter( view => { 
      return (view.match(/\//g)||[]).length <= (path.match(/\//g)||[]).length
    } );

    // Match url segments from most specific to least specific
    for( let viewPath of sortedViews ) {
      viewClass = this._routeMap[ viewPath ].class;
      viewContext = this._routeMap[ viewPath ].context;
      startOnly = false;
      viewPathBits = viewPath.split( "/" );
      foundIndex = 0;
      found = 0;
      params = {};

      // Remove any leading empty element
      if ( viewPathBits[ 0 ] == "" )
        viewPathBits.splice( 0, 1 );

      // Only search from the start?
      if ( viewPath.charAt( 0 ) == "/" )
        startOnly = true;

      // Allow multiple matches per view
      while ( foundIndex >= 0 ) {
        // Match known views from most specific to least specific aginst the URL pieces
        foundIndex = this.arrayContains( pathBits, viewPathBits, startOnly );
        if ( foundIndex == -1 ) break;

        // Capture any url parameters
        for ( let [ idx, bit ] of viewPathBits.entries() ) {
          if ( bit.includes( ':' ) )
            params[ bit.slice( 1 ) ] = pathBits[ foundIndex + idx ];
        }

        // Found an index, replace piece(s) with the view class
        pathBits.splice( foundIndex, viewPathBits.length, { 
          'class'       : viewClass, 
          'context'     : viewContext,
          'path'        : viewPath,
          'params'      : params,
          'descendants' : [],
          'target'      : {},
          'targets'     : [],
          'slots'       : viewClass.slotCount(),
        } );
        found += viewPathBits.length;

        // If have all bits found, stop
        if ( found == pathBits.length ) break;
      }

      if ( ! pathBits.some( bit => typeof bit == "string" ) )
        break;
    }

    // Any view segment not resolved to a view?
    if ( ! pathBits.every( bit => typeof bit != "string" ) ) {
      this.app.log( 'error', 'Unable to fully resolve path' );
      failedBits = pathBits.filter( bit => { return typeof bit == "string" } );
      for ( let bit of failedBits ) {
        this.app.log( 'error', "View segment", bit, "did not resolve" );
      }
      throw new Error( "Unable to fully resolve path to views" );
    }

    return pathBits;
  }


  /*
   * Check if one array contains another, in order
   * @param {string[]} array1 - The maybe-containing array
   * @param {string[]} array2 - The array to look for
   * @param {Boolean} start - If a match is only possible at the start
   * @returns {Number} - Index of the match, or -1 if not found.
   */
  arrayContains( array1, array2, start ) {
    let failnow = false;
    let allfound = false;
    let idx = 0;      // Index of url-array being tested
    let foundpos = 0; // Any index found of the view's path-array
    let from = 0;     // The first index found of the match

    // Loop through all elemnents
    while( ! failnow && ! allfound ) {

      // Look for the next piece
      // eg 'foo' from [ 'foo', 'bar' ] within [ 'foo', 'goo' ]
      foundpos = array1.indexOf( array2[ idx ] ); 

      // Support url parameters which should always be in a multi-segment path
      if ( foundpos == -1 && array2[ idx ].charAt( 0 ) == ':' && typeof array1[ from + idx ] == "string" )
        foundpos = from + idx;

      // First piece not found? Early exist
      if ( foundpos == -1 ) {
        failnow = true;
        break;
      }

      // Need to be at the front?
      if ( start && foundpos != 0 && idx == 0 ) {
        failnow = true;
        break;
      }

      // Note the starting index of the match
      if ( idx == 0 ) from = foundpos;

      // Subsequent pieces not found where expected?
      if ( idx > 0 && foundpos != from + idx ) {
        failnow = true;
        break;
      }

      // All found?
      if ( idx == array2.length - 1 )
        allfound = true;

      // Piece found, look for the next
      idx++;
    }

    if ( failnow )
      return -1;
    else
      return from;

  }

}


export default SimpleRouter;

