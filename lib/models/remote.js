

import equal from 'fast-deep-equal';


/**
* Load information type
* @typedef {Object} LoadInfoType
* @property {Array<String|Number>} [load]      - List of loaded record IDs
* @property {Array<String|Number>} [update]    - List of updated record IDs
* @property {Array<String|Number>} [delete]    - List of deleted record IDs
*/

/**
* Persist information type
* @typedef {Object} PersistInfoType
* @property {Array<String|Number>} [insert]    - List of inserted record IDs
* @property {Array<String|Number>} [update]    - List of updated record IDs
* @property {Array<String|Number>} [delete]    - List of deleted record IDs
*/


/**
 * The remote model mixin. Adds functionality for a fully cached remotely-sourced dataset.
 *
 * As this is a fully cached model, it is not suitable for very large datasets.
 *
 * This model keeps and tracks changes within the cached data until it is
 * to be persisted. In the case of multiple operations, will perform a delete,
 * then updates, then inserts. Any failure prevents further operations from
 * taking place.
 *
 * @param {*} baseclass - The class to mix onto.
 * @return {mixin:RemoteModel}
 */
const RemoteModel = baseclass =>


/**
 * @mixin
 * @alias RemoteModel
 */
class extends baseclass {

  /**
   * Constructor for a remote data collection model
   * @see {BaseModel#constructor}
   * @param {Object}    app                              - The main application object
   * @param {...Object} confs                            - Configuration options
   * @param {Object}    [confs]                          - Single configuration object
   * @param {Boolean}   [confs.versioned=false]          - If record(s) are versioned with the version field
   * @param {String}    [confs.versionField="version"]   - The version field, defaulting to 'version'
   * @param {Object}    [confs.url]                      - Object map containing URLs for actions
   * @param {String}    [confs.url.update]               - The URL to use for update actions
   * @param {String}    [confs.url.delete]               - The URL to use for delete actions
   * @param {String}    [confs.url.insert]               - The URL to use for insert actions
   * @param {String}    [confs.url.load]                 - The URL to use for fetch actions
   * @param {Boolean}   [confs.loadFirstFetch=false]     - If the model should load on first fetch
   * @param {Boolean}   [confs.loadQuery=false]          - If the model should (re)load on query change
   * @param {Boolean}   [confs.loadContext=false]        - If the model should (re)load on context change
   * @param {Number}    [confs.batchSize=0]              - The number of records sent per request to remote store. Zero means single batch for all records.
   */
  constructor( app, ...configs ) {

    const defaults = {
      modelType       : [ 'remote' ],
      url             : {},
      loadFirstFetch  : false,
      loadQuery       : false,
      loadContext     : false,
      versioned       : false,
      versionField    : 'version',
      batchSize       : 0
    };

    super( app, defaults, ...configs );

    // Default version to zero
    if ( this.config[ 'versioned' ] && ! this.config.defaultValues.hasOwnProperty( this.config.versionField ) )
      this.config.defaultValues[ this.config.versionField ] = 0;

    return this;
  }


  /**
   * Setup the collection
   * @protected
   */
  setup() {

    super.setup();

    // State properties
    this._loading = undefined; /* store a load promise */
    this._operation = undefined;
    this._loaded = false;
    this._inconsistent = false;
    this._recovery = false;
    this._recoverCount = 0;
    this._context = {};
    this._query = {};
    this._modified = [];
    this._reidentified = undefined;
    this._clearonload = false;
    this._loadedQuery = undefined;
    this._loadedContext = undefined;
  }


  /**
   * Has this model failed to persist something or is it inconsistent?
   * @public
   * @type {Boolean}                      - If failed
   */
  get failed() {
    let failed = false;
    if ( this._inconsistent )
      failed = true;
    if ( ! failed && this.config.modelType.includes( 'collection' ) )
      failed = this._ids.some( id => this._data.get( id )[ '_errored' ] );
    else if ( ! failed )
      failed = this._record[ '_errored' ];
    return failed;
  }


  /**
   * Is this model inconsistent or de-syncronised with the remote store?
   * @public
   * @type {Boolean}                      - If inconsistent
   */
  get inconsistent() {
    return this._inconsistent;
  }


  /**
   * Is this model loaded?
   * @public
   * @type {Boolean}                      - If this model is loaded
   */
  get loaded() {
    return this._loaded;
  }


  /**
   * Does this model have any non-persisted changes?
   * @public
   * @return {Boolean}                    - If any unsaved changes exist
   */
  isChanged() {
    if ( this.config.modelType.includes( 'record' ) )
      return this._record[ '_operation' ] !== undefined;
    else
      return this._modified.length > 0;
  }


  /**
   * Wait for the current operation to be complete
   * @public
   * @async
   * @param {Number} [max=200]  - Maximum time in seconds to wait
   * @returns {Promise}         - A promise which resolves or rejects if max time is reached
   */
  async waitForOperation( max=200 ) {
    const between = 200;
    const since = Date.now();

    let waiter = () => {
      return new Promise( res => {
        setTimeout(() => { res() }, between );
      })
    }

    while ( this._operation !== undefined ) {
      await waiter();
      if ( since + ( max * 1000 ) < Date.now() )
        throw new Error( 'Operation continues after timeout reached' );
    }
  }


  /**
   * Get the current context of this model
   * @public
   * @param {Boolean} [previous=false]  - If the previous context loaded with should be returned
   * @return {Object}                   - The current context of this model
   */
  getContext( previous=false ) {
    if ( previous )
      return this.app.util.clone( this._loadedContext );
    else
      return this.app.util.clone( this._context );
  }


  /**
   * Get the current query used by this model
   * @public
   * @param {Boolean} [previous=false]  - If the previous query loaded with should be returned
   * @return {Object}   - Any current query used for requests by this model
   */
  getQuery( previous=false ) {
    if ( previous )
      return this.app.util.clone( this._loadedQuery );
    else
      return this.app.util.clone( this._query );
  }


  /**
   * Set the model's context and or query
   * @public
   * @async
   * @param {Object} [context]  - Context map with URL bind variables for further queries
   * @param {Object} [query]    - Query map for further queries
   * @returns {Promise}         - A promise which resolves if context is set, otherwise throws
   */
  async setContextQuery( context={}, query={} ) {
    const oldcontext = this.getContext( true );
    const oldquery = this.getQuery( true );
    let newcontext = this.app.util.clone( context );
    let newquery = this.app.util.clone( query );
    let querychanged = false;
    let contextchanged = false;
    let load = false;

    querychanged = equal( oldquery, newquery ) ? false : true;
    contextchanged = equal( oldcontext, newcontext ) ? false : true;

    if ( ! querychanged && ! contextchanged ) {
      this.app.log( "debug", "Context and query unchanged" );
      return;
    }

    try {
      // Set context and or query and flag clearing
      if ( ! contextchanged )
        newcontext = oldcontext
      if ( ! querychanged )
        newquery = oldquery;

      // Set model to clear
      this._clearonload = true;

      // Reload as needed
      if ( contextchanged && this.config.loadContext )
        load = true;
      else if ( querychanged && this.config.loadQuery )
        load = true;

      if ( load && this.checkBinds( newcontext ) ) {
        await this.load( newcontext, newquery );
      }
    }
    catch( err ) {
      // Fall back context on failure
      this.app.log( "error", "Set context or query failed:", err.message );
      throw err;
    }
  }


  /**
   * Check that we have all required context (binds) for our URLs
   * @param {Object} [context]    - Alternate context to check with
   * @returns {Boolean}           - If all required binds are complete
   */
  checkBinds( context ) {
    if ( ! context )
      context = this._context;

    for ( let urltype in this.config.url ) {
      let url = this.config.url[ urltype ];

      if ( url.includes( ':' ) ) {
        let fields = url.match( /:(\w+?):/g ) || [];
        for ( let field of fields ) {
          let prop = field.replace( /\:/g, '' );
          if ( ! context.hasOwnProperty( prop ) ) {
            return false;
          }
        }
      }
    }

    return true;
  }



  /**
   * Get the metadata for a record
   * @public
   * @param {String|Number} id            - The record ID to fetch metadata for
   * @returns {Object} meta               - The resulting record metadata if record is found, otherwise undefined
   * @returns {String} meta.operation     - Any pending operation, insert, update, delete, or ""
   * @returns {Boolean} meta.errored      - If this record is in an errored state
   * @returns {String} meta.message       - Any informational or error message attached to the record
   */
  getRecordInfo( id ) {
    let rec, ret = {
      'operation': "",
      'errored': false,
      'message': ""
    };

    if ( this.config.modelType.includes( 'record' ) ) {
      if ( this._record ) {
        ret[ 'operation' ] = this._record[ '_operation' ];
        ret[ 'errored' ] = this._record[ '_errored' ];
        ret[ 'message' ] = this._record[ '_message' ];
        return ret;
      }
    }
    else {
      rec = this._data.get( id );
      if ( rec ) {
        ret[ 'operation' ] = rec[ '_operation' ];
        ret[ 'errored' ] = rec[ '_errored' ];
        ret[ 'message' ] = rec[ '_message' ];
        return ret;
      }
    }
  }


  /**
   * Get any changes to a specific record
   * @returns {Object}                    - Changes to the record, empty if none
   */
  getChanges( id ) {
    let record;
    if ( this.config.modelType.includes( 'record' ) ) {
      if ( id !== undefined && id !== this._record[ this.idField ] )
        return {};
      if ( this._record._operation == 'insert' )
        return this.getRecord( id );
      return this.app.util.clone( this._record[ '_pending' ] );
    }
    else {
      if ( ! this._ids.includes( id ) )
        return {};
      record = this._data.get( id );
      if ( record._operation == 'insert' )
        return this.getRecord( id );
      return this.app.util.clone( record._pending );
    }
  }



  /**
   * Get a modification safe record for an ID
   * @param {String|Number} id      - The record ID to fetch
   * @returns {Object|undefined}    - The resulting record or undefined if it doesn't exist
   */
  getRecord( id ) {
    let rec;
    if ( this.config.modelType.includes( 'record' ) ) {
      if ( id !== undefined && id !== this._record[ this.idField ] )
        return undefined;
      return Object.assign(
        {},
        this.pick( this._record ),
        this._record[ '_pending' ]
      );
    }
    else {
      if ( ! this._ids.includes( id ) )
        return undefined;
      rec = this._data.get( id );
      return Object.assign(
        {},
        this.pick( rec ),
        rec[ '_pending' ]
      );
    }
  }



  /**
   * Get a modification unsafe record for an ID
   * @private
   * @param {String|Number} id            - The record ID to fetch
   * @returns {Object|undefined}          - The resulting record or undefined if it doesn't exist
   */
  _getRecord( id ) {
    if ( this.config.modelType.includes( 'record' ) ) {
      if ( id !== undefined && id !== this._record[ this.idField ] )
        return undefined;
      return this._record;
    }
    else {
      return super._getRecord( id );
    }
  }


  /**
   * Return a promise to load JSON data from somewhere. Resolves when successful, or rejects if recovery is disabled or fails.
   * @async
   * @override
   * @param {Object} [context]            - Any context (bind variables) for the dataset's urls
   * @param {Object} [query]              - Any supplemental query specific for the load
   * @returns {Promise<LoadInfoType>}     - Promise which resolves or throws
   */
  async load( context, query ) {
    // Duplicate loads get the same promise
    if ( this._loading )
      return this._loading;

    this.app.log( "debug", "Loading model", this.constructor.name );

    if ( this._operation !== undefined ) {
      this.app.log( "error", "Unable to load while another operation in progress" );
      throw new Error( "Unable to load while another operation in progress" );
    }

    // If context or query has changed, clear previous loaded data
    if ( context && this._context && ! equal( this.getContext( true ), context ) )
      this._clearonload = true;
    else if ( query && this._query && ! equal( this.getQuery( true ), query ) )
      this._clearonload = true;

    // Keep any context & supplemental query parameters
    if ( context )
      this._context = this.app.util.clone( context );
    if ( query )
      this._query = this.app.util.clone( query );

    if ( ! this.checkBinds() ) {
      this.app.log( "debug", "Cannot load without context parameters" );
      throw new Error( "Cannot load without context parameters" );
    }

    this._operation = "load";

    // Attempt the load
    this._loading = this._load();
    this._loading
      .then( () => {
        // Mark as loaded, clear the promise
        this._loading = undefined;
        this._operation = undefined;
      } )
      .catch( (err) => {
        this.app.log( "error", "Failure during load", err );
        this._loading = undefined;
        this._operation = undefined;
        this._inconsistent = true;
        throw err;
      } );

    return this._loading;
  }


  /**
   * Reload the model using the same context and query
   * @async
   * @returns {Promise<LoadInfoType>}     - Promise which resolves or throws
   */
  async reload() {
    // Duplicate loads get the same promise
    if ( this._loading )
      return this._loading;

    this.app.log( "debug", "Re-loading model", this.constructor.name );

    // Attempt the load
    return this.load( this._loadedContext, this._loadedQuery );
  }



  /**
   * Internal method to load or reload JSON data from somewhere
   * @private
   * @async
   * @returns {LoadInfoType}          - A summary of changes from this load
   */
  async _load() {
    const query = this._query;
    let url, response, records, keepids, keepdata, nowids, curids, remids, loadids, changedids;

    // Set the operation in the query
    query[ 'operation' ] = 'load';

    this.app.log( "debug", "Loading data from remote data store" );

    // Try the load
    try {
      url = this.getUrl( 'load' );

      response = await this._remoteRequest( url, 'GET', query );

      if ( ! response || response['success'] != true ) {
        this.app.log( "error", "Load failed", response );
        throw new Error( response.message );
      }

      // Ensure we have records
      records = response[ 'records' ] || response[ 'record' ];
      if ( ! records ) {
        this.app.log( "error", "Malformed response", response );
        throw new Error( "Malformed response" );
      }

      // Store context and query used
      this._loadedQuery = this._query;
      this._loadedContext = this._context;

      // Clear previous load if needed, keeping inserts only
      if ( this._clearonload && this.config.modelType.includes( 'collection' ) ) {
        this.app.log( "debug", `Clearing previously loaded records on load` );
        keepids = [];
        keepdata = new Map();
        for ( let modId of this._modified ) {
          if ( this._data.get( modId )._operation == "insert" ) {
            keepids.push( modId );
            keepdata.set( modId, this.app.util.clone( this._data.get( modId ) ) );
          }
        }
        this._data = keepdata;
        this._ids = keepids;
        this._clearonload = false;
      }

      // Get loaded records and cooerce to array
      if ( ! Array.isArray( records ) )
        records = [ records ];
      nowids = records.map( r => r[ this.idField ] );

      // Records models always lose local records regardless if loading
      if ( this.config.modelType.includes( 'record' ) && records.length ) {
        if ( this._record[ this.idField ] !== records[ 0 ][ this.idField ] )
          this._record = {};
      }

      // Get current IDS
      if ( this.config.modelType.includes( 'record' ) ) {
        if ( this.length )
          curids = [ this._record[ this.idField ] ];
        else
          curids = [];
      }
      else
        curids = this.app.util.clone( this._ids );

      // Find existing records no longer there
      remids = curids.filter( c => { return ! nowids.includes( c ) } );
      remids = remids.filter( c => { return this._getRecord( c )[ '_operation' ] !== "insert" } );

      // Find new records
      loadids = nowids.filter( n => {
        return ! curids.includes( n ) || this._getRecord( n )._operation == "insert"
      } );

      // Merge in updates, additions
      changedids = this.parse( records, 'load' );

      // Remove new IDs from changed IDs
      changedids = changedids.filter( c => { return ! loadids.includes( c ) } );

      // Remove gone records
      if ( remids.length )
        super.remove( remids );

      this._loaded = true;
      this._inconsistent = false;
    }
    catch( err ) {
      this._inconsistent = true;
      this.app.log( "error", "Failure during loading", err );
      throw new Error( `Failure during loading: ${err.message}` );
    }
    return {
      'load': loadids,
      'update': changedids,
      'delete': remids
    };
  }




  /**
   * Fetch one or more records
   * @override
   * @see {BasicCollection#fetch}
   */
  async fetch( ...args ) {

    // Load if lazy access
    if ( this.config.loadFirstFetch && ! this.loaded )
      await this.load();

    return super.fetch( ...args );
  }


  /**
   * Fetch one record only
   * @override
   * @see {BasicCollection#fetchOne}
   */
  async fetchOne( ...args ) {
    // Load if lazy access
    if ( this.config.loadFirstFetch && ! this.loaded )
      await this.load();

    return super.fetchOne( ...args );
  }


  /**
   * Reset all changes to one or more records
   * @param {String|String[]} ids             - The IDs for the records to reset
   * @returns {Object} info                   - Details about IDs that were persisted
   * @returns {String[]} info.delete          - The IDs of records deleted
   * @returns {String[]} info.update          - The IDs of records updated
   * @returns {String[]} info.insert          - The IDs of records inserted
   */
  reset( ids ) {
    const info = { 'insert': [], 'delete': [], 'update': [] };
    const blankmeta = {
      '_pending':     {},
      '_operation':   "",
      '_errored':     false,
      '_message':     undefined
    };
    let rec;

    // Cooerce to array
    if ( ! Array.isArray( ids ) )
      ids = [ ids ];

    ids.forEach( id => {

      this.app.log( "debug", "Resetting record", id );

      rec = this._getRecord( id );
      switch( rec[ '_operation' ] ) {
        case "insert":
          super.remove( [ id ] );
          info[ 'delete' ].push( id );
          break;
        case "update":
          this._setRecord( Object.assign( {}, this.pick( rec ), blankmeta ) );
          info[ 'update' ].push( id );
          break;
        case "delete":
          this._setRecord( Object.assign( {}, this.pick( rec ), blankmeta ) );
          info[ 'insert' ].push( id );
          break;
      }
    } );

    return info;
  }


  /**
   * Insert one or more new records into the data cache. Resolves with a list of IDs added.
   * Either auto-persists, or persist manually by calling persist().
   * @override
   * @param {Object|Object[]} records         - The data record(s) to insert
   * @returns {String[]} ids                  - The record ID or IDs effected by this action
   * @throws                                  - In case of any error
   */
  add( records ) {
    let toadd = [], record, ids = [];

    if ( this._inconsistent ) {
      this.app.log( "error", "Refusing to add records to an inconsistent model" );
      throw new Error( "Refusing to add records to an inconsistent model" );
    }

    this.app.log( 'debug', 'Adding record(s) to data model' );

    // Cooerce to array
    if ( ! Array.isArray( records ) )
      records = [ records ];

    if ( records.length > 1 && this.config.modelType.includes( 'record' ) ) {
      this.app.log( "error", "Unable to add multiple records to a single record model" );
      throw new Error( "Unable to add multiple records to a single record model" );
    }

    if ( this.config.modelType.includes( 'record' ) && Object.keys( this._record ).length > 0 ) {
      this.app.log( "error", "Cannot add record to non-empty record model" );
      throw new Error( "Cannot add record to non-empty record model" );
    }

    // Add any defaults
    records.forEach( record => {
      toadd.push( this.pick( this.app.util.deepMerge( {}, this.config.defaultValues, record ) ) );
    } );

    // Check records have required fields
    this.check( toadd );

    // Add each into our cache and mark as new
    toadd.forEach( addrec => {

      // Create a modification safe record
      record = this.pick( addrec );

      // Ensure an ID is set, even if remote authority ignores it
      if ( ! record.hasOwnProperty( this.idField ) || record[ this.idField ] === "" )
        record[ this.idField ] = this.newId();

      // Meta info
      record[ '_pending' ] = {};
      record[ '_operation' ] = "insert";
      record[ '_errored' ] = false;
      record[ '_message' ] = undefined;

      // Directly add the new record
      if ( this.config.modelType.includes( 'collection' ) ) {
        this._ids.push( record[ this.idField ] );
        this._data.set( record[ this.idField ], record );
        // To assist finding this record later
        this._modified.push( record[ this.idField ] );
      }
      else
        this._record = record;

      ids.push( record[ this.idField ] );
    } );

    return ids;
  }


  /**
   * Add one or more records to the remote store. Resolves to a list of added IDs or throws
   * @private
   * @async
   * @param {Object[]} records         - The data record(s) to insert
   * @returns {Promise<Array>}         - List of new records added
   */
  async _add( records ) {
    let tmp, response, mapping, newrecords, url, fatal = false, query = {}, tmpids = [], newids = [];

    // Persist
    try {
      url = this.getUrl( 'insert' );

      // Keep the IDs of temporary records for a moment
      tmpids = records.map( r => r[ this.idField ] );

      // Build query
      query[ 'operation' ] = "insert";
      if ( this.config.modelType.includes( 'collection' ) )
        query[ 'records' ] = records;
      else
        query[ 'record' ] = records[ 0 ];

      // Make the request
      response = await this._remoteRequest( url, 'POST', query );

      newrecords = response[ 'records' ] || response[ 'record' ];
      mapping = response[ 'record_map' ];

      // Check for failure
      if ( ! response || ! response['success'] ) {
        this.app.log( "error", "Operation failed", response );
        throw new Error( "Operation failure: " + response.message );
      }

      if ( ! newrecords ) {
        this.app.log( "error", "Malformed response", response );
        throw new Error( "Malformed response: missing record structure" );
      }
    }
    catch( err ) {
      this.app.log( 'error', 'Failed to add one or more records:', err );

      // Error messages for specific records?
      if ( response && response[ 'messages' ] && typeof response[ 'messages' ] == "object" ) {
        for ( let id of Object.keys( response[ 'messages' ] ) )
          this.setRecordError( id, response[ 'messages' ][ id ] );
        fatal = false;
      }
      // If there's an error for a specific record
      else if ( response && response[ 'record' ] && response[ 'record' ].hasOwnProperty( this.idField ) ) {
        if ( this.hasRecord( response[ 'record' ][ this.idField ] ) )
          this.setRecordError( response[ 'record' ][ this.idField ], response[ 'message' ] );
        fatal = true;
      }
      // Otherwise set same message for all records
      else if ( response && response[ 'message' ] ) {
        for( let record of records )
          this.setRecordError( record[ this.idField ], response[ 'message' ] );
        fatal = true;
      }
      // Just set the error message on all records
      else {
        for( let record of records )
          this.setRecordError( record[ this.idField ], err.message );
        fatal = true;
      }

      if ( fatal )
        throw err;
    }

    try {

      if ( ! Array.isArray( newrecords ) )
        newrecords = [ newrecords ];

      newids = this.parse( newrecords, 'insert' );

      // Remove old records and store any ID changes
      if ( mapping ) {
        for ( let oldid in mapping ) {
          tmp = tmpids.find( i => i == oldid || i == parseInt( oldid ) );
          if ( tmp !== undefined ) {
            this._reidentified.set( tmp, mapping[ oldid ] );
            this.remove( tmp );
          }
        }
      }
      else if ( tmpids.length == 1 && newids.length == 1
          && records[0][ this.idField ] !== newrecords[0][ this.idField ]
      ) {
        // Persisted one record, got one back with different ID, so re-identify
        this._reidentified.set( tmpids[ 0 ], newids[ 0 ] );
        this.remove( tmpids );
      }
      else {
        // No known mapping, just remove records with temporary IDs
        for ( let oldid of tmpids ) {
          if ( ! newids.includes( oldid ) )
            this.remove( oldid );
        }
      }
    }
    catch( e ) {
      this.app.log( 'error', 'Failed to parse added record, model inconsistent.', e );
      this._inconsistent = true;
      throw e;
    }

    return newids;
  }


  /**
   * Rollback any attempted or pending change
   */
  rollback() {
    let rec, operation, remids = [];
    this.app.log( "info", "Abandoning changes and operation" );

    // Forget pending operation
    this._operation = undefined;

    // Forget loading error
    if ( this._loading )
      this._loading = undefined;

    // Clear list of modified records
    this._modified = [];

    // Undo all data changes
    if ( this.config.modelType.includes( 'collection' ) ) {
      for ( let id of this._ids ) {
        rec = this._data.get( id );
        if ( rec[ '_operation' ] == "insert" )
          remids.push( rec[ this.idField ] );
        else if ( rec[ '_operation' ] == "delete" || rec[ '_operation' ] == "update" ) {
          rec[ '_operation' ] = undefined;
          rec[ '_errored' ] = false;
          rec[ '_pending' ] = {};
          rec[ '_message' ] = undefined;
        }
      }

      // Remove added records
      if ( remids.length )
        super.remove( remids );
    }
    else {
      // Undo a record change
      if ( ! this._record )
        return;
      rec = this._record;
      operation = rec[ '_operation' ];
      if ( operation == "insert" )
        this._record = {};
      if ( operation == "delete" || operation == "update" ) {
        rec[ '_operation' ] = undefined;
        rec[ '_errored' ] = false;
        rec[ '_pending' ] = {};
        rec[ '_message' ] = undefined;
      }
    }
  }


  /**
   * Reset the model to a clean initialised state
   * @async
   */
  async clear() {
    super.clear();
    this._loaded = false;
    this._loading = undefined;
    this._operation = undefined;
    this._inconsistent = false;
    this._clearonload = false;
    this._context = {};
    this._query = {};
    this._loadedQuery = {};
    this._loadedContext = {};
    this._modified = [];
  }



  /**
   * Mark one or more data records for deletion by their corresponding ID. New records will
   * be removed immediately. Either auto-persists, or persist manually by calling persist().
   * @override
   * @param {String|String[]} ids       - The ID or list of IDs to remove.
   * @returns {String[]}                - The IDs impacted by this change
   * @throws                            - In event of any failure
   */
  remove( ids ) {
    let record, removed = [];

    if ( this._inconsistent ) {
      this.app.log( "error", "Refusing to remove records from an inconsistent model" );
      throw new Error( "Refusing to remove records from an inconsistent model" );
    }

    this.app.log( 'debug', 'Marking record(s) for removal' );

    // Cooerce to array
    if ( ! Array.isArray( ids ) )
      ids = [ ids ];

    if ( ids.length > 1 && this.config.modelType.includes( 'record' ) ) {
      this.app.log( "error", "Unable to remove multiple records from a single record model" );
      throw new Error( "Unable to remove multiple records from a single record model" );
    }

    // Check each record exists before deletion
    if ( ! ids.every( id => this.hasRecord( id ) ) ) {
      this.app.log( "error", "Unable to remove record not known" );
      throw new Error( "Unable to remove record not known" );
    }

    if ( this.config.modelType.includes( 'record' ) ) {
      // Removing a new record just removes it
      if ( this._record[ '_operation' ] == "insert" ) {
        super.remove();
        if ( this._modified.includes( this._record[ this.idField ] ) )
          this._modified.splice( this._modified.indexOf( this._record[ this.idField ] ), 1 );
      }
      else
        this._record[ '_operation' ] = "delete";
      removed.push( this._record[ this.idField ] );
    }
    else {
      for ( let id of ids ) {
        record = this._data.get( id );
        if ( record[ '_operation' ] == "insert" ) {
          super.remove( [ id ] );
          if ( this._modified.includes( id ) )
            this._modified.splice( this._modified.indexOf( id ), 1 );
        }
        else {
          record[ '_operation' ] = "delete";
          if ( ! this._modified.includes( record[ this.idField ] ) )
            this._modified.push( record[ this.idField ] );
        }
        removed.push( id );
      }
    }
    return ids;
  }




  /**
   * Remove one or more records from the remote store
   * @private
   * @async
   * @param {Object|Object[]} records       - The record(s) to remove
   * @returns {Promise<String[]|Number[]>}  - The IDs impacted by this change
   */
  async _remove( records ) {
    const query = {};
    let parsedIds, remrecords, response, url, fatal = false;

    // Persist as needed
    try {
      url = this.getUrl( 'delete' );

      // Build query
      query[ 'operation' ] = "delete";
      if ( this.config.modelType.includes( 'collection' ) )
        query[ 'records' ] = records;
      else
        query[ 'record' ] = records[ 0 ];

      // Make the request
      response = await this._remoteRequest( url, 'POST', query );

      remrecords = records;

      // Check for failure
      if ( ! response || ! response['success'] ) {
        this.app.log( "error", "Operation failed", response );
        throw new Error( "Operation failure: " + response.message );
      }
    }
    catch( err ) {
      this.app.log( 'error', 'Failed to remove one or more records', err );

      // Error messages for specific records?
      if ( response && response[ 'messages' ] && typeof response[ 'messages' ] == "object" ) {
        remrecords = remrecords.filter( (rec) => {
          if ( response[ 'messages' ].hasOwnProperty( rec[ this.idField ] ) ) {
            this.setRecordError( id, response[ 'messages' ][ id ] );
            return false;
          }
          else
            return true;
        } );
        fatal = false;
      }
      // If there's an error for a specific record
      else if ( response && response[ 'record' ] && response[ 'record' ].hasOwnProperty( this.idField ) ) {
        if ( this.hasRecord( response[ 'record' ][ this.idField ] ) )
          this.setRecordError( response[ 'record' ][ this.idField ], response[ 'message' ] );
        fatal = true;
      }
      // Otherwise set same message for all records
      else if ( response && response[ 'message' ] ) {
        for( let record of records )
          this.setRecordError( record[ this.idField ], response[ 'message' ] );
        fatal = true;
      }
      // Just set the error message on all records
      else {
        for( let record of records )
          this.setRecordError( record[ this.idField ], err.message );
        fatal = true;
      }

      if ( fatal )
        throw err;
    }

    try {
      parsedIds = this.parse( remrecords, 'delete' );
    }
    catch( e ) {
      this.app.log( 'error', 'Failed to parse removed records, model inconsistent.', e );
      this._inconsistent = true;
      throw e;
    }
    return parsedIds;
  }


  /**
   * Mark or perform an update for one or more records.
   * Either auto-persists, or persist manually by calling persist().
   * @override
   * @param {Object|Object[]} records       - The updated record or records to store.
   * @returns {String[]}                    - The IDs impacted by this change
   */
  update( records ) {
    let rec, delta, ids = [];

    if ( this._inconsistent ) {
      this.app.log( "error", "Refusing to update records in an inconsistent model" );
      throw new Error( "Refusing to update records in an inconsistent model" );
    }

    this.app.log( 'debug', 'Marking record(s) for update' );

    // Cooerce to array
    if ( ! Array.isArray( records ) )
      records = [ records ];

    if ( records.some( r => r[ this.idField ] == undefined ) ) {
      this.app.log( "error", "Cannot update record without ID" );
      throw new Error( "Cannot update record without ID" );
    }

    if ( ! records.every( r => this.hasRecord( r[ this.idField ] ) ) ) {
      this.app.log( "error", "Cannot update record that doesn't exist" );
      throw new Error( "Cannot update record that doesn't exist" );
    }

    // Check records have required fields
    this.check( records );

    records.forEach( record => {
      rec = this._getRecord( record[ this.idField ] );
      delta = this._delta( this.pick( record ) );

      // Make sure there's a change
      if ( Object.keys( delta ).length == 0 ) {
        this.app.log( "warning", "No changes identified in request to update record" );
        return;
      }

      if ( rec[ '_operation' ] !== "insert" )
        rec[ '_operation' ] = "update";

      rec[ '_pending' ] = Object.assign( {}, rec[ '_pending' ], delta );
      if ( ! this._modified.includes( rec[ this.idField ] ) )
        this._modified.push( rec[ this.idField ] );
      ids.push( rec[ this.idField ] );
    } );

    return ids;
  }


  /**
   * Update one or more records to the remote data store
   * @private
   * @async
   * @param {Object|Object[]} records       - The updated record or records to store.
   * @returns {Promise<String[]|Number[]>}  - The IDs impacted by this change
   */
  async _update( records ) {
    const query = {};
    let parsedIds, response, updrecords, url, fatal = false;

    // Persist
    try {
      url = this.getUrl( 'update' );

      // Build query
      query[ 'operation' ] = "update";
      if ( this.config.modelType.includes( 'collection' ) )
        query[ 'records' ] = records;
      else
        query[ 'record' ] = records[ 0 ];

      // Make the request
      response = await this._remoteRequest( url, 'POST', query );

      updrecords = response[ 'records' ] || response[ 'record' ];

      // Check for failure
      if ( ! response || ! response['success'] ) {
        this.app.log( "error", "Operation failed", response );
        throw new Error( "Operation failure: " + response.message );
      }

      if ( ! updrecords ) {
        this.app.log( "error", "Malformed response", response );
        throw new Error( "Malformed response: missing record structure" );
      }
    }
    catch( err ) {
      this.app.log( 'error', 'Failed to update one or more records', err );

      // Error messages for specific records?
      if ( response && response[ 'messages' ] && typeof response[ 'messages' ] == "object" ) {
        for ( let id of Object.keys( response[ 'messages' ] ) )
          this.setRecordError( id, response[ 'messages' ][ id ] );
        fatal = false;
      }
      // Specific record error?
      else if ( response && response[ 'record' ] && response[ 'record' ].hasOwnProperty( this.idField ) ) {
        if ( this.hasRecord( response[ 'record' ][ this.idField ] ) )
          this.setRecordError( response[ 'record' ][ this.idField ], response[ 'message' ] );
        fatal = true;
      }
      // Otherwise set same message for all records
      else if ( response && response[ 'message' ] ) {
        for( let record of records )
          this.setRecordError( record[ this.idField ], response[ 'message' ] );
        fatal = true;
      }
      // Some other error, set the  message on all records
      else {
        for( let record of records )
          this.setRecordError( record[ this.idField ], err.message );
        fatal = true;
      }

      if ( fatal )
        throw err;
    }

    try {
      parsedIds = this.parse( updrecords, 'update' );
    }
    catch( e ) {
      this.app.log( 'error', 'Failed to parse updated record, model inconsistent.', e );
      this._inconsistent = true;
      throw e;
    }
    return parsedIds;
  }


  /**
   * Persist pending changes to the remote model
   * @async
   * @param {String[]} ids                  - An optional list of IDs to persist instead of all
   * @returns {Promise<PersistInfoType>}    - Resolves to info on records persisted
   * @throws                                - In the event of an error breaking consistency
   */
  async persist( ids=[] ) {
    let batchsize, tmp, batchrecs = [], records = [];
    let removed = [], updated = [], added = [], done = [];
    batchsize = this.config.batchSize;

    if ( this._inconsistent ) {
      this.app.log( "error", "Refusing to submit local changes of inconsistent model. Reload required" );
      throw new Error( "Refusing to submit local changes of inconsistent model. Reload required." );
    }

    if ( this._operation != undefined ) {
      this.app.log( "error", "Unable to persist changes while another operation in progress" );
      throw new Error( "Unable to persist changes while another operation in progress" );
    }

    this._operation = "persist";

    this.app.log( "info", "Persisting model changes to remote store" );
    this.app.log( "debug", "Persisting model changes to remote store, ", ids );

    this._reidentified = new Map();

    try {

      // Deletes first
      records = this._getByOperation( 'delete', ids );
      while ( records.length > 0 ) {
        if ( batchsize > 0 )
          batchrecs = records.splice( 0, batchsize );
        else
          batchrecs = records.splice( 0 );
        try {
          tmp = await this._remove( batchrecs );
          removed = removed.concat( tmp );
        }
        catch( err ) {
          this.app.log( "error", "Failure during delete operation, aborting" );
          throw err;
        }
      }

      // Updates
      records = this._getByOperation( 'update', ids );
      while ( records.length > 0 ) {
        if ( batchsize > 0 )
          batchrecs = records.splice( 0, batchsize );
        else
          batchrecs = records.splice( 0 );
        try {
          tmp = await this._update( batchrecs );
          updated = updated.concat( tmp );
        }
        catch( err ) {
          this.app.log( "error", "Failure during update operation, aborting" );
          throw err;
        }
      }

      // Inserts
      records = this._getByOperation( 'insert', ids );
      while ( records.length > 0 ) {
        if ( batchsize > 0 )
          batchrecs = records.splice( 0, batchsize );
        else
          batchrecs = records.splice( 0 );
        try {
          tmp = await this._add( batchrecs );
          added = added.concat( tmp );
        }
        catch( err ) {
          this.app.log( "error", "Failure during insert operation, aborting" );
          throw err;
        }
      }
    }
    catch( err ) {
      this.app.log( "error", "Failure during persist, check incomplete changes" );
      if ( this._inconsistent ) {
        // Operation left set to prevent further actions
        this.app.log( "error", "Model inconsistent with remote store, requires reload" );
        throw new Error( "Model inconsistent with remote store, requires reload" );
      }

      // Model remains consistent although change(s) failed. Completed records only will be returned.
    }

    this._operation = undefined;

    done = removed.concat( updated, added );
    this._modified = this._modified.filter( m => { return ! done.includes( m ) } );

    return { 'delete': removed, 'update': updated, 'insert': added };
  }


  /**
   * Get records waiting for a particular action
   * @param {String} action             - The chosen action
   * @param {String[]} [ids]            - An optional list of IDs to choose from
   * @returns {Object[]}                - A list of records found
   */
  _getByOperation( action, ids=[] ) {
    let rec, recs = [];

    if ( this.config.modelType.includes( 'record' ) ) {
      if ( ids.length && this._record && ids.includes( this._record[ this.idField ] ) && this._record[ '_operation' ] == action  )
        recs.push( this.getRecord( this._record[ this.idField ] ) );
      else if ( ! ids.length && this._record && this._record[ '_operation' ] == action )
        recs.push( this.getRecord( this._record[ this.idField ] ) );
    }
    else {
      this._modified.forEach( id => {
        if ( ids.length && ! ids.includes( id ) )
          return;
        rec = this._getRecord( id );
        if ( rec && rec[ '_operation' ] == action )
          recs.push( this.getRecord( id ) );
      } );
    }

    return recs;
  }


  /**
   * Parse provided data into our store.
   * @private
   * @override
   * @param {Object[]} records          - Array of objects representing records
   * @param {String} action             - The action behind this request, of load, insert, delete, update
   * @returns {String|String[]}         - The IDs impacted by this change
   * @throws                            - If any record is unable to be parsed
   */
  parse( records, action ) {
    if ( this.config.modelType.includes( 'collection' ) )
      return this._parseCollection( records, action );
    else
      return this._parseRecord( records );
  }


  /**
   * Parse one record for this record
   * @private
   * @param {Object} record             - Record to parse. First only if array provided.
   * @param {String} action             - The action behind this request, of load, insert, delete, update
   * @returns {String[]}                - The IDs impacted by this change
   */
  _parseRecord( record, action ) {
    let toparse, ids = [];

    if ( Array.isArray( record ) )
      toparse = record[ 0 ];
    else
      toparse = record;

    // Check for required fields
    this.check( toparse );

    // For removal, delete the record, otherwise try to ingest the record
    if ( action == "delete" )
      super.remove();
    else if ( this._mergeRecord( toparse, action ) )
      ids.push( toparse[ this.idField ] );

    return ids;
  }


  /**
   * Parse records for a collection
   * @protected
   * @param {Object[]} records          - Array of objects representing records
   * @param {String} action             - The action behind this request, of load, insert, delete, update
   * @returns {String|String[]}         - The IDs impacted by this change
   */
  _parseCollection( records, action ) {
    let ids = [];

    // Coerce records to array
    if ( ! Array.isArray( records ) )
      records = [ records ];

    // Check for required fields
    this.check( records );

    // For removal, delete the records. Otherwise ingest
    if ( action == "delete" )
      ids = super.remove( records.map( r => r[ this.idField ] ) );
    else {
      records.forEach( record => {
        // Merge, insert or update. Attempt to merge in the record
        if ( this._mergeRecord( record, action ) )
          ids.push( record[ this.idField ] )
      } );
    }
    return ids;
  }


  /**
   * Merge a record into our store
   * @private
   * @param {Object} record               - The record to merge
   * @param {String} action               - The context of this merge, one of insert, update or load
   * @returns {Boolean}                   - If the record changed
   */
  _mergeRecord( record, action ) {
    const meta = {
      '_pending':     {},
      '_operation':   "",
      '_errored':     false,
      '_message':     undefined
    };
    let current, changed;

    // Get any current record
    current = this._getRecord( record[ this.idField ] )

    // New record?
    if ( current == undefined ) {
      this._setRecord( Object.assign( {}, record, meta ) );
      return true;
    }

    // An update from an update? Always take the change
    if ( action == "update" ) {
      this._setRecord( Object.assign( {}, record, meta ) );
      return true;
    }

    // Figure out if this changed
    changed = this._isChanged( current, record );

    // Without attempted changes? Absorb if changed
    if ( ! current[ '_operation' ] ) {
      if ( changed )
        this._setRecord( Object.assign( {}, record, meta, current[ '_message' ] ) );
      return changed;
    }

    // A update or load over a deleted record? Keep the delete but update data
    if ( current[ '_operation' ] == "delete" ) {
      if ( changed )
        this._setRecord( Object.assign( {}, current, this.pick( record ) ) );
      return changed;
    }

    // A load or add over an insert? This should never happen and is likely a key clash. Any bind to affected record will not be aware.
    if ( current[ '_operation' ] == "insert" && this.config.modelType.includes( 'collection' ) ) {
      this.app.log( "warning", "Conflict between new remote record and local addition, reassigning local record" );
      current[ this.idField ] = this.newId();
      this._ids.push( current[ this.idField ] );
      this._data.set( current[ this.idField ], current );
      this._setRecord( Object.assign( {}, record, meta ) )
      return changed;
    }

    // A load or add over a new single-record. Cannot predict the correct course of action.
    if ( current[ '_operation' ] == "insert" ) {
      this._inconsistent = true;
      this.app.log( "error", "Unable to resolve new record received from poll conflicting with local addition" );
      throw new Error( "Unable to resolve new record received from poll conflicting with local addition" );
    }

    // A load over an update... Set a warning
    if ( current[ '_operation' ] == "update" ) {
      if ( changed && action == "load" )
        this._setRecord( Object.assign( {}, current, record, { '_message': "Remote update occured, check changes" } ) );
      return changed
    }

    return false;
  }


  /**
   * Set a given record into ourself blindly
   * @private
   */
  _setRecord( record ) {
    if ( this.config.modelType.includes( 'collection' ) ) {
      if ( ! this.hasRecord( record[ this.idField ] ) )
        this._ids.push( record[ this.idField ] );
      this._data.set( record[ this.idField ], record );
    }
    else
      this._record = record;
  }


  /**
   * Get the appropriate URL for an action
   * @param {String} action               - The action the URL is required for, eg insert, delete or update
   * @returns {String}                    - The resulting URL
   * @throws                              - If unable to build a URL
   */
  getUrl( action ) {
    const context = this.getContext();
    let url, urlpath, built, urlstring, fields, tmp;

    // The action must be defined
    if ( action == undefined ) {
      this.app.log( "error", `Cannot get URL without action` );
      throw new Error( `Cannot get URL without action` );
    }

    if ( ! this.config.url.hasOwnProperty( action ) || ! this.config.url[ action ] ) {
      this.app.log( "error", "No URL defined for action", action );
      throw new Error( "No URL defined for action " + action );
    }

    try {
      urlstring = this.config[ 'url' ][ action ];
      url = new URL( urlstring, window.location.origin );
      urlpath = url.pathname;
    }
    catch( err ) {
      this.app.log( "error", "Unable to process action URL", err.message );
      throw new Error( "Failed to parse action URL: " + err.message );
    }

    // Interpolate any fields from provided context
    if ( urlpath.includes( ':' ) ) {
      fields = urlpath.match( /:(\w+?):/g ) || [];
      for ( let field of fields ) {
        tmp = field.replace( /\:/g, '' );
        if ( ! context.hasOwnProperty( tmp ) ) {
          this.app.log( "warn", "Cannot interpolate unknown field", tmp );
          throw new Error( "Cannot interpolate unknown field: " + tmp );
        }
        urlpath = urlpath.replace( field, encodeURIComponent( context[ tmp ] ) );
      }
    }

    // Re-construct URL and check
    try {
      built = new URL( urlpath, url.origin );
    }
    catch( err ) {
      this.app.log( "error", "Unable to process parsed action URL", err.message );
      throw new Error( "Failed to process parsed action URL: " + err.message );
    }

    if ( built.pathname.indexOf( ':' ) !== -1 ) {
      this.app.log( "error", "Unable to resolve URL interpolations from", built.href );
      throw new Error( "Unable to resolve URL interpolations from " + built.href );
    }

    return built.href;
  }


  /**
   * Perform the remote request
   * @async
   * @private
   * @param {String} urlString              - The URL to query
   * @param {String} method                 - The HTTP method to use
   * @param {Object} data                   - Request payload
   * @returns {Object} response             - The parsed JSON response
   * @returns {Boolean} response.success    - If the request was successful
   * @returns {String} response.message     - Error description if not successful
   * @returns {Number} response.status      - HTTP Status code if known, or -1 if connection failed
   */
  async _remoteRequest( urlString, method="GET", data={} ) {
    const opts = {};
    let url, json, response;

    method = method.toUpperCase();
    opts[ 'method' ] = method;
    if ( method == "POST" ) {
      opts[ 'headers' ] = { 'Content-type': 'application/json' };
      opts[ 'body' ] = JSON.stringify( data );
    }

    this.app.log( 'debug', "Making remote request", urlString, method, data );

    // Build full URL
    url = new URL( urlString, window.location.origin );

    // Add search query for GET
    if ( method == "GET" ) {
      if ( Object.entries( data ).length != 0 )
        url.search = new URLSearchParams( data ).toString();
    }

    // Try the query
    try {
      response = await fetch( url, opts );
      if ( ! response.ok ) {
        try {
          json = await response.json();
          return { 'success': false, 'message': json.message || "Unknown error", "status": response.status };
        }
        catch( err ) {
          return { 'success': false, 'message': response.statusText, "status": response.status };
        }
      }
      json = await response.json();
    }
    catch( err ) {
      this._inconsistent = true;
      this.app.log( "error", "Failed remote request:", err.message );
      return { "success": false, "message": err.message, "status": err.status || -1  };
    }

    return json;
  }


  /**
   * Check if a two records are different
   */
  _isChanged( rec1, rec2 ) {
    const verField = this.config.versionField;
    let same = false;

    // If versioned, compare version before comparing records
    if ( this.config.versioned
        && rec1.hasOwnProperty[ verField ]
        && rec2.hasOwnProperty[ verField ]
        && rec1[ verField ] == rec2[ verField ] )
      return false;

    same = equal( this.pick( rec1 ), this.pick( rec2 ) );
    return ! same;
  }




  /**
   * Generate a delta for a provided partial record against our copy
   * @param {Object} record         - The record to delta
   */
  _delta( record ) {
    let changes = {}, keys = [], gold;
    gold = this.getRecord( record[ this.idField ] );
    keys = Object.keys( gold );

    // Version field is not locally modifiable
    if ( this.config.versioned && keys.includes( this.config.versionField ) )
      keys.splice( keys.indexOf( this.versionField ), 1 );

    keys.forEach( key => {
      if ( record.hasOwnProperty( key ) && gold[ key ] !== record[ key ] )
        changes[ key ] = record[ key ];
    } );

    return changes;
  }


  /**
   * Generate a new temporary ID. Ideally not in the range of possible IDs for the store.
   * Override if not suitable.
   * @returns {Number}              - A new random negative ID
   */
  newId() {
    let id;
    id = Math.floor( ( Math.random() * 100000000 ) + 1 ) * -1;
    if ( this.hasRecord( id ) )
      return this.newId();
    return id;
  }



  /**
   * Get a list a required fields
   * @returns {String[]}            - A list of required fields
   */
  get requiredFields() {
    const required = super.requiredFields;
    if ( this.config.versioned )
      required.push( this.config.versionField );
    return required;
  }

}

export { RemoteModel };

