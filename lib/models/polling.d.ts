/**
 * The Polling remote model mixin. Adds functionality to poll a remote store for changes.
 *
 * There are two approaches for polling, either checking each record individually, or checking
 * as a batch. Batch checks best suited to large datasets that don't change often. Polling for
 * record models only polls with individual records, is not suitable for large datasets.
 *
 * NOTE: For batch polling, it is assumed that added records would be included by one or more
 * previous loads. If not, the added record will not be polled via the batch poll!
 *
 * @param {*} baseclass - The class to mix onto.
 * @return {mixin:PollingModel}
 */
export function PollingModel(baseclass: any): mixin;
