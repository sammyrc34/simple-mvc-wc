/**
 * Load information type
 */
export type LoadInfoType = {
    /**
     * - List of loaded record IDs
     */
    load?: Array<string | number>;
    /**
     * - List of updated record IDs
     */
    update?: Array<string | number>;
    /**
     * - List of deleted record IDs
     */
    delete?: Array<string | number>;
};
/**
 * Persist information type
 */
export type PersistInfoType = {
    /**
     * - List of inserted record IDs
     */
    insert?: Array<string | number>;
    /**
     * - List of updated record IDs
     */
    update?: Array<string | number>;
    /**
     * - List of deleted record IDs
     */
    delete?: Array<string | number>;
};
/**
* Load information type
* @typedef {Object} LoadInfoType
* @property {Array<String|Number>} [load]      - List of loaded record IDs
* @property {Array<String|Number>} [update]    - List of updated record IDs
* @property {Array<String|Number>} [delete]    - List of deleted record IDs
*/
/**
* Persist information type
* @typedef {Object} PersistInfoType
* @property {Array<String|Number>} [insert]    - List of inserted record IDs
* @property {Array<String|Number>} [update]    - List of updated record IDs
* @property {Array<String|Number>} [delete]    - List of deleted record IDs
*/
/**
 * The remote model mixin. Adds functionality for a fully cached remotely-sourced dataset.
 *
 * As this is a fully cached model, it is not suitable for very large datasets.
 *
 * This model keeps and tracks changes within the cached data until it is
 * to be persisted. In the case of multiple operations, will perform a delete,
 * then updates, then inserts. Any failure prevents further operations from
 * taking place.
 *
 * @param {*} baseclass - The class to mix onto.
 * @return {mixin:RemoteModel}
 */
export function RemoteModel(baseclass: any): mixin;
