

/**
 * The Polling remote model mixin. Adds functionality to poll a remote store for changes.
 *
 * There are two approaches for polling, either checking each record individually, or checking
 * as a batch. Batch checks best suited to large datasets that don't change often. Polling for
 * record models only polls with individual records, is not suitable for large datasets.
 *
 * NOTE: For batch polling, it is assumed that added records would be included by one or more
 * previous loads. If not, the added record will not be polled via the batch poll!
 *
 * @param {*} baseclass - The class to mix onto.
 * @return {mixin:PollingModel}
 */
const PollingModel = baseclass =>

/**
 * Mixin to enable a model to poll a remote store for changes
 * @mixin
 * @alias PollingModel
 */
class extends baseclass {

  /**
   * Constructor for the polling remote data collection model.
   * @see BaseModel#constructor
   * @see BasicCollection#constructor
   * @see BasicRecord#constructor
   * @param {Object}  app                             - The main application object.
   * @param {...Object}  confs                        - Configuration options
   * @param {Object}  confs.url                       - Object map of URLs for actions
   * @param {String}  confs.url.poll                  - The URL to use for polling
   * @param {Number}  [confs.pollTime=10]             - Time (seconds) between poll requests
   * @param {String}  [confs.pollType="auto"]         - Polling type to use for collections, 'auto', 'batch' or 'each'. Records only use 'each'
   * @param {Boolean} [confs.pollUnbound=false]       - Poll when the model is not bound
   * @param {Boolean} [confs.pollEmpty=false]         - Poll when the model is empty and loaded
   */
  constructor( app, ...configs ) {

    const defaults = {
      modelType           : [ 'polling' ],
      url                 : {},
      pollTime            : 10,
      pollType            : 'auto',
      pollUnbound         : false,
      pollEmpty           : false
    };

    super( app, defaults, ...configs );
  }


  /**
   * Setup polling
   */
  setup() {

    // State properties
    this._pollTimer       = undefined;
    this._lastStamp       = undefined;
    this._pollFailed      = undefined;

    super.setup();
  }


  /**
   * Lazy setup a model for polling
   * @override
   * @async
   * @see {@link RemoteModel#load} for arguments and return values.
   */
  async load( ...args ) {
    let ret;

    await this.stopPolling();
    ret = await super.load( ...args );
    this.startPolling();

    return ret;
  }


  /**
   * Proxy the remote request to catch any stamp updates
   * @override
   * @see {@link RemoteModel#remoteRequest} for arguments and return values.
   */
  async _remoteRequest( ...args ) {
    let response;

    response = await super._remoteRequest( ...args );

    // Get any included stamp from any requests, setting an initial value
    if ( response && response[ 'success' ] && response.hasOwnProperty( 'stamp' ) ) {
      this._lastStamp = response[ 'stamp' ];
    }

    return response;
  }


  /**
   * Proxy the clear
   * @async
   * @override
   */
  async clear() {
    await super.clear();
    await this.stopPolling();
    this._lastStamp = undefined;
  }


  /**
   * Poll for changes
   * @async
   */
  async poll() {
    const pollunbound = this.config.pollUnbound;
    const pollempty = this.config.pollEmpty;
    let type = this.config.pollType;

    if ( ! this._loaded ) {
      this.app.log( "debug", `Thwarting polling for non-loaded model ${this.constructor.name}` );
      return this._continuePolling();
    }

    if ( ! pollempty && this.length == 0 ) {
      this.app.log( "debug", `Thwarting polling for empty model ${this.constructor.name}` );
      return this._continuePolling();
    }

    if ( ! pollunbound && ! this.isBound() ) {
      this.app.log( "debug", `Thwarting polling for unbound model ${this.constructor.name}` );
      return this._continuePolling();
    }

    if ( this._operation !== undefined || this._inconsistent ) {
      this.app.log( "debug", "Thwarting polling for busy or inconsistent model" );
      return this._continuePolling();
    }

    this.app.log( "debug", "Polling " + this.constructor.name + " for changes" );

    if ( type == "auto" ) {
      if ( this.config.modelType.includes( 'collection' ) )
        type = "batch";
      else if ( this.config.modelType.includes( 'record' ) )
        type = "each";
    }

    try {
      if ( type == "each" )
        await this.pollEach();
      else if ( type == "batch" )
        await this.pollBatch();
    }
    catch( e ) {
      this.app.log( "error", "Poll failure", e.message );
    }

    // Poll again
    this._continuePolling();
  }


  /**
   * Poll for a batch change for larger collections
   * @async
   */
  async pollBatch() {
    let url, response, laststamp, query;

    // Start with base query
    query = this.app.util.clone( this._query );
    query[ 'operation' ] = 'poll';

    // Polling URL
    url = this.getUrl( 'poll' );
    if ( url == undefined ) {
      this.app.log( "error", "Unable to poll without URL" );
      throw new Error( "Unable to poll without URL" );
    }

    laststamp = this._lastStamp;

    this._operation = "poll";

    // Poll for some comparable value
    response = await this._remoteRequest( url, 'GET', query );
    if ( ! response || response.success !== true || ! response.hasOwnProperty( 'stamp' ) ) {
      if ( this._pollFailed ) {
        // Recurrent failures are silent
        this._operation = undefined;
        return;
      }
      else {
        this.app.log( "error", "Poll failure", response );
        this._pollFailed = Date.now();
        this._operation = undefined;
        throw new Error( "Poll failure: " + response.message );
      }
    }

    if ( this._pollFailed ) {
      this.app.log( "debug", `Model ${this.constructor.name} polling resumed since ${new Date( this._pollFailed )}` );
      this._pollFailed = undefined;
    }

    // Load if needed
    if ( laststamp !== response[ 'stamp' ] ) {
      this.app.log( "debug", "Changes found, loading model", laststamp, response[ 'stamp' ] );
      this._lastStamp = response[ 'stamp' ]

      try {
        await this._load();
        this._operation = undefined;
      }
      catch( e ) {
        this._operation = undefined;
        this.app.log( "error", "Failed in loading changes" );
        throw e;
      }
    }
    else {
      this._operation = undefined;
      this.app.log( "debug", "No changes from poll" );
    }

  }


  /**
   * Poll for changes to each record individually (send each record, expect a version back for each)
   * @async
   * @throws - In the event of any failure in polling
   */
  async pollEach() {
    const idField      = this.config.idField;
    const versionField = this.config.versionField;
    let url, response, retRecords, records, query;

    if ( ! this.config.versioned )
      throw new Error( "Unable to poll each record without versioning" );

    // Get the load context and query
    query   = this.app.util.clone( this._query );
    query[ 'operation' ] = 'poll';

    // Polling URL
    url = this.getUrl( 'poll' );
    if ( url == undefined ) {
      this.app.log( "error", "Unable to poll without URL" );
      throw new Error( "Unable to poll without URL" );
    }

    this._operation = "poll";

    // Get the record(s) and pick fields as needed
    if ( this.config.modelType.includes( 'collection' ) ) {
      records = this._ids.map( id => this._data.get( id ) );
      records = records.filter( r => r[ '_operation' ] !== "insert" ); // Exclude new records
      records = records.map( r => { return this.pick( r, [ idField, versionField ] ) } );
      }
    else {
      records = this._record;
      records = this.pick( records, [ idField, versionField ] );
    }

    // Support polling with collections or records
    if ( this.config.modelType.includes( 'collection' ) )
      query[ "records" ] = records;
    else
      query[ "record" ] = records;

    // Poll for some comparable value
    response = await this._remoteRequest( url, 'POST', query );
    if ( ! response || response.success != true ) {
      this._operation = undefined;
      this.app.log( "error", "Poll failure", response );
      throw new Error( "Poll failure: " + response.message );
    }

    // Support collection and record models
    if ( this.config.modelType.includes( 'collection' ) )
      retRecords = response[ 'records' ];
    else
      retRecords = response[ 'record' ];

    this._operation = undefined;

    return this.parse( retRecords, 'load' );
  }


  /**
   * Clear any poll timer
   * @public
   * @async
   */
  async stopPolling() {
    await this.waitForOperation();

    if ( this._pollTimer !== undefined )
      clearTimeout( this._pollTimer );
    this._pollTimer = undefined;
  }


  /**
   * Start the poll timer
   * @public
   */
  startPolling() {
    if ( this._pollTimer == undefined )
      this._pollTimer = setTimeout( () => { this.poll() }, this.config.pollTime * 1000 );
  }


  /**
   * Continue polling, resetting any timer
   * @private
   */
  _continuePolling() {
    if ( this._pollTimer !== undefined )
      clearTimeout( this._pollTimer );
    this._pollTimer = setTimeout( () => { this.poll() }, this.config.pollTime * 1000 );
  }



}


export { PollingModel };

