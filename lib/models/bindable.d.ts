/**
 * The Bindable mixin. Adds functionality to bind the model to view components via
 * direct callbacks or anything functions via events.
 *
 * Sends direct status and data change callbacks to bound components by way of two
 * methods, _statusChanged for status updates, and _dataChanged for data updates.
 *
 * Status updates are:
 *  - cleared   = The model has been cleared to a default state
 *  - pending   = A remote request is going to be made, completed by complete or failure
 *  - online    = The model is connected and subscribed to a websocket
 *  - enable    = The model is enabled after being disabled
 *  - disable   = The model is disabled after being enabled
 *  - failure   = The model failed to perform the operation
 *  - complete  = The model completed the operation
 *  - change    = The model has experienced a change, status available in detail.
 *
 * Additional status updates for related components only are:
 *  - bind      = The component is bound to the model
 *  - unbind    = The component's bind is removed
 *
 * Data updates are: load, add, update and remove, and relevant record ID(s) provided
 *  - load      = The model has (re)loaded, records are available,
 *  - add       = One or more records have been added
 *  - update    = One or more records have been updated
 *  - remove    = One or more records have been removed
 *  - change    = One or more records changed, status, ids and local flag available in detail
 *
 * Also provides the documented event interface to attach to.
 *
 * @param {*} baseclass - The class to mix onto.
 * @return {mixin:Bindable}
 */
export function BindableModel(baseclass: any): mixin;
