
import { BaseModel } from '../model.js';


/**
 * Field criteria type where keys are operations, and values to be filtered on.
 * String type properties coerce values to strings before comparison.
 * If one FieldCriteriaType contains multiple rules, all must match.
 * @typedef {Object} FieldCriteriaType
 * @property {Number} [gt]          - Field value must be greater than
 * @property {Number} [lt]          - Field value must be less than
 * @property {Number} [ge]          - Field value must be greater than or equal to
 * @property {Number} [le]          - Field value must be less than or equal to
 * @property {String} [eq]          - Field value must be equal to value
 * @property {String} [ct]          - Field value must contain value
 * @property {String} [sw]          - Field value must start with value
 * @property {String} [ew]          - Field value must end with value
 * @property {String} [has]         - Field value must be a list and contain value
 * @property {String[]} [in]        - Field value must be one of values
 * @property {Boolean} [:case=true] - If strings are case sensitive
 */


/**
 * Field Criteria collection type, containing one or more field scoped filter criteria
 * @typedef {Object} FieldCriteriaCollection
 * @property {Array<FieldCriteriaType>} :rules  - List of field criteria types
 * @property {String} [:mode="AND"]             - Operator for multiple criteria, AND or OR.
 */


/**
 * Filter definitions where keys denote field name, and valus are a criteria, criteria list or function.
 * Filter funtions are called with a list of IDs to filter, and the model as the second argument.
 * If one FilterType contains multiple fields, all must match
 * @typedef {Object<string, Function|FieldCriteriaType|FieldCriteriaCollection>} FilterType
 */


/**
 * A simple array of filter collections, filter types or filter functions.
 * @typedef {Array<FiltersCollection|FilterType|Function>} FilterList
 */


/**
 * Filters collection type, containing one list of filter types, collection or functions
 * @typedef {Object} FiltersCollection
 * @property {FilterList} :rules      - List of rules
 * @property {String} [:mode="AND"]   - Operator for multiple filters, AND or OR.
 * Functions are called with a list of IDs currently passing, exptected to return a list of
 * filtered IDs
 *
 * @example
 * Eg.  {
 *        ':rules': [
 *          { field_a: { ct: "abc" } }
 *        ]
 *      }
 *        - single match, field_a must contain "abc"
 *
 *      {
 *        ':rules': [
 *          { field_a: { ct: "abc" } },
 *          { field_b: { ct: "def" } }
 *        ]
 *      }
 *        - field_a must contain "abc" AND field_b contains "def"
 *
 *      {
 *        ':rules': [
 *          { field_a: {
 *            ':mode': "OR",
 *            ':rules': [
 *              { ct: "abc" },
 *              { ct: "def" }
 *            ]
 *          } }
 *        ]
 *      }
 *        - field_a must contain "abc" OR "def"
 *
 *      {
 *        ':rules': [
 *          { field_a: {
 *            ':rules': [
 *              { sw: "abc" },
 *              { ew: "def" }
 *           ]
 *        } }
 *        ]
 *      }
 *        - field_a must start with "abc" AND end with "def"
 *
 *      {
 *        ':mode': "OR",
 *        ':rules': [
 *          { field_a: { ct: "abc" } },
 *          { field_b: { ct: "def" } }
 *        ]
 *      }
 *        - field_a contains "abc" OR field_b contains "def"
 *
 *      {
 *        ':mode': "OR",
 *        ':rules': [
 *          Function,
 *          { field_a: Function, field_b: { ct: "def" } }
 *        ]
 *      }
 *        - field_a via function call AND field_b must contain "def"
 *
 *      {
 *        ':rules': [
 *          { field_a: { eq: "one" },
 *          {
 *            ':mode': "OR",
 *            ':rules': [
 *              { field_b: { ct: 'two' } },
 *              { field_c: { ct: 'three' }
 *            ]
 *          }
 *        ]
 *      }
 *        - field_a is "one" and either field_b contains "two" or field_c contains "three"
 */


/**
 * Sort criteria type
 * @typedef {Object} SortCriteria     - Sort criteria containing either field and either func, or direction and type
 * @property {String} field           - Field to sort on
 * @property {Function} [func]        - Custom field sort function, called with field A value, field B value
 * @property {String} [direction=asc] - Sort direction, asc for ascending, desc for descending
 * @property {String} [type]          - Field data type, number or string
 */


/**
 * Sorts type, a mixed array of functions or sort criteria.
 * Functions are called with the ID for record A and B, expected to return a Number per Array.sort's compareFn
 * @typedef {Array<SortCriteria|Function>} SortsType
 */


/**
 * Fetch result type containing records and metadata
 * @typedef {Array} ResultType        - Fetch result type, records and metadata
 * @property {Number} totalCount      - Total number of records in the data store
 * @property {Number} fetchCount      - Total number of records retrieved by the fetch
 * @property {Number} filterCount     - Total number of filtered records
 */




/**
 * A basic client-side and in-memory multi-record collection with a simple interface
 * @class BasicCollection
 * @extends BaseModel
 * @classdesc The BasicCollection class represents a simple in-memory collection of keyed data items.
 */
class BasicCollection extends BaseModel {


  /**
   * Constructor for a basic data collection model
   * @param {Object} app                          - The main application object.
   * @param {...Object} configs                   - Configuration options
   * @param {Object}    [configs.conf]              - Single configuration object
   * @param {String[]}  [configs.indexFields]         - List of fields to index for quicker access
   * @see BaseModel#constructor
   */
  constructor( app, ...configs ) {
    const defaults = {
      indexFields: [],
      modelType: [ 'collection' ]
    };

    super( app, defaults, ...configs );
  }


  /**
   * Setup the collection
   */
  setup() {

    super.setup();

    // To store collection data, keyyed by the ID, and the IDs to maintain order
    this._data = new Map();
    this._ids = [];
    this._idxs = {};
  }



  /**
   * Clear all in-memory data.
   */
  clear() {
    this._data = new Map();
    this._ids = [];
    this._idxs = {};
  }


  /**
   * Get the total number of records.
   * @returns {Number} length - The number of records
   */
  get length() {
    return this._ids.length;
  }


  /**
   * Get a single cloned record by ID
   * @async                       - Asynchronous to maintain interface with remote collection
   * @param {String|Number} id    - The ID of the record.
   * @returns {Object|undefined}  - The record or undefined if none found
   */
  async fetchOne( id ) {
    if ( this._ids.includes( id ) )
      return this.getRecord( id );
    else
      return undefined;
  }


  /**
   * Get a range of records with optional sorting and filtering
   * @async                                           - Asynchronous to maintain interface with remote collections
   * @param {Number} start=0                          - The record number to start from. Defaults to 0;
   * @param {Number} count=N                          - The number of records to fetch. Defaults to full length.
   * @param {SortsType} [sorts]                       - Any sorts to apply to the fetch
   * @param {FiltersCollection} [filters]             - A collection of filters to apply
   * @returns {Promise<ResultType>}                   - Return records and metadata
   */
  async fetch( start=0, count=undefined, sorts=[], filters={} ) {
    const retdata = new Array();
    let ids = this._ids, filteredIds = [];

    if ( count == undefined || count == 0 )
      count = this.length;

    // Start with all IDs passing
    if ( filters.hasOwnProperty( ':rules' ) && filters[ ':rules' ].length )
      filteredIds = this._filter( ids, filters );
    else
      filteredIds = ids;

    // Not enough rows?
    if ( start > filteredIds.length ) {
      retdata.totalCount = this._ids.length;
      retdata.fetchCount = 0;
      retdata.filterCount = filteredIds.length;
      return retdata;
    }

    // Sort next
    if ( sorts && sorts.length )
      filteredIds = this._sort( filteredIds, sorts );

    // Gather data
    if ( filteredIds.length && start <= filteredIds.length ) {
      for ( var i=start; i < start + count; i++ ) {
        if ( i >= filteredIds.length )
          break;
        retdata.push( this.getRecord( filteredIds[i] ) );
      }
    }

    // Add result metadata
    if ( filters[ ':rules' ] && filters[ ':rules' ].length )
      retdata.filterCount = filteredIds.length;
    else
      retdata.filterCount = this._ids.length;
    retdata.totalCount = this._ids.length;
    retdata.fetchCount = retdata.length;

    return retdata;
  }


  /**
   * Get a modification-safe record by ID
   * @public
   * @param {String|Number} id    - The record ID
   * @returns {Object|undefined}  - The record object, or undefined if it doesn't exist
   */
  getRecord( id ) {
    if ( ! this.hasRecord( id ) )
      return undefined;
    return this.pick( this._getRecord( id ) );
  }


  /**
   * Get all modification-safe records. Method fetch is preferred as this method supports neither sorting or filtering.
   * @public
   * @returns {Object[]|undefined}  - Modification safe record objects
   */
  getRecords() {
    const out = [];
    for ( let id of this._ids )
      out.push( this.getRecord( id ) );
    return out;
  }


  /**
   * Get the raw and modification UNSAFE record for an ID
   * @private
   * @param {String|Number} id  - The record ID
   * @returns {Object}          - The record object
   */
  _getRecord( id ) {
    return this._data.get( id );
  }


  /**
   * Add data to the in-memory data model
   * @async
   * @param {Object[]} records  - An array of records in which to add.
   * @returns {String[]}        - The IDs impacted by this change
   */
  add( records ) {
    const idField = this.idField;
    const clean = [];
    let cleanrec;

    // Coerce to array
    if ( ! Array.isArray( records ) )
      records = [ records ];

    // Apply defaults, then pick only known fields
    for( let record of records ) {
      cleanrec = this.pick( this.app.util.deepMerge(
          {},
          this.config.defaultValues,
          record,
          { '_errored': false, '_message': undefined }
      ) );
      this.resolve( cleanrec );
      if ( cleanrec[ idField ] === undefined || cleanrec[ idField ] === "" ) {
        this.app.log( "error", "Cannot add record to collection without ID" );
        throw new Error( "Cannot add record to collection without ID" );
      }
      if ( this._ids.includes( cleanrec[ idField ] ) ) {
        this.app.log( "error", "Cannot add record to collection with existing ID", cleanrec[ idField ] );
        throw new Error( "Cannot add record to collection with existing ID" );
      }
      clean.push( cleanrec );
    }

    return this.parse( clean );
  }


  /**
   * Remove one or more data records by their corresponding ID.
   * @param {String[]|Number[]} ids     - The IDs to remove
   * @returns {String[]}                - The IDs impacted by this change
   */
  remove( ids ) {
    const removed = [];
    let idIndex;

    /* Check all records exist */
    if ( ! ids.every( i => {
      return this._ids.includes( i );
    } ) ) {
      this.app.log( 'error', "Cannot remove record that does not exist" );
      throw new Error( "Cannot remove record that does not exist" );
    }

    /* remove */
    for ( let id of ids ) {
      idIndex = this._ids.indexOf( id );
      this._ids.splice( idIndex, 1 );
      removed.push( this._data.get( id ) );
      this._data.delete( id );
    }

    // Remove from indexes
    this._removeIndexes( removed );

    return ids;
  }


  /**
   * Update one or more record to the data store
   * @param {Object|Object[]} records   - The updated record(s) to store.
   * @returns {String[]}                - The IDs impacted by this change
   * @throws                            - If unable to update records
   */
  update( records ) {
    var idField = this.idField;

    // Coerce records to an array
    if ( ! Array.isArray( records ) )
      records = [ records ];

    /* Check all records exist */
    if ( ! records.every( r => {
      return this._ids.includes( r[ idField ] );
    } ) ) {
      this.app.log( 'error', "Unable to update record that does not exist" );
      throw new Error( "Unable to update record that does not exist" );
    }

    return this.parse( records );
  }


  /**
   * Check that all provided records have required fields
   * @param {Object[]} records        - The records to check
   * @throws                          - If any record doesn'
   */
  check( records ) {
    var idField = this.idField;
    var fields = this.requiredFields;

    /* Check each record has the id */
    if ( ! records.every( record => {
      return record.hasOwnProperty( idField ) && record[ idField ] !== undefined;
    } ) ) {
      this.app.log( 'error', "Record failed check, missing ID" );
      throw new Error( "Record failed check, missing ID" );
    }

    /* Check each record for required fields */
    if ( fields.length && ! records.every( record => {
      return fields.every( field => {
        var found = record.hasOwnProperty( field ) && record[ field ] !== undefined;
        if ( ! found )
          this.app.log( "debug", "Required field", field, "not found in record" );
        return found;
      } );
    } ) ) {
      this.app.log( "error", "Record failed check, missing one or more required fields" );
      throw new Error( "Record failed check, missing one or more required fields" )
    }
  }


  /**
   * Parse provided data into our store.
   * Atomic method, failure safe for the model.
   * @param {Object[]} records  - Array of objects representing records
   * @returns {String[]}        - All IDs parsed into the store
   * @throws                    - If any record is unable to be parsed
   */
  parse( records ) {
    const parsedIds = [];
    const idField = this.idField;
    const existing = [];
    let id;

    // Check for required fields
    this.check( records );

    // Add or update each record
    for ( let record of records ) {
      id = record[ idField ];

      // New record or existing?
      if ( ! this._ids.includes( id ) )
        this._ids.push( id );
      else
        existing.push( this._data.get( id ) );

      // Store the record
      this._data.set( id, this.pick( record ) );

      parsedIds.push( id );
    };

    // Update indexes new records
    this._removeIndexes( existing );
    this._indexRecords( parsedIds );

    return parsedIds;
  }


  /**
   * Check if a given record exists
   * @param {String} id           - The ID of the record
   * @returns {Boolean}           - Indicating if the record exists
   */
  hasRecord( id ) {
    return this._ids.includes( id );
  }



  /**
   * Helper method to search quickly for the first record with a field value. Useful for unique value fields.
   * @public
   * @param {String} field        - The field to search on
   * @param {*} value             - The value to search for
   * @returns {Object}            - The found record or undefined if not found
   */
  searchOne( field, value ) {
    const indexFields = this.config.indexFields || [];
    if ( field === this.idField )
      return this.getRecord( value );
    else if ( indexFields.includes( field ) ) {
      if ( this._idxs[ field ] ) {
        if ( this._idxs[ field ].has( value ) )
          return this.getRecord( this._idxs[ field ].get( value )[0] );
      }
    }
    else {
      this._ids.forEach( id => {
        if ( this._data.get( id )[ field ] == value )
          return this.getRecord( id );
      } );
    }
  }


  /**
   * Get a list of unique field values for field filtering
   * @returns {*[]}       - The unique values the field has
   */
  uniqueValues( field ) {
    const idxFields = this.config.indexFields || [];
    const idxs = this._idxs;
    const vals = new Set();

    if ( idxFields.includes( field ) && idxs[ field ] )
      return Array.from( idxs[ field ].keys() );
    else {
      for( let id of this._ids )
        vals.add( this._data.get( id )[ field ] );
      return Array.from( vals );
    }
  }


  /**
   * Has this model got any errored records?
   * @type {Boolean}                      - If failed
   */
  get failed() {
    return this._ids.some( id => this._data.get( id )[ '_errored' ] );
  }



  /**
   * Set one records error and error state
   * @param {String|Number} id            - The record ID to modify
   * @param {String} error                - The error message
   */
  setRecordError( id, error ) {
    const record = this._getRecord( id );
    if ( ! record )
      throw new Error( "No such record exists" );
    record[ '_message' ] = error;
    record[ '_errored' ] = true;
  }


  /**
   * Clear a record state
   * @param {String|Number} id            - The record ID to modify
   */
  clearRecordError( id ) {
    const record = this._getRecord( id );
    if ( ! record )
      throw new Error( "No such record exists" );
    record[ '_message' ] = undefined;
    record[ '_errored' ] = false;
  }


  /**
   * Get any message attached to a record
   * @returns {String|undefined}          - Message if defined, otherwise undefined
   */
  getMessage( id ) {
    let record = this._getRecord( id );
    if ( record )
      return record[ '_message' ];
    return undefined;
  }



  /**
   * Get the metadata for a record
   * @param {String|Number} id            - The record ID to fetch metadata for
   * @returns {Object} meta               - The resulting record metadata if record is found, otherwise undefined
   * @returns {Boolean} meta.errored      - If this record is in an errored state
   * @returns {String} meta.message       - Any informational or error message attached to the record
   */
  getRecordInfo( id ) {
    let record, info = {
      'errored': false,
      'message': ""
    };

    record = this._data.get( id );
    if ( record ) {
      info[ 'errored' ] = record[ '_errored' ];
      info[ 'message' ] = record[ '_message' ];
    }
    return info;
  }


  /**
   * Reset metadata for one or more records
   * @param {String|Number|String[]|Number[]} ids - The IDs for the records to reset
   */
  reset( ids ) {
    let rec;

    // Cooerce to array
    if ( ! Array.isArray( ids ) )
      ids = [ ids ];

    ids.forEach( id => {
      rec = this._getRecord( id );
      rec[ '_errored' ] = false;
      rec[ '_message' ] = undefined;
    } );
  }




  /**
   * Add to or update existing index for specified IDs
   * @protected
   * @param {String[]} ids          - IDs to re-index
   */
  _indexRecords( ids ) {
    const idxFields = this.config.indexFields || [];
    const idxs = this._idxs;
    let val;
    if ( ids == undefined )
      ids = this._ids;

    // Remove any old indexes not needed
    for ( let idxField in idxs ) {
      if ( ! idxFields.includes( idxField ) )
        delete idxs[ idxField ];
    }

    // Build indexes
    for ( let idxField of idxFields ) {
      if ( ! idxs.hasOwnProperty( idxField ) )
        idxs[ idxField ] = new Map();
      for ( let recId of ids ) {
        val = this._data.get( recId )[ idxField ];
        if ( ! idxs[ idxField ].has( val ) )
          idxs[ idxField ].set( val, [] );
        idxs[ idxField ].get( val ).push( recId );
      }
    }
  }


  /**
   * Remove indexes for the provided record
   * @param {Object[]} records    - Records to remove indexes for
   */
  _removeIndexes( records ) {
    const idxFields = this.config.indexFields || [];
    const idxs = this._idxs;
    let pos, arr;

    if ( ! idxs )
      return;

    for ( let record of records ) {
      for ( let idxField of idxFields ) {
        if ( idxs && idxs[ idxField ]
          && record[ idxField ] !== undefined
          && idxs[ idxField ].get( [ record[ idxField ] ] ) )
        {
          arr = idxs[ idxField ].get( record[ idxField ] );
          pos = arr.indexOf( record[ idField ] );
          if ( pos !== -1 )
            arr.splice( pos, 1 );
        }
      }
    }
  }


  /**
   * Return a function to sort two records by a given number field and direction
   * @private
   * @param {String} direction        - The direction for the sort, either asc or desc
   * @returns {Function}
   */
  _sortNumber( direction ) {
    if ( direction == "asc" )
      return (a,b) => { return a - b };
    else
      return (a,b) => { return b - a };
  }

  /**
   * Return a function to sort two records by a given string field and direction
   * @private
   * @param {String} direction        - The direction for the sort, either asc or desc
   * @returns {Function}              - The sorting function
   */
  _sortAlpha( direction ) {
    if ( direction == "asc" ) {
      return (a,b) => {
        let aa = a, bb = b;
        if ( a === undefined || a === null || ! a.localeCompare )
          aa = "";
        if ( b === undefined || b === null || ! b.localeCompare )
          bb = "";
        return aa.localeCompare( bb );
      }
    }
    else {
      return (a,b) => {
        let aa = a, bb = b;
        if ( a === undefined || a === null || ! a.localeCompare )
          aa = "";
        if ( b === undefined || b === null || ! b.localeCompare )
          bb = "";
        return bb.localeCompare( aa );
      }
    }
  }


  /**
   * Filter a list of records by given filter
   * @param {Number[]|String[]} ids               - ID list to filter
   * @param {FiltersCollection} filters           - The filters to apply
   * @returns {Number[]|String[]}                 - Matching IDs
   */
  _filter( ids, filters ) {
    const indexFields = this.config.indexFields || [];
    let rules, subpassing, passing, mode = "AND";

    if ( ! filters.hasOwnProperty( ':rules' ) )
      return
    else
      rules = filters[ ':rules' ];

    // Setup starting point from mode
    if ( filters.hasOwnProperty( ':mode' ) )
      mode = filters[ ':mode' ];
    if ( mode == "AND" )
      passing = ids;
    else if ( mode == "OR" )
      passing = [];

    // Sort by indexed fields first to increase performance for large sets when performing ANDs
    if ( mode == "AND" && rules.length > 1 ) {
      rules = rules.sort( (a,b) => {
        let aval = 0, bval = 0;
        if ( typeof a == "object" && ! Array.isArray( a ) && Object.keys( a ).some( k => indexFields.includes( k ) ) )
          aval = 1;
        if ( typeof b == "object" && ! Array.isArray( b ) && Object.keys( b ).some( k => indexFields.includes( k ) ) )
          bval = 1;
        return bval - aval;
      } );
    }

    // Loop over filters, accumulate passing records
    for ( let filter of rules ) {

      // Nested filter
      if ( typeof filter == "object" && filter.hasOwnProperty( ':rules' ) ) {
        subpassing = this._filter( mode == "OR" ? ids : passing, filter );
        passing = this._accumulate( passing, subpassing, mode );
        continue;
      }

      // Function filter
      if ( typeof filter == "function" ) {
        subpassing = filter( mode == "OR" ? ids : passing, this );
        passing = this._accumulate( passing, subpassing, mode );
        continue;
      }

      // Record filter
      if ( typeof filter == "object" ) {
        subpassing = this._applyFilter( mode == "OR" ? ids : passing, filter );
        passing = this._accumulate( passing, subpassing, mode );
      }
    }

    return passing;
  }


  /**
   * Accumulate record IDs as needed
   * @param {Number[]|String[]} alist   - List to IDs to intersect
   * @param {Number[]|String[]} blist   - List to IDs to intersect
   * @param {String} mode               - Mode to accumulate records, AND or OR
   * @returns {Number[]|String[]}       - Accumulated record IDs
   */
  _accumulate( alist, blist, mode ) {
    let passing = [];

    if ( mode == "OR" ) {
      passing = alist.concat( blist );
    }
    else if ( mode == "AND" ) {
      for ( let val of blist ) {
        if ( alist.includes( val ) )
          passing.push( val );
      }
    }
    return passing;
  }



  /**
   * Apply a filter of multiple criteria to a record set
   * Eg:
   * {
   *   'field_a': { ':rules': [ { 'sw': 'foo', 'ew': 'bar' }, { 'ew': 'car' }, Function ], ':mode': "OR" },
   *   'field_b': { 'in': [ 123, 456 ] }
   * }
   * @param {Array<Number|String>} ids      - Records IDs to filter
   * @param {FilterType} filter             - Filter definition
   * @returns {Array<Number|String>}        - Matching record IDs
   */
  _applyFilter( ids, filter ) {
    const indexFields = this.config.indexFields || [];
    const filtered = [];
    let rules, rule, mode, indexed = [], tmpfilter, passing, matched, submatched, record;

    passing = ids;

    // Filter indexed fields first
    for ( let field in filter ) {
      if ( ! indexFields.includes( field ) )
        continue;

      // Less IDs than index values?
      if ( ids.length <= this._idxs[ field ].size )
        continue;

      indexed.push( field );

      tmpfilter = {};
      tmpfilter[ field ] = Object.assign( {}, filter[ field ] );

      rule = {};
      rule[ field ] = filter[ field ];
      matched = this._applyIndexFilter( rule );
      passing = this._accumulate( passing, matched, "AND" );
    }

    // If no non-indexed fields, return early
    if ( Object.keys( filter ).reduce( (v,field) => {
      if ( field.startsWith( ':' ) )
        return v;
      if ( ! indexed.includes( field ) )
        return v + 1;
      return v;
    }, 0 ) == 0 )
      return passing;

    // Filter each record
    for ( let id of passing ) {

      matched = true;

      record = this._getRecord( id );

      // All fields must match
      for ( let field in filter ) {

        // Skip indexed fields already processed
        if ( indexed.includes( field ) || field.startsWith( ':' ) )
          continue;

        // Coerce rules to list
        if ( filter[ field ].hasOwnProperty( ':rules' ) )
          rules = filter[ field ][ ':rules' ]
        else if ( ! Array.isArray( filter[ field ] ) )
          rules = [ filter[ field ] ];
        else
          rules = filter[ field ];

        // Matching mode
        if ( filter[ field ].hasOwnProperty( ':mode' ) )
          mode = filter[ field ][ ':mode' ];
        else
          mode = "AND";
        if ( mode == "AND" )
          submatched = true;
        else if ( mode == "OR" )
          submatched = false;

        // Filter until we know an outcome
        for ( let rule of rules ) {

          // Short circuit where possible
          if ( ! submatched && mode == "AND" )
            break;
          else if ( submatched && mode == "OR" )
            break;

          // Test the rule
          if ( typeof rule == "function" )
            submatched = rule( record[ field ] );
          else
            submatched = this._applyCriteria( record[ field ], rule );
        }

        // Short curcuit where possible
        if ( ! submatched )
          break;
      }

      if ( submatched )
        filtered.push( id );
    }

    return filtered;
  }


  /**
   * Apply a filter of multiple criteria to a single value (intended for indexed values)
   * Eg:
   * {
   *   'field_a': { rules: [ { 'sw': 'foo', 'ew': 'bar' }, { 'ew': 'car' }, Function ] },
   *   'field_b': { 'in': [ 123, 456 ] }
   * }
   * @param {FilterType} filter     - Filter definition
   * @returns {Boolean}             - If filters apply to value
   */
  _applyIndexFilter( filter ) {
    let rules, passing, matched;

    for ( let field in filter ) {

      if ( filter[ field ].hasOwnProperty( ':rules' ) )
        rules = filter[ field ][ ':rules' ];
      else if ( ! Array.isArray( filter[ field ] ) )
        rules = [ filter[ field ] ];
      else
        rules = filter[ field ];

      passing = [];

      // Test each rule and accumulate
      for ( let rule of rules ) {
        for ( let idxVal of this._idxs[ field ].keys() ) {

          if ( typeof rule == "function" ) {
            if ( rule( idxVal ) ) {
              matched = this._idxs[ field ].get( idxVal );
              passing = this._accumulate( passing, matched, 'OR' );
            }
            continue;
          }

          if ( this._applyCriteria( idxVal, rule ) ) {
            matched = this._idxs[ field ].get( idxVal );
            passing = this._accumulate( passing, matched, 'OR' );
          }
        }
      }
    }

    return passing;
  }



  /**
   * Check if a given criteria matches a given record
   * @param {Object} value            - Value to check if matching
   * @param {FieldCriteriaType} rule  - Rule to apply to the field
   * @returns {Boolean}               - If criteria matches supplied record
   */
  _applyCriteria( value, rule ) {
    const valueType = typeof value;
    const valueString = ( value !== null ) ? value.toString() : '';
    const ruleTypes = Object.keys( rule ).filter( r => ! r.startsWith( ':' ) );
    let matched, matchCase = true, checkValue;

    // Field doesn't exist? Can't match.
    if ( value === undefined )
      return false;

    // Case setting present?
    if ( rule.hasOwnProperty( ':case' ) )
      matchCase = rule[ ':case' ];

    // Check each criteria type, all must match
    for ( let ruleName of ruleTypes ) {

      matched = false;

      checkValue = rule[ ruleName ];

      switch( ruleName ) {

        case "gt": // Greater than
          if ( value > checkValue )
            matched = true;
          break;

        case "lt": // Less than
          if ( value < checkValue )
            matched = true;
          break;

        case "le": // Less than or equal to
          if ( value <= checkValue )
            matched = true;
          break;

        case "ge": // Greater than or equal to
          if ( value >= checkValue )
            matched = true;
          break;

        case "eq": // Equal to (type specific)
          if ( valueType == "number" && value === checkValue )
            matched = true;
          else if ( valueType == "string" && valueString.match( new RegExp( "^" + checkValue + "$", matchCase ? 'm' : 'im' ) ) )
            matched = true;
          else if ( valueType == "object" && value === checkValue )
            matched = true;
          break;

        case "sw": // String starts with
          if ( valueString.match( new RegExp( "^" + checkValue, matchCase ? 'm' : 'mi' ) ) )
            matched = true;
          break;

        case "ew": // String ends with
          if ( valueString.match( new RegExp( checkValue + '$', matchCase ? 'm' : 'mi' ) ) )
            matched = true;
          break;

        case "ct": // String contains
          if ( valueString.match( new RegExp( checkValue, matchCase ? 'm' : 'mi' ) ) )
            matched = true;
          break;

        case "in": // One of values matches (type specific)
          for ( let check of checkValue ) {
            if ( valueType == "string" && ! matchCase && value.toLowerCase() === check.toString().toLowerCase )
              matched = true;
            if ( value === check )
              matched = true;
            if ( matched )
              break;
          }
          break;

        case "has": // List value contains
          if ( Array.isArray( value ) && ! matchCase
              && value.map( v => v.toLowerCase() ).includes( checkValue.toLowerCase() ) )
            matched = true;
          if ( ! matched && Array.isArray( value ) && value.includes( checkValue ) )
            matched = true;
          break;

      }

      // Short circuit
      if ( ! matched )
        break;

    }

    return matched;
  }


  /**
   * Sort a list of records by given criteria
   * @private
   * @param {String[]|Number[]} ids       - The list of record IDs to sort
   * @param {SortsType} what              - A list of sort criteria
   * @returns {String[]|Number[]}         - Sorted record IDs
   */
  _sort( ids, what ) {
    let sorts = [];
    let tosort = Array.from( ids );
    let sortField, sortDir, sortType, sortFunc;

    // Build sorts
    for ( let sort of what ) {

      // Generic sort function
      if ( typeof sort == "function" ) {
        sorts.push( { 'field': this.idField, 'func': sort } );
        continue;
      }

      // Build a sorter function as needed
      ( { 'field': sortField, 'direction': sortDir, 'type': sortType, 'func': sortFunc } = sort );
      if ( this.hasField( sortField ) === false )
        throw new Error( `Unknown sort field ${sortField}` );
      if ( ! sortDir )
        sortDir = "asc";
      if ( ! sortFunc ) {
        if ( sortType == "number" )
          sortFunc = this._sortNumber( sortDir );
        else if ( sortType == "string" )
          sortFunc = this._sortAlpha( sortDir );
        else
          throw new Error( `Unknown field sort type ${sortType}` );
      }

      sorts.push( { 'field': sortField, 'func': sortFunc } );
    }

    if ( ! sorts.length )
      return;

    // Apply sorts
    tosort.sort( ( aid, bid ) => {
      let res = 0, idx = 0, aval, bval;

      while ( res == 0 && idx < sorts.length ) {
        aval = this._data.get( aid )[ sorts[ idx ].field ];
        bval = this._data.get( bid )[ sorts[ idx ].field ];
        res = sorts[ idx ].func( aval, bval );
        idx++;
      }

      return res;
    } );

    return tosort;
  }



}


export { BasicCollection };

