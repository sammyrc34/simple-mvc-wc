/**
* The IntegratedModel is a more tightly integrated partial-cache client-server model.
*
* Interface compatible with RemoteModel although functionally different with loading and
* fetching.
*
* It is a flexible model that can be used for a variety of datasets, including very large
* or rapidly changing datasets. It uses a simple JSON based RPC API for CRUD operations,
* and optionally a websocket connection for change notifications or full record updates.
*
* It differs from RemoteModel in that it does not fully cache data. Rather, filtering,
* sorting and pagination are conducted server side in the first instance, allowing the model
* to only cache records currently utilised. To maintain consistency, websockets are used
* to listen for change notifications. The model can then respond appropriately to
* maintain the desired state.
*
* In the case of using websockets, the remote data source can notify of a change with type
* and an optional list of record IDs, or can provide full records to ingest. With options
* to re-fetch upon different change types, the model supports a range of use cases.
*
* NOTE 1: This model caches records per fetch, and so cannot be bound to multiple multi-bind
* components without complicating model function with pagination, filtering and more. Attempting
* to bind to multiple multi-bind components results in an error.
*
* NOTE 2: This model does not support in-model field indexes, and such indexes cannot be defined.
*
* NOTE 3: This model does not support Function based filters or sorts. This model only supports
* FiltersCollection, FilterType, CriteriaCollection, CriteriaType filter types only, and
* SortCriteria sort types only without Functions.
*/
export function IntegratedModel(baseclass: any): {
    new (app: SimpleApp, ...configs: any[]): {
        [x: string]: any;
        /**
         * Setup the model
         */
        setup(): void;
        _ws: any;
        _operation: string;
        _subChannel: any;
        _refetchTimer: NodeJS.Timeout;
        _fetched: boolean;
        _lastFetch: number;
        _lastFetchQuery: {
            operation: string;
            include: (string | number)[];
            filters: {};
            sorts: any;
            start: number;
            count: number;
        };
        _lastWSUrl: any;
        _lastTotal: any;
        _lastFiltered: any;
        _loaded: boolean;
        /**
         * Override
         * @override
         * @returns {*[]}       - The unique values the field has
         */
        uniqueValues(): any[];
        /**
         * Has this model fetched?
         * @public
         * @type {Boolean}                - If fetched
         */
        readonly fetched: boolean;
        /**
         * Prepare to subscribe to websockets on a data fetch
         * @async
         * @override
         * @param {Object} [context]      - Any context (bind variables) for the dataset's urls
         * @param {Object} [query]        - Any supplemental query specific for the subsequent queries
         * @returns {Promise}             - Promise which resolves or throws
         */
        load(context?: any, query?: any): Promise<any>;
        _query: any;
        _context: any;
        /**
         * Is this model loaded? (connected)
         * @public
         * @type {Boolean}                      - If this model is loaded
         */
        readonly loaded: boolean;
        /**
         * Attempt connection if utilising websockets
         * @async
         * @protected
         * @throws    - If unable to connect
         */
        _load(): Promise<void>;
        _loadedContext: any;
        _loadedQuery: any;
        /**
         * Override reload behaviour, not supported by integrated models
         * @async
         * @override
         */
        reload(): Promise<void>;
        /**
         * Set the model's context and or query
         * @public
         * @async
         * @override
         * @param {Object} [context]  - Context map with URL bind variables for further queries
         * @param {Object} [query]    - Query map for further queries
         * @returns {Promise}         - A promise which resolves if context is set, otherwise throws
         */
        setContextQuery(context?: any, query?: any): Promise<any>;
        _clearonload: boolean;
        /**
         * Fetch one or more records from the model.
         * @async
         * @param {Number} start=0                          - The record number to start from. Defaults to 0;
         * @param {Number} count=100                        - The number of records to fetch. Defaults to 100. Set to 0 to request & fetch all.
         * @param {SortsType} [sorts=[]]                    - Any sorts to apply to the fetch
         * @param {FiltersCollection} [filters={}]          - A collection of filters to apply
         * @param {Array<String|Number>} [keep=[]]          - Additional records to include in fetch
         * @returns {Promise<ResultType>}                   - Return records and metadata
         */
        fetch(start?: number, count?: number, sorts?: SortsType, filters?: FiltersCollection, keep?: Array<string | number>): Promise<ResultType>;
        /**
         * Fetch one or more records from the remote store
         * @async
         * @protected
         * @returns {Promise<ResultType>}                   - Return records and metadata
         */
        _fetchRemote(): Promise<ResultType>;
        _inconsistent: boolean;
        /**
         * Refetch records from remote store with last fetched query
         * @throws    - In event
         * @async
         * @protected
         */
        refetch(): Promise<ResultType>;
        /**
         * Proxy the persist method for remote stores to refetch on change
         * @override
         * @see {@link RemoteModel#persist}
         */
        persist(...args: any[]): Promise<any>;
        /**
         * Clear this models' records and state
         * @async
         * @override
         */
        clear(): Promise<void>;
        _data: Map<any, any>;
        _ids: any;
        _modified: any[];
        /**
         * Clear previously fetched records, keeping new records
         */
        _clearFetched(): void;
        /**
         * Handle a notification of a change to one or more records
         * @async
         * @protected
         * @param {Object} detail                         - Notification details
         * @param {String} detail.change                  - Type of change, insert, update or delete
         * @param {Array<String|Number>} [detail.ids=[]]  - IDs for the change if provided
         * @returns {Promise<Array>}                      - Record IDs that experienced a delete, or an empty list otherwise
         */
        handleNotify(detail: {
            change: string;
            ids?: Array<string | number>;
        }): Promise<any[]>;
        /**
         * Handle a new or updated record
         * @async
         * @protected
         * @param {Object} detail                 - Notification details
         * @param {Array<Object>} detail.records  - Records to ingest
         * @returns {Promise<Array>}              - Record IDs that have been ingested
         */
        handleRecords(detail: {
            records: Array<any>;
        }): Promise<any[]>;
        /**
         * Handle a separate request-unrelated websocket event
         * @async
         * @param {String} type         - Event type, 'disconnect', 'connect', 'unsubscribe', 'resubscribe', 'notify', 'records',
         * @param {Object} [detail={}]  - Event detail
         */
        handleEvent(type: string, detail?: any): Promise<void | any[]>;
        /**
         * Manage a websocket re-connection
         * @async
         * @protected
         */
        handleConnect(): Promise<void>;
        /**
         * Manage a websocket connection failure
         * @protected
         */
        handleDisconnect(): void;
        /**
         * Manage an non-requested unsubscribe event
         * @protected
         */
        handleUnsubscribe(): void;
        /**
         * Manage a re-subscribe event
         * @async
         * @protected
         */
        handleResubscribe(): Promise<void>;
        /**
         * Subscribe to websocket channel
         * @async
         * @throws  - If unable to subscribe
         */
        _subscribe(): Promise<void>;
        /**
         * Attempt to connect to endpoint as needed
         * @async
         * @private
         * @throws    - If unable to connect to websock endpoint
         */
        _tryConnect(): Promise<void>;
        /**
         * Attempt to subscribe for change notifications
         * @async
         */
        _trySubscribe(): Promise<void>;
        /**
         * Attempt to unsubscribe from change notifications
         * @async
         */
        _tryUnsubscribe(): Promise<void>;
        /**
         * Check filters are suitable for server side filtering.
         * @param {FiltersCollection|CriteriaCollection} filters  - Filters to check
         * @throws - In event of unsupported filter
         */
        _checkFilters(filters: FiltersCollection | CriteriaCollection): any;
        /**
         * Check sorts are suitable for server side sorting
         * @param {SortsType} sorts     - Sorts to check
         */
        _checkSorts(sorts: SortsType): SortsType;
    };
    [x: string]: any;
};
