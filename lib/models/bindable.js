

/**
 * Model is cleared to an empty state
 * @event BindableModel#status-cleared
 */
const onStatusCleared = 'status-cleared';

/**
 * Model is now pending a remote action
 * @event BindableModel#status-pending
 */
const onStatusPending = 'status-pending';

/**
 * Model is now connected and or subscribed to a remote socket or database
 * @event BindableModel#status-online
 */
const onStatusOnline = 'status-online';

/**
 * Model now enabled after being disabled
 * @event BindableModel#status-enabled
 */
const onStatusEnabled = 'status-enabled';

/**
 * Model now disabled after being enabled
 * @event BindableModel#status-disabled
 */
const onStatusDisabled = 'status-disabled';

/**
 * Model experienced failure to perform an action
 * @event BindableModel#status-failed
 */
const onStatusFailed = 'status-failed';

/**
 * Model pending action now complete
 * @event BindableModel#status-complete
 */
const onStatusComplete = 'status-complete';

/**
 * Generic event to indicate that a status change has occured
 * @event BindableModel#status-changed
 * @type {Object}
 * @property {String} status            - New model status
 */
const onStatusChanged = 'status-changed';

/**
 * Model has completed a load
 * @event BindableModel#data-loaded
 * @type {Object}
 * @property {String[]|Number} ids      - List of loaded IDs.
 */
const onDataLoaded = 'data-loaded';

/**
 * Model has completed an addition
 * @event BindableModel#data-added
 * @type {Object}
 * @property {String[]|Number[]} ids    - List of updated IDs.
 * @property {Boolean} local            - If the change is a local change only
 */
const onDataAdded = 'data-added';

/**
 * Model has completed an update
 * @event BindableModel#data-updated
 * @type {Object}
 * @property {String[]|Number[]} ids    - List of updated IDs.
 * @property {Boolean} local            - If the change is a local change only
 */
const onDataUpdated = 'data-updated';

/**
 * Model has completed a removal
 * @event BindableModel#data-removed
 * @type {Object}
 * @property {String[]|Number[]} ids    - List of removed IDs.
 * @property {Boolean} local            - If the change is a local change only
 */
const onDataRemoved = 'data-removed';

/**
 * Generic event to indicate the model has had some kind of change
 * @event BindableModel#data-changed
 * @type {Object}
 * @property {String[]|Number[]} ids    - List of changed IDs.
 * @property {Boolean} local            - If the change is a local change only
 */
const onDataChanged = 'data-changed';

/**
 * Model record ID has changed
 * @event BindableModel#data-rebind
 * @type {Object}
 * @property {Number|String} oldid      - The old ID which has changed
 * @property {Number|String} newid      - The new record ID
 */
const onDataRebind = 'data-rebind';



const eventsMap = {
  // Status events
  'status_cleared' : onStatusCleared,
  'status_pending' : onStatusPending,
  'status_online'  : onStatusOnline,
  'status_enable'  : onStatusEnabled,
  'status_disable' : onStatusDisabled,
  'status_failure' : onStatusFailed,
  'status_complete': onStatusComplete,
  'status_change'  : onStatusChanged,


  // Data events
  'data_load'    : onDataLoaded,
  'data_insert'  : onDataAdded,
  'data_update'  : onDataUpdated,
  'data_delete'  : onDataRemoved,
  'data_change'  : onDataChanged,
  'data_rebind'  : onDataRebind
};


/**
 * The Bindable mixin. Adds functionality to bind the model to view components via
 * direct callbacks or anything functions via events.
 *
 * Sends direct status and data change callbacks to bound components by way of two
 * methods, _statusChanged for status updates, and _dataChanged for data updates.
 *
 * Status updates are:
 *  - cleared   = The model has been cleared to a default state
 *  - pending   = A remote request is going to be made, completed by complete or failure
 *  - online    = The model is connected and subscribed to a websocket
 *  - enable    = The model is enabled after being disabled
 *  - disable   = The model is disabled after being enabled
 *  - failure   = The model failed to perform the operation
 *  - complete  = The model completed the operation
 *  - change    = The model has experienced a change, status available in detail.
 *
 * Additional status updates for related components only are:
 *  - bind      = The component is bound to the model
 *  - unbind    = The component's bind is removed
 *
 * Data updates are: load, add, update and remove, and relevant record ID(s) provided
 *  - load      = The model has (re)loaded, records are available,
 *  - add       = One or more records have been added
 *  - update    = One or more records have been updated
 *  - remove    = One or more records have been removed
 *  - change    = One or more records changed, status, ids and local flag available in detail
 *
 * Also provides the documented event interface to attach to.
 *
 * @param {*} baseclass - The class to mix onto.
 * @return {mixin:Bindable}
 */
const BindableModel = baseclass =>
  /**
   * Mixin to enable a model to bind to view components.
   * @mixin
   * @alias Bindable
   */
class extends baseclass {

  /**
   * Constructor for the bindable model
   * @param {Object}  app                             - The main application object.
   * @param {...Object}  configs                      - Configuration options
   */
  constructor( app, ...configs ) {
    super( app, ...configs );

    // State properties
    this._binds = {};
    this._isEnabled = true;
    this._delegate = document.createDocumentFragment();
    this._lastStatus = undefined;
  }


  setup() {

    // Remote models start off being disabled
    if ( this.config.modelType.includes( 'remote' ) )
      this._isEnabled = false;

    super.setup()
  }

  /**
   * Add an event listener
   * @param {...Object} args - Proxied arguments for the normal EventTarget.addEventListener function.
   */
  addEventListener( ...args ) {
    return this._delegate.addEventListener( ...args );
  }

  /**
   * Dispatch or fire event
   * @param {...Object} args - Proxied arguments for the normal EventTarget.dispatchEvent function.
   */
  dispatchEvent( ...args ) {
    return this._delegate.dispatchEvent( ...args );
  }

  /**
   * Remove an event listener
   * @param {...Object} args - Proxied arguments for the normal EventTarget.removeEventListener function.
   */
  removeEventListener( ...args ) {
    return this._delegate.removeEventListener( ...args );
  }

  /**
   * Get the last global status result of this model
   */
  get lastStatus() {
    if ( ! this._isEnabled )
      return 'disable';
    return this._lastStatus;
  }


  /**
   * Is this model bound to any, or a specific control
   * @param {String} [uuid]           - The instance's UUID if checking one UUID
   * @returns {Boolean}               - Is there any binds, or is the provided control bound?
   */
  isBound( uuid ) {
    if ( uuid != undefined )
      return this._binds.hasOwnProperty( uuid );
    return Object.keys( this._binds ).length > 0;
  }


  /**
   * Allow manual data change events in the case of direct data modification
   * @param {String} change           - The change type, either load, add, update or remove
   * @param {String|String[]} ids=[]  - ID or list of IDs affected by the change
   */
  dataChanged( change, ids=[] ) {
    this.app.log( 'debug', "Manual data change event for", change );
    if ( ! [ 'load', 'add', 'update', 'remove' ].includes( change ) ) {
      this.app.log( "error", "Unknown data change event" );
      throw new Error( "Unknown data change event" );
    }
    return this._updateBinds( 'data', change, ids, false );
  }


  /**
   * Bind this to a mulit-row component.
   * NOTE: Can cause side-effects of reloading if context changes.
   * @param {String} uuid             - The component UUID.
   * @param {Object} instance         - The component instance
   * @param {Object} [context]        - Optional context from the model, causing a reload if different
   */
  async bindMulti( uuid, instance, context ) {

    if ( this.config.modelType.includes( 'record' ) )
      throw new Error( "Attempt to bind single record model to multi records" );

    this.app.log( "debug", "Binding", instance.constructor.name, "to multi records" );

    // Prevent multiple multi-bound components for integrated models
    if ( this.config.modelType.includes( 'integrated' ) ) {
      for ( let uuid in this._binds ) {
        if ( this._binds[ uuid ].mode == "multi" )
          throw new Error( `Attempt to multi-bind already multi-bound integrated model` );
      }
    }

    // Store the model
    this._binds[ uuid ] = {
      'mode'    : 'multi',
      'instance': instance
    };

    // Set any provided context which might reload the model
    if ( this.setContextQuery && context ) {
      try {
        await this.setContextQuery( context );
      }
      catch( err ) {
        this.app.log( "error", "Setting context failed during bind:", err.message );
        this.app.log( 'debug', err );
      }
    }

    // The component is now bound
    await instance._statusChanged( 'bind' );

    // Let the model know if we have data
    if ( this.length )
      await instance._dataChanged( 'load' );

    // Propagate last status
    await instance._statusChanged( this.lastStatus );
  }

  /**
   * Bind a single record of this model to a target.
   * @param {String} uuid             - The instance's UUID.
   * @param {Object} instance         - The component instance to bind to.
   * @param {String} bindid           - The id of the record to bind.
   * @param {Boolean} [bindnew=false] - Whether to permit a bind to a new record
   * @param {Object} [context]        - Optional context from the model, causing a reload if different
   */
  async bindSingle( uuid, instance, bindid, bindnew=false, context ) {
    var isnew = false;

    this.app.log( "debug", "Binding", instance.constructor.name, "to single record", bindid );

    if ( ! this.hasRecord( bindid ) ) {
      if ( ! bindnew ) {
        this.app.log( "error", "Unable to bind to unknown record", bindid );
        throw new Error( "Unable to bind to unknown record" );
      }
      isnew = true;
    }

    this._binds[ uuid ] = {
      'instance': instance,
      'mode'    : 'single',
      'id'      : bindid,
      'new'     : isnew
    };

    // Set any context which may reload the model
    if ( this.setContextQuery && context ) {
      try {
        await this.setContextQuery( context );
      }
      catch( err ) {
        this.app.log( "error", "Setting context failed during bind:", err.message );
        this.app.log( 'debug', err );
      }
    }

    // Component is now bound
    await instance._statusChanged( 'bind' );

    // Propagate last status
    await instance._statusChanged( this.lastStatus );
  }


  /**
   * Quietly change a bind ID, typically in the case of the ID changing
   */
  changeSingleBindId( uuid, newid ) {
    var bind;
    bind = this._binds[ uuid ];
    if ( ! bind ) {
      this.app.log( "error", "Unable to alter bind that does not exist" );
      throw new Error( "Unable to alter bind that does not exist" );
    }
    bind[ 'id' ] = newid;
    bind[ 'new' ] = false;
  }


  /**
   * Unbind one component from this model
   * @param {String} uuid             - The instance's UUID.
   */
  async unbind( uuid, instance ) {
    if ( this._binds.hasOwnProperty( uuid ) ) {
      this.app.log( "debug", "Unbinding bound control", uuid );
      delete this._binds[ uuid ];
    }
    return await instance._statusChanged( 'unbind' );
  }

  /**
   * Proxy load to update bound components
   * @override
   * @see {@link RemoteModel#_load}
   */
  async _load( ...args ) {
    let loadInfo;

    if ( this.config.modelType.includes( 'integrated' ) )
      return this._loadIntegrated( ...args );

    this._updateBinds( 'status', 'pending' );

    try {
      loadInfo = await super._load( ...args );
      for ( let type of [ 'load', 'update', 'delete' ] )
        this._updateBinds( 'data', type, loadInfo[ type ], false );
      if ( ! this._isEnabled ) {
        this._updateBinds( 'status', 'enable' );
        this._isEnabled = true;
      }
      this._updateBinds( 'status', 'complete' );
    }
    catch( e ) {
      // Not able to be used?
      if ( this._isEnabled && this._inconsistent ) {
        this._updateBinds( 'status', 'disable' );
        this._isEnabled = false;
      }
      this._updateBinds( 'status', 'failure' );
      throw e;
    }

    return loadInfo;
  }



  /**
   * Proxy load for integrated models which doesn't fetch data in load methods
   * @async
   */
  async _loadIntegrated( ...args ) {

    // No status updates if not using websockets
    if ( ! this.config.url.websock || ! this.config.wsChannel )
      return super._load( ...args );

    try {
      this._updateBinds( 'status', 'pending' );
      await super._load( ...args );

      if ( ! this._isEnabled && ! this._inconsistent ) {
        this._updateBinds( 'status', 'enable' );
        this._isEnabled = true;
      }
      this._updateBinds( 'status', 'complete' );

      // Let components know they can fetch
      this._updateBinds( 'status', 'online' );
    }
    catch( err ) {
      if ( this._isEnabled ) {
        this._updateBinds( 'status', 'disable' );
        this._isEnabled = false;
      }
      this._updateBinds( 'status', 'failure' );
      throw err;
    }
  }



  /**
   * Proxy load-delta method to notify of updates and deletions
   * This is silent for status changes
   */
  async _loadDelta() {
    var added, updated, removed;
    try {
      [ added, updated, removed ] = await super._loadDelta();
      if ( added.length )
        this._updateBinds( 'data', 'add', added, false );
      if ( updated.length )
        this._updateBinds( 'data', 'update', updated, false );
      if ( removed.length )
        this._updateBinds( 'data', 'remove', removed, false );
    }
    catch( e ) {
      if ( this._isEnabled && this._inconsistent ) {
        this._updateBinds( 'status', 'disable' );
        this._isEnabled = false;
      }
      this._updateBinds( 'status', 'failure' );
      throw e;
    }
  }


  /**
   * Proxy a fetch request for integrated models. Non integrated models use ancestor fetch methods.
   * @async
   * @override
   * @param {Number} start                    - Position to start fetching from
   * @param {Number} [count=N]                - The number of records to fetch
   * @param {SortsType} [sorts]               - Any sorts to apply to the fetch
   * @param {FiltersCollection} [filters]     - Any set of filters to apply
   * @param {...*} [rest]                     - Supplementary arguments provided to superclass calls
   * @see {IntegratedModel#fetch}
   * @see {RemoteCollection#fetch}
   * @see {BasicCollection#fetch}
   * @returns {Promise<ResultType>}  - Return records and metadata
   */
  async fetch( start, count, sorts, filters, ...rest ) {
    let keepIds = [], bindDetails, bindId, bindInst, bindMode, recInfo;

    if ( ! this.config.modelType.includes( 'integrated' ) )
      return super.fetch( start, count, sorts, filters, ...rest );

    // Get any single record binds, add to list of records to keep
    for ( let uuid in this._binds ) {

      bindDetails = this._binds[ uuid ];
      bindId   = bindDetails[ 'id' ];
      bindInst = bindDetails[ 'instance' ];
      bindMode = bindDetails[ 'mode' ];

      if ( bindMode == "single" && bindInst ) {
        recInfo = this.getRecordInfo( bindId );
        if ( ! recInfo || recInfo.operation == "insert" )
          continue;
        keepIds.push( bindId );
      }
    }

    return super.fetch( start, count, sorts, filters, keepIds, ...rest );
  }



  /**
   * Proxy a integrated model remote fetch request
   * @async
   * @override
   * @see {IntegratedModel#_fetchRemote}
   * @returns {Promise<ResultType>}  - Return records and metadata
   */
  async _fetchRemote( ...args ) {
    let resultSet;

    try {
      this._updateBinds( 'status', 'pending' );
      resultSet = await super._fetchRemote( ...args );
      if ( ! this._isEnabled ) {
        this._updateBinds( 'status', 'enable' );
        this._isEnabled = true;
      }
      this._updateBinds( 'status', 'complete' );

      this._updateBinds( 'data', 'load', resultSet.map( rec => rec[ this.idField ] ) );
    }
    catch( err ) {
      if ( this._isEnabled ) {
        this._updateBinds( 'status', 'disable' );
        this._isEnabled = false;
      }
      this._updateBinds( 'status', 'failure' );
      throw err;
    }
    return resultSet;
  }


  /**
   * Proxy a websock record update/injection
   * @async
   * @override
   * @see {IntegratedModel#handleRecords}
   */
  async handleRecords( ...args ) {
    let ids;

    try {
      ids = await super.handleRecords( ...args );
      this._updateBinds( 'data', 'load', ids );
    }
    catch( err ) {
      this.app.log( "error", "Failure to handle record notification", err.message );
      if ( this._isEnabled ) {
        this._updateBinds( 'status', 'disable' );
        this._isEnabled = false;
      }
      throw err;
    }
    return ids;
  }


  /**
   * Proxy a websock record notification
   * @async
   * @override
   * @see {IntegratedModel#handleNotify}
   */
  async handleNotify( ...args ) {
    let removed;

    try {
      removed = await super.handleNotify( ...args );
      if ( removed.length )
        this._updateBinds( 'data', 'remove', removed );
    }
    catch( err ) {
      this.app.log( "error", "Failure to handle change notification", err.message );
      if ( this._isEnabled ) {
        this._updateBinds( 'status', 'disable' );
        this._isEnabled = false;
      }
      throw err;
    }
    return removed;
  }



  /**
   * Proxy reset to update bound components
   */
  reset( ids ) {
    let detail = super.reset( ids );

    // Support collection model
    if ( detail == undefined ) {
      detail = {};
      detail[ 'update' ] = ids;
    }

    if ( detail[ 'delete' ].length )
      this._updateBinds( 'data', 'remove', detail[ 'delete' ], true );
    if ( detail[ 'update' ].length )
      this._updateBinds( 'data', 'update', detail[ 'update' ], true );
    if ( detail[ 'insert' ].length )
      this._updateBinds( 'data', 'add', detail[ 'insert' ], true );
  }


  /**
   * Proxy rollback to update bound components
   */
  rollback( ...args ) {
    super.rollback( ...args );
    this._updateBinds( 'data', 'load' );
  }



  /**
   * Proxy add to update bound components
   * @override
   * @see {@link RemoteCollection#add} or {@link RemoteRecord#add} for arguments and return values.
   */
  add( ...args ) {
    let ids;
    try {
      ids = super.add( ...args );
      this._updateBinds( 'data', 'add', ids, this.config.modelType.includes( 'remote' ) );
    }
    catch(e) {
      throw e;
    }
    return ids;
  }


  /**
   * Proxy update method to update bound components
   * @override
   * @async
   * @see {@link RemoteCollection#update} or {@link RemoteRecord#update} for arguments and return values.
   */
  update( ...args ) {
    let ids;
    try {
      ids = super.update( ...args );
      this._updateBinds( 'data', 'update', ids, this.config.modelType.includes( 'remote' ) );
    }
    catch( e ) {
      throw e;
    }
    return ids;
  }


  /**
   * Proxy remove to update bound components
   * @override
   * @see {@link RemoteCollection#remove} or {@link RemoteRecord#remove} for arguments and return values.
   */
  remove( ...args ) {
    let ids, filtered = [];
    try {
      ids = super.remove( ...args );

      // Don't update binds for re-identified records
      for ( let id of ids ) {
        if ( ! this._reidentified || ! this._reidentified.has( id ) )
          filtered.push( id );
      }
      if ( filtered.length )
        this._updateBinds( 'data', 'remove', filtered, this.config.modelType.includes( 'remote' ) );
    }
    catch(e) {
      throw e;
    }
    return ids;
  }


  /**
   * Proxy the persis method for remote stores
   * @override
   * @see {@link RemoteModel#persist}
   */
  async persist( ...args ) {
    let detail;

    try {
      this._updateBinds( 'status', 'pending' );
      detail = await super.persist( ...args );
      this._updateBinds( 'status', 'complete' );
      if ( detail[ 'delete' ].length )
        this._updateBinds( 'data', 'remove', detail[ 'delete' ], false );
      if ( detail[ 'update' ].length )
        this._updateBinds( 'data', 'update', detail[ 'update' ], false );
      if ( detail[ 'insert' ].length )
        this._updateBinds( 'data', 'add', detail[ 'insert' ], false );

      // Re-bind re-identified records
      if ( this._reidentified && this._reidentified.size ) {
        for ( let [oldid,newid] of this._reidentified ) {
          this._updateBinds( 'data', 'rebind', [ oldid, newid ], false );
        }
        this._reidentified = new Map();
      }
    }
    catch( e ) {
      this.app.log( "error", "Failure to persist model", e.message );
      if ( this._inconsistent ) {
        this._updateBinds( 'status', 'disable' );
        this._isEnabled = false;
        throw e;
      }
      this._updateBinds( 'status', 'failure' );
    }
    return detail;
  }



  /**
   * Proxy clearing the model
   * @async
   * @override
   */
  async clear() {
    await super.clear();
    this._updateBinds( 'status', 'cleared' );
  }


  /**
   * Proxy connection error handling
   * @async
   * @override
   */
  async handleDisconnect() {
    super.handleDisconnect();

    if ( this._isEnabled && this._inconsistent ) {
      this._isEnabled = false;
      this._updateBinds( 'status', 'disable' );
    }
  }


  /**
   * Proxy websock unsubscribe
   */
  async handleUnsubscribe() {
    super.handleUnsubscribe();

    if ( this._isEnabled && this._inconsistent ) {
      this._isEnabled = false;
      this._updateBinds( 'status', 'disable' );
    }
  }


  /**
   * Proxy setting record error
   */
  setRecordError( id, error ) {
    super.setRecordError( id, error );
    this._updateBinds( 'data', 'update', [ id ], true );
  }


  /**
   * Proxy clearing error method
   */
  clearRecordError( id ) {
    super.clearRecordError( id );
    this._updateBinds( 'data', 'update' [ id ], true );
  }


  /**
   * Handle a change to the data or model
   * @private
   * @param {String} type                               - The type of change, either data or status
   * @param {String} change                             - The status or data change
   * @param {String|Number|Array<String|Number>} ids=[] - Single, list or map of IDs affected by data change
   * @param {Boolean} local=false                       - If this change type is local only
   */
  _updateBinds( type, change, ids=[], local=false ) {
    let ev, evname;
    let detail = {};

    this.app.log( "debug", "Updating bound components of", this.constructor.name, local ? "with local" : "with", type, change );

    // Cooerce ids to an Array
    if ( ! Array.isArray( ids ) )
      ids = [ ids ];

    // Keep last status change
    if ( type == "status" )
      this._lastStatus = change;

    // Dispatch self-emitted events first
    if ( eventsMap.hasOwnProperty( type + "_" + change ) ) {
      evname = eventsMap[ type + "_" + change ];

      // Append any IDs for data changes, and local flag
      if ( type == "data" ) {
        if ( change == "rebind" ) {
          detail[ "oldid" ] = ids[ 0 ];
          detail[ "newid" ] = ids[ 1 ];
        }
        else
          detail[ "ids" ] = this.app.util.clone( ids );
        detail[ "local" ] = local;
      }
      if ( type == "status" ) {
        detail[ 'status' ] = change;
      }

      // Emit on self
      ev = new CustomEvent( evname, { 'bubbles': false, 'cancelable': false, 'composed': false, 'detail': detail } );
      this.dispatchEvent( ev );

      // And generic change event
      this.app.log( "debug", `Emitting change event for ${type} change` );
      ev = new CustomEvent( eventsMap[ type + '_change' ], { 'bubbles': false, 'cancelable': false, 'composed': false, 'detail': detail } );
      this.dispatchEvent( ev );
    }


    // Build detail for bound components
    detail = {
      'change'    : change,
      'ids'       : ids,
      'local'     : local
    }

    // Update bound components
    for ( let uuid in this._binds ) {

      const bindDetails = this._binds[ uuid ];
      const bindId   = bindDetails[ 'id' ];
      const bindInst = bindDetails[ 'instance' ];
      const bindMode = bindDetails[ 'mode' ];

      switch( type ) {
        case 'status':
          bindInst._statusChanged( change ); // Note, not waiting
          break;
        case 'data':
          if ( bindMode == "multi" )
            bindInst._dataChanged( detail ); // Note, not waiting
          else if ( bindMode == "single" && ids.includes( bindId ) )
            bindInst._dataChanged( detail ) /* update or load */
          break;
      }
    }
  }
}


export { BindableModel };


