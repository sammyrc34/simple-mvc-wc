
import { BaseModel } from '../model.js';


/**
 * A basic client-side and in-memory single record with a simple interface
 * @class BasicRecord
 * @extends BaseModel
 * @classdesc The BasicRecord class represents a simple in-memory record
 */
class BasicRecord extends BaseModel {

  /**
   * Constructor for a basic data collection model
   * @param {Object} app        - The main application object.
   * @param {...Object} configs - Configuration options.
   * @see BaseModel#constructor
   */
  constructor( app, ...configs ) {
    const defaults = {
      indexFields: [],
      modelType: [ 'record' ]
    };

    super( app, defaults, ...configs );
  }


  /**
   * Setup the record
   */
  setup() {
    super.setup();

    // To store the record's data
    this._record = {};
  }


  /**
   * Clear the record data
   */
  clear() {
    this._record = {};
  }


  /**
   * Get the number of records
   * @returns {Number} length - Model length
   */
  get length() {
    if ( Object.keys( this._record ).length )
      return 1;
    else
      return 0;
  }


   /**
   * Get a copy of the data record
   * @returns {Object} record - The contained data record or undefined if no record
   */
  async fetch() {
    if ( Object.keys( this._record ).length == 0 )
      return undefined;
    else
      return this.getRecord();
  }


  /**
   * Check if a given record exists
   * @param   {String}  id        - The ID of the record
   * @returns {Boolean} bool      - Indicating if the record exists
   */
  hasRecord( id ) {
    const idField = this.idField;
    return this._record && this._record[ idField ] === id;
  }


  /** Get a single record by ID
   * @async
   * @param {String} id                   - The ID of the record.
   * @returns {Object|undefined} record   - The record or undefined if not found
   */
  async fetchOne( id ) {
    const idField = this.idField;
    if ( this._record[ idField ] === id )
      return this.getRecord( );
    else
      return undefined;
  }


  /**
   * Get our record directly
   * @returns {Object} record - Modification safe record object
   */
  getRecord() {
    return this.pick( this._getRecord( ) );
  }


  /**
   * Get all modification-safe records, synonym for getRecord for Record based classes
   * @public
   * @returns {Object} record - Modification safe record object
   */
  getRecords() {
    return this.getRecord()
  }


  /**
   * Get a raw and modification UNSAFE record
   * @private
   * @returns {Object}      - Our raw record object
   */
  _getRecord() {
    return this._record;
  }


  /** Add a record to the in-memory data model.
   * @param {Object} record       - The new record.
   * @returns {String} id         - The record ID of the new record
   * @throws                      - If unable to add the provided record
   */
  add( record ) {
    var rec;

    /* Make sure we don't have a record already */
    if ( Object.keys( this._record ).length > 0 ) {
      this.app.log( 'error', "Record already exists, unable to add record" );
      throw new Error( "Record already exists, unable to add record" );
    }

    // Apply defaults, then pick only known fields
    rec = this.pick( this.app.util.deepMerge( {}, this.config.defaultValues, record ) );
    this.resolve( rec );

    this.parse( rec );
  }


  /**
   * Remove the data record. Functionally equivalent to clear() for BasicRecord.
   */
  remove() {
    /* remove */
    this.clear();
  }


  /** Update the record
   * @param {Object} record       - The updated record
   * @throws                      - If unable to update the record
   */
  update( record ) {
    var idField = this.idField;

    // Ensure record is singular
    if ( Array.isArray( record ) )
      record = record[0];

    /* Check the record has the id */
    if ( ! record.hasOwnProperty( idField ) || record[ idField ] == undefined ) {
      this.app.log( 'error', "Unable to update record without ID" );
      throw new Error( "Unable to update record without ID" );
    }

    this.parse( record );
  }


  /**
   * Check that the provided record has required fields
   * @param {Object} record           - The record to check
   * @throws                          - If the record check fails
   */
  check( record ) {
    var rec;
    var idField = this.idField;
    var fields = this.requiredFields;

    if ( Array.isArray( record ) )
      rec = record[ 0 ];
    else
      rec = record;

    /* Check the record has the id */
    if ( ! rec.hasOwnProperty( idField ) || rec[ idField ] === undefined ) {
      this.app.log( 'error', "Record failed check, missing ID" );
      throw new Error( "Record failed check, missing ID" );
    }

    /* Check the record for required fields */
    if ( fields.length && ! fields.every( field => {
      return rec.hasOwnProperty( field ) && rec[ field ] !== undefined;
    } ) ) {
      this.app.log( "error", "Record failed check, missing one or more required fields" );
      throw new Error( "Record failed check, missing one or more required fields" )
    }

  }



  /**
   * Parse provided data record into our store.
   * @param {Object} record       - Object containing the record
   * @throws                      - If the record cannot be parsed
   */
  parse( record ) {
    // Check for required fields
    this.check( record );

    /* Update record */
    this._record = this.pick( record );
  }


}



export { BasicRecord };


