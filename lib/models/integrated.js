

import equal from 'fast-deep-equal';

/**
* The IntegratedModel is a more tightly integrated partial-cache client-server model.
*
* Interface compatible with RemoteModel although functionally different with loading and
* fetching.
*
* It is a flexible model that can be used for a variety of datasets, including very large
* or rapidly changing datasets. It uses a simple JSON based RPC API for CRUD operations,
* and optionally a websocket connection for change notifications or full record updates.
*
* It differs from RemoteModel in that it does not fully cache data. Rather, filtering,
* sorting and pagination are conducted server side in the first instance, allowing the model
* to only cache records currently utilised. To maintain consistency, websockets are used
* to listen for change notifications. The model can then respond appropriately to
* maintain the desired state.
*
* In the case of using websockets, the remote data source can notify of a change with type
* and an optional list of record IDs, or can provide full records to ingest. With options
* to re-fetch upon different change types, the model supports a range of use cases.
*
* NOTE 1: This model caches records per fetch, and so cannot be bound to multiple multi-bind
* components without complicating model function with pagination, filtering and more. Attempting
* to bind to multiple multi-bind components results in an error.
*
* NOTE 2: This model does not support in-model field indexes, and such indexes cannot be defined.
*
* NOTE 3: This model does not support Function based filters or sorts. This model only supports
* FiltersCollection, FilterType, CriteriaCollection, CriteriaType filter types only, and
* SortCriteria sort types only without Functions.
*/


// FIXME: what are we doing with query?
// And what to do when context changes?

const IntegratedModel = baseclass =>

/**
 * Mixin to enable a remote model to operate as a partial cache with remote filtering, sorting, pagination
 * @mixin
 * @alias IntegratedModel
 */
class extends baseclass {


  /**
   * Constructor for integrated remote data collection model.
   *
   * NOTE: The loadFirstFetch configuration of the base RemoteModel has no effect.
   *
   * @param {SimpleApp}   app                               - The main application object
   * @param {...Object}   configs                           - Configuration object(s)
   * @param {Boolean}     [configs.versioned=false]         - If record(s) are versioned with the version field
   * @param {String}      [configs.versionField="version"]  - The version field, defaulting to 'version'
   * @param {Object}      configs.url                       - URL configurations
   * @param {String}      [configs.url.websock]             - The URL to use for websocket updates / notifications
   * @param {String}      configs.url.fetch                 - The URL to use for fetch actions
   * @param {Boolean}     [configs.refetchInsert=false]     - Re-fetch upon unknown insert notifications
   * @param {Boolean}     [configs.refetchUpdate=false]     - Re-fetch upon unknown update notifications
   * @param {Boolean}     [configs.refetchDelete=false]     - Re-fetch upon unknown delete notifications
   * @param {Number}      [configs.cacheAge=0]              - Seconds until cache is re-fetched, 0 to never re-fetch, -1 to always fetch remote
   * @param {String}      [configs.wsChannel]               - Channel to subscribe to for notifications, supports contextual binds like URLs
   */
  constructor( app, ...configs ) {
    const defaults = {
      modelType       : [ 'integrated' ],
      url             : {},
      refetchInsert    : false,
      refetchUpdate    : false,
      refetchDelete    : false,
      cacheAge        : 0,
      wsChannel       : undefined,
    };

    const overrides = {
      'loadFirstFetch': false,
      'indexFields'   : undefined,
    }

    super( app, defaults, ...configs, overrides );

    return this;
  }



  /**
   * Setup the model
   */
  setup() {
    super.setup();

    this._ws = undefined;

    this._operation = undefined;        // Current operation if any
    this._subChannel = undefined;       // Currently subscribed channel
    this._refetchTimer = undefined;     // Timer for any re-fetch
    this._fetched = false;              // Do we have already fetched records
    this._lastFetch = undefined;        // Time of last successful remote fetch
    this._lastFetchQuery = undefined;   // To store most recent fetch query
    this._lastWSUrl = undefined;        // Last WS URL
    this._lastTotal = 0;                // Last known total number of records
    this._lastFiltered = 0;             // Last known filtered number of records
    this._loaded = false;               // If currently connected to WS
  }


  /**
   * Override
   * @override
   * @returns {*[]}       - The unique values the field has
   */
  uniqueValues() {
    this.app.log( 'warn', `Cannot determine unique field values of ${this.constructor.name}, use custom filtertype instead` );
    return [];
  }




  /**
   * Has this model fetched?
   * @public
   * @type {Boolean}                - If fetched
   */
  get fetched() {
    return this._fetched;
  }


  /**
   * Prepare to subscribe to websockets on a data fetch
   * @async
   * @override
   * @param {Object} [context]      - Any context (bind variables) for the dataset's urls
   * @param {Object} [query]        - Any supplemental query specific for the subsequent queries
   * @returns {Promise}             - Promise which resolves or throws
   */
  async load( context, query ) {

    this.app.log( "debug", "Attempting model load" );

    if ( this._operation !== undefined )
      this.app.log( "warn", "Load performed while another operation in progress" );

    // Store any query and context
    if ( query && ! equal( query, this.getQuery( true ) ) )
      this._query = this.app.util.clone( query );
    if ( context && ! equal( context, this.getContext( true ) ) )
      this._context = this.app.util.clone( context );

    // Forget previous query
    this._lastFetchQuery = undefined;

    // Make any required connection
    await this._load();
  }


  /**
   * Is this model loaded? (connected)
   * @public
   * @type {Boolean}                      - If this model is loaded
   */
  get loaded() {
    if ( ! this._loaded )
      return false;
    if ( ! this._ws )
      return false;
    return this._ws && this._ws.readyState == 1;
  }


  /**
   * Attempt connection if utilising websockets
   * @async
   * @protected
   * @throws    - If unable to connect
   */
  async _load() {
    if ( ! this.config.url.websock || ! this.config.wsChannel )
      return;

    if ( ! this.checkBinds() ) {
      this.app.log( "debug", "Cannot load without context parameters" );
      throw new Error( "Cannot load without context parameters" );
    }

    this.app.log( "debug", "Connecting to websocket service" );

    try {
      this._operation = "load";
      await this._tryConnect();
      this._operation = undefined;
      this._loaded = true;
      this._loadedContext = this._context;
      this._loadedQuery = this._query;
    }
    catch( err ) {
      this._operation = undefined;
      this.app.log( 'error', `Unable to connect to endpoint: ${err.message}` );
      this.app.log( 'debug', err.stack );
      throw err;
    }
  }



  /**
   * Override reload behaviour, not supported by integrated models
   * @async
   * @override
   */
  async reload() {
    this.app.log( "debug", "Bypassing reload for", this.constructor.name );
    return;
  }


  /**
   * Set the model's context and or query
   * @public
   * @async
   * @override
   * @param {Object} [context]  - Context map with URL bind variables for further queries
   * @param {Object} [query]    - Query map for further queries
   * @returns {Promise}         - A promise which resolves if context is set, otherwise throws
   */
  async setContextQuery( context={}, query={} ) {
    const oldcontext = this.getContext( true );
    const oldquery = this.getQuery( true );
    let newcontext = this.app.util.clone( context );
    let newquery = this.app.util.clone( query );
    let querychanged = false;
    let contextchanged = false;
    let load = false;

    querychanged = equal( oldquery, newquery ) ? false : true;
    contextchanged = equal( oldcontext, newcontext ) ? false : true;

    if ( ! querychanged && ! contextchanged ) {
      this.app.log( "debug", "Context and query unchanged" );
      return;
    }

    try {
      // Set context and or query and flag clearing
      if ( ! contextchanged )
        newcontext = oldcontext
      if ( ! querychanged )
        newquery = oldquery;

      // Set model to clear
      this._clearonload = true;

      // Reload as needed
      if ( contextchanged && this.config.loadContext )
        load = true;
      else if ( querychanged && this.config.loadQuery )
        load = true;

      if ( this.loaded )
        await this._tryUnsubscribe();

      if ( load && this.checkBinds( newcontext ) ) {
        await this.load( newcontext, newquery );
      }
    }
    catch( err ) {
      // Fall back context on failure
      this.app.log( "error", "Set context & query failed:", err.message );
      throw err;
    }
  }





  /**
   * Fetch one or more records from the model.
   * @async
   * @param {Number} start=0                          - The record number to start from. Defaults to 0;
   * @param {Number} count=100                        - The number of records to fetch. Defaults to 100. Set to 0 to request & fetch all.
   * @param {SortsType} [sorts=[]]                    - Any sorts to apply to the fetch
   * @param {FiltersCollection} [filters={}]          - A collection of filters to apply
   * @param {Array<String|Number>} [keep=[]]          - Additional records to include in fetch
   * @returns {Promise<ResultType>}                   - Return records and metadata
   */
  async fetch( start=0, count=100, sorts=[], filters={}, keep=[] ) {
    let retdata = new Array();
    let newcount = 0, query = {}, remote = false;

    this.app.log( 'debug', `Fetching records from model` );

    // Build query
    query.operation = "fetch";
    query.include = keep;
    query.filters = {};
    query.sorts = [];
    query.extra = {};
    sorts = this.app.util.clone( sorts );
    if ( filters.hasOwnProperty( ':rules' ) && filters[ ':rules' ].length )
      query.filters = this._checkFilters( filters );
    if ( ! Array.isArray( sorts ) )
      sorts = [ sorts ];
    if ( this._query && Object.keys( this._query ).length )
      Object.assign( query.extra, this._query );
    query.sorts = this._checkSorts( sorts );
    query.start = start;
    query.count = count;

    // Remote or local fetch?
    if ( this.config.cacheAge == -1 ) // Records not cached?
      remote = true;
    else if ( ! this._lastFetchQuery ) // No previous query?
      remote = true;
    else if ( this._inconsistent ) // inconsistent?
      remote = true;
    else if ( ! equal( query, this._lastFetchQuery ) ) // Different query
      remote = true;

    if ( remote )
      this.app.log( 'debug', `Fetching records from remote source` );

    // Store query
    this._lastFetchQuery = query;

    // Perform remote fetch as needed
    if ( remote )
      return this._fetchRemote();

    // For a local fetch, use default behaviour and then modify metadata
    retdata = await super.fetch( start, count, sorts, filters );

    if ( count == 0 )
      count = this._ids.length;

    // Count number of inserting records
    for ( let id of this._ids ) {
      if ( this._data.get( id )._operation == "insert" )
        newcount++;
    }

    // Note, estimated filter and total count
    retdata.filterCount = this._lastFiltered + ( retdata.length - this._lastFiltered );
    retdata.totalCount = this._lastTotal + newcount;
    retdata.fetchCount = retdata.length;

    return retdata;
  }



  /**
   * Fetch one or more records from the remote store
   * @async
   * @protected
   * @returns {Promise<ResultType>}                   - Return records and metadata
   */
  async _fetchRemote() {
    let retdata = new Array();
    let url, ids, records, additional, response, newcount = 0;

    try {

      // Try to (re)connect as needed, and ensure a first load notifies of such
      if ( ! this._loaded )
        await this._load();
      else
        await this._tryConnect();

      // Try to subscribe to websocks as needed
      await this._subscribe();
    }
    catch( err ) {
      // Log and continue
      this.app.log( "warn", "Connection or subscription failure during fetch", err );
      this._inconsistent = true;
    }

    // Attempt the fetch
    try {

      url = this.getUrl( 'fetch' );
      if ( ! url )
        throw new Error( `Fetch URL not defined` );
      this.app.log( 'debug', `Performing remote fetch from ${url}` );

      // Perform the fetch
      response = await this._remoteRequest( url, 'POST', this._lastFetchQuery );

      // Ensure successful overall response
      if ( ! response || response['success'] != true ) {
        this.app.log( "error", "Fetch failed", response );
        throw new Error( `${response.message} (status ${response.status})` );
      }

      // Ensure we have records even if empty
      records = response.records;
      if ( ! Array.isArray( records ) ) {
        this.app.log( "error", "Malformed response, unexpected records field", response );
        throw new Error( "Malformed response" );
      }

      // Also get additional records for bound components
      additional = response.additional || [];
      if ( ! Array.isArray( additional ) ) {
        this.app.log( "error", "Malformed response, unexpected additional field", response );
        throw new Error( "Malformed response" );
      }

      // Clear previously fetched records
      this._clearFetched();

      // Load into our cache for modifications / removal
      ids = this.parse( records, 'load' );
      for ( let id of ids )
        retdata.push( this.getRecord( id ) );

      // Load in additional records too
      this.parse( additional, 'load' );

      // Count number of new records in model
      for ( let id of this._ids ) {
        if ( this._data.get( id )._operation == "insert" ) {
          newcount++;
        }
      }

      // Add record metadata if provided
      if ( response.meta ) {
        this._lastTotal = response.meta.totalCount;
        this._lastFiltered = response.meta.filterCount;
      }
      else {
        this._lastTotal = retdata.length;
        this._lastFiltered = retdata.length;
      }

      // If new records, sort and filter locally again
      if ( newcount ) {
        retdata = await super.fetch( start, count, sorts, filters );

        retdata.fetchCount = retdata.length;
        retdata.totalCount = this._lastTotal + newcount;
        retdata.filterCount = this._lastFiltered + ( retdata.length - records.length );
      }
      else {
        retdata.fetchCount = retdata.length;
        retdata.totalCount = this._lastTotal + newcount;
        retdata.filterCount = this._lastFiltered + newcount;
      }

      // Flag as consistent and fetched
      this._fetched = true;
      this._inconsistent = false;
      this._lastFetch = Date.now();

      // Set a re-fetch as needed
      if ( this.config.cacheAge > 0 ) {
        if ( this._refetchTimer )
          clearTimeout( this._refetchTimer );
        this._refetchTimer = setTimeout( () => { this.refetch();  } );
      }
      else if ( this._refetchTimer ) {
        clearTimeout( this._refetchTimer );
        this._refetchTimer = undefined;
      }

    }
    catch( err ) {
      this._inconsistent = true;
      throw err;
    }

    return retdata;
  }



  /**
   * Refetch records from remote store with last fetched query
   * @throws    - In event
   * @async
   * @protected
   */
  async refetch() {
    let retdata, query;

    query = this._lastFetchQuery;
    if ( ! query )
      throw new Error( `Cannot re-fetch without previous query` );

    // Attempt the fetch
    try {
      retdata = await this._fetchRemote();
    }
    catch( err ) {
      this._inconsistent = true;
      this.app.log( "error", "Failure during re-fetching", err.message );
      this.app.log( "debug", err.stack );
      throw err;
    }

    return retdata;
  }


  /**
   * Proxy the persist method for remote stores to refetch on change
   * @override
   * @see {@link RemoteModel#persist}
   */
  async persist( ...args ) {
    let detail;

    detail = await super.persist( ...args );

    if ( detail && ( detail.insert.length || detail.update.length || detail.delete.length ) ) {
      this.app.log( 'debug', `Re-fetching model post persist` );
      this.refetch(); // Note, async
    }

    return detail;
  }



  /**
   * Clear this models' records and state
   * @async
   * @override
   */
  async clear( ) {
    this._fetched = false;
    this._loaded = false;
    this._data = new Map();
    this._ids = [];
    this._modified = [];

    // Clear previous query, context
    this._lastFetchQuery = undefined;
    this._context = {};
    this._query = {};

    await this._tryUnsubscribe();

    this._lastWSUrl = undefined;
    this._ws = undefined;
  }



  /**
   * Clear previously fetched records, keeping new records
   */
  _clearFetched() {
    const newIds = [];
    const newData = new Map();
    for ( let id of this._ids ) {
      if ( this._data.get( id )._operation == "insert" ) {
        newIds.push( id );
        newData.set( id, this._data.get( id ) );
      }
    }
    this._fetched = false;
    this._data = newData;
    this._ids = newIds;
  }



  /**
   * Handle a notification of a change to one or more records
   * @async
   * @protected
   * @param {Object} detail                         - Notification details
   * @param {String} detail.change                  - Type of change, insert, update or delete
   * @param {Array<String|Number>} [detail.ids=[]]  - IDs for the change if provided
   * @returns {Promise<Array>}                      - Record IDs that experienced a delete, or an empty list otherwise
   */
  async handleNotify( detail ) {
    let known = [], unknown = [], filtered = [], removed = [], refetch = false
    let changeType = detail.change || 'unknown';
    let ids = detail.ids || [];

    this.app.log( 'debug', `Notification of ${changeType} change for records`, ids );

    // Find records that are known and unknown
    for ( let testId of ids ) {
      if ( this.hasRecord( testId ) )
        known.push( testId );
      else
        unknown.push( testId );
    }

    if ( unknown.length )
      this.app.log( 'debug', `Notification for unknown records`, unknown );

    // Work out if re-fetch required
    if ( changeType !== "delete" && ids.length && known.length )
      refetch = true;
    else if ( changeType == "insert" && unknown.length && this.config.refetchInsert )
      refetch = true;
    else if ( changeType == "update" && unknown.length && this.config.refetchUpdate )
      refetch = true;
    else if ( changeType == "delete" && unknown.length && this.config.refetchDelete )
      refetch = true;

    if ( refetch )
      this.app.log( 'debug', `Model re-fetch required` );

    // If not re-fetching, process deletes for known records now
    try {
      if ( ! refetch && changeType == "delete" && known.length ) {
        for ( let id of this._ids ) {
          if ( ! ids.includes( id ) )
            filtered.push( id );
          else {
            removed.push( id );
            this._data.delete( id );
          }
        }
        this._ids = filtered;
        return removed;
      }
    }
    catch( err ) {
      this.app.log( 'error', `Failed to delete notification: ${err.messsage}` );
      this._inconsistent = true;
      throw err;
    }

    if ( refetch ) {
      try {
        await this.refetch();
        return removed;
      }
      catch( err ) {
        this.app.log( 'error', `Failed to fetch after ${changeType} notification: ${err.message}` );
        throw err;
      }
    }

    return removed;
  }



  /**
   * Handle a new or updated record
   * @async
   * @protected
   * @param {Object} detail                 - Notification details
   * @param {Array<Object>} detail.records  - Records to ingest
   * @returns {Promise<Array>}              - Record IDs that have been ingested
   */
  async handleRecords( detail ) {
    let records = detail.records || [];
    let ids, testId, known = [], unknown = [];

    try {
      // Find records that are known and unknown
      for ( let record of records ) {
        if ( ! record.hasOwnProperty( this.idField ) )
          throw new Error( `Record malformed` );

        testId = record[ this.idField ];
        if ( this.hasRecord( testId ) )
          known.push( testId );
        else
          unknown.push( testId );
      }

      // Ingest the records as provided
      ids = this.parse( records, 'load' );
    }
    catch( err ) {
      this._inconsistent = true;
      this.app.log( 'error', `Failed to process shared records: ${err.messsage}` );
      throw err;
    }

    return ids;
  }


  /**
   * Handle a separate request-unrelated websocket event
   * @async
   * @param {String} type         - Event type, 'disconnect', 'connect', 'unsubscribe', 'resubscribe', 'notify', 'records',
   * @param {Object} [detail={}]  - Event detail
   */
  async handleEvent( type, detail={} ) {

    this.app.log( "debug", `Event notification of type ${type} recieved` );

    try {
      switch( type ) {
        case "connect":
          return await this.handleConnect();
        case "disconnect":
          return this.handleDisconnect();
        case "unsubscribe":
          return this.handleUnsubscribe();
        case "resubscribe":
          return await this.handleResubscribe();
        case "notify":
          return await this.handleNotify( detail );
        case "records":
          return await this.handleRecords( detail );
      }
    }
    catch( err ) {
      this.app.log( 'error', `Failure in handling event notification: ${err.message}` );
      this.app.log( 'debug', err.stack );
    }
  }


  /**
   * Manage a websocket re-connection
   * @async
   * @protected
   */
  async handleConnect() {
    try {
      // Refetch if we have records and have disconnected
      if ( this._fetched && this._inconsistent )
        await this.refetch();
    }
    catch( err ) {
      this.app.log( 'error', `Failed in re-fetch: ${err.message}` );
      this._inconsistent = true;
      throw err;
    }
  }


  /**
   * Manage a websocket connection failure
   * @protected
   */
  handleDisconnect() {
    this.app.log( 'debug', `Model ${this.constructor.name} socket disconnected` );
    this._loaded = false;
    this._inconsistent = true;
  }


  /**
   * Manage an non-requested unsubscribe event
   * @protected
   */
  handleUnsubscribe() {
    this.app.log( 'debug', `Model ${this.constructor.name} unexpected unsubscribe` );
    this._inconsistent = true;
  }


  /**
   * Manage a re-subscribe event
   * @async
   * @protected
   */
  async handleResubscribe() {
    try {
      if ( this._fetched && this._inconsistent )
        await this.refetch();
    }
    catch( err ) {
      this.app.log( 'error', `Failed in resubscribe re-fetch: ${err.message}` );
      this._inconsistent = true;
      throw err;
    }
  }



  /**
   * Subscribe to websocket channel
   * @async
   * @throws  - If unable to subscribe
   */
  async _subscribe() {
    if ( ! this.config.url.websock || ! this.config.wsChannel )
      return;
    if ( ! this._ws || this._ws.readyState !== 1 )
      throw new Error( "Cannot subscribe while connection not open" );

    await this._trySubscribe();
  }


  /**
   * Attempt to connect to endpoint as needed
   * @async
   * @private
   * @throws    - If unable to connect to websock endpoint
   */
  async _tryConnect() {
    const url = this.getUrl( 'websock' );

    // If changing URL, unsubscribe first
    if ( this._lastWSUrl && url !== this._lastWSUrl ) {
      this._tryUnsubscribe();
    }

    // Already connected or connecting?
    if ( this._ws && this._ws.readyState <= 1 ) {
      this.app.log( 'debug', `Endpoint connection already exists` );
      return;
    }

    this.app.log( "debug", "Connecting to websock endpoint", url );

    // Get a client handle and connect
    this._ws = this.app.fetchWebsock( url );
    try {
      await this._ws.connect();
      this._lastWSUrl = url;
    }
    catch( err ) {
      this.app.log( 'error', `Failed connecting to websock endpoint: ${err.message}` );
      throw err;
    }
  }



  /**
   * Attempt to subscribe for change notifications
   * @async
   */
  async _trySubscribe( ) {
    const context = this.getContext( true );
    let fields, tmp, channel = this.config.wsChannel;

    // Replace contextual bind variables
    if ( channel.includes( ':' ) ) {
      fields = channel.match( /:(\w+?):/g );
      for ( let field of fields ) {
        tmp = field.replace( /\:/g, '' );
        if ( ! context.hasOwnProperty( tmp ) ) {
          this.app.log( "warn", "Subscribe not possible with non-interpolated field", tmp );
          throw new Error( "Cannot interpolate unknown field: " + tmp );
        }
        channel = channel.replace( field, encodeURIComponent( context[ tmp ] ) );
      }
    }

    // Already subscribed?
    if ( this._subChannel && channel == this._subChannel )
      return;
    else if ( this._subChannel ) {
      try {
        await this._tryUnsubscribe();
      }
      catch( err ) {
        this.app.log( "warn", `Failed to unsubscribe from ${this._subChannel}, subscribing anyway` );
      }
    }

    this.app.log( "debug", `Subscribing to websock channel ${channel}` );

    await this._ws.subscribe( channel, this );

    this._subChannel = channel;
  }


  /**
   * Attempt to unsubscribe from change notifications
   * @async
   */
  async _tryUnsubscribe() {
    let channel = this._subChannel;

    if ( ! channel || ! this._ws )
      return;

    this.app.log( "debug", `Unsubscribing from websock channel ${channel}` );

    try {
      // Best effort approach
      this._subChannel = undefined;
      await this._ws.unsubscribe( channel, this );
    }
    catch( err ) {
      this.app.log( "warn", `Unable to unsubscribe: ${err.message}` );
    }
  }


  /**
   * Check filters are suitable for server side filtering.
   * @param {FiltersCollection|CriteriaCollection} filters  - Filters to check
   * @throws - In event of unsupported filter
   */
  _checkFilters( filters ) {
    const rules = filters[ ':rules' ];
    const mode = filters[ ':mode' ];

    if ( mode && ( mode !== "AND" && mode !== "OR" ) )
      throw new Error( `Unsupported filter mode ${mode}` );

    if ( ! rules )
      return;

    if ( ! Array.isArray( rules ) )
      throw new Error( `Unknown rules type found` );

    for ( let rule of rules ) {

      if ( Array.isArray( rule ) )
        throw new Error( `Unknown filter rule type, expecting object found array` );

      if ( typeof rule == "object" && rule.hasOwnProperty( ':rules' ) ) {
        this._checkFilters( rule );
        continue;
      }

      if ( typeof rule == "object" ) {
        for ( let field in rule ) {

          if ( field == ":rules" ) {
            this._checkFilters( rule[ field ] );
            continue;
          }

          if ( Array.isArray( rule[ field ] ) || typeof rule[ field ] !== "object" ) {
            throw new Error( `Unknown filter field rule type, expecting object` );
          }

          for ( let type in rule[ field ] ) {

            if ( type == "has" )
              throw new Error( `Unsupported value operator 'has'` );

            if ( Array.isArray( rule[ field ][ type ] ) ) {
              for ( let val of rule[ field ][ type ] ) {
                if ( typeof val == "function" )
                  throw new Error( `Unsupported criteria value function` );
              }
            }

            if ( typeof rule[ field ][ type ] == "function" )
              throw new Error( `Unsupported criteria value function` );
          }
        }
      }
    }

    return filters;
  }



  /**
   * Check sorts are suitable for server side sorting
   * @param {SortsType} sorts     - Sorts to check
   */
  _checkSorts( sorts ) {

    for ( let sort of sorts ) {
      if ( typeof sort == "function" )
        throw new Error( `Unsupported sort function` );
      if ( sort.hasOwnProperty( 'func' ) && typeof sort[ 'func' ] == "function" )
        throw new Error( `Unsupported sort function` );
    }

    return sorts;
  }



}

export { IntegratedModel };
