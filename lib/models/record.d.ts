/**
 * A basic client-side and in-memory single record with a simple interface
 * @class BasicRecord
 * @extends BaseModel
 * @classdesc The BasicRecord class represents a simple in-memory record
 */
export class BasicRecord extends BaseModel {
    /**
     * Constructor for a basic data collection model
     * @param {Object} app        - The main application object.
     * @param {...Object} configs - Configuration options.
     * @see BaseModel#constructor
     */
    constructor(app: any, ...configs: any[]);
    _record: any;
    /**
     * Clear the record data
     */
    clear(): void;
    /**
     * Get the number of records
     * @returns {Number} length - Model length
     */
    get length(): number;
    /**
    * Get a copy of the data record
    * @returns {Object} record - The contained data record or undefined if no record
    */
    fetch(): any;
    /**
     * Check if a given record exists
     * @param   {String}  id        - The ID of the record
     * @returns {Boolean} bool      - Indicating if the record exists
     */
    hasRecord(id: string): boolean;
    /** Get a single record by ID
     * @async
     * @param {String} id                   - The ID of the record.
     * @returns {Object|undefined} record   - The record or undefined if not found
     */
    fetchOne(id: string): any | undefined;
    /**
     * Get our record directly
     * @returns {Object} record - Modification safe record object
     */
    getRecord(): any;
    /**
     * Get all modification-safe records, synonym for getRecord for Record based classes
     * @public
     * @returns {Object} record - Modification safe record object
     */
    public getRecords(): any;
    /**
     * Get a raw and modification UNSAFE record
     * @private
     * @returns {Object}      - Our raw record object
     */
    private _getRecord;
    /** Add a record to the in-memory data model.
     * @param {Object} record       - The new record.
     * @returns {String} id         - The record ID of the new record
     * @throws                      - If unable to add the provided record
     */
    add(record: any): string;
    /**
     * Remove the data record. Functionally equivalent to clear() for BasicRecord.
     */
    remove(): void;
    /** Update the record
     * @param {Object} record       - The updated record
     * @throws                      - If unable to update the record
     */
    update(record: any): void;
    /**
     * Check that the provided record has required fields
     * @param {Object} record           - The record to check
     * @throws                          - If the record check fails
     */
    check(record: any): void;
    /**
     * Parse provided data record into our store.
     * @param {Object} record       - Object containing the record
     * @throws                      - If the record cannot be parsed
     */
    parse(record: any): void;
}
import { BaseModel } from '../model.js';
