/**
 * Field criteria type where keys are operations, and values to be filtered on.
 * String type properties coerce values to strings before comparison.
 * If one FieldCriteriaType contains multiple rules, all must match.
 */
export type FieldCriteriaType = {
    /**
     * - Field value must be greater than
     */
    gt?: number;
    /**
     * - Field value must be less than
     */
    lt?: number;
    /**
     * - Field value must be greater than or equal to
     */
    ge?: number;
    /**
     * - Field value must be less than or equal to
     */
    le?: number;
    /**
     * - Field value must be equal to value
     */
    eq?: string;
    /**
     * - Field value must contain value
     */
    ct?: string;
    /**
     * - Field value must start with value
     */
    sw?: string;
    /**
     * - Field value must end with value
     */
    ew?: string;
    /**
     * - Field value must be a list and contain value
     */
    has?: string;
    /**
     * - Field value must be one of values
     */
    in?: string[];
    /**
     * :case=true] - If strings are case sensitive
     */
    ""?: boolean;
};
/**
 * Field Criteria collection type, containing one or more field scoped filter criteria
 */
export type FieldCriteriaCollection = {
    /**
     * :rules  - List of field criteria types
     */
    ""?: Array<FieldCriteriaType>;
};
/**
 * Filter definitions where keys denote field name, and valus are a criteria, criteria list or function.
 * Filter funtions are called with a list of IDs to filter, and the model as the second argument.
 * If one FilterType contains multiple fields, all must match
 */
export type FilterType = {
    [x: string]: Function | FieldCriteriaType | FieldCriteriaCollection;
};
/**
 * A simple array of filter collections, filter types or filter functions.
 */
export type FilterList = Array<FiltersCollection | FilterType | Function>;
/**
 * Filters collection type, containing one list of filter types, collection or functions
 */
export type FiltersCollection = {
    /**
     * :rules      - List of rules
     */
    ""?: FilterList;
};
/**
 * - Sort criteria containing either field and either func, or direction and type
 */
export type SortCriteria = {
    /**
     * - Field to sort on
     */
    field: string;
    /**
     * - Custom field sort function, called with field A value, field B value
     */
    func?: Function;
    /**
     * - Sort direction, asc for ascending, desc for descending
     */
    direction?: string;
    /**
     * - Field data type, number or string
     */
    type?: string;
};
/**
 * Sorts type, a mixed array of functions or sort criteria.
 * Functions are called with the ID for record A and B, expected to return a Number per Array.sort's compareFn
 */
export type SortsType = Array<SortCriteria | Function>;
/**
 * - Fetch result type, records and metadata
 */
export type ResultType = any[];
/**
 * Field criteria type where keys are operations, and values to be filtered on.
 * String type properties coerce values to strings before comparison.
 * If one FieldCriteriaType contains multiple rules, all must match.
 * @typedef {Object} FieldCriteriaType
 * @property {Number} [gt]          - Field value must be greater than
 * @property {Number} [lt]          - Field value must be less than
 * @property {Number} [ge]          - Field value must be greater than or equal to
 * @property {Number} [le]          - Field value must be less than or equal to
 * @property {String} [eq]          - Field value must be equal to value
 * @property {String} [ct]          - Field value must contain value
 * @property {String} [sw]          - Field value must start with value
 * @property {String} [ew]          - Field value must end with value
 * @property {String} [has]         - Field value must be a list and contain value
 * @property {String[]} [in]        - Field value must be one of values
 * @property {Boolean} [:case=true] - If strings are case sensitive
 */
/**
 * Field Criteria collection type, containing one or more field scoped filter criteria
 * @typedef {Object} FieldCriteriaCollection
 * @property {Array<FieldCriteriaType>} :rules  - List of field criteria types
 * @property {String} [:mode="AND"]             - Operator for multiple criteria, AND or OR.
 */
/**
 * Filter definitions where keys denote field name, and valus are a criteria, criteria list or function.
 * Filter funtions are called with a list of IDs to filter, and the model as the second argument.
 * If one FilterType contains multiple fields, all must match
 * @typedef {Object<string, Function|FieldCriteriaType|FieldCriteriaCollection>} FilterType
 */
/**
 * A simple array of filter collections, filter types or filter functions.
 * @typedef {Array<FiltersCollection|FilterType|Function>} FilterList
 */
/**
 * Filters collection type, containing one list of filter types, collection or functions
 * @typedef {Object} FiltersCollection
 * @property {FilterList} :rules      - List of rules
 * @property {String} [:mode="AND"]   - Operator for multiple filters, AND or OR.
 * Functions are called with a list of IDs currently passing, exptected to return a list of
 * filtered IDs
 *
 * @example
 * Eg.  {
 *        ':rules': [
 *          { field_a: { ct: "abc" } }
 *        ]
 *      }
 *        - single match, field_a must contain "abc"
 *
 *      {
 *        ':rules': [
 *          { field_a: { ct: "abc" } },
 *          { field_b: { ct: "def" } }
 *        ]
 *      }
 *        - field_a must contain "abc" AND field_b contains "def"
 *
 *      {
 *        ':rules': [
 *          { field_a: {
 *            ':mode': "OR",
 *            ':rules': [
 *              { ct: "abc" },
 *              { ct: "def" }
 *            ]
 *          } }
 *        ]
 *      }
 *        - field_a must contain "abc" OR "def"
 *
 *      {
 *        ':rules': [
 *          { field_a: {
 *            ':rules': [
 *              { sw: "abc" },
 *              { ew: "def" }
 *           ]
 *        } }
 *        ]
 *      }
 *        - field_a must start with "abc" AND end with "def"
 *
 *      {
 *        ':mode': "OR",
 *        ':rules': [
 *          { field_a: { ct: "abc" } },
 *          { field_b: { ct: "def" } }
 *        ]
 *      }
 *        - field_a contains "abc" OR field_b contains "def"
 *
 *      {
 *        ':mode': "OR",
 *        ':rules': [
 *          Function,
 *          { field_a: Function, field_b: { ct: "def" } }
 *        ]
 *      }
 *        - field_a via function call AND field_b must contain "def"
 *
 *      {
 *        ':rules': [
 *          { field_a: { eq: "one" },
 *          {
 *            ':mode': "OR",
 *            ':rules': [
 *              { field_b: { ct: 'two' } },
 *              { field_c: { ct: 'three' }
 *            ]
 *          }
 *        ]
 *      }
 *        - field_a is "one" and either field_b contains "two" or field_c contains "three"
 */
/**
 * Sort criteria type
 * @typedef {Object} SortCriteria     - Sort criteria containing either field and either func, or direction and type
 * @property {String} field           - Field to sort on
 * @property {Function} [func]        - Custom field sort function, called with field A value, field B value
 * @property {String} [direction=asc] - Sort direction, asc for ascending, desc for descending
 * @property {String} [type]          - Field data type, number or string
 */
/**
 * Sorts type, a mixed array of functions or sort criteria.
 * Functions are called with the ID for record A and B, expected to return a Number per Array.sort's compareFn
 * @typedef {Array<SortCriteria|Function>} SortsType
 */
/**
 * Fetch result type containing records and metadata
 * @typedef {Array} ResultType        - Fetch result type, records and metadata
 * @property {Number} totalCount      - Total number of records in the data store
 * @property {Number} fetchCount      - Total number of records retrieved by the fetch
 * @property {Number} filterCount     - Total number of filtered records
 */
/**
 * A basic client-side and in-memory multi-record collection with a simple interface
 * @class BasicCollection
 * @extends BaseModel
 * @classdesc The BasicCollection class represents a simple in-memory collection of keyed data items.
 */
export class BasicCollection extends BaseModel {
    /**
     * Constructor for a basic data collection model
     * @param {Object} app                          - The main application object.
     * @param {...Object} configs                   - Configuration options
     * @param {Object}    [configs.conf]              - Single configuration object
     * @param {String[]}  [configs.indexFields]         - List of fields to index for quicker access
     * @see BaseModel#constructor
     */
    constructor(app: any, ...configs: any[]);
    _data: Map<any, any>;
    _ids: any[];
    _idxs: {};
    /**
     * Clear all in-memory data.
     */
    clear(): void;
    /**
     * Get the total number of records.
     * @returns {Number} length - The number of records
     */
    get length(): number;
    /**
     * Get a single cloned record by ID
     * @async                       - Asynchronous to maintain interface with remote collection
     * @param {String|Number} id    - The ID of the record.
     * @returns {Object|undefined}  - The record or undefined if none found
     */
    fetchOne(id: string | number): any | undefined;
    /**
     * Get a range of records with optional sorting and filtering
     * @async                                           - Asynchronous to maintain interface with remote collections
     * @param {Number} start=0                          - The record number to start from. Defaults to 0;
     * @param {Number} count=N                          - The number of records to fetch. Defaults to full length.
     * @param {SortsType} [sorts]                       - Any sorts to apply to the fetch
     * @param {FiltersCollection} [filters]             - A collection of filters to apply
     * @returns {Promise<ResultType>}                   - Return records and metadata
     */
    fetch(start?: number, count?: number, sorts?: SortsType, filters?: FiltersCollection): Promise<ResultType>;
    /**
     * Get a modification-safe record by ID
     * @public
     * @param {String|Number} id    - The record ID
     * @returns {Object|undefined}  - The record object, or undefined if it doesn't exist
     */
    public getRecord(id: string | number): any | undefined;
    /**
     * Get all modification-safe records. Method fetch is preferred as this method supports neither sorting or filtering.
     * @public
     * @returns {Object[]|undefined}  - Modification safe record objects
     */
    public getRecords(): any[] | undefined;
    /**
     * Get the raw and modification UNSAFE record for an ID
     * @private
     * @param {String|Number} id  - The record ID
     * @returns {Object}          - The record object
     */
    private _getRecord;
    /**
     * Add data to the in-memory data model
     * @async
     * @param {Object[]} records  - An array of records in which to add.
     * @returns {String[]}        - The IDs impacted by this change
     */
    add(records: any[]): string[];
    /**
     * Remove one or more data records by their corresponding ID.
     * @param {String[]|Number[]} ids     - The IDs to remove
     * @returns {String[]}                - The IDs impacted by this change
     */
    remove(ids: string[] | number[]): string[];
    /**
     * Update one or more record to the data store
     * @param {Object|Object[]} records   - The updated record(s) to store.
     * @returns {String[]}                - The IDs impacted by this change
     * @throws                            - If unable to update records
     */
    update(records: any | any[]): string[];
    /**
     * Check that all provided records have required fields
     * @param {Object[]} records        - The records to check
     * @throws                          - If any record doesn'
     */
    check(records: any[]): void;
    /**
     * Parse provided data into our store.
     * Atomic method, failure safe for the model.
     * @param {Object[]} records  - Array of objects representing records
     * @returns {String[]}        - All IDs parsed into the store
     * @throws                    - If any record is unable to be parsed
     */
    parse(records: any[]): string[];
    /**
     * Check if a given record exists
     * @param {String} id           - The ID of the record
     * @returns {Boolean}           - Indicating if the record exists
     */
    hasRecord(id: string): boolean;
    /**
     * Helper method to search quickly for the first record with a field value. Useful for unique value fields.
     * @public
     * @param {String} field        - The field to search on
     * @param {*} value             - The value to search for
     * @returns {Object}            - The found record or undefined if not found
     */
    public searchOne(field: string, value: any): any;
    /**
     * Get a list of unique field values for field filtering
     * @returns {*[]}       - The unique values the field has
     */
    uniqueValues(field: any): any[];
    /**
     * Has this model got any errored records?
     * @type {Boolean}                      - If failed
     */
    get failed(): boolean;
    /**
     * Set one records error and error state
     * @param {String|Number} id            - The record ID to modify
     * @param {String} error                - The error message
     */
    setRecordError(id: string | number, error: string): void;
    /**
     * Clear a record state
     * @param {String|Number} id            - The record ID to modify
     */
    clearRecordError(id: string | number): void;
    /**
     * Get any message attached to a record
     * @returns {String|undefined}          - Message if defined, otherwise undefined
     */
    getMessage(id: any): string | undefined;
    /**
     * Get the metadata for a record
     * @param {String|Number} id            - The record ID to fetch metadata for
     * @returns {Object} meta               - The resulting record metadata if record is found, otherwise undefined
     * @returns {Boolean} meta.errored      - If this record is in an errored state
     * @returns {String} meta.message       - Any informational or error message attached to the record
     */
    getRecordInfo(id: string | number): any;
    /**
     * Reset metadata for one or more records
     * @param {String|Number|String[]|Number[]} ids - The IDs for the records to reset
     */
    reset(ids: string | number | string[] | number[]): void;
    /**
     * Add to or update existing index for specified IDs
     * @protected
     * @param {String[]} ids          - IDs to re-index
     */
    protected _indexRecords(ids: string[]): void;
    /**
     * Remove indexes for the provided record
     * @param {Object[]} records    - Records to remove indexes for
     */
    _removeIndexes(records: any[]): void;
    /**
     * Return a function to sort two records by a given number field and direction
     * @private
     * @param {String} direction        - The direction for the sort, either asc or desc
     * @returns {Function}
     */
    private _sortNumber;
    /**
     * Return a function to sort two records by a given string field and direction
     * @private
     * @param {String} direction        - The direction for the sort, either asc or desc
     * @returns {Function}              - The sorting function
     */
    private _sortAlpha;
    /**
     * Filter a list of records by given filter
     * @param {Number[]|String[]} ids               - ID list to filter
     * @param {FiltersCollection} filters           - The filters to apply
     * @returns {Number[]|String[]}                 - Matching IDs
     */
    _filter(ids: number[] | string[], filters: FiltersCollection): number[] | string[];
    /**
     * Accumulate record IDs as needed
     * @param {Number[]|String[]} alist   - List to IDs to intersect
     * @param {Number[]|String[]} blist   - List to IDs to intersect
     * @param {String} mode               - Mode to accumulate records, AND or OR
     * @returns {Number[]|String[]}       - Accumulated record IDs
     */
    _accumulate(alist: number[] | string[], blist: number[] | string[], mode: string): number[] | string[];
    /**
     * Apply a filter of multiple criteria to a record set
     * Eg:
     * {
     *   'field_a': { ':rules': [ { 'sw': 'foo', 'ew': 'bar' }, { 'ew': 'car' }, Function ], ':mode': "OR" },
     *   'field_b': { 'in': [ 123, 456 ] }
     * }
     * @param {Array<Number|String>} ids      - Records IDs to filter
     * @param {FilterType} filter             - Filter definition
     * @returns {Array<Number|String>}        - Matching record IDs
     */
    _applyFilter(ids: Array<number | string>, filter: FilterType): Array<number | string>;
    /**
     * Apply a filter of multiple criteria to a single value (intended for indexed values)
     * Eg:
     * {
     *   'field_a': { rules: [ { 'sw': 'foo', 'ew': 'bar' }, { 'ew': 'car' }, Function ] },
     *   'field_b': { 'in': [ 123, 456 ] }
     * }
     * @param {FilterType} filter     - Filter definition
     * @returns {Boolean}             - If filters apply to value
     */
    _applyIndexFilter(filter: FilterType): boolean;
    /**
     * Check if a given criteria matches a given record
     * @param {Object} value            - Value to check if matching
     * @param {FieldCriteriaType} rule  - Rule to apply to the field
     * @returns {Boolean}               - If criteria matches supplied record
     */
    _applyCriteria(value: any, rule: FieldCriteriaType): boolean;
    /**
     * Sort a list of records by given criteria
     * @private
     * @param {String[]|Number[]} ids       - The list of record IDs to sort
     * @param {SortsType} what              - A list of sort criteria
     * @returns {String[]|Number[]}         - Sorted record IDs
     */
    private _sort;
}
import { BaseModel } from '../model.js';
