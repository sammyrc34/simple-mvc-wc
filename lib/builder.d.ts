export default SimpleBuilder;
declare class SimpleBuilder {
    constructor(app: any);
    app: any;
    render(views: any, query?: {}, target?: any): Promise<void>;
    /**
     * Rebuild and render a view and any sub-views
     */
    reRender(view: any, query?: {}): Promise<void>;
    /**
     * Clear a view from the DOM
     * @param {Object} view                   - The view to build
     * @param {HTMLElement} view.target       - The target element for the view to clear
     * @param {Object[]} currentViews         - All currently rendered views
     */
    clear(view: {
        target: HTMLElement;
    }, currentViews: any[]): void;
    _buildFrom(view: any, target: any, query: any): Promise<void>;
}
