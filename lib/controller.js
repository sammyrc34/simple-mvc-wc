
// 
// Controllers are distinct units for logic implementation.
//

export class SimpleController {


  /**
   * Create a new Controller instance
   * @param {SimpleApp} app - The application instance
   * @returns {SimpleController} controller - The controller instance
   */
  constructor( app ) {
    // Keep a reference to the app
    this.app = app;
  }


  /**
   * Called by a View when attempting to execute an action. 
   * The event and View will be available to interact with the UI, and action name
   * in the event one method is mapped to multiple actions.
   * Handlers can be syncronous or asyncronous.
   * @async
   * @param {Object} [evt] - Any event which precipitated this action
   * @param {Object} view - The view performing the action
   * @param {String} action - The action to perform
   * @param {...*} [args] - Additional arguments for the action
   */
  async doAction( evt, view, action, ...args ) {

    this.app.log( "debug", "Executing action", action, "for view", view.name );

    var handler = this.constructor.actionMap()[ action ];
    if ( handler == undefined )
      return this.app.log( 'error', 'No controller method for action', action );

    try {
      return await this[ handler ]( evt, view, action, ...args );
    }
    catch( err ) {
      this.app.log( 'error', 'Controller failed in action', action, ':', err.name, err.message, err.stack );
    }

  }


  /**
   * Return a map of action names to controller method names
   * @returns {Object} - The action map
   */
  static actionMap() {
    return {};
  }

}

