/**
 * A simple collection class
 * @class SimpleCollection
 * @augments BasicCollection
 * @augments BindableModel
 */
export class SimpleCollection extends BasicCollection {
    constructor(app: any, ...configs: any[]);
}
/**
 * A simple remote collection class
 * @class SimpleRemoteCollection
 * @augments BasicCollection
 * @augments RemoteModel
 * @augments BindableModel
 */
export class SimpleRemoteCollection extends BasicCollection {
    constructor(app: any, ...configs: any[]);
}
/**
 * A partial remote model collection class
 * @class SimpleIntegratedCollection
 * @augments IntegratedModel
 * @augments BasicCollection
 * @augments RemoteModel
 * @augments BindableModel
 */
export class SimpleIntegratedCollection {
    constructor(app: any, ...configs: any[]);
}
/**
 * A simple remote polling collection
 * @class SimplePollingCollection
 * @augments BasicCollection
 * @augments RemoteModel
 * @augments PollingModel
 * @augments BindableModel
 */
export class SimplePollingCollection extends BasicCollection {
    constructor(app: any, ...configs: any[]);
}
/**
 * A simple record
 * @class SimpleRecord
 * @augments BasicRecord
 * @augments BindableModel
 */
export class SimpleRecord extends BasicRecord {
    constructor(app: any, ...configs: any[]);
}
/**
 * A simple remote record class
 * @class SimpleRemoteRecord
 * @augments BasicRecord
 * @augments RemoteModel
 * @augments BindableModel
 */
export class SimpleRemoteRecord extends BasicRecord {
    constructor(app: any, ...configs: any[]);
}
/**
 * A simple remote polling record class
 * @class SimplePollingRecord
 * @augments BasicRecord
 * @augments RemoteModel
 * @augments PollingModel
 * @augments BindableModel
 */
export class SimplePollingRecord extends BasicRecord {
    constructor(app: any, ...configs: any[]);
}
import { BasicCollection } from './models/collection.js';
import { BasicRecord } from './models/record.js';
