
import { render, css, svg, html, nothing } from 'lit';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { SimpleApp } from './index.js';

// 
// Views are defintions of one or more components and or views.
//
// They're static classes, reflecting their stateless nature. Parameters and
// query parameters serve as input.
//

export class SimpleView {

  /**
   * Main view entrypoint, returning the static content in a Lit TemplateResult 
   * @param {Object} [params={}]          - Any URL bind parameters for the view
   * @param {Object} [query={}]           - Any URL query for the request
   * @param {String} [context]            - Any context string provided when registering the view
   * @returns {TemplateResult} result     - The built TemplateResult for rendering
   */
  static build( _0, _1, _2 ) {
  }

  /** 
   * Re-build and render this view in place with the same parameters and query
   */
  static async rebuild() {
    await this.app.router.rebuild( this );
  }

  /**
   * Return our application
   * @type {SimpleApp}        - The application instance
   * @returns {SimpleApp} app - The application instance
   */
  static get app() { return window.$app; }
  get app() { return window.$app; }

  /**
   * Return the Lit renderer used to render content to a container
   * @type {Function}         - Lit render function
   * @returns {Function} func - Lit render function
   */
  static get render() { return render; }
  get render() { return render; }

  /**
   * Fetch the Lit html builder function to build TemplateResult objects from literal HTML
   * @type {Function}         - Lit HTML template builder function
   * @returns {Function} func - Lit HTML template builder function
   */
  static get builder() { return html; }
  get builder() { return html; }

  /**
   * Fetch the Lit CSS builder function to build CSSResult objects from literal CSS
   * @type {Function}         - Lit CSS template builder function
   * @returns {Function} func - Lit CSS template builder function
   */
  static get cssBuilder() { return css; }
  get cssBuilder() { return css; }

  /**
   * Fetch the Lit SVG builder function to build TemplateResult objects from literal SVGs
   * @type {Function}         - Lit SVG literal builder
   * @returns {Function} func - Lit SVG builder function
   */
  static get svgBuilder() { return svg; }
  get svgBuilder() { return svg; }

  /**
   * Return the Lit nothing
   * @type {nothing}            - The lit nothing object
   * @returns {nothing} nothing - The Lit nothing object
   */
  static get nothing() { return nothing };
  get nothing() { return nothing };

  /**
   * Build the provided string as SVG, to be included in template literals.
   * Should not be called with unsanitised input.
   * @param {String} string         - String to interpret as SVG
   * @returns {DirectiveResult} svg - Resulting SVG result
   */
  static includeSvg( string ) { return unsafeSVG( string ); }
  includeSvg( string ) { return unsafeSVG( string ); }

  /**
   * Build the provided string as HTML, to be included in template literals.
   * Should not be called with unsanitised input.
   * @param {String} string          - String to interpret as HTML
   * @returns {DirectiveResult} html - Resulting HTML result
   */
  static includeHtml( string ) { return unsafeHTML( string ); }
  includeHtml( string ) { return unsafeHTML( string ); }

  /**
   * Return the provided input if it is defined, otherwise return the Lit nothing
   * @param {*} thing      - The input to check if defined
   * @returns {*|nothing}  - The provided input, or nothing
   */
  static ifDefined( thing ) { return ( thing === undefined ) ? nothing : thing; }
  ifDefined( thing ) { return ( thing === undefined ) ? nothing : thing; }

  /**
   * Return a handler function for a provided controller action
   * @param {String} name       - The name of the action
   * @param {...*} [args]       - Optional supplementary arguments
   * @returns {Function}        - The function to handle the action
   */
  static action( name, ...args ) {
    return async ( evt ) => { return await this.doAction( evt, name, ...args ) };
  }

  /**
   * Find a controller and perform the action
   * @param {Object} [evt]      - The Event object if this action was fired from an event
   * @param {String} action     - The name of the action
   * @param {...*} [args]       - Optional supplementary arguments
   */
  static async doAction( evt, action, ...args ) {
    this.app.log( 'debug', 'Performing controller action', action );
    // Find a controller who handles this action name
    const controller = this.app.fetchController( this, action );

    if ( controller == undefined )
      return this.app.log( 'error', 'No controller found for action', action );

    return await controller.doAction( evt, this, action, ...args );
  }

  // Title segment
  static title() {
    return undefined;
  }



  /**
   * Disconnect this view, perform any cleanup
   * @param {Object} [params={}]          - Any URL bind parameters for the view
   * @param {Object} [query={}]           - Any URL query data
   */
  static disconnect( _0, _1) {
  }


  /**
   * Connect this view, perform any initialising tasks
   * @param {Object} [params={}]          - Any URL bind parameters for the view
   * @param {Object} [query={}]           - Any URL query data
   */
  static connect() {
  }


  /**
   * Re-connect this view, perform any update tasks
   * @param {Object} [params={}]          - Any URL bind parameters for the view
   * @param {Object} [query={}]           - Any URL query data
   */
  static reconnect() {
  }


  /**
   * How many sub-views will this view accept?
   * @returns {Number} count              - Number of sub-views accepted.
   */
  static slotCount() {
    return 0;
  }


  /**
   * Can this view contain the provided sub-view?
   * @abstract
   * @param {Object} view                 - The view being queried
   * @param {String} view.path            - The path for the queried view
   * @param {Object} view.class           - The class for the queried view
   * @param {String} [view.context]       - Any context name for this view
   * @returns {Boolean}                   - If this view will house the queried view.
   */
  static canContain() {
    return false;
  }


  /**
   * Check if this view can be contained by a provided parent view. Used to allow
   * views to control which parents they accept
   * @abstract
   * @param {Object} view                 - The view being queried
   * @param {String} view.path            - The path for the queried view
   * @param {Object} view.class           - The class for the queried view
   * @param {String} [view.context]       - Any context name for this view
   */
  static canBeContained() {
    return true;
  }


  /**
   * Get target containers for sub-views. Called after the view has rendered.
   * @abstract
   * @param {Object[]} subviews                 - The child views being rendered
   * @param {Object} subviews[].class           - The view class
   * @param {String} subviews[].path            - The path for the view
   * @param {String} [subviews[].context]       - The context name for the view
   * @returns {HTMLElement[]} targets           - The target container element
   */
  static getTargets() {
    throw new Error( "Method getTargets must be defined in views with descendents" );
  }

}


