export default SimpleRouter;
declare class SimpleRouter {
    constructor(app: any);
    app: any;
    _lastViews: any[];
    _lastQuery: {};
    _routeMap: {};
    /**
     * Register a routeable view
     * @param {String} pathpiece          - The view's URL segment
     * @param {Class}  viewclass          - The view's class
     * @param {String} [contextName]      - Context name for URL
     */
    registerView(pathpiece: string, viewclass: Class, contextName?: string): void;
    navigate(overrideUrl: any): Promise<void>;
    /**
     * Attempt to rebuild a provided view
     * @async
     * @throws              - In event of any error
     */
    rebuild(viewClass: any): Promise<void>;
    cleanPath(path: any): any;
    checkHeirarchy(views: any): void;
    pathToViews(path: any): any;
    arrayContains(array1: any, array2: any, start: any): number;
}
