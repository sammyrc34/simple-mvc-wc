export class SimpleComponent extends LitElement {
    /**
     * Register this component to the browser
     * This is used to prevent tree shaking more than anything.
     */
    static register(): void;
    /**
     * Base component properties
     * @returns {Object} - Generic component properties
     */
    static get properties(): any;
    /**
     * Return our application
     * @type {SimpleApp}     - The application instance
     */
    static get app(): SimpleApp;
    /**
     * Fetch the HTML builder for literal HTML
     * @type {html}          - HTML literal builder
     */
    static get builder(): (strings: TemplateStringsArray, ...values: unknown[]) => import("lit-html").TemplateResult<1>;
    /**
     * Fetch the CSS builder for literal CSS
     * @type {css}           - CSS literal builder
     */
    static get cssBuilder(): (strings: TemplateStringsArray, ...values: (number | import("lit").CSSResultGroup)[]) => import("lit").CSSResult;
    /**
     * Fetch the SVG builder for literal SVGs
     * @type {svg}           - SVG literal builder
     */
    static get svgBuilder(): (strings: TemplateStringsArray, ...values: unknown[]) => import("lit-html").TemplateResult<2>;
    /**
     * Include a non-literal SVG
     * @returns {Function} - SVG non-literal builder function
     */
    static includeSvg(...args: any[]): Function;
    /**
     * Include non-literal HTML
     * @returns {Function} - HTML non-literal builder function
     */
    static includeHtml(...args: any[]): Function;
    /**
     * Include an optional attribute if the value is defined
     * @returns {*|nothing}  - The provided value or nothing if undefined
     */
    static ifDefined(thing: any): any | typeof nothing;
    /**
     * Include an optional attribute if the value is truthy
     * @returns {*|nothing}  - The provided value or nothing if undefined
     */
    static ifTruthy(thing: any): any | typeof nothing;
    /**
     * Boilerplate styles getter
     */
    static get styles(): any[];
    events: {};
    uuid: any;
    /**
     * Return our application
     * @type {SimpleApp}     - The application instance
     */
    get app(): SimpleApp;
    /**
     * Fetch the HTML builder for literal HTML
     * @type {html}          - HTML literal builder
     */
    get builder(): (strings: TemplateStringsArray, ...values: unknown[]) => import("lit-html").TemplateResult<1>;
    /**
     * Fetch the CSS builder for literal CSS
     * @type {css}           - CSS literal builder
     */
    get cssBuilder(): (strings: TemplateStringsArray, ...values: (number | import("lit").CSSResultGroup)[]) => import("lit").CSSResult;
    /**
     * Fetch the SVG builder for literal SVGs
     * @type {svg}           - SVG literal builder
     */
    get svgBuilder(): (strings: TemplateStringsArray, ...values: unknown[]) => import("lit-html").TemplateResult<2>;
    /**
     * Include a non-literal SVG
     * @returns {Function} - SVG non-literal builder function
     */
    includeSvg(...args: any[]): Function;
    /**
     * Include non-literal HTML
     * @returns {Function} - HTML non-literal builder function
    */
    includeHtml(...args: any[]): Function;
    /**
     * Include an optional attribute if the value is defined
     * @returns {*|nothing}  - The provided value or nothing if undefined
     */
    ifDefined(thing: any): any | typeof nothing;
    /**
     * Include an optional attribute if the value is truthy
     * @returns {*|nothing}  - The provided value or nothing if undefined
     */
    ifTruthy(thing: any): any | typeof nothing;
    /**
     * Adopt custom styles into this instance
     * @param {CSSResult} additional  - The styles to adopt
     */
    adoptCustomStyles(additional?: CSSResult): void;
}
import { LitElement } from 'lit';
import { SimpleApp } from './index.js';
import { nothing } from 'lit';
