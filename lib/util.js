

/**
 * Static utility function class
 */

export default class Util {

 /**
   * Deep-clone a Map / Set / Array / Function / Object
   * @static
   * @param {Object|Array|Map|Set|Function} thing   - The item to clone
   * @returns {Object|Array}                        - The deep-cloned object or array
   */
  static clone( thing ) {
    let clone;
    if ( typeof thing == "number" || typeof thing == "string" || thing === null || thing === undefined || Number.isNaN( thing ) ) {
      return thing;
    }
    if ( thing instanceof Function ) {
      clone = new Function( 'return' + thing.toString() );
      return clone;
    }
    if ( thing instanceof Map ) {
      clone = new Map();
      for ( let [key,val] of thing )
        clone.set( this.clone( key ), this.clone( val ) );
      return clone;
    }
    if ( thing instanceof Set ) {
      clone = new Set();
      for ( let thing of thing )
        clone.add( this.clone( thing ) );
      return clone;
    }
    if ( Array.isArray( thing ) ) {
      clone = new Array();
      for ( let idx in thing )
        clone[ idx ] = this.clone( thing[ idx ] );
      return clone;
    }
    else if ( typeof thing == "object" ) {
      clone = {};
      for ( let key in thing ) {
        clone[ key ] = this.clone( thing[ key ] );
      }
    }
    return clone;
  };


  /**
   * Deep-merge objects
   * @static
   * @param {Object} dest         - The object to merge into, modified in place
   * @param {...Object} sources   - The objects to merge
   * @throws                      - If attempting to merge incompatible objects
   */
  static deepMerge( dest, ...sources ) {
    while( sources.length ) {
      var tomerge = sources.shift();
      if ( tomerge == undefined )
        continue;
      if ( typeof tomerge !== "object" || tomerge === null || Array.isArray( tomerge ) )
        throw new Error( "Merge source not an object: found a " + typeof tomerge );

      Object.keys( tomerge ).forEach( key => {

        if ( Array.isArray( tomerge[ key ] ) ) {
          if ( dest.hasOwnProperty( key ) && ! Array.isArray( dest[ key ] ) )
            throw new Error( 'Unable to merge array into non-array' );
          else if ( ! dest.hasOwnProperty( key ) )
            dest[ key ] = [];
          dest[ key ] = dest[ key ].concat( this.clone( tomerge[ key ] ) );
          return;
        }

        if ( 
            ! ( tomerge[ key ] instanceof Date ) && 
            typeof tomerge[ key ] === "object" && 
            tomerge[ key ] !== null )
        {
          if ( dest.hasOwnProperty( key ) ) {
            if ( typeof dest[ key ] !== "object" || dest[ key ] === null || Array.isArray( dest[ key ] ) )
              throw new Error( 'Unable to merge object into non-object' );
          }
          else
            dest[ key ] = {};
          this.deepMerge( dest[ key ], tomerge[ key ] );
          return;
        }

        dest[ key ] = tomerge[ key ];
      } );
    }
    return dest;
  }



  /**
   * Convert a regular expression to a partial-match capable regex
   * Adapted from code written by Lucas Trzesniewski, from https://stackoverflow.com/questions/22483214/regex-check-if-input-still-has-chances-to-become-matching/41580048#41580048
   * @param {RegExp} regex  - The regular expression to convert
   * @returns {RegExp}      - Partial match capable regular expression
   * @throws                - In event of failure during conversion
   */
  static partialRegex( regex ) {
    let source, idx = 0, result = "", tmp;

    if ( ! regex || ! regex instanceof RegExp )
      throw new Error( "Not a regular expression" );

    const appendRaw = ( nbChars ) => { 
      result += source.substr( idx, nbChars );
      idx += nbChars;
    };

    const appendOptional = ( nbChars ) => {
      result += "(?:" + source.substr( idx, nbChars ) + "|$)";
      idx += nbChars;
    };

    const process = () => {

      while ( idx < source.length ) {
        switch ( source[idx] ) {
          case "\\":
            switch (source[idx + 1]) {
              case "c":
                appendOptional(3);
                break;
              case "x":
                appendOptional(4);
                break;
              case "u":
                if (re.unicode) {
                  if (source[idx + 2] === "{") {
                    appendOptional( source.indexOf("}", idx) - idx + 1 );
                  } else {
                    appendOptional(6);
                  }
                } else {
                  appendOptional(2);
                }
                break;
              case "p":
              case "P":
                if (re.unicode) {
                  appendOptional(source.indexOf("}", idx) - idx + 1);
                } else {
                  appendOptional(2);
                }
                break;

              case "k":
                appendOptional(source.indexOf(">", idx) - idx + 1);
                break;

              default:
                appendOptional(2);
                break;
            }
            break;
          case "[":
            tmp = /\[(?:\\.|.)*?\]/g;
            tmp.lastIndex = idx;
            tmp = tmp.exec( source );
            appendOptional( tmp[0].length );
            break;
          case "|":
          case "^":
          case "$":
          case "*":
          case "+":
          case "?":
            appendRaw(1);
            break;
          case "{":
            tmp = /\{(\d+),?(\d*)\}/g;
            tmp.lastIndex = idx;
            tmp = tmp.exec(source);
            if (tmp) {
              appendRaw(tmp[0].length);
            }
            else {
              appendOptional(1);
            }
            break;
          case "(":
            if (source[idx + 1] == "?") {
              switch (source[idx + 2]) {
                case ":":
                  result += "(?:";
                  idx += 3;
                  process();
                  result += "|$)";
                  break;
                case "=":
                  result += "(?=";
                  idx += 3;
                  process();
                  result += ")";
                  break;
                case "!":
                  tmp = idx;
                  idx += 3;
                  process();
                  result += source.substr(tmp, idx - tmp);
                  break;
                case "<":
                  switch (source[idx + 3]) {
                    case "=":
                    case "!":
                      tmp = idx;
                      idx += 4;
                      process();
                      result += source.substr(tmp, idx - tmp);
                      break;
                    default:
                      appendRaw(source.indexOf(">", idx) - idx + 1);
                      process();
                      result += "|$)";
                      break;        
                  }
                  break;
              }
            } else {
              appendRaw(1);
              process();
              result += "|$)";
            }
            break;
          case ")":
            ++idx;
            return result;
          default:
            appendOptional(1);
            break;
        }
      }

      return result;
    }

    if ( ! regex.source )
      source = regex;
    else 
      source = regex.source;
    result = process();

    return new RegExp( result, regex.flags);
  }


}

