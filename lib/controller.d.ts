export class SimpleController {
    /**
     * Return a map of action names to controller method names
     * @returns {Object} - The action map
     */
    static actionMap(): any;
    /**
     * Create a new Controller instance
     * @param {SimpleApp} app - The application instance
     * @returns {SimpleController} controller - The controller instance
     */
    constructor(app: SimpleApp);
    app: SimpleApp;
    /**
     * Called by a View when attempting to execute an action.
     * The event and View will be available to interact with the UI, and action name
     * in the event one method is mapped to multiple actions.
     * Handlers can be syncronous or asyncronous.
     * @async
     * @param {Object} [evt] - Any event which precipitated this action
     * @param {Object} view - The view performing the action
     * @param {String} action - The action to perform
     * @param {...*} [args] - Additional arguments for the action
     */
    doAction(evt?: any, view: any, action: string, ...args?: any[]): Promise<any>;
}
