export class SimpleView {
    /**
     * Main view entrypoint, returning the static content in a Lit TemplateResult
     * @param {Object} [params={}]          - Any URL bind parameters for the view
     * @param {Object} [query={}]           - Any URL query for the request
     * @param {String} [context]            - Any context string provided when registering the view
     * @returns {TemplateResult} result     - The built TemplateResult for rendering
     */
    static build(_0: any, _1: any, _2: any): TemplateResult;
    /**
     * Re-build and render this view in place with the same parameters and query
     */
    static rebuild(): Promise<void>;
    /**
     * Return our application
     * @type {SimpleApp}        - The application instance
     * @returns {SimpleApp} app - The application instance
     */
    static get app(): SimpleApp;
    /**
     * Return the Lit renderer used to render content to a container
     * @type {Function}         - Lit render function
     * @returns {Function} func - Lit render function
     */
    static get render(): Function;
    /**
     * Fetch the Lit html builder function to build TemplateResult objects from literal HTML
     * @type {Function}         - Lit HTML template builder function
     * @returns {Function} func - Lit HTML template builder function
     */
    static get builder(): Function;
    /**
     * Fetch the Lit CSS builder function to build CSSResult objects from literal CSS
     * @type {Function}         - Lit CSS template builder function
     * @returns {Function} func - Lit CSS template builder function
     */
    static get cssBuilder(): Function;
    /**
     * Fetch the Lit SVG builder function to build TemplateResult objects from literal SVGs
     * @type {Function}         - Lit SVG literal builder
     * @returns {Function} func - Lit SVG builder function
     */
    static get svgBuilder(): Function;
    /**
     * Return the Lit nothing
     * @type {nothing}            - The lit nothing object
     * @returns {nothing} nothing - The Lit nothing object
     */
    static get nothing(): typeof nothing;
    /**
     * Build the provided string as SVG, to be included in template literals.
     * Should not be called with unsanitised input.
     * @param {String} string         - String to interpret as SVG
     * @returns {DirectiveResult} svg - Resulting SVG result
     */
    static includeSvg(string: string): DirectiveResult;
    /**
     * Build the provided string as HTML, to be included in template literals.
     * Should not be called with unsanitised input.
     * @param {String} string          - String to interpret as HTML
     * @returns {DirectiveResult} html - Resulting HTML result
     */
    static includeHtml(string: string): DirectiveResult;
    /**
     * Return the provided input if it is defined, otherwise return the Lit nothing
     * @param {*} thing      - The input to check if defined
     * @returns {*|nothing}  - The provided input, or nothing
     */
    static ifDefined(thing: any): any | typeof nothing;
    /**
     * Return a handler function for a provided controller action
     * @param {String} name       - The name of the action
     * @param {...*} [args]       - Optional supplementary arguments
     * @returns {Function}        - The function to handle the action
     */
    static action(name: string, ...args?: any[]): Function;
    /**
     * Find a controller and perform the action
     * @param {Object} [evt]      - The Event object if this action was fired from an event
     * @param {String} action     - The name of the action
     * @param {...*} [args]       - Optional supplementary arguments
     */
    static doAction(evt?: any, action: string, ...args?: any[]): Promise<any>;
    static title(): any;
    /**
     * Disconnect this view, perform any cleanup
     * @param {Object} [params={}]          - Any URL bind parameters for the view
     * @param {Object} [query={}]           - Any URL query data
     */
    static disconnect(_0: any, _1: any): void;
    /**
     * Connect this view, perform any initialising tasks
     * @param {Object} [params={}]          - Any URL bind parameters for the view
     * @param {Object} [query={}]           - Any URL query data
     */
    static connect(): void;
    /**
     * Re-connect this view, perform any update tasks
     * @param {Object} [params={}]          - Any URL bind parameters for the view
     * @param {Object} [query={}]           - Any URL query data
     */
    static reconnect(): void;
    /**
     * How many sub-views will this view accept?
     * @returns {Number} count              - Number of sub-views accepted.
     */
    static slotCount(): number;
    /**
     * Can this view contain the provided sub-view?
     * @abstract
     * @param {Object} view                 - The view being queried
     * @param {String} view.path            - The path for the queried view
     * @param {Object} view.class           - The class for the queried view
     * @param {String} [view.context]       - Any context name for this view
     * @returns {Boolean}                   - If this view will house the queried view.
     */
    static canContain(): boolean;
    /**
     * Check if this view can be contained by a provided parent view. Used to allow
     * views to control which parents they accept
     * @abstract
     * @param {Object} view                 - The view being queried
     * @param {String} view.path            - The path for the queried view
     * @param {Object} view.class           - The class for the queried view
     * @param {String} [view.context]       - Any context name for this view
     */
    static canBeContained(): boolean;
    /**
     * Get target containers for sub-views. Called after the view has rendered.
     * @abstract
     * @param {Object[]} subviews                 - The child views being rendered
     * @param {Object} subviews[].class           - The view class
     * @param {String} subviews[].path            - The path for the view
     * @param {String} [subviews[].context]       - The context name for the view
     * @returns {HTMLElement[]} targets           - The target container element
     */
    static getTargets(): HTMLElement[];
    get app(): any;
    get render(): {
        (value: unknown, container: HTMLElement | DocumentFragment, options?: import("lit-html").RenderOptions): import("lit-html").RootPart;
        setSanitizer: (newSanitizer: import("lit-html").SanitizerFactory) => void;
        createSanitizer: import("lit-html").SanitizerFactory;
        _testOnlyClearSanitizerFactoryDoNotCallOrElse: () => void;
    };
    get builder(): (strings: TemplateStringsArray, ...values: unknown[]) => import("lit-html").TemplateResult<1>;
    get cssBuilder(): (strings: TemplateStringsArray, ...values: (number | import("lit").CSSResultGroup)[]) => import("lit").CSSResult;
    get svgBuilder(): (strings: TemplateStringsArray, ...values: unknown[]) => import("lit-html").TemplateResult<2>;
    get nothing(): symbol;
    includeSvg(string: any): import("lit-html/directive.js").DirectiveResult<typeof import("lit-html/directives/unsafe-svg.js").UnsafeSVGDirective>;
    includeHtml(string: any): import("lit-html/directive.js").DirectiveResult<typeof import("lit-html/directives/unsafe-html.js").UnsafeHTMLDirective>;
    ifDefined(thing: any): any;
}
import { SimpleApp } from './index.js';
import { nothing } from 'lit';
