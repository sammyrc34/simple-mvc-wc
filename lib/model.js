
import { v4 as uuidv4 } from 'uuid';

/**
 * Base model class for common functionality
 */
class BaseModel {

  /**
   * Constructor for a basic model
   * @param {Object} app                    - The main application object.
   * @param {Object[]} configs              - Configuration options
   * @param {Object} [configs.conf]         - One configuration object.
   * @param {String} [conf.idField="id"]    - Set to indicate the field containing the primary record identifier field.
   * @param {String[]} [conf.optFields]     - A set list of optional fields to use. Uses the record as is if no optional or required fields are provided.
   * @param {String[]} [conf.reqFields]     - A set list of required fields to use. Uses the record as is if no optional or required fields are provided.
   * @param {Object} [defaultValues]        - Default values for added records
   */
  constructor( app, ...configs ) {
    const defaults = {
      idField: 'id',
      optFields: [],
      reqFields: [],
      defaultValues: {}
    };

    // Set configuration
    this.config = app.util.deepMerge( {}, defaults, ...configs );

    // Give idField a default
    if ( ! this.config.defaultValues.hasOwnProperty( this.config.idField ) )
      this.config.defaultValues[ this.config.idField ] = '';

    this.uuid = uuidv4();

    // Keep a reference to the app
    this.app = app;
  }


  /**
   * Setup anything additional required, or perform post-instance work
   */
  setup() {
  }

 
  /** 
   * Get a default (blank) record
   * @returns {Object}                - A blank record object
   */
  defaultRecord() {
    const record = Object.assign( {}, this.config.defaultValues );
    this.resolve( record );
    return record;
  }


  /**
   * Resolve any functions within the provided object, mutating the object
   * @param {Object} record       - The object to resolve functions within
   */
  resolve( record ) {
    Object.keys( record ).forEach( key => {
      if ( typeof record[ key ] === "object" && record[ key ] !== null )
        this.resolve( record[ key ] );
      else if ( typeof record[ key ] === "function" )
        record[ key ] = record[ key ]( this );
    } );
  }



  /**
   * Pick desired fields from a data object.
   * @param {Object} record     - The data record to pick from.
   * @param {String[]} fields   - Alternative fields to pick, otherwise picks all optional and required fields
   * @returns {Object}          - The resulting data record in a new object
   */
  pick( record, fields=[] ) {
    var pickFields = [];
    if ( fields.length == 0 && this.config.reqFields.length == 0 && this.config.optFields.length == 0 )
      pickFields = Object.keys( record ).filter( k => ! k.startsWith( '_' ) );
    else if ( fields.length == 0  )
      pickFields = this.optionalFields.concat( this.requiredFields );
    else
      pickFields = fields;
    return pickFields.reduce((a, e) => (a[e] = record[e], a), {}); 
  }


  /**
   * Get a list of all fields ( optional and required )
   * @returns {String[]}        - The list of fields known
   */
  get allFields() {
    return this.requiredFields.concat( this.optionalFields );
  }

  /**
   * Getter helper for model consumers to get the ID field
   * @returns {String}          - The field containing the record's ID
   */
  get idField() {
    return this.config.idField;
  }


  /**
   * Get a list a required fields
   * @returns {String[]}        - A list of required fields
   */
  get requiredFields() {
    return this.config.reqFields.concat( this.idField );
  }

  /** 
   * Get a list of optional fields
   * @returns {String[]}        - A list of optional fields
   */
  get optionalFields() {
    return this.config.optFields;
  }


  /**
   * Has this model got the given field?
   * @param {String}              - The field to check
   * @return {Boolean|undefined}  - Undefined if no fields configured, otherwise boolean if field is known 
   */
  hasField( field ) {
    if ( this.config.reqFields.length == 0 && this.config.optFields.length == 0 )
      return undefined;
    else 
      return this.allFields.includes( field );
  }

}


export { BaseModel };


