/**
 * Base model class for common functionality
 */
export class BaseModel {
    /**
     * Constructor for a basic model
     * @param {Object} app                    - The main application object.
     * @param {Object[]} configs              - Configuration options
     * @param {Object} [configs.conf]         - One configuration object.
     * @param {String} [conf.idField="id"]    - Set to indicate the field containing the primary record identifier field.
     * @param {String[]} [conf.optFields]     - A set list of optional fields to use. Uses the record as is if no optional or required fields are provided.
     * @param {String[]} [conf.reqFields]     - A set list of required fields to use. Uses the record as is if no optional or required fields are provided.
     * @param {Object} [defaultValues]        - Default values for added records
     */
    constructor(app: any, ...configs: {
        conf?: any;
    });
    config: any;
    uuid: any;
    app: any;
    /**
     * Setup anything additional required, or perform post-instance work
     */
    setup(): void;
    /**
     * Get a default (blank) record
     * @returns {Object}                - A blank record object
     */
    defaultRecord(): any;
    /**
     * Resolve any functions within the provided object, mutating the object
     * @param {Object} record       - The object to resolve functions within
     */
    resolve(record: any): void;
    /**
     * Pick desired fields from a data object.
     * @param {Object} record     - The data record to pick from.
     * @param {String[]} fields   - Alternative fields to pick, otherwise picks all optional and required fields
     * @returns {Object}          - The resulting data record in a new object
     */
    pick(record: any, fields?: string[]): any;
    /**
     * Get a list of all fields ( optional and required )
     * @returns {String[]}        - The list of fields known
     */
    get allFields(): string[];
    /**
     * Getter helper for model consumers to get the ID field
     * @returns {String}          - The field containing the record's ID
     */
    get idField(): string;
    /**
     * Get a list a required fields
     * @returns {String[]}        - A list of required fields
     */
    get requiredFields(): string[];
    /**
     * Get a list of optional fields
     * @returns {String[]}        - A list of optional fields
     */
    get optionalFields(): string[];
    /**
     * Has this model got the given field?
     * @param {String}              - The field to check
     * @return {Boolean|undefined}  - Undefined if no fields configured, otherwise boolean if field is known
     */
    hasField(field: any): boolean | undefined;
}
