

/**
 * Model composition & exporting class
 */

import { BindableModel } from './models/bindable.js';
import { PollingModel } from './models/polling.js';
import { RemoteModel } from './models/remote.js';
import { BasicRecord } from './models/record.js';
import { BasicCollection } from './models/collection.js';
import { IntegratedModel } from './models/integrated.js';


/** 
 * A simple collection class
 * @class SimpleCollection
 * @augments BasicCollection
 * @augments BindableModel
 */
export class SimpleCollection extends BindableModel( BasicCollection ) {
  constructor( app, ...configs ) { super( app, ...configs ); }
}

/**
 * A simple remote collection class
 * @class SimpleRemoteCollection
 * @augments BasicCollection
 * @augments RemoteModel
 * @augments BindableModel
 */
export class SimpleRemoteCollection extends BindableModel( RemoteModel( BasicCollection ) ) {
  constructor( app, ...configs ) { super( app, ...configs ); }
}

/**
 * A partial remote model collection class
 * @class SimpleIntegratedCollection
 * @augments IntegratedModel
 * @augments BasicCollection
 * @augments RemoteModel
 * @augments BindableModel
 */
export class SimpleIntegratedCollection extends BindableModel( IntegratedModel( RemoteModel( BasicCollection ) ) ) {
  constructor( app, ...configs ) { super( app, ...configs ); }
}

/**
 * A simple remote polling collection
 * @class SimplePollingCollection
 * @augments BasicCollection
 * @augments RemoteModel
 * @augments PollingModel
 * @augments BindableModel
 */
export class SimplePollingCollection extends BindableModel( PollingModel( RemoteModel( BasicCollection ) ) ) {
  constructor( app, ...configs ) { super( app, ...configs ); }
}

/**
 * A simple record
 * @class SimpleRecord
 * @augments BasicRecord
 * @augments BindableModel
 */
export class SimpleRecord extends BindableModel( BasicRecord ) {
  constructor( app, ...configs ) { super( app, ...configs ); }
}

/**
 * A simple remote record class
 * @class SimpleRemoteRecord
 * @augments BasicRecord
 * @augments RemoteModel
 * @augments BindableModel
 */
export class SimpleRemoteRecord extends BindableModel( RemoteModel( BasicRecord ) ) {
  constructor( app, ...configs ) { super( app, ...configs ); }
}

/**
 * A simple remote polling record class
 * @class SimplePollingRecord
 * @augments BasicRecord
 * @augments RemoteModel
 * @augments PollingModel
 * @augments BindableModel
 */
export class SimplePollingRecord extends BindableModel( PollingModel( RemoteModel( BasicRecord ) ) ) {
  constructor( app, ...configs ) { super( app, ...configs ); }
}


