
import { LitElement, adoptStyles, css, svg, html, nothing } from 'lit';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';
import { v4 as uuidv4 } from 'uuid';

import { SimpleApp } from './index.js';


const registered = {};


class SimpleComponent extends LitElement {

  constructor() {
    super();

    // import defaults
    for ( let prop in this.constructor.properties ) {
      if ( this.constructor.properties[ prop ].hasOwnProperty( 'def' ) )
        this[ prop ] = this.constructor.properties[ prop ].def;
    }

    this.events = {};
    this.uuid = uuidv4();
  }

  /**
   * Register this component to the browser
   * This is used to prevent tree shaking more than anything.
   */
  static register() {
    const elementName = this._elementName;
    if ( ! registered.hasOwnProperty( elementName ) && customElements.get( elementName ) == undefined ) {
      customElements.define( elementName, this );
      registered[ elementName ] = true;
    }
  }

  /**
   * Base component properties
   * @returns {Object} - Generic component properties
   */
  static get properties() {
    return { 
      'uuid':   { reflect: false, type: String }
    }
  }


  /**
   * Create the render root for the instance and apply all styles
   */
  createRenderRoot() {
    const customStyles = this.app.fetchStyles( this.constructor._elementName );
    let allStyles = this.constructor.styles;
    let finalisedStyles;
    if ( customStyles )
      allStyles = allStyles.concat( customStyles );
    finalisedStyles = this.constructor.finalizeStyles( allStyles );
    this.constructor.elementStyles = finalisedStyles;
    return super.createRenderRoot();
  }


  /**
   * Return our application
   * @type {SimpleApp}     - The application instance
   */
  static get app() { return window.$app; }

  /**
   * Return our application
   * @type {SimpleApp}     - The application instance
   */
  get app() { return window.$app; }

  /**
   * Fetch the HTML builder for literal HTML
   * @type {html}          - HTML literal builder
   */
  static get builder() { return html; }

  /**
   * Fetch the HTML builder for literal HTML
   * @type {html}          - HTML literal builder
   */
  get builder() { return html; }

  /**
   * Fetch the CSS builder for literal CSS
   * @type {css}           - CSS literal builder
   */
  static get cssBuilder() { return css; }

  /**
   * Fetch the CSS builder for literal CSS
   * @type {css}           - CSS literal builder
   */
  get cssBuilder() { return css; }

  /**
   * Fetch the SVG builder for literal SVGs
   * @type {svg}           - SVG literal builder
   */
  static get svgBuilder() { return svg; }

  /**
   * Fetch the SVG builder for literal SVGs
   * @type {svg}           - SVG literal builder
   */
  get svgBuilder() { return svg; }

  /**
   * Include a non-literal SVG 
   * @returns {Function} - SVG non-literal builder function
   */
  static includeSvg( ...args ) { return unsafeSVG( ...args ); }

  /**
   * Include a non-literal SVG 
   * @returns {Function} - SVG non-literal builder function
   */
  includeSvg( ...args ) { return unsafeSVG( ...args ); }

  /**
   * Include non-literal HTML
   * @returns {Function} - HTML non-literal builder function
   */
  static includeHtml( ...args ) { return unsafeHTML( ...args ); }

  /**
   * Include non-literal HTML
   * @returns {Function} - HTML non-literal builder function
  */
  includeHtml( ...args ) { return unsafeHTML( ...args ); }

  /**
   * Include an optional attribute if the value is defined
   * @returns {*|nothing}  - The provided value or nothing if undefined
   */
  static ifDefined( thing ) { return ( thing === undefined ) ? nothing : thing; }

  /**
   * Include an optional attribute if the value is defined
   * @returns {*|nothing}  - The provided value or nothing if undefined
   */
  ifDefined( thing ) { return ( thing === undefined ) ? nothing : thing; }


  /**
   * Include an optional attribute if the value is truthy
   * @returns {*|nothing}  - The provided value or nothing if undefined
   */
  static ifTruthy( thing ) { return ( thing ) ? thing : nothing; }

  /**
   * Include an optional attribute if the value is truthy
   * @returns {*|nothing}  - The provided value or nothing if undefined
   */
  ifTruthy( thing ) { return ( thing ) ? thing : nothing; }



  /** 
   * Boilerplate styles getter
   */
  static get styles() {
    return [];
  }

  /**
   * Adopt custom styles into this instance
   * @param {CSSResult} additional  - The styles to adopt
   */
  adoptCustomStyles( additional=[] ) {
    const customStyles = this.app.fetchStyles( this.constructor._elementName );

    if ( ! Array.isArray( additional ) )
      additional = [ additional ];
    else
      additional = Array.from( additional );
    if ( customStyles.length )
      additional = additional.concat( customStyles );

    adoptStyles( this.shadowRoot ? this.shadowRoot : this.renderRoot, this.constructor.styles.concat( additional ) );
  }


  /**
   * Execute any functionality needed upon rendering. Call superclass method if overriding.
   */
  connectedCallback() {
    super.connectedCallback();
  }


  /**
   * Execute any disconnect functionality. Call superclass if overriding.
   */
  disconnectedCallback() {
    super.disconnectedCallback();
  }

}


export { SimpleComponent };

