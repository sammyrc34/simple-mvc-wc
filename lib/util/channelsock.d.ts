/**
 * A ws based websocket server supporting multiple channels for use with SimplePartialModel
 *
 * Options to the constructor are provided to the WebSocketServer. If authentication
 * or other session management is needed, recommend utilising an external HTTP(s)
 * server to manage authentication during the connection upgrade.
 *
 * Channels are URI like, can contain placeholders like /customers/:cust allowing
 * clients to subscribe to /customers/123 for example.
 *
 * Utilise events to handle other authentication needs.
 *
 * @extends EventEmitter
 * @class
 */
export class ChannelSock extends EventEmitter {
    /**
     * Client error event
     * @event ChannelSock#clienterror
     * @type {Error}          - The causative error
     */
    /**
     * Server error event. Must be listened to or will exit the Node process
     * @event ChannelSock#error
     * @type {Error}          - The causative error
     */
    /**
     * Event before client connection completion. Throw in handler to reject the connection.
     * @event ChannelSock#connection
     * @param {http.IncomingMessage} request  - The connection HTTP request
     * @param {WebSocket} sock                - The WebSocket for the connection
     */
    /**
     * Event after a client has connected.
     * @event ChannelSock#connected
     * @param {http.IncomingMessage} request  - The connection HTTP request
     * @param {WebSocket} sock                - The WebSocket for the connection
     */
    /**
     * Event before client subscribe attempt. Throw in handler to reject the subscribe.
     * @event ChannelSock#subscribe
     * @param {String} channel - The channel for the subscribe
     * @param {String} uuid    - The socket UUID for the subscribe
     * @param {WebSocket} sock - The WebSocket for the subscribe
     */
    /**
     * Event after client subscribe
     * @event ChannelSock#subscribed
     * @param {String} channel - The channel for the subscribe
     * @param {String} uuid    - The socket UUID for the subscribe
     * @param {WebSocket} sock - The WebSocket for the subscribe
     */
    /**
     * Event after client unsubscribe
     * @event ChannelSock#unsubscribed
     * @param {String} channel - The channel for the subscribe
     * @param {String} uuid    - The socket UUID for the subscribe
     * @param {WebSocket} sock - The WebSocket for the subscribe
     */
    /** @typedef {Object} CSOpts
     * @property {Number}    [opts.maxClients=0]          - Set a client limit, 0 to disable
     * @property {Number}    [opts.subscribeLimit=100]    - Channel limit per client, set to something responsible
     * @property {Boolean}   [opts.clientCheck=true]      - Check client connections periodically
     * @property {String[]}  [opts.channels=[]]           - List of defined channels
     * @property {Function}  [opts.identFunc]             - Callback to return a UUID for the client, called with HTTP's IncomingMessage
     */
    /**
     * Build a new ChannelSock instance
     * @param {CSOpts}    opts                            - ChannelSock options
     * @param {Object}    wsOpts                          - WebSocketServer options, see ws documentation
     */
    constructor(opts: {
        /**
         * - Set a client limit, 0 to disable
         */
        maxClients?: number;
        /**
         * - Channel limit per client, set to something responsible
         */
        subscribeLimit?: number;
        /**
         * - Check client connections periodically
         */
        clientCheck?: boolean;
        /**
         * - List of defined channels
         */
        channels?: string[];
        /**
         * - Callback to return a UUID for the client, called with HTTP's IncomingMessage
         */
        identFunc?: Function;
    }, wsOpts: any);
    _conf: {
        channels: any[];
        maxClients: number;
        subscribeLimit: number;
        clientCheck: boolean;
    } & {
        /**
         * - Set a client limit, 0 to disable
         */
        maxClients?: number;
        /**
         * - Channel limit per client, set to something responsible
         */
        subscribeLimit?: number;
        /**
         * - Check client connections periodically
         */
        clientCheck?: boolean;
        /**
         * - List of defined channels
         */
        channels?: string[];
        /**
         * - Callback to return a UUID for the client, called with HTTP's IncomingMessage
         */
        identFunc?: Function;
    };
    _channels: Map<any, any>;
    _clients: Map<any, any>;
    _checkTimer: NodeJS.Timeout;
    _ws: any;
    /**
     * Handle an upgrade to WebSockets, call if utilising own HTTP(s) server
     * @public
     * @param {http.IncomingMessage} request        - The HTTP(s) request
     * @param {stream.Duplex} socket                - Network socket object
     * @param {Buffer} head                         - The first packet of the upgraded stream
     */
    public handleUpgrade(request: http.IncomingMessage, socket: stream.Duplex, head: Buffer): void;
    /**
     * Define a new channel
     * @public
     * @param {String} path   - New channel path
     */
    public addChannel(path: string): void;
    /**
     * Remove an existing channel
     * @public
     * @param {String} path   - Channel path to remove
     */
    public removeChannel(path: string): void;
    /**
     * Emit a record change message for a given channel
     * @public
     * @param {String} channel              - The channel for the notification
     * @param {String} type                 - Change type, one of update, delete, or insert
     * @param {Array<String|Number>} [ids]  - Array of IDs for the notification if available
     * @throws                              - If unable to notify a change
     */
    public notifyChange(channel: string, type: string, ids?: Array<string | number>): void;
    /**
     * Emit one or more new or updated records for a given channel
     * @public
     * @param {String} channel      - Channel path to notify
     * @param {Object[]} records    - Updated records
     */
    public notifyRecords(channel: string, records: any[]): void;
    /**
     * Notify clients with a manual message for a given channel
     * @public
     * @param {String} channel    - Channel path to notify
     * @param {String} message    - The JSON string message to send
     */
    public notifyManual(channel: string, message: string): void;
    /**
     * Broadcast a manual message to all connected clients
     * @public
     * @param {String} message    - The JSON string message to broadcast
     */
    public broadcastManual(message: string): void;
    /**
     * Check all clients are active
     */
    _checkClients(): void;
    /**
     * Find a matching channel with placeholders
     * @private
     * @param {String} channel    - Channel to match
     * @returns {String|null}     - Channel name if found, otherwise null
     */
    private _findPlaceholder;
    /**
     * Send a message to a Map of clients
     * @private
     * @param {Map|Set} clients   - Set or map of clients to notify
     * @param {String} message    - Message to dispatch
     */
    private _notifyClients;
    /**
     * Close a client connection and remove subscriptions
     * @private
     * @param {String} uuid     - The UUID of client
     */
    private _closeClient;
    /**
     * Connect WS server events
     * @private
     */
    private _connectEvents;
    /**
     * Handle a new connection, connect events and set connection identifier
     * @private
     * @param {WebSocket} sock        - The connection socket
     * @param {http.IncomingMessage}  - The HTTP request
     */
    private _handleConnection;
    /**
     * Emit an error that occurred on the WebSocketServer
     * @private
     * @param {Error} err         - The error raised
     * @fires ChannelSock#error
     */
    private _handleError;
    /**
     * Emit an error that occurred with a client and optionally close the connection
     * @private
     * @param {WebSocket} sock    - WebSocket experiencing the error
     * @param {Boolean} term      - If the connection should be terminated
     * @param {Error} err         - The error raised
     * @fires ChannelSock#clienterror
     */
    private _handleClientError;
    /**
     * Handle a ping response from a client
     * @param {WebSocket} sock    - The WebSocket for the client
     */
    _handlePong(sock: WebSocket): void;
    /**
     * Handle a message from a client
     * @private
     * @param {WebSocket} sock    - The WebSocket for the client
     * @param {Buffer} data       - The received message in a buffer
     */
    private _handleMessage;
    /**
     * Handle a subscribe request
     * @private
     * @param {WebSocket} sock    - The WebSocket for the client
     * @param {Object} message    - The Object message received
     */
    private _handleSubscribe;
    /**
     * Handle an unsubscribe request
     * @private
     * @param {WebSocket} sock    - The WebSocket for the client
     * @param {Object} message    - The Object message received
     */
    private _handleUnsubscribe;
    /**
     * Unsubscribe clients from a given channel
     */
    _unsubscribeClients(channel: any): void;
    /**
     * Notify a subscriber of unsubscription
     * @private
     * @param {String} channel      - The channel to be unsubscribed
     * @param {String} uuid         - The client uuid
     */
    private _notifyUnsubscribe;
}
import { EventEmitter } from 'node:events';
