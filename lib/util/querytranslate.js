

/**
 * Utility class to assist with smart model filtering and sorting
 * @class
 */
class QueryTranslate {

  /**
   * Return a new query translation instance
   * @see BasicCollection#FiltersCollection
   * @see BasicCollection#SortsType
   * @param {String} language           - Intended output language, sqlite, oracle, mysql
   * @param {FiltersCollection} filters - Filters
   * @param {SortsType} sorts           - Sorts
   * @constructor
   */
  constructor( language, filters, sorts=[] ) {
    this._filters = filters;
    this._sorts = sorts;
    this._lang = language;

    this._binds = [];

    this._check();

    return this;
  }




  /**
   * Return a filter clause
   * Note, do not rely on filters to provide security or limit access of rows.
   * @param {Object} columnMap  - Map of filter or sort fields to DB column names
   * @param {String} tableAlias - Table alias
   * @returns {String}          - The filter clause, or match-all clause if no filters are provided
   */
  getFilter( columnMap={}, tableAlias="" ) {
    const filters = this._filters;
    let rules, mode;

    this._alias = tableAlias;
    this._colmap = columnMap;

    // Clear binds
    this._binds = [];

    rules = filters[ ':rules' ];
    mode = filters[ ':mode' ];

    // No filters?
    if ( ! rules )
      return '1=1';

    return this._buildFilters( rules, mode );
  }


  /**
   * Return the statement binds list for the filter clause
   * @returns {Array<String|Number>}  - Bound values
   */
  getBinds() {
    return this._binds;
  }


  /**
   * Return an order by clause or empty string without sorts
   * @param {Object} columnMap  - Map of filter or sort fields to DB column names
   * @param {String} tableAlias - Table alias
   * @returns {String}          - Order by clause
   */
  getSort( columnMap={}, tableAlias="" ) {
    const sorts = [];
    let column, dir, alias = "";

    if ( tableAlias )
      alias = tableAlias + ".";

    for ( let crit of this._sorts ) {
      column = alias + ( columnMap[ crit.field ] || crit.field );
      dir = ( crit[ 'direction' ] == "desc" ) ? "DESC" : "ASC";
      sorts.push( `${column} ${dir}` );
    }

    if ( ! sorts.length )
      return '';

    return `ORDER BY ${sorts.join( ', ' )}`;
  }



  /**
   * Ensure objects as expected
   */
  _check() {
    if ( typeof this._filters !== "object" )
      throw new Error( `Unexpected filters found` );

    if ( ! Array.isArray( this._sorts ) )
      throw new Error( `Unexpected sorts found` );

    if ( this._lang !== "oracle" && this._lang !== "sqlite" && this._lang !== "mysql" )
      throw new Error( `Unsupported language ${this._lang}` );
  }


  /*
   * Recursively construct a filter clause
   * @param {FilterList} rules      - List of rules to construct from
   * @param {String} [mode="AND"]   - Mode of combining multiple rules, AND or OR
   * @returns {String}              - Filter clause
   */
  _buildFilters( rules, mode="AND" ) {
    let sub, subs = [], submode, stmt = '';

    for ( let rule of rules ) {

      if ( typeof rule !== "object" || Array.isArray( rule ) )
        throw new Error( `Unsupported rule type '${typeof rule}' found` );

      if ( mode !== "AND" && mode !== "OR" )
        throw new Error( `Unsupported mode ${mode}, expected AND or OR` );

      for ( let field in rule ) {

        if ( field == ":mode" )
          continue;

        if ( field == ":rules" ) {
          sub = this._buildFilters( rule[ field ], rule[ ':mode' ] )
          stmt += stmt ? ` ${mode} ( ${sub} )` : `( ${sub} )`;
          continue;
        }

        if ( typeof rule[ field ] !== "object" )
          throw new Error( `Unsupported field rule type ${typeof rule[field]} found` );

        if ( rule[ field ].hasOwnProperty( ':rules' ) ) {
          subs = [];
          submode = rule[ field ][ ':mode' ];

          if ( submode !== "AND" && submode !== "OR" )
            throw new Error( `Unsupported mode ${submode}, expected AND or OR` );

          for ( let subrule of rule[ field ][ ':rules' ] ) {
            subs.push( this._buildCriteria( field, subrule, submode ) );
          }
          stmt += stmt ? ` ${mode} ` : '';
          stmt += ` ( ${subs.join( ' ' + submode + ' ' )} )`;
        }
        else {
          submode == "AND";
          sub = this._buildCriteria( field, rule[ field ], submode );
          if ( ! sub )
            continue;
          stmt += stmt ? ` ${mode} ${sub}` : `${sub}`;
        }
        continue;
      }
    }

    return stmt;
  }


  /**
   * Return a string
   * @param {String} field      - Filter field name
   * @param {CriteriaType} rule - Criteria rule
   * @param {String} [mode=AND] - Criteria mode, OR or AND
   * @returns {String}          - SQL constraint fragment
   */
  _buildCriteria( field, rule, mode="AND" ) {
    const colmap = this._colmap;
    const clauses = [];
    let alias = this._alias;
    let colname, matchcase = true;

    if ( alias )
      alias = alias + '.';

    if ( rule.hasOwnProperty( ':case' ) )
      matchcase = rule[ ':case' ];

    // Ensure field name is alphanumeric only
    if ( null == field.match( /^[a-zA-Z_][a-zA-Z0-9_]*$/ ) )
      throw new Error( `Unsupported non-alphanumeric field name` );

    colname = alias + ( colmap[ field ] || field );

    for ( let crit in rule ) {

      if ( crit.startsWith( ':' ) )
        continue;

      clauses.push( this._buildOneCriteria( colname, crit, rule[ crit ], matchcase ) );
    }

    return clauses.join( `_ ${mode} _` );
  }


  /**
   * Build one filter statement
   * @param {String} colname      - The name of the database column
   * @param {String} operator     - The operator name
   * @param {String|String[]} val - The value or values to filter with
   * @param {Boolean} matchcase   - If the string match is case sensitive
   */
  _buildOneCriteria( colname, operator, val, matchcase ) {
    const lang = this._lang;
    let tmp, bindnum = 0;

    const bindvar = () => {
      if ( this._lang == "sqlite" )
        return '?';
      else if ( this._lang == "oracle" )
        return ':' + bindnum++;
    }

    switch( operator ) {
      case "in":
        tmp = [];
        if ( ! Array.isArray( val ) )
          throw new Error( `Expecting array value for IN operator` );
        for ( let v of val ) {
          this._binds.push( v );
          tmp.push( bindvar() );
        }
        return `${colname} IN ( ${tmp.join(',')} )`;
      case "eq":
        if ( matchcase ) {
          this._binds.push( val );
          return `${colname} = ${bindvar()}`;
        }
        else {
          this._binds.push( val + "" ); // Ensure numbers are strings
          return `UPPER( ${colname} ) = UPPER( ${bindvar()} )`;
        }
      case "ct":
        this._binds.push( val );
        if ( matchcase ) {
          if ( lang == "oracle" )
            return `${colname} LIKE '%' || ${bindvar()} || '%'`;
          if ( lang == "mysql" )
            return `${colname} LIKE BINARY '%' || ${bindvar()} || '%'`;
          if ( lang == "sqlite" )
            return `${colname} GLOB '*' || ${bindvar()} || '*'`;
        }
        else {
          if ( lang == "oracle" )
            return `UPPER( ${colname} ) LIKE UPPER( '%' || ${bindvar()} || '%')`
          if ( lang == "mysql" || lang == "sqlite" )
            return `${colname} LIKE '%' || ${bindvar()} || '%'`;
        }
      case "sw":
        this._binds.push( val );
        if ( matchcase ) {
          if ( lang == "oracle" )
            return `${colname} LIKE ${bindvar()} || '%'`;
          if ( lang == "mysql" )
            return `${colname} LIKE BINARY ${bindvar()} || '%'`;
          if ( lang == "sqlite" )
            return `${colname} GLOB ${bindvar()} || '*'`;
        }
        else {
          if ( lang == "oracle" )
            return `UPPER( ${colname} ) LIKE UPPER( ${bindvar()} || '%')`
          if ( lang == "mysql" || lang == "sqlite" )
            return `${colname} LIKE ${bindvar()} || '%'`;
        }
      case "ew":
        this._binds.push( val );
        if ( matchcase ) {
          if ( lang == "oracle" )
            return `${colname} LIKE '%' || ${bindvar()}`;
          if ( lang == "mysql" )
            return `${colname} LIKE BINARY '%' || ${bindvar()}`;
          if ( lang == "sqlite" )
            return `${colname} GLOB '*' || ${bindvar()}`;
        }
        else {
          if ( lang == "oracle" )
            return `UPPER( ${colname} ) LIKE UPPER( '%' || ${bindvar()})`
          if ( lang == "mysql" || lang == "sqlite" )
            return `${colname} LIKE '%' || ${bindvar()}`;
        }
      case "gt":
        this._binds.push( val );
        return `${colname} > ${bindvar()}`;
      case "ge":
        this._binds.push( val );
        return `${colname} >= ${bindvar()}`;
      case "lt":
        this._binds.push( val );
        return `${colname} < ${bindvar()}`;
      case "le":
        this._binds.push( val );
        return `${colname} <= ${bindvar()}`;
    }
  }
}

export default QueryTranslate;

