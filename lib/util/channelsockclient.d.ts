/**
 * Channel WebSocket Client for the ChannelSock server service
 *
 * Provides a client interface for an aggregated WebSocket connection to a single
 * endpoint making one or more channel subscriptions.
 */
export class ChannelSockClient {
    /**
     * Create a client interface instance
     * @constructor
     * @param {String} endpoint   - Endpoint URL to connect to
     */
    constructor(app: any, endpoint: string);
    app: any;
    _endpoint: string;
    _sock: WebSocket;
    _connProm: Promise<any>;
    _models: WeakMap<object, any>;
    _subs: Map<any, any>;
    _subProms: Set<any>;
    _unsubProms: Set<any>;
    _termProm: Map<any, any>;
    _terminating: boolean;
    _connTimer: any;
    _connDelay: number;
    _failCount: number;
    /**
     * Connect to remote endpoint as needed
     * @async
     * @throws      - In event of connection failure
     */
    connect(): Promise<any>;
    /**
     * Proxy ws connection state method
     * @type {Number}   - Connection state identifier, 0 = connecting, 1 = open, 2 = closing, 3 = closed
     */
    get readyState(): number;
    /**
     * Subscribe a model to a channel for changes
     * @async
     * @param {String} channel    - Channel to subscribe to
     * @param {Object} model      - Model that is subscribing
     * @throws                    - If not able to connect or subscribe
     */
    subscribe(channel: string, model: any): Promise<any>;
    /**
     * Unsubscribe a model from a channel
     * @async
     * @param {String} channel  - Channel to unsubscribe from
     * @param {Object} model    - Model to unsubscribe
     */
    unsubscribe(channel: string, model: any): Promise<any>;
    /**
     * Terminate the connection and notify models. Will not reconnect without an
     * explicit call to connect()
     * @async
     */
    terminate(): Promise<any>;
    /**
     * Re-subscribe to channels as needed after connection
     * @private
     */
    private _resubscribe;
    /**
     * Handle an incoming message or reqsponse
     * @private
     * @param {MessageEvent} ev   - The websocket message event
     */
    private _handleMessage;
    /**
     * Handle connection, letting models with a subscription know
     * @private
     */
    private _handleConnect;
    /**
     * Handle a subscribe reply
     * @private
     */
    private _handleSubscribe;
    /**
     * Handle record ID change notifications
     * @private
     * @param {Object} message                      - Incoming update message
     * @param {String} message.channel              - Channel for the change
     * @param {String} message.change               - Change type, of insert, update or delete
     * @param {Array<String|Number>} [message.ids]  - IDs affected by the change if provided
     */
    private _handleNotify;
    /**
     * Handle record changes
     * @private
     * @param {Object} message                    - Incoming records message
     * @param {String} message.channel            - Channel for the records
     * @param {Object[]} message.records          - Records changed
     */
    private _handleRecords;
    /**
     * Handle an unsubscribe reply
     * @private
     */
    private _handleUnsubscribe;
    /**
     * Handle connection closure or error (which results in closeure)
     * @private
     * @param {CloseEvent|Event} ev   - The websocket close or generic event
     * @param {Boolean} errored=false - If the closure if the result of an error
     *
     * NOTE: not all close events have preceeding error events, and not all error
     * events have subsequent close events.
     */
    private _handleClose;
}
