

import { WebSocketServer } from 'ws';
import { EventEmitter } from 'node:events';
import { v4 as uuidv4 } from 'uuid';


/**
 * A ws based websocket server supporting multiple channels for use with SimplePartialModel
 *
 * Options to the constructor are provided to the WebSocketServer. If authentication
 * or other session management is needed, recommend utilising an external HTTP(s)
 * server to manage authentication during the connection upgrade.
 *
 * Channels are URI like, can contain placeholders like /customers/:cust allowing
 * clients to subscribe to /customers/123 for example.
 *
 * Utilise events to handle other authentication needs.
 *
 * @extends EventEmitter
 * @class
 */
class ChannelSock extends EventEmitter {


  /**
   * Client error event
   * @event ChannelSock#clienterror
   * @type {Error}          - The causative error
   */

  /**
   * Server error event. Must be listened to or will exit the Node process
   * @event ChannelSock#error
   * @type {Error}          - The causative error
   */

  /**
   * Event before client connection completion. Throw in handler to reject the connection.
   * @event ChannelSock#connection
   * @param {http.IncomingMessage} request  - The connection HTTP request
   * @param {WebSocket} sock                - The WebSocket for the connection
   */

  /**
   * Event after a client has connected.
   * @event ChannelSock#connected
   * @param {http.IncomingMessage} request  - The connection HTTP request
   * @param {WebSocket} sock                - The WebSocket for the connection
   */

  /**
   * Event before client subscribe attempt. Throw in handler to reject the subscribe.
   * @event ChannelSock#subscribe
   * @param {String} channel - The channel for the subscribe
   * @param {String} uuid    - The socket UUID for the subscribe
   * @param {WebSocket} sock - The WebSocket for the subscribe
   */

  /**
   * Event after client subscribe
   * @event ChannelSock#subscribed
   * @param {String} channel - The channel for the subscribe
   * @param {String} uuid    - The socket UUID for the subscribe
   * @param {WebSocket} sock - The WebSocket for the subscribe
   */

  /**
   * Event after client unsubscribe
   * @event ChannelSock#unsubscribed
   * @param {String} channel - The channel for the subscribe
   * @param {String} uuid    - The socket UUID for the subscribe
   * @param {WebSocket} sock - The WebSocket for the subscribe
   */


  /** @typedef {Object} CSOpts
   * @property {Number}    [opts.maxClients=0]          - Set a client limit, 0 to disable
   * @property {Number}    [opts.subscribeLimit=100]    - Channel limit per client, set to something responsible
   * @property {Boolean}   [opts.clientCheck=true]      - Check client connections periodically
   * @property {String[]}  [opts.channels=[]]           - List of defined channels
   * @property {Function}  [opts.identFunc]             - Callback to return a UUID for the client, called with HTTP's IncomingMessage
   */

  /**
   * Build a new ChannelSock instance
   * @param {CSOpts}    opts                            - ChannelSock options
   * @param {Object}    wsOpts                          - WebSocketServer options, see ws documentation
   */
  constructor( opts, wsOpts ) {
    const defaults = {
      channels:         [],
      maxClients:       0,
      subscribeLimit:   100,
      clientCheck:      true
    };

    super();

    this._conf = Object.assign( {}, defaults, opts );
    this._channels = new Map();
    this._clients = new Map();
    this._checkTimer = undefined;

    this._ws = new WebSocketServer( wsOpts );

    for ( let chanName of this._conf.channels )
      this.addChannel( chanName );

    if ( this._conf.clientCheck )
      this._checkTimer = setInterval( () => this._checkClients(), 30 * 1000 );

    this._connectEvents();
  }



  /**
   * Handle an upgrade to WebSockets, call if utilising own HTTP(s) server
   * @public
   * @param {http.IncomingMessage} request        - The HTTP(s) request
   * @param {stream.Duplex} socket                - Network socket object
   * @param {Buffer} head                         - The first packet of the upgraded stream
   */
  handleUpgrade( request, socket, head ) {
    this._ws.handleUpgrade( request, socket, head, ( clientSock ) => {
      this._ws.emit( 'connection', clientSock, request );
    } );
  }



  /**
   * Define a new channel
   * @public
   * @param {String} path   - New channel path
   */
  addChannel( path ) {
    if ( ! this._channels.get( path ) ) {
      this._channels.set( path, {
        'placeholder': null,
        'socks': new Map()
      } );
    }
  }



  /**
   * Remove an existing channel
   * @public
   * @param {String} path   - Channel path to remove
   */
  removeChannel( path ) {
    if ( this._channels.has( path ) ) {

      // Remove placeholder'd channels too
      if ( path.indexOf( ':' ) ) {
        for ( let channel of this._channels.keys() ) {
          if ( this._channels.get( channel ).placeholder == path ) {
            this.removeChannel( channel );
          }
        }
      }

      // Tell subscribed clients
      this._unsubscribeClients( path );
      this._channels.delete( path );
    }
  }



  /**
   * Emit a record change message for a given channel
   * @public
   * @param {String} channel              - The channel for the notification
   * @param {String} type                 - Change type, one of update, delete, or insert
   * @param {Array<String|Number>} [ids]  - Array of IDs for the notification if available
   * @throws                              - If unable to notify a change
   */
  notifyChange( channel, type, ids=[] ) {
    let foundChannel, message = {};
    let chanStruct = this._channels.get( channel );

    // Look for a placeholder channel if it's not defined
    if ( ! chanStruct ) {
      foundChannel = this._findPlaceholder( channel );
      if ( ! foundChannel )
        throw new Error( `Channel not known` );
      else // No subscribed channel matching placeholder
        return;
    }

    if ( type !== "update" && type !== "delete" && type !== "insert" )
      throw new Error( `Unknown change type ${type}` );
    if ( ! Array.isArray( ids ) )
      throw new Error( `Unexpected IDs` );

    message[ 'channel' ] = channel;
    message[ 'change' ] = type;
    message[ 'type' ] = 'notify';
    message[ 'ids' ] = ids;

    this._notifyClients( chanStruct.socks, JSON.stringify( message ) );
  }


  /**
   * Emit one or more new or updated records for a given channel
   * @public
   * @param {String} channel      - Channel path to notify
   * @param {Object[]} records    - Updated records
   */
  notifyRecords( channel, records ) {
    let foundChannel, message = {};
    let chanStruct = this._channels.get( channel );

    // Look for a placeholder channel if it's not defined
    if ( ! chanStruct ) {
      foundChannel = this._findPlaceholder( channel );
      if ( ! foundChannel )
        throw new Error( `Channel not known` );
      else // No subscribed channel matching placeholder
        return;
    }

    if ( ! Array.isArray( records ) || ! records.length )
      throw new Error( `Records not specified or invalid` );

    message[ 'channel' ] = channel;
    message[ 'records' ] = records;
    message[ 'type' ] = 'records';

    this._notifyClients( chanStruct.socks, JSON.stringify( message ) );
  }



  /**
   * Notify clients with a manual message for a given channel
   * @public
   * @param {String} channel    - Channel path to notify
   * @param {String} message    - The JSON string message to send
   */
  notifyManual( channel, message ) {
    let channelStruct = this._channels.get( channel );
    if ( channelStruct ) {
      this._notifyClients( channelStruct.socks, message );
      return;
    }
  }



  /**
   * Broadcast a manual message to all connected clients
   * @public
   * @param {String} message    - The JSON string message to broadcast
   */
  broadcastManual( message ) {
    let clients = new Map();
    for( let uuid of this._clients.keys() )
      clients.set( uuid, this._clients.get( uuid ).sock );
    this._notifyClients( clients, message );
  }



  /**
   * Check all clients are active
   */
  _checkClients() {
    const toRemove = [];

    for ( let uuid of this._clients.keys() ) {
      let client = this._clients.get( uuid );

      if ( ! client.sock.pingresponse ) {
        toRemove.push( uuid );
        continue;
      }

      // Closing or gone?
      if ( client.sock.readyState > 1 ) {
        toRemove.push( uuid );
        continue;
      }

      // Ping client
      if ( client.sock.readyState == 1 ) {
        client.sock.pingresponse = false;
        client.sock.ping();
      }
    }

    for ( let uuid of toRemove )
      this._closeClient( uuid );
  }



  /**
   * Find a matching channel with placeholders
   * @private
   * @param {String} channel    - Channel to match
   * @returns {String|null}     - Channel name if found, otherwise null
   */
  _findPlaceholder( channel ) {
    let pathBits, chanBits, next = false;;
    for ( let path of this._channels.keys() ) {

      // Skip anything without a placeholder
      if ( path.indexOf( ':' ) == -1 )
        continue;

      pathBits = path.split( '/' );
      chanBits = channel.split( '/' );
      if ( pathBits.length !== chanBits.length )
        continue;

      for ( let idx = 0; idx < chanBits.length; idx++ ) {
        if ( ! pathBits[ idx ].startsWith( ':' ) ) {
          if ( pathBits[ idx ] !== chanBits[ idx ] ) {
            next = true;
            break;
          }
        }
      }
      if ( next )
        continue;

      return path;
    }
    return null;
  }



  /**
   * Send a message to a Map of clients
   * @private
   * @param {Map|Set} clients   - Set or map of clients to notify
   * @param {String} message    - Message to dispatch
   */
  _notifyClients( clients, message ) {
    for ( let sock of clients.values() ) {
      if ( sock.readyState == 1 ) {
        sock.send( message );
      }
    }
  }



  /**
   * Close a client connection and remove subscriptions
   * @private
   * @param {String} uuid     - The UUID of client
   */
  _closeClient( uuid ) {
    let sock;

    for ( let channel of this._channels.keys() ) {
      if ( this._channels.get( channel ).socks.has( uuid ) )
        this._channels.get( channel ).socks.delete( uuid );
    }

    if ( this._clients.has( uuid ) ) {
      sock = this._clients.get( uuid ).sock;
      if ( sock.readyState == 1 )
        sock.close();
      else
        sock.terminate();
      this._clients.delete( uuid );
    }
  }



  /**
   * Connect WS server events
   * @private
   */
  _connectEvents() {
    this._ws.on( 'connection', this._handleConnection.bind( this ) );
    this._ws.on( 'error', this._handleError.bind( this ) );
  }



  /**
   * Handle a new connection, connect events and set connection identifier
   * @private
   * @param {WebSocket} sock        - The connection socket
   * @param {http.IncomingMessage}  - The HTTP request
   */
  _handleConnection( sock, request ) {
    let uuid;
    sock.on( 'error', ( err ) => { this._handleClientError( sock, true, err ) } );
    sock.on( 'message', ( data ) => { this._handleMessage( sock, data ) } );
    sock.on( 'pong', () => { this._handlePong( sock ) } );

    sock.pingresponse = true;

    // Get the UUID
    try {
      if ( this._conf.identFunc ) {
        uuid = this._conf.identFunc( request );
        if ( uuid )
          sock.uuid = uuid;
        else
          throw new Error( `UUID not provided` );
      }
      else
        sock.uuid = uuidv4();
    }
    catch( err ) {
      this._handleClientError( sock, true, new Error( `Client identification failed: ${err.message}` ) );
      return;
    }

    if ( this._conf.maxClients > 0 && this._clients.size >= this._conf.maxClients ) {
      this._handleClientError( sock, true, new Error( `Client connection limit reached` ) );
      return;
    }

    // Try to complete the connection
    try {
      this.emit( 'connection', request, sock );
    }
    catch( err ) {
      sock.send( JSON.stringify( {
        'type'    : 'connect',
        'success' : false,
        'message' : `Connect request rejected: ${err.message}`
      } ) );
      this._handleClientError( sock, false, new Error( 'Connection request rejected' ) );
      return;
    }

    this._clients.set( sock.uuid, {
      sock: sock,
      channels: new Set()
    } );

    this.emit( 'connected', request, sock );
  }



  /**
   * Emit an error that occurred on the WebSocketServer
   * @private
   * @param {Error} err         - The error raised
   * @fires ChannelSock#error
   */
  _handleError( err ) {
    this.emit( 'error', err );
  }



  /**
   * Emit an error that occurred with a client and optionally close the connection
   * @private
   * @param {WebSocket} sock    - WebSocket experiencing the error
   * @param {Boolean} term      - If the connection should be terminated
   * @param {Error} err         - The error raised
   * @fires ChannelSock#clienterror
   */
  _handleClientError( sock, term, err ) {
    let uuid;
    if ( term ) {
      uuid = sock.uuid;
      if ( ! this._clients.has( uuid ) ) {
        if ( sock.readyState <= 1 )
          sock.close();
        else
          sock.terminate();
      }
      else if ( uuid )
        this._closeClient( sock.uuid );
      else
        sock.terminate();
    }
    this.emit( 'clienterror', err );
  }



  /**
   * Handle a ping response from a client
   * @param {WebSocket} sock    - The WebSocket for the client
   */
  _handlePong( sock ) {
    if ( this._clients.has( sock.uuid ) ) {
      this._clients.get( sock.uuid ).sock.pingresponse = true;
    }
  }



  /**
   * Handle a message from a client
   * @private
   * @param {WebSocket} sock    - The WebSocket for the client
   * @param {Buffer} data       - The received message in a buffer
   */
  _handleMessage( sock, data ) {
    let message, type;

    try {
      message = JSON.parse( data );

      type = message.type;
      if ( ! type )
        return this._handleClientError( sock, false, new Error( `Unknown message type` ) );

      switch( type ) {
        case "subscribe":
          this._handleSubscribe( sock, message );
          break;
        case "unsubscribe":
          this._handleUnsubscribe( sock, message );
          break;
      }
    }
    catch( err ) {
      this._handleClientError( sock, false, err );
    }
  }



  /**
   * Handle a subscribe request
   * @private
   * @param {WebSocket} sock    - The WebSocket for the client
   * @param {Object} message    - The Object message received
   */
  _handleSubscribe( sock, message ) {
    const channelPath = message.channel;
    const uuid = sock.uuid;
    let placeholder, channelStruct;

    // Check a path is provided
    if ( ! channelPath ) {
      sock.send( JSON.stringify( {
        'id'      : message.id,
        'type'    : 'subscribe',
        'success' : false,
        'message' : `Unspecified channel for subscription`
      } ) );
      return this._handleClientError( sock, false, new Error( `Unspecified channel for subscription` ) );
    }

    // Check we can subscribe to the channel
    try {
      this.emit( 'subscribe', channelPath, uuid, sock );
    }
    catch( err ) {
      sock.send( JSON.stringify( {
        'id'      : message.id,
        'type'    : 'subscribe',
        'success' : false,
        'message' : `Subscribe request rejected`
      } ) );
      this._handleClientError( sock, false, new Error( 'Subscribe request rejected' ) );
      return;
    }

    // Get existing channel or find a placeholder'd channel
    channelStruct = this._channels.get( channelPath );
    if ( ! channelStruct ) {
      placeholder = this._findPlaceholder( channelPath );
      if ( placeholder ) {
        this._channels.set( channelPath, {
          'socks'         : new Map(),
          'placeholder'   : placeholder
        } );
        channelStruct = this._channels.get( channelPath );
      }
    }

    if ( ! channelStruct ) {
      sock.send( JSON.stringify( {
        'id'      : message.id,
        'type'    : 'subscribe',
        'success' : false,
        'message' : `Channel does not exist`
      } ) );
      this._handleClientError( sock, false, new Error( `Attempt to subscribe to unknown channel` ) );
      return;
    }

    // Channel available, check limits
    if ( this._clients.get( uuid ).channels.size > this._conf.subscribeLimit ) {
      sock.send( JSON.stringify( {
        'id'      : message.id,
        'type'    : 'subscribe',
        'success' : false,
        'message' : `Subscribe limit reached`
      } ) );
      this._handleClientError( sock, false, new Error( `Subscribe limit reached` ) );
      return;
    }

    // Good to subscribe
    channelStruct.socks.set( uuid, sock );
    this._clients.get( uuid ).channels.add( channelPath );
    sock.send( JSON.stringify( {
      'id'      : message.id,
      'type'    : 'subscribe',
      'success' : true
    } ) );

    this.emit( 'subscribed', channelPath, uuid, sock );
  }



  /**
   * Handle an unsubscribe request
   * @private
   * @param {WebSocket} sock    - The WebSocket for the client
   * @param {Object} message    - The Object message received
   */
  _handleUnsubscribe( sock, message ) {
    const channelPath = message.channel;
    const uuid = sock.uuid;
    let channelStruct;

    if ( ! channelPath )
      return this._handleClientError( sock, false, new Error( `Unspecified channel for unsubscribe` ) );

    // Get existing channel or fail
    channelStruct = this._channels.get( channelPath );
    if ( ! channelStruct ) {
      sock.send( JSON.stringify( {
        'type'    : 'unsubscribe',
        'success' : false,
        'message' : `Channel does not exist`
      } ) );
      this._handleClientError( sock, false, new Error( `Attempt to unsubscribe from unknown channel` ) );
      return;
    }

    // Unsubscribe
    channelStruct.socks.delete( uuid );
    this._clients.get( uuid ).channels.delete( channelPath );
    sock.send( JSON.stringify( {
      'type'    : 'unsubscribe',
      'success' : true
    } ) );

    this.emit( 'unsubscribed', channelPath, uuid, sock );
  }



  /**
   * Unsubscribe clients from a given channel
   */
  _unsubscribeClients( channel ) {
    for ( let [ uuid, client ] of this._clients ) {
      if ( client.channels.has( channel ) ) {
        client.channels.delete( channel );
        this._notifyUnsubscribe( channel, uuid );
      }
    }
  }


  /**
   * Notify a subscriber of unsubscription
   * @private
   * @param {String} channel      - The channel to be unsubscribed
   * @param {String} uuid         - The client uuid
   */
  _notifyUnsubscribe( channel, uuid ) {
    const clients = new Set();
    const message = {
      'type'    : 'unsubscribe',
      'channel' : channel
    };
    clients.add( this._clients.get( uuid ).sock );
    this._notifyClients( clients, JSON.stringify( message ) );
  }



}

export { ChannelSock };

