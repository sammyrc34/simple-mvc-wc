export default QueryTranslate;
/**
 * Utility class to assist with smart model filtering and sorting
 * @class
 */
declare class QueryTranslate {
    /**
     * Return a new query translation instance
     * @see BasicCollection#FiltersCollection
     * @see BasicCollection#SortsType
     * @param {String} language           - Intended output language, sqlite, oracle, mysql
     * @param {FiltersCollection} filters - Filters
     * @param {SortsType} sorts           - Sorts
     * @constructor
     */
    constructor(language: string, filters: FiltersCollection, sorts?: SortsType);
    _filters: FiltersCollection;
    _sorts: SortsType;
    _lang: string;
    _binds: any[];
    /**
     * Return a filter clause
     * Note, do not rely on filters to provide security or limit access of rows.
     * @param {Object} columnMap  - Map of filter or sort fields to DB column names
     * @param {String} tableAlias - Table alias
     * @returns {String}          - The filter clause, or match-all clause if no filters are provided
     */
    getFilter(columnMap?: any, tableAlias?: string): string;
    _alias: string;
    _colmap: any;
    /**
     * Return the statement binds list for the filter clause
     * @returns {Array<String|Number>}  - Bound values
     */
    getBinds(): Array<string | number>;
    /**
     * Return an order by clause or empty string without sorts
     * @param {Object} columnMap  - Map of filter or sort fields to DB column names
     * @param {String} tableAlias - Table alias
     * @returns {String}          - Order by clause
     */
    getSort(columnMap?: any, tableAlias?: string): string;
    /**
     * Ensure objects as expected
     */
    _check(): void;
    _buildFilters(rules: any, mode?: string): string;
    /**
     * Return a string
     * @param {String} field      - Filter field name
     * @param {CriteriaType} rule - Criteria rule
     * @param {String} [mode=AND] - Criteria mode, OR or AND
     * @returns {String}          - SQL constraint fragment
     */
    _buildCriteria(field: string, rule: CriteriaType, mode?: string): string;
    /**
     * Build one filter statement
     * @param {String} colname      - The name of the database column
     * @param {String} operator     - The operator name
     * @param {String|String[]} val - The value or values to filter with
     * @param {Boolean} matchcase   - If the string match is case sensitive
     */
    _buildOneCriteria(colname: string, operator: string, val: string | string[], matchcase: boolean): string;
}
