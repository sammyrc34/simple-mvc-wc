

import { v4 as uuidv4 } from 'uuid';


/**
 * Channel WebSocket Client for the ChannelSock server service
 *
 * Provides a client interface for an aggregated WebSocket connection to a single
 * endpoint making one or more channel subscriptions.
 */

class ChannelSockClient {


  /**
   * Create a client interface instance
   * @constructor
   * @param {String} endpoint   - Endpoint URL to connect to
   */
  constructor( app, endpoint ) {

    this.app = app;
    this._endpoint = endpoint;
    this._sock = undefined;
    this._connProm = undefined;
    this._models = new WeakMap();
    this._subs = new Map();
    this._subProms = new Set();
    this._unsubProms = new Set();
    this._termProm = new Map();
    this._terminating = false;
    this._connTimer = undefined;
    this._connDelay = 500;
    this._failCount = 0;
    return this;
  }


  /**
   * Connect to remote endpoint as needed
   * @async
   * @throws      - In event of connection failure
   */
  async connect() {

    this.app.log( 'debug', `Connecting to websock server` );

    // Get a socket as needed
    if ( ! this._sock || this._sock.readyState == 3 ) {
      this._sock = new WebSocket( this._endpoint );

      // Promisify connection
      this._connProm = new Promise( ( resolve, reject ) => {

        // Catch errors during connection
        this._sock.addEventListener( 'error', ( err ) => {
          this.app.log( "error", "Connection failed: ", err.message );

          // Reconnect as needed
          if ( ! this._connTimer ) {
            this.app.log( "debug", `Websocket reconnection queued` );
            this._connTimer = setTimeout( () => {
              this._connTimer = undefined;
              this.connect();
            }, this._connDelay );

            this._failCount++;
            this._connDelay = 500 + this._failCount ** 5;
            if ( this._connDelay > 60000 )
              this._connDelay = 60000;
          }

          return reject( err );
        }, { once: true } )

        // Catch open connections
        this._sock.addEventListener( 'open', () => {

          this.app.log( 'debug', `Websocket connection open` );

          this._sock.addEventListener( 'message', this._handleMessage.bind( this ) );
          this._sock.addEventListener( 'error', (ev) => { this._handleClose( ev, true ) } );
          this._sock.addEventListener( 'close' , (ev) => { this._handleClose( ev ); } );

          // Reset delay and reconnection
          if ( this._connTimer )
            clearTimeout( this._connTimer )
          this._connTimer = undefined;
          this._connDelay = 500;
          this._failCount = 0;
          this._terminating = false;

          // Let subscribed models know
          this._handleConnect();

          // Re-subscribe as needed without waiting
          this._resubscribe();

          return resolve();
        }, { once: true } );

      } );
    }
    else
      this.app.log( 'debug', `Existing socket, connection not attempted` );

    return this._connProm;
  }



  /**
   * Proxy ws connection state method
   * @type {Number}   - Connection state identifier, 0 = connecting, 1 = open, 2 = closing, 3 = closed
   */
  get readyState() {
    if ( ! this._sock )
      return 3;
    return this._sock.readyState;
  }



  /**
   * Subscribe a model to a channel for changes
   * @async
   * @param {String} channel    - Channel to subscribe to
   * @param {Object} model      - Model that is subscribing
   * @throws                    - If not able to connect or subscribe
   */
  async subscribe( channel, model ) {
    let prom, uuid, prev;
    let chanModelSet = this._subs.get( channel );

    this.app.log( 'debug', `Attempting subscribe to ${channel} by model ${model.uuid}` );

    // Connect as needed
    if ( ! this._sock )
      await this.connect();

    // Setup channel model structure as needed
    if ( ! chanModelSet ) {
      this._subs.set( channel, new Set() );
      chanModelSet = this._subs.get( channel );
    }

    // Model already subscribed? Unsubscribe first
    prev = this._models.get( model );
    if ( prev && prev !== channel ) {
      this.app.log( 'debug', `Attempting unsubscribe before subscribe by model ${model.uuid}` );
      await this.unsubscribe( prev, model );
    }

    // Another model already subscribed to this channel?
    if ( chanModelSet && chanModelSet.size > 0 ) {
      this._models.set( model, channel );
      chanModelSet.add( model );
      return;
    }

    // Add to the set of models for the channel early
    chanModelSet.add( model );
    this._models.set( model, channel );

    // Try the subscribe
    prom = new Promise( ( resolve, reject ) => {
      uuid = uuidv4();

      // Setup a resolver / rejector for the request
      this._subProms.add( {
        'channel'     : channel,
        'resolver'    : resolve,
        'rejecter'    : reject,
        'model'       : model,
        'id'          : uuid
      } );

      // Attempt the subscribe
      this._sock.send( JSON.stringify( {
        'type'      : "subscribe",
        'channel'   : channel,
        'id'        : uuid
      } ) );
    } );

    return prom;
  }



  /**
   * Unsubscribe a model from a channel
   * @async
   * @param {String} channel  - Channel to unsubscribe from
   * @param {Object} model    - Model to unsubscribe
   */
  async unsubscribe( channel, model ) {
    let chanModelSet = this._subs.get( channel );
    let prom, uuid;

    this.app.log( 'debug', `Attempting unsubscribe from ${channel} by model ${model.uuid}` );

    // Not subscribed to this channel or not connected?
    if ( ! chanModelSet || ! this._sock || this._sock.readyState !== 1 ) {
      if ( chanModelSet ) {
        chanModelSet.delete( model );
        this._models.delete( model );
        if ( ! chanModelSet.size )
          this._subs.delete( channel );
      }
      if ( ! this._sock || this._sock.readyState !== 1 )
        this._models.delete( model );
        if ( chanModelSet && ! chanModelSet.size )
          this._subs.delete( channel );
      return;
    }

    // Another model still subscribed?
    for ( let submodel of chanModelSet ) {
      if ( submodel == model ) {
        chanModelSet.delete( model );
        this._models.delete( model );
        if ( ! chanModelSet.size )
          this._subs.delete( channel );
        return;
      }
    }

    // Try the unsubscribe
    prom = new Promise( ( resolve, reject ) => {

      uuid = uuidv4();

      // Setup a resolver / rejector for the request
      this._unsubProms.add( {
        'channel'     : channel,
        'resolver'    : resolve,
        'rejecter'    : reject,
        'model'       : model,
        'id'          : uuid
      } );

      // Attempt the unsubscribe
      this._sock.send( JSON.stringify( {
        'type'      : "unsubscribe",
        'channel'   : channel,
        'id'        : uuid
      } ) );
    } );

    return prom;
  }


  /**
   * Terminate the connection and notify models. Will not reconnect without an
   * explicit call to connect()
   * @async
   */
  async terminate() {
    let prom;

    this.app.log( 'info', `Terminating websock connection` );

    // Reset reconnection timer and state
    if ( this._connTimer ) {
      clearTimeout( this._connTimer )
      this._connTimer = undefined;
      this._connDelay = 500;
      this._failCount = 0;
    }

    // Already terminated?
    if ( ! this._sock )
      return;

    // Already terminating?
    if ( this._termProm.size )
      return this._termProm.get( 'promise' );

    // Setup promise for completion;
    prom = new Promise( (resolve,reject) => {
      this._termProm.set( 'resolver', resolve );
      this._termProm.set( 'rejecter', reject );
    } );

    this._terminating = true;
    this._termProm.set( 'promise', prom );

    this._sock.terminate();

    return prom;
  }


  /**
   * Re-subscribe to channels as needed after connection
   * @private
   */
  async _resubscribe() {
    let uuid;

    // Always clean up lingering promises first, should be none
    for ( let unsub of this._unsubProms ) {
      unsub.rejecter();
      this._unsubProms.delete( unsub );
    }
    for ( let sub of this._subProms ) {
      if ( sub.rejecter )
        sub.rejecter();
      this._subProms.delete( sub );
    }

    if ( ! this._subs.size )
      return;

    this.app.log( 'debug', `Re-subscribing to channels` );

    // Re-subscribe
    for ( let chan of this._subs.keys() ) {
      if ( ! this._subs.get( chan ).size )
        continue;

      uuid = uuidv4();

      // Store the request for reply handling
      this._subProms.add( {
        'channel'     : chan,
        'resubscribe' : true,
        'id'          : uuid
      } );

      // Attempt the subscribe
      this._sock.send( JSON.stringify( {
        'type'          : "subscribe",
        'channel'       : chan,
        'id'            : uuid
      } ) );
    }
  }



  /**
   * Handle an incoming message or reqsponse
   * @private
   * @param {MessageEvent} ev   - The websocket message event
   */
  _handleMessage( ev ) {
    const msg = JSON.parse( ev.data ) || {};
    let type = msg.type;

    if ( ! type ) {
      this.app.log( "error", "Malformed message", msg );
      throw new Error( "Malformed message" );
    }

    this.app.log( "debug", `${type ? type : "unknown"} message received` );

    switch( type ) {
      case "subscribe":
        return this._handleSubscribe( msg );
      case "unsubscribe":
        return this._handleUnsubscribe( msg );
      case "notify":
        return this._handleNotify( msg );
      case "records":
        return this._handleRecords( msg );
    }
  }


  /**
   * Handle connection, letting models with a subscription know
   * @private
   */
  _handleConnect() {
    let modelSet, models = new Set();

    // Collect models from subscriptions
    for ( let channel of this._subs.keys() ) {
      modelSet = this._subs.get( channel );
      for ( let model of modelSet ) {
        models.add( model );
      }
    }

    // Alert models without waiting
    for ( let model of models )
      model.handleEvent( 'connect' );
  }


  /**
   * Handle a subscribe reply
   * @private
   */
  _handleSubscribe( message ) {
    let chan, req, resub = false;

    // Find the request
    for ( let waiting of this._subProms ) {
      if ( message.id == waiting.id ) {
        req = waiting;
        break;
      }
    }
    if ( ! req ) {
      this.app.log( 'error', `Subscribe reply ${message.id} received without request` );
      return;
    }

    resub = req.resubscribe || false;
    chan = req.channel;

    // Subscribe failure
    if ( ! message.success ) {
      this.app.log( 'error', `Subscribe failure, ${message.message}` );
      // FIXME: try resubs again?
      if ( req.rejecter )
        req.rejecter( message.message );
      this._subProms.delete( req );
      return;
    }

    // Success
    this.app.log( 'debug', `Subscribe to channel ${chan} successful` );
    if ( req.resolver )
      req.resolver();
    if ( resub ) {
      for ( let model of this._subs.get( chan ) )
        model.handleEvent( 'resubscribe' );
    }
    this._subProms.delete( req );
  }


  /**
   * Handle record ID change notifications
   * @private
   * @param {Object} message                      - Incoming update message
   * @param {String} message.channel              - Channel for the change
   * @param {String} message.change               - Change type, of insert, update or delete
   * @param {Array<String|Number>} [message.ids]  - IDs affected by the change if provided
   */
  _handleNotify( message ) {
    const change = message.change;
    const channel = message.channel;
    const ids = message.ids || [];
    let chanModelSet;

    if ( channel == undefined || change == undefined )
      throw new Error( `Malformed change notification` );

    // Get models subscribed to the channel
    chanModelSet = this._subs.get( channel );
    if ( ! chanModelSet ) {
      this.app.log( 'warn', `Change notification for unsubscribed channel` );
      return;
    }

    for ( let model of chanModelSet )
      model.handleEvent( 'notify', { 'change': change, 'ids': ids } );
  }


  /**
   * Handle record changes
   * @private
   * @param {Object} message                    - Incoming records message
   * @param {String} message.channel            - Channel for the records
   * @param {Object[]} message.records          - Records changed
   */
  _handleRecords( message ) {
    const channel = message.channel;
    const recs = message.records;
    let chanModelSet;

    if ( channel == undefined || recs == undefined || ! recs.length )
      throw new Error( `Malformed record change message` );

    // Get models subscribed to the channel
    chanModelSet = this._subs.get( channel );
    if ( ! chanModelSet ) {
      this.app.log( "warn", `Records notification for unsubscribed channel` );
      return;
    }

    for ( let model of chanModelSet )
      model.handleEvent( 'records', { 'records': recs } );
  }



  /**
   * Handle an unsubscribe reply
   * @private
   */
  _handleUnsubscribe( message ) {
    let chanModelSet, req;

    // Find the request
    for ( let waiting of this._unsubProms ) {
      if ( message.id == waiting.id ) {
        req = waiting;
        break;
      }
    }

    // Unexpected unsubscribe?
    if ( ! req ) {
      this.app.log( 'debug', `Unexpected unsubscribe from channel ${message.channel}` );
      chanModelSet = this._subs.get( channel );
      if ( chanModelSet && chanModelSet.size > 0 ) {
        this.app.log( 'debug', `Notifying subscribed models` );
        for ( let model of chanModelSet )
          model.handleEvent( 'unsubscribe' );
      }
      return;
    }

    // Get channel model list
    chanModelSet = this._subs.get( req.channel );
    if ( ! chanModelSet )
      this.app.log( 'warn', `Unsubscribe reply ${message.id} for channel without model set` );

    // Unsubscribe failure is assumed not subscribed, therefore achieving unsubscription
    if ( ! message.success )
      this.app.log( 'warn', `Unsubscribe failure, ${message.message}, assuming not subscribed` );
    else
      this.app.log( 'debug', `Unsubscribe to channel ${req.channel} successful` );

    // Forget the model and resolve
    if ( chanModelSet )
      chanModelSet.delete( req.model );
    this._models.delete( req.model );
    this._unsubProms.delete( req );
    return req.resolver();
  }



  /**
   * Handle connection closure or error (which results in closeure)
   * @private
   * @param {CloseEvent|Event} ev   - The websocket close or generic event
   * @param {Boolean} errored=false - If the closure if the result of an error
   *
   * NOTE: not all close events have preceeding error events, and not all error
   * events have subsequent close events.
   */
  async _handleClose( ev, errored=false ) {
    let modelSet, resolver, rejecter, models = new Set();

    // From our own terminate request?
    if ( this._termProm.size ) {
      resolver = this._termProm.get( 'resolver' );
      rejecter = this._termProm.get( 'rejecter' );
      this._termProm.clear();
      this.app.log( 'info', 'Connection closed successfully' );
    }
    else
      this.app.log( 'warn', 'Connection lost unexpectantly' );

    // Already errored?
    if ( this._sock == undefined ) {
      if ( resolver )
        resolver();
      return;
    }

    if ( errored )
      this.app.log( 'debug', `Websocket errored and closed: ${ev.reason}` );
    else
      this.app.log( 'debug', `Websocket closed ${ev.wasClean ? `cleanly` : `unexpectantly`}: ${ev.reason}` );

    // Gather models and notify
    for ( let channel of this._subs.keys() ) {
      modelSet = this._subs.get( channel );
      for ( let model of modelSet )
        models.add( model );
    }
    for ( let model of models )
      model.handleEvent( 'disconnect' );

    // Reconnect as needed
    if ( this._subs.size && ! this._terminating ) {
      this._sock = undefined;
      if ( ! this._connTimer ) {
        this.app.log( "debug", `Websocket reconnection queued` );
        this._connTimer = setTimeout( () => {
          this._connTimer = undefined;
          this.connect();
        }, this._connDelay );

        this._failCount++;
        this._connDelay = 500 + this._failCount ** 5;
        if ( this._connDelay > 60000 )
          this._connDelay = 60000;
      }
    }
    else {
      // No subscriptions or terminating, remove the socket and reject promises
      this._sock = undefined;
      for ( let prom of this._subProms )
        prom.rejecter();
      for ( let prom of this._unsubProms )
        prom.rejecter();
      this._subProms = new Set();
      this._unsubProms = new Set();

      if ( this._connTimer ) {
        clearTimeout( this._connTimer );
        this._connTimer = undefined;
      }
    }

    if ( errored && rejecter )
      rejecter();
    else if ( resolver )
      resolver();
  }


}

export { ChannelSockClient };
