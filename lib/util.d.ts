/**
 * Static utility function class
 */
export default class Util {
    /**
      * Deep-clone a Map / Set / Array / Function / Object
      * @static
      * @param {Object|Array|Map|Set|Function} thing   - The item to clone
      * @returns {Object|Array}                        - The deep-cloned object or array
      */
    static clone(thing: any | any[] | Map<any, any> | Set<any> | Function): any | any[];
    /**
     * Deep-merge objects
     * @static
     * @param {Object} dest         - The object to merge into, modified in place
     * @param {...Object} sources   - The objects to merge
     * @throws                      - If attempting to merge incompatible objects
     */
    static deepMerge(dest: any, ...sources: any[]): any;
    /**
     * Convert a regular expression to a partial-match capable regex
     * Adapted from code written by Lucas Trzesniewski, from https://stackoverflow.com/questions/22483214/regex-check-if-input-still-has-chances-to-become-matching/41580048#41580048
     * @param {RegExp} regex  - The regular expression to convert
     * @returns {RegExp}      - Partial match capable regular expression
     * @throws                - In event of failure during conversion
     */
    static partialRegex(regex: RegExp): RegExp;
}
