
/**
 * The main application module
 * @module App
 */

import SimpleRouter from './router.js';
import SimpleBuilder from './builder.js';
import SimpleUtil from './util.js';

import equal from 'fast-deep-equal';

const defaults = {
  name: 'Application',
  prefix: '/',
  debug: false
};

export class EventEmitter {
  /**
   * @protected
   * @class
   * @classdesc Class for event emitting and listening
   */
  constructor() {
    this._delegate = document.createDocumentFragment();
  } 

  /**
   * Add an event listener
   * @param {...Object} args - Proxied arguments for the normal EventTarget.addEventListener function.
   */
  addEventListener( ...args ) {
    return this._delegate.addEventListener( ...args );
  }

  /**
   * Dispatch or fire event
   * @param {...Object} args - Proxied arguments for the normal EventTarget.dispatchEvent function.
   */
  dispatchEvent( ...args ) {
    return this._delegate.dispatchEvent( ...args );
  }

  /**
   * Remove an event listener
   * @param {...Object} args - Proxied arguments for the normal EventTarget.removeEventListener function.
   */
  removeEventListener( ...args ) {
    return this._delegate.removeEventListener( ...args );
  }
}


export class SimpleApp extends EventEmitter {

  /**
   * Construct the application instance
   * @param {Object} conf                 - Application configuration.
   * @param {String} conf.name            - Application name.
   * @param {String} conf.container       - The ID for the HTML element indicating where to insert the application.
   * @param {String} conf.defaultpage     - The default landing page for new connections. 
   * @param {String} [conf.prefix="/"]    - The prefix for the application if not hosted on the root of the server.
   * @param {String} [conf.debug=false]   - Indicate whether debugging messages are enabled.
   * @param {String} [conf.notFoundPage]  - An optional page to render if URL doesn't resolve
   * @param {String} [conf.errorPage]     - An optional page to render if URL doesn't build
   * @param {Class}  [conf.sockClient]    - The ChannelSocketClient class if using Websockets with Integrated Models
   * @returns {SimpleApp} app             - The application instance
   */
  constructor( conf ) {
    super();

    let ourconf = Object.assign( {}, defaults, conf );
    this.config = ourconf;

    this._cleanPaths();

    this.router = new SimpleRouter( this, ourconf ); 
    this.builder = new SimpleBuilder( this, ourconf );
    this.util = SimpleUtil;

    this.state = {};
    this.forwardstate = {};

    this.models = new Map();
    this.controllers = new Map();
    this.customStyles = new Map();
    this.sockClients = new Map();

    // Intercept browser navigation events
    window.addEventListener( 'popstate', e => this._browserAction( e ) );

    // Store the app globally
    window.$app = this;
  }


  /**
   * Register a view for navigation and display
   * @param {String} viewUrl        - The URL segment or segments associated with the view.
   * @param {Object} viewClass      - The view class.
   * @param {String} [contextName]  - Context name provided to the view upon build, helpful if using the same view for multiple URL segments
   */
  registerView( viewUrl, viewClass, contextName ) {
    this.log( 'debug', `Registering view ${viewUrl} ${contextName ? `[${contextName}]` : ''}` );
    this.router.registerView( viewUrl, viewClass, contextName );
  }


  /**
   * Register a controller for one or more views
   * @param {String} controllerName   - The name associated with the controller
   * @param {Object} controllerClass  - The controller's class
   * @param {Object[]} viewClasses    - The views classes this controller is associated with
   */
  registerController( controllerName, controllerClass, viewClasses ) {
    this.log( 'debug', 'Registering controller', controllerName );
    if ( this.controllers.has( controllerName ) ) {
      this.log( 'warn', 'Refusing to register controller already registered' );
      return;
    }
    this.controllers.set( controllerName, {
      'views': viewClasses,
      'class': controllerClass,
      'instance': undefined
    } );
  }


  /**
   * Register an instanced or static model
   * @param {String} modelName            - The name associated with the model
   * @param {Object} modelClass           - The class for the model
   * @param {Boolean} [instanced=false]   - If this model is instanced, false for a singleton
   */
  registerModel( modelName, modelClass, instanced=false ) {
    this.log( 'debug', `Registering ${instanced ? `instanced` : `non-instanced`} model`, modelName );
    if ( this.models.has( modelName ) ) {
      this.log( 'warn', 'Refusing to register model already registered' );
      return;
    }
    this.models.set( modelName, {
      'class': modelClass,
      'instanced': instanced,
      'instance': undefined
    } );
  }


  /**
   * Set custom styles for a provided component by name.
   * Note, this should be done before start() is called. Otherwise currently 
   * rendered views will not update to reflect custom styles.
   * @param {String}    componentName - The component to import this style
   * @param {CSSResult} style         - A Lit CSSResult generated by the css builder
   */
  registerStyle( componentName, style ) {
    const existing = this.customStyles.get( componentName );
    this.customStyles.set( componentName, existing ? existing.concat( [ style ] ) : [ style ] );
  }


  /**
   * Fetch a controller for a view and action
   * @param {Object} viewClass        - The class for the view
   * @param {String} action           - The action intended
   * @returns {Object} controller     - The controller or undefined if none found
   */
  fetchController( viewClass, action ) {
    this.log( 'debug', 'Fetching controller for action', action );
    let controller, found = undefined;
    for ( let controllerName of this.controllers.keys() ) {
      controller = this.controllers.get( controllerName );
      if ( controller[ 'views' ].indexOf( viewClass ) != -1 ) {
        if ( controller[ 'class' ].actionMap().hasOwnProperty( action ) ) {
          if ( controller[ 'instance' ] == undefined )
            controller[ 'instance' ] = new controller[ 'class' ]( this );
          found = controller[ 'instance' ];
          break;
        }
      }
    }
    return found;
  }

 
  /**
   * Fetch any custom styles for a component
   * @param {String} componentName      - The component to fetch styles for
   * @returns {Array<CSSResult>} styles - A list of Lit CSSResults or undefined if no styles
   */
  fetchStyles( componentName ) {
    return this.customStyles.get( componentName ) || [];
  }


  /**
   * Fetch a instanced or shared model by name
   * @param {String} modelName  - The name associated with the model
   * @param {Object} config={}  - Supplementary configuration
   * @returns {Object} model    - Shared or new model instance
   */
  fetchModel( modelName, config={} ) {
    let modelConf, inst;
    if ( ! this.models.has( modelName ) )
      return undefined;
    modelConf = this.models.get( modelName );

    // An instanced model?
    if ( modelConf.instanced ) {
      inst = new modelConf[ 'class' ]( this, config );
      inst.setup();
      return inst;
    }

    // A shared model
    if ( modelConf.instance == undefined ) {
      modelConf.instance = new modelConf[ 'class' ]( this, config );
      modelConf.instance.setup();
    }
    return modelConf.instance;
  }


  /**
   * Clear one or all models instances. Typically used for session cleanup.
   * @param {String} [modelName]    - The name of one model to destroy, otherwise all are cleared
   */
  clearModels( modelName ) {
    let model;
    if ( modelName ) {
      this.log( 'debug', `Clearing model ${modelName} instance` );
      model = this.models.get( modelName );
      if ( model.instance !== undefined )
        model.instance.clear();
    }
    else {
      this.log( 'debug', `Clearing all model instances` );
      for ( let oneModelName of this.models.keys() )
        this.clearModels( oneModelName );
    }
  }



  /** 
   * Fetch a channeled websocket client for an endpoint.
   * @param {String} endpoint           - URL for the Websocket endpoint
   * @return {ChannelSockClient} client - The existing or new channeled websocket instance
   */
  fetchWebsock( endpoint ) {
    const clients = this.sockClients;
    let client = clients.get( endpoint );
    if ( client )
      return client;
    client = new this.config.sockClient( this, endpoint );
    clients.set( endpoint, client );
    return client;
  }



  /**
   * Is the browser mobile sized?
   */
  isMobile() {
    return window.matchMedia("only screen and (max-width: 760px)").matches;
  }


  /**
   * Start the client application and navigate as wished.
   * @async
   */
  async start() {
    const loc = window.location;
    const query = {};
    let path = loc.pathname;
    let hash = loc.hash;
    let rawquery = loc.search;
    let startev, params;

    this.log( 'debug', "Starting application" );


    // Check for minimum supported browser version
    // FIXME: Unimplemented
    //

    // Remove any application prefix
    path = path.replace( this.prefix, '' );
    if ( hash != '' ) hash = hash.slice( 1 );
    if ( rawquery != '' ) rawquery = rawquery.slice( 1 );

    // Process query parameters
    if ( rawquery != '' ) {
      params = new URLSearchParams( rawquery );
      for( let param of params ) {
        let [k, v] = param;
        if ( query.hasOwnProperty( k ) ) {
          if ( ! Array.isArray( query[k] ) )
            query[ k ] = [ query[ k ] ];
          query[ k ].push( v );
        }
        else
          query[ k ] = v;
      }
    }

    // Navigate to the desired or default page
    await this.navigate( path, hash, query, "replace" );

    // Let the application know
    startev = new CustomEvent( 'app-start', { cancelable: false } );
    this.dispatchEvent( startev );
  }


  /**
   * Scroll to an element
   * @param {String} id - The id of the element to scroll to
   */
  scrollTo( id ) {
    this.log( 'debug', 'Scrolling to element ID', id );
    const elem = document.getElementById( id );
    if ( ! elem ) {
      this.app.log( 'warn', "Scroll id does not resolve to a page element" );
      return false;
    }
    else
      elem.scrollIntoView(true);
  }


  /**
   * Handle a browser navigation action
   * @param {Object} ev - The associated event
   * @private
   */
  _browserAction( ev ) {
    // Can we navigate?
    if ( ! ev.state || ! ev.state.hasOwnProperty( 'url' ) )
      return;
    this.log( 'debug', "Browser navigation attempted, navigating", ev );
    // Reset the current state to the history-stored state, clear the next and then navigate.
    this.navigate( ev.state.url.path, ev.state.url.hash, ev.state.url.query, "none" );
  }



  /**
   * Navigate to an application view
   * @async
   * @param {String} [path=undefined]   - The appication-based relative path to navigate to. If not provided, the default url or url from the state is used.
   * @param {String} [hash=""]          - The hash or page element to scroll to after navigating
   * @param {Object} [query={}]         - The search query arguments for the view(s)
   * @param {String} [mode=push]        - The mode for navigation state history control. Push for new navigations, replace for initial navigation.
   */
  async navigate( path=undefined, hash="", query={},  mode="push" ) {
    this.log( 'debug', "Attempting navigate to", path, "with mode", mode );
    const prefix = this.prefix;
    const nextUrl = {};
    const prevUrl = this.getStateProp( 'url', false ) || {};
    const prevQuery = prevUrl.query;
    const prevPath = prevUrl.path;
    let next;

    // Which default landing page if nothing specified?
    if ( path == undefined || path == "" || path == "/" ) {
      if ( hash != undefined && hash != "" && prevPath != undefined ) 
        path = prevPath;
      else
        path = this.defaultpage;
    }

    // Navigating to external?
    if ( path.match( /^https?/i ) !== null ) {
      window.location.href = path;
      return;
    }

    // TODO: Support relative paths (../foo)

    // Create URL struct
    nextUrl[ 'path' ] = path;
    nextUrl[ 'hash' ] = hash;
    nextUrl[ 'query' ] = query;

    // Prevent same page navigation without query or state change
    if ( prevPath == path && equal( prevQuery, query ) ) {
      next = Object.assign( {}, this.getState( true ), { 'url': nextUrl } );
      if ( equal( this.getState(false), next ) ) {
        this.log( 'debug', 'Thwarting same-page navigation without state change' );
        return false;
      }
    }

    // Add the target path into the next state
    this.setStateProp( 'url', nextUrl, true );

    // If navigating somewhere normally, update the browser state before we leave
    if ( mode == "push" )
      history.replaceState( this.getState( false ), '', prefix + prevPath );

    // Before navigation hooks and events
    if ( ! this._beforeNavigate() )
      return false;

    // Immediately roll state before navigation
    if ( mode == "replace" ) {
      this.log( "debug", "Replacing navigation state for path", prefix + path );
      history.replaceState( this.getState( true ), '', prefix + path );
    }
    if ( mode == "push" ) {
      this.log( "debug", "Pushing new navigation state for path", prefix + path );
      history.pushState( this.getState( true ), '', prefix + path ); 
    }

    // If traversing history, recover our
    if ( mode == "none" ) {
      this.setState( history.state, false );
      this.setState( {}, true );
    }
    else {
      this.setState( this.getState( true ), false );
      this.setState( {}, true );
    }

    // Try to navigate
    try {
      await this.router.navigate();
    }
    catch( err ) {
      if ( err.navCode ) {
        if ( err.navCode == 404 && this.config[ 'notFoundPage' ] )
          this.router.navigate( this.config[ 'notFoundPage' ] );
        else if ( err.navCode == 500 && this.config[ 'errorPage' ] )
          this.router.navigate( this.config[ 'errorPage' ] );
      }
    }

    // After navigation hooks and events
    this._afterNavigate();
  }


  /**
   * Fire after navigation hooks and events
   */
  _afterNavigate() {
    const url = this.getStateProp( 'url', false );
    let afterev;

    // Ensure Z Index is reset
    this.setStateProp( 'top-z-index', 1 );

    // Hooks
    if ( typeof this['afterNavigate'] == "function" ) {
      cont = this.afterNavigate( { 
        'path': url.path,
        'hash': url.hash,
        'query': url.query
      } );
      if ( ! cont ) {
        this.log( "debug", "The beforeNavigate handler returned false, preventing navigation." );
        return false;
      }
    }

    // Events
    afterev = new CustomEvent( 'after-navigate', { detail: { 'path': url.path, 'hash': url.hash, 'query': url.query } } );
    this.dispatchEvent( afterev );
  }


  /**
   * Fire hooks and events prior to navigation, allow cancellation
   */
  _beforeNavigate() {
    const url = this.getStateProp( 'url', true );
    let cont = true;
    let beforeev;

    // Call hooks
    if ( typeof this['beforeNavigate'] == "function" ) {
      cont = this.beforeNavigate( { 
        'path': url.path,
        'hash': url.hash,
        'query': url.query
      } );
      if ( ! cont ) {
        this.log( "debug", "The beforeNavigate handler returned false, preventing navigation." );
        return false;
      }
    }

    // Events too
    beforeev = new CustomEvent( 'before-navigate', { cancelable: true, detail: { 'path': url.path, 'hash': url.hash, 'query': url.query } } );
    cont = this.dispatchEvent( beforeev );
    if ( ! cont ) {
      this.log( 'debug', 'The beforeNavigate event handler cancelled navigation.' );
      return false;
    }

    return true;
  }


  /* State management methods */

  /**
   * Add to the current or forward application state store.
   * @param {Object} toAdd          - An Object of keys and values to store in the state.
   * @param {Boolean} [next=false]  - Indicates if the addition is for the next navigation state.
   */
  addToState( toAdd, next=false ) {
    if ( next ) this.forwardstate = Object.assign( this.forwardstate, toAdd );
    else this.state = Object.assign( this.state, toAdd );
  }

  /**
   * Replace the current or forward application state store.
   * @param {Object} newState       - An Object of keys and values to store.
   * @param {Boolean} [next=false]  - Indicates if the new state is for the next navigation state.
   */
  setState( newState, next=false ) {
    if ( next ) this.forwardstate = newState;
    else this.state = newState;
  }

  /**
   * Get the current or forward application state store.
   * @param {Boolean} [next=false]  - Indicates if the fetch is for the current or next navigation states.
   */
  getState( next=false ) {
    if ( next ) return this.forwardstate;
    else return this.state;
  }

  /**
   * Get a single property from the current or forward application state store.
   * @param {String} propName       - The key name of the property to fetch.
   * @param {Boolean} [next=false]  - Indicates if the fetch is for the current or next navigation states.
   */
  getStateProp( propName, next=false ) {
    if ( next ) return this.forwardstate[ propName ];
    else return this.state[ propName ];
  }


  /**
   * Set a single property for the current or next application state store
   * @param {String} propName       - The key name of the property to set
   * @param {*} propValue           - The value of the property to set
   * @param {Boolean} [next=false]  - Indicates if the set is for the current or next state.
   */
  setStateProp( propName, propValue, next=false ) {
    if ( next ) this.forwardstate[ propName ] = propValue;
    else this.state[ propName ] = propValue;
  }


  /**
   * Remove a single property from the state
   * @param {String} propName       - The key name of the property to remove.
   * @param {Boolean} [next=false]  - Indicates if the remove is for the current or next state.
   */
  removeStateProp( propName, next=false ) {
    if ( next ) delete this.forwardstate[ propName ];
    else delete this.state[ propName ];
  }


  /**
   * Application logger
   * @param {String} level    - The log level for this message. One of debug, info, warn, error.
   * @param {...Object} args  - Arguments proxied to the console.log function.
   */
  log( level, ...args ) {
    switch( level ) {
      case 'debug': 
        if ( this.config.debug )
          console.debug( '[debug]', ...args );
        break;
      case 'info':
        console.info( '[info]', ...args );
        break;
      case 'warning':
      case 'warn':
        console.warn( '[warn]', ...args );
        break;
      case 'error':
        console.error( '[error]', ...args );
    }
  }


  /**
   * Get the top-most z-index taking a given range into account
   * @param {Number} range        - How large the range to provide
   */
  getTopZ( range ) {
    const current = this.getStateProp( 'top-z-index' );
    const next = current + range;
    this.setStateProp( 'top-z-index', next );
    return next;
  }


  /**
   * Ensure prefix and default page paths are formatted as needed
   * @private
   */
  _cleanPaths() {
    let conf = this.config;

    // Enforce slashes at start and end of the prefix
    if ( conf.prefix.length > 1 ) {
      if ( conf.prefix.charAt( 0 ) != "/" )
        conf.prefix = "/" + conf.prefix;
      if ( ! conf.prefix.endsWith( "/" ) )
        conf.prefix = conf.prefix + "/";
    }

    // No leading or trailing slash of default page
    if ( conf.defaultpage.charAt( 0 ) == "/" )
      conf.defaultpage = conf.defaultpage.slice( 1 );
    if ( conf.defaultpage.endsWith( "/" ) )
      conf.defaultpage = conf.defaultpage.slice( 0, -1 );

  }

  /**
   * Get the current URL information
   */
  get url() {
    return Object.assign( { 'path': "", 'hash': "", 'query': {} }, this.getStateProp( 'url', false ) );
  }

  get name() {
    return this.config['name'];
  }

  get prefix() {
    return this.config['prefix'];
  }

  get defaultpage() {
    return this.config['defaultpage'];
  }

  get container() {
    return this.config['container'];
  }

}


