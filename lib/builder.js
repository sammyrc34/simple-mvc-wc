

import { render, nothing } from 'lit';


class SimpleBuilder {

  constructor( app ) {
    this.app = app;
  }


  /*
   * Render a view somewhere
   * @async
   * @param {Object[]} views              - The views to render
   * @param {String} views[].path         - The path for the view
   * @param {Object} views[].class        - The view class
   * @param {Array}  views[].descendants  - Decendent views
   * @param {Object} query={}             - Any query for the render
   * @param {String} [target]             - The target document element ID.
   */
  async render( views, query={}, target=this.app.container ) {
    this.app.log( 'debug', 'Rendering views', views.map( v => v[ 'path' ] ) );
    var container = document.getElementById( target );
    if ( container == null ) {
      this.app.log( 'error', "Unable to locate target element to render to", target );
      throw new Error( "Unable to locate target element to render into" );
    }

    // Build a template tree from the first view
    await this._buildFrom( views[0], container, query );
  }


  /**
   * Rebuild and render a view and any sub-views
   */
  async reRender( view, query={} ) {
    // Build recursively from outer most to inner
    try { 
      this.app.log( "debug", "Building existing view " + view[ 'class' ].name + " to previous target" );
      await this._buildFrom( view, view.target, query );
    }
    catch( err ) {
      this.app.log( "error", "Failed to re-render view", err.message );
      throw err;
    }

  }


  /** 
   * Clear a view from the DOM
   * @param {Object} view                   - The view to build
   * @param {HTMLElement} view.target       - The target element for the view to clear
   * @param {Object[]} currentViews         - All currently rendered views
   */
  clear( view, currentViews ) {
    if ( view.target && document.body.contains( view.target ) ) {
      // Don't clear in the case of shared targets
      if ( currentViews.find( v => v.target == view.target ) )
        return;
      render( nothing, view.target );
    }
  }


  /*
   * Recursively build content to render
   * @async
   * @private
   * @param {Object} view                   - The view to build
   * @param {String} view.path              - The path of view
   * @param {Object} view.class             - The class for the view
   * @param {String} view.context           - Any context name for the view
   * @param {Object} view.params            - Any URL bind parameters for the view
   * @param {Array}  view.descendants       - Descendant views for this view
   * @param {HTMLElement} target            - The target element container
   * @param {Object} query                  - The query when building this view
   * @throws                                - In event of any error building and rendering
   */
  async _buildFrom( view, target, query ) {
    let targets = [], proms = [];
    let tmptarget, prom, result;

    // Build recursively from outer most to inner
    try { 

      // Build the view
      result = await view[ 'class' ].build( view.params, query, view.context );

      // Render
      this.app.log( "debug", "Rendering view", view[ 'class' ].name );
      await render( result, target ); 
      view.target = target;

      // Sub views to build?
      if ( view.descendants.length ) {  

        // Get indexed targets list for sub-views
        targets = view[ 'class' ].getTargets( view.descendants );
        if ( targets.length !== view.descendants.length )
          throw new Error( "Unable to build view, invalid sub-view targets provided" );

        // Start to build subviews recursively without waiting
        view.descendants.forEach( ( subview, idx ) => {
          this.app.log( "debug", "Rendering descendant view", subview['class'].name );
          tmptarget = targets[ idx ];
          prom = this._buildFrom( subview, tmptarget, query );
          proms.push( prom );
        } );

        // Wait for all descendants to build and render
        await Promise.all( proms );

        // Save descendant targets
        view.targets = targets;
      }
    }
    catch( e ) {
      this.app.log( "error", "Failed during render", e );
      throw e;
    }
  }



}

export default SimpleBuilder;

